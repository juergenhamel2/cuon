package de.cyrus.cuonAddress;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Handler;
import android.widget.EditText;

public class notes implements constants {
	// XmlRPC
	public XMLRPCClient client;

	public HashMap cuonUser;

	public HashMap<String, String> allTabs;
	public HashMap tabs;
	public List<String> liTabs = new ArrayList<String>();
	public String[] liTitle;
	private String activeTab;
	public int activeModul;
	public EditText eTitle;
	public EditText eText;

	public int actualIndex = 0;
	public int actualPartnerIndex = 0;
	public int actualDmsID = 0;
	public int actualPartnerID = 0;

	public String esTitle;
	public String esCategory;
	public String esSub;
	public File actualPDF;

	public void init(XMLRPCClient oClient, HashMap user) {
		client = oClient;
		cuonUser = user;

	}

	public void start() {
		System.out.println("dms start");

	}

	public void show(int AddressID) {
		System.out.println("note start");
		XMLRPCMethod method = new XMLRPCMethod("Address.sl_getNotes", new XMLRPCMethodCallback() {
			public void callFinished(Object result) {
				System.out.println("result = " + ", " + result.toString());
				allTabs = (HashMap<String, String>) result;
				// tabs = allTabs.get(0);

				liTabs.clear();

				for (String key : allTabs.keySet()) {
					System.out.println(" key  = " + key);

					System.out.println(" id = " + allTabs.get(key).toString());
					liTabs.add(key);
				}
				System.out.println("Set dms Fields 0");
				try {
					System.out.println("Set dms Fields 0.5");
					actualIndex = 0;
					System.out.println("Set dms Fields 1");
					setNotesFields(actualIndex);
					System.out.println("Set dms Fields 2");

				} catch (Exception cus1) {
					System.out.println("error dms1");
					System.out.println(cus1 + " " + cus1.getClass().getName() + " " + cus1.getMessage());
				}
			}
		});

		Object[] params = { cuonUser, AddressID };
		method.call(params);
	}

	public void setNotesFields(int listIndex) {
		// System.out.println("Set dms Fields - 1.5");
		// System.out.println("dms liTabs 0 = " + listIndex);
		// System.out.println("dms liTabs 1 = " + liTabs.get(0));

		System.out.println("dmsfield = " + allTabs.get(Integer.toString(listIndex)).toString());

		// String[] liTitle =
		// allTabs.get(liTabs.get(listIndex)).toString().split(cCut1);

		eTitle.setText(liTitle[listIndex]);
		eText.setText(allTabs.get(Integer.toString(listIndex)).toString());

	}

	public void swipeLeft() {
		// Toast.makeText(this, "swipe notes right", Toast.LENGTH_SHORT).show();
		System.out.println("swipe right ");
		if (actualIndex < (liTabs.size() - 1)) {
			actualIndex += 1;

			setNotesFields(actualIndex);
		}
	}

	public void swipeRight() {

		System.out.println("swipe left ");
		if (actualIndex > 0) {
			actualIndex -= 1;

			setNotesFields(actualIndex);
		}

	}

	interface XMLRPCMethodCallback {
		void callFinished(Object result);
	}

	class XMLRPCMethod extends Thread {
		private String method;
		private Object[] params;
		private Handler handler;
		private XMLRPCMethodCallback callBack;

		public XMLRPCMethod(String method, XMLRPCMethodCallback callBack) {
			this.method = method;
			this.callBack = callBack;
			handler = new Handler();
		}

		public void call() {
			call(null);
		}

		public void call(Object[] params) {
			// status.setTextColor(0xff80ff80);
			// status.setError(null);
			// status.setText("Calling host " + uri.getHost());
			// tests.setEnabled(false);
			this.params = params;
			start();
		}

		@Override
		public void run() {
			try {
				final long t0 = System.currentTimeMillis();
				final Object result = client.callEx(method, params);
				final long t1 = System.currentTimeMillis();
				handler.post(new Runnable() {
					public void run() {
						// tests.setEnabled(true);
						// status.setText("XML-RPC call took " + (t1-t0) +
						// "ms");
						callBack.callFinished(result);
					}
				});
			} catch (final XMLRPCFault e) {
				handler.post(new Runnable() {
					public void run() {
						// testResult.setText("");
						// tests.setEnabled(true);
						// status.setTextColor(0xffff8080);
						// status.setError("", errorDrawable);
						// status.setText("Fault message: " + e.getFaultString()
						// + "\nFault code: " + e.getFaultCode());
						// Log.d("Test", "error", e);
					}
				});
			} catch (final XMLRPCException e) {
				handler.post(new Runnable() {
					public void run() {
						// testResult.setText("");
						// tests.setEnabled(true);
						// status.setTextColor(0xffff8080);
						// status.setError("", errorDrawable);
						Throwable couse = e.getCause();
						// if (couse instanceof HttpHostConnectException) {
						// status.setText("Cannot connect to " + uri.getHost()
						// +
						// "\nMake sure server.py on your development host is running !!!");
						// } else {
						// status.setText("Error " + e.getMessage());
						// }
						// Log.d("Test", "error", e);
					}
				});
			}
		}

	}
}
