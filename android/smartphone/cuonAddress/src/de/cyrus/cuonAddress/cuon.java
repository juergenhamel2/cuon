package de.cyrus.cuonAddress;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.os.Handler;
import android.util.Log;
import android.widget.EditText;

public class cuon implements constants {
	// XmlRPC
	public XMLRPCClient client;

	public HashMap cuonUser;

	public HashMap<String, String> allTabs;
	public HashMap tabs;
	public List<String> liTabs = new ArrayList<String>();

	private HashMap<String, String> partnerTabs;
	public List<String> liPartnerTabs = new ArrayList<String>();

	private String activeTab;
	private int activeModul;
	public EditText eAddress;
	public EditText ePhone1;
	public EditText ePhone2;
	public EditText eEmail1;
	public EditText ePartnerAddress;
	public EditText ePartnerPhone1;
	public EditText ePartnerPhone2;
	public EditText ePartnerPhone3;
	public int actualIndex = 0;
	public int actualPartnerIndex = 0;
	public int actualAddressID = 0;
	public int actualPartnerID = 0;

	public void init(XMLRPCClient oClient, HashMap user) {
		client = oClient;
		cuonUser = user;

	}

	public void start() {
		System.out.println("Cuon start");

	}

	public void searchAdress(String s1, String s2, String s3, String s4) {

		activeModul = mCuonAddress1;
		XMLRPCMethod method = new XMLRPCMethod("Address.sl_search1", new XMLRPCMethodCallback() {
			public void callFinished(Object result) {

				try {
					System.out.println("result = " + ", " + result.toString());
					allTabs = (HashMap<String, String>) result;
					// tabs = allTabs.get(0);

					liTabs.clear();

					for (String key : allTabs.keySet()) {
						System.out.println(" key  = " + key);
						System.out.println(" id = " + allTabs.get(key).toString());
						liTabs.add(key);
					}

					actualIndex = 0;
					setAddressFields(actualIndex);

				} catch (Exception cus1) {
					System.out.println("error c1");
					System.out.println(cus1 + " " + cus1.getClass().getName() + " " + cus1.getMessage());
				}
			}

		});

		Object[] params = { cuonUser, s1, s2, s3, s4 };
		method.call(params);

	}

	public String getActualAddress() {

		return allTabs.get(liTabs.get(actualIndex)).toString();
	}

	public void searchPartner(EditText seAddress, EditText sePhone1, EditText sePhone2, EditText sePhone3) {
		ePartnerAddress = seAddress;
		ePartnerPhone1 = sePhone1;
		ePartnerPhone2 = sePhone2;
		ePartnerPhone3 = sePhone3;
		activeModul = mCuonPartner1;
		XMLRPCMethod method = new XMLRPCMethod("Address.sl_getPartner", new XMLRPCMethodCallback() {
			public void callFinished(Object result) {

				try {
					System.out.println("result = " + ", " + result.toString());
					partnerTabs = (HashMap<String, String>) result;
					// tabs = allTabs.get(0);

					liPartnerTabs.clear();
					for (String key : partnerTabs.keySet()) {
						System.out.println(" key  = " + key);
						System.out.println(" id = " + partnerTabs.get(key).toString());
						liPartnerTabs.add(key);
					}

					actualPartnerIndex = 0;
					setPartnerFields(actualPartnerIndex);

				} catch (Exception cus2) {
					System.out.println("error c2");
					System.out.println(cus2 + " " + cus2.getClass().getName() + " " + cus2.getMessage());
				}
			}

		});

		Object[] params = { cuonUser, actualAddressID };
		method.call(params);
	}

	public void setAddressFields(int listIndex) {

		System.out.println("listindex = " + listIndex);
		System.out.println(liTabs.get(0).toString());
		System.out.println(allTabs.get(liTabs.get(listIndex)).toString());
		eAddress.setText(allTabs.get(liTabs.get(listIndex)).toString());
		actualAddressID = Integer.parseInt(liTabs.get(listIndex).toString());

		// fetch first phone number
		XMLRPCMethod method = new XMLRPCMethod("Address.sl_getPhones", new XMLRPCMethodCallback() {
			public void callFinished(Object result) {

				System.out.println("result = " + ", " + result.toString());
				HashMap<String, String> phones = (HashMap<String, String>) result;
				ePhone1.setText(phones.get("phone"));
				ePhone2.setText(phones.get("phone_handy"));
				eEmail1.setText(phones.get("email"));
			}

		});

		Object[] params = { cuonUser, actualAddressID };
		method.call(params);

	}

	public void setPartnerFields(int listIndex) {
		System.out.println("Partner listIndex = " + listIndex);
		System.out.println("Partner listIndex 2 = " + liPartnerTabs.get(listIndex).toString());

		ePartnerAddress.setText(partnerTabs.get(liPartnerTabs.get(listIndex)).toString());
		System.out.println("Partner listIndex 3 ");
		actualPartnerID = Integer.parseInt(liPartnerTabs.get(listIndex).toString());
		System.out.println("actualPartnerID = " + actualPartnerID);
		// fetch phone numbers
		XMLRPCMethod method = new XMLRPCMethod("Address.sl_getPartnerPhones", new XMLRPCMethodCallback() {
			public void callFinished(Object result) {

				System.out.println("result = " + ", " + result.toString());
				HashMap<String, String> phones = (HashMap<String, String>) result;
				ePartnerPhone1.setText(phones.get("phone1"));
				ePartnerPhone2.setText(phones.get("phone"));
				ePartnerPhone3.setText(phones.get("phone_handy"));

			}

		});

		Object[] params = { cuonUser, actualPartnerID };
		method.call(params);

	}

	public void swipeLeft() {
		switch (activeModul) {
		case mCuonAddress1:
			System.out.println("swipe right ");
			if (actualIndex < (liTabs.size() - 1)) {
				actualIndex += 1;
			}

			setAddressFields(actualIndex);
			break;
		case mCuonPartner1:
			System.out.println("swipe right ");
			if (actualPartnerIndex < (liPartnerTabs.size() - 1)) {
				actualPartnerIndex += 1;
			}

			setPartnerFields(actualPartnerIndex);
			break;

		}

	}

	public void swipeRight() {
		switch (activeModul) {
		case mCuonAddress1:
			System.out.println("swipe left ");
			if (actualIndex > 0) {
				actualIndex -= 1;
			}

			setAddressFields(actualIndex);
			break;

		case mCuonPartner1:
			System.out.println("swipe left ");
			if (actualPartnerIndex > 0) {
				actualPartnerIndex -= 1;
			}

			setPartnerFields(actualPartnerIndex);
			break;

		}
	}

	interface XMLRPCMethodCallback {
		void callFinished(Object result);
	}

	class XMLRPCMethod extends Thread {
		private String method;
		private Object[] params;
		private Handler handler;
		private XMLRPCMethodCallback callBack;

		public XMLRPCMethod(String method, XMLRPCMethodCallback callBack) {
			this.method = method;
			this.callBack = callBack;
			handler = new Handler();
		}

		public void call() {
			call(null);
		}

		public void call(Object[] params) {
			// status.setTextColor(0xff80ff80);
			// status.setError(null);
			// status.setText("Calling host " + uri.getHost());
			// tests.setEnabled(false);
			this.params = params;
			start();
		}

		@Override
		public void run() {
			try {
				final long t0 = System.currentTimeMillis();
				final Object result = client.callEx(method, params);
				final long t1 = System.currentTimeMillis();
				handler.post(new Runnable() {
					public void run() {
						// tests.setEnabled(true);
						// status.setText("XML-RPC call took " + (t1-t0) +
						// "ms");
						callBack.callFinished(result);
					}
				});
			} catch (final XMLRPCFault e) {
				handler.post(new Runnable() {
					public void run() {
						// testResult.setText("");
						// tests.setEnabled(true);
						// status.setTextColor(0xffff8080);
						// status.setError("", errorDrawable);
						// status.setText("Fault message: " + e.getFaultString()
						// + "\nFault code: " + e.getFaultCode());
						Log.d("Test", "error", e);
					}
				});
			} catch (final XMLRPCException e) {
				handler.post(new Runnable() {
					public void run() {
						// testResult.setText("");
						// tests.setEnabled(true);
						// status.setTextColor(0xffff8080);
						// status.setError("", errorDrawable);
						Throwable couse = e.getCause();
						// if (couse instanceof HttpHostConnectException) {
						// status.setText("Cannot connect to " + uri.getHost()
						// +
						// "\nMake sure server.py on your development host is running !!!");
						// } else {
						// status.setText("Error " + e.getMessage());
						// }
						// Log.d("Test", "error", e);
					}
				});
			}
		}
	}

}