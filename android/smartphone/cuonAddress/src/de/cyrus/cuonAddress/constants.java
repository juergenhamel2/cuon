package de.cyrus.cuonAddress;

interface constants {
	static final int[] cuonAddressVersion = { 12, 1, 26 };
	static final int mLocalChat = 0;
	static final int mIM = 1;
	static final int mGroupIM = 2;

	static final int mCuon = 100;
	static final int mCuonAddress1 = 2000;
	static final int mCuonPartner1 = 2100;
	static final int mCuonAddressNote = 2010;
	static final int mCuonDmsAddress1 = 11001;
	static final int mCuonDmsPartner1 = 11002;

	static final String cCut1 = Character.toString((char) 172);
	static final String cCut2 = Character.toString((char) 175);
	static final String cCut3 = Character.toString((char) 184);
}