package de.cyrus.cuonAddress;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import android.os.Environment;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.widget.EditText;

public class dms implements constants {
	// XmlRPC
	public XMLRPCClient client;

	public HashMap cuonUser;

	public HashMap<String, String> allTabs;
	public HashMap tabs;
	public List<String> liTabs = new ArrayList<String>();

	private String activeTab;
	public int activeModul;
	public EditText eTitle;
	public EditText eCategory;
	public EditText eSub;
	public EditText eSearchTitle;
	public EditText eSearchCategory;
	public EditText eSearchSub;
	public int actualIndex = 0;
	public int actualPartnerIndex = 0;
	public int actualDmsID = 0;
	public int actualPartnerID = 0;

	public String esTitle;
	public String esCategory;
	public String esSub;
	public File actualPDF;

	public void init(XMLRPCClient oClient, HashMap user) {
		client = oClient;
		cuonUser = user;

	}

	public void start() {
		System.out.println("dms start");

	}

	public void searchAddressInfo(int addressID) {

		XMLRPCMethod method = new XMLRPCMethod("DMS.sl_showDMS", new XMLRPCMethodCallback() {
			public void callFinished(Object result) {
				System.out.println("result = " + ", " + result.toString());
				allTabs = (HashMap<String, String>) result;
				// tabs = allTabs.get(0);

				liTabs.clear();

				for (String key : allTabs.keySet()) {
					System.out.println(" key  = " + key);

					System.out.println(" id = " + allTabs.get(key).toString());
					liTabs.add(key);
				}
				System.out.println("Set dms Fields 0");
				try {
					System.out.println("Set dms Fields 0.5");
					actualIndex = 0;
					System.out.println("Set dms Fields 1");
					setDmsFields(actualIndex);
					System.out.println("Set dms Fields 2");

				} catch (Exception cus1) {
					System.out.println("error dms1");
					System.out.println(cus1 + " " + cus1.getClass().getName() + " " + cus1.getMessage());
				}
			}
		});

		Object[] params = { cuonUser, addressID, activeModul, esTitle, esCategory, esSub };
		method.call(params);
	}

	public void setDmsFields(int listIndex) {
		// System.out.println("Set dms Fields - 1.5");
		// System.out.println("dms liTabs 0 = " + listIndex);
		// System.out.println("dms liTabs 1 = " + liTabs.get(0));

		System.out.println("dmsfield = " + allTabs.get(liTabs.get(listIndex)).toString());

		String[] liTitle = allTabs.get(liTabs.get(listIndex)).toString().split(cCut1);
		String[] liCategory = liTitle[1].toString().split(cCut2);

		eTitle.setText(liTitle[0].toString());
		eCategory.setText(liCategory[0].toString());
		eSub.setText(liCategory[1].toString());
		actualDmsID = Integer.parseInt(liTabs.get(listIndex).toString());

	}

	public File saveFile(String Data) {

		String Name = UUID.randomUUID().toString();
		File sPDF = new File("none");

		byte[] bData = Base64.decode(Data, 0);

		File sdCard = Environment.getExternalStorageDirectory();
		File dir;
		try {
			dir = new File(sdCard.getAbsolutePath() + "/cuon/dms");
			dir.mkdirs();
		} catch (Exception ex) {

			dir = new File("/cuon/dms");
			dir.mkdirs();
		}

		/* Convert the Bytes read to a String. */
		try {
			sPDF = new File(dir + "/" + Name + ".pdf");
			System.out.println("dms21 " + sPDF.toString());
			FileOutputStream fos = new FileOutputStream(sPDF);
			fos.write(bData);
			fos.flush();
			fos.close();
		} catch (Exception ex2) {
			System.out.println("error dms 20.5");
			System.out.println(ex2 + " " + ex2.getClass().getName() + " " + ex2.getMessage());
		}
		return sPDF;
	}

	public void swipeLeft() {
		switch (activeModul) {
		case mCuonDmsAddress1:
			System.out.println("swipe right ");
			if (actualIndex < (liTabs.size() - 1)) {
				actualIndex += 1;
			}

			setDmsFields(actualIndex);
			break;
		}
	}

	public void swipeRight() {
		switch (activeModul) {
		case mCuonDmsAddress1:
			System.out.println("swipe left ");
			if (actualIndex > 0) {
				actualIndex -= 1;
			}

			setDmsFields(actualIndex);
			break;
		}
	}

	interface XMLRPCMethodCallback {
		void callFinished(Object result);
	}

	class XMLRPCMethod extends Thread {
		private String method;
		private Object[] params;
		private Handler handler;
		private XMLRPCMethodCallback callBack;

		public XMLRPCMethod(String method, XMLRPCMethodCallback callBack) {
			this.method = method;
			this.callBack = callBack;
			handler = new Handler();
		}

		public void call() {
			call(null);
		}

		public void call(Object[] params) {
			// status.setTextColor(0xff80ff80);
			// status.setError(null);
			// status.setText("Calling host " + uri.getHost());
			// tests.setEnabled(false);
			this.params = params;
			start();
		}

		@Override
		public void run() {
			try {
				final long t0 = System.currentTimeMillis();
				final Object result = client.callEx(method, params);
				final long t1 = System.currentTimeMillis();
				handler.post(new Runnable() {
					public void run() {
						// tests.setEnabled(true);
						// status.setText("XML-RPC call took " + (t1-t0) +
						// "ms");
						callBack.callFinished(result);
					}
				});
			} catch (final XMLRPCFault e) {
				handler.post(new Runnable() {
					public void run() {
						// testResult.setText("");
						// tests.setEnabled(true);
						// status.setTextColor(0xffff8080);
						// status.setError("", errorDrawable);
						// status.setText("Fault message: " + e.getFaultString()
						// + "\nFault code: " + e.getFaultCode());
						Log.d("Test", "error", e);
					}
				});
			} catch (final XMLRPCException e) {
				handler.post(new Runnable() {
					public void run() {
						// testResult.setText("");
						// tests.setEnabled(true);
						// status.setTextColor(0xffff8080);
						// status.setError("", errorDrawable);
						Throwable couse = e.getCause();
						// if (couse instanceof HttpHostConnectException) {
						// status.setText("Cannot connect to " + uri.getHost()
						// +
						// "\nMake sure server.py on your development host is running !!!");
						// } else {
						// status.setText("Error " + e.getMessage());
						// }
						// Log.d("Test", "error", e);
					}
				});
			}
		}
	}

}