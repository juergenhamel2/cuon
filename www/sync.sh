#!/bin/sh
case $1 in


#rsync  -r -avz --numeric-ids -e 'ssh -p 22'  ~/work2/slandy/bin/slandy.apk root@cuon.org://var/www/vhosts/cuon_web/downloads


old)


    
    rsync  -avz --numeric-ids -e 'ssh -p 22'  *.html root@cuon.org://var/www/vhosts/cuon_web
    
    rsync  -avz --numeric-ids -e 'ssh -p 22'  *.js root@cuon.org://var/www/vhosts/cuon_web
    
    rsync  -avz --numeric-ids -e 'ssh -p 22'  *.php root@cuon.org://var/www/vhosts/cuon_web

    rsync  -avz --numeric-ids -e 'ssh -p 22'  *.css root@cuon.org://var/www/vhosts/cuon_web
    
    rsync  -avz --numeric-ids -e 'ssh -p 22'  *.gif root@cuon.org://var/www/vhosts/cuon_web
    
    rsync  -r -avz --numeric-ids -e 'ssh -p 22'  FAQ root@cuon.org://var/www/vhosts/cuon_web
    
     rsync  -avz --numeric-ids -e 'ssh -p 22' css/* root@cuon.org://var/www/vhosts/cuon_web/css 
    rsync  -avz --numeric-ids -e 'ssh -p 22' Crowdfunding/*.html root@cuon.org://var/www/vhosts/cuon_web/Crowdfunding 
    rsync  -avz --numeric-ids -e 'ssh -p 22' Cuon/*.xhtml root@cuon.org://var/www/vhosts/cuon_web/Cuon 
    rsync  -avz --numeric-ids -e 'ssh -p 22' Cuon/*.html root@cuon.org://var/www/vhosts/cuon_web/Cuon
    rsync  -avz --numeric-ids -e 'ssh -p 22' Cuon/*.css root@cuon.org://var/www/vhosts/cuon_web/Cuon 
    rsync  -avz --numeric-ids -e 'ssh -p 22' Cuon/*.png root@cuon.org://var/www/vhosts/cuon_web/Cuon 
    
    rsync  -avz --numeric-ids -e 'ssh -p 22' Cuon/*.pdf root@cuon.org://var/www/vhosts/cuon_web/Cuon 
    rsync  -avz --numeric-ids -e 'ssh -p 22' Cuon/images/screenshots/*.png root@cuon.org://var/www/vhosts/cuon_web/Cuon/images/screenshots
    rsync  -avz --numeric-ids -e 'ssh -p 22'  Cyrus/*.html root@cuon.org://var/www/vhosts/cuon_web/Cyrus
    rsync  -avz --numeric-ids -e 'ssh -p 22'  Downloads/*.html root@cuon.org://var/www/vhosts/cuon_web/Downloads 
    rsync  -avz --numeric-ids -e 'ssh -p 22' cuon_more/*.html root@cuon.org://var/www/vhosts/cuon_web/cuon_more 
    rsync  -avz --numeric-ids -e 'ssh -p 22' Screenshots/*.html root@cuon.org://var/www/vhosts/cuon_web/Screenshots
    rsync  -avz --numeric-ids -e 'ssh -p 22' CuonDia/*.html root@cuon.org://var/www/vhosts/cuon_web/CuonDia
    
    # Images
    rsync -r -avz --numeric-ids -e 'ssh -p 22' images root@cuon.org://var/www/vhosts/cuon_web
    
    # Api
    
    #client api
    
    rsync -r  -avz --numeric-ids -e 'ssh -p 22' /misc/Projekte/cuon/api/html/* root@cuon.org://var/www/vhosts/cuon_web/api
    rsync  -r -avz --numeric-ids -e 'ssh -p 22' /misc/Projekte/cuon/api/pdf/*.pdf  root@cuon.org://var/www/vhosts/cuon_web/api
    rsync -r  -avz --numeric-ids -e 'ssh -p 22' /misc/Projekte/cuon/html/* root@cuon.org://var/www/vhosts/cuon_web/doc
    
    # server api
    rsync  -r -avz --numeric-ids -e 'ssh -p 22' /misc/Projekte/cuon/api/cuonserver/* root@cuon.org://var/www/vhosts/cuon_web/api/cuonserver
    
    
    # public
    rsync -r -avz --numeric-ids -e 'ssh -p 22' public_html root@cuon.org://var/www/vhosts/cuon_web
    
    # English
    rsync  -avz --numeric-ids -e 'ssh -p 22' en_Cuon/*.html root@cuon.org://var/www/vhosts/cuon_web/en_Cuon
    rsync  -avz --numeric-ids -e 'ssh -p 22' en_Cuon/*.xhtml root@cuon.org://var/www/vhosts/cuon_web/en_Cuon
    rsync  -avz --numeric-ids -e 'ssh -p 22' en_Cuon/*.css root@cuon.org://var/www/vhosts/cuon_web/en_Cuon
    rsync  -avz --numeric-ids -e 'ssh -p 22' en_Cuon/*.pdf root@cuon.org://var/www/vhosts/cuon_web/en_Cuon
    rsync -r -avz --numeric-ids -e 'ssh -p 22' en_Cuon/images root@cuon.org://var/www/vhosts/cuon_web/en_Cuon/
    
	# Translate
	cp /misc/Projekte/cuon/cuon_client/*.po Translate
	rsync  -avz --numeric-ids -e 'ssh -p 22' Translate/* root@cuon.org://var/www/vhosts/cuon_web/Translate
    
  
    cd autopostgres 
    postgresql_autodoc -d cuon
    cd ..

    
    rsync  -avz --numeric-ids -e 'ssh -p 22' autopostgres/cuon.html root@cuon.org://var/www/vhosts/cuon_web/database_html

    cd ../cuon_client/cpp
    doxygen Doxyfile
    cd ../../www


    rsync -r -avz --numeric-ids -e 'ssh -p 22' ../cuon_client/cpp/doc/* root@cuon.org://var/www/vhosts/cuon_web/doc_cpp



;;

*)

 rsync -r -avz --numeric-ids -e 'ssh -p 22' * root@cuon.org://var/www/vhosts/cuon_web

esac



# now set the rights

ssh root@cuon.org "cd /var/www/ ; chown -R www-data:www-data *"
