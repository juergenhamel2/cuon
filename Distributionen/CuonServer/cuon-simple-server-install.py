#!/usr/bin/python2.7

# cuon_server install
import os,  sys,  platform
import subprocess,  shlex,  shutil
import commands
import locale
import pwd,  grp
from gi.repository import Gtk
import ConfigParser

class cssi():
    def __init__(self,  user=None):
        
        self.user = user
        self.win = None
        self.grid = None
        self.CalendarDir="/usr/local/iCal"
        self.program_names = ["Postgres", "Subversion", "ssh"]
        self.programs = []
        self.programs_gentoo = ["/usr/bin/postmaster", "/usr/bin/svn", "/usr/bin/ssh-keygen"]
        self.programs_ubuntu = ["/usr/bin/pg_ctlcluster", "/usr/bin/svn", "/usr/bin/ssh-keygen"]
        self.programs_debian = ["/usr/bin/pg_ctlcluster", "/usr/bin/svn", "/usr/bin/ssh-keygen"]
        
        self.program_installer_gentoo = [{"Postgres":["dev-db/postgresql-server", "app-admin/eselect-postgresql", "dev-db/postgresql-docs"]}, {"Subversion":["dev-vcs/subversion"]}, {"ssh":["virtual/ssh"]}]
        self.program_installer_ubuntu = [{"Postgres":["postgresql-9.1", "postgresql-client-9.1"]},{"Subversion":["subversion"]},  {"ssh":["ssh"]}]
        self.program_installer_debian = [{"Postgres":["postgresql-9.1", "postgresql-client-9.1"]},{"ssh":["ssh"]} ]
        self.program_installer = []
        self.programs_exist = []
        
        
        self.python_modules = ["PIL", "reportlab", "twisted.web", "twisted.words", "pygments", "webkit", "pg"]
        self.python_modules_exist = []
        self.python_installer = []
        self.python_installer_gentoo = [{"PIL":["dev-python/imaging"]}, {"reportlab":["dev-python/reportlab"]}, {"twisted.web":["dev-python/twisted-web"]}, {"twisted.words":[]}, {"pygments":[]}, {"webkit":["dev-python/pywebkitgtk"]},{"pg":[]} ]
        self.python_installer_ubuntu = [{"PIL":["python-imaging",  "python-imaging-sane"]},  {"reportlab":["python-reportlab"]} , {"twisted.web":["python-twisted-web" ]}, {"twisted.words":["python-twisted-words"]}, {"pygments":["python-pygments"]}, {"webkit":["python-webkit"]},{"pg":["python-pygresql"]} ]
        self.python_installer_debian = []
        self.OS_Installer = None
        self.OS = None
        self.Sudo = "" # or gksu
        self.Terminals = ["gnome-terminal", "konsole", "xfce4-terminal", "terminal", "xterm"]
        self.Terminal = None
        self.cpServer = ConfigParser.ConfigParser()
        self.CUON_FS = "/etc/cuon"
        self.dia = MessageDialogWindow()
        
    def checkOS(self):
        print "start check OS"
        if sys.platform.startswith('linux'):
            # Linux-specific code here...

            os_desc = os.uname()[1].upper()
        
            os_name = os.uname()[2].upper()
            os_machine = os.uname()[3].upper()
            os_type = os.uname()[4].upper()
            
            os_dist = platform.linux_distribution()[0].upper()
            

            print os_dist,  os_name,  os_machine,  os_type
            if os_dist.find("GENTOO")>= 0: 
                if os.path.exists("/usr/bin/emerge"):
                    self.OS = "GENTOO"
                    self.program_installer = self.program_installer_gentoo
                    self.python_installer = self.python_installer_gentoo
                    self.programs = self.programs_gentoo
                    print 'Check1',  self.programs ,  self.programs_gentoo
                    self.OS_Installer = "/usr/bin/emerge "
            elif os_dist.find("UBUNTU")>= 0: 
                if os.path.exists("/usr/bin/apt-get"):
                    self.OS = "UBUNTU"
                    self.program_installer = self.program_installer_ubuntu
                    self.python_installer = self.python_installer_ubuntu
                    self.programs = self.programs_ubuntu
                    self.OS_Installer = "/usr/bin/apt-get install "
            elif os_dist.find("DEBIAN")>= 0: 
                if os.path.exists("/usr/bin/apt-get"):
                    self.OS = "DEBIAN"        
                    self.program_installer = self.program_installer_debian
                    self.python_installer = self.python_installer_debian
                    self.programs = self.programs_debian
                    self.OS_Installer = "/usr/bin/apt-get install "
            
            
            
            print "OS = ",  self.OS
            for j in self.Terminals:
                if os.path.exists("/usr/bin/" + j):
                    self.Terminal = "/usr/bin/" + j
                    print "Terminal = " + self.Terminal
                    break
                    
                    
            
    def checkEnvironment(self):
        self.programs_exist = []
        self.python_modules_exist = []
        print 'programs = ',  self.programs
        for program in self.programs:
            print program
            if os.path.exists(program):
                self.programs_exist.append(True)
            else:
                self.programs_exist.append(False)
                
                

        print 'Exist 8',  self.programs,  self.programs_exist
 
        for python_module in self.python_modules:
            try:
                print python_module
                if python_module == "webkit":
                    if os.path.exists("/usr/lib/python2.7/site-packages/webkit/webkit.so"):
                        self.python_modules_exist.append(True)
                    elif os.path.exists("/usr/lib/python2.7/dist-packages/webkit/webkit.so"):
                        self.python_modules_exist.append(True)
                    else:
                        self.python_modules_exist.append(False)
                else:
                        
                    if __import__(python_module):
                        self.python_modules_exist.append(True)
            except ImportError:
                self.python_modules_exist.append(False)
            except:
                self.python_modules_exist.append(False)

        print 'Exist 9',  self.python_modules,  self.python_modules_exist
        
    def on_button_clicked(self, widget):
        print "Hello World"
        self.dia.wrong_requirement()
        
    def start(self,  again=False):
        print 'again',  again
        self.checkOS()
        self.checkEnvironment()
        if not again:
           
            self.win = Gtk.Window()
            self.win.connect("delete-event", Gtk.main_quit)
       
            self.button = Gtk.Button(label="Next")
            self.button.connect("clicked", self.on_check_missing)
        if again:
            self.win.remove(self.grid)
            
        self.grid = Gtk.Table(10, 4, True)
        z= 0
        print self.programs_exist
        for name in self.program_names:
            print z, self.programs_exist[z]
            self.grid.attach(Gtk.Label(name), 0, 1, z, z+1)
            self.grid.attach(Gtk.Label(`self.programs_exist[z]`), 1, 2, z,z+1)
            z += 1
            
        z = 0
        for pName in self.python_modules:
            l1 = Gtk.Label(pName)
            l1.set_justify(Gtk.Justification.LEFT)
            self.grid.attach(l1, 3, 4, z,z+1, 0, 0.5, 0, 0.5)
            self.grid.attach(Gtk.Label(`self.python_modules_exist[z]`), 4, 5,z,  z+1, 0, 0.5, 0, 0.5)
            z += 1


        if not again:
            self.grid.attach(self.button, 4, 5, 9 , 10)
            
        self.win.add(self.grid)

        self.win.show_all()
        self.dia.warn(self.user)
        if self.dia.Q2 == False:
            sys.exit(0)
            
            
        if not again:
            Gtk.main()
       
        
    def on_check_missing(self, widget, again=False):
        if again:
            self.start(again)
        if not self.Terminal:
           self.dia.AbortInfo1() 
           sys.exit(0)
           
        if False in self.python_modules_exist or False in self.programs_exist:
            if again:
                self.dia.error1()
                sys.exit(0)
            self.dia.wrong_requirement()
            print 'q1',  self.dia.Q1
           
                
            if self.dia.Q1:
                self.try_install_missing_programs()
        else:
            # All is ok, next step ssh
            self.configure_ssh()
            

    def try_install_missing_programs(self):
        s = ""
        for i in range(len(self.programs_exist)):
            if not self.programs_exist[i]:
                print self.programs_exist
                print self.program_installer
                print self.program_names
                print self.program_names[i]
                for j in self.program_installer[i][self.program_names[i]]:
                    s += j + " "
        if s:            
            s = self.Terminal +  ' -e "' + self.Sudo + " " + self.OS_Installer +" " + s  +'"' 
            print s
            #shellcommand = shlex.split('"' + s + '"')
            liStatus = subprocess.call(args = s,  shell = True)
        
        s = ""
        for i in range(len(self.python_modules_exist)):
            if not self.python_modules_exist[i]:
                try:
                    print i, self.python_modules[i], self.python_installer[i]
                
                    print self.python_installer[i][self.python_modules[i]]
                    for j in self.python_installer[i][self.python_modules[i]]:
                        s += j + " "
                except:
                    pass
        
        if s:
            s = self.Terminal + ' -e "' + self.Sudo + " " +self.OS_Installer +' ' + s +'"'             
            print "start Terminal with " + s 
            #shellcommand = shlex.split(s )
            #print shellcommand
            liStatus = subprocess.call(args=s,  shell=True  )
            print liStatus
            
        self.checkEnvironment()
        self.on_check_missing(None, again=True)
       
        
    def configure_ssh(self):
        self.dia.sshInfo1()
        print 'open ssh terminal 1'
        # generate key
        s =  self.Terminal + ' -e "' +  " /usr/bin/ssh-keygen -t rsa -f /root/cuon_server_key" + '"'
        liStatus = subprocess.call(args=s,  shell=True  )
        print 'ok,  done',  s
        #copy to user .ssh as id_rsa
        s = self.Terminal + ' -e "' + self.Sudo +" mkdir  /home/" + self.user + "/.ssh ; mv /root/cuon_server_key /home/" + self.user + "/.ssh/id_rsa ; mv /root/cuon_server_key.pub  /home/" + self.user + "/.ssh/id_rsa.pub ; chown " + self.user +  ":" + self.user + " /home/" + self.user + "/.ssh/id_rsa* ; /mkdir /root/.ssh " + '"'
        
        print s
        liStatus = subprocess.call(args=s,  shell=True  )
        
        # insert them to the authorized_keys
        s = self.Terminal + ' -e "' + self.Sudo +" cat /home/" + self.user + "/.ssh/cuon_server_key.pub >> /root/.ssh/authorized_keys " + '"'
        print s
        liStatus = subprocess.call(args=s,  shell=True  )
        
        # next Step postgres
        self.configure_postgres()
        
    def configure_postgres(self):
        # insert line at pg_hba.conf
        # at a line to the pg_hba.conf
        # check different locations, very bad (
        h = None
        z=0
        for j in ["/etc/postgresql/9.1/main/pg_hba.conf", "/etc/postgresql-9.1/pg_hba.conf",  "/etc/postgresql-9.1/pg_hba.conf", "/etc/postgresql-9.0/pg_hba.conf"]:
        
            if os.path.exists(j):
                h= j
                 
                break
            z+= 1

                  
        if h:
            f = open(h, 'r')
            s = f.readline()
            newConf = ""
            while s:
                #print s
                #print s[0:5]
                if s[0:5]== "local":
                    print "local found",  s, s.find("postgres")  
                    
                    if s.find("postgres") > 0: 
                        print "postgres in local found"
                        s = s.replace("peer", "trust")
                        print "replaced = ",  s
                newConf += s
                s = f.readline()
            #print newConf
            f.close()
            f = open(h, 'w')
            f.write(newConf)
            f.close()
            
            f = open(h, 'a')
                            
            f.write("# NEW generated Line for the cuon Database \nlocal   cuon             all                                     trust\n")
            f.close()
            
            #s = self.Terminal + ' -e ' + self.Sudo + ' echo "# NEW generated Line for the cuon Database \nlocal   cuon             all                                     trust \n"  >> ' + h
            #print s
            #iStatus = subprocess.call(args=s,  shell=True  )
            s = None
            if self.OS in ["DEBIAN", "UBUNTU"]:
                #s = self.Terminal + ' -e ' + "/etc/init.d/postgresql restart "
                s =  self.Terminal + ' -e ' + '"/etc/init.d/postgresql restart"'
            if self.OS == "GENTOO":
                if z == 2:
                    s = self.Terminal + ' -e ' + '"/etc/init.d/postgresql-9.1 restart" '
                elif z == 3:
                    s = self.Terminal + ' -e ' + '"/etc/init.d/postgresql-9.0 restart " '
            if s:
                print s
                iStatus = subprocess.call(args=s,  shell=True  )
        else:
            sys.exit(0)
         
        #ok, create database and user
        #set the path
        pa = ""
        for j in ["/usr/lib/postgresql-9.1/bin", "/usr/lib/postgresql-9.0/bin","/usr/lib/postgresql/9.0/bin", "/usr/lib/postgresql/9.1/bin"   ]:
        
            if os.path.exists(j):
                pa = j + "/"
                break
            
        
        s =  self.Terminal + ' -e ' + pa + '"createdb -Upostgres -E utf-8 cuon" '
        print "create database = " ,  s
        liStatus = subprocess.call(args=s,  shell=True  )
        s = self.Terminal + ' -e ' + pa + '"createlang -Upostgres -d cuon plpgsql"'
        liStatus = subprocess.call(args=s,  shell=True  )
        s = self.Terminal + ' -e ' + pa + '"createuser -Upostgres -d -s cuon_admin"'
        liStatus = subprocess.call(args=s,  shell=True  )
        s = self.Terminal + ' -e ' + pa + '"createuser -Upostgres -D -S -R zope"'
        liStatus = subprocess.call(args=s,  shell=True  )
        
        self.configure_cuon()
        
    def configure_cuon(self):
        # version 83
        setupDir = "/home/" + self.user+ "/Projekte/"
        setupStartDir = setupDir + "cuon/cuon_client"
        try:
            sLocale =  locale.getdefaultlocale()[0].split("_")[0]
        except Exception,  params:
            print Exception,  params
            sLocale = "us"
        
        if not os.path.exists(setupDir):
           os.mkdir(setupDir)
        os.chdir(setupDir)
        s = self.Terminal + ' -e ' +  '"svn co -r 83 https://cuon.svn.sourceforge.net/svnroot/cuon cuon  "' 
        print s
        liStatus = subprocess.call(args=s,  shell=True  )
        print "get svn ",  liStatus
        # now write the setup.ini 
        os.chdir(setupStartDir)
        f = open("cuon_setup.ini", "w")
        s = "[local]\nxmlrpc_port = 7080\nprotocol = http\ndescription = Install on Localhost\nssh_port = 22\nip = 127.0.0.1\ndefault = True\nlocale = " + sLocale + "\ncuonadmin = cuon_admin"
        f.write(s)
        f.close()
        os.chdir(setupDir +"cuon/LGPL")
        s = self.Terminal + ' -e ' +  '"tar -xvzf iCalendar-0.11.tgz ; cd iCalendar ; python setup.py install"' 
        print s
        liStatus = subprocess.call(args=s,  shell=True  )
        
        dirList=os.listdir(setupDir)
        os.chown(setupDir, pwd.getpwnam(self.user).pw_gid ,grp.getgrnam( self.user).gr_gid ) 
        for fname in dirList:
            os.chown(fname, pwd.getpwnam(self.user).pw_gid ,grp.getgrnam( self.user).gr_gid ) 
       
       
        # now write install config files
        
        # Now write the config files
        server_ini = self.CUON_FS + "/server.ini"
        if not os.path.exists(server_ini):
            shutil.copy(setupDir +"cuon/cuon_server/examples/server.ini", self.CUON_FS )
            shutil.copy(setupDir +"cuon/cuon_server/examples/user_cfg", self.CUON_FS )
            shutil.copy(setupDir +"cuon/cuon_server/examples/clients.ini", self.CUON_FS )
            shutil.copy(setupDir +"cuon/cuon_server/examples/menus.ini", self.CUON_FS )
            
            
        try:
            self.cpServer,  f = self.getParser(self.CUON_FS + "/server.ini")
            
            
            
            # Instances 
            value = self.getConfigOption('INSTANCES','XMLRPC')
            if value:
                self.XMLRPC_INSTANCES = int(value)
        except:
            pass
            

class MessageDialogWindow(Gtk.Window):
   
        
        

    def __init__(self):
        self.Q1 = False
        self.Q2 = False
    def AbortInfo1(self):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, "A valid terminal is missing, we must abort, sorry.")
        dialog.format_secondary_text(
            "Install a Terminal Emulator like Gnome-Terminal, Konsole, terminal or similar")
        dialog.run()
        print "INFO dialog closed"

        dialog.destroy()
    def sshInfo1(self):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, "We install now a pgp key for root access to the cuon-server")
        dialog.format_secondary_text(
            "Please, press ONLY Enter in the terminal window !! No Passphrase is allowed !!")
        dialog.run()
        print "INFO dialog closed"

        dialog.destroy()
    def sshInfo2(self):
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
            Gtk.ButtonsType.OK, "We install now a pgp key at the authorized_keys file")
        dialog.format_secondary_text(
            "Perhaps you must enter a password for su or sudo.")
        dialog.run()
        print "INFO dialog closed"

        dialog.destroy()
    def error1(self):
        
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.ERROR,
            Gtk.ButtonsType.CANCEL, "There are again missing program files")
        dialog.format_secondary_text(
            "Sorry, you shall try manually install the missing files")
        dialog.run()
        print "ERROR dialog closed"

        dialog.destroy()

    def warn(self,  user ):
        self.Q2 = False
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.WARNING,
            Gtk.ButtonsType.OK_CANCEL, "WARNING Warning WARNING")
        dialog.format_secondary_text(
            "This setup install a cuon-server on this computer. To do this, the user " + user + " get lasting root access\n Please,  press cancel if you are not sure that you want this !!! PLEASE !!!!")
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            print "WARN dialog closed by clicking OK button"
            self.Q2 = True
        elif response == Gtk.ResponseType.CANCEL:
            print "WARN dialog closed by clicking CANCEL button"

        dialog.destroy()

    def wrong_requirement(self):
        self.Q1 = False
        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.QUESTION,
            Gtk.ButtonsType.YES_NO, "Some Programs or Python Module are missing!")
        dialog.format_secondary_text(
            "Shall I try to install them ?")
        response = dialog.run()
        if response == Gtk.ResponseType.YES:
            print "QUESTION dialog closed by clicking YES button"
            self.Q1 = True
        elif response == Gtk.ResponseType.NO:
            print "QUESTION dialog closed by clicking NO button"

        dialog.destroy()
#
#if [ -d $CalendarDir ]; then
#   echo "dir iCal ok"
#    cd $CalendarDir/iCalendar
#    sudo python ./setup.py install 
#    
#    ## create database
#    #sudo su postgres
#    #createdb -E utf-8 cuon 
#    #createlang -d cuon plpgsql
#    #echo "now creating the user "zope" with no Rights"
#    #createuser zope
#    #echo "and this is your cuonadmin user with superrights"
#    #createuser cuonadmin
#    
#else
#    echo " No Calendar found, something wrong! We stop it."
#fi
#
    def getConfigOption(self, section, option, configParser = None):
        value = None
        if configParser:
            cps = configParser
        else:
           cps = self.cpServer
           
        if cps.has_option(section,option):
            try:
                value = cps.get(section, option).strip()
                #print 'options = ',option,  value
            except:
                value = None
            #print 'getConfigOption', section + ', ' + option + ' = ' + value
        return value
    
      
        
    def getParser(self, sFile):
        cpParser = ConfigParser.ConfigParser()
        f = open(sFile)
        #print 'f1 = ', f
        cpParser.readfp(f)
        #print 'cpp', cpParser
        return cpParser, f
        
        
print sys.argv
if len(sys.argv) > 1:
    print sys.argv[1]
    
    t1 = cssi(user=sys.argv[1])
    t1.start()
