\contentsline {section}{Contents}{1}{section*.1}
\contentsline {section}{\numberline {1}Package cuon}{11}{section.1}
\contentsline {subsection}{\numberline {1.1}Modules}{11}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Variables}{12}{subsection.1.2}
\contentsline {section}{\numberline {2}Module cuon.AI}{13}{section.2}
\contentsline {subsection}{\numberline {2.1}Variables}{13}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Class AI}{13}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Methods}{13}{subsubsection.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2}Class Variables}{14}{subsubsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.3}Instance Variables}{14}{subsubsection.2.2.3}
\contentsline {section}{\numberline {3}Module cuon.Address}{15}{section.3}
\contentsline {subsection}{\numberline {3.1}Variables}{15}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Class Address}{15}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Methods}{15}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Class Variables}{17}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Instance Variables}{17}{subsubsection.3.2.3}
\contentsline {section}{\numberline {4}Module cuon.Article}{19}{section.4}
\contentsline {subsection}{\numberline {4.1}Variables}{19}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Class Article}{19}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Methods}{19}{subsubsection.4.2.1}
\contentsline {subsubsection}{\numberline {4.2.2}Class Variables}{20}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Instance Variables}{21}{subsubsection.4.2.3}
\contentsline {section}{\numberline {5}Module cuon.CalendarGen}{22}{section.5}
\contentsline {subsection}{\numberline {5.1}Functions}{22}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Variables}{22}{subsection.5.2}
\contentsline {section}{\numberline {6}Module cuon.CuonFuncs}{24}{section.6}
\contentsline {subsection}{\numberline {6.1}Variables}{24}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Class CuonFuncs}{24}{subsection.6.2}
\contentsline {subsubsection}{\numberline {6.2.1}Methods}{24}{subsubsection.6.2.1}
\contentsline {subsubsection}{\numberline {6.2.2}Class Variables}{24}{subsubsection.6.2.2}
\contentsline {subsubsection}{\numberline {6.2.3}Instance Variables}{24}{subsubsection.6.2.3}
\contentsline {section}{\numberline {7}Module cuon.DMS}{26}{section.7}
\contentsline {subsection}{\numberline {7.1}Variables}{26}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Class DMS}{26}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Methods}{26}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}Class Variables}{27}{subsubsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.3}Instance Variables}{27}{subsubsection.7.2.3}
\contentsline {section}{\numberline {8}Module cuon.Database}{28}{section.8}
\contentsline {subsection}{\numberline {8.1}Variables}{28}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Class Database}{28}{subsection.8.2}
\contentsline {subsubsection}{\numberline {8.2.1}Methods}{28}{subsubsection.8.2.1}
\contentsline {subsubsection}{\numberline {8.2.2}Class Variables}{31}{subsubsection.8.2.2}
\contentsline {subsubsection}{\numberline {8.2.3}Instance Variables}{31}{subsubsection.8.2.3}
\contentsline {section}{\numberline {9}Module cuon.Email}{32}{section.9}
\contentsline {subsection}{\numberline {9.1}Variables}{32}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Class cuonemail}{32}{subsection.9.2}
\contentsline {subsubsection}{\numberline {9.2.1}Methods}{32}{subsubsection.9.2.1}
\contentsline {subsubsection}{\numberline {9.2.2}Class Variables}{33}{subsubsection.9.2.2}
\contentsline {subsubsection}{\numberline {9.2.3}Instance Variables}{33}{subsubsection.9.2.3}
\contentsline {section}{\numberline {10}Module cuon.Email2}{34}{section.10}
\contentsline {subsection}{\numberline {10.1}Variables}{35}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}Class SimpleMail\_Exception}{35}{subsection.10.2}
\contentsline {subsubsection}{\numberline {10.2.1}Methods}{35}{subsubsection.10.2.1}
\contentsline {subsubsection}{\numberline {10.2.2}Properties}{36}{subsubsection.10.2.2}
\contentsline {subsection}{\numberline {10.3}Class NoFromAddress\_Exception}{36}{subsection.10.3}
\contentsline {subsubsection}{\numberline {10.3.1}Methods}{36}{subsubsection.10.3.1}
\contentsline {subsubsection}{\numberline {10.3.2}Properties}{36}{subsubsection.10.3.2}
\contentsline {subsection}{\numberline {10.4}Class NoToAddress\_Exception}{37}{subsection.10.4}
\contentsline {subsubsection}{\numberline {10.4.1}Methods}{37}{subsubsection.10.4.1}
\contentsline {subsubsection}{\numberline {10.4.2}Properties}{37}{subsubsection.10.4.2}
\contentsline {subsection}{\numberline {10.5}Class NoSubject\_Exception}{38}{subsection.10.5}
\contentsline {subsubsection}{\numberline {10.5.1}Methods}{38}{subsubsection.10.5.1}
\contentsline {subsubsection}{\numberline {10.5.2}Properties}{38}{subsubsection.10.5.2}
\contentsline {subsection}{\numberline {10.6}Class AttachmentNotFound\_Exception}{39}{subsection.10.6}
\contentsline {subsubsection}{\numberline {10.6.1}Methods}{39}{subsubsection.10.6.1}
\contentsline {subsubsection}{\numberline {10.6.2}Properties}{39}{subsubsection.10.6.2}
\contentsline {subsection}{\numberline {10.7}Class Attachments}{40}{subsection.10.7}
\contentsline {subsubsection}{\numberline {10.7.1}Methods}{40}{subsubsection.10.7.1}
\contentsline {subsubsection}{\numberline {10.7.2}Properties}{40}{subsubsection.10.7.2}
\contentsline {subsection}{\numberline {10.8}Class Recipients}{41}{subsection.10.8}
\contentsline {subsubsection}{\numberline {10.8.1}Methods}{41}{subsubsection.10.8.1}
\contentsline {subsubsection}{\numberline {10.8.2}Properties}{42}{subsubsection.10.8.2}
\contentsline {subsection}{\numberline {10.9}Class CCRecipients}{42}{subsection.10.9}
\contentsline {subsubsection}{\numberline {10.9.1}Methods}{42}{subsubsection.10.9.1}
\contentsline {subsubsection}{\numberline {10.9.2}Properties}{43}{subsubsection.10.9.2}
\contentsline {subsection}{\numberline {10.10}Class BCCRecipients}{43}{subsection.10.10}
\contentsline {subsubsection}{\numberline {10.10.1}Methods}{44}{subsubsection.10.10.1}
\contentsline {subsubsection}{\numberline {10.10.2}Properties}{44}{subsubsection.10.10.2}
\contentsline {subsection}{\numberline {10.11}Class Email}{45}{subsection.10.11}
\contentsline {subsubsection}{\numberline {10.11.1}Methods}{46}{subsubsection.10.11.1}
\contentsline {subsubsection}{\numberline {10.11.2}Properties}{47}{subsubsection.10.11.2}
\contentsline {section}{\numberline {11}Module cuon.Finances}{48}{section.11}
\contentsline {subsection}{\numberline {11.1}Variables}{48}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Class Finances}{48}{subsection.11.2}
\contentsline {subsubsection}{\numberline {11.2.1}Methods}{48}{subsubsection.11.2.1}
\contentsline {subsubsection}{\numberline {11.2.2}Class Variables}{50}{subsubsection.11.2.2}
\contentsline {subsubsection}{\numberline {11.2.3}Instance Variables}{50}{subsubsection.11.2.3}
\contentsline {section}{\numberline {12}Module cuon.Garden}{51}{section.12}
\contentsline {subsection}{\numberline {12.1}Variables}{51}{subsection.12.1}
\contentsline {subsection}{\numberline {12.2}Class Garden}{51}{subsection.12.2}
\contentsline {subsubsection}{\numberline {12.2.1}Methods}{51}{subsubsection.12.2.1}
\contentsline {subsubsection}{\numberline {12.2.2}Class Variables}{53}{subsubsection.12.2.2}
\contentsline {subsubsection}{\numberline {12.2.3}Instance Variables}{53}{subsubsection.12.2.3}
\contentsline {section}{\numberline {13}Module cuon.Grave}{54}{section.13}
\contentsline {subsection}{\numberline {13.1}Variables}{54}{subsection.13.1}
\contentsline {subsection}{\numberline {13.2}Class Grave}{54}{subsection.13.2}
\contentsline {subsubsection}{\numberline {13.2.1}Methods}{54}{subsubsection.13.2.1}
\contentsline {subsubsection}{\numberline {13.2.2}Class Variables}{55}{subsubsection.13.2.2}
\contentsline {subsubsection}{\numberline {13.2.3}Instance Variables}{55}{subsubsection.13.2.3}
\contentsline {section}{\numberline {14}Module cuon.Misc}{57}{section.14}
\contentsline {subsection}{\numberline {14.1}Variables}{57}{subsection.14.1}
\contentsline {subsection}{\numberline {14.2}Class Misc}{57}{subsection.14.2}
\contentsline {subsubsection}{\numberline {14.2.1}Methods}{57}{subsubsection.14.2.1}
\contentsline {subsubsection}{\numberline {14.2.2}Class Variables}{59}{subsubsection.14.2.2}
\contentsline {subsubsection}{\numberline {14.2.3}Instance Variables}{59}{subsubsection.14.2.3}
\contentsline {section}{\numberline {15}Module cuon.Order}{60}{section.15}
\contentsline {subsection}{\numberline {15.1}Variables}{60}{subsection.15.1}
\contentsline {subsection}{\numberline {15.2}Class Order}{60}{subsection.15.2}
\contentsline {subsubsection}{\numberline {15.2.1}Methods}{60}{subsubsection.15.2.1}
\contentsline {subsubsection}{\numberline {15.2.2}Class Variables}{63}{subsubsection.15.2.2}
\contentsline {subsubsection}{\numberline {15.2.3}Instance Variables}{63}{subsubsection.15.2.3}
\contentsline {section}{\numberline {16}Module cuon.Projects}{65}{section.16}
\contentsline {subsection}{\numberline {16.1}Variables}{65}{subsection.16.1}
\contentsline {subsection}{\numberline {16.2}Class Projects}{65}{subsection.16.2}
\contentsline {subsubsection}{\numberline {16.2.1}Methods}{65}{subsubsection.16.2.1}
\contentsline {subsubsection}{\numberline {16.2.2}Class Variables}{66}{subsubsection.16.2.2}
\contentsline {subsubsection}{\numberline {16.2.3}Instance Variables}{66}{subsubsection.16.2.3}
\contentsline {section}{\numberline {17}Module cuon.Report}{67}{section.17}
\contentsline {subsection}{\numberline {17.1}Variables}{67}{subsection.17.1}
\contentsline {subsection}{\numberline {17.2}Class Report}{67}{subsection.17.2}
\contentsline {subsubsection}{\numberline {17.2.1}Methods}{67}{subsubsection.17.2.1}
\contentsline {subsubsection}{\numberline {17.2.2}Class Variables}{69}{subsubsection.17.2.2}
\contentsline {subsubsection}{\numberline {17.2.3}Instance Variables}{69}{subsubsection.17.2.3}
\contentsline {section}{\numberline {18}Module cuon.ReportServer}{70}{section.18}
\contentsline {subsection}{\numberline {18.1}Variables}{70}{subsection.18.1}
\contentsline {subsection}{\numberline {18.2}Class ReportServer}{70}{subsection.18.2}
\contentsline {subsubsection}{\numberline {18.2.1}Methods}{70}{subsubsection.18.2.1}
\contentsline {subsubsection}{\numberline {18.2.2}Class Variables}{71}{subsubsection.18.2.2}
\contentsline {subsubsection}{\numberline {18.2.3}Instance Variables}{71}{subsubsection.18.2.3}
\contentsline {section}{\numberline {19}Package cuon.Reports}{72}{section.19}
\contentsline {subsection}{\numberline {19.1}Modules}{72}{subsection.19.1}
\contentsline {subsection}{\numberline {19.2}Variables}{72}{subsection.19.2}
\contentsline {section}{\numberline {20}Module cuon.Reports.MyXML}{73}{section.20}
\contentsline {subsection}{\numberline {20.1}Variables}{73}{subsection.20.1}
\contentsline {subsection}{\numberline {20.2}Class MyXML}{73}{subsection.20.2}
\contentsline {subsubsection}{\numberline {20.2.1}Methods}{73}{subsubsection.20.2.1}
\contentsline {section}{\numberline {21}Module cuon.Reports.report}{76}{section.21}
\contentsline {subsection}{\numberline {21.1}Variables}{76}{subsection.21.1}
\contentsline {subsection}{\numberline {21.2}Class report}{76}{subsection.21.2}
\contentsline {subsubsection}{\numberline {21.2.1}Methods}{76}{subsubsection.21.2.1}
\contentsline {subsubsection}{\numberline {21.2.2}Class Variables}{79}{subsubsection.21.2.2}
\contentsline {subsubsection}{\numberline {21.2.3}Instance Variables}{79}{subsubsection.21.2.3}
\contentsline {section}{\numberline {22}Module cuon.Reports.report2\_latex}{80}{section.22}
\contentsline {subsection}{\numberline {22.1}Variables}{80}{subsection.22.1}
\contentsline {subsection}{\numberline {22.2}Class report2}{80}{subsection.22.2}
\contentsline {subsubsection}{\numberline {22.2.1}Methods}{80}{subsubsection.22.2.1}
\contentsline {subsubsection}{\numberline {22.2.2}Class Variables}{83}{subsubsection.22.2.2}
\contentsline {subsubsection}{\numberline {22.2.3}Instance Variables}{84}{subsubsection.22.2.3}
\contentsline {section}{\numberline {23}Module cuon.Reports.report\_addresses\_phone1}{85}{section.23}
\contentsline {subsection}{\numberline {23.1}Variables}{85}{subsection.23.1}
\contentsline {subsection}{\numberline {23.2}Class report\_addresses\_phone1}{85}{subsection.23.2}
\contentsline {subsubsection}{\numberline {23.2.1}Methods}{85}{subsubsection.23.2.1}
\contentsline {section}{\numberline {24}Module cuon.Reports.report\_addresses\_phone11}{86}{section.24}
\contentsline {subsection}{\numberline {24.1}Variables}{86}{subsection.24.1}
\contentsline {subsection}{\numberline {24.2}Class report\_addresses\_phone11}{86}{subsection.24.2}
\contentsline {subsubsection}{\numberline {24.2.1}Methods}{86}{subsubsection.24.2.1}
\contentsline {section}{\numberline {25}Module cuon.Reports.report\_addresses\_phone12}{87}{section.25}
\contentsline {subsection}{\numberline {25.1}Variables}{87}{subsection.25.1}
\contentsline {subsection}{\numberline {25.2}Class report\_addresses\_phone12}{87}{subsection.25.2}
\contentsline {subsubsection}{\numberline {25.2.1}Methods}{87}{subsubsection.25.2.1}
\contentsline {section}{\numberline {26}Module cuon.Reports.report\_articles\_number1}{88}{section.26}
\contentsline {subsection}{\numberline {26.1}Variables}{88}{subsection.26.1}
\contentsline {subsection}{\numberline {26.2}Class report\_articles\_number1}{88}{subsection.26.2}
\contentsline {subsubsection}{\numberline {26.2.1}Methods}{88}{subsubsection.26.2.1}
\contentsline {section}{\numberline {27}Module cuon.Reports.report\_articles\_pickles\_standard}{89}{section.27}
\contentsline {subsection}{\numberline {27.1}Variables}{89}{subsection.27.1}
\contentsline {subsection}{\numberline {27.2}Class report\_articles\_pickles\_standard}{89}{subsection.27.2}
\contentsline {subsubsection}{\numberline {27.2.1}Methods}{89}{subsubsection.27.2.1}
\contentsline {section}{\numberline {28}Module cuon.Reports.report\_basics}{90}{section.28}
\contentsline {subsection}{\numberline {28.1}Variables}{90}{subsection.28.1}
\contentsline {subsection}{\numberline {28.2}Class report\_basics}{90}{subsection.28.2}
\contentsline {subsubsection}{\numberline {28.2.1}Methods}{90}{subsubsection.28.2.1}
\contentsline {section}{\numberline {29}Module cuon.Reports.report\_cab\_monthly}{91}{section.29}
\contentsline {subsection}{\numberline {29.1}Variables}{91}{subsection.29.1}
\contentsline {subsection}{\numberline {29.2}Class report\_cab\_monthly}{91}{subsection.29.2}
\contentsline {subsubsection}{\numberline {29.2.1}Methods}{91}{subsubsection.29.2.1}
\contentsline {section}{\numberline {30}Module cuon.Reports.report\_enquiry\_standard}{92}{section.30}
\contentsline {subsection}{\numberline {30.1}Variables}{92}{subsection.30.1}
\contentsline {subsection}{\numberline {30.2}Class report\_enquiry\_standard}{92}{subsection.30.2}
\contentsline {subsubsection}{\numberline {30.2.1}Methods}{92}{subsubsection.30.2.1}
\contentsline {section}{\numberline {31}Module cuon.Reports.report\_grave\_plant\_lists}{93}{section.31}
\contentsline {subsection}{\numberline {31.1}Variables}{93}{subsection.31.1}
\contentsline {subsection}{\numberline {31.2}Class report\_grave\_plant\_lists}{93}{subsection.31.2}
\contentsline {subsubsection}{\numberline {31.2.1}Methods}{93}{subsubsection.31.2.1}
\contentsline {section}{\numberline {32}Module cuon.Reports.report\_hibernation\_incoming\_document}{94}{section.32}
\contentsline {subsection}{\numberline {32.1}Variables}{94}{subsection.32.1}
\contentsline {subsection}{\numberline {32.2}Class report\_hibernation\_incoming\_document}{94}{subsection.32.2}
\contentsline {subsubsection}{\numberline {32.2.1}Methods}{94}{subsubsection.32.2.1}
\contentsline {section}{\numberline {33}Module cuon.Reports.report\_hibernation\_invoice}{95}{section.33}
\contentsline {subsection}{\numberline {33.1}Variables}{95}{subsection.33.1}
\contentsline {subsection}{\numberline {33.2}Class report\_hibernation\_invoice}{95}{subsection.33.2}
\contentsline {subsubsection}{\numberline {33.2.1}Methods}{95}{subsubsection.33.2.1}
\contentsline {section}{\numberline {34}Module cuon.Reports.report\_hibernation\_outgoing\_document}{96}{section.34}
\contentsline {subsection}{\numberline {34.1}Variables}{96}{subsection.34.1}
\contentsline {subsection}{\numberline {34.2}Class report\_hibernation\_outgoing\_document}{96}{subsection.34.2}
\contentsline {subsubsection}{\numberline {34.2.1}Methods}{96}{subsubsection.34.2.1}
\contentsline {section}{\numberline {35}Module cuon.Reports.report\_hibernation\_pickup\_document}{97}{section.35}
\contentsline {subsection}{\numberline {35.1}Variables}{97}{subsection.35.1}
\contentsline {subsection}{\numberline {35.2}Class report\_hibernation\_pickup\_document}{97}{subsection.35.2}
\contentsline {subsubsection}{\numberline {35.2.1}Methods}{97}{subsubsection.35.2.1}
\contentsline {section}{\numberline {36}Module cuon.Reports.report\_list\_list\_of\_invoices}{98}{section.36}
\contentsline {subsection}{\numberline {36.1}Variables}{98}{subsection.36.1}
\contentsline {subsection}{\numberline {36.2}Class report\_list\_list\_of\_invoices}{98}{subsection.36.2}
\contentsline {subsubsection}{\numberline {36.2.1}Methods}{98}{subsubsection.36.2.1}
\contentsline {section}{\numberline {37}Module cuon.Reports.report\_list\_of\_inpayment}{99}{section.37}
\contentsline {subsection}{\numberline {37.1}Variables}{99}{subsection.37.1}
\contentsline {subsection}{\numberline {37.2}Class report\_list\_of\_inpayment}{99}{subsection.37.2}
\contentsline {subsubsection}{\numberline {37.2.1}Methods}{99}{subsubsection.37.2.1}
\contentsline {section}{\numberline {38}Module cuon.Reports.report\_list\_of\_reminder}{100}{section.38}
\contentsline {subsection}{\numberline {38.1}Variables}{100}{subsection.38.1}
\contentsline {subsection}{\numberline {38.2}Class report\_list\_of\_reminder}{100}{subsection.38.2}
\contentsline {subsubsection}{\numberline {38.2.1}Methods}{100}{subsubsection.38.2.1}
\contentsline {section}{\numberline {39}Module cuon.Reports.report\_list\_of\_residue}{101}{section.39}
\contentsline {subsection}{\numberline {39.1}Variables}{101}{subsection.39.1}
\contentsline {subsection}{\numberline {39.2}Class report\_list\_of\_residue}{101}{subsection.39.2}
\contentsline {subsubsection}{\numberline {39.2.1}Methods}{101}{subsubsection.39.2.1}
\contentsline {section}{\numberline {40}Module cuon.Reports.report\_list\_of\_top}{102}{section.40}
\contentsline {subsection}{\numberline {40.1}Variables}{102}{subsection.40.1}
\contentsline {subsection}{\numberline {40.2}Class report\_list\_of\_top}{102}{subsection.40.2}
\contentsline {subsubsection}{\numberline {40.2.1}Methods}{102}{subsubsection.40.2.1}
\contentsline {section}{\numberline {41}Module cuon.Reports.report\_order\_list\_of\_invoices}{103}{section.41}
\contentsline {subsection}{\numberline {41.1}Variables}{103}{subsection.41.1}
\contentsline {subsection}{\numberline {41.2}Class report\_order\_list\_of\_invoices}{103}{subsection.41.2}
\contentsline {subsubsection}{\numberline {41.2.1}Methods}{103}{subsubsection.41.2.1}
\contentsline {section}{\numberline {42}Module cuon.Reports.report\_order\_standard\_invoice}{104}{section.42}
\contentsline {subsection}{\numberline {42.1}Variables}{104}{subsection.42.1}
\contentsline {subsection}{\numberline {42.2}Class report\_order\_standard\_invoice}{104}{subsection.42.2}
\contentsline {subsubsection}{\numberline {42.2.1}Methods}{104}{subsubsection.42.2.1}
\contentsline {section}{\numberline {43}Module cuon.Reports.report\_order\_standard\_supply}{105}{section.43}
\contentsline {subsection}{\numberline {43.1}Variables}{105}{subsection.43.1}
\contentsline {subsection}{\numberline {43.2}Class report\_order\_standard\_supply}{105}{subsection.43.2}
\contentsline {subsubsection}{\numberline {43.2.1}Methods}{105}{subsubsection.43.2.1}
\contentsline {section}{\numberline {44}Module cuon.Reports.report\_printAddress}{106}{section.44}
\contentsline {subsection}{\numberline {44.1}Variables}{106}{subsection.44.1}
\contentsline {subsection}{\numberline {44.2}Class report\_printAddress}{106}{subsection.44.2}
\contentsline {subsubsection}{\numberline {44.2.1}Methods}{106}{subsubsection.44.2.1}
\contentsline {section}{\numberline {45}Module cuon.Reports.report\_proposal\_standard}{107}{section.45}
\contentsline {subsection}{\numberline {45.1}Variables}{107}{subsection.45.1}
\contentsline {subsection}{\numberline {45.2}Class report\_proposal\_standard}{107}{subsection.45.2}
\contentsline {subsubsection}{\numberline {45.2.1}Methods}{107}{subsubsection.45.2.1}
\contentsline {section}{\numberline {46}Module cuon.Reports.report\_stockgoods\_number1}{108}{section.46}
\contentsline {subsection}{\numberline {46.1}Variables}{108}{subsection.46.1}
\contentsline {subsection}{\numberline {46.2}Class report\_stockgoods\_number1}{108}{subsection.46.2}
\contentsline {subsubsection}{\numberline {46.2.1}Methods}{108}{subsubsection.46.2.1}
\contentsline {section}{\numberline {47}Module cuon.Reports.standardlist}{109}{section.47}
\contentsline {subsection}{\numberline {47.1}Variables}{109}{subsection.47.1}
\contentsline {subsection}{\numberline {47.2}Class standardlist}{109}{subsection.47.2}
\contentsline {subsubsection}{\numberline {47.2.1}Methods}{109}{subsubsection.47.2.1}
\contentsline {subsubsection}{\numberline {47.2.2}Class Variables}{110}{subsubsection.47.2.2}
\contentsline {subsubsection}{\numberline {47.2.3}Instance Variables}{110}{subsubsection.47.2.3}
\contentsline {section}{\numberline {48}Module cuon.SQL}{111}{section.48}
\contentsline {subsection}{\numberline {48.1}Variables}{111}{subsection.48.1}
\contentsline {subsection}{\numberline {48.2}Class SQL}{111}{subsection.48.2}
\contentsline {subsubsection}{\numberline {48.2.1}Methods}{111}{subsubsection.48.2.1}
\contentsline {subsubsection}{\numberline {48.2.2}Class Variables}{112}{subsubsection.48.2.2}
\contentsline {subsubsection}{\numberline {48.2.3}Instance Variables}{112}{subsubsection.48.2.3}
\contentsline {section}{\numberline {49}Module cuon.SVG}{114}{section.49}
\contentsline {subsection}{\numberline {49.1}Functions}{114}{subsection.49.1}
\contentsline {subsection}{\numberline {49.2}Variables}{158}{subsection.49.2}
\contentsline {subsection}{\numberline {49.3}Class SVG}{162}{subsection.49.3}
\contentsline {subsubsection}{\numberline {49.3.1}Methods}{162}{subsubsection.49.3.1}
\contentsline {subsubsection}{\numberline {49.3.2}Class Variables}{163}{subsubsection.49.3.2}
\contentsline {subsubsection}{\numberline {49.3.3}Instance Variables}{163}{subsubsection.49.3.3}
\contentsline {subsection}{\numberline {49.4}Class newSVG}{163}{subsection.49.4}
\contentsline {subsubsection}{\numberline {49.4.1}Methods}{163}{subsubsection.49.4.1}
\contentsline {subsection}{\numberline {49.5}Class Line}{163}{subsection.49.5}
\contentsline {subsubsection}{\numberline {49.5.1}Methods}{163}{subsubsection.49.5.1}
\contentsline {subsection}{\numberline {49.6}Class Circle}{164}{subsection.49.6}
\contentsline {subsubsection}{\numberline {49.6.1}Methods}{164}{subsubsection.49.6.1}
\contentsline {subsection}{\numberline {49.7}Class Rectangle}{164}{subsection.49.7}
\contentsline {subsubsection}{\numberline {49.7.1}Methods}{164}{subsubsection.49.7.1}
\contentsline {subsection}{\numberline {49.8}Class Text}{164}{subsection.49.8}
\contentsline {subsubsection}{\numberline {49.8.1}Methods}{164}{subsubsection.49.8.1}
\contentsline {section}{\numberline {50}Package cuon.Slandy}{165}{section.50}
\contentsline {subsection}{\numberline {50.1}Modules}{165}{subsection.50.1}
\contentsline {subsection}{\numberline {50.2}Variables}{165}{subsection.50.2}
\contentsline {section}{\numberline {51}Module cuon.Slandy.GridUser}{166}{section.51}
\contentsline {subsection}{\numberline {51.1}Variables}{166}{subsection.51.1}
\contentsline {subsection}{\numberline {51.2}Class GridUser}{166}{subsection.51.2}
\contentsline {subsubsection}{\numberline {51.2.1}Methods}{166}{subsubsection.51.2.1}
\contentsline {subsubsection}{\numberline {51.2.2}Class Variables}{167}{subsubsection.51.2.2}
\contentsline {subsubsection}{\numberline {51.2.3}Instance Variables}{167}{subsubsection.51.2.3}
\contentsline {section}{\numberline {52}Module cuon.Slandy.slandy}{168}{section.52}
\contentsline {subsection}{\numberline {52.1}Variables}{168}{subsection.52.1}
\contentsline {subsection}{\numberline {52.2}Class UserServer}{168}{subsection.52.2}
\contentsline {subsubsection}{\numberline {52.2.1}Methods}{168}{subsubsection.52.2.1}
\contentsline {subsubsection}{\numberline {52.2.2}Class Variables}{168}{subsubsection.52.2.2}
\contentsline {subsubsection}{\numberline {52.2.3}Instance Variables}{169}{subsubsection.52.2.3}
\contentsline {section}{\numberline {53}Module cuon.Slandy.test\_slandy}{170}{section.53}
\contentsline {subsection}{\numberline {53.1}Variables}{170}{subsection.53.1}
\contentsline {section}{\numberline {54}Module cuon.Slandy.xmlValues}{171}{section.54}
\contentsline {subsection}{\numberline {54.1}Variables}{171}{subsection.54.1}
\contentsline {subsection}{\numberline {54.2}Class xmlValues}{171}{subsection.54.2}
\contentsline {subsubsection}{\numberline {54.2.1}Methods}{171}{subsubsection.54.2.1}
\contentsline {section}{\numberline {55}Module cuon.Stock}{172}{section.55}
\contentsline {subsection}{\numberline {55.1}Variables}{172}{subsection.55.1}
\contentsline {subsection}{\numberline {55.2}Class Stock}{172}{subsection.55.2}
\contentsline {subsubsection}{\numberline {55.2.1}Methods}{172}{subsubsection.55.2.1}
\contentsline {subsubsection}{\numberline {55.2.2}Class Variables}{173}{subsubsection.55.2.2}
\contentsline {subsubsection}{\numberline {55.2.3}Instance Variables}{173}{subsubsection.55.2.3}
\contentsline {section}{\numberline {56}Module cuon.Support}{174}{section.56}
\contentsline {subsection}{\numberline {56.1}Variables}{174}{subsection.56.1}
\contentsline {subsection}{\numberline {56.2}Class Support}{174}{subsection.56.2}
\contentsline {subsubsection}{\numberline {56.2.1}Methods}{174}{subsubsection.56.2.1}
\contentsline {subsubsection}{\numberline {56.2.2}Class Variables}{175}{subsubsection.56.2.2}
\contentsline {subsubsection}{\numberline {56.2.3}Instance Variables}{175}{subsubsection.56.2.3}
\contentsline {section}{\numberline {57}Module cuon.Tweet}{176}{section.57}
\contentsline {subsection}{\numberline {57.1}Variables}{176}{subsection.57.1}
\contentsline {subsection}{\numberline {57.2}Class Tweet}{176}{subsection.57.2}
\contentsline {subsubsection}{\numberline {57.2.1}Methods}{176}{subsubsection.57.2.1}
\contentsline {subsubsection}{\numberline {57.2.2}Class Variables}{177}{subsubsection.57.2.2}
\contentsline {subsubsection}{\numberline {57.2.3}Instance Variables}{177}{subsubsection.57.2.3}
\contentsline {section}{\numberline {58}Module cuon.User}{178}{section.58}
\contentsline {subsection}{\numberline {58.1}Variables}{178}{subsection.58.1}
\contentsline {subsection}{\numberline {58.2}Class User}{178}{subsection.58.2}
\contentsline {subsubsection}{\numberline {58.2.1}Methods}{178}{subsubsection.58.2.1}
\contentsline {subsubsection}{\numberline {58.2.2}Class Variables}{179}{subsubsection.58.2.2}
\contentsline {subsubsection}{\numberline {58.2.3}Instance Variables}{179}{subsubsection.58.2.3}
\contentsline {section}{\numberline {59}Module cuon.Web}{180}{section.59}
\contentsline {subsection}{\numberline {59.1}Variables}{180}{subsection.59.1}
\contentsline {subsection}{\numberline {59.2}Class Web}{180}{subsection.59.2}
\contentsline {subsubsection}{\numberline {59.2.1}Methods}{180}{subsubsection.59.2.1}
\contentsline {subsubsection}{\numberline {59.2.2}Class Variables}{181}{subsubsection.59.2.2}
\contentsline {subsubsection}{\numberline {59.2.3}Instance Variables}{181}{subsubsection.59.2.3}
\contentsline {section}{\numberline {60}Module cuon.Web2}{182}{section.60}
\contentsline {subsection}{\numberline {60.1}Variables}{182}{subsection.60.1}
\contentsline {subsection}{\numberline {60.2}Class Web2}{182}{subsection.60.2}
\contentsline {subsubsection}{\numberline {60.2.1}Methods}{182}{subsubsection.60.2.1}
\contentsline {subsubsection}{\numberline {60.2.2}Class Variables}{183}{subsubsection.60.2.2}
\contentsline {subsubsection}{\numberline {60.2.3}Instance Variables}{183}{subsubsection.60.2.3}
\contentsline {section}{\numberline {61}Module cuon.WebAI}{184}{section.61}
\contentsline {subsection}{\numberline {61.1}Variables}{184}{subsection.61.1}
\contentsline {subsection}{\numberline {61.2}Class WebAI}{184}{subsection.61.2}
\contentsline {subsubsection}{\numberline {61.2.1}Methods}{184}{subsubsection.61.2.1}
\contentsline {subsubsection}{\numberline {61.2.2}Class Variables}{185}{subsubsection.61.2.2}
\contentsline {subsubsection}{\numberline {61.2.3}Instance Variables}{185}{subsubsection.61.2.3}
\contentsline {section}{\numberline {62}Module cuon.WebShop}{186}{section.62}
\contentsline {subsection}{\numberline {62.1}Variables}{186}{subsection.62.1}
\contentsline {subsection}{\numberline {62.2}Class WebShop}{186}{subsection.62.2}
\contentsline {subsubsection}{\numberline {62.2.1}Methods}{186}{subsubsection.62.2.1}
\contentsline {subsubsection}{\numberline {62.2.2}Class Variables}{187}{subsubsection.62.2.2}
\contentsline {subsubsection}{\numberline {62.2.3}Instance Variables}{187}{subsubsection.62.2.3}
\contentsline {section}{\numberline {63}Module cuon.ai}{188}{section.63}
\contentsline {subsection}{\numberline {63.1}Variables}{188}{subsection.63.1}
\contentsline {subsection}{\numberline {63.2}Class ai}{188}{subsection.63.2}
\contentsline {subsubsection}{\numberline {63.2.1}Methods}{188}{subsubsection.63.2.1}
\contentsline {subsubsection}{\numberline {63.2.2}Class Variables}{188}{subsubsection.63.2.2}
\contentsline {subsubsection}{\numberline {63.2.3}Instance Variables}{189}{subsubsection.63.2.3}
\contentsline {section}{\numberline {64}Module cuon.ai\_main}{190}{section.64}
\contentsline {subsection}{\numberline {64.1}Variables}{190}{subsection.64.1}
\contentsline {subsection}{\numberline {64.2}Class ai\_main}{190}{subsection.64.2}
\contentsline {subsubsection}{\numberline {64.2.1}Methods}{190}{subsubsection.64.2.1}
\contentsline {section}{\numberline {65}Module cuon.basics}{191}{section.65}
\contentsline {subsection}{\numberline {65.1}Variables}{191}{subsection.65.1}
\contentsline {subsection}{\numberline {65.2}Class basics}{191}{subsection.65.2}
\contentsline {subsubsection}{\numberline {65.2.1}Methods}{191}{subsubsection.65.2.1}
\contentsline {subsubsection}{\numberline {65.2.2}Class Variables}{194}{subsubsection.65.2.2}
\contentsline {subsubsection}{\numberline {65.2.3}Instance Variables}{194}{subsubsection.65.2.3}
\contentsline {section}{\numberline {66}Module cuon.createWebAnalysis}{195}{section.66}
\contentsline {subsection}{\numberline {66.1}Variables}{195}{subsection.66.1}
\contentsline {section}{\numberline {67}Module cuon.gridxml}{196}{section.67}
\contentsline {subsection}{\numberline {67.1}Variables}{196}{subsection.67.1}
\contentsline {subsection}{\numberline {67.2}Class gridxml}{196}{subsection.67.2}
\contentsline {subsubsection}{\numberline {67.2.1}Methods}{196}{subsubsection.67.2.1}
\contentsline {section}{\numberline {68}Module cuon.iCal}{197}{section.68}
\contentsline {subsection}{\numberline {68.1}Variables}{197}{subsection.68.1}
\contentsline {subsection}{\numberline {68.2}Class iCal}{197}{subsection.68.2}
\contentsline {subsubsection}{\numberline {68.2.1}Methods}{197}{subsubsection.68.2.1}
\contentsline {subsubsection}{\numberline {68.2.2}Class Variables}{198}{subsubsection.68.2.2}
\contentsline {subsubsection}{\numberline {68.2.3}Instance Variables}{199}{subsubsection.68.2.3}
\contentsline {section}{\numberline {69}Module cuon.jabberBot}{200}{section.69}
\contentsline {subsection}{\numberline {69.1}Class jabberBot}{200}{subsection.69.1}
\contentsline {subsubsection}{\numberline {69.1.1}Methods}{200}{subsubsection.69.1.1}
\contentsline {subsubsection}{\numberline {69.1.2}Class Variables}{201}{subsubsection.69.1.2}
\contentsline {subsubsection}{\numberline {69.1.3}Instance Variables}{201}{subsubsection.69.1.3}
\contentsline {section}{\numberline {70}Module cuon.simplemail}{202}{section.70}
\contentsline {subsection}{\numberline {70.1}Variables}{203}{subsection.70.1}
\contentsline {subsection}{\numberline {70.2}Class SimpleMail\_Exception}{203}{subsection.70.2}
\contentsline {subsubsection}{\numberline {70.2.1}Methods}{203}{subsubsection.70.2.1}
\contentsline {subsubsection}{\numberline {70.2.2}Properties}{204}{subsubsection.70.2.2}
\contentsline {subsection}{\numberline {70.3}Class NoFromAddress\_Exception}{204}{subsection.70.3}
\contentsline {subsubsection}{\numberline {70.3.1}Methods}{204}{subsubsection.70.3.1}
\contentsline {subsubsection}{\numberline {70.3.2}Properties}{204}{subsubsection.70.3.2}
\contentsline {subsection}{\numberline {70.4}Class NoToAddress\_Exception}{205}{subsection.70.4}
\contentsline {subsubsection}{\numberline {70.4.1}Methods}{205}{subsubsection.70.4.1}
\contentsline {subsubsection}{\numberline {70.4.2}Properties}{205}{subsubsection.70.4.2}
\contentsline {subsection}{\numberline {70.5}Class NoSubject\_Exception}{206}{subsection.70.5}
\contentsline {subsubsection}{\numberline {70.5.1}Methods}{206}{subsubsection.70.5.1}
\contentsline {subsubsection}{\numberline {70.5.2}Properties}{206}{subsubsection.70.5.2}
\contentsline {subsection}{\numberline {70.6}Class AttachmentNotFound\_Exception}{207}{subsection.70.6}
\contentsline {subsubsection}{\numberline {70.6.1}Methods}{207}{subsubsection.70.6.1}
\contentsline {subsubsection}{\numberline {70.6.2}Properties}{207}{subsubsection.70.6.2}
\contentsline {subsection}{\numberline {70.7}Class Attachments}{208}{subsection.70.7}
\contentsline {subsubsection}{\numberline {70.7.1}Methods}{208}{subsubsection.70.7.1}
\contentsline {subsubsection}{\numberline {70.7.2}Properties}{208}{subsubsection.70.7.2}
\contentsline {subsection}{\numberline {70.8}Class Recipients}{209}{subsection.70.8}
\contentsline {subsubsection}{\numberline {70.8.1}Methods}{209}{subsubsection.70.8.1}
\contentsline {subsubsection}{\numberline {70.8.2}Properties}{210}{subsubsection.70.8.2}
\contentsline {subsection}{\numberline {70.9}Class CCRecipients}{210}{subsection.70.9}
\contentsline {subsubsection}{\numberline {70.9.1}Methods}{210}{subsubsection.70.9.1}
\contentsline {subsubsection}{\numberline {70.9.2}Properties}{210}{subsubsection.70.9.2}
\contentsline {subsection}{\numberline {70.10}Class BCCRecipients}{211}{subsection.70.10}
\contentsline {subsubsection}{\numberline {70.10.1}Methods}{211}{subsubsection.70.10.1}
\contentsline {subsubsection}{\numberline {70.10.2}Properties}{211}{subsubsection.70.10.2}
\contentsline {subsection}{\numberline {70.11}Class Email}{211}{subsection.70.11}
\contentsline {subsubsection}{\numberline {70.11.1}Methods}{212}{subsubsection.70.11.1}
\contentsline {subsubsection}{\numberline {70.11.2}Properties}{213}{subsubsection.70.11.2}
\contentsline {section}{\numberline {71}Module cuon.web\_root}{214}{section.71}
\contentsline {subsection}{\numberline {71.1}Class Image}{214}{subsection.71.1}
\contentsline {subsubsection}{\numberline {71.1.1}Methods}{214}{subsubsection.71.1.1}
\contentsline {subsection}{\numberline {71.2}Class WebRoots}{214}{subsection.71.2}
\contentsline {subsubsection}{\numberline {71.2.1}Methods}{214}{subsubsection.71.2.1}
\contentsline {section}{Index}{215}{section*.2}
