<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Cyrus-Computer GmbH freie Software, Warenwirtschaft unter LINUX</title>
<meta name="robots" content="index"> 
<meta name="description" content="CUON is a business application running under LINUX, with Linux and Windows Clients, Addresses, Articles, Project, Orderbook, Invoices, Document Management System">
<meta name="keywords" content="C.U.O.N., Support Ticket bug tracker">
</head>

<body>

<script src="menu_navigation.js" type="text/javascript"></script> 
<link rel="stylesheet" type="text/css" href="main_index.css">
%#template to generate a HTML table from a list of tuples (or list of lists, or tuple of tuples or ...)
<p>The open items are as follows:</p>
<table border="1">
%for row in rows:
  <tr>
  
      <td><a href="/show_tickets/{{row['id']}}">Jump To</a></td><td>{{row["id"]}}</td> <td>{{row["designation"]}}</td>
  </tr>
%end
</table>

</body>
</html>
