#!/usr/bin/python
# -*- coding: utf-8 -*-
##Copyright (C) [2010]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 



import bottle
from bottle import route, run,  request,  response,  template,static_file
from bottle import redirect, abort

from cuon.basics import basics
from cuon.Database import Database
from xmlrpclib import ServerProxy
from cuon.gridxml import  gridxml
import types,os,datetime,sys, commands
import pickle
import ConfigParser
from lxml import etree

basic = basics()
oDatabase = Database()


if basic.XMLRPC_ALLOW_HTTP:
    print basic.XMLRPC_PROTO + '://' +basic.XMLRPC_HOST + ':' +  `basic.XMLRPC_HTTP_PORT`
    
    sv= ServerProxy(basic.XMLRPC_PROTO + '://' +basic.XMLRPC_HOST + ':' +  `basic.XMLRPC_HTTP_PORT`,  allow_none = 1)
    print 'Allow Http for ', sv
else:
    print 'use HTTPS'
    sv= ServerProxy(basic.XMLRPC_PROTO + '://' +basic.XMLRPC_HOST + ':' +  `basic.XMLRPC_HTTPS_PORT`,  allow_none = 1)

web3user = basic.WEB_USER3
print web3user


rootDir = '/var/cuon_www/SupportTicket/'
listDir = "/var/cuon/Documents/Lists"
print bottle.TEMPLATE_PATH 
bottle.TEMPLATE_PATH = [rootDir +'views/']
bottle.TEMPLATES.clear()

# jasperstarter

pathToJasperstarter = "/usr/share/cuon/jasperstarter/bin/jasperstarter"
newPath ="/usr/share/cuon/cuon_server/src/cuon/Reports/"



# read all list configs /etc/cuon/x_lists.cfg
allFiles = os.listdir("/etc/cuon")
print allFiles
listFiles = []
user=None
session_id=None
client_id = None

cpServer = ConfigParser.ConfigParser()
cpServer.readfp(open(basic.CUON_FS + '/clients.ini'))

##############################################################################
#
#    Lists for jasper
#
##############################################################################

for i in allFiles:
    if(i.find("lists.cfg")> 0):
        listFiles.append(i)
print listFiles
dLists = {}
for aFile in listFiles:
    f = open("/etc/cuon/" + aFile,"r")
    s = f.readline()
   
    while s:
        liEntries = s.split(";")
        #listname;Username;Client;Type;parameter,parameter,...
        liUser = liEntries[1].split(",")
        iClient = liEntries[2].strip()
        sType =  liEntries[3].strip()
        sModul = liEntries[4].strip()
        sDescription =  liEntries[5].strip()
        liParams =  liEntries[6].split(",")
        liUser1 = []
        print liUser 
        for sUser in liUser:
            print sUser
            section ="CLIENT_" + iClient
            option = sUser
            value = basic.getConfigOption( section, option, cpServer)
            if value:
                liValue = value.split(",")
                liUser1.extend(liValue)
            else:
                liUser1.append(sUser)
                
        dLists[ liEntries[0].strip()] = {"User":liUser1,"Client":iClient,"Type":sType,"Modul":sModul,"Description":sDescription, "Params":liParams}
        s = f.readline()
    print "Liste = ", dLists


##############################################################################
#
#    bash commands
#
##############################################################################
    
bashFiles = []
for i in allFiles:
    print  i[-3:] 
    if(i.find("bash.cfg")> 0 and i[-3:] == "cfg"):
        bashFiles.append(i)
print bashFiles


dBash = {}
for aFile in bashFiles:
    f = open("/etc/cuon/" + aFile,"r")
    s = f.readline()
   
    while s:
        liEntries = s.split(";")
        print liEntries
        #bashorder;Username;Client;Type;parameter,parameter;execute_user
        liUser = liEntries[1].split(",")
        iClient = liEntries[2].strip()
        sType =  liEntries[3].strip()
        sModul = liEntries[4].strip()
        sDescription =  liEntries[5].strip()
        liParams =  liEntries[6].split(",")
        sExecuteUser =  liEntries[7].strip()
        sReturn =  liEntries[8].strip()
        for sUser in liUser:
            print sUser
            section ="CLIENT_" + iClient
            option = sUser
            value = basic.getConfigOption( section, option, cpServer)
            if value:
                liValue = value.split(",")
                liUser1.extend(liValue)
            else:
                liUser1.append(sUser)
        dBash[ liEntries[0].strip()] = {"User":liUser1,"Client":iClient,"Type":sType,"Modul":sModul,"Description":sDescription, "Params":liParams,"ExecUser":  sExecuteUser, "ReturnSuffix":sReturn}
        s = f.readline()
    print "Bash = ", dBash


##############################################################################
#
#    special files, GDPdU
#
##############################################################################


specialFiles = []
for i in allFiles:
    print  i[-3:] 
    if(i.find("special.cfg")> 0 and i[-3:] == "cfg"):
        specialFiles.append(i)
print specialFiles


dSpecial = {}
for aFile in specialFiles:
    f = open("/etc/cuon/" + aFile,"r")
    s = f.readline()
   
    while s:
        liEntries = s.split(";")
        print liEntries
        #specialorder;Username;Client;Type;Modul;Description,Params,Tables, DataSupplier, Media
        liUser = liEntries[1].split(",")
        iClient = liEntries[2].strip()
        sType =  liEntries[3].strip()
        sModul =  liEntries[4].strip()
        sSpecial =  liEntries[5].strip()
        sDescription =  liEntries[6].strip()
        liParams =  liEntries[7].split(",")
        liTables= liEntries[8].split(",")
        liDataSupplier = liEntries[9].split(",")
        sMedia =  liEntries[10].strip()
        
        for sUser in liUser:
            print sUser
            section ="CLIENT_" + iClient
            option = sUser
            value = basic.getConfigOption( section, option, cpServer)
            if value:
                liValue = value.split(",")
                liUser1.extend(liValue)
            else:
                liUser1.append(sUser)
        dSpecial[ liEntries[0].strip()] = {"User":liUser1,"Client":iClient,"Type":sType,"Modul":sModul, "Special":sSpecial,"Description":sDescription, "Params":liParams,"Tables":liTables,"Supplier":liDataSupplier, "Media":sMedia}
        s = f.readline()
    print "Special = ", dSpecial




##############################################################################
#
#    Login and authentication
#
##############################################################################

    
       
@route ("/login/")
def login():
        s = '''Login to the C.U.O.N.-Server: </br>
         <form action="/readLogin/" method="post">
            Username: <input name="username" type="text" />
            Password: <input name="password" type="password" />
            client: <input name="client" type="text" />

            <input value="Login" type="submit" />
        </form>
    '''
        return s

@route ("/readLogin/",  method = 'POST')
def readLogin():
    username = request.forms.get('username')
    password = request.forms.get('password')
    client_id  = request.forms.get('client')
    dicUser = getAuth(username,password,client_id)
    if dicUser["SessionID"] != "TEST":
        return getStartString()
    else:
        loginFailed()
        
    

def loginFailed():
    s=''' LOGIN FAILED'''
    return s


def getAuth (user =basic.WEB_USER3 , password = basic.WEB_PASSWORD3, client = basic.WEB_CLIENT_ID3):
    print 'get Auth for ', user, password, client
    #dicUser=sv.Support.getAuthorization(basic.WEB_USER3 ,  basic.WEB_PASSWORD3,  basic.WEB_CLIENT_ID3 )
    sid = oDatabase.xmlrpc_createSessionID(user, password)
    dicUser={'Name': user,'SessionID':sid,'userType':'cuon',  'client':int( client)}
    #print request.environ
    
    #env22  = request.environ['wsgi.input'].read(int(request.environ['CONTENT_LENGTH']))
    #print env22
    #args = gridxml().xmltodict(env22)   
    #print args
    print dicUser
    expire_date = datetime.datetime.now()
    expire_date = expire_date + datetime.timedelta(hours=8)
   
    response.set_cookie("cuon_Username",dicUser["Name"], expires=expire_date, path='/' )
    response.set_cookie("cuon_session_id",dicUser["SessionID"], expires=expire_date, path='/')
    response.set_cookie("cuon_client_id",`dicUser["client"]`, expires=expire_date, path='/')
    response.set_cookie("cuon_list_login","1", expires=expire_date, path='/')
    print "getAuth = ", dicUser
    # test
    print "req = "
    print "cookie",   request.get_cookie("cuon_Username")
    return dicUser
    
def setCookie():
      if request.get_cookie("cuon_list_login"):
          user =  request.get_cookie("cuon_Username")
          session_id = request.get_cookie("cuon_session_id")
          client_id =  int(request.get_cookie("cuon_client_id"))
          
          return " Nice to see " + user
      else:
          login()
      return 


##############################################################################
#
#    Lists for jasper
#
##############################################################################

@route("/Lists/")
def Lists():
    s = ''' <form action="/choosedList/" method="POST"></br>'''
    dicUser = getUser()
    counter = 0
    for key in dLists.keys():
        print dicUser["Name"] ,  dLists[key]["User"]
        if dicUser["Name"] in dLists[key]["User"]:

            s += key +  '''<input type="radio" name="radio" ''' + key + ''' " value=" ''' + key
            if counter == 0:
                s +=  '''\" checked=\"checked\">Yes>''';
            else:
                s +=  '''\" checked=\"checked\">No>''';
            s += '''   ''' +  dLists[key]["Description"] + ''' </br>'''

            counter += 1
       
            
            
    s += '''<input value="Choose" type="submit" />'''

    return s


@route("/choosedList/",  method = "POST")
def choosedList():
    s = ''' <form action="/setParameter2List/" method="POST"></br>'''
    dicUser = getUser()
    key = request.forms.get('radio').strip()
    expire_date = datetime.datetime.now()
    expire_date = expire_date + datetime.timedelta(hours=1)
   
    response.set_cookie("cuon_list_key",key, expires=expire_date, path='/' )
    print "sList = ", key
    
    if dicUser["Name"] in  dLists[key]["User"]:
        print dicUser["Name"] ,  dLists[key]["User"]

        
        if dLists[key].has_key("Params"):
            for param in  dLists[key]["Params"]:
                iVal = param.find("=")
                if(iVal > 0):
                    param = param[:iVal]
                s+= param + ": " + ''' <input name=''' + param.strip() + ''' type="text" /></br>'''
                    
    s += '''<input value="create List"  type="submit" />'''
    print s
    return s


@route("/setParameter2List/", method = "POST")
def   setParameter2List():
    shellCommand = "cd " + "%newPath%" + " ; " + pathToJasperstarter + " pr " +"%reportFile%" + " -t postgres  -u " + "%User%" +" -f pdf -H " +basic.POSTGRES_HOST +" --db-port "+ `basic.POSTGRES_PORT` + " -n cuon -o " + listDir + "/%pdffile%" +" -P "
    dicUser = getUser()
    key = request.get_cookie("cuon_list_key")
    listParams = " "
    print "request.forms = ", request.forms
    print  request.forms.get("lastname_from" )
    if dLists[key].has_key("Params"):
        for param in  dLists[key]["Params"]:
            iVal = param.find("=")
            if(iVal > 0):
                param1 = param[:iVal]
            try:
                p = request.forms.get(param1.strip() ).strip()
            except Exception,params:
                print Exception,params
                p = ""
            p=p.strip("\n").strip()
            if p:
                
                listParams += param1 + "=" + p +" "
            else:
                listParams += param + " "
                
        
    #shellCommand = "cd " + "%newPath%" + " ; " + pathToJasperstarter + " pr " +"%reportFile%" + " -t postgres  -u " + "%User%" +" -f pdf -H " +basic.POSTGRES_HOST +" --db-port "+ `basic.POSTGRES_PORT` + " -n cuon -o " + listDir +"/%pdffile%" +" -P "
    #cd /usr/share/cuon/cuon_server/src/cuon/Reports/XML ; /usr/share/cuon/jasperstarter/bin/jasperstarter pr /usr/share/cuon/cuon_server/src/cuon/Reports/XML/addresses_phonelist1.jasper -t postgres  -u jhamel -f pdf -H localhost --db-port 5432 -n cuon -o /var/cuon/Documents/Lists/Addresses/Adressenliste_1_-jhamel -P  lastname_from=a lastname_to=b firstname_from=NONE firstname_to=NONE city_from=NONE city_to=NONE country_from=NONE country_to=NONE info_contains=NONE newsletter_contains=NONE  CUON_TITLE="Address Phonelist1"

    print shellCommand
    #Type = Default, Client,User
    if dLists[key]["Type"] == "Default":
        shellCommand = shellCommand.replace("%newPath%",newPath +"XML")
    elif  dLists[key]["Type"] == "Client":
        shellCommand = shellCommand.replace("%newPath%",newPath + "client_" +`dicUser["client"]`)
    elif  dLists[key]["Type"] == "Client":
        shellCommand = shellCommand.replace("%newPath%",newPath + "client_" + dicUser["Name"])

    print dLists, key 

    shellCommand = shellCommand.replace("%User%", dicUser["Name"])
    pdffile =  basic.getNewUUID()
    
    shellCommand = shellCommand.replace("%pdffile%",pdffile)
     
    shellCommand = shellCommand.replace("%reportFile%",key + ".jasper")

    shellCommand += listParams

    print "shellcommand = ", shellCommand
    liStatus = os.system(shellCommand)
    
    #print "root-dir = ", rootDir
                                         
    return sendOtherFiles(pdffile+".pdf")
    
 

##############################################################################
#
#     Bash
#
##############################################################################


@route("/Bash/")
def Bash():
    s = ''' <form action="/choosedBash/" method="POST"></br>'''
    dicUser = getUser()
    counter = 0
    for key in dBash.keys():
        print dicUser["Name"] ,  dBash[key]["User"]
        if dicUser["Name"] in dBash[key]["User"]:

            s += key +  '''<input type="radio" name="bashradio" ''' + key + ''' " value=" ''' + key
            if counter == 0:
                s +=  '''\" checked=\"checked\">Yes>''';
            else:
                s +=  '''\" checked=\"checked\">No>''';
            s += '''   ''' +  dBash[key]["Description"] + ''' </br>'''

            counter += 1
       
            
            
    s += '''<input value="Choose" type="submit" />'''

    return s

@route("/choosedBash/",  method = "POST")
def choosedBash():
    s = ''' <form action="/setParameter2Bash/" method="POST"></br>'''
    dicUser = getUser()
    key = request.forms.get('bashradio').strip()
    expire_date = datetime.datetime.now()
    expire_date = expire_date + datetime.timedelta(hours=1)
   
    response.set_cookie("cuon_bash_key",key, expires=expire_date, path='/' )
    print "sBash = ", key
    
    if dicUser["Name"] in  dBash[key]["User"]:
        print dicUser["Name"] ,  dBash[key]["User"]

        
        if dBash[key].has_key("Params"):
            for param in  dBash[key]["Params"]:
                iVal = param.find("=")
                if(iVal > 0):
                    param = param[:iVal]
                s+= param + ": " + ''' <input name=''' + param.strip() + ''' type="text" /></br>'''
                    
    s += '''<input value="create Bash"  type="submit" />'''
    print s
    return s


@route("/setParameter2Bash/", method = "POST")
def   setParameter2Bash():
    
    dicUser = getUser()
    key = request.get_cookie("cuon_bash_key")
    bashParams = " "
    shellCommand = ""
    print "request.forms = ", request.forms
    if  dBash[key]["ExecUser"] != "root":
        shellCommand = "su " +  dBash[key]["ExecUser"] + " ; "
    
    if dBash[key].has_key("Params"):
        for param in  dBash[key]["Params"]:
            iVal = param.find("=")
            if(iVal > 0):
                param1 = param[:iVal]
            try:
                p = request.forms.get(param1.strip() ).strip()
            except Exception,params:
                print Exception,params
                p = ""
            p=p.strip("\n").strip()
            if p:
                
                bashParams += " " + p +" "
            else:
                bashParams +=  " "
                
        



    shellCommand += key + " "
    print shellCommand
    #Type = Default, Client,User
    if dBash[key]["Type"] == "Default":
        shellCommand = shellCommand.replace("%newPath%",newPath +"XML")
    elif  dBash[key]["Type"] == "Client":
        shellCommand = shellCommand.replace("%newPath%",newPath + "client_" +`dicUser["client"]`)
    elif  dBash[key]["Type"] == "Client":
        shellCommand = shellCommand.replace("%newPath%",newPath + "client_" + dicUser["Name"])

    print dBash, key 

    shellCommand = shellCommand.replace("%User%", dicUser["Name"])
    outfile =  basic.getNewUUID()
    
    #shellCommand = shellCommand.replace("%pdffile%",pdffile)
     
    
    shellCommand += bashParams

    shellCommand += " > " +listDir + "/" + outfile + "." + dBash[key]["ReturnSuffix"]
    print "shellcommand = ", shellCommand
    liStatus = os.system(shellCommand)
    
    #print "root-dir = ", rootDir
                                         
    return sendOtherFiles(outfile + "."  + dBash[key]["ReturnSuffix"] ) 
    
                     


##############################################################################
#
#     Specialfiles:  GDPdU
#
##############################################################################


@route("/Special/")
def Special():
    s = ''' <form action="/choosedSpecial/" method="POST"></br>'''
    dicUser = getUser()
    counter = 0
    for key in dSpecial.keys():
        print dicUser["Name"] ,  dSpecial[key]["User"]
        if dicUser["Name"] in dSpecial[key]["User"]:

            s += key +  '''<input type="radio" name="specialradio" ''' + key + ''' " value=" ''' + key
            if counter == 0:
                s +=  '''\" checked=\"checked\">Yes>''';
            else:
                s +=  '''\" checked=\"checked\">No>''';
            s += '''   ''' +  dSpecial[key]["Description"] + ''' </br>'''

            counter += 1
       
            
            
    s += '''<input value="Choose" type="submit" />'''

    return s


@route("/choosedSpecial/",  method = "POST")
def choosedSpecial():
    s = ''' <form action="/setParameter2Special/" method="POST"></br>'''
    dicUser = getUser()
    key = request.forms.get('specialradio').strip()
    expire_date = datetime.datetime.now()
    expire_date = expire_date + datetime.timedelta(hours=1)
   
    response.set_cookie("cuon_special_key",key, expires=expire_date, path='/' )
    print "sSpecial = ", key
    
    if dicUser["Name"] in  dSpecial[key]["User"]:
        print dicUser["Name"] ,  dSpecial[key]["User"]

        
        if dSpecial[key].has_key("Params"):
            for param in  dSpecial[key]["Params"]:
                iVal = param.find("=")
                if(iVal > 0):
                    param = param[:iVal]
                s+= param + ": " + ''' <input name=''' + param.strip() + ''' type="text" /></br>'''
                    
    s += '''<input value="create Special"  type="submit" />'''
    print s
    return s


@route("/setParameter2Special/", method = "POST")
def   setParameter2Special():
    
    dicUser = getUser()
    key = request.get_cookie("cuon_special_key")
    specialParams = " "
    shellCommand = ""
    print "request.forms = ", request.forms
   
    
    if dSpecial[key].has_key("Params"):
        for param in  dSpecial[key]["Params"]:
            iVal = param.find("=")
            if(iVal > 0):
                param1 = param[:iVal]
            try:
                p = request.forms.get(param1.strip() ).strip()
            except Exception,params:
                print Exception,params
                p = ""
            p=p.strip("\n").strip()
            if p:
                
                specialParams += " " + p +" "
            else:
                specialParams +=  " "
                
        



    shellCommand += key + " "
    print shellCommand
    #Type = Default, Client,User
    if dSpecial[key]["Type"] == "Default":
        shellCommand = shellCommand.replace("%newPath%",newPath +"XML")
    elif  dSpecial[key]["Type"] == "Client":
        shellCommand = shellCommand.replace("%newPath%",newPath + "client_" +`dicUser["client"]`)
    elif  dSpecial[key]["Type"] == "Client":
        shellCommand = shellCommand.replace("%newPath%",newPath + "client_" + dicUser["Name"])

    print dSpecial, key
    print "special: ", dSpecial[key]["Special"]
    if dSpecial[key]["Special"] == "GDPdU":
        createGDPdU(key,specialParams)
        

  
    outfile =  basic.getNewUUID()
    
   
                                         
    #return sendOtherFiles(outfile + "."  + dSpecial[key]["ReturnSuffix"] ) 
    return
                     


def createGDPdU(key,specialParams):
    print "now create xml file"
    root = etree.Element('DataSet')
    #root.set()
    ver = etree.SubElement(root,"Version")
    ver.text = "1.0"
    DS =  etree.SubElement(root,"DataSupplier")
    DS_Name = etree.SubElement(DS,"Name")
    DS_Name.text = dSpecial[key]["Supplier"][0]
    DS_Location = etree.SubElement(DS,"Location")
    DS_Location.text = dSpecial[key]["Supplier"][1]
    DS_Comment = etree.SubElement(DS,"Comment")
    DS_Comment.text = dSpecial[key]["Supplier"][2]
    Media =  etree.SubElement(root,"Media")
    MName = etree.SubElement(Media,"Name")
    MName.text = dSpecial[key]["Media"]
    dicUser = getUser()
    for table in dSpecial[key]["Tables"]:
         Table =  etree.SubElement(Media,"Table")
         URL =  etree.SubElement(Table,"URL")
         URL.text = table
         try:
            cpServer = ConfigParser.ConfigParser()
            
            cpServer.readfp(open(basic.CUON_FS + '/clients.ini'))

            sTable = basic.getConfigOption("CLIENT_"+`dicUser["client"]` , table, cpServer)
            print sTable
            liColumns = sTable.strip().split(",")
            Name =  etree.SubElement(Table,"Name")
            Name.text = liColumns[0]
            Desc =  etree.SubElement(Table,"Description")
            Desc.text = liColumns[1]
            
            Validity =  etree.SubElement(Table,"Validity")
            Range =   etree.SubElement(Validity,"Range")
            liSpecialParams = specialParams.split(" ")
            print specialParams, liSpecialParams
            sFrom =  etree.SubElement(Range,"From")
            sFrom.text = liSpecialParams[2]
            sTo = etree.SubElement(Range,"To")
            sTo.text = liSpecialParams[4]
            Format =  etree.SubElement(Validity,"Format")
            Format.text = "YYYYMMDD"
            #<DecimalSymbol>.</DecimalSymbol>
            # <DigitGroupingSymbol>,</DigitGroupingSymbol>
            Dec =  etree.SubElement(Table,"DecimalSymbol")
            Dec.text = "."
            DGS =  etree.SubElement(Table,"DigitGroupingSymbol")
            DGS.text = ","
            VL = etree.SubElement(Table,"VariableLength")
            print len(liColumns) 
            for column  in range(len(liColumns) - 2):
                print column + 2
                print liColumns[column + 2]
                liColumn =  liColumns[column + 2].split(" ")
                print "column = ", liColumn, len(liColumn)
                
                if len(liColumn) == 3:
                    if liColumn[2].strip() == "primary_key":
                        PK = etree.SubElement(VL,"VariablePrimaryKey")
                        Name = etree.SubElement(PK,"Name")
                        Name.text = liColumn[0]
                        if liColumn[1] == "integer":
                            print "pk is integer"
                            nu = etree.SubElement(PK,"Numeric")
                elif len(liColumn) == 5:
                    print "Start FK"
                    if liColumn[2].strip()  == "foreign_key":     
                        VC =  etree.SubElement(VL,"VariableColumn")
                        FK = etree.SubElement(VC,"Name")
                        FK.text = liColumn[0]
                        print  liColumn[1]
                        if liColumn[1].strip() == "integer":
                            print "fk is integer"
                            nu = etree.SubElement(FK,"Numeric")
                    print "End FK"
                else:
                    VC =  etree.SubElement(VL,"VariableColumn")
                    NK = etree.SubElement(VC,"Name")
                    NK.text = liColumn[0]
                    liColumn[1] =  liColumn[1].strip()
                    print "column 0-1 = ", liColumn, liColumn[0], liColumn[1],  len(liColumn[1])
                    iLen =  len(liColumn[1])
                    if liColumn[1]  == "integer":
                        nu = etree.SubElement(NK,"Numeric")
                        
                    elif  iLen > 17 and liColumn[1][0:17] == "character_varying":
                       
                        nu = etree.SubElement(NK,"AlphaNumeric")
                    elif  liColumn[1]  == "date":
                       
                        nu = etree.SubElement(NK,"Date")
                        df = etree.SubElement(nu,"Format")
                        df.text = "YYYYMMDD"
                    elif  liColumn[1]  == "time_without_time_zone":
                      
                        nu = etree.SubElement(NK,"Time")
                        df = etree.SubElement(nu,"Format")
                        df.text = "HH:MM:SS"
                    elif  liColumn[1]  == " double_precision":
                      
                        nu = etree.SubElement(NK,"Numeric")
                         
                            
                                                
             
         except Exception,params:
            print "error at clients.ini"
            print Exception,params
            
    str = etree.tostring(root, xml_declaration=True ,encoding='UTF-8',  pretty_print=True, doctype='<!DOCTYPE DataSet SYSTEM "gdpdu-01-08-2002.dtd">')
    print str
    MainPath = "/var/cuon/Documents/Specials"
    if not os.path.exists(MainPath):
        os.makedirs(MainPath)
    print specialParams, liSpecialParams[6] 
    if not os.path.exists(MainPath + "/" + liSpecialParams[6] ):
        os.makedirs(MainPath+ "/" + liSpecialParams[6] )
    try:
        f = open (MainPath+ "/" + liSpecialParams[6] + "/index.xml", "wb")
        f.write(str)
        f.close()
        
    except Exception,params:
        print "error at writing xml"
        print Exception,params  
    
              
    
    return

        
##############################################################################
#
#     Headers and HTML Answer
#
##############################################################################

@route('/hello/', method = 'GET')
def hello():
    print "test the db"
    print sv
    print sv.Database.is_running()
    print request.environ
    return "Hello World! " + `sv.Database.is_running()`

@route('/index.html',  method = 'GET')
@route('/',  method = 'GET')
def index():
    bottle.TEMPLATES.clear()
  
    return getStartString()

def getStartString():
    dicUser = getUser()
    s=''' '''
    if dicUser.has_key("Name") and dicUser["Name"]:
        s += '''Loginuser = ''' + dicUser["Name"] + ''' </br>'''
        
    s += ''' <form action="/login/" method="get">
             <input value="Login" type="submit" />
        </form></br>'''
  
    s += '''<form action="/Lists/" method="get">
             <input value="Lists" type="submit" />
        </form>
        <form action="/Bash/" method="get">
             <input value="Bash" type="submit" />
        </form>
    <form action="/Special/" method="get">
             <input value="Special" type="submit" />
        </form>
    '''

    return s
    
def getUser():
    client_id = 0
    session_id = "TEST"
    dicUser = {}
    print "request = " , request
    user =  request.cookies.get("cuon_Username")
    print "User11 = ", user
    if user:
        try:
            session_id = request.cookies.get("cuon_session_id")
            print "sesion_id", session_id
            
            client_id =  int(request.cookies.get("cuon_client_id"))
            dicUser={'Name': user,'SessionID':session_id,'userType':'cuon',  'client':int( client_id)}
            print dicUser
            
                
            return dicUser

        except Exception, params:
            print Exception,params
            client_id = 0
            session_id = "TEST"
    else:
        dicUser = {}
        
    return dicUser  
  




    #setCookie() 




    
#    
#    result = sv.Support.getProjects(dicUser)
#    print result
    
#    output = template('index_example',   rows=result)

    
    
#    return output
    
   
##############################################################################
#
#     Tickets
#
##############################################################################
 
@route('/show_tickets/:id')
def showTicketsForProject(id):
    bottle.TEMPLATES.clear()
    dicUser = getAuth()
    print dicUser
    try:
        print 'try1 '
        response.set_cookie("SupportProjectID", id)
        print 'try2 '

    except Exception,  params:
        print Exception,  params
    
    
    result = sv.Support.getTickets(dicUser,  id )
    print result
    
    output = template('showTickets_example',   rows=result)

    
    
    return output
    
      
@route('/show_ticket_details/:id',  method = 'GET')
def showTicket_details(id):
    bottle.TEMPLATES.clear()
    dicUser = getAuth()
    print dicUser
    try:
        prID = request.get_cookie("SupportProjectID")
        print 'Cookie ID = ',  prID
    except:
        prID = 0
    
    result = sv.Support.getTicketDetails(dicUser,  id )
    print result
    
    output = template('showTicketDetails_example',   rows=result)

    
    
    return output
     
    
@route('/:name',  method = 'GET')
def sendOtherFiles(name):
    print "sendfile called " + name
    return static_file(name, root=listDir)




run(  port=basic.WEB_PORT3 , host=basic.WEB_HOST3, reloader=True) # This starts the HTTP server
