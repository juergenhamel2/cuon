#! /usr/bin/python
#xmlrpc-server
from twisted.web import xmlrpc, resource, static
from twisted.internet import defer
from twisted.internet import reactor
from twisted.web import server
import sys
import cuon.CuonFuncs
import cuon.SVG


baseSettings = cuon.basics.basics()


r = cuon.CuonFuncs.CuonFuncs()
oSVG = cuon.SVG.SVG()


r.putSubHandler('SVG', oSVG)


try:    
    port = int(sys.argv[1])
except:
    port = 0
print port
print baseSettings.CHART_PORT ,  port,  baseSettings.CHART_PORT + port
print sys.argv

reactor.listenTCP(baseSettings.CHART_PORT + port, server.Site(r))
reactor.run()
