# -*- coding: utf-8 -*-

##Copyright (C) [2003, 2004, 2005, 2006, 2007]  [Juergen Hamel, D-32584 Loehne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

import sys
import commands


class decodeutf8:
    def __init__(self):
        pass
        
    def start(self, filename):
        shellcommand = 'rm  ' + filename + '_NEW ' 
        liStatus = commands.getstatusoutput(shellcommand)
        print liStatus
        
        f_in = open(filename,'r')
        f_out = open(filename +'_NEW','a')
        
        s = f_in.readline()
        while s:
            try:
                s2 = s.decode('latin-1').encode('utf-8')
            except Exception, param:
                print Exception, param
                s2 = ''
            f_out.write(s2)
            s = f_in.readline()
        
        f_in.close()
        f_out.close()
        
        shellcommand = 'cp  ' + filename + ' ' + filename +'_OLD'
        liStatus = commands.getstatusoutput(shellcommand)
        print liStatus
        shellcommand = 'cp  ' + filename + '_NEW ' + filename 
        liStatus = commands.getstatusoutput(shellcommand)
        print liStatus
        
            
            
du = decodeutf8()
du.start(sys.argv[1])

                
    