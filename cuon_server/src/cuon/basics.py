# coding=utf-8
##Copyright (C) [2003]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

import time,  glob,  os 
import xmlrpclib
from twisted.web import xmlrpc
from twisted.internet import defer
from twisted.internet import reactor
from twisted.web import server
import time,  re
import random
import sys,os  
import shelve,  pickle
import ConfigParser
import bz2, base64
import types
import uuid, ast 

class basics(xmlrpc.XMLRPC):
    def __init__(self):
        self.Android = {}
        self.Android['Smartphone']={}
        self.Android['Smartphone']['cuonAddress']={'Major':12, 'Minor':1,  'Rev':26}
        
        self.debug = 0
        self.DEBUG = False
        self.DEBUG_MODUS = 1
        self.MN = {}
         
        self.MN['Address'] = 2000
        self.MN['Address_info'] = 2001
        self.MN['Address_stat_caller'] = 2002
        self.MN['Address_stat_rep'] = 2003
        self.MN['Address_stat_salesman'] = 2004
        self.MN['Address_stat_schedul'] = 2005
        
        self.MN['Partner'] = 2100
        self.MN['Partner_info'] = 2101
        
        self.MN['Partner_Schedul'] = 2200
        self.MN['Partner_Schedul_info'] = 2201

        self.MN['Articles'] = 3000
        self.MN['Articles_stat_misc1'] = 3001
        
        self.MN['MaterialGroups'] = 23000
        
        self.MN['DMS'] = 11000
        self.MN['Forms_Address_Notes_Misc'] = 11010
        self.MN['Forms_Address_Notes_Contacter'] = 11011
        self.MN['Forms_Address_Notes_Rep'] = 11012
        self.MN['Forms_Address_Notes_Salesman'] = 11013
         
        self.MN['Project'] = 14000
        self.MN['Project_info'] = 14001
        self.MN['Project_stat_misc1'] = 14011
        self.MN['Project_phase'] = 14100
        self.MN['Project_task'] = 14200
        self.MN['Project_staff_resources'] = 14300
        self.MN['Project_material_resources'] = 14400
        self.MN['Project_programming'] = 14500
        
        self.MN["Sourcecode_project"] = 15000;
        self.MN["Sourcecode_part"] = 15100;
        self.MN["Sourcecode_modul"] = 15200;
        self.MN["Sourcecode_file"] = 15300;
        
        self.MN['SupportTicket']  = 17000


        self.MN['Web2'] = 20000
        self.MN['Stats'] = 22000
        self.MN['Calendar'] = 28000
        self.MN['Think'] = 29000
        
        self.MN['Leasing'] = 30001
        
        self.MN['Grave'] = 40000
        self.MN['GraveServiceNotesService'] = 40100
        self.MN['GraveServiceNotesSpring'] = 40101
        self.MN['GraveServiceNotesSummer'] = 40102
        self.MN['GraveServiceNotesAutumn'] = 40103
        self.MN['GraveServiceNotesWinter'] = 40104
        self.MN['GraveServiceNotesAnnual'] = 40105
        self.MN['GraveServiceNotesUnique'] = 40106
        self.MN['GraveServiceNotesHolliday'] = 40107
        
        self.MN['Graveyard'] = 41000
        self.MN['Garden'] = 42000
        
        self.MN['Hibernation'] = 110000  
        self.MN['HibernationPlantNumber'] = 110100
         
         
        self.MN['Botany'] = 110500
        self.UserType = {}
        self.UserType['OrderNormal'] = 1000
        self.UserType['OrderCash1'] = 1001
        self.UserType['OrderCash2'] = 1002
        self.UserType['OrderCash3'] = 1003
        
         
        self.MimeType = {}
        
        self.MimeType['image/gif'] =  ['gif']
        self.MimeType['text/plain'] = [ 'txt']
        self.MimeType['text/html'] =  ['html']
        self.MimeType['image/jpeg'] =  ['jpg']
        self.MimeType['application/pdf'] =  ['pdf']
        
        self.MimeType['application/msword'] =  ['doc']
        self.MimeType['application/vnd.oasis.opendocument.text'] =  ['odt']
        self.MimeType['application/vnd.oasis.opendocument.spreadsheet'] = [ 'ods']
        self.MimeType['application/vnd.oasis.opendocument.graphics'] = [ 'odg']
        self.MimeType['application/vnd.oasis.opendocument.presentation'] = [ 'odp']
        self.MimeType['application/x-bzip'] =  ['bz']
        self.MimeType['application/x-bzip2'] = [ 'bz2']
        self.MimeType['application/x-compressed'] =  ['zip']
        self.MimeType['application/x-zip-compressed'] =  ['zip']
        self.MimeType['application/zip'] = [ 'zip']
        self.MimeType['multipart/x-zip'] =  ['zip']
        self.MimeType['application/excel'] =  ['xls']
        self.MimeType['application/vnd.ms-excel'] =  ['xls']
        self.MimeType['application/x-excel'] = [ 'xls']
        self.MimeType['application/x-msexcel'] =  ['xls']
        self.MimeType['image/png'] =  ['png']
        self.MimeType['image/tiff'] =  ['tiff']
        self.MimeType['image/bmp'] =  ['bmp']
        self.MimeType['image/x-windows-bmp'] =  ['bmp']
        self.MimeType['text/x-python'] =  ['py']
        self.MimeType['application/x-php'] =  ['php']
        self.MimeType['text/x-ini'] = ['ini', 'cfg']
        self.MimeType['application/xml'] = ['xml']
        self.MimeType['text/css'] = ['css']
        self.MimeType['text/x-c++src'] = ['cpp', 'hpp']
        self.MimeType['text/x-sql'] = ['sql']
        self.MimeType['text/x-lua'] = ['lua']
        self.MimeType['text/x-java'] = ['java']
        self.MimeType['text/x-makefile'] = ['make']
        self.MimeType['application/x-perl'] = ['pl']
        self.MimeType['text/x-csrc'] = ['c', 'h']


        self.FileTypes = {}
        self.FileTypes['Office'] = ['odt','doc','odg','ods', 'odp', 'xls']
        self.FileTypes['Image'] = ['png','jpg','jpeg','bmp', 'tiff']
        self.FileTypes['ebook'] = ['epub','mobi']
        self.FileTypes['email'] = ['mail','eml']

        
        self.cCut1 = unichr(172)
        self.cCut2 = unichr(175)
        self.cCut3 = unichr(184)
        
        self.numLocalesComa = ['DE', 'NL', 'AT', 'BE', 'IT', 'DK', 'FI', 'EE', 'FR', 'GR', 'FL', 'BG', 'NO', 'SE', 'CH', 'PL', 'PL', 'RO', 'PE', 'ES', 'TR', 'YV']
        self.SSL_OFFSET = 500
        
        self.CUON_FS = '/etc/cuon'  
        liClients = []
        self.liClientsOptionsPrefs = ["CurrencyName", "CurrencySign", "CurrencyPrecision"]
        self.liClientsOptionsMail = ["sendNotes0"]
        self.liClientsOptionsOrder = ["order_add_position_gift_bank", "proposal_number", "proposal_designation", "enquiry_number", "enquiry_designation", "orderbook_number","orderbook_designation", "project_number", "project_designation","AddressTreeFields","AddressTreeFieldsTypes","AddressTreeHeader","AddressSearchFields","EnquiryTreeFields","EnquiryTreeFieldsTypes","EnquiryTreeHeader","EnquirySearchFields","ProposalTreeFields","ProposalTreeFieldsTypes","ProposalTreeHeader","ProposalSearchFields","OrderTreeFields","OrderTreeFieldsTypes","OrderTreeHeader","OrderSearchFields","cash_desk_top","order_gift_bonus","order_percent_bonus","modul_order_default_customer_startCash1" ,"modul_order_default_customer_startCash2","modul_order_default_customer_startCash3" ]


        self.liClientsOptionsGrave = ["cbGraveService", "cbTypeOfGrave", "cbTypeOfPaying", "cbPercentsGrave", "cbGraveSpringPeriod",  "cbGraveAutumnPeriod",  "cbGraveWinterPeriod",  "cbGraveUniquePeriod", "cbGraveHollidayPeriod", "cbGraveYearlyPeriod", "cbGraveSorting", "cbGraveTypeOfTab", "user_grave_schedule_yearly"]

        self.liClientsOptionsGraveHeadlines = ["order_main_headline_articles_id",  "order_service_headline_articles_id",  "order_spring_headline_articles_id", "order_summer_headline_articles_id", "order_autumn_headline_articles_id", "order_winter_headline_articles_id",  "order_holliday_headline_articles_id",  "order_Yearly_headline_articles_id",  "order_Unique_headline_articles_id", "order_part_id", "order_grave_materialgroups_for_tax_vat_1","order_grave_materialgroups_new_tax_vat_id_1"  ]
        

        self.liClientsOptionsERPsend = [ "erp_print_newsletter_misc_higher_then","erp_email_newsletter_misc_higher_then"]
        self.liClientsOptionsERP1 = ["erp_orderbook_1","erp_orderbook_2","erp_orderbook_3","erp_orderbook_4","erp_orderbook_5","erp_proposal_1","erp_proposal_2","erp_proposal_3","erp_proposal_4","erp_proposal_5","erp_address_newer_then_1","erp_address_newer_then_2","erp_address_newer_then_3","erp_address_newer_then_4"]

        liClients.append(self.liClientsOptionsPrefs)
        liClients.append(self.liClientsOptionsMail)
        liClients.append( self.liClientsOptionsGrave)
        liClients.append( self.liClientsOptionsGraveHeadlines )
        liClients.append( self.liClientsOptionsOrder )
        liClients.append( self.liClientsOptionsERPsend)
        liClients.append( self.liClientsOptionsERP1)
        
        self.liClientsOptions = [item for sublist in liClients for item in sublist]        
        self.dicClientsOptions = {}


        # instances
        self.XMLRPC_INSTANCES = 1
        self.REPORT_INSTANCES = 1
        self.ADD_PORT_INSTANCES = 20
        self.INSTANCES_PASSWORD = 'Test'
        
       # xmlrpc settings
        self.XMLRPC_HTTP_PORT = 7080
        self.XMLRPC_HTTPS_PORT = 7580
        self.XMLRPC_HOST = 'localhost'
        self.XMLRPC_PROTO = "http"
        self.XMLRPC_ALLOW_HTTP = True


        self.WEBPATH = '/var/cuon_www/' 
        self.WEB_HOST = 'localhost'
        self.WEB_PORT = 7081
        self.ICALPATH = '/var/cuon_www/iCal/'
        
        self.WEB_HOST2 = 'localhost'
        self.WEB_PORT2 = 7084
        self.WEB_START2 = False
        
        self.WEB_HOST3 = 'localhost'
        self.WEB_PORT3 = 7085
        self.WEB_START3 = False
        self.WEB_USER3 = 'Test'
        self.WEB_PASSWORD3 = 'Test'
        self.WEB_CLIENT_ID3 = 1
        
        self.WEB_HOST4 = 'localhost'
        self.WEB_PORT4 = 7086
        self.WEB_START4 = False
        
        
        self.AI_PORT = 7082
        self.AI_HOST = '84.244.7.139'
        self.AI_SERVER = "http://84.244.7.139:" + `self.AI_PORT`
        
        self.SVG_PORT = 7087
        self.SVG_HOST = 'localhost'
        
        self.REPORT_PORT = 7083
        self.REPORT_HOST = 'localhost'
        self.REPORTPATH = "/usr/share/cuon/cuon_server/src/cuon/Reports/XML"

        self.DocumentPathOrderInvoice = '/var/cuon/Documents/Order/Invoice'
        self.DocumentPathOrderProposal = '/var/cuon/Documents/Order/Proposal'
        self.DocumentPathOrderEnquiry = '/var/cuon/Documents/Order/Enquiry'

        self.DocumentPathHibernationIncoming = '/var/cuon/Documents/Hibernation/Incoming'
        self.DocumentPathHibernationPickup = '/var/cuon/Documents/Hibernation/Pickup'
        
        
        self.DocumentPathListsAddresses = '/var/cuon/Documents/Lists/Addresses'
        self.DocumentPathListsArticles = '/var/cuon/Documents/Lists/Articles'
        self.DocumentPathTmp =  '/var/cuon/tmp'
        
        self.DocumentPathHibernationInvoice = '/var/cuon/Documents/Hibernation/Invoice'
        self.DocumentPathGravesInvoice = '/var/cuon/Documents/Graves/Invoice'
        self.DocumentPathGravesPlants = '/var/cuon/Documents/Graves/Plants'
        self.DocumentPathSVG = '/var/cuon/Documents/SVG'
        
        self.WIKI_PORT = 7084
        self.ONLINE_BOOK = 'http://84.244.7.139:7084/?action=xmlrpc2'
        
        self.POSTGRES_HOST = 'localhost'
        self.POSTGRES_PORT = 5432
        self.POSTGRES_DB = 'cuon'
        self.POSTGRES_USER = 'Test'
        
        
        self.OSC_HOST = 'localhost'
        self.OSC_PORT = 5432
        self.OSC_DB = 'cuon'
        self.OSC_USER = 'Test'
        self.liSQL_ERRORS =  ['NONE','ERROR']
        
        self.PdfEncoding = 'latin-2'
        
        self.EMAILSERVER = None
        self.EMAILPORT = '25'
        self.EMAILUSER = 'jhamel'
        self.EMAILPASSWORD = None
        self.EMAILENCODING = 'utf-8'
        self.EMAILLOGINCRYPT = ['No Encryption', 'Automaitic','Plain Text','Login', 'Cram-MD5','Digest-MD5'  ]
        

        self.CRONTAB = [[1,"self.perform_test,48,13,dow=[0,1,2,3,4,5,6]"] ]

        self.SESSION_TIME = 8
        self.CURRENCY_NAME = 'EUR'
        self.CURRENCY_SIGN = '€'
        self.CURRENCY_ROUND = 2

        self.DIC_USER = {}
        self.DIC_USER['SQLDateFormat'] = 'DD.MM.YYYY'
        self.DIC_USER['SQLTimeFormat'] = 'HH24:MI'
        self.DIC_USER['SQLDateTimeFormat'] = 'DD.MM.YYYY HH24:MI'
        self.DIC_USER['DateTimeFormatstring'] = '%d.%m.%Y %H:%M'
        self.DIC_USER['DateformatString'] = '%d.%m.%Y '
        self.DIC_USER['TimeformatString'] = '%H:%M'
        
        self.JABBERSERVER=None
        self.JABBERUSERNAME=None
        self.JABBERPASSWORD=None
        
        self.AUTOMATIC_SCHEDUL = False
        
        self.IMAP_HOST = 'localhost'
        self.IMAP_PORT = 143
        self.IMAP_USERNAME = 'test'
        self.IMAP_PASSWORD = 'test'
        
        self.SEND_VERSION_INFO = 'YES'
        
        # create the PDF
        self.PDF_CREATOR = {}
        self.PDF_CREATOR['OFFICE'] = "libreoffice --invisible --norestore --nologo -pt Virtual_PDF_Printer"
        self.PDF_CREATOR['IMAGE'] = "convert"
        self.PDF_CREATOR['EBOOK'] = "ebook-convert "
        
        #OTS
        self.OTS_RATIO = 20
        try:
            self.cpServer = ConfigParser.ConfigParser()
            self.cpServer.readfp(open(self.CUON_FS + '/crontab.ini'))
            liSections = self.cpServer.sections()
            if liSections:
                for sSection in liSections:
                    client_id = int(sSection[sSection.find("_")+1:])
                    liItems = self.cpServer.items(sSection)
                    print sSection, liItems
                    if liItems:
                        for oneItem in liItems:
                            liOneItem = oneItem[1].split()
                            sItem = ""
                            lastItem=""
                            for anItem in liOneItem:
                                if anItem == liOneItem[-1]:
                                    lastItem = anItem
                                else:
                                    sItem += "," + anItem
                            sItem = lastItem + sItem
                                
                            self.CRONTAB.append([client_id,sItem ])
        except Exception, params:
            print "error crontab"
            print Exception, params
                    



        try:
            self.cpServer = ConfigParser.ConfigParser()
            
            self.cpServer.readfp(open(self.CUON_FS + '/server.ini'))
            
            
            # Instances 
            value = self.getConfigOption('INSTANCES','XMLRPC')
            if value:
                self.XMLRPC_INSTANCES = int(value)
              
            value = self.getConfigOption('INSTANCES','REPORT')
            if value:
                self.REPORT_INSTANCES = int(value)   
            value = self.getConfigOption('INSTANCES','ADD_PORT')
            if value:
                self.ADD_PORT_INSTANCES = int(value)   
                if self.ADD_PORT_INSTANCES < 10:
                    self.ADD_PORT_INSTANCES = 10
            
            
            
            # xmlrpc settings
             #ALLOW_HTTP: NO
            #HTTP_PORT:7080
            #HTTPS_PORT:7580
            #HOST:localhost
            
            value = self.getConfigOption('XMLRPC','HOST')
            if value:
                self.XMLRPC_HOST = value
                
            value = self.getConfigOption('XMLRPC','HTTP_PORT')
            if value:
                self.XMLRPC_HTTP_PORT = int(value)
                
            value = self.getConfigOption('XMLRPC','HTTPS_PORT')
            if value:
                self.XMLRPC_HTTPS_PORT = int(value)
                    
                
            value = self.getConfigOption('XMLRPC','ALLOW_HTTP')
            if value:
            
                self.XMLRPC_ALLOW_HTTP = self.checkBool(value)        
                
            value = self.getConfigOption('XMLRPC','PROTOCOL')
            if value:
                self.XMLRPC_PROTO = value    
            
            # Automatic schedul 

            value = self.getConfigOption('AUTOMATIC','SCHEDUL')
            if value:
                if value.upper() == 'YES':
                    self.AUTOMATIC_SCHEDUL = True
              
            # Debug
            value = self.getConfigOption('DEBUG','ACTIVATE')
            if value:
                if value.upper() == 'YES':
                    self.DEBUG = True
                
            
            # AI
            value = self.getConfigOption('AI','AI_HOST')
            if value:
                self.AI_HOST = value
                
            value = self.getConfigOption('AI','AI_PORT')
            if value:
                self.AI_PORT = int(value)
                
            # Postgres
            value = self.getConfigOption('POSTGRES','POSTGRES_HOST')
            if value:
                self.POSTGRES_HOST = value
                
            value = self.getConfigOption('POSTGRES','POSTGRES_PORT')
            if value:
                self.POSTGRES_PORT = int(value)
                
            value = self.getConfigOption('POSTGRES','POSTGRES_DB')
            if value:
                self.POSTGRES_DB = value
                
            value = self.getConfigOption('POSTGRES','POSTGRES_USER')
            if value:
                self.POSTGRES_USER = value
                
            value = self.getConfigOption('POSTGRES','SESSION_TIME')
            if value:
                try:
                    self.SESSION_TIME = int(value)
                except:
                    pass
                    
            # Web2
            value = self.getConfigOption('WEB2','HOST')
            if value:
                self.WEB_HOST2 = value
            value = self.getConfigOption('WEB2','PORT')
            if value:
                self.WEB_PORT2 = int(value)
            value = self.getConfigOption('WEB2','START')
            if value:
            
                self.WEB_START2 = self.checkBool(value)        
            # Web3
            value = self.getConfigOption('WEB3','HOST')
            if value:
                self.WEB_HOST3 = value
            value = self.getConfigOption('WEB3','PORT')
            if value:
                self.WEB_PORT3 = int(value)
            value = self.getConfigOption('WEB3','START')
            if value:
            
                self.WEB_START3 = self.checkBool(value)   
            value = self.getConfigOption('WEB3','USER')   
            if value:
                self.WEB_USER3 = value
                
            value = self.getConfigOption('WEB3','PASSWORD')   
            if value:
                self.WEB_PASSWORD3 = value
                    
            value = self.getConfigOption('WEB3','CLIENT_ID')   
            if value:
                self.WEB_CLIENT_ID3 = int(value)
                    
                
                
            # Web4
            value = self.getConfigOption('WEB4','HOST')
            if value:
                self.WEB_HOST4 = value
            value = self.getConfigOption('WEB4','PORT')
            if value:
                self.WEB_PORT4 = int(value)
                    
            value = self.getConfigOption('WEB4','START')
            if value:
            
                self.WEB_START4 = self.checkBool(value)      
            # OSCOMMERCE
            value = self.getConfigOption('OSCOMMERCE','OSC_HOST')
            if value:
                self.OSC_HOST = value
                
            value = self.getConfigOption('OSCOMMERCE','OSC_PORT')
            if value:
                self.OSC_PORT = int(value)
                
            value = self.getConfigOption('OSCOMMERCE','OSC_DB')
            if value:
                self.OSC_DB = value
                
            value = self.getConfigOption('OSCOMMERCE','OSC_USER')
            if value:
                self.OSC_USER = value
                
                
            #PDF
            value = self.getConfigOption('PDF','ENCODING')
            if value:
                self.PdfEncoding = value

            # EMAIL Config
            value = self.getConfigOption('EMAIL','DEFAULTSERVER')
            if value:
                self.EMAILSERVER = value
            
            value = self.getConfigOption('EMAIL','DEFAULTPORT')
            if value:
                self.EMAILPORT = value
                        
            value = self.getConfigOption('EMAIL','DEFAULTUSER')
            if value:
                self.EMAILUSER = value
            
            value = self.getConfigOption('EMAIL','DEFAULTPASSWORD')
            if value:
                self.EMAILPASSWORD = value
            
            value = self.getConfigOption('EMAIL','ENCODING')
            if value:
                self.EMAILENCODING = value
                
            value = self.getConfigOption('EMAIL','CRYPT')
            if value:
                self.EMAILCRYPT = value
                
            value = self.getConfigOption('CURRENCY','NAME')
            if value:
                self.CURRENCY_NAME = value
            
            value = self.getConfigOption('CURRENCY','SIGN')
            if value:
                self.CURRENCY_SIGN = value
                
            value = self.getConfigOption('CURRENCY','ROUND')
            if value:
                self.CURRENCY_ROUND = int(value)
               
            # jABBER
            value = self.getConfigOption('JABBER','SERVER')
            if value:
                self.JABBERSERVER = value
            
            value = self.getConfigOption('JABBER','USERNAME')
            if value:
                self.JABBERUSERNAME = value
            
            value = self.getConfigOption('JABBER','PASSWORD')
            if value:
                self.JABBERPASSWORD = value
            
            # IMAP
            
            value = self.getConfigOption('IMAP','SERVER')
            if value:
                self.IMAP_HOST = value
                
            value = self.getConfigOption('IMAP','PORT')
            if value:
                self.IMAP_PORT = int(value)
                
            value = self.getConfigOption('IMAP','USERNAME')
            if value:
                self.IMAP_USERNAME = value
            
            value = self.getConfigOption('IMAP','PASSWORD')
            if value:
                self.IMAP_PASSWORD = value
                
            # VERSION_INFO
            value = self.getConfigOption('VERSION','SEND_VERSION_INFO')
            if value:
                self.SEND_VERSION_INFO = value.upper()
            # PDF-CREATOR
            value = self.getConfigOption('PDFCREATOR','OFFICE')
            if value:
              self.PDF_CREATOR['OFFICE'] =  value.strip()
            value = self.getConfigOption('PDFCREATOR','IMAGE')
            if value:
              self.PDF_CREATOR['IMAGE'] =  value.strip()  

#            self.REPORT_DOUBLE = 1
#            value = self.getConfigOption('SPECIAL','REPORT_DOUBLE')
#            if value:
#              self.REPORT_DOUBLE =  int(value.strip() )
#            
            # OTS
            
            value = self.getConfigOption('TEXTEXTRACT','OTS_RATIO')
            if value:
              self.OTS_RATIO = int( value.strip())
              
        except Exception, params:
            print "Error read ini-File"
            print Exception
            print params
            
            
        AI_SERVER = "http://" + self.AI_HOST + ":" + `self.AI_PORT`
        self.ai_server = xmlrpclib.ServerProxy(AI_SERVER)
        REPORT_SERVER = "http://" + self.REPORT_HOST + ":" + `self.REPORT_PORT`
        pv = sys.version
       
        self.report_server = xmlrpclib.ServerProxy(REPORT_SERVER,allow_none=True)
       

        if self.XMLRPC_ALLOW_HTTP:
            self.LOCALXMLRPCSERVER = xmlrpclib.ServerProxy("http://" + self.XMLRPC_HOST +":" + `self.XMLRPC_HTTP_PORT`)
        else:
            self.LOCALXMLRPCSERVER = xmlrpclib.ServerProxy("https://" + self.XMLRPC_HOST +":" + `self.XMLRPC_HTTPS_PORT`)
            
        WEB_SERVER = "http://" + self.WEB_HOST + ":" + `self.WEB_PORT`
        self.web_server = xmlrpclib.ServerProxy(WEB_SERVER)
        # Limits
        self.LIMITSQL = 100
        self.LIMITGARDEN = 100
        self.LIMITADDRESS = 100
        self.LIMITARTICLES = 100
        self.LIMITPROJECT = 100
        self.LIMITORDER = 100
        self.LIMITDMS = 100
        


        # clients ini

        try:
            self.cpServer = ConfigParser.ConfigParser()
            
            self.cpServer.readfp(open(self.CUON_FS + '/clients.ini'))

            liSections = self.cpServer.sections()
            for sSection in liSections:
                self.dicClientsOptions[sSection] = {}
                for sOption in self.liClientsOptions:
                     
                    value = self.getConfigOption(sSection,sOption)
                    if value:
                        self.dicClientsOptions[sSection][sOption] = value
 
        except Exception,params:
            print "error at clients.ini"
            print Exception,params






        # SQL
        
        try:
            self.cpServer = ConfigParser.ConfigParser()
            
            self.cpServer.readfp(open(self.CUON_FS + '/sql.ini'))
            
            value = self.getConfigOption('LIMIT','GARDEN')
            if value:
                self.LIMITGARDEN = value
                
            value = self.getConfigOption('LIMIT','ADDRESS')
            if value:
                self.LIMITADDRESS = value
   
            value = self.getConfigOption('LIMIT','ARTICLES')
            if value:
                self.LIMITARTICLES = value
   
            value = self.getConfigOption('LIMIT','PROJECT')
            if value:
                self.LIMITPROJECT = value
   
            value = self.getConfigOption('LIMIT','ORDER')
            if value:
                self.LIMITORDER = value
                
            value = self.getConfigOption('LIMIT','DMS')
            if value:
                self.LIMITDMS = value   
      
        except Exception, params:
            print "Error read ini-File = sql.ini" 
            print Exception
            print params        
            
    
        self.liModules = []
        self.dicLimitTables = {}
        
        # Garden
        for i in ['hibernation', 'hibernation_plant', 'botany']:
            
            self.dicLimitTables[i] = self.LIMITGARDEN
            
        for i in ['address', 'parner', 'partner_schedul']:
            
            self.dicLimitTables[i] = self.LIMITADDRESS
            
        for i in ['articles']:
            
            self.dicLimitTables[i] = self.LIMITARTICLES
        
        for i in ['projects', 'project_phases']:
            
            self.dicLimitTables[i] = self.LIMITPROJECT
            
        for i in ['orderbook', 'inpayment']:
            
            self.dicLimitTables[i] = self.LIMITORDER
            
        for i in ['dms']:
            
            self.dicLimitTables[i] = self.LIMITDMS
                
            
#        self.dicLimitTables['GARDEN'] = {'list':['hibernation', 'hibernation_plant', 'botany'],'limit':self.LIMITGARDEN}
#        self.liModules.append('GARDEN')
#        
#        self.dicLimitTables['ADDRESS'] = {'list':['address'], 'limit':self.LIMITADDRESS}
#        self.liModules.append('ADDRESS')
#        
#        self.dicLimitTables['PROJECTS'] = {'list':['projects', 'project_phases'], 'limit':self.LIMITPROJECT}
#        self.liModules.append('PROJECTS')


#             # Executables

        self.prefDMS= {}
        self.prefDMS['exe'] = {}
        self.prefDMS['exe']['writer'] = '/usr/bin/libreoffice'
        self.prefDMS['exe']['calc'] = '/usr/bin/libreoffice'
        self.prefDMS['exe']['draw'] = '/usr/bin/libreoffice'
        self.prefDMS['exe']['impress'] = '/usr/bin/libreoffice'
        self.prefDMS['exe']['image'] = '/usr/bin/gimp'
        self.prefDMS['exe']['music'] = '/usr/bin/xmms'
        self.prefDMS['exe']['ogg'] = '/usr/bin/xmms'
        self.prefDMS['exe']['wav'] = '/usr/bin/xmms' 
        self.prefDMS['exe']['pdf'] = '/usr/bin/evince'
        self.prefDMS['exe']['tex'] = '/usr/bin/xemacs'
        self.prefDMS['exe']['ltx'] = '/usr/bin/xemacs'
        self.prefDMS['exe']['txt'] = '/usr/bin/gedit'
        self.prefDMS['exe']['flowchart'] = '/usr/bin/dia'
        self.prefDMS['exe']['googleearth'] = 'googleearth'
        self.prefDMS['exe']['internet'] = '/usr/bin/firefox'
        self.prefDMS['exe']['html'] = '/usr/bin/bluefish'
        self.prefDMS['exe']['python'] = '/usr/bin/gedit'
        self.prefDMS['exe']['mindmap'] = '/usr/bin/vym'
        self.prefDMS['exe']['CAD'] = '/usr/bin/qcad'
        self.prefDMS['exe']['EPUB'] = '/usr/bin/calibre'
        self.prefDMS['exe']['EMAIL'] = '/usr/bin/thunderbird'
        
             # File-format
        self.prefDMS['fileformat'] = {}
        self.prefDMS['fileformat']['scanImage'] = {'format':'Image Scanner', 'suffix':['NONE'], 'executable': 'INTERN'}
        self.prefDMS['fileformat']['LINK'] =  {'format':'LINK', 'suffix':['NONE'], 'executables': 'INTERN'}
        self.prefDMS['fileformat']['oow'] =  {'format':'Open Office Writer',  'suffix':['sxw', 'sdw','odt','ott','doc','rtf'], 'executable': self.prefDMS['exe']['writer'] }
        self.prefDMS['fileformat']['ooc'] =  {'format':'Open Office Calc',  'suffix':['sxc','sdc','ods','ots','xls'], 'executable': self.prefDMS['exe']['calc']}
        self.prefDMS['fileformat']['ood'] =  {'format':'Open Office Draw',  'suffix':['sxd','odg','otg'], 'executable': self.prefDMS['exe']['draw']}
        self.prefDMS['fileformat']['ooi'] =  {'format':'Open Office Impress', 'suffix':['sti','sxi','odp','otp'], 'executable': self.prefDMS['exe']['impress']}
        self.prefDMS['fileformat']['gimp'] =  {'format':'Gimp',  'suffix':['xcf','jpg','gif','png', 'tif', 'tiff'], 'executable': self.prefDMS['exe']['image']}
        self.prefDMS['fileformat']['mp3'] =  {'format':'MP3',  'suffix':['mp3'], 'executable': self.prefDMS['exe']['music']}
        self.prefDMS['fileformat']['ogg'] =  {'format':'OGG',  'suffix':['ogg'], 'executable': self.prefDMS['exe']['ogg']}
        self.prefDMS['fileformat']['wav'] =  {'format':'WAV',  'suffix':['wav'], 'executable': self.prefDMS['exe']['wav']}
        self.prefDMS['fileformat']['txt'] =  {'format':'Text',  'suffix':['txt'], 'executable': self.prefDMS['exe']['txt']}
        self.prefDMS['fileformat']['tex'] =  {'format':'TEX',  'suffix':['tex',], 'executable': self.prefDMS['exe']['tex']}
        self.prefDMS['fileformat']['latex'] =  {'format':'LATEX',  'suffix':['ltx',], 'executable': self.prefDMS['exe']['ltx']}
        self.prefDMS['fileformat']['pdf'] =  {'format':'Adobe PDF',  'suffix':['pdf',], 'executable': self.prefDMS['exe']['pdf']}
        
        self.prefDMS['fileformat']['dia'] =  {'format':'DIA', 'suffix':['dia'], 'executable': self.prefDMS['exe']['flowchart']}
        self.prefDMS['fileformat']['googleearth'] =  {'format':'KMZ', 'suffix':['kmz','kml','eta'], 'executable': self.prefDMS['exe']['googleearth']}
        self.prefDMS['fileformat']['html'] =  {'format':'HTML', 'suffix':['html','htm'], 'executable': self.prefDMS['exe']['html']}
        self.prefDMS['fileformat']['www'] =  {'format':'WWW', 'suffix':['www','http'], 'executable': self.prefDMS['exe']['internet']}
        self.prefDMS['fileformat']['python'] =  {'format':'PYTHON', 'suffix':['py'], 'executable': self.prefDMS['exe']['python']}
        self.prefDMS['fileformat']['mindmap'] =  {'format':'MINDMAP', 'suffix':['vym', 'mm', 'mmp', 'emm', 'nmind', 'TWD'], 'executable': self.prefDMS['exe']['mindmap']}
        self.prefDMS['fileformat']['cad'] =  {'format':'CAD', 'suffix':['dwb', 'stp', 'step', 'dwg', 'dxf', 'igs', 'iges'], 'executable': self.prefDMS['exe']['CAD']}
        
        self.prefDMS['fileformat']['bin'] =  {'format':'BINARY', 'suffix':['bin'], 'executable': self.prefDMS['exe']['txt']}
        self.prefDMS['fileformat']['epub'] =  {'format':'BINARY', 'suffix':['epub',"mobi"], 'executable': self.prefDMS['exe']['EPUB']}
        self.prefDMS['fileformat']['email'] =  {'format':'EML', 'suffix':['eml'], 'executable': self.prefDMS['exe']['EMAIL']}
        
        self.OrderStatus  = {}
        self.OrderStatus['OrderStart'] = 500
        self.OrderStatus['OrderEnd'] = 799
        self.OrderStatus['ProposalStart'] = 300
        self.OrderStatus['ProposalEnd'] = 399
        self.OrderStatus['EnquiryStart'] = 100
        self.OrderStatus['EnquiryEnd'] = 199
        
        
        self.FCT = {}
        self.FCT["fct_getDays"] = "int"
        self.FCT["fct_getLastname"] = "string"
        self.FCT["fct_getValueAsCurrency"] = "string"
        self.FCT["fct_getPositionSingleNetPrice"] = "float"
        self.FCT["fct_getOrderTotalNetSum"] = "float"
        self.FCT["fct_getHibernation_Plant_Count"] = "int"


        self.liTime = []
        
        self.liTime.append('0:00')
        self.liTime.append('0:15')
        self.liTime.append('0:30')
        self.liTime.append('0:45')
        self.liTime.append('1:00')
        self.liTime.append('1:15')
        self.liTime.append('1:30')
        self.liTime.append('1:45')
        self.liTime.append('2:00')
        self.liTime.append('2:15')
        self.liTime.append('2:30')
        self.liTime.append('2:45')
        self.liTime.append('3:00')
        self.liTime.append('3:15')
        self.liTime.append('3:30')
        self.liTime.append('3:45')
        self.liTime.append('4:00')
        self.liTime.append('4:15')
        self.liTime.append('4:30')
        self.liTime.append('4:45')
        self.liTime.append('5:00')
        self.liTime.append('5:15')
        self.liTime.append('5:30')
        self.liTime.append('5:45')
        self.liTime.append('6:00')
        self.liTime.append('6:15')
        self.liTime.append('6:30')
        self.liTime.append('6:45')
        self.liTime.append('7:00')
        self.liTime.append('7:15')
        self.liTime.append('7:30')
        self.liTime.append('7:45')
        self.liTime.append('8:00')
        self.liTime.append('8:15')
        self.liTime.append('8:30')
        self.liTime.append('8:45')
        self.liTime.append('9:00')
        self.liTime.append('9:15')
        self.liTime.append('9:30')
        self.liTime.append('9:45')
        self.liTime.append('10:00')
        self.liTime.append('10:15')
        self.liTime.append('10:30')
        self.liTime.append('10:45')
        self.liTime.append('11:00')
        self.liTime.append('11:15')
        self.liTime.append('11:30')
        self.liTime.append('11:45')
        self.liTime.append('12:00')
        self.liTime.append('12:15')
        self.liTime.append('12:30')
        self.liTime.append('12:45')
        self.liTime.append('13:00')
        self.liTime.append('13:15')
        self.liTime.append('13:30')
        self.liTime.append('13:45')
        self.liTime.append('14:00')
        self.liTime.append('14:15')
        self.liTime.append('14:30')
        self.liTime.append('14:45')
        self.liTime.append('15:00')
        self.liTime.append('15:15')
        self.liTime.append('15:30')
        self.liTime.append('15:45')
        self.liTime.append('16:00')
        self.liTime.append('16:15')
        self.liTime.append('16:30')
        self.liTime.append('16:45')
        self.liTime.append('17:00')
        self.liTime.append('17:15')
        self.liTime.append('17:30')
        self.liTime.append('17:45')
        self.liTime.append('18:00')
        self.liTime.append('18:15')
        self.liTime.append('18:30')
        self.liTime.append('18:45')
        self.liTime.append('19:00')
        self.liTime.append('19:15')
        self.liTime.append('19:30')
        self.liTime.append('19:45')
        self.liTime.append('20:00')
        self.liTime.append('20:15')
        self.liTime.append('20:30')
        self.liTime.append('20:45')
        self.liTime.append('21:00')
        self.liTime.append('21:15')
        self.liTime.append('21:30')
        self.liTime.append('21:45')
        self.liTime.append('22:00')
        self.liTime.append('22:15')
        self.liTime.append('22:30')
        self.liTime.append('22:45')
        self.liTime.append('23:00')
        self.liTime.append('23:15')
        self.liTime.append('23:30')
        self.liTime.append('23:45')
        
        
    def getConfigOption(self, section, option, configParser = None):
        value = None
        if configParser:
            cps = configParser
        else:
           cps = self.cpServer
           
        if cps.has_option(section,option):
            try:
                value = cps.get(section, option).strip()
                #print 'options = ',option,  value
            except:
                value = None
            #print 'getConfigOption', section + ', ' + option + ' = ' + value
        return value
    
      
        
    def getParser(self, sFile):
        cpParser = ConfigParser.ConfigParser()
        f = open(sFile)
        #print 'f1 = ', f
        cpParser.readfp(f)
        #print 'cpp', cpParser
        return cpParser, f
        
    def out(self, s):
        self.writeLog(s,self.debug)
        
    def checkEndTime(self, fTime):
        ok = 0
        try:
            if time.time() < fTime:
                ok = 1
        except:
            self.out('Error in time-routine')
                    
        return ok
        
    def createNewSessionID(self, secValue = 42000):
        
#        s = ''
#        
#        n = random.randint(0,1000000000)
#        for i in xrange(27):
#            ok = True
#            while ok:
#                r = random.randint(65,122)
#                if r < 91 or r > 96:
#                    ok = False
#                    s = s + chr(r)
#    
#        s = s + `n`
        #writeLog(s)
        if self.SESSION_TIME:
            secValue = self.SESSION_TIME * 3600 

        s=str(uuid.uuid4())
        return {'SessionID':s, 'endTime': time.time() + secValue}

    def getUserInfo(self, dicUser):
        liUser = []
        liUser.append(dicUser['Name'])
        liUser.append(`dicUser['client']`)
        if  dicUser.has_key('noWhereClient') and dicUser['noWhereClient'].upper() == 'YES':
            liUser.append('1')
        elif dicUser.has_key('noWhereClient') and dicUser['noWhereClient'].upper() == 'NO':
             liUser.append('0')
        else:
            liUser.append('1')
        
        liUser.append(dicUser['Locales'])
        liUser.append(dicUser['SQLDateFormat'])
        liUser.append(dicUser['SQLTimeFormat'])
        liUser.append(dicUser['SQLDateTimeFormat'])
        
        
        return liUser
        
    def getWhere(self, sWhere, dicUser, Single = 0, Prefix=""):
        #self.writeLog('Start getWhere Single = ' +`Single`)
        if  isinstance(dicUser, types.ListType):
            dicUser = self.list2Dic(dicUser)
            
        
        if not dicUser.has_key('noWhereClient'):
            if sWhere and len(sWhere) > 0 and Single == 0:
               iFind = sWhere.upper().find('WHERE' )
               if iFind >= 0:
                  sWhere = sWhere[0:iFind + 5] + " "+ Prefix + "client = " + `dicUser['client']` + " and "+ Prefix + "status != 'delete' and " + sWhere[iFind +5:] 
               else:
                   sWhere = " where "+ Prefix + "client = " + `dicUser['client']` + " and "+ Prefix + "status != 'delete' "
            elif Single == 1:
               sWhere = " Where "+ Prefix + "client = " + `dicUser['client']` + " and " + Prefix + "status != 'delete' "
        
            elif Single == 2:
               sWhere = " and "+ Prefix + "client = " + `dicUser['client']` + " and "+ Prefix + "status != 'delete' "
        
            else:
               sWhere = " where "+ Prefix + "client = " + `dicUser['client']` + " and "+ Prefix + "status != 'delete' "
        
        #self.writeLog('getWhere = ' + `sWhere`)
        if not sWhere:
            sWhere = ' '
        return sWhere       
    def openDB(self):
        #self.dbase = shelve.open(os.path.normpath(self.CUON_FS + '/' + 'cuonData'))
        pass

    def closeDB(self):
        #self.dbase.close()
        pass
        
    def saveObject(self, key, obj):
        pkey = os.path.normpath(self.CUON_FS + '/KEY_' + key)
        fkey = open(pkey,'w')
        pickle.dump(obj,fkey, 1)
        fkey.close()
        #self.dbase[key] = oValue

    def loadObject(self, key):
        pkey = os.path.normpath(self.CUON_FS +'/KEY_' + key)
        #print 'pkey = ',  pkey
        try:
            fkey = open(pkey)
            #print '1, ', fkey
            obj =  pickle.load(fkey)
            #print '2,  ', obj
            fkey.close()
        except:
            obj = None
        return obj
       
    def writeLog(self, sLogEntry, debugValue = 1):
        
        if debugValue > 100 or self.DEBUG:
            try:
                #print 'debugValue', debugValue
                if debugValue == 1 or self.DEBUG_MODUS > 0:
                    
                    file = open('/var/log/cuon_allserver.log','a')
                    file.write(time.ctime(time.time() ))
                    file.write('\n')
                    file.write(sLogEntry)
                    file.write('\n')
                    file.close()
                    #print sLogEntry
            except:
                pass
                
            
    def getStaffID(self, dicUser):
        return "(select id from staff where staff.cuon_username = '" +  dicUser['Name'] + "' and staff.client = " + `dicUser["client"]` +" )"
    
    
    def getTimeString(self, time_id):
        
        return self.liTime[time_id]
                
              
    def getTime(self,s ):
        Hour,Minute = divmod(s,4)
        Minute = Minute * 15
        
        return Hour, Minute
    
    def rebuild(self, data):
        s = self.doDecode64(data)
        Data = self.decompress(s)
        return Data
        
    
    def decompress(self, data):
        Data = None
        try:
            Data = bz2.decompress(data)
                 
        except Exception, param:
            print Exception, param
        #print 'data', data
        #print 'Data', Data
        return Data
        
    def doDecode64(self, data):
        Data = None
        try:
            Data = base64.decodestring(data)
                 
        except Exception, param:
            print Exception, param
        #print 'data', data
        #print 'Data', Data
        return Data
        
    def xmlrpc_testXmlrpc(self, iA = None, iB=None):
        print 'test xmlrpc', iA, iB
        if iA and iB:
            return iA * iB
        else:
            return 'Test without Values'
    
   
                
    def getRandomFilename(self, sPrefix='.tmp'):
    
    
       return str(uuid.uuid4())+ sPrefix
        
    
    def getBeforeYears(self, datepart,z1):
            beforeYears = 0
            newTime = time.localtime()
        
            if datepart == 'month' and z1 > 0:
                if (newTime.tm_mon - z1) <= 0:
                    beforeYears = 1
            if datepart == 'quarter' and z1 > 0:
                if (newTime.tm_mon - z1)*3 <= 0:
                    beforeYears = 1
                
            return newTime.tm_year - beforeYears       
            
    def getNow(self, vSql, z1,  year=1900):
        newTime = time.localtime()
        if year == 1900:
            year = int(time.strftime('%Y', newTime))
        newTime = time.localtime()
        datepart = vSql['id']
        now = 0
            
        if datepart == 'month' :
            now = newTime.tm_mon - z1
            if now < 1:
                now += 12
                year -= 1
                
        elif datepart == 'quarter' :
            if newTime.tm_mon in [1, 2, 3]:
                now = 1
            elif newTime.tm_mon in [4, 5, 6]:
                now = 2
            elif newTime.tm_mon in [7, 8, 9]:
                now = 3
            elif newTime.tm_mon in [10, 11, 12]:
                now = 4
            now -= z1
            
            if now < 1:
                now += 4
                year -= 1
                
        elif datepart == 'week' :
           now =  "  date_part('" + vSql['sql'] + "', now()) - " + `z1`
        elif datepart == 'day' :
           now =  "  date_part('" + vSql['sql'] + "', now()) - " + `z1`
            
        else:
            now = z1
        if isinstance(now, types.IntType):
            now = `now`
            
        return now,  `year`
        
        
   
    def addDateTime(self, firstRecord):
        dicTime = self.getActualDateTime()
        if dicTime:
            for key in dicTime:
                firstRecord['date_' + key] = dicTime[key]
        return firstRecord       




    def getTime(self,s ):
        try:
            if isinstance(s,types.StringType):
                iS = int(s)
            else:
                iS = s
                
            Hour,Minute = divmod(iS,4)
            Minute = Minute * 15
        except:
            Hour = 0
            Minute = 0
            
        
        return Hour, Minute
        
    def getTimeString(self, s):
        Hour, Minute = self.getTime(s)
        #print Hour, Minute
        sHour = `Hour`
        if Minute == 0:
            sMinute = '00'
        else:
            sMinute = `Minute`
        
        s = sHour + ':' + sMinute
        return s
        
        
    def getActualDateTime(self):
        dicTime = {}
        try:
            newTime = time.localtime()
            tDate =  time.strftime(self.DIC_USER['DateformatString'], newTime)
            tTime =  time.strftime(self.DIC_USER['TimeformatString'], newTime)
            tStamp = time.strftime(self.DIC_USER['DateTimeFormatstring'], newTime)
            dicTime = {'date':tDate, 'time':tTime, 'timestamp':tStamp }
        except Exception,  params:
            print Exception, params
            dicTime = {'date':' ', 'time':' ', 'timestamp':' ' }
            
        
        return dicTime
        
    
    def getActualDateString(self):
        dicTime = self.getActualDateTime()
        return dicTime['date']+'-'+ dicTime['timestamp']
        
    
    def getDateTime(self,dateString,strFormat="%Y-%m-%d"):
        # Expects "YYYY-MM-DD" string
        # returns a datetime object
        eSeconds = time.mktime(time.strptime(dateString,strFormat))
        return datetime.datetime.fromtimestamp(eSeconds)
    
    def formatDate(self,dtDateTime,strFormat="%Y-%m-%d"):
        # format a datetiself, me object as YYYY-MM-DD string and return
        return dtDateTime.strftime(strFormat)
    
    def getFirstOfMonth2(self, dtDateTime):
        #what is the first day of the current month
        ddays = int(dtDateTime.strftime("%d"))-1 #days to subtract to get to the 1st
        delta = datetime.timedelta(days= ddays)  #create a delta datetime object
        return dtDateTime - delta                #Subtract delta and return
    
    def getFirstOfMonth(self, dtDateTime):
        #what is the first day of the current month
        #format the year and month + 01 for the current datetime, then form it back
        #into a datetime object
        return self.getDateTime(self.formatDate(dtDateTime,"%Y-%m-01"))
        
    def getFirstOfYear(self, dtDateTime):
        #what is the first day of the current month
        #format the year and month + 01 for the current datetime, then form it back
        #into a datetime object
        return self.getDateTime(self.formatDate(dtDateTime,"%Y-01-01"))
    
    def getLastOfMonth(self, dtDateTime):
        dYear = dtDateTime.strftime("%Y")        #get the year
        dMonth = str(int(dtDateTime.strftime("%m"))%12+1)#get next month, watch rollover
        if int(dMonth) == 1:
            iYear = int(dYear)
            iYear += 1
            dYear = `iYear`
        print dMonth, len(dMonth)
        print dYear, len(dYear)
        
        dDay = "1"                               #first day of next month
        nextMonth = self.getDateTime("%s-%s-%s"%(dYear,dMonth,dDay))#make a datetime obj for 1st of next month
        delta = datetime.timedelta(seconds=1)    #create a delta of 1 second
        return nextMonth - delta                 #subtract from nextMonth and return
        
    def getLastOfYear(self, dtDateTime):
        dYear = dtDateTime.strftime("%Y")        #get the year
        
        return self.getDateTime(self.formatDate(dtDateTime,"%Y-12-31"))
    
    
    def getFirstDayOfMonth(self,sdate=None):
        if sdate == None:
            dDate = datetime.date.today()   
        else:
            dDate = sdate
            
        return self.getFirstOfMonth(dDate)
        
    def getLastDayOfMonth(self,sdate=None):
        if sdate == None:
            dDate = datetime.date.today()   
        else:
            dDate = sdate
            
        return self.getLastOfMonth(dDate)
        
        
    def getSeconds(self, dt):
        eSeconds = time.mktime( dt.timetuple())
        return eSeconds

    
    def getFirstDayOfMonthAsSeconds(self,sdate=None):
        return self.getSeconds(self.getFirstDayOfMonth(sdate))
        
    def getLastDayOfMonthAsSeconds(self,sdate=None):
        return self.getSeconds(self.getLastDayOfMonth(sdate))
        
        
    def getFirstLastDayOfLastMonthAsSeconds(self,sdate=None):
        currentFirstDay = self.getFirstDayOfMonth(sdate)
        secs = self.getSeconds(currentFirstDay) - 10
        print 'secs' , secs
        ddate = datetime.datetime.fromtimestamp(secs)
        
        dBegin = self.getFirstDayOfMonthAsSeconds(ddate)
        dEnd = self.getLastDayOfMonthAsSeconds(ddate)
        print dBegin,dEnd
        return dBegin,dEnd
        
        
    def checkType(self, oType, sType):
        bRet = False
        if sType == 'string':
            if isinstance(oType, types.StringType):
                bRet = True
        elif sType == 'int':
            if isinstance(oType, types.IntType):
                bRet = True
        elif sType == 'float':
            if isinstance(oType, types.FloatType):
                bRet = True
        elif sType == 'unicode':
            if isinstance(oType, types.UnicodeType):
                bRet = True                                      
        return bRet
        
    def convertTo(self, value, sType, iRound=0):
        s = None
        if sType == "String":
            try:
                print 'convertTo Values = ',  value, sType,  iRound
                s = '%0*d' % (iRound, value)
                #s = `value`
                s = s.strip()
                sL = len(s)
                if s[0] == 'L':
                    s = s[1:]
                if s[sL-1]=='L':
                    s = s[:sL-1]
            except:
                pass
        print 'ConvertTo result = ',  s
        return s
                
        
    def getAssociatedTable(self, iNumber):
        sTable = None
        iDMS = 0
        
        if iNumber:
            if iNumber == 1:
                sTable = 'botany'
                iDMS = 110500
                
        return sTable, iDMS
        
        
    def  getNormalSqlData(self,  dicUser, braces=True,  coma=True):
        print `dicUser`
        liFields = []
        liValues = []
        
        sF = 'client '
        sV = `dicUser['client']` + ' '
        
        #set to every sf,sv the coma
        if coma:
            sF =  ', ' + sF
            sV =  ', ' + sV
            
        # set at last sf, sv the braces
        if braces:
            sF +=  ' )'
            sV +=  ')'
        
        liFields.append( sF)
        liValues.append( sV)
        
        return liFields,  liValues
        
    def checkBool(self,  value) :
        if value.strip().upper() in ['YES', 'Y', 'JA', 'J', 'SI', 'TRUE']:
            return True
        else:
            return False
            
        
    def xmlrpc_getComboReportLists(self, dicUser,  sPattern):
       
        
        liReport = []
        liReport1 = []
        liReport2 = []
        liReport3 = []
        liReportS = []
        liReportS2 = []
        
        repPath = "/usr/share/cuon/cuon_server/src/cuon/Reports/"
        # check user
        
        if os.path.isdir(repPath + 'user_' + dicUser['Name']):
            liReport1 = glob.glob(repPath + 'user_' + dicUser['Name'] + "/" + sPattern)
                                                                       
        # check to client id 
        if dicUser['client'] == -7:
            for clientID in xrange(50):
                if os.path.isdir(repPath + 'client_' + `clientID`):
                    liReport2 += glob.glob(repPath + 'client_' + `clientID` + "/" + sPattern)
        else:
            if os.path.isdir(repPath + 'client_' + `dicUser['client']`):
                liReport2 = glob.glob(repPath + 'client_' + `dicUser['client']` + "/" + sPattern)
                    
            #check last the XML
        liReport3 = glob.glob(repPath + 'XML' + "/" + sPattern)
        liReportS = liReport1 + liReport2 + liReport3
        if liReportS:
            liReportS2 = []
            for sFile in liReportS:
                print "sfile = ",  sFile
                liFile = sFile.split('/')
                liReportS2.append(liFile[len(liFile)-1])
                #print 'liReport = ',  liReportS2
                
                        
            for item in liReportS2:
                if not item in liReport:
                    liReport.append(item)
                #liReport.append(sFile[1])
                    
                
        return liReport
         
    def getNullUUID(self):
        return '00000000-0000-0000-0000-000000000000'
           
    def make64BitInt(self, x, y):
        return   (long(x) << 32) + long(y)


    def getIRandom(self, iValue):
        
        return  int(random.random()*iValue)
     
     
 
    def  normalizeXML(self,  sValue):
        if sValue:
            sValue = sValue.replace('&', '&amp;')
      
            sValue = sValue.replace('\'', '&apos;')
            sValue = sValue.replace('\"', '&quot; ')
            sValue = sValue.replace('<', '&lt;')
            sValue = sValue.replace('>', '&gt;')
            # for testing:
            #sValue = sValue.replace('/', '')
        return sValue
        
    def normalizeHtml(self,  sValue):
        sValue  =  sValue.replace('ä', '&auml;')
        sValue  =  sValue.replace('Ä',	'&Auml;')
        sValue  =  sValue.replace('ö',	'&ouml;')
        sValue  =  sValue.replace('Ö',	'&Ouml;')
        sValue  =  sValue.replace('ü', '&uuml;')
        sValue  =  sValue.replace('Ü',	'&Uuml;')
        sValue  =  sValue.replace('ß', '&szlig;')
        sValue  =  sValue.replace('€', '&euro;')
        sValue  =  sValue.replace('\n', '<BR />')
        sValue  =  sValue.replace('\t', '&nbsp;')
        
        return sValue
        
    def normalizeText(self, sValue):
        sValue  =  sValue.replace( '&auml;','ä')
        sValue  =  sValue.replace('&Auml;','Ä')
        sValue  =  sValue.replace( '&ouml;','ö')
        sValue  =  sValue.replace('&Ouml;','Ö')
        sValue  =  sValue.replace( '&uuml;','ü')
        sValue  =  sValue.replace('&Uuml;','Ü')
        sValue  =  sValue.replace('&szlig;','ß' )
        sValue  =  sValue.replace( '&euro;','€')
        sValue  =  sValue.replace( '<BR />','\n')
        sValue  =  sValue.replace( '&nbsp;','\t')

        return sValue


    def getNewUUID(self):
        return str(uuid.uuid4())
        

    def getCurrency(self, fValue):
        rValue = ("%." + `self.CURRENCY_ROUND` + "f") % round(fValue, self.CURRENCY_ROUND)
        
        print 'Currency = ',  rValue
        return rValue

    def getFormatedNumber(self, sNumber, sLocale):
        
        
        if sLocale in self.numLocalesComa :
   
            sNumber = sNumber.replace(".",",")
            again = 1
            while again:    
              (sNumber,again) = re.subn(r"(\d)(\d\d\d\D)",r"\1.\2",sNumber)
              
              
        return sNumber
            
            
    def checkIt(self, dicUser):
        if not isinstance(dicUser["client"], types.IntType):
            try:
                dicUser["client"] =  int(dicUser["client"])
            except:
                dicUser["client"] = 1

        return dicUser  
           
               

    def getCheckedValue(self, value, type, min = None, max = None,iRound = -1):
        retValue = None
        try:
            assert type
            if type == 'int':
                if isinstance(value, types.IntType) or isinstance(value, types.LongType) :
                
                
                    try:
                        retValue = int(value)
                    except:
                        retValue = 0
                    

                else:
                    try:
                        assert value != None
                        if isinstance(value, types.StringType):
                            value = value.strip()
                            if value[0] == 'L'  or value[0] == 'l':
                                value = value[1:]
                            
                        retValue = int(value)
                    except:
                        retValue = 0
                
                        
            elif type == 'float':
                if not isinstance(value, types.FloatType):
                    try:
                        assert value != None

                        if isinstance(value, types.StringType):
                            
                            value = value.strip()
                            convert = False
                            #print 'convert userlocales = ', self.dicUser['Locales']
                            for sLocale in self.decimalLocale['coma']:
                                #print sLocale
                                if sLocale == self.dicUser['Locales']:
                                    convert = True
                            #print 'convert = ',  convert,  value
                            if convert:
                                #print 'convert to normal float'
                                #value = value.replace('.','')
                                value = value.replace(',','.')
                                
                                print 'convert2 = ',  convert,  value   
                            if value[0] == 'L'  or value[0] == 'l':
                                value = value[1:]
                            #print "value0 = ", value,  value[:-1]
                            while not value[-1].isdigit() :
                                if not value:
                                    break
                                try:
                                    #print "value = ", value,  value[:-1]
                                    value= value[:-1]
                                    
                                except:
                                    break
                                    
                        retValue = float(value)
                    except Exception, params:
                        #print Exception, params
                        retValue = 0.0
                else:
                    retValue =  value 
            elif type == 'toStringFloat':
                if isinstance(value, types.StringType):
                    if value == 'NONE':
                        value = '0.00'
                    elif value == 'None':
                        value = '0.00'
                    
                        
                    convert = False
                    print 'convert userlocales = ', self.dicUser['Locales']
                    for sLocale in self.decimalLocale['coma']:
                        #print sLocale
                        if sLocale == self.dicUser['Locales']:
                            convert = True
                    if convert:
                        #print 'convert to normal float'
                        value = value.replace('.',',')
                        #value = value.replace(',','.')
                    retValue = value 

            elif type == 'toLocaleString':
                retValue = value
                if iRound >= 0:
                    value = round(value,iRound)
                try:
                    if isinstance(value, types.FloatType):
                        convert = False
                        for sLocale in self.decimalLocale['coma']:
                            #print sLocale
                            if sLocale == self.dicUser['Locales']:
                                convert = True
                        print 'convert = ',  convert
                        print 'float = ',  value
                        value = "%f" % value
                        if convert:
                            value = value.replace('.',',') 
                        else:
                            value = value
                        print 'after convert = ',  value
                        retValue = value
                except:
                    retValue = '0'
                
            elif type == 'date':
                #print 'value by date', value
                print 'date1',  value
                retvalue = time.strptime(value, self.dicUser['DateformatString'])
                print 'dt2 = ', retvalue
                
                        
                    #elif entry.getVerifyType() == 'date' and isinstance(sValue, types.StringType):
                    #    dt = datetime.datetimeFrom(sValue)
                    #dt = datetime.strptime(sValue, "YYYY-MM-DD HH:MM:SS.ss")
                    #dt = datetime.datetime(1999)
                    #    # self.out( dt)
                    #    sValue = dt.strftime(self.sDateFormat)
                    
            elif type == 'formatedDate':
                print 'value by formatedDate', value
                checkvalue = []
                retValue = ''
                try:
                    checkvalue = time.strptime(value, self.dicUser['DateformatString'])
                    self.printOut( 'dtFormated2 = ', checkvalue)
                except:
                    retValue = ''
                    checkvalue = []
                if checkvalue and checkvalue[0] == 1900 and checkvalue[1] == 1 and checkvalue[2] == 1:
                    # 1900/1/1 --> set to empty
                    retvalue = ''
                elif checkvalue:
                    retValue = value
                    
                
                
                    
            elif type == 'toStringDate':
                print 'value by toStringDate', value
                retValue = time.strftime(self.dicUser['DateformatString'],value)
                self.printOut( 'dt5 = ', retValue) 
                
            elif type == 'string':    
                #print 'check string = ', value
                
                if not isinstance(value, types.StringType):
                    value = `value`
                if value == 'NONE':
                    value = ''
                elif value == 'None':
                    value = ''
                
                retValue = value
                
                
                
            else:
                retValue = value
        
        except AssertionError:
            print 'No type set '
            retValue = value
        except Exception,params:
            print Exception, params
            retValue = value
        
        #print 'retvalue = ', retValue
        
        return retValue
        
    
    def getClient(self,iClient):
        return "CLIENT_" + `iClient`
    
    def getSqlArrayFromSearchlist(dicSearchfields):
        sSearch = ""
        for key in dicSearchlist:
          sSearch += "'" + key  +"', '" + dicSearchfields[key] + "'"
        print "sSearch = ", sSearch

        return sSearch
    
    def list2Dic(self,liList):
        print "liList = ", liList
        newDic = {}
        try:

            i = 0
            maxLi = len(liList)
            while i < maxLi:
                print "Dic from this list items: ",i, liList[i], liList[i+1]
                newDic[liList[i]] = liList[i+1]
                i += 2
        except:
            pass
        
        print "newDic = ", newDic
        return newDic

    def dic2List(self,dicA):
        liA = []
        for key in dicA.keys():
            liA.append(key)
            liA.append(dicA[key])
            
        return liA 


    def listdic2List(self, liA):
        liRet = []
        for i in liA:
            liRet.append(self.dic2List(i) )
            
        return liRet
    
    def setClientID(self,id):
         if isinstance( id, types.StringType) :
            id = int(dicUser['client'])
         return id

    def string2Dic(self, str):
        print "str = ",str
        mydict = ast.literal_eval(str)

        return mydict
                     
