# coding=utf-8
##Copyright (C) [2003-2012]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

from twisted.web import xmlrpc
import os
import sys
import time
from datetime import datetime
import random   
import xmlrpclib
from basics import basics
import Database
import urllib
import httplib
import md5,types
import uuid

class User(xmlrpc.XMLRPC, basics):
    def __init__(self):
        basics.__init__(self)
        self.Database = Database.Database()
        self.home_url = 'www.cuon.org'
        self.headers = {'Content-type': 'application/x-www-form-urlencoded'}
       
        
    def xmlrpc_getNameOfStandardProfile(self, dicUser):
        sSql = "select profile_name from preferences where username = '" + dicUser['Name'] +"' and is_standard_profile = TRUE and status != 'delete'"
        
        result = self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )
        #print "Profile = ",  result
        
        if result not in ['NONE','ERROR']:
           return result[0]['profile_name']
        else:
           return result
           
 
    def xmlrpc_getIDOfStandardProfile(self, dicUser):
        sSql = "select id from preferences where username = '" + dicUser['Name'] +"' and is_standard_profile = TRUE and status != 'delete'"
        
        result = self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )
        print "Profile = ",  result
        
        if result not in ['NONE','ERROR']:
           return result[0]['id']
        else:
           return 0


    def xmlrpc_getLocales(self, dicUser, sLocale = "en"):
        #print "fetch locales for ", sLocale
        sLocale=sLocale.strip("+")
        sLocale = sLocale.strip()
        #print "fetch locales for ", sLocale
        
        dicRet = {}
        s= []
        if not sLocale or sLocale == "en":
            return dicRet
        
        sPath = "/usr/share/locale/" +sLocale +"/" + "LC_MESSAGES/cuon.po"
        #print "sPath = ", sPath

        try:
            f = open(sPath,"r")
            if f:
                s = f.read()
            f.close()
            #print "s = ", s
        except:
            s = []
        liS = s.split("\n")
        iZ = 1
        tKey=None
        tS = None
        for sLine in liS:
            #print "Line = " + `iZ`, sLine,sLine[0:5],  sLine[6:]
            iZ += 1

            
            if sLine[0:5] == "msgid":
                tKey = sLine[6:]
                tKey = tKey.strip()
                tKey = tKey[1:len(tKey)-1]
                #print "tKey = ", tKey
            elif sLine[0:6] == "msgstr":
                
                tS = sLine[7:]
              
                tS = tS.strip('"')
                print "ts = ", tKey, tS
                #tS = tS[1:len(tS)-1]
                if tKey and tS:
                    #print "found ", tKey,tS
                    dicRet[tKey]=tS
                    tKey=None
                    tS = None

        #print dicRet

        return dicRet

       


           
    def xmlrpc_getPreferencesFromStandard(self, dicUser):

        print "start to load user preferences", dicUser
        dicUser = self.checkIt(dicUser)
        print "after check", dicUser
        sSql = "select * from preferences where username = '" + dicUser['Name'] +"' and is_standard_profile = TRUE and status != 'delete'"
        
        result = self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )
        if result not in ['NONE','ERROR']:
           print "Found = ",result[0]
           return [result[0]]
        else:
           return result

    

    def xmlrpc_getSinglePreferences(self,sPref, sProfile=None,dicUser=None):

        if not sProfile:
            iID = self.xmlrpc_getIDOfStandardProfile(dicUser)
            if iID:
                sSql = "select " + sPref + " from preferences where id = " + `iID` 
                print "sSql = ", sSql 
                result = self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )
                if result not in ['NONE','ERROR']:
                    print "Found = ",result[0]
                    return [result[0]]
                else:
                    return result
          
            
    def xmlrpc_getStandardProfile(self, sProfile, dicUser):
        self.writeLog('Profile =' + `sProfile`)
        sSql = 'select * from preferences where username = \'' + dicUser['Name'] +'\' and profile_name = \'' + sProfile +'\''
        
        result = self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )
        if result not in ['NONE','ERROR']:
           return result[0]
        else:
           return result

    def xmlrpc_getEnabledMenus(self,iModus, dicUser):
        liEnabledModules = []

        liModules = self.xmlrpc_getModulList(dicUser)
        for iL in liModules:
            if iL.has_key('addresses') or iL.has_key("all"):
                liEnabledModules.append('mi_addresses1')
            if iL.has_key('articles') or iL.has_key("all"):
                liEnabledModules.append('mi_articles1')
            if iL.has_key('project') or iL.has_key("all"):
                liEnabledModules.append('mi_project1')
            if iL.has_key('sourcecode') or iL.has_key("all"):
                liEnabledModules.append('mi_sourcenavigator')
            if iL.has_key('dms') or iL.has_key("all"):
                liEnabledModules.append('mi_dms1')
            if iL.has_key('botany') or iL.has_key("all"):
                liEnabledModules.append('mi_botany')
            if iL.has_key('stock') or iL.has_key("all"):
                liEnabledModules.append('mi_stock1')
            if iL.has_key('order') or iL.has_key("all"):
                liEnabledModules.append('mi_order1')
            if iL.has_key('proposal') or iL.has_key("all"):
                liEnabledModules.append('proposal1')
            if iL.has_key('enquiry') or iL.has_key("all"):
                liEnabledModules.append('enquiry')
            if iL.has_key('hibernation') or iL.has_key("all"):
                liEnabledModules.append('mi_hibernation')
            if iL.has_key('graves') or iL.has_key("all"):
                liEnabledModules.append('mi_graves')



                
            if iL.has_key('staff') :
                liEnabledModules.append('mi_staff1')
        
        return liEnabledModules

        
    def xmlrpc_getModulList(self, dicUser):
        #dicUser = {'Name':'jhamel'}
        cp1,f = self.getParser('/etc/cuon/menus.cfg')
        # Name of known modules
        # addresses
        # articles
        # biblio
        # account_book
        # order
        # dms
        # staff
        # expert_system
        values = ['all','addresses','articles','biblio','account_book','order','dms','staff','expert_system','forms','forms_addresses']
        dicUserModules = {}
        dicUserModules[dicUser['Name']] = []


        for key in values:
            value = self.getConfigOption(dicUser['Name'],key,cp1)
            
            if value:
                liValue = value.split('#')
                liKey = []
                for i in liValue:
                    i = i.strip()
                    liPoint = i.split(';')
                    print liPoint
                    print i
                    try:
                        
                        print 'item',liPoint
                        dicKey = {}

                        dicKey[key] = {liPoint[0]:liPoint[1]}
                        dicUserModules[dicUser['Name']].append(dicKey)
                        

                    except Exception,params:
                        print Exception,params
                
        values = ['extendet_gpl']
        
        
        dicKeys = []
        for key in values:
            value0 = self.getConfigOption(dicUser['Name'],key,cp1)
            
            if value0:
                liValue0 = value0.split('%')
                for value in liValue0:
                    liValue = value.split('#')
                    dicKey = {}
                    print 'Value', value
                    for i in liValue:
                        i = i.strip()
                        liPoint = i.split(';')
                        print 'liPoint',liPoint
                        print 'i = ', i
                        try:
                            if liPoint[1][0] == '[':
                                print 'list',liPoint
                                liList = (liPoint[1][1:len(liPoint[1])-1]).split(',')
                                print 'liList', liList
                                dicKey[liPoint[0]] = liList
                                print 'end 1'
                               
                            elif liPoint[1][0] == '{':
    
                                print 'dic',liPoint
                                liList = (liPoint[1][1:len(liPoint[1])-1]).split(',')
                                print 'liList', liList
                                liDics = {}
                                for list1 in liList:
                                    liD = list1.split(',')
                                    for dic in liD:
                                        dic_inv = dic.split(':')
                                        liDics[dic_inv[0]] = dic_inv[1]
                                        print liDics
                                dicKey[liPoint[0]] = liDics
                                print 'end 2'
                                
    
                            else:
                                print 'item',liPoint
    
                                dicKey[liPoint[0]] = liPoint[1]
                                print 'end 3'
                                
    
                        except Exception,params:
                            print Exception,params
                    
                    print 'DicKey ', dicKey
                    dicKeys.append(dicKey)
                dicUserModules[dicUser['Name']].append({key:dicKeys})
        
        dicModul = {}
        # User alle
        dicUserModules['AllUser'] = [{'all':{'Priv':'all'}},{'experimental':{'Priv':'all'}} ]
        dicUserModules['AllUser'].append({'staff':{'Priv':'all'}})
        #dicExt1 = {'extendet_gpl':[{'Priv':'all','MenuItem':{'Main':'data','Sub':'Extendet1'},'Imports':['cuon.Ext1.ext1','cuon.Ext1.ext2']}]}
        #dicExt1['extendet_gpl'][0]['MenuStart']='cuon.Ext1.ext1.ext1()'
        
        #dicUserModules['AllUser'].append(dicExt1)
        #print dicUserModules
        
        # User jhamel
##        dicUserModules['jhamel'] = [{'all':{'Priv':'all'}},{'experimental':{'Priv':'all'}} ]
##        dicUserModules['jhamel'].append({'staff':{'Priv':'all'}})
##        dicUserModules['jhamel'].append({'expert_system':{'Priv':'all'}})
##        dicUserModules['jhamel'].append({'project':{'Priv':'all'}})
##        dicUserModules['jhamel'].append({'forms':{'Priv':'all'}})
       
        #dicExt1 = {'extendet_gpl':[{'Priv':'all','MenuItem':{'Main':'data','Sub':'Extendet1', 'ExternalNumber':'ext1'},'Imports':['cuon.Ext1.ext1','cuon.Ext1.ext2']}]}
        #dicExt1['extendet_gpl'][0]['MenuStart']='cuon.Ext1.ext1.ext1()'
        
        
        #dicExt1['extendet_gpl'].append({'Priv':'all','MenuItem':{'Main':'action1','Sub':'hibernation1', 'ExternalNumber':'ext2'},'Imports':['cuon.Garden.hibernation','cuon.Garden.hibernation']})
       
        #dicExt1['extendet_gpl'][1]['MenuStart']='cuon.Garden.hibernation.hibernationwindow(self.allTables)'
        
        #dicExt1['extendet_gpl'].append({'Priv':'all','MenuItem':{'Main':'data','Sub':'botany1', 'ExternalNumber':'ext3'},'Imports':['cuon.Garden.botany','cuon.Garden.botany']})
        #dicExt1['extendet_gpl'][2]['MenuStart']='cuon.Garden.botany.botanywindow(self.allTables)'
        
        #dicUserModules[dicUser['Name']].append(dicExt1)
        
        
        
        
        # User cuon
        dicUserModules['cuon'] = [{'all':{'Priv':'all'}},{'experimental':{'Priv':'all'}} ]
        dicUserModules['cuon'].append({'staff':{'Priv':'all'}})
        dicExt1 = {'extendet_gpl':[{'Priv':'all','MenuItem':{'Main':'data','Sub':'Extendet1'},'Imports':['cuon.Ext1.ext1','cuon.Ext1.ext2']}]}
        dicExt1['extendet_gpl'][0]['MenuStart']='cuon.Ext1.ext1.ext1()'
        
        dicUserModules['cuon'].append(dicExt1)
        
        
        
        try:
           if dicUserModules.has_key(dicUser['Name']):
              dicModul = dicUserModules[dicUser['Name']]
           else:
              dicModul = dicUserModules['AllUser']
        except:
           pass
        print 'ModulesTest', dicUserModules
        return dicModul
        
    def xmlrpc_getClientInfo(self, dicUser):
        self.writeLog('Start1 execute py_getListsOfClients')
        
        dicUserClients = {}
        
        try:
            cpClients, f = self.getParser(self.CUON_FS + '/clients.ini')
            print 'f = ', f
            assert cpClients.get(dicUser['Name'], 'clientsRightCreate')
            assert cpClients.get(dicUser['Name'], 'clientsIDs')
            
            dicUserClients[dicUser['Name']]= {}
            dicUserClients[dicUser['Name']]['clientsRight']={}
            dicUserClients[dicUser['Name']]['clientsRight']['create']=cpClients.get(dicUser['Name'], 'clientsRightCreate')
            dicUserClients[dicUser['Name']]['clientsID']=cpClients.get(dicUser['Name'], 'clientsIDs').split(',')
            print 'dicUserClients-1', dicUserClients
        except Exception, params:
            print Exception, params
            dicUserClients['AllUser']={}
            dicUserClients['AllUser']['clientsRight']={}
            dicUserClients['AllUser']['clientsRight']['create']='NO'
            dicUserClients['AllUser']['clientsID']=[1]
            print 'dicUserClients-except', dicUserClients
        
        
        try:
           if dicUserClients.has_key(dicUser['Name']):
              dicClients = dicUserClients[dicUser['Name']]
           else:
              dicClients = dicUserClients['AllUser']
        except:
           pass
        self.writeLog('ListOfclients = ' + `dicClients`)
        
        return dicClients
    def xmlrpc_getDate(self, dicUser):
        #currentDate = time.localtime()
        #print currentDate
        #nD2 = datetime(currentDate[0], currentDate[1],currentDate[2],currentDate[3],currentDate[4], currentDate[5])
        dt = time.strftime(dicUser['DateformatString'])
        print dt
        return dt
        
    def xmlrpc_getStaffAddressString(self, dicUser):
        sSql = "select staff.lastname || ', ' || staff.firstname as address_string from staff where staff.cuon_username = '"
        sSql += dicUser['Name']  + "' " 
        sSql += self.getWhere('', dicUser, Single = 2)
        result = self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )
        if result not in ['NONE','ERROR']:
            return result[0]['address_string']
        else:
            return result
        
   
    def xmlrpc_setUserData(self,  dicUser): 
        # first check uuid 
        if isinstance(dicUser['client'], types.StringType):
            dicUser['client'] = int(dicUser['client'])
        try:
            if self.SEND_VERSION_INFO == 'YES':
                sSql = "select client_uuid from clients where id = " + `dicUser['client']`
                clientUUID =  self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )[0]['client_uuid']
                id, dicVersion = self.Database.xmlrpc_getLastVersion()
                print 'db client UUID ',  clientUUID,  len(clientUUID)
                if clientUUID and clientUUID not in self.liSQL_ERRORS and len(clientUUID.strip() ) == 36:
                    pass
                else:
                    clientUUID = str(uuid.uuid4())
                    sSql = "update clients set client_uuid = '" + clientUUID +"' where  id = " + `dicUser['client']`
                    self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )
                #response, content = http.request(self.home_url + clientUUID, 'GET', headers=headers)   
                sInfo = clientUUID +'###' +  md5.new(dicUser['Name']).hexdigest() + '###' + `dicVersion['Major']` +'.' + `dicVersion['Minor']` + '-' + `dicVersion['Rev']` + '###' + `dicUser['client']`
                
                try:
                    conn = httplib.HTTPConnection(self.home_url, 6080)
                    conn.request("GET", '/sayVersion/' +sInfo)
                    #r2 = conn.getresponse()
                    conn.close()
                except:
                    pass
        except:
            pass
        
        sSql = "select * from fct_setUserData( array " +`self.getUserInfo(dicUser)` + " )"
        print sSql
        return self.Database.xmlrpc_executeNormalQuery(sSql, dicUser )
        

    def xmlrpc_getOrderCashInfo(self, sType, dicUser):
        cp1,f = self.getParser('/etc/cuon/user.cfg')
        value = self.getConfigOption("ORDERTYPE", dicUser['Name'],cp1)
        if value:
            if value.upper() == "ORDERCASH" + sType[len(sType)-1:]:
                return True
            else:
                return False
        else:
            return False
            
    def xmlrpc_getOrderCashDeskCustomer(self, cashDesk, dicUser):
        cp1,f = self.getParser('/etc/cuon/clients.ini')
        cd_id = self.getConfigOption('CLIENT_' + `dicUser['client']`,'modul_order_default_customer_' + cashDesk, cp1)
        if cd_id and int(cd_id)>0:
	    return int(cd_id)
	else:
	    return 0
	
    def xmlrpc_getOrderCashDeskPrinter(self, cashDesk, dicUser):
        sCashDesk = "exe_print_cash" +  cashDesk[9:]
        sPrinter = self.xmlrpc_getSinglePreferences(sCashDesk,None,dicUser )
        print "OrderCashPrinter = ", sPrinter
        newPrinter = "NONE"

        try:
            newPrinter = sPrinter[0][sCashDesk]
        except:
            pass
        if not newPrinter:
            newPrinter = "NONE"
                
        return newPrinter




    def setDicUserKeys(self, dKey, sKey):
        self.dicUserKeys[dKey] = sKey
                
    def xmlrpc_getUserKeys(self, sModul, dicUser):
        self.dicUserKeys = {}

        cp1,f = self.getParser('/etc/cuon/user.cfg')
        keys = self.getConfigOption(dicUser['Name'], "KEYS_" + sModul, cp1)
        if keys:
	   liKeys = keys.split("&")
           for key_pair in liKeys:
               liKey = key_pair.split(",")
               
               self.setDicUserKeys(liKey[0],liKey[1])

        else:
            if sModul == "Address":

               #Address
               self.setDicUserKeys('address_edit','e')
               self.setDicUserKeys('address_delete','d')
               self.setDicUserKeys('address_new','n')
               self.setDicUserKeys('address_save','s')
               self.setDicUserKeys('address_print','p')


               self.setDicUserKeys('address_partner_edit','e')
               self.setDicUserKeys('address_partner_delete','d')
               self.setDicUserKeys('address_partner_new','n')
               self.setDicUserKeys('address_partner_print','p')
               self.setDicUserKeys('address_partner_save','s')



        return  self.dicUserKeys

           
	    
	    
    def xmlrpc_checkCashDeskPin(self, short_key,pin,  dicUser):
        
        cp1,f = self.getParser('/etc/cuon/user.cfg')
        cd_id = self.getConfigOption(dicUser['Name'],short_key, cp1)
        if cd_id and cd_id.strip() == pin.strip():
	    return short_key
	else:
	    return "NONE"

    def xmlrpc_checkSpecialCashDeskUser(self,dicUser):
        bOK = False
        cp1,f = self.getParser('/etc/cuon/user.cfg')
        specialUser = self.getConfigOption(dicUser['Name'],"SpecialCashDeskUser", cp1)
        try:
            if specialUser and specialUser.strip().lower() == "yes":
                bOK = True
        except:
            bOK = False

        return bOK

    def setPrefs(self,dicUser):
        
        dicPrefs = self.xmlrpc_getPreferencesFromStandard(dicUser)
        print "dicPrefs = ", dicPrefs
        
        self.prefDMS['exe']['pdf'] = dicPrefs[0]["exe_pdf"]
        self.prefDMS['fileformat']['pdf']['executable'] = self.prefDMS['exe']['pdf']
        
    def getDMSPrefs(self,dicUser):
        self.setPrefs(dicUser)
        return  self.prefDMS
    
