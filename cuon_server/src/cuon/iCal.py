from icalendar import Calendar, Event,Alarm
import time
from datetime import datetime, timedelta
import random
import xmlrpclib
import Database
import Misc

from twisted.web import xmlrpc
 
from basics import basics

class iCal(xmlrpc.XMLRPC, basics):
    def __init__(self):
        basics.__init__(self)
        self.oDatabase = Database.Database()
        self.dicCal = {}

    def setDicCal(self,dicUser):
        
        misc = Misc.Misc()
        self.dicCal  = misc.getCalendarInfo(dicUser)
        

    def createUID(self):
        uid = ""
        if self.dicCal["CalendarCreate"] =="YES":
            s = ''

            n = random.randint(500000,1000000000)
            for i in xrange(27):
                ok = True
                while ok:
                    r = random.randint(65,122)
                    if r < 91 or r > 96:
                        ok = False
                        s = s + chr(r)

            s = s + `n`
            sTime = time.asctime(time.localtime())
            uid = sTime + ' ' + s
        return uid
    
    def getParsedDate(self, sDate):
        sValue = time.strftime(sDate, '%Y.%m.%d %H:%M')
        return sValue
    
    def createEvent(self, dicEvent, UID = None):
        
        event = Event()
        ok = True 
        if self.dicCal["CalendarCreate"] =="YES":
            #print ' start create event '
            try:
                if dicEvent.has_key('summary'):
                    event.add('summary', dicEvent['summary'])
                    #print 'Summary', dicEvent['summary']
                if dicEvent.has_key('dtstart'):
                    s = time.strptime(dicEvent['dtstart'], dicEvent['DateTimeformatString'])
                    event.add('dtstart', datetime(s[0],s[1],s[2],s[3],s[4],s[5]))
                    
                    #print 'dtstart', datetime(s[0],s[1],s[2],s[3],s[4],s[5])
                else:
                    ok = False
                    
                if dicEvent.has_key('dtend'):
                    s = time.strptime(dicEvent['dtend'], dicEvent['DateTimeformatString'])
                    event.add('dtend', datetime(s[0],s[1],s[2],s[3],s[4],s[5]))
                    #print 'dtend', datetime(s[0],s[1],s[2],s[3],s[4],s[5])
                if dicEvent.has_key('dtstamp'):
                    event.add('dtstamp', dicEvent['dtstamp'])
                    #print 'stamp',  dicEvent['dtstamp']
                if not UID:
                    dicEvent['uid'] = `dicEvent['id']` + '#### ' + self.createUID()
                else:
                    dicEvent['uid'] = UID

                event.add('uid',dicEvent['uid'])

                #print 'UID', dicEvent['uid']
                if dicEvent.has_key('priority'):
                    event.add('priority',dicEvent['priority'] )
                if dicEvent.has_key('location'):
                    event.add('location',dicEvent['location'] )
                if dicEvent.has_key('status'):
                    event.add('status',dicEvent['status'] )
                if dicEvent.has_key('description'):
                    event.add('description',dicEvent['description'] )    
                if dicEvent.has_key('valarm') and dicEvent['valarm'] == True:
                    #print 0
                    oAlarm=Alarm()
                    
                    #print 1, oAlarm
                    oAlarm.add('action',"DISPLAY")
                    #oAlarm.add('value').value='DURATION'
                    #print 2, oAlarm
                    total_minutes = dicEvent['trigger']
                    idays = int(total_minutes/86400)
                    ihours  = int( (total_minutes - idays*86400) / 3600)
                    iminutes = int (total_minutes - idays*86400 - ihours*3600 )
                    #print idays, ihours, iminutes
                    #oAlarm.add('trigger',timedelta(minutes=-total_minutes  ))
                    td = timedelta(days=-idays, hours=-ihours,minutes=-iminutes)
                    #print td
                                   
                    oAlarm.add('trigger',td )
                    
                    #print 3,oAlarm
                    event.add_component(oAlarm)
                    #print 4, event, oAlarm
            except Exception, param:
                print 'Except error 55'
                print Exception
                print param

                if not ok:
                    event = None
        return event
        
    def delCalendar(self,sName):
        if self.dicCal["CalendarCreate"] =="YES":
            newCal = self.createCal()
            self.writeCalendar(sName, newCal)
          
    def getCalendar(self, sName):
        Cal = None
        if self.dicCal["CalendarCreate"] =="YES":
           
            try:
                s = self.readCalendar(sName)
                Cal = Calendar.from_ical(s)
            except Exception, param:
                print 'Except error 77'
                #print Exception
                #print param
            if not Cal:
                Cal = self.createCal()
        return Cal
        
    def writeCalendar(self, sName, Cal):
        if self.dicCal["CalendarCreate"] =="YES":
            #print "writeCal = ", Cal
            try:
                f = open(self.ICALPATH + sName,'wb')
                f.write(Cal.to_ical() )
                f.close()
            except Exception, param:
                print 'Except error 88'
                print Exception
                print param  
        
        return True
        
    def readCalendar(self,sName):
        sCal = None
        if self.dicCal["CalendarCreate"] =="YES":
           
            f = None
            try:
                f = open(self.ICALPATH + sName)

                sCal = f.read()
                f.close()
            except Exception, param:

                print 'Except error by open iCal'
                print Exception
                print param
                if f:
                    print 'close f'
                    f.close()
                sCal = 'BEGIN:VCALENDAR\r\nPRODID:-//My calendar product//mxm.dk//\r\nVERSION:2.0\r\nBEGIN:VEVENT\r\nDTEND:20050404T100000Z\r\nDTSTAMP:20050404T001000Z\r\nDTSTART:20050404T080000Z\r\nPRIORITY:5\r\nSUMMARY:Python meeting about calendaring\r\nUID:20050115T101010/27346262376@mxm.dk\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n\n'
        #print "sCal by readCalendar = ", sCal
        return sCal
     
    def setCalendarValues(self,sName,firstRecord, dicUser, sUserKey, completeNew = False):
        self.setDicCal(dicUser)
        ok = False 
        if self.dicCal["CalendarCreate"] =="YES":
            
            dicEvent = self.getDicCal(firstRecord, dicUser,sUserKey)

            if dicEvent.has_key('staff_cuon_username') and len(dicEvent['staff_cuon_username']) > 0:
                #print 'create Calendar for schedul_staff', dicEvent['staff_cuon_username']
                #print dicEvent['staff_cuon_username'], len(dicEvent['staff_cuon_username'])
                sName = 'iCal_' + dicEvent['staff_cuon_username']


            Cal = self.getCalendar(sName)
            #print 'Cal', Cal
            newEvent = self.createEvent(dicEvent)

            if completeNew and newEvent:
            
                Cal.add_component(newEvent)
                ok = True
                self.writeCalendar(sName, Cal)

            else:
                Cal2 = self.createCal()
                for i in Cal.walk('VEVENT'):
                    #print 'i = ', i
                    if i.has_key('UID'):
                        #print 'uid = ', i['UID']
                        sSearch = `firstRecord['id']` +'####'
                        #print sSearch
                        if i['UID'][0:len(sSearch)] != sSearch:
                            #print 'uid not found'
                            Cal2.add_component(i)
                    else:
                        Cal2.add_component(i)

                #print 'newEvent = ' + `newEvent`
                if newEvent and ( (newEvent.has_key('status') and  newEvent['status'] != "CANCELLED") or not newEvent.has_key('status')) :
                    Cal2.add_component(newEvent)
                    ok = True
                self.writeCalendar(sName, Cal2)
                
        return ok

    def addEvent(self, sName, firstRecord, dicUser, completeNew = False):
        self.setDicCal(dicUser)
        ok = False 
        if self.dicCal["CalendarCreate"] =="YES":


            # calendar of the contakter
            ok = self.setCalendarValues(sName, firstRecord, dicUser,'User', completeNew)
            # calendar of the schedul staff
            ok = self.setCalendarValues(sName, firstRecord, dicUser,'schedul_staff', completeNew)



    ##            print 'Cal', Cal
    ##            Cal2 = self.createCal()
    ##            for i in Cal.walk('VEVENT'):
    ##                print 'i = ', i
    ##                if i.has_key('UID'):
    ##                    print 'uid = ', i['UID']
    ##                    sSearch = `firstRecord['id']` +'####'
    ##                    print  '--->' + i['UID'][0:len(sSearch)] + '<---', '+++' + sSearch + '+++'
    ##
    ##                    print sSearch
    ##                    if i['UID'][0:len(sSearch)] != sSearch:
    ##                        print 'uid not found'
    ##                        Cal2.add_component(i)
    ##                    else:
    ##                        print 'UID foiCal.pyund'
    ##                        print  i['UID'][0:len(sSearch)-1], sSearch
    ##                else:
    ##                    Cal2.add_component(i)
    ##                
    ##            newEvent = self.createEvent(dicEvent)
    ##            print 'newEvent = ' + `newEvent`
    ##            if newEvent:
    ##                Cal2.add_component(newEvent)
    ##                self.writeCalendar('iCal_' + dicEvent['staff_cuon_username'], Cal2)
    ##                ok = True

        return ok
    
    def getDicCal(self, firstRecord, dicUser, sUser):
        self.setDicCal(dicUser)
        ok = False
        dicCal1 = {}
        if self.dicCal["CalendarCreate"] =="YES":
            
            result = None
            self.writeLog('Start getDicCal')
            self.writeLog('getDicCal firstRecord = ' + `firstRecord`)
            if sUser == 'User':
                try:
                    sSql = " select partner_schedul.id as sch_id,staff.cuon_username as staff_cuon_username, staff.lastname as st_lastname, staff.firstname as st_firstname, address.id as adr_id, address.lastname as adr_lastname, address.firstname as adr_firstname, address.zip as adr_zip, address.country as adr_country, address.city as adr_city, "
                    sSql += " address.phone as adr_phone, adress.phone_handy as adr_mobilphone, partner.firstname as pa_firstname, partner.lastname as pa_lastname, partner.phone as pa_phone, partner.handy_phone as pa_mobilphone , " 
                    sSql += "alarm_notify as sch_alarm, alarm_days as sch_alarm_days, alarm_hours as sch_alarm_hours, alarm_minutes as sch_alarm_minutes, alarm_time as sch_alarm_time  "

                    sSql += "from address, partner, partner_schedul, staff where partner.id = " 
                    sSql += `firstRecord['partnerid']` + " and address.id = partner.addressid and partner.id = partner_schedul.partnerid and staff.cuon_username = partner_schedul.user_id and partner_schedul.id = " + `firstRecord['id']`
                    result = self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
                except Exception, params:
                    print ' Exception getDicCal sql'
                    #print Exception, params

            elif sUser == 'schedul_staff':
                try:
                    sSql = " select partner_schedul.id as sch_id,staff.cuon_username as staff_cuon_username, staff.lastname as st_lastname, staff.firstname as st_firstname, address.id as adr_id, address.lastname as adr_lastname, address.firstname as adr_firstname, address.zip as adr_zip, address.country as adr_country, address.city as adr_city , "
                    sSql += " address.phone as adr_phone, adress.phone_handy as adr_mobilphone, partner.firstname as pa_firstname, partner.lastname as pa_lastname, partner.phone as pa_phone, partner.handy_phone as pa_mobilphone , "
                    sSql += "alarm_notify as sch_alarm, alarm_days as sch_alarm_days, alarm_hours as sch_alarm_hours, alarm_minutes as sch_alarm_minutes, alarm_time as sch_alarm_time  "
                    sSql += " from address, partner, partner_schedul, staff where partner.id = " 
                    sSql += `firstRecord['partnerid']` + " and address.id = partner.addressid and partner.id = partner_schedul.partnerid and staff.id = partner_schedul.schedul_staff_id and partner_schedul.id = " + `firstRecord['id']`
                    result = self.oDatabase.xmlrpc_executeNormalQuery(sSql, dicUser)
                except Exception, params:
                    print 'Exception 2 getDicCal sql'
                    #print Exception, params



            
            #print 'firstRecord = ', firstRecord
            # Save TimeTransformation
            #dicCal1['DateTimeformatString'] = dicUser['DateTimeformatString']
            dicCal1['DateTimeformatString'] = self.DIC_USER['DateTimeFormatstring']
            sDate =  firstRecord['schedul_date']
            sDateEnd = firstRecord['schedul_date_end']
            #print 'result = ', result
            if result and result == 'NONE':
                result = None
            print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"    
            print result
            print "------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
            
            try:
                sTime = self.getTimeString( firstRecord['schedul_time_begin'])
                dicCal1['dtstart'] = sDate + ' ' + sTime


            except Exception, param:
                print 'Except error getDicCal 1'
                #print Exception
                #print param


                self.writeLog('1 Time error by ' + `firstRecord['schedul_time_begin']`)
            try:
                sTime = self.getTimeString( firstRecord['schedul_time_end'])
                if len(sDateEnd) > 0:

                    dicCal1['dtend'] = sDateEnd + ' ' + sTime
                else:
                    dicCal1['dtend'] = sDate + ' ' + sTime



            except Exception, param:
                print 'Except error getDicCal 2'
                #print Exception
                #print param

            try:
                dicCal1['description'] = ''
                for entry in self.dicCal["CalendarDescription"]:
                    print "diccal entry = ", entry
                    if result:
                        dicCal1['description'] += self.tryDecode(result[0][entry]) + ", "
                        

            except Exception, param:
                dicCal1['description'] = ' '
                print 'Except error getDicCal 3'
                #print Exception
                #print param

                self.writeLog('String error by ' + `firstRecord['short_remark']`)
            try:
                dicCal1['id'] = firstRecord['id']
                if result and result not in ['NONE','ERROR']:


                    dicCal1['location'] = result[0]['adr_lastname']+ ','+ result[0]['adr_country'] + '-' + result[0]['adr_zip'] + ' ' + result[0]['adr_city']
                    dicCal1['location'] = self.tryDecode(dicCal1['location'])
                #print 'location', dicCal1['location']

            except Exception, params:
                print 'Error by location'
                #print Exception, params

            try:
                dicCal1['summary'] = ""
                dicCal1['summary'] += firstRecord['notes']

                dicCal1['summary'] = self.tryDecode(dicCal1['summary'])


            except Exception, params:
                print 'Error by notes'
                print Exception, params
            try:

                if result and result not in ['NONE','ERROR']:
                    if firstRecord['process_status'] >= 0 and firstRecord['process_status'] <= 99:
                        dicCal1['status'] = "TENTATIVE"
                    elif firstRecord['process_status'] >= 100 and  firstRecord['process_status'] <= 800:
                    #dicCal1['status'] = `firstRecord['process_status']`
                        dicCal1['status'] = "CONFIRMED"
                    elif firstRecord['process_status'] >= 801:
                        dicCal1['status'] = "CANCELLED" 


                


            except Exception, params:
                print 'Error by status'
                print Exception, params

            #self.writeLog('dicCal1 = ' + `dicCal1`)

            try:
                if result:
                    dicCal1['staff_cuon_username'] = result[0]['staff_cuon_username'] 
            except Exception, param:
                print 'Except error getDicCal 4'
                #print Exception
                #print param

            try:
                if result and result not in ['NONE','ERROR']:
                    if result[0]['sch_alarm'] == 't':
                        print "alarm = true"
                        dicCal1['valarm'] = True
                        dicCal1['duration'] = 'DURATION'
                        if  result[0]['sch_alarm_days'] == "NONE":
                            alarm_days = 0
                        else:
                            alarm_days = int(result[0]['sch_alarm_days'])
                            
                        if  result[0]['sch_alarm_hours']  == "NONE":
                            alarm_hours =0
                        else:
                            alarm_hours = + int( result[0]['sch_alarm_hours'] )
                                             
                        if   result[0]['sch_alarm_minutes'] == "NONE":
                            alarm_minutes = 0
                        else:
                            alarm_minutes =  int(result[0]['sch_alarm_minutes'])
                            
                        minutes =alarm_days * 86400 + alarm_hours * 60  +  alarm_minutes
                        
                        
                        print "alarm minutes = ", minutes
                        dicCal1['trigger'] =minutes
            except Exception, param:
                  print 'Error by valarm'
                  print Exception, param

            print "############################################################################"              
            print "dicCal1 = ", dicCal1
            print "############################################################################"              
        return dicCal1     
    
    def createCal(self):
        s ='BEGIN:VCALENDAR\r\nPRODID:-//CUON\r\nVERSION:2.0\r\nEND:VCALENDAR\r\n\n'
       #'BEGIN:VCALENDAR\r\nPRODID:-//CUON\r\nVERSION:2.0\r\nBEGIN:VEVENT\r\nDTEND:20050404T100000Z\r\nDTSTAMP:20050404T001000Z\r\nDTSTART:20050404T080000Z\r\nPRIORITY:5\r\nSUMMARY:Python meeting about calendaring\r\nUID:20050115T101010/27346262376@mxm.dk\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n\n'
       
        Cal = Calendar.from_ical(s)
        return Cal
        
    def overwriteCal(self, sName, liRecords, dicUser):
        self.setDicCal(dicUser)
        if self.dicCal["CalendarCreate"] =="YES":
            Cal = self.createCal()
            for record in liRecords:
                dicEvent = self.getDicCal(record, dicUser)

                newEvent = self.createEvent(dicEvent)
                print 'newEvent = ' + `newEvent`
                if newEvent:
                    Cal.add_component(newEvent)

            self.writeCalendar(sName, Cal)

        

    def tryDecode(self, s):
        #print 'before:', s

        try:
            s = s.decode('utf-8')
        except:
            try:
                s = s.decode('latin-1')
            except:
                try:
                    s = s.decode('latin-2')
                except:
                    try:
                        s = s.decode('iso-8859-15')
                    except:
                        try:
                            s = s.decode('utf-7')
                        except:
                            try:
                                s = s.decode('CP-1250')
                            except:
                                try:
                                    v = s.encode('utf-8')
                                except Exception, params:
                                    print '-----> no decode method is functional, clean s '
                                    s = ''
                                    #print Exception, params

        #print len(s)
        #try:
        #    print 'after1:', s
        #except:
        #    pass

                                
##        try:
##            s = s.encode('utf-8')
##        except Exception, params:
##            print 'Error by encode'
##            print Exception,params
##        try:    
##            print 'after2:', s
##        except:
##            pass
##            
        return s
        
                
#i=iCal()
#dicEvent={}
#s = 'BEGIN:VCALENDAR\r\nPRODID:-//My calendar product//mxm.dk//\r\nVERSION:2.0\r\nBEGIN:VEVENT\r\nDTEND:20050404T100000Z\r\nDTSTAMP:20050404T001000Z\r\nDTSTART:20050404T080000Z\r\nPRIORITY:5\r\nSUMMARY:Python meeting about calendaring\r\nUID:20050115T101010/27346262376@mxm.dk\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n\n'
#dicEvent['dtstart']=datetime(2006,06,05,10,0,0)
#s2 = i.xmlrpc_addEvent(s,dicEvent)
#print 's2=', s2

