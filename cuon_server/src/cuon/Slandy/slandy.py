# coding=utf-8

##Copyright (C) [2011]  [Juergen Hamel, D-32584 Loehne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
import uuid
from twisted.web import xmlrpc
#from twisted.internet import defer
from twisted.internet.threads import deferToThread
from twisted.internet import reactor
from twisted.web import server
import GridUser


class UserServer( xmlrpc.XMLRPC ):
    
     def __init__(self):
        
        xmlrpc.XMLRPC.__init__(self)
        self.PORT = 9012
    
    

try:    
    port = int(sys.argv[1])
except:
    port = 0
print port

u = UserServer()
oUser = GridUser.GridUser()


u.putSubHandler('Slandy', oUser)

print 'Port = ',  u.PORT
reactor.listenTCP(u.PORT + port , server.Site(u))
reactor.run()
