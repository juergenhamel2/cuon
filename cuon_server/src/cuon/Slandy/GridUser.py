import uuid
import time,  datetime
import select
import socket
from xml.dom.minidom import parse, parseString
from twisted.internet.threads import deferToThread
from copy import deepcopy
import hashlib

from xmlValues import xmlValues

from twisted.web import xmlrpc

class GridUser(xmlrpc.XMLRPC,  xmlValues):

    def __init__(self):
        
        xmlValues.__init__(self)
        
        self.HOST = 'cuonsim2.de'    # The remote host
        self.PORT = 8338              # The same port as used by the server
        self.User = {}
        self.Block = False 
        self.chatMessages = ['AvatarUUID','AvatarName','Chat','IM', 'CurrentParcel','CurrentLocation', 'NearbyAvatar', 'FriendList','GroupList','NearByList','StatusChange']
        self.Msg = []
        self.Cut = '|'
    def xmlrpc_is_running(self):
        print 'test connect'
        return 42

    def createSocket(self, singleUser):
        singleUser['Socket'] = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        singleUser['Socket'].connect((self.HOST, self.PORT))
        singleUser['Socket'].settimeout( 0.3 )
        self.User[singleUser['UUID']] = singleUser

        #singleUser['Socket'].sendall('<Request>  <Command>Ping</Command>  <Arguments>    <!-- No arguments. -->  </Arguments></Request>')


    def cleanChatMessages(self, singleUser):
        for key in self.chatMessages:
            if key in  ['AvatarUUID', 'AvatarName']:
                singleUser[key] = None
            else:
                singleUser[key] = []
                
        return singleUser
        
    def xmlrpc_Login(self,  Firstname,  Lastname,  Password, Client=0,   Grid = 'SecondLife',  Location = 'Last', Sim=None, x=None,  y=None,  z=None):
        singleUser = {'UUID':str(uuid.uuid4()), 'Name':Firstname + '_' + Lastname,'Socket':None,  'Grid':Grid,  'Client':Client}
       
        
        singleUser = self.cleanChatMessages(singleUser)
        self.createSocket(singleUser)
       
        
        ok = True
        if Firstname != "NO" and Lastname != "NO":
            newLoginString = self.xmlString['Login'].replace('##Firstname', Firstname)
            newLoginString = newLoginString.replace('##Lastname', Lastname)
            newLoginString = newLoginString.replace('##Password', Password)
            #newLoginString = newLoginString.replace('##Password', hashlib.md5(hashlib.md5(Password).hexdigest() +":").hexdigest() )
            newLoginString = newLoginString.replace('##StartLocation', Location)
            if Sim:
                newLoginString = newLoginString.replace('##SimName', Sim)
                if x:
                    newLoginString = newLoginString.replace('##XCoord', x)
                else:
                    newLoginString = newLoginString.replace('##XCoord', '128')
                if y:
                    newLoginString = newLoginString.replace('##YCoord', y)
                else:
                    newLoginString = newLoginString.replace('##YCoord', '128')
                       
                if z:
                    newLoginString = newLoginString.replace('##ZCoord', z)
                else:
                    newLoginString = newLoginString.replace('##ZCoord', '128')
                       
            else:
                
                newLoginString = newLoginString.replace('##SimName', '')
                newLoginString = newLoginString.replace('##XCoord', '')
                newLoginString = newLoginString.replace('##YCoord', '')
                newLoginString = newLoginString.replace('##ZCoord', '')
            print 'len = ',  len(newLoginString )
            #singleUser['Socket'].setblocking(0)
    
            # Turn off for Testing
            #singleUser['Socket'].sendall(newLoginString)
            singleUser = {'UUID':"Test", 'Name':Firstname + '_' + Lastname,'Socket':None,  'Grid':Grid,  'Client':Client}
            
            #print 'send Tracking'
            #newString = self.xmlString['setAvatarTrackingState'].replace('##SetItTo', 'True')
            #singleUser['Socket'].sendall(newString )
            
            print 'newLoginString = ',  newLoginString
    #        time.sleep(2)
    #        sNewString = self.xmlString['tpTo'].replace('##x',x)
    #        sNewString = sNewString.replace('##y',y)
    #        sNewString = sNewString.replace('##z',z)
    #        sNewString = sNewString.replace('##z',z)
    #        sNewString = sNewString.replace('##SimName',Sim)
    #        singleUser['Socket'].sendall(sNewString)
            
    #        
        else:
            singleUser = {'UUID':"Test", 'Name':Firstname + '_' + Lastname,'Socket':None,  'Grid':Grid,  'Client':Client}
       
        return singleUser['UUID']
        
    def xmlrpc_Logout(self,  uuid):
        try:
            singleUser = self.User[uuid]
            
        
            singleUser['Socket'].sendall(self.xmlString['Logout'])
        except:
            pass
            
        
        
                
        return True
        
    def xmlrpc_setTracking(self, uuid,  bOK = True):
        singleUser = self.User[uuid]

        newString = self.xmlString['setAvatarTrackingState'].replace('##SetItTo', `bOK`)
        singleUser['Socket'].sendall(newString)

    def xmlrpc_sendLocal(self, uuid, msg, Channel,  Type):
        print 'sendLocal = ',  uuid, msg, Channel, Type
        singleUser = self.User[uuid]
        s1 = self.xmlString['sendLocal'].replace('##Msg',msg.encode('UTF-8'))
        s1 = s1.replace('##Channel',Channel)
        s1 = s1.replace('##Type',Type)
        print 'msg = ',  s1, singleUser['Name']
        #self.Msg.append([singleUser,s1 ])
        try:
           
            singleUser['Socket'].sendall(s1)
        except Exception,  params:
            print Exception,  params
            
        return True
        
        
    def xmlrpc_sendIM(self, uuid, msg, sendTo):
        
        singleUser = self.User[uuid]   
        s1 = self.xmlString['sendIM'].replace('##Msg',msg.encode('UTF-8'))
        s1 = s1.replace('##UUID',sendTo)
        singleUser['Socket'].sendall(s1)
        return True
        
         
    def xmlrpc_getFriendslist(self, uuid):
        print 'request for the Friendslist'
        singleUser = self.User[uuid]
        singleUser['Socket'].sendall(self.xmlString['getFriendsList'])
        return True
        
                                     
                                     
    def xmlrpc_test(self, data,  uuid):
        singleUser = self.User[uuid]
        
        ok = self.parseXml(data, singleUser)
        print singleUser
        return ok
   #        returnValues['Chat'] = "kkkkkkiiiiiii##;;##jdjdjdjdjdj"
#        return returnValues
#        

    def xmlrpc_get_alive(self, uuid):
        self.Block = True
        return  deferToThread(self.checkAlive, uuid)
        
        
    def checkAlive(self, uuid):
        print 'get alive starts with ',  uuid
        returnValues = {}

        try:
            singleUser = self.User[uuid]
            self.cleanChatMessages(singleUser)
            #singleUser['Socket'].sendall(self.xmlString['Pong']);
            #print 'singleuser by getAlive = ',  singleUser
            allDatas = []
            allData = ''
            pongData = ''
            data = None
            while True:
                while True:
                    
                    try:            
    
                        data = singleUser['Socket'].recv(1)
                    except:
                        data = None
                    
                    if not data or data == '\n':
                        #allData += data
                        break 
                    else:
                        allData += data 
        #                pongData += data
        #                if pongData.find("Ping")>= 0:
        #                    
        #                    print 'send Pong',  pongData
        #                    singleUser['Socket'].sendall(self.xmlString['Pong']);
        #                    pongData = ''
        #          
                if allData:
                    allDatas.append(allData)
                    allData = ''
                    
                if not data :
                    break
                
            print 'Received',singleUser['Name'],   allDatas
            #liData = allData.split('\n')
            #print liData
            #for oneData in liData:
            for allData in allDatas:
                singleUser, Value = self.parseXml(allData,  singleUser)
                #print 'newSingleUser = ',  singleUser
                
                if Value == 'Ping':
                    singleUser['Socket'].sendall(self.xmlString['Pong']);

            returnValues = {}
            
            if singleUser['Client'] == 1:
                for key in self.chatMessages:
                    if not singleUser[key]:
                        singleUser[key] = 'NONE'
                    returnValues[key] = ''
              
                    
                
                for key in self.chatMessages:
                    #print 'Key1 = ',  key
                    if singleUser[key] != 'NONE':
                        if key == 'Chat':
                            for msg in singleUser[key]:
                                #print 'msg = ',  msg
                                text_msg = datetime.datetime.strftime(datetime.datetime.today(),"%H:%M") + " " + msg['FromName'] +' '
                                if msg['ChatType'] == 'Normal':
                                    text_msg += 'says: '
                                elif msg['ChatType'] == 'Shout':
                                    text_msg += 'shouts: '
                                    
                                elif msg['ChatType'] == 'Whisper':
                                    text_msg += 'whispers: '
                                else:
                                    text_msg += 'says: '
                                #print 'Key2 = '
                                text_msg += msg['Message']
                                returnValues[key] += text_msg + '\n\n' + self.Cut 
                                #print 'Key3 = '
                        elif key == "IM":
                             for msg in singleUser[key]:
                                print 'msg = ',  msg
                                text_msg =  msg['UUID'] + msg['Name'] + "\n" + datetime.datetime.strftime(datetime.datetime.today(),"%H:%M") + " " 
                                
                                #print 'Key2 = '
                                text_msg += msg['Message']
                                returnValues[key] += text_msg + '\n\n' + self.Cut 
                        elif key == "CurrentParcel":
                            
                             for msg in singleUser[key]:
                                text_msg = ''
                                for oneKey in msg.keys():
                                    print 'oneKey = ',  oneKey
                                    if not msg[oneKey]:
                                        text_msg += oneKey + unichr(172)  + "NONE" + unichr(175) 
                                    else:
                                        text_msg += oneKey + unichr(172)  + msg[oneKey] + unichr(175) 
                                    print text_msg
                                returnValues[key] += text_msg + self.Cut 
                                 
                                 
                    else:    
                        returnValues[key] = singleUser[key]
                        #print 'Key5 = '
                    
                    #returnValues['rValue']=Value
                           
            else:
                
                returnValues = []
    
                for key in self.chatMessages:
                    if not singleUser[key]:
                        singleUser[key] = ['NONE']
              
                    
                returnValues = {}
                for key in self.chatMessages:
                    returnValues[key] = singleUser[key]
                    
                #returnValues['rValue']=Value
                            
           
                self.Block = False
        except Exception,  params:
            print Exception,  params
            returnValues = 'NONE'
            
        #print '#############################################################################'
        #print 'returnvalues  = ',  returnValues
        return  returnValues



    def parseXml(self,  oneData,  singleUser):
        replyType = 'NONE'
        try:
            
            oneXML = parseString(oneData)
            print oneXML.toxml()
            Reply = oneXML.getElementsByTagName('Reply')
           
            if Reply:        
                ChatMessage = {}
                #print Reply[0].nodeName
                replyType = Reply[0].firstChild.nodeValue.strip()
                print 'ReplyType = ',  replyType
                if replyType == 'Chat':
                    # local chat 
                    
                    
                    ChatMessage["Message"] = oneXML.getElementsByTagName('Message')[0].firstChild.nodeValue.strip()
                    #print Message
                    ChatMessage["Audible"] = oneXML.getElementsByTagName('Audible')[0].firstChild.nodeValue.strip()
                    ChatMessage["ChatType"] = oneXML.getElementsByTagName('ChatType')[0].firstChild.nodeValue.strip()
                    ChatMessage["Audible"] = oneXML.getElementsByTagName('Audible')[0].firstChild.nodeValue.strip()
                    ChatMessage["SourceType"] = oneXML.getElementsByTagName('SourceType')[0].firstChild.nodeValue.strip()
                    ChatMessage["FromName"] = oneXML.getElementsByTagName('FromName')[0].firstChild.nodeValue.strip()
                    ChatMessage["UUID"] = oneXML.getElementsByTagName('UUID')[0].firstChild.nodeValue.strip()
                    ChatMessage["OwnerUUID"] = oneXML.getElementsByTagName('OwnerUUID')[0].firstChild.nodeValue.strip()
                    ChatMessage["Distance"] = oneXML.getElementsByTagName('Distance')[0].firstChild.nodeValue.strip()
                    print 'add Message = ',ChatMessage["Message"]  
                    singleUser["Chat"].append(ChatMessage)
                    print 'add Message = 2'
                    
                    
                elif replyType == 'InstantMessage':
                    # IM 
                    
                    ChatMessage["Name"] = oneXML.getElementsByTagName('Name')[0].firstChild.nodeValue.strip()
                    ChatMessage["UUID"] = oneXML.getElementsByTagName('UUID')[0].firstChild.nodeValue.strip()
                    ChatMessage["Message"] = oneXML.getElementsByTagName('Message')[0].firstChild.nodeValue.strip()
                    ChatMessage["Type"] = oneXML.getElementsByTagName('Type')[0].firstChild.nodeValue.strip()
                    ChatMessage["SessionUUID"] = oneXML.getElementsByTagName('SessionUUID')[0].firstChild.nodeValue.strip()
                    print 'IM = ',  ChatMessage
                    singleUser["IM"].append(ChatMessage)
                    
                elif replyType == 'AvatarStatusChange':
                    
                    #singleUser['AvatarUUID'] = oneXML.getElementsByTagName('UUID')[0].firstChild.nodeValue.strip()
                    singleUser['Status'] = oneXML.getElementsByTagName('Status')[0].firstChild.nodeValue.strip()
                    #ChatMessage['AvatarName'] = oneXML.getElementsByTagName('Name')[0].firstChild.nodeValue.strip()
                    ChatMessage["Status"] = singleUser['Status'] 
                    ChatMessage["AvatarUUID"] = oneXML.getElementsByTagName('UUID')[0].firstChild.nodeValue.strip()
                    
                    singleUser["StatusChange"].append(ChatMessage)
                    print '#####################################################################################'
                    print 'avatarStatus = ', singleUser["StatusChange"] 
                    
                elif replyType == 'CurrentParcel':
                    
                    ChatMessage["Name"] = oneXML.getElementsByTagName('Name')[0].firstChild.nodeValue.strip()
                    #ChatMessage["Category"] = oneXML.getElementsByTagName('Category')[0].firstChild.nodeValue.strip()
                    ChatMessage["Description"] = oneXML.getElementsByTagName('Description')[0].firstChild.nodeValue.strip()

                    ChatMessage["Flags"] = oneXML.getElementsByTagName('Flags')[0].firstChild.nodeValue.strip()
                    ChatMessage["MusicURL"] = oneXML.getElementsByTagName('MusicURL')[0].firstChild.nodeValue.strip()
                    ChatMessage["MediaObscured"] = oneXML.getElementsByTagName('MediaObscured')[0].firstChild.nodeValue.strip()
                    ChatMessage["MusicObscured"] = oneXML.getElementsByTagName('MusicObscured')[0].firstChild.nodeValue.strip()
                    ChatMessage["PublicCount"] = oneXML.getElementsByTagName('PublicCount')[0].firstChild.nodeValue.strip()
                    ChatMessage["AllowUnverified"] = oneXML.getElementsByTagName('AllowUnverified')[0].firstChild.nodeValue.strip()
                    ChatMessage["AllowAnonymous"] = oneXML.getElementsByTagName('AllowAnonymous')[0].firstChild.nodeValue.strip()
                    ChatMessage["UUID"] = oneXML.getElementsByTagName('SnapshotUUID')[0].firstChild.nodeValue.strip()
                    ChatMessage["Status"] = oneXML.getElementsByTagName('Status')[0].firstChild.nodeValue.strip()
                    ChatMessage["TotalPrims"] = oneXML.getElementsByTagName('TotalPrims')[0].firstChild.nodeValue.strip()
                    #ChatMessage["Media"] = {}
                    #ChatMessage["Media"]["Description"] = oneXML.getElementsByTagName('Description')[0].firstChild.nodeValue.strip()
                    #ChatMessage["Media"]["AutoScale"] = oneXML.getElementsByTagName('AutoScale')[0].firstChild.nodeValue.strip()
                    #ChatMessage["Media"]["Height"] = oneXML.getElementsByTagName('Height')[0].firstChild.nodeValue.strip()
                    #ChatMessage["Media"]["Loop"] = oneXML.getElementsByTagName('Loop')[0].firstChild.nodeValue.strip()
                    #ChatMessage["Media"]["Type"] = oneXML.getElementsByTagName('Type')[0].firstChild.nodeValue.strip()
                    #ChatMessage["Media"]["URL"] = oneXML.getElementsByTagName('URL')[0].firstChild.nodeValue.strip()
                    #ChatMessage["Media"]["Width"] = oneXML.getElementsByTagName('Width')[0].firstChild.nodeValue.strip()
                    singleUser["CurrentParcel"].append(ChatMessage)
                    
                elif replyType == 'CurrentLocation':
                    
                    ChatMessage["SimName"] = oneXML.getElementsByTagName('SimName')[0].firstChild.nodeValue.strip()
                    ChatMessage["ImageUUID"] = oneXML.getElementsByTagName('ImageUUID')[0].firstChild.nodeValue.strip()
                    ChatMessage["X"] = oneXML.getElementsByTagName('X')[0].firstChild.nodeValue.strip()
                    ChatMessage["Y"] = oneXML.getElementsByTagName('Y')[0].firstChild.nodeValue.strip()
                    ChatMessage["Z"] = oneXML.getElementsByTagName('Z')[0].firstChild.nodeValue.strip()
                    
                    singleUser["CurrentLocation"].append(ChatMessage)
                    
                elif replyType == 'NearbyAvatar':
                   
                    ChatMessage["Name"] = oneXML.getElementsByTagName('Name')[0].firstChild.nodeValue.strip()
                    ChatMessage["UUID"] = oneXML.getElementsByTagName('UUID')[0].firstChild.nodeValue.strip()
                    ChatMessage["X"] = oneXML.getElementsByTagName('X')[0].firstChild.nodeValue.strip()
                    ChatMessage["Y"] = oneXML.getElementsByTagName('Y')[0].firstChild.nodeValue.strip()
                    ChatMessage["Z"] = oneXML.getElementsByTagName('Z')[0].firstChild.nodeValue.strip()
                    ChatMessage["Sex"] = oneXML.getElementsByTagName('Sex')[0].firstChild.nodeValue.strip()
                    ChatMessage["Distance"] = oneXML.getElementsByTagName('Distance')[0].firstChild.nodeValue.strip()
                    ChatMessage["Present"] = oneXML.getElementsByTagName('Present')[0].firstChild.nodeValue.strip()
                    singleUser["NearbyAvatar"].append(ChatMessage)
                    
                elif replyType == 'FriendsList':
                   
                    friends = oneXML.getElementsByTagName('Friend')
                    #print 'Friends = ',  friends.toxml()
                    for friend in friends:
                        Child = friend.firstChild
                        ChatMessage[Child.tagName] = Child.firstChild.nodeValue.strip()
                        Child = Child.nextSibling
                        ChatMessage[Child.tagName] = Child.firstChild.nodeValue.strip()
                        Child = Child.nextSibling
                        ChatMessage[Child.tagName] = Child.firstChild.nodeValue.strip()
                        
                        
                        singleUser["FriendList"].append(deepcopy(ChatMessage)) 
                        
                elif replyType == 'GroupList':
                   
                    groups = oneXML.getElementsByTagName('Group')
                    for group in groups:
                        Child = group.firstChild
                        ChatMessage[Child.tagName] = Child.firstChild.nodeValue.strip()
                        Child = Child.nextSibling
                        ChatMessage[Child.tagName] = Child.firstChild.nodeValue.strip()
                       
                        
                        
                        singleUser["GroupList"].append(deepcopy(ChatMessage)) 
                        
                    
        except Exception,  params:
            print 'parseXml = ',  Exception,  params
            
        #print oneXML.getElementsByTagName('Reply')[0].toxml()
        #print 'root = ',  oneXML.getChildren()
        return singleUser,  replyType
        
        
    
        
