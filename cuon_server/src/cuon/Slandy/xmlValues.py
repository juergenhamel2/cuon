

class xmlValues:
    def __init__(self):
        self.xmlString = {}
        self.xmlString['Pong'] ="<Request><Command>Pong</Command><Arguments></Arguments></Request>\n"
        self.xmlString['Login']= "<Request><Command>Login</Command><Arguments><FirstName>##Firstname</FirstName><LastName>##Lastname</LastName><Password>##Password</Password><StartLocation>##StartLocation</StartLocation><SimName>##SimName</SimName><X>##XCoord</X><Y>##YCoord</Y><Z>##ZCoord</Z><ClientName>Slandy</ClientName></Arguments></Request>\n"
        self.xmlString['LoginShort']= "<Request><Command>Login</Command><Arguments><FirstName>##Firstname</FirstName><LastName>##Lastname</LastName><Password>##Password</Password> <StartLocation>##StartLocation</StartLocation><SimName/><X/><Y/><Z/><ClientName/></Arguments></Request>\n"
        self.xmlString['Logout']= "<Request><Command>Logout</Command><Arguments></Arguments></Request>\n"
        
        self.xmlString['getFriendsList']= "<Request><Command>FriendsList</Command><Arguments></Arguments></Request>\n"

        self.xmlString['setAvatarTrackingState']= "<Request><Command>AvatarTrackingSetState</Command><Arguments><!-- Enable or disable tracking. --><Enabled>##SetItTo</Enabled></Arguments></Request>\n"
        
        self.xmlString['ImageSetFormat'] = "<Request><Command>ImageSetFormat</Command><Arguments><!-- The image format. --><Format>##IMAGE</Format></Arguments></Request>"
        
        self.xmlString['tpTo'] = "<Request><Command>TeleportGlobal</Command><Arguments><!-- The global X coordinate (int). --><X>##x</X><!-- The global Y coordinate (int). --><Y>##y</Y><!-- The global Z coordinate (int). --><Z>##z</Z><!-- The Sim name. --><SimName>##SimName</SimName></Arguments></Request>\n"


        self.xmlString['sendLocal'] = "<Request><Command>ChatSend</Command><Arguments><Message>##Msg</Message><Channel>##Channel</Channel><ChatType>##Type</ChatType></Arguments></Request>\n"
        
        
        self.xmlString['sendIM'] = "<Request><Command>InstantMessageSend</Command><Arguments><UUID>##UUID</UUID><Message>##Msg</Message></Arguments></Request>\n"



"""Avatar Status Change
--------------------

Sent whenever an avatar changes from being online to offline (or vice versa). 

<Response>
  <Reply>
    AvatarStatusChange
  </Reply>
  <Fields>
    <!-- The name of the avatar changing state. -->
    <Name>
      Avatar Name
    </Name>
    <!-- The UUID of the avatar changing state. -->
    <UUID>
      Avatar UUID
    </UUID>
    <!-- The new state of the avatar. -->
    <Status>
      Online|Offline
    </Status>
  </Fields>
</Response>

Chat
----

Sent whenever a message is heard within chat. 

<Response>
  <Reply>
    Chat
  </Reply>
  <Fields>
    <!-- The message text being received. -->
    <Message>
      Message
    </Message>
    <!-- The audibility level of the message. -->
    <Audible>
      Barely|Fully|Not
    </Audible>
    <!-- The chat type of the message. -->
    <ChatType>
      Normal|Shout|Whisper
    </ChatType>
    <!-- The type of source sending the message. -->
    <SourceType>
      Agent|Object|System
    </SourceType>
    <!-- The name of the source sending the message. -->
    <FromName>
      From Name
    </FromName>
    <!-- The UUID of the source sending the message. -->
    <UUID>
      Source UUID
    </UUID>
    <!-- The UUID of the owner of the source sending the message. -->
    <OwnerUUID>
      Source Owner UUID
    </OwnerUUID>
    <!-- The distance of the message sender from us (in meters). -->
    <Distance>
      Meters
    </Distance>
  </Fields>
</Response>

Error
-----

Sent whenever an error is encountered. The error message can be fairly verbose. It's up to the client how the message is displayed or dealt with. 

<Response>
  <Reply>
    Error
  </Reply>
  <Fields>
    <!-- The error text. -->
    <Error>
      Error text
    </Error>
  </Fields>
</Response>

Friendship Offer
----------------

Sent when an agent offers friendship to us. 

<Response>
  <Reply>
    FriendshipOffer
  </Reply>
  <Fields>
    <!-- The name of the agent offering friendship. -->
    <Name>
      Agent Name
    </Name>
    <!-- The UUID of the agent offering friendship. -->
    <UUID>
      Agent UUID
    </UUID>
    <!-- The ID of the session in which friendship is being offered. -->
    <SessionUUID>
      Session ID
    </SessionUUID>
  </Fields>
</Response>

Friendship Offer Response
-------------------------

Sent when an agent responds to our offer of friendship. 

<Response>
  <Reply>
    FriendshipOfferResponse
  </Reply>
  <Fields>
    <!-- The name of the agent to which the friendship request is being responded to. -->
    <Name>
      Agent Name
    </Name>
    <!-- The UUID of the agent to which the friendship request is being responded to. -->
    <UUID>
      Agent UUID
    </UUID>
    <!-- The decision of the friendship response. -->
    <Accepted>
      True|False
    </Accepted>
  </Fields>
</Response>

Friends List
------------

Sent when a friends list is requested. There may be 0..* <Friend> nodes. 

<Response>
  <Reply>
    FriendsList
  </Reply>
  <Fields>
    <!-- A friend. -->
    <Friend>
      <!-- The name of the friend. -->
      <Name>
        FriendName
      </Name>
      <!-- The UUID of the friend. -->
      <UUID>
        FriendUUID
      </UUID>
      <!-- The status of the friend. -->
      <Status>
        Online|Offline
      </Status>
    </Friend>
  </Fields>
</Response>

Instant Message
---------------

Sent when an instant message is received. 

<Response>
  <Reply>
    InstantMessage
  </Reply>
  <Fields>
    <!-- The name of the sender. -->
    <Name>
      Name
    </Name>
    <!-- The UUID of the sender. -->
    <UUID>
      UUID
    </UUID>
    <!-- The message received. -->
    <Message>
      Message
    </Message>
    <!-- The message type. -->
    <Type>
      Online|Offline
    </Type>
    <!-- The session UUID. In the case of group chats, this is the group ID. If this is all zeroes, it indicates a message from Second Life. -->
    <SessionUUID>
      The session ID
    </SessionUUID>
  </Fields>
</Response>

Search Avatar
-------------

Sent upon an avatar search returning. There may be 0..* <Result> fields. 

<Response>
  <Reply>
    AvatarSearchResult
  </Reply>
  <Fields>
    <!-- A search result. -->
    <Result>
      <!-- The name of the search result. -->
      <Name>
        Avatar Name
      </Name>
      <!-- The UUID of the search result. -->
      <UUID>
        Avatar UUID
      </UUID>
    </Result>
  </Fields>
</Response>

Teleport Offer
--------------

Sent when an agent offers us teleportation. 

<Response>
  <Reply>
    TeleportOffer
  </Reply>
  <Fields>
    <!-- The name of the agent offering the teleportation. -->
    <Name>
      Agent Name
    </Name>
    <!-- The UUID of the agent offering the teleportation. -->
    <UUID>
      Agent UUID
    </UUID>
    <!-- The message accompanying the teleporation offer. -->
    <Message>
      Teleportation Message
    </Message>
  </Fields>
</Response>

Typing Status Change
--------------------

Sent when an agent's typing status changes. 

<Response>
  <Reply>
    TypingStatusChange
  </Reply>
  <Fields>
    <!-- The name of the avatar whose typing status has changed. -->
    <Name>
      Avatar Name
    </Name>
    <!-- The UUID of the avatar whose typing status has changed. -->
    <UUID>
      Avatar UUID
    </UUID>
    <!-- The avatar's new typing status. -->
    <TypingStatus>
      Typing|NotTyping
    </TypingStatus>
  </Fields>
</Response>

Disconnect
----------

This is sent when the client disconnect. 

<Response>
  <Reply>
    Disconnect
  </Reply>
  <Fields>
    <!-- The reason for the disconnection. -->
    <Reason>
      ClientInitiated|NetworkTimeout|ServerInitiated|SimShutdown
    </Reason>
    <!-- The message sent with the disconnection. -->
    <Message>
      Message text
    </Message>
  </Fields>
</Response>

Group Invitation Offer
----------------------

This is sent when a group invite is received. 

<Response>
  <Reply>
    GroupInvitationOffer
  </Reply>
  <Fields>
    <!-- The UUID of the inviting group. -->
    <GroupUUID>
      Group UUID
    </GroupUUID>
    <!-- The name of the inviting agent. -->
    <AgentName>
      Agent Name
    </AgentName>
    <!-- The name of the inviting group. -->
    <GroupName>
      Group Name
    </GroupName>
    <!-- The UUID of the session in which the invitation was sent. -->
    <SessionUUID>
      Session UUID
    </SessionUUID>
  </Fields>
</Response>

Message Box
-----------

Sent whenever the client should display a message that would be displayed in a message box within Second Life. It's up to the client how it is displayed. 

<Response>
  <Reply>
    MessageBox
  </Reply>
  <Fields>
    <!-- The message text. -->
    <Message>
      Message text
    </Message>
    <!-- The severity. -->
    <Severity>
      ALERT|ERROR|INFORMATIONAL
    </Severity>
  </Fields>
</Response>

Inventory Offer
---------------

Sent when an item of inventory is offered. 

<Response>
  <Reply>
    InventoryOffer
  </Reply>
  <Fields>
    <!-- The name of the sender. -->
    <Name>
      Name
    </Name>
    <!-- The UUID of the sender. -->
    <UUID>
      UUID
    </UUID>
    <!-- The name of the item being offered. -->
    <ItemName>
      Item name
    </ItemName>
    <!-- The message type. -->
    <Type>
      Online|Offline
    </Type>
  </Fields>
</Response>

Group List
----------

Sent when a group list is requested. There may be 0..* <Group> nodes. 

<Response>
  <Reply>
    GroupList
  </Reply>
  <Fields>
    <!-- A group. -->
    <Group>
      <!-- The name of the group. -->
      <Name>
        Group Name
      </Name>
      <!-- The UUID of the group. -->
      <UUID>
        Group UUID
      </UUID>
    </Group>
  </Fields>
</Response>

Group Members
-------------

Sent when a list of group members is requested. There may be 0..* <Member> nodes. 

<Response>
  <Reply>
    GroupMembers
  </Reply>
  <Fields>
     <!-- The UUID of the group. -->
    <GroupUUID>
      Group UUID
    </GroupUUID>
    <Member>
      <!-- The name of the member. -->
      <Name>
        Member Name
      </Name>
      <!-- The UUID of the member. -->
      <UUID>
        Member UUID
      </UUID>
      <!-- The status of the member. -->
      <Status>
        Online|Last Seen Date
      </Status>
    </Member>
  </Fields>
</Response>

Group Message
-------------

Sent when a group message is received. 

<Response>
  <Reply>
    GroupMessage
  </Reply>
  <Fields>
    <!-- The name of the sending agent. -->
    <Name>
      Agent Name
    </Name>
    <!-- The UUID of the sending agent. -->
    <UUID>
      Agent UUID
    </UUID>
    <!-- The message. -->
    <Message>
      Message Text
    </Message>
    <!-- The UUID of the group. -->
    <GroupUUID>
      Group UUID
    </GroupUUID>
  </Fields>
</Response>

Session Members
---------------

Sent when a list of session members is requested. There may be 0..* <Member> nodes. 

<Response>
  <Reply>
    SessionMembers
  </Reply>
  <Fields>
     <!-- The UUID of the session. -->
    <SessionUUID>
      Session UUID
    </SessionUUID>
    <Member>
      <!-- The name of the member. -->
      <Name>
        Member Name
      </Name>
      <!-- The UUID of the member. -->
      <UUID>
        Member UUID
      </UUID>
      <!-- Whether the member is a moderator. -->
      <IsModerator>
        True|False
      </IsModerator>
      <!-- Whether the member can voice chat. -->
      <CanVoiceChat>
        True|False
      </CanVoiceChat>
      <!-- Whether the member's text is muted. -->
      <IsTextMuted>
        True|False
      </IsTextMuted>
      <!-- Whether the member's voice is muted. -->
      <IsVoiceMuted>
        True|False
      </IsVoiceMuted>
    </Member>
  </Fields>
</Response>

Group Notice
------------

Sent when a group notice is received. 

<Response>
  <Reply>
    GroupNotice
  </Reply>
  <Fields>
    <!-- The name of the sending agent. -->
    <AgentName>
      Agent Name
    </AgentName>
    <!-- The UUID of the group. -->
    <GroupUUID>
      Group UUID
    </GroupUUID>
    <!-- The subject. -->
    <Subject>
      Subject Text
    </Subject>
    <!-- The message. -->
    <Message>
      Message Text
    </Message>
  </Fields>
</Response>

Session Member Join
-------------------

Sent when a member joins a session. 

<Response>
  <Reply>
    SessionMemberJoin
  </Reply>
  <Fields>
    <!-- The UUID of the session (group UUID). -->
    <SessionUUID>
      The UUID of the session (group UUID).
    </SessionUUID>
    <!-- The UUID of the member. -->
    <MemberUUID>
      Member UUID
    </MemberUUID>
    <!-- The name of the member. -->
    <MemberName>
      Member Name
    </MemberName>
  </Fields>
</Response>

Session Member Leave
--------------------

Sent when a member leaves a session. 

<Response>
  <Reply>
    SessionMemberLeave
  </Reply>
  <Fields>
    <!-- The UUID of the session (group UUID). -->
    <SessionUUID>
      The UUID of the session (group UUID).
    </SessionUUID>
    <!-- The UUID of the member. -->
    <MemberUUID>
      Member UUID
    </MemberUUID>
    <!-- The name of the member. -->
    <MemberName>
      Member Name
    </MemberName>
  </Fields>
</Response>

Group Search
------------

Sent when a group search result is returned. There may be 0..* <Group> nodes. A single node with default/blank values will be returned if no results were found. You may receive multiple responses of this type for a single search as packets can only be so large on the Second Life network, it is up to the client how it deals with multiple packets of results, but the search UUID is included to allow stitching of results. 

<Response>
  <Reply>
    GroupSearch
  </Reply>
  <Fields>
    <!-- The UUID associated with the query. Used to relate the result to the query. -->
    <QueryUUID>
      Query UUID
    </QueryUUID>
    <!-- A group. -->
    <Group>
      <!-- The name of the group. -->
      <Name>
        Name
      </Name>
      <!-- The UUID of the group. -->
      <UUID>
        UUID
      </UUID>
    </Group>
  </Fields>
</Response>

Group Search Start
------------------

Sent when a group search is started. This is so that the client can relate the result to the query. 

<Response>
  <Reply>
    GroupSearchStart
  </Reply>
  <Fields>
    <!-- The UUID associated with the query. -->
    <QueryUUID>
      Query UUID
    </QueryUUID>
    <!-- The query string. -->
    <QueryString>
      Query String
    </QueryString>
  </Fields>
</Response>

Event Search Start
------------------

Sent when an event search is started. This is so that the client can relate the result to the query. 

<Response>
  <Reply>
    EventSearchStart
  </Reply>
  <Fields>
    <!-- The UUID associated with the query. -->
    <QueryUUID>
      Query UUID
    </QueryUUID>
    <!-- The query string. -->
    <QueryString>
      Query String
    </QueryString>
    <!-- The category being searched. -->
    <Category>
      All|Arts|Charity|Commercial|Discussion|Education|Games|LiveMusic|Miscellaneous|NightLife|Pageants|Sport
    </Category>
    <!-- Whether to include mature results. -->
    <Mature>
      True|False
    </Mature>
  </Fields>
</Response>

Event Search
------------

Sent when an event search result is returned. There may be 0..* <Event> nodes. A single node with default/blank values will be returned if no results were found. You may receive multiple responses of this type for a single search as packets can only be so large on the Second Life network, it is up to the client how it deals with multiple packets of results, but the search UUID is included to allow stitching of results. 

<Response>
  <Reply>
    EventSearch
  </Reply>
  <Fields>
    <!-- The UUID associated with the query. Used to relate the result to the query. -->
    <QueryUUID>
      Query UUID
    </QueryUUID>
    <!-- An event. -->
    <Event>
      <!-- The name of the event. -->
      <Name>
        Name
      </Name>
      <!-- The IDof the event (not a UUID!) -->
      <ID>
        ID
      </ID>
      <!-- The event time (UNIX timestamp). -->
      <Time>
        Event time
      </Time>
      <!-- The event date (SL time). -->
      <Date>
        Event Date
      </Date>
      <!-- Whether the event is mature or not. -->
      <Mature>
        True|False
      </Mature>
    </Event>
  </Fields>
</Response>

Event Info
----------

Sent when info about an event is returned. 

<Response>
  <Reply>
    EventInfo
  </Reply>
  <Fields>
   <!-- The name of the event. -->
   <Name>
     Event name
   </Name>
   <!-- The event ID (not UUID!) -->
   <ID>
     Event ID (not UUID!)
   </ID>
   <!-- The Event category. -->
   <Category>
     All|Arts|Charity|Commercial|Discussion|Education|Games|LiveMusic|Miscellaneous|Nightlife|Pageants|Sport
   </Category>
   <!-- The event date (Second Life time). -->
   <Date>
     Second Life date
   </Date>
   <!-- The event date (UNIX time stamp). -->
   <DateUNIX>
     UNIX date stamp
   </DateUnix>
   <!-- The event description. -->
   <Description>
     Event description
   </Description>
   <!-- The event duration (in minutes). -->
   <Duration>
     Event duration (minutes).
   </Duration>
   <!-- The maturity flag. -->
   <Mature>
     True|False
   </Mature>
   <!-- The global X coordinate. -->
   <X>
     Global X coordinate.
   </X>
   <!-- The global Y coordinate. -->
   <Y>
     Global Y coordinate.
   </Y>
   <!-- The global Z coordinate. -->
   <Z>
     Global Z coordinate.
   </Z>
   <!-- The name of the Sim hosting the event (for teleportation). -->
   <SimName>
     Sim name hosting the event
   </SimName>
   <!-- Whether the event has a cover charge. -->
   <CoverCharge>
     0 = No charge, 1 = Charge?
   </CoverCharge>
   <!-- The amount the event costs. -->
   <Amount>
     Cover charge amount
   </Amount>
  </Fields>
</Response>

Nearby Avatar
-------------

Sent when information about a nearby avatar is received. 

<Response>
  <Reply>
    NearbyAvatar
  </Reply>
  <Fields>
    <!-- The avatar's name. -->
    <Name>
      Avatar Name
    </Name>
    <!-- The avatar's UUID. -->
    <UUID>
      Avatar UUID
    </UUID>
    <!-- The local X coordinate of the avatar. -->
    <X>
      Local X coordinate
    </X>
    <!-- The local Y coordinate of the avatar. -->
    <Y>
      Local Y coordinate
    </Y>
    <!-- The local Z coordinate of the avatar. -->
    <Z>
      Local Z coordinate
    </Z>
    <!-- The sex of the avatar. -->
    <Sex>
      MALE|FEMALE|UNKNOWN
    </Sex>
    <!-- The distance from the avatar (in meters). -->
    <Distance>
      Distance in meters
    </Distance>
    <!-- Indicate whether the avatar is present or not. -->
    <Present>
      True|False
    </Present>
</Fields>
</Response>

Current Location
----------------

Sent in response to a request for the current location of the avatar. 

<Response>
  <Reply>
    CurrentLocation
  </Reply>
  <Fields>
    <!-- The Sim name. -->
    <SimName>
      Avatar Name
    </SimName>
    <!-- The UUID of the current region's map image. -->
    <ImageUUID>
      Map image UUID
    </ImageUUID>
    <!-- The local X coordinate of the avatar. -->
    <X>
      Local X coordinate
    </X>
    <!-- The local Y coordinate of the avatar. -->
    <Y>
      Local Y coordinate
    </Y>
    <!-- The local Z coordinate of the avatar. -->
    <Z>
      Local Z coordinate
    </Z>
  </Fields>
</Response>

Avatar Tracking Can Set State
-----------------------------

Sent in response to a request for whether the avatar tracking state can be enabled or disabled. 

<Response>
  <Reply>
    AvatarTrackingCanSetState
  </Reply>
  <Fields>
    <!-- Whether a change is allowed. -->
    <Allowed>
      True|False
    </Allowed>
  </Fields>
</Response>

Avatar Tracking Set State
-------------------------

Sent in response to a request to change the avatar tracking state. 

<Response>
  <Reply>
    AvatarTrackingSetState
  </Reply>
  <Fields>
    <!-- Whether a change occurred. -->
    <Success>
      True|False
    </Success>
  </Fields>
</Response>

Avatar Profile
--------------

Sent in response to a request for an avatar's profile. There may be 0..* <Pick> nodes. 

<Response>
  <Reply>
    AvatarProfile
  </Reply>
  <Fields>
    <!-- The UUID of the avatar. -->
    <AvatarUUID>
      Avatar UUID
    </AvatarUUID>
     <!-- The name of the avatar. -->
    <AvatarName>
      Avatar Name
    </AvatarName>
    <!-- Collection of properties. -->
    <Properties>
      <!-- Profile about text. -->
      <AboutText>
        About text
      </AboutText
      <!-- Whether the profile may be published. -->
      <AllowPublish>
        True|False
      </AllowPublish>
      <!-- The date on which the profile was born. -->
      <BornOn>
        Date of birth
      </BornOn>
      <!-- Charter member. -->
      <CharterMember>
        Charter member
      </CharterMember>
      <!-- The UUID of the first life image. -->
      <FirstLifeImageUUID>
        UUID of first life image (all zeroes if none)
      </FirstLifeImageUUID>
      <!-- The first life text. -->
      <FirstLifeText>
        First life profile text
      </FirstLifeText>
      <!-- Profile flags. -->
      <Flags>
        AllowPublish, Identified, MaturePublish, Online, Transacted
      </Flags>
      <!-- Whether the profile is identified. -->
      <Identified>
        True|False
      </Identified>
      <!-- Whether the profile is mature. -->
      <IsMature>
        True|False
      </IsMature>
      <!-- Whether the avatar associated with the profile is online. -->
      <IsOnline>
        True|False
      </IsOnline>
      <!-- The UUID of the avatar's profile (all zeroes if none). -->
      <PartnerUUID>
        UUID of partner (all zeroes if none)
      </ParnerUUID>
      <!-- The Second Life image UUID (all zeroes if none). -->
      <SecondLifeImageUUID>
        UUID of second life image (all zeroes if none)
      </SecondLifeImageUUID>
      <!-- The URL in the profile. -->
      <ProfileURL>
        URL in profile
      </ProfileURL>
      <!-- Whether the profile has transacted. -->
      <HasTransacted>
        True|False
      </HasTransacted>
    </Properties>
    <!-- Interests-->
    <Interests>
      <!-- Languages entered by user. -->
      <Languages>
        Languages
      </Languages>
      <!-- Skills mask (from checkboxes). -->
      <SkillsMask>
        Mask of checked skills checkboxes
      </SkillsMask>
      <!-- Skills text. -->
      <SkillsText>
        Skills text
      </SkillsText>
      <!-- "Want to" mask (from checkboxes). -->
      <WantToMask>
        Mask of checked "want to" checkboxes
      </WantToMask>
      <!-- "Want to" text. -->
      <WantToText>
        "Want to" text
      </WantToText>
    </Interests>
    <!-- Collection of picks. -->
    <Picks>
      <!-- A pick. -->
      <Pick>
        <!-- The UUID of the pick. -->
        <UUID>
          Pick UUID
        </UUID>
        <!-- The name of the pick. -->
        <Name>
          Pick name
        </Name>
      </Pick>
    </Picks>
  </Fields>
</Response>

Image
-----

Sent in response to an image request. 

<Response>
  <Reply>
    Image
  </Reply>
  <Fields>
    <!-- The UUID of the image. -->
    <UUID>
      Image UUID
    </UUID>
    <!-- The base64 encoded image in the format requested. -->
    <Image>
      Base64 encoded image
    </Image>
    <Format>
      PNG|JPG|BMP...
    </Format>
  </Fields>
</Response>

Image Can Set State
-------------------

Sent in response to an image can set state request. 

<Response>
  <Reply>
    ImageCanSetState
  </Reply>
  <Fields>
    <!-- Whether the client can set the image sending state. -->
    <Allowed>
      True|False
    </Allowed>
  </Fields>
</Response>

Image Set Format
----------------

Sent in response to an image set format request. 

<Response>
  <Reply>
    ImageSetFormat
  </Reply>
  <Fields>
    <!-- Whether the format change was successful or not. -->
    <Success>
      True|False
    </Success>
    <!-- The current image format (after change, if any). -->
    <Format>
      PNG|JPG|BMP...
    </Format>
  </Fields>
</Response>

Image Set State
---------------

Sent in response to an image set state request. 

<Response>
  <Reply>
    ImageSetState
  </Reply>
  <Fields>
    <!-- Whether the state change was successful or not. -->
    <Success>
      True|False
    </Success>
  </Fields>
</Response>

Image State
-----------
Sent in response to an image state request. 

<Response>
  <Reply>
    ImageState
  </Reply>
  <Fields>
    <!-- Whether image requests are enabled or not. -->
    <Enabled>
      True|False
    </Enabled>
    <!-- The current image format (after change, if any). -->
    <Format>
      PNG|JPG|BMP...
    </Format>
  </Fields>
</Response>

Avatar Tracking State
---------------------

Sent in response to an avatar tracking state request. 

<Response>
  <Reply>
    AvatarTrackingState
  </Reply>
  <Fields>
    <!-- Whether avatar tracking is enabled or not. -->
    <Enabled>
      True|False
    </Enabled>
  </Fields>
</Response>

Current Parcel
--------------

Sent when details of the current parcel arrive. 

<Response>
  <Reply>
    CurrentParcel
  </Reply>
  <Fields>
    <!-- The name of the parcel. -->
    <Name>
      Parcel name
    </Name>
    <!-- The category of the parcel. -->
    <Category>
      Adult|Any|Arts|Business|Educational|Gaming|Hangout|Linden|Newcomer|None|Other|Park|Residential|Shopping|Stage|Unknown
    </Category>
    <!-- Parcel flags. -->
    <Flags>
      Parcel flags
    </Flags>
    <!-- Music URL -->
    <MusicURL>
      The URL for the streaming music
    </MusicURL>
    <!-- Whether media functionality is obscured. -->
    <MediaObscured>
      True|False
    </MediaObscured>
    <!-- Whether music functionality is obscured. -->
    <MusicObscured>
      True|False
    </MusicObscured>
    <!-- Public count -->
    <PublicCount>
      Public count
    </PublicCount>
    <!-- Whether unverified users are allowed. -->
    <AllowUnverified>
      True|False
    </AllowUnverified>
    <!-- Whether anonymous users are allowed. -->
    <AllowAnonymous>
      True|False
    </AllowAnonymous>
    <!-- UUID of the snapshot for the parcel (image). -->
    <SnapshotUUID>
      UUID of snapshot (picture).
    </SnapshotUUID>
    <!-- Parcel status. -->
    <Status>
      Abandoned|Leased|LeasePending|None|Unknown
    </Status>
    <!-- Total number of prims. -->
    <TotalPrims>
      Total prims
    </TotalPrims>
    <!-- Media info. -->
    <Media>
      <!-- Whether media is autoscaled. -->
      <AutoScale>
        True|False
      </AutoScale>
      <!-- Media description. -->
      <Description>
        Media description
      </Description>
      <!-- Media height. -->
      <Height>
        Media height
      </Height>
      <!-- Whether media loops. -->
      <Loop>
        True|False
      </Loop>
      <!-- Media mimetype. -->
      <Type>
        X/Y
      </Type>
      <!-- Media URL. -->
      <URL>
        Media URL
      </URL>
      <!-- Media width. -->
      <Width>
        Media width
      </Width>
    </Media>
  </Fields>
</Response>

Ping
----

Sent to ask the client for a pong response. 

<Response>
  <Reply>
    Ping
  </Reply>
  <Fields>
    <!-- No fields. -->
  </Fields>
</Response>

Balance Change
--------------

Sent when the agent's balance changes. 

<Response>
  <Reply>
    BalanceChange
  </Reply>
  <Fields>
    <!-- Message associated with the change. -->
    <Message>
      Message
    </Message>
  </Fields>
</Response>

Encryption Details
------------------
Sent when the server informs the client of encryption details. 

<Response>
  <Reply>
    EncryptionDetails
  </Reply>
  <Fields>
    <!-- The public key to be used when encrypting the session key. -->
    <PublicKey>
      The public key
    </PublicKey>
  </Fields>
</Response>

Friendship Termination
----------------------

Sent when a friendship is terminated with an agent. 

<Response>
  <Reply>
    FriendshipTerminated
  </Reply>
  <Fields>
    <!-- The name of the agent whose friendship has been terminated. -->
    <Name>
      Agent Name
    </Name>
    <!-- The UUID of the agent whose friendship has been terminated. -->
    <UUID>
      Agent UUID
    </UUID>
  </Fields>
</Response>

Clear nearby avatars
--------------------

Sent when the client should clear its collection of nearby avatars. 

<Response>
  <Reply>
    NearbyAvatarsClear
  </Reply>
  <Fields>
    <!-- No fields. -->
  </Fields>
</Response>

Balance
-------

Sent when a balance request is received. 

<Response>
  <Reply>
    Balance
  </Reply>
  <Fields>
    <!-- The balance in L$ (int). -->
    <Balance>
      100
    </Balance>
  </Fields>
</Response>

Session Join
------------

Sent when a request to join a session (group chat) is processed. 

<Response>
  <Reply>
    SessionJoin
  </Reply>
  <Fields>
    <!-- The UUID of the session. -->
    <SessionUUID>
      UUID
    </SessionUUID>
    <!-- The name of the session. -->
    <SessionName>
      Name
    </SessionName>
    <!-- The success state of the requested join. -->
    <Success>
      True|False
    </Success>
  </Fields>
</Response>

Avatar Profile Pick
-------------------

Sent when an avatar profile pick is resolved. 

<Response>
  <Reply>
    AvatarProfilePick
  </Reply>
  <Fields>
    <!-- The UUID of the pick. -->
    <UUID>
      Pick UUID
    </UUID>
    <!-- The name of the pick. -->
    <Name>
      Pick name
    </Name>
    <!-- The UUID of the pick creator. -->
    <CreatorUUID>
      The creator of the pick
    </CreatorUUID>
    <!-- The description of the pick. -->
    <Description>
      Pick description
    </Description>
    <!-- Whether the pick is enabled. -->
    <IsEnabled>
      True|False
    </IsEnabled>
    <!-- The original name of the pick (the pick may have been renamed by the creator). -->
    <OriginalName>
      The original name of the pick (before it was renamed, if at all)
    </OriginalName>
    <!-- The UUID Of the parcel housing the pick. -->
    <ParcelUUID>
      The UUID of the parcel housing the pick
    </ParcelUUID>
    <!-- The global X coordinate of the pick. -->
    <X>
      The global X coordinate of the pick
    </X>
    <!-- The global Y coordinate of the pick. -->
    <Y>
      The global Y coordinate of the pick
    </Y>
    <!-- The global Z coordinate of the pick. -->
    <Z>
      The global Z coordinate of the pick
    </Z>
    <!-- The name of the Sim housing the pick. -->
    <SimName>
      The name of the Sim housing the pick
    </SimName>
    <!-- The UUID of the snapshot (the pick picture). -->
    <SnapshotUUID>
      The UUID of the snapshot (the pick picture)
    </SnapshotUUID>
    <!-- The pick sort order. -->
    <SortOrder>
      The pick sort order
    </SortOrder>
    <!-- Whether this is a top pick. -->
    <TopPick>
      True|False
    </TopPick>
    <!-- The user this pick is associated with. -->
    <User>
      The user this pick is associated with
    </User>
  </Fields>
</Response>

TosUpdate
---------

Sent to inform the client of a ToS update.

<Response>
  <Reply>
    TosUpdate
  </Reply>
  <Fields>
    <!-- No fields. -->
  </Fields>
</Response>S


"""




"""
Key
---

This request is sent in response to an "EncryptionDetails" response. The Base64-RSA encrypted key
MUST be encrypted and encoded using the same algorithm as defined in "Crypto.cs -> EncryptRSA(...)".

<Key>Base64-RSA encrypted key triple DES</Key>

Login Request
-------------

This logs an avatar into the Second Life grid. 

Note that you should specify either a StartLocation OR a SimName and coordinate tuple. If both are specified, the StartLocation will take precedence. If neither are specified, an error will be returned to the client. If a valid StartLocation is specified, but an invalid SimName or coordinate tuple is specified, the login will still go ahead. 

<Request>
  <Command>Login</Command>
  <Arguments>
    <!-- The first name of the avatar. -->
    <FirstName>
      First Name
    </FirstName>
    <!-- The last name of the avatar. -->
    <LastName>
      Last Name
    </LastName>
    <!-- The password associated with the avatar. -->
    <Password>
      X
    </Password>
    <!-- Start location. -->
    <StartLocation>
      home|last
    </StartLocation>
    <!-- Sim name to start in. -->
    <SimName>
      Name of Sim
    </SimName>
    <!-- Local X coordinate within sim (int). -->
    <X>
      Local X coordinate
    </X>
    <!-- Local Y coordinate within sim (int). -->
    <Y>
      Local Y coordinate
    </Y>
    <!-- Local Z coordinate within sim (int). -->
    <Z>
      Local Z coordinate
    </Z>
    <ClientName>
	  The name of the client
	</ClientName>
  </Arguments>
</Request>

Logout Request
--------------

This logs out the currently logged-in avatar (if any). 

<Request>
  <Command>Logout</Command>
  <Arguments>
    <!-- No arguments. -->      
  </Arguments>
</Request>

Friends List
------------

This requests a list of all friends associated with the currently logged-in avatar. 

<Request>
  <Command>FriendsList</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Send Message
------------

This sends an instant message to an avatar. 

<Request>
  <Command>InstantMessageSend</Command>
  <Arguments>
    <!-- The UUID of the intended message recipient. -->
    <UUID>
      UUID
    </UUID>
    <!-- The message. -->
    <Message>
      I like cheese
    </Message>
  </Arguments>
</Request>

Search Avatar Request
---------------------

This searches for avatars with a given name. See Search Avatar Response for returned data. 

<Request>
  <Command>SearchAvatar</Command>
  <Arguments>
    <!-- The name of the avatar to search for (full name or partial). -->
    <Name>
      Some avatar
    </Name>
  </Arguments>
</Request>

Friendship Request
------------------

This requests the friendship of an avatar. 

<Request>
  <Command>FriendshipRequest</Command>
  <Arguments>
    <!-- The UUID of the avatar being offered friendship. -->
    <UUID>
      UUID
    </UUID>
    <!-- The message to send with the friendship request. -->
    <Message>
      Would you like to be friends?
    </Message>
  </Arguments>
</Request>

Friendship Accept
-----------------

This accepts a friendship offer from an avatar. 

<Request>
  <Command>FriendshipAccept</Command>
  <Arguments>
    <!-- The UUID of the avatar whom offered friendship. -->
    <UUID>
      UUID
    </UUID>
    <!-- The UUID of the session in which friendship was offered. -->
    <SessionUUID>
      UUID
    </SessionUUID>
  </Arguments>
</Request>

Friendship Decline
------------------

This declines a friendship from an avatar. 

<Request>
  <Command>FriendshipDecline</Command>
  <Arguments>
    <!-- The UUID of the avatar whom offered friendship. -->
    <UUID>
      UUID
    </UUID>
    <!-- The UUID of the session in which friendship was offered. -->
    <SessionUUID>
      UUID
    </SessionUUID>
    </Arguments>
</Request>

Chat
----

This is used to chat in public (or on any other channel). 

<Request>
  <Command>ChatSend</Command>
  <Arguments>
    <!-- Message text. -->
    <Message>
      Text
    </Message>
    <!-- Channel on which to chat. -->
    <Channel>
      0
    </Channel>
    <!-- Chat type (Normal|Whisper|Shout). -->
    <ChatType>
      Normal
    </ChatType>
  </Arguments>
</Request>

Teleport Accept
---------------
This is used to accept a teleportation offer from another agent. 

<Request>
  <Command>TeleportAccept</Command>
  <Arguments>
    <!-- The UUID of the agent offering teleportation. -->
    <UUID>
      UUID
    </UUID>
  </Arguments>
</Request>

Teleport Decline
----------------
This is used to decline a teleportation offer from another agent. 

<Request>
  <Command>TeleportDecline</Command>
  <Arguments>
    <!-- The UUID of the agent offering teleportation. -->
    <UUID>
      UUID
    </UUID>
  </Arguments>
</Request>

Typing Status Change
--------------------
This is used to indicate to Second Life that the client's typing status has changed (i.e. client is currently typing or not). 

<Request>
  <Command>TypingStatusChange</Command>
  <Arguments>
    <!-- The UUID of the intended message recipient. -->
    <UUID>
      UUID
    </UUID>
    <!-- The typing status (True|False). -->
    <Typing>
      True
    </Typing>
  </Arguments>
</Request>

Group Invitation Accept
-----------------------

This is used to accept a group invitation. 

<Request>
  <Command>GroupInvitationAccept</Command>
  <Arguments>
    <!-- The UUID of the inviting group. -->
    <GroupUUID>
      UUID
    </GroupUUID>
    <!-- The UUID of the session in which the invitation was offered. -->
    <SessionUUID>
      UUID
    </SessionUUID>
  </Arguments>
</Request>

Group Invitation Decline
------------------------

This is used to decline a group invitation. 

<Request>
  <Command>GroupInvitationDecline</Command>
  <Arguments>
    <!-- The UUID of the inviting group. -->
    <GroupUUID>
      UUID
    </GroupUUID>
    <!-- The UUID of the session in which the invitation was offered. -->
    <SessionUUID>
      UUID
    </SessionUUID>
  </Arguments>
</Request>

Group Leave
-----------

This is used to leave a group. 

<Request>
  <Command>GroupLeave</Command>
  <Arguments>
    <!-- The UUID of the group being left. -->
    <GroupUUID>
      UUID
    </GroupUUID>
  </Arguments>
</Request>

Group Join
----------

This is used to join a group. 

<Request>
  <Command>GroupJoin</Command>
  <Arguments>
    <!-- The UUID of the group being joined. -->
    <GroupUUID>
      UUID
    </GroupUUID>
  </Arguments>
</Request>

Group List
----------

This is used to request a list of groups that the agent is a member of. 

<Request>
  <Command>GroupList</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Group Members
-------------

This is used to list the members of a group. 

<Request>
  <Command>GroupMembers</Command>
  <Arguments>
    <!-- The UUID of the group containing the members to be listed. -->
    <GroupUUID>
      UUID
    </GroupUUID>
  </Arguments>
</Request>

Send Group Message
------------------

This sends an group message. 

<Request>
  <Command>GroupSendMessage</Command>
  <Arguments>
    <!-- The UUID of the group to receive the message. -->
    <GroupUUID>
      UUID
    </GroupUUID>
    <!-- The message. -->
    <Message>
      I like cheese
    </Message>
  </Arguments>
</Request>

Group Chat Join
---------------

Join a group chat session. 

<Request>
  <Command>GroupChatJoin</Command>
  <Arguments>
    <!-- The UUID of the group whose chat you wish to join. -->
    <GroupUUID>
      UUID
    </GroupUUID>
  </Arguments>
</Request>

Group Chat Leave
----------------

Leave a group chat session. 

<Request>
  <Command>GroupChatLeave</Command>
  <Arguments>
    <!-- The UUID of the group whose chat you wish to leave. -->
    <GroupUUID>
      UUID
    </GroupUUID>
  </Arguments>
</Request>

Session Members
---------------

List the members of a session. 

<Request>
  <Command>SessionMembers</Command>
  <Arguments>
    <!-- The UUID of the session whose members you wish to list. This is just the group UUID for group chats. -->
    <SessionUUID>
      UUID
    </SessionUUID>
  </Arguments>
</Request>

Group Search
------------

Used to search for a group. 

<Request>
  <Command>GroupSearch</Command>
  <Arguments>
    <!-- The query string to search for. -->
    <QueryString>
      Query String
    </QueryString>
  </Arguments>
</Request>

Event Search
------------

Used to search for an event. 

<Request>
  <Command>EventSearch</Command>
  <Arguments>
    <!-- The query string to search for. -->
    <QueryString>
      Query String
    </QueryString>
    <!-- The category to search within. -->
    <Category>
      All|Arts|Charity|Commercial|Discussion|Education|Games|LiveMusic|Miscellaneous|NightLife|Pageants|Sport
    </Category>
    <!-- Whether to include mature results. -->
    <Mature>
      True|False
    </Mature>
  </Arguments>
</Request>

Event Info
----------

Used to request event information. 

<Request>
  <Command>EventInfo</Command>
  <Arguments>
    <!-- The ID the event (not a UUID!) -->
    <EventID>
      12345
    </EventID>
  </Arguments>
</Request>

Teleport Global
---------------

Used to teleport using global coordinates and a Sim name. 

<Request>
  <Command>TeleportGlobal</Command>
  <Arguments>
    <!-- The global X coordinate (int). -->
    <X>
      1
    </X>
    <!-- The global Y coordinate (int). -->
    <Y>
      2
    </Y>
    <!-- The global Z coordinate (int). -->
    <Z>
      3
    </Z>
    <!-- The Sim name. -->
    <SimName>
      Sim Name
    </SimName>
  </Arguments>
</Request>

Teleport Home
-------------

Used to teleport home. 

<Request>
  <Command>TeleportHome</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Teleport Local
--------------

Used to teleport using local coordinates and a Sim name. 

<Request>
  <Command>TeleportLocal</Command>
  <Arguments>
    <!-- The local X coordinate (int). -->
    <X>
      1
    </X>
    <!-- The local Y coordinate (int). -->
    <Y>
      2
    </Y>
    <!-- The local Z coordinate (int). -->
    <Z>
      3
    </Z>
    <!-- The Sim name. -->
    <SimName>
      Sim name
    </SimName>
  </Arguments>
</Request>

Current Location
----------------

Used to request the avatar's current location. 

<Request>
  <Command>CurrentLocation</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Avatar Tracking Can Set State
-----------------------------

Used to determine whether the client is capable of changing the nearby avatar tracking state. 

<Request>
  <Command>AvatarTrackingCanSetState</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Avatar Tracking Set State
-------------------------

Used to change the nearby avatar tracking state. 

<Request>
  <Command>AvatarTrackingSetState</Command>
  <Arguments>
    <!-- Enable or disable tracking. -->
    <Enabled>
      True|False
    </Enabled>
  </Arguments>
</Request>

Teleport Lure
-------------

Used to offer a teleport to an agent. 

<Request>
  <Command>TeleportLure</Command>
  <Arguments>
    <!-- The UUID of the agent to offer teleportation to. -->
    <UUID>
      Agent UUID
    </UUID>
    <!-- The message to send with the lure. -->
    <Message>
      Lure message
    </Message>
  </Arguments>
</Request>

Avatar Profile
--------------

Used to request an avatar's profile. 

<Request>
  <Command>AvatarProfile</Command>
  <Arguments>
    <!-- The UUID of the avatar whose profile you wish to request. -->
    <UUID>
      Avatar UUID
    </UUID>
  </Arguments>
</Request>

Image
-----

This sends request for an image. 

<Request>
  <Command>Image</Command>
  <Arguments>
    <!-- The UUID of the image. -->
    <UUID>
      UUID
    </UUID>
  </Arguments>
</Request>

Image Can Set State
-------------------

Request whether we can set image state. 

<Request>
  <Command>ImageCanSetState</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Image Set Format
----------------

Set the desired image format. 

<Request>
  <Command>ImageSetFormat</Command>
  <Arguments>
    <!-- The image format. -->
    <Format>
      PNG|JPG|BMP...
    </Format>
  </Arguments>
</Request>

Image Set State
---------------

Set the image sending state. 

<Request>
  <Command>ImageSetState</Command>
  <Arguments>
    <!-- Enable or disable images. -->
    <Enabled>
      True|False
    </Enabled>
  </Arguments>
</Request>

Image State
-----------

Determine the server's image sending state. 

<Request>
  <Command>ImageState</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Avatar Tracking State
---------------------

Determine the avatar tracking state. 

<Request>
  <Command>AvatarTrackingState</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Autopilot
---------

Request autopilot to a destination within the current Sim. 

<Request>
  <Command>Autopilot</Command>
  <Arguments>
    <!-- The local X coordinate (int). -->
    <X>
      1
    </X>
    <!-- The local Y coordinate (int). -->
    <Y>
      2
    </Y>
    <!-- The local Z coordinate (int). -->
    <Z>
      3
    </Z>
  </Arguments>
</Request>

Autopilot Cancel
----------------

Cancel autopilot. 

<Request>
  <Command>AutopilotCancel</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Pong
----

This should be sent as soon as possible after receiving a Ping response from the server. It is used to determine if the client is still active. 

<Request>
  <Command>Pong</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Send Money
----------

This sends money to an agent. 

<Request>
  <Command>MoneySend</Command>
  <Arguments>
    <!-- The UUID of the intended money recipient. -->
    <UUID>
      UUID
    </UUID>
    <!-- The amount (int). -->
    <Amount>
      10
    </Amount>
  </Arguments>
</Request>

Send Money Group
----------------

This sends money to a group. 

<Request>
  <Command>MoneySendGroup</Command>
  <Arguments>
    <!-- The UUID of the intended money recipient. -->
    <UUID>
      Group UUID
    </UUID>
    <!-- The amount (int). -->
    <Amount>
      10
    </Amount>
  </Arguments>
</Request>

Home Set
--------

This attempts to set the agent's home location to the current location. 

<Request>
  <Command>HomeSet</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Friendship Terminate
--------------------

This terminates a friendship with another avatar. 

<Request>
  <Command>FriendshipTerminate</Command>
  <Arguments>
    <!-- The UUID of the avatar being whose friendship is being terminated. -->
    <UUID>
      UUID
    </UUID>
  </Arguments>
</Request>

Balance
-------

This requests the agent's balance. 

<Request>
  <Command>Balance</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Offline Messages
----------------

This requests offline messages from the server. 

<Request>
  <Command>OfflineMessages</Command>
  <Arguments>
    <!-- No arguments. -->
  </Arguments>
</Request>

Avatar Profile Pick
-------------------

This requests details of an avatar's profile pick. 

<Request>
  <Command>AvatarProfilePick</Command>
  <Arguments>
    <!-- The UUID of the avatar being whose profile pick is being requested. -->
    <avatarUUID>
      Avatar UUID
    </avatarUUID>
    <!-- The UUID of the pick being requested. -->
    <pickUUID>
      Pick UUID
    </pickUUID>
  </Arguments>
</Request>

AcceptTos
---------

This indicates the acceptance decision of the ToS.

<Request>
  <Command>AcceptTos</Command>
  <Arguments>
    <!-- The first name of the login. -->
    <FirstName>
      FirstName
    </FirstName>
    <!-- The last name of the login. -->
    <LastName>
      LastName
    </LastName>
    <!-- The acceptance decision. -->
    <Accept>
      true|false
    </Accept>
  </Arguments>
</Request>
"""
