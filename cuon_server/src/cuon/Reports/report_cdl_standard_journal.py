# -*- coding: utf-8 -*-
##Copyright (C) [2013]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 


import os
import types
from report_basics import report_basics
import locale
from locale import gettext as _
import time

class report_cdl_standard_journal(report_basics):
    def __init__(self):
        report_basics.__init__(self)
        
        self.dicReportData = {}
        self.dicResults = {}
        
        
        self.dicReportData['Title'] = _('Journal generatet by CUON')

        self.dicReportData['lPageNumber'] = _('Pagenumber:')
        self.dicReportData['fPageNumber'] = 1
        self.dicReportData['Designation'] = _('Designation')
        self.dicReportData['lOrderNumber'] = _('Order-Number:')
        
    
    
    def getReportData(self, dicSearchfields,sType, oOrder, reportDefs,  dicUser ):
        
        
        self.fileName = reportDefs['DocumentPathOrderInvoice'] + '/' +_('cash_desk-') + `time.mktime(time.localtime())` + '.pdf' 
        reportDefs['pdfFile'] = os.path.normpath(self.fileName)
        print '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*'
        #dicOrder['invoiceDate'] = oOrder.xmlrpc_getInvoiceDate(dicOrder['orderid'], dicUser)
        dicCDL = {}                                                                                                     
        dicResult = oOrder.xmlrpc_getCDLValues(dicSearchfields,sType, dicUser)
        #print "cdl = ", dicResult
      
        
        self.dicResults['cdl'] = dicResult
               
        dicResult = oOrder.xmlrpc_getCDLSum(dicSearchfields,sType, dicUser)
        #print "CDL-Sum = ", dicResult
        self.dicResults['cdl_sum'] = dicResult
          
        dicResult = oOrder.xmlrpc_getCDLMinusValues(dicSearchfields,sType, dicUser)
        #print "CDL-Sum = ", dicResult
        self.dicResults['cdl_minus_sum'] = dicResult
        

            # values in this order:
            # 1 reportname
            # 2 dicUser
            # 3 dicResults
            # 4 dicReportData
            # 5 reportDefs
        if sType == "AllValues":
            repDef = reportDefs['ReportPath'] + '/order_cash_desk_journal.xml'
        elif sType == "AllDaily":
            repDef = reportDefs['ReportPath'] + '/order_cash_desk_journalAllDaily.xml'

        elif sType == "AllUser":
            repDef = reportDefs['ReportPath'] + '/order_cash_desk_journalAllUser.xml'
        elif sType == "UserDaily":

            repDef = reportDefs['ReportPath'] + '/order_cash_desk_journalUserDaily.xml'

        elif sType == "CreditCard":

            repDef = reportDefs['ReportPath'] + '/order_cash_desk_journalCreditCard.xml'

        elif sType == "Stats":

            repDef = reportDefs['ReportPath'] + '/order_cash_desk_journalStats.xml'


        else:
            repDef = reportDefs['ReportPath'] + '/order_cash_desk_journal.xml'

        #print "dicResults ", self.dicResults
        #print "dicReportData ",  self.dicReportData
        #print "reportDefs ", reportDefs, repDef 
        return repDef , dicUser, self.dicResults, self.dicReportData, reportDefs
        
        
        
        
