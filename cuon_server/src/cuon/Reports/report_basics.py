import locale
from locale import gettext as _
import os


class report_basics:
    def __init__(self):
        pass
        
    def getPdfEncoding(self, value, ReportDefs):
        retValue = None
        try:
            retValue = (value.decode('utf-7')).encode(ReportDefs['PdfEncoding'])
            #print 'decode = utf-7', retValue
        except:
            try:
                retValue = (value.decode('iso-8859-15')).encode(ReportDefs['PdfEncoding']) 
                #print 'decode = iso-8859-15', retValue
            except:
                try:
                    retValue = (value.decode('utf-8')).encode(ReportDefs['PdfEncoding']) 
                    #print 'decode = utf-8', retValue


                 
                except Exception, params:
                    #print Exception, params
                    retValue = value
        return retValue

    def getPDFValues(self, dicResult, ReportDefs):
        for i in dicResult:
            for j in i.keys():
                if isinstance(i[j],  types.StringType):
                    i[j] = self.getPdfEncoding(i[j],reportDefs )

        return dicResult
            

    def normalizePath(self, sPath):
        sPath = sPath.replace("'","")
        sPath = sPath.replace(" ","_")
        sPath = os.path.normpath(sPath)

        return sPath
