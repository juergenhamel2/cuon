# -*- coding: utf-8 -*-

##Copyright (C) [2002]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

from twisted.web import xmlrpc
import os
import sys
import time
import random   
import xmlrpclib
import pg 
import string
import bz2
import base64
import types

from basics import basics

class SQL(xmlrpc.XMLRPC, basics):
    def __init__(self):
        basics.__init__(self)
        
    def xmlrpc_executeNormalQuery(self, cSql, dicUser={'Name':'zope', 'SessionID':'0'},ENCODE=True):
        print "Start ExecuteQuery"
        t1 = time.mktime(time.localtime())
        #self.writeLog('------->SQL starts at : ' + `t1`)
        conn = None
        dicResult = None
        rows = None
        if isinstance(dicUser, types.ListType):
            dicUser = self.list2Dic(dicUser)
        print "dicUser at executeNormalQuery", dicUser
        
        sUser = None
        try:
            self.writeLog('execute SQL1 = ' + `cSql`)
            self.writeLog('execute SQL User = ' + `dicUser`)
            if dicUser.has_key('Database') and dicUser['Database'] == 'osCommerce':
                pass
            else:
                #print "ex1"
                rows = None
                if not dicUser['Name'] or dicUser['Name'] == 'zope':
                    sUser = 'zope'
                    dicUser['noWhereClient'] = 'YES'
                    #print "1"
                elif dicUser.has_key('userType'):
                    sUser = self.checkUser(dicUser['Name'], dicUser['SessionID'], dicUser['userType'])
                    #print "2"
                else:
                    #print "User = ",  dicUser['Name'],  dicUser['SessionID'] 
                    sUser = self.checkUser(dicUser['Name'], dicUser['SessionID'])
                    #print "3",  sUser
                # put here sUser
                print 'sUser 0=', sUser
                #print "ex2"
                if not sUser:
                    sUser = 'zope'
                    dicUser['noWhereClient'] = 'YES'
                print"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
                print 'User 1= ',  sUser
                print"%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
                self.writeLog('execute SQL User = ' + sUser)
                self.writeLog('User = ' + sUser)
                #DSN = 'dbname=cuon host=localhost user=' + sUser
                print "connet to db with ",  sUser
                conn = pg.connect(dbname = 'cuon',host = self.POSTGRES_HOST, port = self.POSTGRES_PORT, user = sUser)
                #conn = pg.connect(dbname = 'cuon',host = self.POSTGRES_HOST, user = sUser)
                #curs.execute(cSql.decode('utf-8'))
                #conn = libpq.PQconnectdb(dbname='cuon',host = 'localhost', user = sUser)
                print "conn = ",  conn
                #self.writeLog('conn for session  = ' + `conn`)
                try:
                    if ENCODE:
                        
                        rows = conn.query(cSql.encode('utf-8'))
                    else:
                        rows = conn.query(cSql)
                    #self.writeLog('return from database = ' + `rows`)
                except:
                    rows = 'ERROR'
                    #rows = 'NONE'
                    
                #print 'rows = ', rows
                #print 'Sql-Execute = ', ok
                #conn.commit()
            ##        try:
            ##            rows = curs.dictfetchall()
            ##        except:
            ##            pass
                #self.writeLog('Rows = ' + `rows`)
                #conn.close()
        except Exception, params:
            self.writeLog(`Exception`)
            self.writeLog(`params`)
        try:
            if rows and rows not in ['NONE','ERROR']:
                try:
                    dicResult = rows.dictresult()
                except Exception, params:
                    self.writeLog('try dic-Result')
                    self.writeLog(`Exception`)
                    self.writeLog(`params`)
                    self.writeLog('----------------------------- dicResult could not be created, set to NONE --------------')
                    
                    dicResult = None
            else :
                dicResult = 'NONE'
            
            #self.writeLog('dic-Result 32' + `dicResult`)
            
            try:
                assert dicResult 
                if dicResult in ['NONE','ERROR']:
                    pass
                else:
                    #print 'dicResult', dicResult
                    sDecode = None
                    sEncode = None
                    if dicUser.has_key('Database'):
                        if dicUser['Database'] == 'osCommerce':
                            
                            sEncode = 'latin-1'
                        elif dicUser['Database'] == 'cuon':
                            sEncode = None
                            sDecode = None
                            
                    else:
                        sDecode = None
            
                   
                
                    for i in xrange(len(dicResult)):
                        for j in dicResult[i].keys():
                    
                            try:
                                if dicResult[i][j] == None:
                                    dicResult[i][j] = 'NONE'
                                if sDecode:
                                    dicResult[i][j]=dicResult[i][j].decode(sDecode)
                                if sEncode:
                                    dicResult[i][j]=dicResult[i][j].encode(sEncode)
                    
                            except:
                                pass
            except Exception, param:
                self.writeLog('Except-Error')
                self.writeLog(`Exception` +', \n' + `param`)
                
               
                dicResult = 'ERROR'
                #dicResult = 'NONE'
                
            if dicResult == None:
                dicResult ='NONE'
            #self.writeLog('sql return 2 = ' + `dicResult`)
        except Exception, param:
            self.writeLog( Exception)
            self.writeLog( param)
        try:
            #self.writeLog( '----------> SQL ')
            #self.writeLog(cSql)
            conn.close()
            #self.writeLog( '<-------SQL need : ' + ` time.mktime(time.localtime()) -t1` )
        except Exception, params:
            self.writeLog( 'executeQuery 3 ERROR = ' + `Exception` + ' ' + `params`)

        #self.writeLog('sql return = ' + `dicResult`)
            
        return dicResult
     
        
    def xmlrpc_getListEntries(self, dicEntries, sTable="", sSort="", sWhere="", bDistinct=False,  liFields=None, dicUser={}):
        print
        print
        print 'start xmlrpc_getListEntries for table ',sTable,liFields," -- dicEntries = ", dicEntries
        print
        print
        
        import string
        import time

        if isinstance(dicEntries, types.ListType):
            dicEntries = self.list2Dic(dicEntries)
            

        print "dicEntries = ", dicEntries
        
        if sWhere == None:
            sWhere = ''
        #print 'table = ', sTable
        #print "dicEntries ",  dicEntries
        
#        for cModul in self.liModules:
#            if not ok:
#                for li in self.dicLimitTables[cModul]['list']:
#                    if li == sTable:
#                        
#                        self.LIMITSQL = self.dicLimitTables[cModul]['limit']
        
        try:
            self.LIMITSQL = self.dicLimitTables[sTable]
        except:
            self.LIMITSQL = 100000
            
    
            
        #dicEntries['status'] = 'string'
        if bDistinct:
            sSql = 'select distinct '
        else:
            sSql = 'select '
        liTable = []
        try:    
            #print 'rep lace id with table.id'
            del dicEntries['id']
            dicEntries[sTable + '.id'] = 'int'
        except Exception, params:
            #print Exception, params
            dicEntries[sTable + '.id'] = 'int'
        findID = False
        for i in liFields:
            if i.lower()== "id":
                findID = True

        if not findID:
            liFields.append("id")
        print dicEntries
        print liFields
        try:
            for i in liFields:

                print "i in liFields ",  i
                print "0 ------------------------------------------------------------------"
                if i.lower().strip().find("fct_") > -1:
                    print "0.1 ------------------------------------------------------------------"
                    s1 = i[0:i.find("(")].strip()
                    s2 = i[i.lower().find(" as ")+4 :].strip()
                    dicEntries[s2]=self.FCT[s1]
                else:
                    if i.lower().strip().find(" as ") >-1:
                         print "0.2 ------------------------------------------------------------------"
                         s1 = i
                         s2 = i[i.lower().find(" as ")+4 :].strip()
                         dicEntries[s2]=i

                    else:
                        print "0.3 ------------------------------------------------------------------"
                        s1 = i
                        s2 = i

                if i == 'id':
                    i = sTable + '.id'
                    s1 = i
                    s2 = i
                print "1 ------------------------------------------------------------------"
                print  dicEntries, s2
                if dicEntries[s2] == 'date':
                    print "1.1 ------------------------------------------------------------------"
                    sSql = sSql + "to_char(" + i + ",  \'" + self.DIC_USER['SQLDateFormat'] + "\') as " + s2  + ', '
                elif dicEntries[s2] == 'time':
                    sSql = sSql + "to_char(" + i + ",  \'" + self.DIC_USER['SQLTimeFormat'] + "\') as " + s2  + ', '
                elif dicEntries[s2] == 'datetime':
                    sSql = sSql  + "to_char(" + i +", \'" + self.DIC_USER['SQLDateTimeFormat'] + "\') as " + s2  + ', '

                else:
                    print "1.3 ------------------------------------------------------------------"
                    sSql = sSql + i + ', '
                  
                print "sSql = " ,  sSql 

                print "i before find,  search for .",  i
                #print "s1, s2 before find,  search for .",  s1,  s2
                if s1.find('.') > 0:
                    liTable.append(s1[:s1.find('.')])


            print liTable
            sSql = sSql[0: string.rfind(sSql,',') ]
            #self.writeLog('sWhere =' + `sWhere`)

            sWhere = self.getWhere(sWhere, dicUser, Prefix=sTable + '.')



            sSql += ' from ' + sTable 
            #zComa = 0
            liTableNames = [sTable]
            for i in liTable:
                appendTablename = True
                for name in liTableNames:
                    if i == name:
                       appendTablename = False 
                if appendTablename:
                    sSql += ',' + i 
                    liTableNames.append(i)
                #zComa += 1
            #if zComa > 0:
            #    sSql = sSql[0: string.rfind(sSql,',') ]

            sSql +=  ' ' + sWhere + ' order by ' + sSort

            print "ssql 4 = ", sSql

            #self.writeLog(`sSql`)
            sSql = sSql + ' LIMIT ' + `self.LIMITSQL`
            #print 'self.LIMITSQL', self.LIMITSQL
            sSql = sSql.replace('specialsql1','where')
            print sSql
            #print dicUser
            result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
            #result2 = []
            #for li in result:
            #
            #    if string.strip(unicode(li['status'])) != unicode('delete'):
            #        result2.append(li)
            #        self.writeLog(string.strip(`li['status']`))




            #self.writeLog(repr(result))
            #print result
        except Exception, params:
            print Exception, params
            result=[]
        return result
        
    def xmlrpc_loadRecord(self, nameOfTable, record, dicColumns,  dicUser ):
        import string 
        sSql = 'select '
        
        for i in dicColumns.keys():
           if dicColumns[i] == 'date':
              sSql = sSql  + "to_char(" + i +", \'" + self.DIC_USER['SQLDateFormat'] + "\') as " +i  + ', '
           elif dicColumns[i] == 'time':
              sSql = sSql  + "to_char(" + i +", \'" + self.DIC_USER['SQLTimeFormat'] + "\') as " + i  + ', '
           elif dicColumns[i] == 'timestamp':
              sSql = sSql  + "to_char(" + i +", \'" + self.DIC_USER['SQLDateTimeFormat'] + "\') as " + i  + ', '
         
           else:
              sSql = sSql + i + ', ' 
        
        sSql = sSql[0: string.rfind(sSql,',') ]
        sSql = sSql + " from " + nameOfTable + " where id = " + `record`
        print "sSql by loadRecord = ",  sSql
        result = self.xmlrpc_executeNormalQuery(sSql,dicUser)
        
       
           
        return result
           
    def xmlrpc_saveRecord(self, sNameOfTable='EMPTY', id=0, dicValues ={}, dicUser={}, liBigEntries='NO'):       
        import string
        import types
        print sNameOfTable, id
        #print "dicUser= ", dicUser
        if isinstance(dicUser['client'], types.StringType):
            dicUser['client'] = int( dicUser['client'])
                      
        dicValues['client'] = [dicUser['client'], 'int']
##        if liBigEntries != 'NO':
##            for lb in liBigEntries:
##                sKey = dicUser['Name'] + '_' + lb
##                self.writeLog('sKey in Entries' + `sKey`)
##                dicValues[lb][0] = self.getValue(sKey)
##        
        print 'begin RECORD2 id = ' + `id`
        if id > 0:
            # update
            print '+++++++ start Update +++++++++++++++++++++++++'
            print "dicValues = ",dicValues

            sSql = 'update ' + sNameOfTable + ' set  '
            
            for i in dicValues.keys():
                liValue = dicValues[i]
                if liValue:
                    sSql = sSql + i
                    #print "I = ", i

                    #print liValue[1] , liValue[0]
                    #print
                    if liValue[1] == 'string' or liValue[1] == 'varchar':
                        sSql = sSql  + " = \'" + liValue[0]+ "\', "

                    elif liValue[1] in ['int', 'integer']:
                        sSql = sSql  + " =  " + `int(liValue[0])` + ", "

                    elif liValue[1] in ['float', 'double']:
                        try:
                            f1 = float(liValue[0])

                            sSql = sSql  + " = " + `f1` + ", "
                        except:
                            sSql  += " = " + `0` + ", "

                    elif liValue[1] == 'date':
                        if len(liValue[0]) < 6:
                            sSql = sSql  + " = NULL, "
                        else:

                            sSql = sSql  + " = \'" + liValue[0]+ "\', "
                    elif liValue[1] == 'time':
                        if len(liValue[0]) < 5:
                            sSql = sSql  + " = NULL, "
                        else:
                            sSql = sSql  + " = \'" + liValue[0]+ "\', "

                    elif liValue[1] ==  'bool':
                        self.writeLog('REC2-bool ')
                        if liValue[0] == 1:
                            liValue[0] = 'True'
                        if liValue[0] == 0:
                            liValue[0] = 'False'
                        sSql = sSql + " = " + liValue[0] + ", "
                    else:
                        sSql = sSql  + " = \'" + liValue[0]+ "\', "

            sSql = sSql[0:string.rfind(sSql,',')]
        
            sSql = sSql + ' where id = ' + self.convertTo(id, 'String')
            self.writeLog('Update SQL = ' + sSql)
            
        else:
            #self.writeLog('new RECORD2')
            sUUID = self.getNewUUID()
            sSql = "insert into  " + sNameOfTable + " ( uuid," 
                                                       
            sSql2 = "values ('"+ sUUID  + "',  "
            #self.writeLog('REC2-1 ' + `sSql` + `sSql2`)
            for i in dicValues.keys():
                sSql = sSql + i + ', '
                #self.writeLog('REC2-1.1 ' + `sSql`)
                liValue = dicValues[i]
                #self.writeLog('REC2-1.2 ' + `liValue`)
                print "recValue = " , i,  liValue
                if liValue == None or liValue == []:
                    sSql2 = sSql2 + "\'\', " 
                else:
                    if liValue[1] ==  'string' or liValue[1] == 'varchar':
                        if len(liValue[0]) == 0:
                            sSql2 = sSql2 + "\'\', " 
                        else:
                            sSql2 = sSql2  + "\'" + liValue[0] + "\', "
                        #self.writeLog('REC2-2 ' + `sSql` + `sSql2`)
                    elif liValue[1] == 'int':
                        if not liValue[0]:
                            liValue[0] = '0'
                        sSql2 = sSql2  + `int(liValue[0])` + ", "
                        #self.writeLog('REC2-3 ' + `sSql` + `sSql2`)
                    elif liValue[1] == 'float':
                        if not liValue[0]:
                            liValue[0] = '0.00'
                        sSql2 = sSql2  + `float(liValue[0])` + ", "
                        #self.writeLog('REC2-4 ' + `sSql` + `sSql2`)
                    elif liValue[1] == 'date':
                        if len(liValue[0]) < 6:
                            sSql2 = sSql2  +  " NULL, "
                        else:
                            sSql2 = sSql2  + " \'" + liValue[0] + "\', "
                            #self.writeLog('REC2-5 ' + `sSql` + `sSql2`)
                    elif liValue[1] == 'time':
                        if len(liValue[0]) < 5:
                            sSql2 = sSql2  +  " NULL, "
                        else:
                             sSql2 = sSql2  + " \'" + liValue[0] + "\', "
                            #self.writeLog('REC2-5 ' + `sSql` + `sSql2`)       
                    elif liValue[1] ==  'bool':
                        #self.writeLog('REC2-bool ')
                        if liValue[0] == 1:
                           liValue[0] = 'True'
                        if liValue[0] == 0:
                           liValue[0] = 'False'
        
                        sSql2 = sSql2  +"\'" + liValue[0] + "\', "
                        #self.writeLog('REC2-6 ' + `sSql` + `sSql2`)
                    else:
                        sSql2 = sSql2  +  " \'" + liValue[0] + "\', "
                        #self.writeLog('REC2-6 ' + `sSql` + `sSql2`) 
             
                        
            #self.writeLog('REC2-10 ' + `sSql` + '__' + `sSql2`) 
            sSql = sSql + 'id, user_id, status'
            sSql2 = sSql2 + 'nextval(\'' + sNameOfTable + '_id\'), current_user, \'create\''  
            
            # set brackets and add
            sSql = sSql + ') ' + sSql2 + ')'
        
        # execute insert
        #self.writeLog('First SQL by RECORD2 = ' + `sSql`)
        print sSql
        #if sNameOfTable == "dms":
        #    result = self.xmlrpc_executeNormalQuery(sSql, dicUser)
        #else:
            #print "b64 encode", base64.standard_b64encode(sSql)
        success = self.xmlrpc_executeNormalQuery("select * from fct_executeChecked('" + base64.standard_b64encode(sSql) +"')" , dicUser,False)
        #self.writeLog(result)
        
            
        
        
        # find last id 
        if id < 0:
        # insert
            try:
               # sSql = 'select last_value from ' + sNameOfTable + '_id'
               # result = self.xmlrpc_executeNormalQuery(sSql,dicUser)
               # id = result[0]['last_value']
               sSql = 'select id from ' + sNameOfTable + " where uuid = '" + sUUID + "'"
               result = self.xmlrpc_executeNormalQuery(sSql,dicUser)
               id = result[0]['id']
            except:
                id = 0
        
        # sSql = sSql[0:string.rfind(sSql,',')]
             
        print "id by save record = ",  id 
        return  id 
              
    def xmlrpc_saveRecordBE(self, sNameOfTable='EMPTY', id=0, dicValues ={}, dicUser={}):       
        for i in dicValues.keys():
            dicValuesBE = {}
            liValue = dicValues[i]
            try:
                bzValue = bz2.compress(liValue[0])
                dicValuesBE[i]=[bzValue,liValue[1]]
            except Exception, params:
                print Exception, params

        return self.xmlrpc_saveRecord(sNameOfTable, id, dicValuesBE, dicUser)
                
                
    def xmlrpc_createBigRow(self, sFile, data, j, dicUser=None):
        debug = 1
        ok = 1
        #self.writeLog('createBigRow reached', debug)
        #self.writeLog('first j = ' + `j`,debug)
        
        sKey = dicUser['Name'] +'_' +sFile
        
        #self.writeLog(sKey, debug)
        if j == 0:
            #self.writeLog('j = ' + `j`, debug)
            self.saveValue(sKey,data)
        else:
            #self.writeLog('j = ' + `j`, debug)
            sData =  self.getValue(sKey)
            if sData not in ['NONE','ERROR']:
                #self.writeLog('len sData = ' + `len(sData)`, debug)
                sData = sData + data
            else:
                sData = data
                
            
            self.saveValue(sKey,sData)
        
        return ok
        
    def xmlrpc_deleteRecord(self,nameOfTable = 'EMPTY', record = -1, dicUser=None):
        sSql = "delete from " + nameOfTable + " where id = " + `record`
        return self.xmlrpc_executeNormalQuery(sSql, dicUser)
    def xmlrpc_loadCompeteTable(self, nameOfTable, dicUser):
        sSql = "select * from " + nameOfTable
        return xmlrpc_executeNormalQuery(sSql, dicUser)


    
