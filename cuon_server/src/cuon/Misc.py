import time
from datetime import datetime
import random
import xmlrpclib
from xmlrpclib import ServerProxy
from twisted.web import xmlrpc
 
from basics import basics
import Database
import commands
import bz2
import base64
import types
import zipfile
   
import os.path
import shlex, subprocess


import locale
from locale import gettext as _

class Misc(xmlrpc.XMLRPC, basics):
    def __init__(self):
        basics.__init__(self)
        self.oDatabase = Database.Database()
        self.myHelpServer = self.getMyHelpServer()
        self.setDefaultValues2Database()
        
        
    def setDefaultValues2Database(self):
        
        for key in self.DIC_USER.keys():
            sSql = " select * from cuon_values where name = '" + key +"'"
            result = self.oDatabase.xmlrpc_executeNormalQuery(sSql)
            print result
            if result and result not in ['ERROR', 'NONE']:
                if isinstance(self.DIC_USER[key],  types.StringType):  
                    sSql = " update cuon_values  set type_c = '" + self.DIC_USER[key] + "' where name = '" + key + "'"
                    print sSql
                    result = self.oDatabase.xmlrpc_executeNormalQuery(sSql)
            else:
                 if isinstance(self.DIC_USER[key],  types.StringType):  
                    sSql = " insert into cuon_values  (id, name,  type_c) values ((select nextval('cuon_values_id')), '" + key + "', '" + self.DIC_USER[key] + "' )"
                    print sSql
                    result = self.oDatabase.xmlrpc_executeNormalQuery(sSql)    
                    
    def getMyHelpServer(self):
        """
        if the CUON_SERVER environment-variable begins with https,
        then the server use SSL for security.
        @return: Server-Object for xmlrpc
        """
        
        sv = None
        try:
            if self.ONLINE_BOOK[0:5] == 'https':
                #sv =  Server( self.td.server  , SSL_Transport(), encoding='utf-8')
                sv =  ServerProxy( self.ONLINE_BOOK,allow_none = 1 ) 
            else:
                sv = ServerProxy(self.ONLINE_BOOK)
                
        except:
            print 'Server error'
            
        
        return sv

    def xmlrpc_getHelpBook(self):
        #Server = xmlrpclib.ServerProxy(self.getHelpServer())

        #print self.getHelpServer()
        #print self.getHelpServer().getRPCVersionSupported()
        print 'Helpserver = ', self.myHelpServer
        
        s = self.myHelpServer.getPageHTML(u"Benutzerhandbuch")
        return s
        
    def xmlrpc_getListOfTOPs (self, dicuser):
        sSql = 'select id, number from terms_of_payment'
        sSql = sSql + context.sql.py_getWhere("",dicUser,1)
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        li = []
        if result not in ['NONE','ERROR']:
           for i in range(len(result)):
               li.append(result[i]['id'] + '    ' + result[i]['number'])
        
        return li
   
    def xmlrpc_getListOfTaxVat(self, dicUser):
        sSql = 'select vat_name from tax_vat'
        sSql += self.getWhere("",dicUser,1)
        sSql += ' order by id '
        print sSql 
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        li = []
        if result not in ['NONE','ERROR']:
           for i in range(len(result)):
               li.append(result[i]['vat_name'])
        
        return li
    def xmlrpc_getFormsAddressNotes(self,iType, dicUser):
        
        sSql = 'select title, id  from dms where insert_from_module = ' + `iType`
        sSql += self.getWhere('', dicUser, Single = 2)
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        liValues = []
        if result not in ['NONE','ERROR']:
            for value in result:
                liValues.append(value['title'] + '###' + `value['id']`)
                
                
        if not liValues:
            liValues = 'NONE'
        return liValues
    def xmlrpc_faxData(self, dicUser, faxdata, phone_number):
        ok = False
        sFaxPath = "/var/spool/cuon-fax"
        Faxserver = None
        Faxport = None
        Faxuser = None
        s = ''
        for i in phone_number:
            if i in ['0','1','2','3','4','5','6','7','8','9']:
                s += i
        phone_number = s
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/server.ini')
            #print cpServer
            #print cpServer.sections()
            
            Faxserver = self.getConfigOption('FAX','HOST', cpServer)
            Faxport = self.getConfigOption('FAX','PORT', cpServer)
            Faxuser = self.getConfigOption('FAX','USER', cpServer)
            self.writeLog('Faxserver = ' + Faxserver)
            
        except:
            pass
            
        self.writeLog( 'send Fax')
        
        filename = sFaxPath + '/fax___' + self.createNewSessionID()['SessionID'] 
        if filename:
            faxdata = base64.decodestring(faxdata)
            faxdata = bz2.decompress(faxdata)

            f = open(filename,'wb')
            f.write(faxdata)
            f.close()
            sSql = "select email from staff where cuon_username = '" +  dicUser['Name'] + "' "
            sSql += self.getWhere("",dicUser,2)
            result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
            sEmail = None
            if result and result not in ['NONE','ERROR']:
                sEmail = result[0]['email']
                
        if Faxserver and Faxport and Faxuser:
            self.writeLog( 'Faxserver found')
            if filename:
                shellcommand = 'scp -P ' + Faxport.strip() +' '  + filename + ' ' + Faxuser.strip() + '@' + Faxserver.strip() + ':/' +sFaxPath
                self.writeLog( shellcommand)
                liStatus = commands.getstatusoutput(shellcommand)
                self.writeLog( `liStatus`)
                # new Parameter
                # -D -R send email when all ok
                # -f emailaddress
                
                
                    
                shellcommand = 'ssh -p ' + Faxport.strip() +' ' + Faxuser.strip() + '@' + Faxserver.strip() +  ' "sendfax -n '
                if sEmail:
                    shellcommand += ' -f ' + sEmail
                shellcommand += ' -D -R -o ' + dicUser['Name'] + ' -d "' + phone_number + '" ' + filename + '"'
                self.writeLog(shellcommand)

                liStatus = commands.getstatusoutput(shellcommand)
                #shellcommand = 'ssh -p ' + Faxport.strip() + ' '  + Faxuser.strip() +'@' + Faxserver.strip() + ' "sendfax -n -o ' + dicUser['Name'] + ' -d \"' + phone_number + '\" ' + filename + ' "'
                #self.writeLog(shellcommand)
                #liStatus = commands.getstatusoutput(shellcommand)
                
                self.writeLog(`liStatus`)
                ok = True
        else:
            if filename:
                shellcommand = 'sendfax -n '
                if sEmail:
                    shellcommand += ' -f ' + sEmail
                shellcommand += ' -o ' + dicUser['Name'] + ' -d "' + phone_number + '" ' + filename
                liStatus = commands.getstatusoutput(shellcommand)
                print shellcommand
                print  liStatus
                ok = True
                #shellcommand = 'rm ' + filename
                #liStatus = commands.getstatusoutput(shellcommand)
                #print shellcommand, liStatus
        return ok 
        
    def xmlrpc_getForm(self, id, dicUser):
        sSql = "select * from dms where id = " + `id` 
        print sSql
        return self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        
    def xmlrpc_getNotes0ID(self, dicUser):
        value = 0
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()
            
            value = int(self.getConfigOption('CLIENT_' + `dicUser['client']`,'Notes0_ID', cpServer))
            
        except Exception, params:
            print 'Error by Notes0 ID Read client.cfg'
            print Exception, params
        print 'notes_0_id', value
        return value
    
    def xmlrpc_sendNotes0(self, dicUser,current_page = -1):
        if not isinstance(dicUser,types.DictType):
            newCur = dicUser
            dicUser = current_page
            current_page = newCur

        if isinstance( dicUser['client'], types.StringType) :
            dicUser['client'] = int(dicUser['client'])

            
        ok = False
        # For BGU
        if current_page == -1:
            current_page = 12
            
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()
            
            value = self.getConfigOption('CLIENT_' + `dicUser['client']`,'sendNotes0', cpServer)
            
            if value and (value == 'Yes' or value == 'YES' or value == 'yes'):
                value = self.getConfigOption('CLIENT_' + `dicUser['client']`,'Pages', cpServer)
                if value and int(value)>0:
                    if int(value)&(2**current_page) == (2**current_page):
                        print 'Notes are in Bitfield', current_page,2**current_page
                        value = self.getConfigOption('CLIENT_' + `dicUser['client']`,'sendNotes0Sender', cpServer)
                        if value and value.find(dicUser['Name']) >= 0:
                            ok = True
                
        except Exception, params:
            print 'Error by Schedul Read user.cfg'
            print Exception, params
        print 'current_page = ', current_page
        return ok 
        
    def xmlrpc_getAdditionalEmailAddressesNotes0(self, addressid, dicUser):
        value = None
        liAddresses = []
        
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()
            
            value = self.getConfigOption('CLIENT_' + `dicUser['client']`,'AdditinalEmailAddressesNotes0', cpServer)
            if value:
                liAddresses = value.split(',')
                
        except Exception, params:
            print 'Error by Schedul Read user.cfg'
            print Exception, params
        print 'notes_0_addEmailAddresses', value
        
        # configOption sendMailsNotes0: caller,rep,salesman
        value = None
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()
            
            value = self.getConfigOption('CLIENT_' + `dicUser['client']`,'sendMailsNotes0', cpServer)
            if value:
                liValues = value.split(',')
                if liValues:
                    for i in liValues:
                        result = None
                        if i.strip() == 'caller':
                            sSql = 'select staff.email as email from staff, address where  address.caller_id  = staff.id' 
                            sSql += ' and address.id = ' + `addressid`
                            result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                        elif i.strip() == 'rep':
                            sSql = 'select staff.email  as email from staff, address where  address.rep_id  = staff.id' 
                            sSql += ' and address.id = ' + `addressid`
                            result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                        elif i.strip() == 'salesman':
                            sSql = 'select staff.email  as email from staff, address where  address.salesman_id  = staff.id' 
                            sSql += ' and address.id = ' + `addressid`
                            result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                        if result and result not in ['NONE','ERROR']:
                            liAddresses.append(result[0]['email'].strip())
            
        except Exception, params:
            print 'Error by Schedul Read user.cfg'
            print Exception, params
        print 'notes_0_addEmailAddresses', value
        if not liAddresses:
            liAddresses = 'NONE'
                
        return liAddresses
        
    
    def xmlrpc_getEmailAddresses(self, sType, dicUser):
        value = None
        liAddresses = []
        
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()
            
            value = self.getConfigOption('CLIENT_' + `dicUser['client']`,sType, cpServer)
            if value:
                liAddresses = value.split(',')
                
        except Exception, params:
            print 'Error by Schedul Read user.cfg'
            print Exception, params
        
        
        # configOption sendMailsNotes0: caller,rep,salesman
        value = None
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
            #print cpServer
            #print cpServer.sections()
            
            value = self.getConfigOption('CLIENT_' + `dicUser['client']`,'sendMailsNotes0', cpServer)
            if value:
                liValues = value.split(',')
                if liValues:
                    for i in liValues:
                        result = None
                        if i.strip() == 'caller':
                            sSql = 'select staff.email as email from staff, address where  address.caller_id  = staff.id' 
                            sSql += ' and address.id = ' + `addressid`
                            result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                        elif i.strip() == 'rep':
                            sSql = 'select staff.email  as email from staff, address where  address.rep_id  = staff.id' 
                            sSql += ' and address.id = ' + `addressid`
                            result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                        elif i.strip() == 'salesman':
                            sSql = 'select staff.email  as email from staff, address where  address.salesman_id  = staff.id' 
                            sSql += ' and address.id = ' + `addressid`
                            result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
                        if result and result not in ['NONE','ERROR']:
                            liAddresses.append(result[0]['email'].strip())
            
        except Exception, params:
            print 'Error by Schedul Read user.cfg'
            print Exception, params
        print 'notes_0_addEmailAddresses', value
        if not liAddresses:
            liAddresses = 'NONE'
                
        return liAddresses
        
        
        
    def xmlrpc_dmsCheckPermissions(self,id,dicUser):
        bUser = False
        bGroup = False
        bAll = False
        allRights = False
        dicRights = {}
        dicRights['Ur'] = True
        dicRights['Uw'] = True
        dicRights['Ux'] = True
        dicRights['Gr'] = True
        dicRights['Gw'] = True
        dicRights['Gx'] = True
        dicRights['Ar'] = True
        dicRights['Aw'] = True
        dicRights['Ax'] = True
        
        groups = None
        liGroups = []
        # read configfile for group
        try:
                       
            cpServer, f = self.getParser(self.CUON_FS + '/user.cfg')
            #print cpServer
            #print cpServer.sections()
            
            groups = self.getConfigOption('GROUPS',dicUser['Name'], cpServer)
        
        except:
            pass
        if groups:
            liGroups = groups.split(',')
        
        sSql = 'select document_rights_activated, document_rights_user_read, document_rights_user_write, document_rights_user_execute, document_rights_group_read, document_rights_group_write, document_rights_group_execute, document_rights_all_read, document_rights_all_write, document_rights_all_execute, document_rights_user,  document_rights_groups from dms where id = ' + `id` 
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        if result and result not in ['ERROR','NONE']:
            for key in result[0].keys():
                if key == 'document_rights_groups' or key == 'document_rights_user':
                    pass
                else:
                    if result[0][key] == 't':
                        print 'Set True = ',key
                        result[0][key] = True
                    else:
                        print 'Set False = ',key
                        result[0][key] = False
            if result[0]['document_rights_activated']: 
                print 'Rights are activated'
            
                if result[0]['document_rights_user'] == dicUser['Name']:
                    print 'User equal'
                    bUser = True
            else:
                allRights = True
        else:
            allRights = True
            
            
        dicRights['Read'] = True
        dicRights['Write'] = True
        dicRights['Execute'] = True
   
        if not allRights:
            dicRights['Ur'] = result[0]['document_rights_user_read'] and bUser
            dicRights['Uw'] = result[0]['document_rights_user_write'] and bUser
            dicRights['Ux'] = result[0]['document_rights_user_execute'] and bUser
            if result[0]['document_rights_groups'] in liGroups:
                bGroup = True
                
            dicRights['Gr'] = result[0]['document_rights_group_read'] and bGroup
            dicRights['Gw'] = result[0]['document_rights_group_write'] and bGroup
            dicRights['Gx'] = result[0]['document_rights_group_execute'] and bGroup
            
            dicRights['Ar'] = result[0]['document_rights_all_read'] 
            dicRights['Aw'] = result[0]['document_rights_all_write'] 
            dicRights['Ax'] = result[0]['document_rights_all_execute'] 
            dicRights['Read'] = False
            dicRights['Write'] = False
            dicRights['Execute'] = False

            if dicRights['Ur'] or dicRights['Gr'] or dicRights['Ar']:
                dicRights['Read'] = True
            if dicRights['Uw'] or dicRights['Gw'] or dicRights['Aw']:
                dicRights['Write'] = True
        
            if dicRights['Ux'] or dicRights['Gx'] or dicRights['Ax']:
                dicRights['Execute'] = True


        #Workaround
        #dicRights['Read'] = False
                
        
        return dicRights
        
    

    def xmlrpc_saveDia(self, sType, dicData):
        print sType
        print dicData

        return 'Hallo'

    def xmlrpc_getTextExtract(self, id, sFileSuffix,  dicUser):
        s = None
        sReturn = False
        sText = None
        sInFile=None
        sOutFile = None
        
        imageData = None
        if sFileSuffix in ['pdf', 'txt', 'odt', 'html']:
                
            sSql = 'select document_image from dms where id = ' + `id`
            liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )
            if liResult:
                s = liResult[0]['document_image']
                try:
                    b = base64.decodestring(s)
                    imageData = bz2.decompress(b)
                except Exception, param:
                    print Exception, param
                    imageData = None
    
            if imageData:
                try: 
                    ratio = self.OTS_RATIO 
                    sOutFile = "/tmp/" + self.getNewUUID() 
                    sInFile = "/tmp/" + self.getNewUUID() 
                    f = open(sOutFile + '.' + sFileSuffix, "w")
                    f.write(imageData)
                    f.close()
                    #print "files = ",  sOutFile,  sInFile
                    shellcommand = shlex.split("/usr/share/cuon/cuon_server/bin/getOts.sh " +sFileSuffix+ " " +  `ratio` + " " + sInFile + " " + sOutFile)
                    liStatus = subprocess.call(shellcommand)
                    #print "ots command = ",  shellcommand, liStatus
                    
                    f = open(sInFile, "r")
                    sText = f.read()
                    f.close()
                    #print "s = ",  sReturn
                    #liStatus = commands.getstatusoutput(shellcommand)
                    #print shellcommand, liStatus
                except Exception,  param:
                    print 'write files'
                    print Exception,  param
                
            
                if sText:
                    try:
                        sText = sText.decode('utf-8')
                        #print 'utf-8'
                    except Exception,  param:
                        print 'try to decode'
                        print Exception,  param
                        
                    sReturn = self.xmlrpc_updateDmsExtract(id, sText, dicUser)
                
                # clean it
                if sOutFile:
                    try:
                        os.remove(sOutFile)
                    except:
                        pass
                    try:
                        os.remove(sOutFile + 'txt')
                    except:
                        pass
                if sInFile:
                    try:
                        os.remove(sInFile)
                    except:
                        pass
                    
        return sReturn


    def xmlrpc_updateDmsExtract(self,  id, sExtract, dicUser):
        #print 'update DMS = ',  id ,  sExtract
       
        liResult = False
#        try:
#            b = bz2.compress(sExtract)
#
#            imageData = base64.encodestring(b)
#        except Exception, param:
#            print Exception, param
#            imageData = None
            
       # print 'base64 codiert',  imageData
        if sExtract:
            
            sSql = "update dms set dms_extract = '" + sExtract + "' where id = " + `id`
            liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )
            
        return liResult
        
        
    def xmlrpc_getIdFromTitle(self, sTitle,  dicUser):
        id = 0
        sSql = "select id from DMS where title = '" + sTitle + "' " 
        sSql += self.getWhere("",dicUser,2)
        liResult = self.oDatabase.xmlrpc_executeNormalQuery( sSql, dicUser )
        
        if liResult not in self.liSQL_ERRORS:
            id = liResult[0]['id']
        
        return id
        
    
    def getCalendarInfo(self,dicUser):
        
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        if isinstance( dicUser['client'], types.StringType) :
            dicUser['client'] = int(dicUser['client'])
        dicCal = {}
        CalendarCreate =  self.getConfigOption('CLIENT_' + `dicUser['client']`,"CalendarCreate", cpServer)
        if CalendarCreate and CalendarCreate.upper() == "YES" :
             dicCal["CalendarCreate"] = "YES"
        else:
             dicCal["CalendarCreate"] = "NO"

        CalendarDescription =  self.getConfigOption('CLIENT_' + `dicUser['client']`,"CalendarDescription", cpServer)
        if CalendarDescription:
            dicCal["CalendarDescription"] = CalendarDescription.split(",")
            
        else:
            dicCal["CalendarDescription"] = []
            
        CalendarAlarm =  self.getConfigOption('CLIENT_' + `dicUser['client']`,"CalendarAlarm", cpServer)
        
        if CalendarAlarm and CalendarAlarm.upper() == "YES" :
             dicCal["CalendarAlarm"] = "YES"
        else:
             dicCal["CalendarAlarm"] = "NO"
             
        CalendarContacter =  self.getConfigOption('CLIENT_' + `dicUser['client']`,"CalendarContacter", cpServer)
        if CalendarContacter and CalendarContacter.upper() == "YES" :
             dicCal["CalendarContacter"] = "YES"
        else:
             dicCal["CalendarContacter"] = "NO"

        print dicCal
        return dicCal
        
    def xmlrpc_getTreeInfo(self, modulname, dicUser):
        liTreeFields = []
        liTreeFieldsType = []
        liTreeFieldsHeader = []
        liSearchfields = []
        liOrderfields = []
        print "dicUser = " , dicUser 
        if isinstance(dicUser, types.ListType):
            dicUser = self.list2Dic(dicUser)
        
            
        print "TreeInfo Modul-Name = ", modulname, dicUser
        
        
        cpServer, f = self.getParser(self.CUON_FS + '/clients.ini')
        if isinstance( dicUser['client'], types.StringType) :
            dicUser['client'] = int(dicUser['client'])
             
        ##########################################################################################
        #
        # address
        #
        ##########################################################################################
        if  modulname == "address":
            print "load values for address"
            print "client = ", 'CLIENT_' + `dicUser['client']`
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"AddressTreeFields", cpServer)
            print "load treefields = ", treefields
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ["lastname as name","lastname2 as name2","firstname","street","zip","city"]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"AddressTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string','string', 'string','string', 'string' ]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"AddressTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('lastname'), _('lastname2'),_('firstname'),_('street'),_('zip'),_('city') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"AddressSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
            print "searchfields = ",searchfields, liSearchfields    
            if not liSearchfields:    
                liSearchfields = [ ['eFindName', 'STRING', 'lastname'],['eFindName2', 'STRING', 'lastname2'],  ['eFindZipcode', 'STRING', 'zip'] , ['eFindCity', 'STRING','city' ], ['eFindFirstname', 'STRING', 'firstname'], ['eFindID', 'INT', 'id = '],["eFindStreet","STRING","street"],[ "eFindPhone","STRING","phone"],[ "eFindFax","STRING","fax"],[ "eFindNewsletter","STRING","newsletter"], ["eFindInfo","STRING","status_info"],["eFindEmail" ,"STRING","email"]]
            

        ##########################################################################################
        #
        # address_bank
        #
        ##########################################################################################
 
   
        elif  modulname == "address_bank":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"AddressBankTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ["depositor as s1","account_number as s2"]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"AddressBankTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string' ]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"AddressBankTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('Depositor'), _('Account-Nr') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"AddressBankSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                
            if not liSearchfields:    
                liSearchfields = [ ]
            
        ##########################################################################################
        #
        # address_partner
        #
        ##########################################################################################
 
        elif  modulname == "partner":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"PartnerTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ["lastname as name","lastname2 as name2","firstname","street","zip","city"]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"PartnerTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string','string', 'string','string', 'string' ]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"PartnerTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('lastname'), _('lastname2'),_('firstname'),_('street'),_('zip'),_('city') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"PartnerSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
            
        elif  modulname == "clients":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ClientsTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields =  ['name', 'designation','client_number as s1']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ClientsTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string','string' ]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ClientsTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('Lastname'), _('Lastname2'),_('Firstname') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ClientsSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                
            if not liSearchfields:    
                liSearchfields = [  ]


          
     
        ##########################################################################################
        #
        # articles
        #
        ##########################################################################################
        if  modulname == "articles":
            print "load values for articles"
            print "client = ", 'CLIENT_' + `dicUser['client']`
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ArticlesTreeFields", cpServer)
            print "load treefields = ", treefields
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['number', 'designation', "fct_getValueAsCurrency(sellingprice1) as price1", "fct_getValueAsCurrency(sellingprice2) as price2", "fct_getValueAsCurrency(sellingprice3) as price3", "fct_getValueAsCurrency(sellingprice4) as price4", 'unit', 'weight']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ArticlesTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string','string', 'string','string', 'string','string','string' ]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ArticlesTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('number'), _('designation'), _('price 1'), _('price 2'), _('price 3'), _('price 4'), _('unit'), _('weight') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ArticlesSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
            print "searchfields = ",searchfields, liSearchfields  
  
            if not liSearchfields:    
                liSearchfields = [ ['eFindNumber', 'STRING', 'number'],['eFindDesignation', 'STRING', 'designation'],  ['eFindmaterialGroupID', 'INT', 'material_group'] ]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ArticlesOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]
          
            #print "orderfields = ",orderfields, liOrderfields  
  
            if not liOrderfields:    
                liOrderfields = [ "number, designation"]

        ##########################################################################################
        #
        # staff
        #
        ##########################################################################################
        if  modulname == "staff":
            print "load values for staff"
            print "client = ", 'CLIENT_' + `dicUser['client']`
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StaffTreeFields", cpServer)
            print "load treefields = ", treefields
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['lastname', 'firstname', "city"]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StaffTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string','string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StaffTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('lastname'), _('firstname'), _('city') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StaffSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
            print "searchfields = ",searchfields, liSearchfields  
  
            if not liSearchfields:    
                liSearchfields = [ ['eFindNumber', 'STRING', 'number'],['eFindDesignation', 'STRING', 'designation'],  ['eFindmaterialGroupID', 'INT', 'material_group'] ]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StaffOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]
          
            #print "orderfields = ",orderfields, liOrderfields  
  
            if not liOrderfields:    
                liOrderfields = [ "lastname,firstname,city"]

                
        ##########################################################################################
        #
        # botany -Divisio
        #
        ##########################################################################################
        if  modulname == "botany_divisio":
            print "load values for diviso"
            print "client = ", 'CLIENT_' + `dicUser['client']`
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DivisioTreeFields", cpServer)
            print "load treefields = ", treefields
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['name', 'description']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DivisioTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string' ]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DivisioTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('name'), _('designation') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DivisioSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
            print "searchfields = ",searchfields, liSearchfields  
  
            if not liSearchfields:    
                liSearchfields = [ ['eFindName', 'STRING', 'name'],['eFindDesignation', 'STRING', 'description'] ]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DivisioOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]
          
            #print "orderfields = ",orderfields, liOrderfields  
  
            if not liOrderfields:    
                liOrderfields = [ "name, description"]

        ##########################################################################################
        #
        # stocks
        #
        ##########################################################################################
        if  modulname == "stocks":
            print "load values for stock"
            print "client = ", 'CLIENT_' + `dicUser['client']`
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockTreeFields", cpServer)
            print "load treefields = ", treefields
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['name', 'designation']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('number'), _('designation') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
            print "searchfields = ",searchfields, liSearchfields  
  
            if not liSearchfields:    
                liSearchfields = [ ['eFindName', 'STRING', 'name'],['eFindDesignation', 'STRING', 'designation'] ]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]
          
            #print "orderfields = ",orderfields, liOrderfields  
  
            if not liOrderfields:    
                liOrderfields = [ "name, designation"]






        ##########################################################################################
        #
        # stock_goods
        #
        ##########################################################################################
        if  modulname == "stock_goods":
            print "load values for goods"
            print "client = ", 'CLIENT_' + `dicUser['client']`
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockGoodsTreeFields", cpServer)
            print "load treefields = ", treefields
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['article_id', 'designation']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockGoodsTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['int', 'string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockGoodsTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('number'), _('designation') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockGoodsSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
            print "searchfields = ",searchfields, liSearchfields  
  
            if not liSearchfields:    
                liSearchfields = [ ['eFindName', 'STRING', 'name'],['eFindDesignation', 'STRING', 'designation'] ]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"StockGoodsOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]
          
            #print "orderfields = ",orderfields, liOrderfields  
  
            if not liOrderfields:    
                liOrderfields = [ "article_id, designation"]


        ##########################################################################################
        #
        # dms
        #
        ##########################################################################################
   

        elif  modulname == "dms":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DMSTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields =  ['title as s1', 'category as s2','document_date as d1']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DMSTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string','string' ]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DMSTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('Title'), _('Category'),_('Date') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"DMSSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                
            if not liSearchfields:    
                liSearchfields = [ ['eSearchTitle', 'STRING', 'title'],['eSearchCategory', 'STRING', "category"] ]
            
        ##########################################################################################
        #
        # enquiry
        #
        ##########################################################################################
 

        elif modulname == "enquiry":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"EnquiryTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields =  ['number', 'enquiry_from', 'fct_getDays(enquiry_from) as days',  'fct_getLastname(addressnumber) as lastname', 'customers_partner_id', 'customer_enquiry_number','designation', 'answered_at', 'process_status']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"EnquiryTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string','int','string','int','string','string','string','int','int']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"EnquiryTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('Number'), _('Date'),_('Days'),  _('Customer'), _('Case Handler'), _('Customer Enquiry Number'), _('designation'), _('Desired Date'), _('Status') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"EnquirySearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                print "liSearchfields for Enquiry = ",  liSearchfields
            if not liSearchfields:    
                liSearchfields =  [ ['eFindEnquiryNumber', 'STRING', 'number'],['eSearchCustomer', "INT", 'addressnumber'],  ['eSearchDesignation', 'STRING', 'designation'], ['eSearchDesiredDate', 'STRING', 'enquiry_from'], ['eSearchStaff', "INT", 'customers_partner_id'], ['eSearchStatus', "INT", 'process_status' ], ['eFindID', "INT", "id ="], ['eSearchCustEnqNumber', "STRING", 'customer_enquiry_number'],  ['eSearchAnsweredAt', "STRING", "'answered_at"] ]  



        ##########################################################################################
        #
        # orderbook
        #
        ##########################################################################################
 
        elif modulname == "orderbook":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['number', 'designation']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('number'), _('designation') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                print "liSearchfields for Order = ",  liSearchfields
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]

            print "orderfields = ",orderfields, liOrderfields  
            print "treefields = ", liTreeFields
            print "treefieldsType = ", liTreeFieldsType
            print "treefieldsHeader = ", liTreeFieldsHeader 
            if not liOrderfields:    
                liOrderfields = [ "number"]

        ##########################################################################################
        #
        # ordergets
        #
        ##########################################################################################
 
        elif modulname == "orderget":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderGetTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['number', 'designation']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderGetTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderGetTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('number'), _('designation') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderGetSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                print "liSearchfields for Order = ",  liSearchfields
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderGetOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]


            if not liOrderfields:    
                liOrderfields = [ "number"]


                
        ##########################################################################################
        #
        # orderpositions
        #
        ##########################################################################################
 
        elif modulname == "orderposition":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderPositionTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['articleid','designation']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderPositionTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['int','string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderPositionTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('article id'), _('designation') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderPositionSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                print "liSearchfields for Order = ",  liSearchfields
            if not liSearchfields:    
                liSearchfields = [["None","None","None"]]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"OrderPositionOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]

            if not liOrderfields:    
                liOrderfields = [ "position"]

                

        ##########################################################################################
        #
        # cashdesk
        #
        ##########################################################################################
 
        elif modulname == "cashdesk":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"CashdeskTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['number', 'designation']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"CashdeskTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"CashdeskTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('number'), _('designation') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"CashdeskSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                print "liSearchfields for Order = ",  liSearchfields
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
            
            orderfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"CashdeskOrderFields", cpServer)
            liOrderfields = []
            if orderfields:
                liOrderfields = [orderfields]

            print "orderfields = ",orderfields, liOrderfields  
  
            if not liOrderfields:    
                liOrderfields = [ "number"]
                

        ##########################################################################################
        #
        # proposal
        #
        ##########################################################################################
 


        elif modulname == "proposal":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ProposalTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['number', 'designation']
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ProposalTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ProposalTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('number'), _('designation') ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"ProposalSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                print "liSearchfields for Order = ",  liSearchfields
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
            
        ##########################################################################################
        #
        # hibernation
        #
        ##########################################################################################
 
        elif modulname == "hibernation":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"HibernationTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ['sequence_of_stock', 'hibernation_numbe',"address.lastname as lastname","address.firstname as firstname"]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"HibernationTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ['string', 'string', 'string', 'string']
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"HibernationTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_('Stocknr'), _('Number'),_('Lastname'), _('Firstname')]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"HibernationSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                print "liSearchfields for Order = ",  liSearchfields
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
            
                        
        ##########################################################################################
        #
        # sourcecode
        #
        ##########################################################################################
 
        elif modulname == "sourcecode_projects":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeProjectsTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ["name", "designation", "starts_at", "fct_getDays(date(starts_at)) as days",  "ends_at", "process_status as status ", "fct_getLastname(customer_id) as lastname", "partner_id"]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeProjectsTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ["string","string","string","int","string","int","string","int"]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeProjectsTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_("Name"), _("Designation"),_("Start at"),_("Days"),  _("End at"),  _("Status"),  _("Customer"),  _("Partner")  ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeProjectsSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
            
                
        ##########################################################################################
        #
        # sourcecode_parts
        #
        ##########################################################################################
 


        elif modulname == "sourcecode_parts":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodePartsTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ["name", "designation", "starts_at", "fct_getDays(date(starts_at)) as days",  "ends_at", "process_status as status ", ]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodePartsTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ["string","string","string","int","string","int"]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodePartsTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_("Name"), _("Designation"),_("Start at"),_("Days"),  _("End at"),  _("Status") ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodePartsSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
        
                
        ##########################################################################################
        #
        # sourcecode_modules
        #
        ##########################################################################################
 
    
            
        elif modulname == "sourcecode_module":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeModuleTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ["name", "designation", "starts_at", "fct_getDays(date(starts_at)) as days",  "ends_at", "process_status as status "]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeModuleTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ["string","string","string","int","string","int"]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeModuleTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_("Name"), _("Designation"),_("Start at"),_("Days"),  _("End at"),  _("Status") ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeModuleSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]
         

                
        ##########################################################################################
        #
        # sourcecode_file
        #
        ##########################################################################################
 

        elif modulname == "sourcecode_file":
            treefields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeFileTreeFields", cpServer)
            if treefields:
                liTreeFields = treefields.strip().split(',')
            if not liTreeFields:
                liTreeFields = ["name", "designation", "starts_at", "fct_getDays(date(starts_at)) as days",  "ends_at", "process_status as status "]
                
                
                
            treefieldsType = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeFileTreeFieldsTypes", cpServer)
            if treefieldsType:
                liTreeFieldsType = treefieldsType.strip().split(',')
            if not liTreeFieldsType:
                liTreeFieldsType = ["string","string","string","int","string","int"]
                
            treeheader = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeFileTreeHeader", cpServer)
            if treeheader:
                liTreeFieldsHeader = treeheader.strip().split(',')
            if not liTreeFieldsHeader:
                liTreeFieldsHeader = [_("Name"), _("Designation"),_("Start at"),_("Days"),  _("End at"),  _("Status") ]
                    
                
            searchfields = self.getConfigOption('CLIENT_' + `dicUser['client']`,"SourcecodeFileSearchFields", cpServer)
            if searchfields:
                liS1 = searchfields.strip().split('&')
                if liS1:
                    for s in liS1:
                        liSearchfields.append(s.split(','))
                
            if not liSearchfields:    
                liSearchfields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]

        liTreeFieldsHeader.append(_("ID"))
        liTreeFields.append("id")
        liTreeFieldsType.append("int")
        print "Fields = ",  liSearchfields, liTreeFields,  liTreeFieldsType,  liTreeFieldsHeader
        for i in range(len(liTreeFields)):
            liTreeFields[i]= liTreeFields[i].strip()
        for i in range(len(liTreeFieldsType)):
            liTreeFieldsType[i]= liTreeFieldsType[i].strip()

        for i in range(len(liTreeFieldsHeader)):
            liTreeFieldsHeader[i]= liTreeFieldsHeader[i].strip()
        for i in range(len(liSearchfields)):
            for j in range(len(liSearchfields[i])):
                liSearchfields[i][j] = liSearchfields[i][j].strip()
        print liSearchfields, liTreeFields,  liTreeFieldsType,  liTreeFieldsHeader, liOrderfields
        
        return [liSearchfields, liTreeFields,  liTreeFieldsType,  liTreeFieldsHeader, liOrderfields]
            
    def xmlrpc_getStatusbar(self,iModulNumber, iID, dicUser):

        """return the text for the statusbar"""
        sSql = "select * from fct_get_statusbar(" + `iModulNumber` + ", " + `iID` + ") as statustext "
        result = self.oDatabase.xmlrpc_executeNormalQuery(sSql,dicUser)
        if result not in self.liSQL_ERRORS:
           return result[0]["statustext"]
        else:
           return "NONE"
       
    
    def xmlrpc_getSearchWhere(self, args):
        print "getSearchWhere = ",  args
        sWhere = None
        args.reverse()
        firstWhere = True
        #print args
        while args:
            s = args.pop()
            v = args.pop()
            #print 'getWhere'
            #print s,v
            #self.printOut( 'args s = ', s)
            #self.printOut( 'args v = ', v)
            if s:
                
                if len(s)>2 and s[0:3] == '###':
                    #print 'found ###',s,v
                    if firstWhere:
                        sWhere = " where " + s[3:]  
                        firstWhere = False
                    else:
                        sWhere = sWhere +" and " + s[3:]
                        
                elif isinstance(v, types.BooleanType) :
                    self.printOut( 'sWhere = ', v )
                    if firstWhere:
                        sWhere = " where " + s +" = " + `v` 
                        firstWhere = False
                    else:
                        sWhere = sWhere +" and " + s + " = " + `v` 
                elif isinstance(v, types.IntType) :
                    self.printOut( 'sWhere = ', v )
                    if firstWhere:
                        sWhere = " where " + s +" " + `v` 
                        firstWhere = False
                    else:
                        sWhere = sWhere +" and " + s + " " + `v`   
                
                elif v[0] == '#':
                    
                    v = v[1:]
                    v = v.replace('?','~')
                    v = v.replace('>','^')
                    if firstWhere:
                        sWhere = " where " + s +" " + v 
                        firstWhere = False
                    else:
                        sWhere = sWhere +" and " + s + " " + v 
                        
                else:
                    if firstWhere:
                        sWhere = " where " + s +" ~* \'.*" + v + '.*\''
                        firstWhere = False
                    else:
                        sWhere = sWhere +" and " + s + ' ~* \'.*' + v + '.*\''
        
        print sWhere 
        return sWhere
    
    

    
    def xmlrpc_getMN(self,dicUser=None):
        return self.MN
