# -*- coding: utf-8 -*-

##Copyright (C) [2012]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
          
          
    


from datetime import datetime, timedelta
import time
from cuon.basics import basics
import cuon.Database
import ConfigParser
from xmlrpclib import ServerProxy
try:
    from Crypto.Cipher import AES
except:
    print 'No Crypto-Module found -- !'
import hashlib



class CronTab(basics):
    def __init__(self):
         basics.__init__(self)
         # self.oDatabase = cuon.Database.Database()
         if  self.XMLRPC_ALLOW_HTTP:
             self.server = "http://" +  self.XMLRPC_HOST + ":" +  `self.XMLRPC_HTTP_PORT + self.ADD_PORT_INSTANCES`
         else:
             self.server = "https://" +  self.XMLRPC_HOST + ":" +  `self.XMLRPC_HTTPS_PORT + self.ADD_PORT_INSTANCES`

         # test
         #self.server = "http://" +  self.XMLRPC_HOST + ":" +  "7310"
         print "server = ", self.server
         self.SP = ServerProxy(self.server,allow_none = 1)

         self.events = []
         for i in self.CRONTAB:
             
             sTime = i[1].replace("*","allMatch")
             
             s = "event = Event(" + sTime + ", client_id= " + `i[0]` + ")"
             print s
             exec s
             self.events.append(event)
             #print self.events


    def run(self):
        tOld = -1
 

        while True:
            t=datetime(*datetime.now().timetuple()[:5])
            tNew = t.minute 
            print tNew, tOld
            if tNew != tOld:
                for e in self.events:
                    e.check(t)
                tOld = tNew
            time.sleep(15)





#        while 1:
#            for e in self.events:
#                e.check(t)

#            t += timedelta(minutes=1)
#            while datetime.now() < t:
#                time.sleep((t - datetime.now()).seconds)


  
    def getUser(self):
    
        config = ConfigParser.RawConfigParser()
        config.read(self.CUON_FS + '/specialuser.cfg')
        value = config.get("USER","CRONTAB")
        liValue = value.split(",")
        dicUser = {}
        dicUser["userType"] = "cuon"
        dicUser["Name"]= liValue[0].strip()
        m = hashlib.sha512(liValue[1].strip() )

        dicUser["SessionID"]=self.SP.Database.createSessionID(dicUser["Name"], m.hexdigest())
        print "cron = ", dicUser
        return dicUser
    
    
    def perform_test(self, client_id):
        print "test 42 and id = " + `client_id`


    def clean_orderbook(self,client_id):
        dicUser = self.getUser()
        dicUser["client"]  = client_id
        self.SP.Order.clean_orderbook(dicUser)
        print "done"
        return 1

    
    def hibernation_check_deliver_1(self, client_id):
        #send hibernation email deliver
        dicUser = self.getUser()
        dicUser["client"]  = client_id
        print dicUser
        self.SP.Garden.hibernation_check_deliver_1(dicUser)
        print "done"
        return 1

    def garden_check_schedule_1(self,client_id):
        dicUser = self.getUser()
        dicUser["client"]  = client_id
        self.SP.Garden.check_schedule_1(dicUser)
        return 1
  

    def address_set_erp1(self,client_id):
         dicUser = self.getUser()
         dicUser["client"]  = client_id
         self.SP.Address.set_erp1(dicUser)
         return 1

    def clean_calendar(self, client_id):
         dicUser = self.getUser()
         dicUser["client"]  = client_id
         self.SP.Web.cron_create_iCal(dicUser)
         self.SP.Web.cron_create_iCal2(dicUser)
         return 1
     


         
# Some utility classes / functions first
class AllMatch(set):
    """Universal set - match everything"""
    def __contains__(self, item): return True

allMatch = AllMatch()

def conv_to_set(obj):  # Allow single integer to be provided

   
    if isinstance(obj, (int,long)):
        return set([obj])  # Single item
    if not isinstance(obj, set):
        obj = set(obj)
    
    return obj

# The actual Event class
class Event(object):
    def __init__(self, action, min=allMatch, hour=allMatch, 
                       day=allMatch, month=allMatch, dow=allMatch, client_id = 0,
                       args=(), kwargs={}):
        self.mins = conv_to_set(min)
        self.hours= conv_to_set(hour)
        self.days = conv_to_set(day)
        self.months = conv_to_set(month)
        self.dow = conv_to_set(dow)
        self.action = action
        self.client_id = client_id
        self.args = args
        self.kwargs = kwargs

    def matchtime(self, t):
        """Return True if this event should trigger at the specified datetime"""
        #print 'Min', t.minute , self.mins
        #print 'Hour', t.hour,  self.hours
        #print 'Day', t.day, self.days
        #print 'Month', t.month, self.months
        #print 'DOW', t.weekday()  , self.dow
        return ((t.minute     in self.mins) and
                (t.hour       in self.hours) and
                (t.day        in self.days) and
                (t.month      in self.months) and
                (t.weekday()  in self.dow))

    def check(self, t):
        if self.matchtime(t):
            print "time found - action starts over"
            self.action(self.client_id,*self.args, **self.kwargs)

def perform_backup(): 
    print 'perform'
    #exec self.LOCALXMLRPCSERVER + ".Addresses.scheduleBirthday(dicUser)
def purge_temps():
    print 'purge'


      
        

 
    

  
c = CronTab()
 # Event(perform_backup, 55, 13, dow=[0, 1, 2, 3, 4, 5, 6] ),  Event(purge_temps, 0, range(9,18,2), dow=range(0,5))
 
 

c.run()

