from direct.showbase import *
from direct.task.Task import *
from pandac.PandaModules import *
from direct.distributed.PyDatagram import PyDatagram

from direct.showbase.DirectObject import DirectObject
from cuon.World.Server import *
import sys 
from twisted.internet.task import LoopingCall
from twisted.internet import reactor

#ShowBase.ShowBase()
#TODO: Make it so when a player disconnects he is removed from the game, make it so the server does not have a window(Custom Showbase Class)

#receive connection > create Player > send Player initializing info > receive updates from Player and adjust data accordingly > send update to all Players(all positions)
try:    
    port = int(sys.argv[1])
except:
    port = 0
print port

#Create the server
taskMgr = TaskManager()
worldServer = Server(9099,1000)

Active = PlayerReg()

taskMgr.add(worldServer.tskListenerPolling,"Poll the connection listener",extraArgs = [Active])
taskMgr.add(worldServer.tskReaderPolling,"Poll the connection reader",extraArgs = [Active])
taskMgr.add(Active.updatePlayers,"Update Every Player",extraArgs = [worldServer,None,"positions"])

print "successful"

LoopingCall(taskMgr.step).start(0.10)
reactor.run()

