# -*- coding: utf-8 -*-

##Copyright (C) [2009 -2010]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

"""
Simple IMAP4 client 
"""

import sys
import cuon.basics

from twisted.internet import protocol
from twisted.internet import ssl
from twisted.internet import defer
from twisted.internet import stdio
from twisted.mail.imap4 import IMAP4Client, CramMD5ClientAuthenticator, LOGINAuthenticator, NoSupportedAuthentication, MessageSet, Query, Not

from twisted.protocols import basic
from twisted.python import util
from twisted.python import log

from email import message_from_string
from email.header import decode_header

#class TrivialPrompter(basic.LineReceiver):
#    from os import linesep as delimiter
#
#    promptDeferred = None
#    
#    def prompt(self, msg):
#        assert self.promptDeferred is None
#        self.display(msg)
#        self.promptDeferred = defer.Deferred()
#        return self.promptDeferred
#    
#    def display(self, msg):
#        self.transport.write(msg)
#    
#    def lineReceived(self, line):    
#        if self.promptDeferred is None:
#            return
#        d, self.promptDeferred = self.promptDeferred, None
#        d.callback(line)

class SimpleIMAP4Client(IMAP4Client):
    greetDeferred = None
    
    def serverGreeting(self, caps):
        self.serverCapabilities = caps
        if self.greetDeferred is not None:
            d, self.greetDeferred = self.greetDeferred, None
            d.callback(self)

class SimpleIMAP4ClientFactory(protocol.ClientFactory):
    usedUp = False

    protocol = SimpleIMAP4Client

    def __init__(self, username, onConn):
        self.ctx = ssl.ClientContextFactory()
        
        self.username = username
        self.onConn = onConn

    def buildProtocol(self, addr):
        assert not self.usedUp
        self.usedUp = True
        
        p = self.protocol(self.ctx)
        p.factory = self
        p.greetDeferred = self.onConn

        auth = CramMD5ClientAuthenticator(self.username)
        p.registerAuthenticator(auth)
        
        return p
    
    def clientConnectionFailed(self, connector, reason):
        d, self.onConn = self.onConn, None
        d.errback(reason)

# Initial callback - invoked after the server sends us its greet message
def cbServerGreeting(proto, username, password):
    # Hook up stdio
    #tp = TrivialPrompter()
    #stdio.StandardIO(tp)
    
    # And make it easily accessible
    #proto.prompt = tp.prompt
    #proto.display = tp.display

    # Try to authenticate securely
    return proto.authenticate(password
        ).addCallback(cbAuthentication, proto
        ).addErrback( cbInsecureLogin, proto, username, password)  

# Fallback error-handler.  If anything goes wrong, log it and quit.
def ebConnection(reason):
    log.startLogging(sys.stdout)
    log.err(reason)
    from twisted.internet import reactor
    reactor.stop()

# Callback after authentication has succeeded
def cbAuthentication(result, proto):
    # List a bunch of mailboxes
    return proto.list("", "*"
        ).addCallback(cbMailboxList, proto
        )

# Errback invoked when authentication fails
#def ebAuthentication(failure, proto, username, password):
    # If it failed because no SASL mechanisms match, offer the user the choice
    # of logging in insecurely.
     #failure.trap(imap4.NoSupportedAuthentication)
    #return proto.prompt("No secure authentication available.  Login insecurely? (y/N) "
     #   ).addCallback(cbInsecureLogin, proto, username, password
       # )
 #  return proto.authenticate.addCallback(cbInsecureLogin, proto, username, password)

# Callback for "insecure-login" prompt
def cbInsecureLogin(result, proto, username, password):
    #if result.lower() == "y":
        # If they said yes, do it.
    return proto.login(username, password
            ).addCallback(cbAuthentication, proto
            )
    #return defer.fail(Exception("Login failed for security reasons."))

# Callback invoked when a list of mailboxes has been retrieved
def cbMailboxList(result, proto):
    result = [e[2] for e in result]
    s = '\n'.join(['%d. %s' % (n + 1, m) for (n, m) in zip(range(len(result)), result)])
    if not s:
        return defer.fail(Exception("No mailboxes exist on server!"))
    #return proto.prompt(s + "\nWhich mailbox? [1] "
      #  ).addCallback(cbPickMailbox, proto, result
        #)
    #print s
    mbox = 'INBOX.AUTOMATIC_CUON'

    if mbox in s:
       
                #print "mbox",  mbox
                return proto.examine(mbox).addCallback(cbExamineMbox, proto  )
    else:
        return defer.fail(Exception("No mailbox named INBOX.AUTOMATIC_CUON exist on server!"))


def getHeader(header_text, default="utf-8"):
    """Decode the specified header"""

    headers = decode_header(header_text)
    header_sections = [unicode(text, charset or default)
                       for text, charset in headers]
    return u"".join(header_sections)  
    
# When the user selects a mailbox, "examine" it.
def cbPickMailbox(result, proto, mboxes):
    mbox = mboxes[int(result or '1') - 1]
    return proto.examine(mbox).addCallback(cbExamineMbox, proto)

# Callback invoked when examine command completes.
def cbExamineMbox(result, proto):
    # Retrieve the subject header of every message on the mailbox.
    return proto.fetchSpecific('1:*', headerType='HEADER.FIELDS', headerArgs=['FROM','TO','SUBJECT']).addCallback(cbFetch, proto )
    #return proto.fetch('1:*', True).addCallback(cbFetch, proto )

# Finally, display headers.
def cbFetch(result, proto):
    keys = result.keys()
    #keys.sort()
    #for k in keys:
        #print '%s %s' % (k, result[k][0][2])
#        print 'k = ', k
#        print 'Result[k] = ',  result[k]
#        for m in result[k]:
#            print 'm = ',  m
#            for n in m:
#                print 'n = ', n
#            
    messages = MessageSet()
    for uid in result:
        messages.add(uid)
    #print "messages = ",  messages
    
    d = proto.fetchMessage(messages, uid=False)
    d = d.addCallback(gotMail,  proto)

def gotMail( result,  proto):
    #self.output("imap: got mail")
    #messages = MessageSet(proto)
    
    #print 'proto = ',  proto
    #result = proto.fetchAll()
    #print 'proto',  `proto`
    #print 'result = ',  result
    #print 'messages =',  messages
   
    for data in result.itervalues():
        #print 'Single Data = ',  data
        message = message_from_string(data['RFC822'])
        sFrom = getHeader(message['From'])
        #print "gotMessages",  message
        #sFrom = message[message.find('From:'):message.find('\n', message.find('From:') )]
        print 'From = ',  sFrom
        sTo = getHeader(message['To'])
        print 'sTo = ',  sTo
        sSubject = getHeader(message['Subject'])
        print 'sSubject = ',  sSubject
        for part in message.walk():
            print 'email part = ',  part.get_content_type() 
        # each part is a either non-multipart, or another multipart message
        # that contains further parts... Message is organized like a tree
            if part.get_content_type() == 'text/plain':
                print 'text'
                #print part.get_payload() # prints the raw text

            if  part.get_content_type() == 'multipart':
                print 'multipart'

    proto.logout()
    ebConnection('normal shutdown')
    
    
    
    
baseSettings = cuon.basics.basics()
print baseSettings.IMAP_USERNAME
print baseSettings.IMAP_PASSWORD
print baseSettings.IMAP_HOST
print baseSettings.IMAP_PORT

onConn = defer.Deferred(
    ).addCallback(cbServerGreeting, baseSettings.IMAP_USERNAME, baseSettings.IMAP_PASSWORD
    ).addErrback(ebConnection
    )

factory = SimpleIMAP4ClientFactory(baseSettings.IMAP_USERNAME, onConn)
    
from twisted.internet import reactor
conn = reactor.connectTCP(baseSettings.IMAP_HOST, baseSettings.IMAP_PORT, factory)
reactor.run()