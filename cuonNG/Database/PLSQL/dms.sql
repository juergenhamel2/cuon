

CREATE  OR REPLACE FUNCTION sh_convert2pdfa(pdf_text text) RETURNS text AS $$
#!/bin/bash
if [ !  -d "temp" ]; then
   mkdir "temp"
fi
cd temp
uuid=$(uuidgen)
echo "$1" > $uuid.bz2.base64
base64 -w0 --decode  $uuid.bz2.base64 > $uuid.bz2
bunzip2 $uuid.bz2
cuon_pdf2pdfa.sh $uuid.pdf $uuid

#gs -dPDFA -dBATCH -dNOPAUSE -dNOOUTERSAVE   -dUseCIEColor -sProcessColorModel=DeviceCMYK -sDEVICE=pdfwrite -sPDFACompatibilityPolicy=1 -sOutputFile=$uuid.pdf $uuid
rm $uuid
rm  $uuid.bz2.base64
#cp $uuid.pdf test2.pdf
bzip2 $uuid.pdf
base64 -w0 $uuid.pdf.bz2 > $uuid.pdf.bz2.base64
value=$(<$uuid.pdf.bz2.base64)

rm  $uuid.pdf.bz2
rm  $uuid.pdf.bz2.base64

echo $value

$$ LANGUAGE 'plsh' ;



CREATE OR REPLACE FUNCTION fct_test(dms_id int) returns text as $$   
 DECLARE
	pdf text ;
	zip text;
	sSql text ;
	
	BEGIN
	sSql = $$'select document_image from dms where id = $$' || dms_id ;
	raise notice $$' ssql = %$$',sSql ;
	execute sSql into zip ;
	-- raise notice $$'zip = %$$', zip ;
	pdf = sh_convert2pdfa(zip) ;
	sSql = $$'update dms set document_image = $$' || quote_literal( pdf) || $$' where id = $$' || dms_id ;
	-- execute sSql ;
	return $$'ok$$';
    END ;
    $$ LANGUAGE 'plpgsql';



CREATE OR REPLACE FUNCTION fct_get_dms_document_image(dms_id int) returns text as $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sImage text ;
        sSql text ;
        
        BEGIN
          
      
    
            select into iPaired_ID paired_id from dms where id = dms_id ;
            if iPaired_ID is null then 
                iPaired_ID = 0 ;
            end if ;
            
            if iPaired_ID > 0 then 
                iSearchID := iPaired_ID ;
            else
                iSearchID := dms_id ;
            end if ;
            
            sSql := ''select document_image from dms where id = '' || iSearchID ;
            -- set later more restrictions
            
            execute(sSql) into sImage ;
            
            return sImage ;
            
        END ;
    $$ LANGUAGE 'plpgsql'; 
 
CREATE OR REPLACE FUNCTION fct_ExtractTextFromDMS(dms_id int) returns text as $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sText text ;
        sSql text ;
        
        BEGIN
          
      
    
            select into iPaired_ID paired_id from dms where id = dms_id ;
            if iPaired_ID is null then 
                iPaired_ID = 0 ;
            end if ;
            
            if iPaired_ID > 0 then 
                iSearchID := iPaired_ID ;
            else
                iSearchID := dms_id ;
            end if ;
            
            sSql := ''select dms_extract from dms where id = '' || iSearchID ;
            -- set later more restrictions
            
            execute(sSql) into sText ;
            
            if sText is null then 
                sText = ''NONE'' ;
            end if ;
                
            return sText ;
            
        END ;
    $$ LANGUAGE 'plpgsql'; 
    

