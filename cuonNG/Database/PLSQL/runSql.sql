
CREATE OR REPLACE FUNCTION fct_getLiInfoOfTable( tablename text) returns text [] as '
    -- check and alter table and column
     
    DECLARE
    ok bool;
    result text [] ;
    r1 record ;
    sSql text ;
    
    
    BEGIN

	sSql = '' SELECT column_name, data_type  FROM information_schema.columns WHERE table_name = '' || quote_literal(tablename) ;
	FOR r1 in execute(sSql) LOOP
	    result = array_append(result,quote_literal(r1.column_name ));
	    if r1.data_type = ''character'' then
	    
		result = array_append(result,''string'');
	    elseif r1.data_type = ''character varying'' then
	    
		result = array_append(result,''string'');
 
	    else

	        result = array_append(result,quote_literal(r1.data_type ));
            END IF ;
	END LOOP ;
         return  result ;
    END ;
    
     ' LANGUAGE 'plpgsql'; 




CREATE OR REPLACE FUNCTION fct_checkColumn( tablename text, columnname text, typename text, first_size int, second_size int ) returns bool as '
    -- check and alter table and column
     
    DECLARE
    ok bool;
    sSql text ;
    
    
    BEGIN
        ok = false ;
        
        sSql := ''SELECT a.attname as a_column, pg_catalog.format_type(a.atttypid, a.atttypmod) as p_datatype FROM  pg_catalog.pg_attribute a '' ;
        sSql := sSql || '' WHERE  a.attnum > 0  AND NOT a.attisdropped AND a.attrelid = ( SELECT c.oid FROM pg_catalog.pg_class c LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace  WHERE c.relname = '' || quote_literal(tablename )  ;
        sSql := sSql || '' AND pg_catalog.pg_table_is_visible(c.oid)  ) '' ;

        raise notice '' sSql = %'',sSql ;
        
        
        return ok ;
    END ;
    
     ' LANGUAGE 'plpgsql'; 
   

   

   
CREATE OR REPLACE FUNCTION fct_duplicate_table( tablename text, tablename2 text ) returns bool as '
    -- insert whole table1 in table2
     
    DECLARE
    ok bool;
    sSql text ;
    sExe text ;
    sExe2 text ;
    sSql2 text ;
    ri record ;
    oldrecord record ;
    len int ;
    
    
    BEGIN
        ok = true ;
    
        sExe := '' insert into '' || tablename2 || '' ( '' ;
        sExe2 := '' select  '' ;
        sSql := '' SELECT ordinal_position, column_name, data_type  FROM information_schema.columns   WHERE    table_schema = '' || quote_literal(''public'') || ''  AND table_name = '' || quote_literal(tablename) || '' ORDER BY ordinal_position '' ;
        
        sSql2 := ''select id from '' || tablename ;
        raise notice '' sSql= %'', sSql ;
        raise notice '' sSql2= %'', sSql2 ;
        FOR oldrecord in execute(sSql2) LOOP
            raise notice ''start loop1 '' ;
            FOR ri in execute(sSql) LOOP
        
        
           
                sExe := sExe || ri.column_name || '', '' ;
                sExe2 := sExe2 || ri.column_name || '', '' ;
                      
                        
            END LOOP;
            raise notice ''end loop2 '' ;
            len := length(sExe);    
            sExe := substring(sExe from 1 for (len-2) ) ;
            sExe := sExe || '' ) '' ;
            
            len := length(sExe2);    
            sExe2 := substring(sExe2 from 1 for (len-2) ) ;
            
            
            
            sExe := sExe || sExe2 || '' from '' || quote_ident(tablename) || '' where id = '' || oldrecord.id ;
            raise notice '' sExe = %'', sExe ;
            -- execute sExe ;
        
         END LOOP;
      return ok ;
    END ;
    
     ' LANGUAGE 'plpgsql'; 
   
   
  
CREATE OR REPLACE FUNCTION fct_duplicate_table_entry( tablename text, tablename2 text, oldid int, id_sequence text ) returns int as '
    -- insert whole table1 in table2
     
    DECLARE
    ok bool;
    sSql text ;
    sExe text ;
    sExe2 text ;
    sSql2 text ;
    ri record ;
    oldrecord record ;
    len int ;
    newid int ;
    sSql3 text ;
    BEGIN
        ok = true ;
        sSql3 = ''select nextval('' || quote_literal(id_sequence) || '') '' ;
        execute(sSql3) into newid ;
        
        sExe := '' insert into '' || tablename2 || '' ( '' ;
        sExe2 := '' ( select  '' ;
        sSql := '' SELECT ordinal_position, column_name, data_type  FROM information_schema.columns   WHERE    table_schema = '' || quote_literal(''public'') || ''  AND table_name = '' || quote_literal(tablename) || '' ORDER BY ordinal_position '' ;
        
        sSql2 := ''select id from '' || tablename || '' where id = '' || oldid  ;
        raise notice '' sSql= %'', sSql ;
        raise notice '' sSql2= %'', sSql2 ;
        FOR oldrecord in execute(sSql2) LOOP
            raise notice ''start loop1 '' ;
            FOR ri in execute(sSql) LOOP
        
        
                IF ri.column_name = ''id'' THEN
                    sExe := sExe || ri.column_name || '', '' ;
                    sExe2 := sExe2 || newid || '' , '' ;
                ELSE 
                    sExe := sExe || ri.column_name || '', '' ;
                    sExe2 := sExe2 || ri.column_name || '', '' ;
                END IF ;
                      
                        
            END LOOP;
            raise notice ''end loop2 '' ;
            len := length(sExe);    
            sExe := substring(sExe from 1 for (len-2) ) ;
            sExe := sExe || '' ) '' ;
            
            len := length(sExe2);    
            sExe2 := substring(sExe2 from 1 for (len-2) ) ;
            
            
            
            sExe := sExe || sExe2 || '' from '' || quote_ident(tablename) || '' where id = '' || oldid  || '' ) '' ;
            raise notice '' sExe = %'', sExe ;
            execute sExe ;
        
         END LOOP;
      return newid ;
    END ;
    
     ' LANGUAGE 'plpgsql'; 
   
DROP FUNCTION fct_get_database( text ) CASCADE ;

CREATE OR REPLACE FUNCTION fct_get_database( stype text ) returns return_text3 as '
    
     
    DECLARE
    sDB text ;
    sDBPositions text ;
    sDBInvoice text ;
    
    rText3 return_text3 ;
    
    BEGIN
     
       
            
                
            if stype = ''Proposal'' then
                sDB = ''proposal'';
                sDBPositions = ''proposalposition'';
                sDBInvoice = ''proposalinvoice'';
                
            elseif stype = ''Enquiry'' then
                sDB = ''enquiry'' ;
                sDBPositions = ''enquiryposition'' ;
                sDBInvoice = ''enquiryinvoice'' ;
            elseif stype = ''Project'' then 
                 
                sDB = ''projects'' ;
                sDBPositions = '''' ;
                sDBInvoice = ''''   ; 
                
            else 
                sDB = ''orderbook'' ;
                sDBPositions = ''orderposition'' ;
                sDBInvoice = ''orderinvoice'' ;
                
            end if ;
        
        rText3.a = sDB ;
        rText3.b = sDBPositions ;
        rText3.c = sDBInvoice ;
        
        return rText3 ;
    END ;
    
     ' LANGUAGE 'plpgsql'; 
      
