-- -*- coding: utf-8 -*-

-- Copyright (C) [2003 -2015]  [Juergen Hamel, D-32584 Loehne, Germany]

-- This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.

-- You should have received a copy of the GNU General Public License along with this program; if not, write to the
-- Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
-- IMPORTANT
-- To use this without cuon (www.cuon.org) replace all $$ with a '




CREATE OR REPLACE FUNCTION fct_savePic2disk( iArticleID integer, float, sType text ) returns  text AS $$

  
    DECLARE
        sSql    text ;
    	actual_value float ;
      	cur1 refcursor ;
        rec1 record ;
	rec2 record;
	ok bool ;
	 sAssociatedTable text ;
	 iDMS int;
	 filename text ;
	    
    BEGIN
	sSql = $$'select number, designation, sellingprice1, sellingprice2, sellingprice3, sellingprice4, wrapping, unit,quantumperwrap, manufactor_id, material_group, sep_info_1,id,  associated_with, associated_id,  fct_getBarcodeForArticleID(id) as barcode from articles where id = iArticleID$$' ;
	execute (sSql) into rec1 ;
	 sAssociatedTable =  fct_get_associated_table(rec1.associated_with) ;
	 iDMS =  fct_get_dms_module(rec1.associated_with);
	
 	if sAssociatedTable = $$'botany$$' || iDMS > 0 then
	 sSql = $$'select document_image, file_suffix from dms where insert_from_module = $$' || iDMS || $$' and title = $$' || quote_literal($$'print001$$') || $$' and sep_info_1 = $$' || rec1.associated_id || $$' $$' || fct_getWhere(2,$$'$$') ;
	 execute (sSql) into rec2 ;

	 end if ;

	 filename = sh_savePic(rec2.file_suffix,rec2.document_image);
	 
	 return filename ;

	
   end ;
    $$ LANGUAGE 'plpgsql';




CREATE OR REPLACE FUNCTION fct_set_article2reserved( iArticleID integer, iStockID integer,fValue float, sType text, iID integer ) returns  bool AS $$

 
    DECLARE
        sSql    text ;
    	actual_value float ;
      	cur1 refcursor ;
        rec1 record ;
	    ok bool ;

    BEGIN
        ok = True ;
        -- First check if it is an existing reserved order
        sSql := $$'select id from stock_goods where stock_id = $$' || iStockID || $$' and article_id = $$' || iArticleID  || $$' $$' ;
       
        IF sType = "Deliver" then 

            sSql = sSql || $$'and delivery_id = $$' || iID || $$' $$' ;

        ELSIF sType = "Invoice" then 

            sSql = sSql || $$'and order_id = $$' || iID || $$' $$' ;

        ELSIF sType = "Project" then 

            sSql = sSql || $$'and project_id = $$' || iID || $$' $$' ;
        
        ELSIF sType = "Booking" then 

            sSql = sSql || $$'and booking_id = $$' || iID || $$' $$' ;

        END IF ;

        




       
        sSql = sSql || $$' and reserved_bit = 1 $$' ||  fct_getWhere(2,$$'$$') ;


        execute (sSql) into rec1 ;

        if found then 


            -- found a value ;
            sSql := $$'update stock_goods set roll_out =  $$' || fValue || $$'where id = $$' || rec1.id ;
            
        else

            sSql := $$'insert into stock_goods (id) values ()$$' ;


        end if ;


	raise notice $$'Test$$';
	
	    return ok ;
	


       END; 
       
    $$ LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION fct_get_lastReservedValue( iArticleID integer, iStockID integer, iID integer) returns  float AS $$

 
    DECLARE
        sSql    text ;
	actual_value float ;
      	cur1 refcursor ;
	
    BEGIN

	sSql := $$'select actual_reserved  from stock_goods where id < $$' || iID || $$' and  stock_id = $$' || iStockID || $$' and article_id = $$' || iArticleID  || $$' $$' ||  fct_getWhere(2,$$'$$')|| $$' order by date_of_change, id desc $$' ;
	
	OPEN cur1 FOR EXECUTE sSql ;
        FETCH cur1 INTO actual_value ;

   	close cur1 ;

	if NOT FOUND then
	   actual_value = 0;
	end if ;
	
		
	-- raise notice $$' Actual Reserved = % $$', actual_value ;
	if actual_value is null then
	   actual_value := 0.00 ;
	end if ;
	   

	return actual_value ;
	


   END ;
    $$ LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION fct_get_lastStockValue( iArticleID integer, iStockID integer, iID integer) returns  float AS $$

 
    DECLARE
        sSql    text ;
	actual_value float ;
      	cur1 refcursor ;
	
    BEGIN

	sSql := $$'select actual_stock  from stock_goods where id < $$' || iID || $$' and  stock_id = $$' || iStockID || $$' and article_id = $$' || iArticleID  || $$' $$' ||  fct_getWhere(2,$$'$$')|| $$' order by date_of_change, id desc $$' ;
	
	OPEN cur1 FOR EXECUTE sSql ;
        FETCH cur1 INTO actual_value ;

	if NOT FOUND then
	   actual_value = 0;
	end if ;

	close cur1 ;    

		
	-- raise notice $$' Actual Stock = % $$', actual_value ;
	if actual_value is null then
	   actual_value := 0.00 ;
	end if ;
	   

	return actual_value ;



   END ;
    $$ LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION fct_get_TotalSum( iArticleID integer, iStockID integer) returns  float AS $$

 
    DECLARE
        sSql    text ;
	actual_value float ;
      	cur1 refcursor ;
        rec1 record ;
	
    BEGIN
    actual_value = 0.00 ;

	sSql := $$'select to_embed, roll_out, reserved_bit from stock_goods where stock_id = $$' || iStockID || $$' and article_id = $$' || iArticleID  || $$' $$' ||  fct_getWhere(2,$$'$$') ;
	
    raise notice $$'sSql = %$$', sSql ;
	OPEN cur1 FOR EXECUTE sSql ;
    FETCH cur1 INTO rec1 ;

    While FOUND LOOP


    	if rec1.to_embed IS NULL then
	       rec1.to_embed = 0.00;
        end if ;
	    if rec1.roll_out IS NULL then
	       rec1.roll_out = 0.00;
        end if ;
        if rec1.reserved_bit IS NULL then
	       rec1.reserved_bit = 0;
        end if ;

        raise notice $$' Total-Sum at Stock = %. article = %, embed =%, roll = % $$', iStockID, iArticleID, rec1.to_embed , rec1.roll_out   ;
        
        if rec1.reserved_bit = 0 then 
            actual_value = actual_value + rec1.to_embed - rec1.roll_out ;
        end if ;

        raise notice $$' Total-Sum 0.1 = % $$', actual_value ;

	
        FETCH NEXT from cur1 INTO rec1 ;

    END LOOP ;

    close cur1 ;

		
	raise notice $$' Total-Sum = % $$', actual_value ;
	if actual_value is null then
	   actual_value := 0.00 ;
	end if ;
	   

	return actual_value ;
	


   END ;
    $$ LANGUAGE 'plpgsql';


CREATE OR REPLACE FUNCTION fct_get_ReservedSum( iArticleID integer, iStockID integer) returns  float AS $$

 
    DECLARE
        sSql    text ;
	actual_value float ;
      	cur1 refcursor ;
        rec1 record ;
	
    BEGIN
    actual_value = 0.00 ;

	sSql := $$'select to_embed, roll_out from stock_goods where stock_id = $$' || iStockID || $$' and article_id = $$' || iArticleID  || $$' and reserved_bit = 1 $$' ||  fct_getWhere(2,$$'$$') ;
	
    raise notice $$'sSql = %$$', sSql ;
	OPEN cur1 FOR EXECUTE sSql ;
    FETCH cur1 INTO rec1 ;

    While FOUND LOOP


    	if rec1.to_embed IS NULL then
	       rec1.to_embed = 0.00;
        end if ;
	    if rec1.roll_out IS NULL then
	       rec1.roll_out = 0.00;
        end if ;
        raise notice $$' Total-Sum at Stock = %. article = %, embed =%, roll = % $$', iStockID, iArticleID, rec1.to_embed , rec1.roll_out   ;

        actual_value = actual_value + rec1.to_embed - rec1.roll_out ;
        raise notice $$' Total-Sum 0.1 = % $$', actual_value ;

	
        FETCH NEXT from cur1 INTO rec1 ;

    END LOOP ;

	close cur1 ;

	raise notice $$' Total-Sum = % $$', actual_value ;
	if actual_value is null then
	   actual_value := 0.00 ;
	end if ;
	   

	return actual_value ;
	


   END ;
    $$ LANGUAGE 'plpgsql';




CREATE OR REPLACE FUNCTION fct_setAllActualStock( ) returns BOOL  AS $$

 
    DECLARE
        sSql    text ;
        sSql2  text;
	actual_value float ;
    fActual_reserved float ;
    fActual_netto float;

    fTotal_sum float;
    fTotal_reserved float;
    fTotal_netto float;


	stockID integer ;
	articleID integer ;
	iID integer ;
	iMaxID integer ;
	i integer ;
	j integer ;
    ok bool ;

	actual_id integer ;
	last_stockvalue  float ;
    last_reservedvalue float ;
    last_nettovalue float ;
    this_reserved_bit integer;

	toEmbed float ;
	rollOut float ;
 	cur1 refcursor ;
	newStockvalue float ;
	
    BEGIN
	ok := true ;
	select into stockID max(id) from stocks ;
	raise notice $$' max StockID = % $$', stockID ;

	select into articleID max(id) from articles ;
	raise notice $$' max articleID = % $$', articleID ;

	select into iMaxID max(id) from stock_goods ;
	raise notice $$' max goods ID = % $$', iMaxID ;

   
	FOR i IN 1..stockID LOOP

	    FOR j in 1..articleID LOOP
	    	-- RAISE NOTICE $$'i,j = % %$$',i,j ;
    
            fTotal_sum = fct_get_TotalSum(  j  , i ) ;
            fTotal_reserved = fct_get_ReservedSum( j,i) ;
 
		    sSql = $$'select id from stock_goods where stock_id = $$' || i || $$' and article_id = $$' || j ||  $$' $$' ||  fct_getWhere(2,$$'$$') || $$' order by date_of_change, id $$';
		    OPEN cur1 FOR EXECUTE sSql ;
        	FETCH cur1 INTO actual_id ;
		    WHILE FOUND LOOP
		        select into last_stockvalue fct_get_lastStockValue(j,i,actual_id);
                select into last_reservedvalue fct_get_lastReservedValue(j,i,actual_id);

		        if last_stockvalue is null then
	   	      	    last_stockvalue := 0.00 ;
		        end if ;
		        select into toEmbed, rollOut to_embed,roll_out from stock_goods where id = actual_id ;
		        if toEmbed is null then
	   	      	    toEmbed := 0.00 ;
		        end if ;

		        if rollOut is null then
	   	      	    rollOut := 0.00 ;
		        end if ;

		        -- RAISE NOTICE $$'last value = %, actual_id = %, to_embed = % $$',last_stockvalue, actual_id, toEmbed ;
		        newStockvalue =  last_stockvalue + toEmbed - rollOut ;
		        -- RAISE NOTICE $$'last value = %, actual_id = %, to_embed = %, newStock = % $$',last_stockvalue, actual_id, toEmbed, newStockvalue ;
			
		        update stock_goods set actual_stock = newStockvalue where id = actual_id ;  

                sSql2 := $$'select reserved_bit from stock_goods where id = $$' || actual_id ;
                execute sSql2 into this_reserved_bit ;

                raise notice $$'this_reserved_bit = %$$', this_reserved_bit ;
                if this_reserved_bit IS NULL THEN
                    this_reserved_bit = 0;
                END IF ;

                if this_reserved_bit = 1 then 
                    update stock_goods set actual_reserved = last_reservedvalue +  toEmbed - rollOut  where id = actual_id ;
                else
                    update stock_goods set actual_reserved = last_reservedvalue  where id = actual_id  ;
                end if ;
                update stock_goods set actual_reserved = last_reservedvalue where id = actual_id   ;
                fActual_netto = newStockvalue - last_reservedvalue ; 
                update stock_goods set actual_netto = fActual_netto where id = actual_id   ;

                update stock_goods set total_sum = fTotal_sum where  article_id = j;
                update stock_goods set reserved_sum = fTotal_reserved where  article_id = j;
                fTotal_netto = fTotal_sum - fTotal_reserved ;
                update stock_goods set netto_sum = fTotal_netto where  article_id = j;


		       FETCH cur1 INTO actual_id ;
		    END LOOP ;
		close cur1 ;

	    END LOOP;

	END LOOP;
	
    	return ok ;	



   END ;
    $$ LANGUAGE 'plpgsql';

 DROP FUNCTION fct_stock_goods_insert() CASCADE ;
 
 CREATE OR REPLACE FUNCTION fct_stock_goods_insert() returns OPAQUE as $$

  DECLARE
    f_upd     varchar(400);
    v_delete varchar(20) ;
    v_actual_stock float;
    
    BEGIN

	v_actual_stock =  fct_get_lastStockValue( NEW.article_id,NEW.stock_id,NEW.id);
	v_actual_stock = v_actual_stock + to_embed - roll_out ;
	
        f_upd := $$'  update $$' || TG_RELNAME || $$' set actual_stock = $$' || v_actual_stock || $$'  where  id = $$' || NEW.id   ;

        -- RAISE NOTICE $$' table-name =  % $$', TG_RELNAME ;
        -- RAISE NOTICE $$' sql =  % $$',f_upd  ;
        -- RAISE NOTICE $$' Name =  % $$', TG_NAME ;
        execute f_upd ;
        RETURN NEW ;
    END;


     $$ LANGUAGE 'plpgsql';




CREATE OR REPLACE FUNCTION fct_get_sellingprice( iArticleID integer, iPricegroup integer) returns  float AS $$

 
    DECLARE
        sSql    text ;
        fPrice float ;
        iAddressID integer ;
        recData record ;
        pricegroupID integer ;
        cur1 refcursor ;
	 
    BEGIN 
     fPrice = 0.00;
     sSql = $$'select sellingprice$$'||iPricegroup||$$' as Price  from articles where id = $$' || iArticleID ;
     raise notice $$'sSql1 = %$$',sSql ;
     execute (sSql) into fPrice ;
     raise notice $$'Price1 = %$$',fPrice ;
     IF fPrice IS NULL then
     	if iPricegroup > 1 then 
	   sSql = $$'select sellingprice1 as Price  from articles where id = $$' || iArticleID ;
           raise notice $$'sSql2 = %$$',sSql ;
           
	   execute (sSql) into fPrice ;
	   raise notice $$'Price2 = %$$',fPrice ;	     
        END IF;
      END IF;

      IF fPrice IS NULL then 
      	 fPrice = 0.00 ;
      END IF;

     return fPrice ;
    END ;
    $$ LANGUAGE 'plpgsql'; 

CREATE OR REPLACE FUNCTION fct_get_price_for_pricegroup(  sModul varchar, iModulID integer, iArticleID integer) returns  float AS $$
 
    DECLARE
        sSql    text ;
        fPrice float ;
        iAddressID integer ;
        recData record ;
        pricegroupID integer ;
        
    BEGIN 
        fPrice := 0.00 ;
           IF sModul = $$'grave$$' THEN
               sSql := $$' select pricegroup1, pricegroup2,pricegroup3,pricegroup4,pricegroup_none from grave where grave.id = $$' || iModulID ;
            ELSEIF sModul = $$'orderposition$$' THEN
               sSql := $$' select pricegroup1, pricegroup2,pricegroup3,pricegroup4,pricegroup_none from orderbook where id = $$' || iModulID ;
               
            END IF ;
            execute(sSql) into recData ;
            
            if recData.pricegroup1 IS NULL then
                pricegroupID :=5 ;
            ELSEIF recData.pricegroup1 = TRUE THEN 
                pricegroupID :=1 ;
            ELSEIF recData.pricegroup2 = TRUE THEN 
                pricegroupID :=2 ;
            ELSEIF recData.pricegroup3 = TRUE THEN 
                pricegroupID :=3 ;
            ELSEIF recData.pricegroup4 = TRUE THEN 
                pricegroupID :=4 ;
            ELSEIF recData.pricegroup_none = TRUE THEN 
                pricegroupID :=5 ;
            ELSE 
                pricegroupID :=5 ;
            END IF ;
            
            raise notice $$' pricegroup id = % $$',pricegroupID ;
            
            
            
            IF pricegroupID = 5 then
                
            IF sModul = $$'grave$$' THEN
                select into recData addressid from grave where id = iModulID ;
                -- raise notice $$' address id = % $$',recData.addressid ;
                iAddressID = recData.addressid ;
            ELSEIF sModul = $$'orderposition$$' THEN
                select into recData addressnumber from orderbook  where orderbook.id = iModulID ;
                -- raise notice $$' address id = % $$',recData.addressnumber;
                iAddressID = recData.addressnumber ;
            END IF ;
            
            
                
                
                
                select into recData pricegroup1, pricegroup2,pricegroup3,pricegroup4,pricegroup_none from addresses_misc where address_id = iAddressID ;
                -- raise notice $$' pricegroup by address = %, % , %$$',recData.pricegroup1, recData.pricegroup2,recData.pricegroup3;
                if recData.pricegroup1 IS NULL then
                    pricegroupID :=1 ;
                ELSEIF recData.pricegroup1 = TRUE THEN 
                    pricegroupID :=1 ;
                ELSEIF recData.pricegroup2 = TRUE THEN 
                    pricegroupID :=2 ;
                ELSEIF recData.pricegroup3 = TRUE THEN 
                    pricegroupID :=3 ;
                ELSEIF recData.pricegroup4 = TRUE THEN 
                    pricegroupID :=4 ;
                ELSEIF recData.pricegroup_none = TRUE THEN 
                    pricegroupID :=5 ;
                ELSE 
                    pricegroupID :=1 ;
                END IF ;
            END IF ;
        
        -- raise notice $$' pricegroup id last value = % $$',pricegroupID ;
        
        if pricegroupID < 5 then 
            sSql = $$'select sellingprice$$'||pricegroupID||$$' as price from articles where id = $$' || iArticleID ;
            -- raise notice $$' sql  = % $$',sSql ;
            
            for recData in execute sSql 
                LOOP 
            END LOOP ;
        
            -- raise notice $$' price = % $$', recData.price ;
         
            fPrice := recData.price ;
            if fPrice IS NULL THEN 
                fPrice := 0.00 ;
            END IF ;
            -- raise notice $$' price = % $$',fPrice ;
        END IF ;
         
        return fPrice ;
    END ;
    $$ LANGUAGE 'plpgsql'; 

    
CREATE OR REPLACE FUNCTION fct_get_new_tax_vat_for_article_1(iArticleID int) returns float as $$
 DECLARE
    sSql text   ;
     r0 record ;
    iNewTaxVat int ;
    sNewTaxVat text ;
    iMGroup record ;
    aMGroups int[] ;
    aMGroupsString text[] ;
    
    sMGroups text ;
    iMGroupsTaxVat int ;
    iClient int ;
    i int ;
    fTaxVat float ;
    
    BEGIN
        iNewTaxVat = -1;
        fTaxVat := 0.00 ;
        
        iClient = fct_getUserDataClient(  ) ;
        sMGroups = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'order_grave_materialgroups_for_tax_vat_1$$') ;
        sNewTaxVat = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'order_grave_materialgroups_new_tax_vat_id_1$$') ;
        
        sSql := $$'select articles.material_group  from articles where articles.id = $$' || iArticleID  ;
        execute(sSql) into r0 ;
       
        aMGroupsString =  string_to_array(sMGroups, $$',$$') ; 
        
        IF r0.material_group IS NOT NULL THEN 
            FOR i IN 1..array_length(aMGroupsString, 1) LOOP
                IF r0.material_group = aMGroupsString[i]::INTEGER THEN 
                    iNewTaxVat =sNewTaxVat::integer ;
                END IF ;
            END LOOP; 
  
        END IF ;
    
        -- IF iNewTaxVat > -1 THEN 
        --     select into fTaxVat vat_value from tax_vat where id = iNewTaxVat ;
        -- END IF ;
        
        if iNewTaxVat = -1 then
            iNewTaxVat = 0 ;
        end if ;
        return iNewTaxVat::float;
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
    
DROP FUNCTION     fct_get_taxvat_for_article(int) ;

CREATE OR REPLACE FUNCTION fct_get_taxvat_for_article( iArticleID integer) returns  return_misc3 as $$
 
    DECLARE
        sSql    text ;
        fTaxVat float ;
        iTaxVatArticle integer ;
        rData  record ;
        rValue return_misc3 ;
    BEGIN 
        fTaxVat := 0.00;
    
        select into rData tax_vat_id from articles where id = iArticleID ;
        IF rData.tax_vat_id IS NULL then
            iTaxVatArticle = 0;
        ELSE
            iTaxVatArticle = rData.tax_vat_id ;
        END IF ;
        
        IF iTaxVatArticle= 0 THEN
            select into rData  material_group.tax_vat from articles,material_group where articles.id = iArticleID and articles.material_group = material_group.id ;
            IF rData.tax_vat IS NULL then
                iTaxVatArticle = 0;
            ELSE
                iTaxVatArticle = rData.tax_vat ;
            END IF ;
        END IF ;
        IF iTaxVatArticle>0 then
            select into rData vat_value, vat_acct1 from tax_vat where id = iTaxVatArticle;
            fTaxVat = rData.vat_value ;
            if rData.vat_acct1 is null OR char_length(rData.vat_acct1) = 0 then 
                rValue.c = $$'38010$$' ;
            else 
                rValue.c = rData.vat_acct1 ;
            end if ;
        else
        
            fTaxVat = 0.00 ;
            rValue.c = $$'38010$$' ;
            
        END IF;

        rValue.a = iTaxVatArticle ;
        rValue.b = fTaxVat ;
        
        
        return rValue ;
    END ;
    $$ LANGUAGE 'plpgsql'; 

       
    
CREATE OR REPLACE FUNCTION fct_get_net_for_article( iArticleID integer) returns  bool as $$
 
    DECLARE
        sSql    text ;
        bNet bool ;
        net bool ;
        rData  record ;
    BEGIN 
        bNet := true ;
        raise notice $$' net value is %$$',bNet ;
        select into rData price_type_net from material_group, articles where articles.id = iArticleID and articles.material_group = material_group.id ;
        
        if rData.price_type_net is null then 
            bNet := true ;
        else
            bNet := rData.price_type_net ;
        end if ;
        
        raise notice $$' net value is %$$',bNet ;
       return bNet ;
    END ;
    $$ LANGUAGE 'plpgsql'; 

    
    
    
CREATE OR REPLACE FUNCTION  fct_duplicateArticle( iArticleID integer) returns  integer as $$
    DECLARE
        sSql    text ;
        newID integer ;
         rData  record ;
         
    BEGIN 
        newID = 0 ;
        
        select nextval($$'articles_id$$') into newID ;
         
        select into rData * from articles where articles.id = iArticleID ;
        
        insert into articles (id , client, status, sep_info_1 , sep_info_2 , sep_info_3 , number , designation  , wrapping , quantumperwrap , unit , manufactor_id , weight , sellingprice1 , sellingprice2 , sellingprice3 , sellingprice4 , tax_vat , material_group , associated_with , associated_id , tax_vat_id ) values (newID ,  rData.client, $$'insert$$', rData.sep_info_1 , rData.sep_info_2 , rData.sep_info_3 , $$'NEW-$$' || rData.number , rData.designation  , rData.wrapping , rData.quantumperwrap , rData.unit , rData.manufactor_id , rData.weight , rData.sellingprice1 ,  rData.sellingprice2 ,  rData.sellingprice3 ,  rData.sellingprice4 , rData. tax_vat ,  rData.material_group ,  rData.associated_with , rData.associated_id ,  rData.tax_vat_id );
        
        
        return newID ;
    END ;
    $$ LANGUAGE 'plpgsql'; 

    
    
CREATE OR REPLACE FUNCTION  fct_getSellingPricesForArticle ( iArticleID integer, iPartID integer) returns   record as $$

DECLARE
        sSql    text ;
        rData  record ;
        rData2  record ;
         
    BEGIN 
    
    sSql = $$'select apl.quantities, sellingprice1, sellingprice2, sellingprice3, sellingprice4, 0.0::float as t1, 0.0::float as t2,0.0::float as t3,0.0::float as t4 from articles, articles_parts_list as apl  where articles.id = $$' || iArticleID || $$' and  articles.id = apl.part_id and apl.article_id = $$' || iPartID || $$' $$' || fct_getWhere(2,$$'apl.$$') ; 
    execute(sSql) into rData ;
    
    if rData.sellingprice1 IS NULL then
        rData.sellingprice1 := 0.0::float  ;
    end if ;
    
    if rData.sellingprice2 IS NULL then
        rData.sellingprice2 := 0.0::float  ;
    end if ;
      if rData.sellingprice3 IS NULL then
        rData.sellingprice3 := 0.0::float  ;
    end if ;
      if rData.sellingprice4 IS NULL then
        rData.sellingprice4 := 0.0::float  ;
    end if ;
    
    rData.t1 =  rData.sellingprice1 * rData.quantities ;
    rData.t2 =  rData.sellingprice2 * rData.quantities ;
    rData.t3 =  rData.sellingprice3 * rData.quantities ;
    rData.t4 =  rData.sellingprice4 * rData.quantities ; 
  
    return  rData ;
    
    END ;
    
    $$ LANGUAGE 'plpgsql'; 

    
  
CREATE OR REPLACE FUNCTION  fct_getTotalSellingPricesForArticle ( iPartID integer) returns   record as $$

    DECLARE
        sSql    text ;
        rData  record ;
        rData2  record ;
        ft1 float ;
        ft2 float ;
        ft3 float ;
        ft4 float ;
        
    BEGIN 
     ft1 := 0.00 ;
     ft2 := 0.00 ;
     ft3 := 0.00 ;
     ft4 := 0.00 ;
     
     
    sSql := $$'select  apl.quantities, sellingprice1, sellingprice2, sellingprice3, sellingprice4, 0.0::float as t1, 0.0::float as t2,0.0::float as t3,0.0::float as t4 from articles,  articles_parts_list as apl where articles.id = apl.part_id and apl.article_id = $$' || iPartID || $$' $$' || fct_getWhere(2,$$'apl.$$') ;
    
    raise notice $$' sSql = %$$', sSql ;
    
    FOR rData in execute(sSql) LOOP 
    
        if rData.sellingprice1 IS NOT NULL then
            raise notice $$'sellingprice 1 = % , %  %$$', rData.sellingprice1, rData.quantities, rData.t1 ;
            ft1 :=  ft1 + (rData.sellingprice1 * rData.quantities );
             raise notice $$'sellingprice 1 = % , % $$', rData.sellingprice1, rData.t1 ;
        end if ;
        if rData.sellingprice2 IS NOT NULL then
            ft2 :=  ft2 + (rData.sellingprice2 * rData.quantities );
        end if ;
        if rData.sellingprice3 IS NOT NULL then
            ft3 :=  ft3 + (rData.sellingprice3 * rData.quantities );
        end if ;
        if rData.sellingprice4 IS NOT NULL then
            ft4 :=  ft4 + (rData.sellingprice4 * rData.quantities );
        end if ;
        
        
        
        
    END LOOP ;
    
        rData.t1 := ft1 ;
        rData.t2 := ft2 ;
        rData.t3 := ft3 ;
        rData.t4 := ft4 ;
        
        return  rData ;
    END ;
    
    $$ LANGUAGE 'plpgsql'; 



     
CREATE OR REPLACE FUNCTION  fct_getBarcodeForArticleID ( iID integer ) returns  text as $$
    DECLARE
	
	b1 text; 
    	sSql text;


    BEGIN

	sSql := $$'select barcode from articles_barcode where article_id = $$' || iID ;
	execute sSql into b1 ;
	if b1 is null then
	   b1 = $$'$$';
	end if ;

	 
    RETURN b1 ;
END;

  $$ LANGUAGE 'plpgsql'; 



     
CREATE OR REPLACE FUNCTION  fct_getBarcodeText ( sArtikel text) returns   text as $$
    DECLARE
	
	code128 text; 
    	chaine text;
    BEGIN
	code128 := $$'$$';
	chaine = sArtikel ;
  	code128 =  fct_getBarcode(chaine);

    RETURN code128;
END;

  $$ LANGUAGE 'plpgsql'; 

  
CREATE OR REPLACE FUNCTION  fct_getBarcodeInt ( iArtikel integer) returns   text as $$    
    DECLARE

	code128 text; 
    	chaine text;
    BEGIN
	code128 := $$'$$';
	chaine := to_char(iArtikel,$$'00000000000$$') ;

  	code128 =  fct_getBarcode(chaine);

    RETURN code128;
END;

  $$ LANGUAGE 'plpgsql'; 







DROP FUNCTION   fct_getBarcode(int)   ;

CREATE OR REPLACE FUNCTION  fct_getBarcode ( sNumber text) returns   text as $$


/* Fonction pour transformer une chaine de caractere en chaine code 128 en PL/pgsql 
Traduction de la fonction VB de Grandzebu par Nicolas Fanchamps en PL/SQL 
Portage de PL/SQL vers PL/pgsql (postgresql 7.4.2) par Gerald Salin, le 07/10/05 

PRE-REQUIS : installation du langage plpgsql dans la base ou sera utilisee la fonction
HOWTO : 
- j ai cree la fonction plSQL via l interface de creation de fonction de phpPgAdmin
- specifier le nom de la fonction
- 1 seul argument de type text
- 1 seul retour de type text
- langage plpgsql
- copier/coller tout ce code dans la partie definition
- valider

on peut appeler la fonction par la commande : select myfonction($$'texte a encoder$$')
*/

DECLARE
  ind integer;         /* Indice d avancement dans la chaine de caractere */
  chaine text; /*recuperation de l argument dans une variable*/
  longueur integer;    /* longueur de la chaine passee en argument */
  checksum integer;    /* Caractere de verification de la chaine codee */
  mini integer;        /* nbr de caracteres numeriques en suivant */
  dummy integer;       /* Traitement de 2 caracteres a la fois */
  tableB BOOLEAN;      /* Booleen pour verifier si on doit utiliser la table B du code 128 */
  code128 text;        /* code 128 de l argument */

BEGIN
  ind:=1; 
  chaine =  sNumber ;
 
  code128 := $$'$$';
  
  longueur := LENGTH(chaine); /* recupere la longueur de la chaine passee en argument */

 IF longueur < 1
    THEN
      RAISE EXCEPTION $$' Argument Absent!!!$$';
    ELSE 
    FOR  ind  IN 1 .. longueur --test de la validite des caracteres composant l argument
      LOOP
        IF (ASCII(SUBSTR(chaine, ind, 1)) < 32) OR (ASCII(SUBSTR(chaine, ind, 1)) > 126)
          THEN
            RAISE EXCEPTION $$' Argument invalide!!!$$';
        END IF;
      END LOOP;
  END IF;
  tableB := TRUE;
  WHILE ind <= longueur
    LOOP

      IF (tableB = TRUE)
        THEN
          --Voir si cest interessant de passer en table C
          --Oui pour 4 chiffres au debut ou a la fin, sinon pour 6 chiffres
          IF ((ind = 1) OR (ind+3 = longueur))
            THEN mini := 4;
          ELSE
            mini := 6;
          END IF;

          --TestNum : si les mini caracteres a partir de ind son numeriques, alors mini = 0
          mini := mini-1;
          IF ((ind + mini) <= longueur)
            THEN
              WHILE mini >= 0
              LOOP
                IF (ASCII(SUBSTR(chaine, ind+mini , 1)) < 48) OR (ASCII(SUBSTR(chaine, ind+mini, 1)) > 57)
                  THEN EXIT;
                END IF;
                mini := mini-1;
              END LOOP;
          END IF;

          --Si mini < 0 on passe en table C
          IF (mini < 0)
            THEN
              IF (ind = 1)
                THEN --Debuter sur la table C
                  code128 := CHR(210);
              ELSE --Commuter sur la table C
                code128 := code128 || CHR(204);
              END IF;
              tableB := FALSE;
          ELSE
            IF (ind = 1)
              THEN --Debuter sur la table B
                code128 := CHR(209);
            END IF;
          END IF;
 END IF;

      IF (tableB = FALSE)
        THEN --On est sur la table C, on va essayer de traiter 2 chiffres
          mini := 2;
          mini := mini-1;
          IF (ind + mini <= longueur)
            THEN
              WHILE mini >= 0
              LOOP
                
                IF (ASCII(SUBSTR(chaine, ind+mini , 1)) < 48) OR (ASCII(SUBSTR(chaine, ind+mini, 1)) > 57)
                  THEN EXIT;
                END IF;
                mini := mini-1;
              END LOOP;
          END IF;

          IF (mini < 0)
            THEN --OK Pour 2 chiffres, les traiter

              dummy := SUBSTR(chaine, ind, 2)::integer;
              IF (dummy < 95)
                THEN
                  dummy := dummy + 32;
              ELSE
                dummy := dummy + 100;
              END IF;
            code128 := code128 || CHR(dummy);
            ind := ind + 2;
          ELSE
            --On a pas deux chiffres, retourner en table B
            code128 := code128 || CHR(205);
            tableB := TRUE;
          END IF;

      END IF;

      IF (tableB = TRUE)
        THEN
          code128 := code128 || SUBSTR(chaine, ind, 1);
          ind := ind + 1;
      END IF;

    END LOOP;
    --Calcul de la clef de controle
    FOR ind IN 1 .. LENGTH(code128)
      LOOP
        dummy := ASCII(SUBSTR(code128, ind, 1));
        IF (dummy < 127)
          THEN
            dummy := dummy - 32;
        ELSE
          dummy := dummy - 100;
        END IF;

        IF (ind = 1)
          THEN
            checksum := dummy;
        END IF;

        checksum := mod(checksum + (ind-1) * dummy, 103);
      END LOOP;

      --Calcul du code ascii de la clef de controle
    IF (checksum < 95)
      THEN
        checksum := checksum + 32;
    ELSE
      checksum := checksum + 100;
    END IF;

    
    code128 := code128 || CHR(checksum) || CHR(211);


RETURN code128;
END;

  $$ LANGUAGE 'plpgsql'; 




CREATE  OR REPLACE FUNCTION sh_savePic(filename text, image text) RETURNS text AS $$
#!/bin/bash
if [ !  -d "temp" ]; then
   mkdir "temp"
fi
cd temp


filename2=$(uuidgen)
returnfilename=$filename2filename



echo $returnfilename 




$$ LANGUAGE 'plsh' ;


