
 CREATE OR REPLACE FUNCTION fct_update( ) returns OPAQUE AS '
    --  set default values at an update operation 
     
    DECLARE
        history_table  varchar(400) ;
    BEGIN
        history_table = TG_TABLE_NAME || "_history" ;
        RAISE NOTICE ''History table  =  % '', history_table ;
       
        if OLD.versions_number is NULL  then 
                OLD.versions_number = 0 ;
        end if ;  
        if not OLD.status = ''delete'' then 
      
            NEW.update_user_id = current_user   ;
            NEW.update_time = (select  now()) ;
            NEW.user_id = OLD.user_id;
            
            NEW.insert_time = OLD.insert_time ;
            NEW.status = ''update'' ;
            RAISE NOTICE ''Name =  % '', TG_NAME ;
            NEW.versions_number = OLD.versions_number+1 ;
            
      
           
            
            RETURN NEW; 
        else 
            RETURN OLD;
        end if ;
      
    END;
    
  
    ' LANGUAGE 'plpgsql'; 
