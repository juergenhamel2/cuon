  CREATE OR REPLACE FUNCTION fct_setErp1( ) returns  bool as $$
 
     DECLARE
    sSql    text ;
    sSql2 text ;
    sSql3 text ;
    r record ;
    bOK bool ;
    recAddress record ;
    recOrder record ;
    orderAge int ;
    iClient int;

    sum_sales float;
    iSum int ;

    iSum_1 int;
    iSum_2 int;
    iSum_3 int;
    iSum_4 int;
    iSum_5 int;
    

    erp_orderbook_1 text;
    erp_orderbook_2 text;
    erp_orderbook_3 text;
    erp_orderbook_4 text;
    erp_orderbook_5 text;

    erp_proposal_1 text;
    erp_proposal_2 text;
    erp_proposal_3 text;    
    erp_proposal_4 text;    
    erp_proposal_5 text;
    
erp_address_newer_then_1 text;
erp_address_newer_then_2 text;
erp_address_newer_then_3 text;
erp_address_newer_then_4 text;


erp_schedule_newer_then_1 text;
erp_schedule_newer_then_2 text;
erp_schedule_newer_then_3 text;
erp_schedule_newer_then_4 text;

erp_orderbook_1_age int ;
erp_orderbook_1_sales int ;
erp_orderbook_1_erp int ;


erp_orderbook_2_age int ;
erp_orderbook_2_sales int ;
erp_orderbook_2_erp int ;


erp_orderbook_3_age int ;
erp_orderbook_3_sales int ;
erp_orderbook_3_erp int ;


erp_orderbook_4_age int ;
erp_orderbook_4_sales int ;
erp_orderbook_4_erp int ;


erp_orderbook_5_age int ;
erp_orderbook_5_sales int ;
erp_orderbook_5_erp int ;


erp_address_newer_then_1_age int;
erp_address_newer_then_1_erp  int;

erp_address_newer_then_2_age int;
erp_address_newer_then_2_erp  int;

erp_address_newer_then_3_age int;
erp_address_newer_then_3_erp  int;

erp_address_newer_then_4_age int;
erp_address_newer_then_4_erp  int;



    BEGIN
    bOK = false ;
    -- first get the config options
    
     iClient = fct_getUserDataClient(  ) ;
     erp_orderbook_1 = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_orderbook_1$$' ) ;
     raise notice $$'erp_orderbook_1 = % $$',erp_orderbook_1 ;
     erp_orderbook_1_age = split_part(erp_orderbook_1,$$',$$',1) ;
     
     erp_orderbook_1_sales  = split_part(erp_orderbook_1,$$',$$',2) ;
     erp_orderbook_1_erp = split_part(erp_orderbook_1,$$',$$',3)   ;
     raise notice $$'erp_orderbook_1 values = %, % , % $$',erp_orderbook_1_age,erp_orderbook_1_sales,erp_orderbook_1_erp ;

     erp_orderbook_2 = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_orderbook_2$$' ) ;

     erp_orderbook_2_age = split_part(erp_orderbook_2,$$',$$',1) ;
     erp_orderbook_2_sales  = split_part(erp_orderbook_2,$$',$$',2) ;
     erp_orderbook_2_erp = split_part(erp_orderbook_2,$$',$$',3)   ;


     erp_orderbook_3 = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_orderbook_3$$' ) ;

     erp_orderbook_3_age = split_part(erp_orderbook_3,$$',$$',1) ;
     erp_orderbook_3_sales  = split_part(erp_orderbook_3,$$',$$',2) ;
     erp_orderbook_3_erp = split_part(erp_orderbook_3,$$',$$',3)   ;



     erp_orderbook_4 = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_orderbook_4$$' ) ;

     erp_orderbook_4_age = split_part(erp_orderbook_4,$$',$$',1) ;
     erp_orderbook_4_sales  = split_part(erp_orderbook_4,$$',$$',2) ;
     erp_orderbook_4_erp = split_part(erp_orderbook_4,$$',$$',3)   ;


     erp_orderbook_5 = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_orderbook_5$$' ) ;

     erp_orderbook_5_age = split_part(erp_orderbook_5,$$',$$',1) ;
     erp_orderbook_5_sales  = split_part(erp_orderbook_5,$$',$$',2) ;
     erp_orderbook_5_erp = split_part(erp_orderbook_5,$$',$$',3)   ;


     -- address
     erp_address_newer_then_1 =  fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_address_newer_then_1$$' ) ;
     erp_address_newer_then_1_age =  split_part(erp_address_newer_then_1,$$',$$',1) ;
     erp_address_newer_then_1_erp =  split_part(erp_address_newer_then_1,$$',$$',2) ;

     raise notice $$'erp_address_newer_then_1 = % $$',erp_address_newer_then_1 ;
   erp_address_newer_then_2 =  fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_address_newer_then_2$$' ) ;
     erp_address_newer_then_2_age =  split_part(erp_address_newer_then_2,$$',$$',1) ;
     erp_address_newer_then_2_erp =  split_part(erp_address_newer_then_2,$$',$$',2) ;

   erp_address_newer_then_3 =  fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_address_newer_then_3$$' ) ;
     erp_address_newer_then_3_age =  split_part(erp_address_newer_then_3,$$',$$',1) ;
     erp_address_newer_then_3_erp =  split_part(erp_address_newer_then_3,$$',$$',2) ;

         
   erp_address_newer_then_4 =  fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'erp_address_newer_then_4$$' ) ;
     erp_address_newer_then_4_age =  split_part(erp_address_newer_then_4,$$',$$',1) ;
     erp_address_newer_then_4_erp =  split_part(erp_address_newer_then_4,$$',$$',2) ;


     -- clear the cuon_erp1 table
     delete from cuon_erp1 ;
 
    sSql = $$'select id,insert_time  from address '$$ ||  fct_getWhere(1,$$' $$') ;
    -- raise notice $$'ssql = % $$',sSql ;
    for recAddress in execute sSql
	 LOOP
	 -- set all erp_newsletter to 0
	 insert into cuon_erp1 (id, address_id, erp_newsletter) values (nextval($$'cuon_erp1_id$$'),recAddress.id,0);
	  -- raise notice $$'id from address for erp1 =  %$$',recAddress.id ;
	  sSql2 = $$'select id from orderbook where addressnumber = $$' || recAddress.id || $$' $$' || fct_getWhere(2,$$' $$') ;
	  -- raise notice $$'ssql = % $$',sSql2 ;
	  iSum = 0;
	  iSum_1 = 0;
	   iSum_2 = 0 ;
         iSum_3 = 0;
         iSum_4 =0;
         iSum_5 = 0;
    
	  for recOrder in execute sSql2
	      LOOP
      	     	-- raise notice $$'Age of order %, id =% $$',orderAge, recOrder.id ;		      
		sSql3 = $$'select  * from fct_check_age_of_order($$' || recOrder.id || $$') $$';
		execute sSql3 into orderAge ;
		
		if orderAge <=  erp_orderbook_5_age then
		   -- get the sales from this order
		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_1_age*30)));
		   -- raise notice $$' sum sales 1 = % $$', sum_sales ;

	           iSum_1 = iSum_1 +  sum_sales::int ;
	           raise notice $$' iSum 1 = % $$', iSum_1 ;
		   
		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_2_age*30)));
		   -- raise notice $$' sum sales 2 = % $$', sum_sales ;
		  
		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_3_age*30)));
		   -- raise notice $$' sum sales 3 = % $$', sum_sales ;
		
		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_4_age*30)));

		   sum_sales = fct_get_sales_of_order( recOrder.id,date(current_date - (erp_orderbook_5_age*30)));
		   -- raise notice $$' sum sales 5 = % $$', sum_sales ;
	
		   
		end if ;
		
		END LOOP ;
		 if iSum_1 > erp_orderbook_1_sales then
		      iSum = iSum +  erp_orderbook_1_erp::int ;
		      raise notice $$' iSum 1 = % $$', iSum_1 ;
		   
		   end if ;
		   
		-- raise notice $$' diff date = %$$',date(recAddress.insert_time) - date(current_date - erp_address_newer_then_1_age*30) ;
		-- raise notice $$' diff date 2 = %$$',date(recAddress.insert_time) - date(current_date - erp_address_newer_then_2_age*30) ;
		if ((date(recAddress.insert_time) - date(current_date - erp_address_newer_then_1_age*30) ))::int  > 0   then
		   iSum = iSum + erp_address_newer_then_1_erp::int ;
		   -- raise notice $$' insert time 1 = % $$', iSum ;
		   
		elsif ((date(recAddress.insert_time) - date(current_date - erp_address_newer_then_2_age*30) ))::int  > 0   then
		   iSum = iSum + erp_address_newer_then_2_erp::int ;
		   -- raise notice $$' insert time 2 = % $$', iSum ;
		elsif ((date(recAddress.insert_time) - date(current_date - erp_address_newer_then_3_age*30) ))::int  > 0   then
		   iSum = iSum + erp_address_newer_then_3_erp::int ;
		   -- raise notice $$' insert time 3 = % $$', iSum ;
		   
		elsif ((date(recAddress.insert_time) - date(current_date - erp_address_newer_then_4_age*30) ))::int  > 0   then
		   iSum = iSum + erp_address_newer_then_4_erp::int ;
		   -- raise notice $$' insert time 4 = % $$', iSum ;
                end if  ;


		-- update cuon_erp1
		update cuon_erp1 set erp_newsletter = iSum where address_id = recAddress.id ;
		
	 END LOOP;
	     
	     
    
    	 
    return bOK  ;
    END ;
    $$ LANGUAGE 'plpgsql'; 
 
CREATE OR REPLACE FUNCTION fct_getAddressFashion(  iFashionValue int, iAddressID int) returns  bool as $$
 
     DECLARE
    sSql    text ;
    r record ;
    bOK bool ;
    
    
    BEGIN
    bOK := False ;
    sSql := $$' select cb_fashion from addresses_misc where address_id = $$' || iAddressID || fct_getWhere(2,$$' $$') ;
    execute(sSql) into r ;

    if r.cb_fashion is not null AND r.cb_fashion > 0 then 
        bOK = True ;
    end if ;

    
    return bOK  ;
       END ;
    $$ LANGUAGE 'plpgsql'; 


 CREATE OR REPLACE FUNCTION fct_getAddress(  iAddressID int) returns  record as $$
 
     DECLARE
    sSql    text ;
    recAddress  record;
    
    
    
    BEGIN
    sSql := $$'select address, lastname, lastname2,firstname, street, zip, city,city as cityfield,  
        state, country from address 
        where id = $$' || iAddressID  || $$' $$' || fct_getWhere(2,$$' $$') ;
    for recAddress in execute sSql 
    LOOP 
        raise notice $$'addresslastnmae = %$$',recAddress.lastname ;
        raise notice $$'address = %$$',recAddress.address ;
        --raise notice $$'cityfield = %$$',recAddress.cityfield ;
       
        if recAddress.country is NULL then
            recAddress.cityfield := recAddress.zip || $$' $$' || recAddress.city ;
        else 
            recAddress.cityfield := recAddress.country || $$'-$$' || recAddress.zip || $$' $$' || recAddress.city ;
        END IF ;
        --recAddress.country := fct_getChar(recAddress.country) ;
        raise notice $$'country = %$$',recAddress.country ;
       raise notice $$'cityfield = %$$',recAddress.cityfield ;
       
    END LOOP ;
    return recAddress  ;
       END ;
    $$ LANGUAGE 'plpgsql'; 
 

drop function  fct_getLastname (int);
CREATE OR REPLACE FUNCTION fct_getLastname(  iAddressID2 int) returns  text as $$
 
     DECLARE
    sSql    text ;
    recAddress  record;
    sLastname text ;
    
   
    iAddressID int ;

    BEGIN

    iAddressID = iAddressID2 ;
	
    if iAddressID is NULL then 
       iAddressID = 0;
    end if ;

    sSql := $$'select lastname from address 
        where id = $$' || iAddressID  || $$' $$' || fct_getWhere(2,$$' $$') ;
    for recAddress in execute sSql 
    LOOP 
        raise notice $$'addresslastnmae = %$$',recAddress.lastname ;
        
        if recAddress.lastname is NULL then
            sLastname := $$'NONE$$'  ;
        else 
            sLastname := recAddress.lastname;
        END IF ;
        
       
    END LOOP ;
    return sLastname ;
       END ;
    $$ LANGUAGE 'plpgsql'; 
 
 CREATE OR REPLACE FUNCTION fct_generatePartner(iAddressID int)    returns  int as $$
 
     DECLARE
    sSql    text ;
    sSql2   text ;
    recAddress  record;
    sLastname text ;
    iNewID int := 0 ;
    newUUID text ;
    
    BEGIN
    execute($$'select * from fct_new_uuid()$$') into newUUID ;
    raise notice $$'new uuid at partner = %$$',newUUID ;
    sSql := $$'select * from address where id = $$' || iAddressID  || $$' $$' || fct_getWhere(2,$$' $$') ;
    raise notice $$' sSql = %$$',sSql ;
    execute(sSql) into recAddress ;
    raise notice $$' rec = %$$',recAddress ;
    if recAddress.lastname is null then 
       recAddress.lastname = $$'$$';
    end if ;
    if recAddress.firstname is null then 
       recAddress.firstname = $$'$$';
    end if ;
    if recAddress.street is null then 
       recAddress.street = $$'$$';
    end if ;

    if recAddress.zip is null then 
       recAddress.zip = $$'$$';
    end if ;
    if recAddress.city is null then 
       recAddress.city = $$'$$';
    end if ;

    if recAddress.phone is null then 
       recAddress.phone = $$'$$';
    end if ;


    sSql2 := $$' insert into partner (id, uuid,lastname, firstname,street,zip,city,phone, addressid) values (nextval($$' || quote_literal($$'partner_id$$') || $$'),$$' || quote_literal(newUUID) || $$',$$' || quote_literal(recAddress.lastname) || $$',$$' || quote_literal(recAddress.firstname) || $$', $$' || quote_literal(recAddress.street) || $$', $$' || quote_literal(recAddress.zip) || $$', $$' || quote_literal(recAddress.city) || $$', $$'  || quote_literal(recAddress.phone) || $$', $$' || iAddressID || $$' ) $$' ;
    raise notice $$' sSql = %$$',sSql2 ;
    execute (sSql2);
    sSql := $$'select id from partner where uuid = $$' || quote_literal(newUUID) ;
    execute(sSql) into iNewID ;
    return iNewID ;
       END ;
    $$ LANGUAGE 'plpgsql';   

            
CREATE OR REPLACE FUNCTION fct_getSchedulTime(  iTimeBegin int, iTimeEnd int , aTimes char[] ,recId int) returns  varchar as $$
 
     DECLARE
    sSql    text ;
    sTime varchar;
    BEGIN 
        sTime := aTimes[iTimeBegin] || $$' - $$' ||  aTimes[iTimeEnd]  ;
        
    return sTime ;
       END ;
    $$ LANGUAGE 'plpgsql'; 



CREATE OR REPLACE FUNCTION fct_getAddressField(  iAddressID int) returns  record as $$
 
     DECLARE
    sSql    text ;
    recAddress  record;
    
    
    
    BEGIN
    
     sSql := $$'select id as address_id, address.address::text as address , address.firstname::text as firstname, address.lastname::text as lastname,
     address.lastname2::text as lastname2,  address.street::text as street, (address.zip  || $$'$$' $$'$$' ||  address.city)  as city ,
     (address.country  ||$$'$$' $$'$$'||  address.zip  || $$'$$' $$'$$'|| address.city)::text  as city_country , address.zip::text as zip,
     address.country::text as country,      address.city::text as city_alone,NULL::text as cityfield, NULL::text as first_last, NULL::text as last_first
     from  address  where id = $$' || iAddressID  || $$' $$' || fct_getWhere(2,$$' $$') ;
    for recAddress in execute sSql 
    LOOP 
        -- raise notice $$'addresslastnmae = %$$',recAddress.lastname ;
        -- raise notice $$'address = %$$',recAddress.address ;
        -- raise notice $$'cityfield = %$$',recAddress.cityfield ;
       
        if recAddress.country is NULL then
            recAddress.cityfield := recAddress.zip || $$' $$' || recAddress.city_alone ;
        else 
            recAddress.cityfield := recAddress.country || $$'-$$' || recAddress.zip || $$' $$' || recAddress.city_alone ;
        END IF ;
        --recAddress.country := fct_getChar(recAddress.country) ;
       -- raise notice $$'country = %$$',recAddress.country ;
       -- raise notice $$'cityfield = %$$',recAddress.cityfield ;
       if recAddress.firstname is NULL then
            recAddress.first_last := recAddress.lastname ;
            recAddress.last_first := recAddress.lastname ;
        else
            recAddress.first_last := recAddress.firstname ||$$' $$' || recAddress.lastname ;
            recAddress.last_first := recAddress.firstname ||$$' $$' || recAddress.lastname ;
        END IF ;
       
    END LOOP ;
    return recAddress  ;
       END ;
    $$ LANGUAGE 'plpgsql'; 
 
 
CREATE OR REPLACE FUNCTION fct_getSchedule1(short_key text,days int) returns  record as $$
 
     DECLARE
    sSql    text ;
    sSql2    text ;

    recAddress  record;
    recSchedule record;
    
    
    BEGIN
	sSql := $$'select $$'$$' $$'$$'::text as address, $$'$$' $$'$$'::text as firstname,$$'$$' $$'$$'::text as  lastname, $$'$$' $$'$$'::text as lastname2, $$'$$' $$'$$'::text as street,  $$'$$' $$'$$'::text as city,  $$'$$' $$'$$'::text as city_country,  $$'$$' $$'$$'::text as zip, $$'$$' $$'$$'::text as country,  $$'$$' $$'$$'::text as city_alone,  $$'$$' $$'$$'::text as cityfield, $$'$$' $$'$$'::text as first_last, $$'$$' $$'$$'::text as last_first,  address.id as address_id, schedul_date::text, dschedul_date, schedul_time_begin, $$'$$' $$'$$'::text as schedul_time_begin_text, partner_schedul.short_remark::text from partner_schedul,partner, address  where fct_getDays(fct_to_date(schedul_date))  = (-1 * $$' || days || $$') AND position($$' || quote_literal(short_key) || $$'in short_remark) > 0 AND partnerid = partner.id and address.id = addressid  $$' ||  fct_getWhere(2,$$'partner_schedul.$$') ;
	raise notice $$'sSql = %$$',sSql ;

	for recSchedule in execute sSql 
	LOOP
	
		if recSchedule.dschedul_date is NULL then 
		   recSchedule.dschedul_date = fct_to_date(recSchedule.schedul_date);
		END IF ;

		sSql2 = $$'select *  from fct_getAddressField($$' || recSchedule.address_id || $$') as ( address_id int, address text , firstname text , lastname text,lastname2 text ,street text , city text , city_country text , zip text , country text , city_alone text ,cityfield text, first_last text , last_first text) $$' ;
		raise notice $$'sSql2 = %$$',sSql2 ;
		execute sSql2 into recAddress ;

		recSchedule.firstname = recAddress.firstname ;
		recSchedule.lastname = recAddress.lastname ;		   
		recSchedule.lastname2 = recAddress.lastname2 ;
		recSchedule.street = recAddress.street ;
		recSchedule.zip = recAddress.zip ;
		recSchedule.city = recAddress.city ;
		
		recSchedule.schedul_time_begin_text = fct_getTimeFromCheckbox(	recSchedule.schedul_time_begin) ;

		return recSchedule ;
	
	END LOOP;

	
	return recAddress;

	
      
	
       END ;
    $$ LANGUAGE 'plpgsql'; 
 

CREATE or REPLACE FUNCTION VectorAdd(IN a float[], IN B float[], OUT c float[])
AS $$

#pragma PGOPENCL Platform : ATI Stream
#pragma PGOPENCL Device : CPU

__kernel __attribute__((reqd_work_group_size(64, 1, 1)))
void VectorAdd( __global const float *a, __global const float *b, __global float *c)
     {
	int i = get_global_id(0);
	c[i] = a[i] + b[i];
     }

$$Language PgOpenCL;


CREATE OR REPLACE FUNCTION fct_addresslist( searchfields text[] , sKey text) returns setof  record as $$
 
     DECLARE
    sSql    text ;
    sSql2    text ;

    sWhere text;
    recAddress  record;

    lastname_from text;
    lastname_to text;
    firstname_from text;
    firstname_to text;
    city_from text;
    city_to  text;
    country_from text;
    country_to text;
    info_contains text;
    newsletter_contains text;
    
    iWhere bool;
    BEGIN
	 iWhere = false ;
    
	 lastname_from := searchfields[1];
  	 lastname_to  := searchfields[2];
	 firstname_from  := searchfields[3];
    	 firstname_to  := searchfields[4];
    	 city_from  := searchfields[5];
    	 city_to   := searchfields[6];
    	 country_from  := searchfields[7];
    	 country_to  := searchfields[8];
    	 info_contains  := searchfields[9];
    	 newsletter_contains  := searchfields[10];

	 
	 sWhere := $$' $$' ;
	 if (char_length(lastname_from) > 0 and char_length(lastname_to) > 0 and lastname_from != $$'NONE$$' and lastname_to != $$'NONE$$' ) then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := $$' where $$' ;
	    else
	       sWhere := $$' and $$' ;
	    end if;

	    sWhere = sWhere || $$' lastname between $$' || quote_literal(lastname_from) || $$' and $$' || quote_literal(lastname_to) || $$' $$' ;


	 end if;
	 if (char_length(firstname_from) > 0 and char_length(firstname_to) > 0 and firstname_from != $$'NONE$$' and firstname_to != $$'NONE$$') then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := $$' where $$' ;
	        else
	       sWhere := $$' and $$' ;
	    end if;

	    sWhere = sWhere || $$' firstname between $$' || quote_literal(firstname_from) || $$' and $$' || quote_literal(firstname_to) || $$' $$' ;


	 end if;

	 if (char_length(city_from) > 0 and char_length(city_to) > 0 and city_from != $$'NONE$$' and city_to != $$'NONE$$') then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := $$' where $$' ;
	        else
	       sWhere := $$' and $$' ;
	    end if;

	    sWhere = sWhere || $$' city between $$' || quote_literal(city_from) || $$' and $$' || quote_literal(city_to) || $$' $$' ;

	 end if ;

	 if (char_length(country_from) > 0 and char_length(country_to) > 0 and country_from != $$'NONE$$' and country_to != $$'NONE$$') then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := $$' where $$' ;
	        else
	       sWhere := $$' and $$' ;
	    end if;

	    sWhere = sWhere || $$' country between $$' || quote_literal(country_from) || $$' and $$' || quote_literal(country_to) || $$' $$' ;


	 end if;
 	 if (char_length(info_contains) > 0 and info_contains != $$'NONE$$' ) then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := $$' where $$' ;
	        else
	       sWhere := $$' and $$' ;
	    end if;

	    sWhere = sWhere || $$' status_info ~* $$' || quote_literal(info_contains) || $$' $$' ;

	 end if;

	 if (char_length(newsletter_contains) > 0  and newsletter_contains != $$'NONE$$' ) then
	    if iWhere = false then
	       iWhere = true ;
	       sWhere := $$' where $$' ;
	        else
	       sWhere := $$' and $$' ;
	    end if;

	    sWhere = sWhere || $$' newsletter ~* $$' || quote_literal(newsletter_contains) || $$' $$' ;

	 end if;

	 sSql := $$'select id, lastname::text, lastname2::text,firstname::text,street::text,country::text, zip::text, city::text,phone::text,phone_handy::text, email::text, fax::text, status_info::text ,newsletter::text from address $$' || sWhere || $$' $$' ||   fct_getWhere(2,$$' $$') ;

	 raise notice $$'sSql = % $$', sSql ;

	 for recAddress in execute sSql LOOP 
	 
	     return next recAddress;

	 end loop ;

	 
	
      
	
       END ;
    $$ LANGUAGE 'plpgsql'; 
