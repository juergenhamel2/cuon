CREATE OR REPLACE FUNCTION fct_create_index(sName text,sTable text, sColumn text ,sUnique text) returns bool AS $$
 DECLARE
    bRet bool := False ;
    sSql text ;
    
    BEGIN
    
      begin
        sSql := $$'DROP  INDEX $$' || sName  ;
        execute sSql ;
        exception when others then
          raise notice $$'error Index Drop$$';
          raise notice $$'SQL = %$$', sSql ;
      end;
      
      begin
      sSql := $$'CREATE $$' || sUnique ||$$' INDEX $$' || sName || $$' ON $$' || sTable || $$' ($$' || sColumn|| $$')$$' ;
      execute sSql ;
      exception when others then
          raise notice $$'error Index Create$$';
          raise notice $$'SQL = %$$', sSql ;
      end;
    

    return bRet ;     
    END ;
        
     $$ LANGUAGE 'plpgsql';
     
CREATE OR REPLACE FUNCTION fct_create_index_gin(sName text,sTable text, sColumn text ,sUnique text) returns bool AS $$
 DECLARE
    bRet bool := False ;
    sSql text ;
    
    BEGIN
    
      begin
        sSql := $$'DROP  INDEX $$' || sName  ;
        execute sSql ;
        exception when others then
          raise notice $$'error Index Drop$$';
          raise notice $$'SQL = %$$', sSql ;
      end;
      
      begin
      sSql := $$'CREATE $$' || sUnique ||$$' INDEX $$' || sName || $$' ON $$' || sTable || $$' USING gin ($$' || sColumn|| $$' gin_trgm_ops)$$' ;
      raise notice $$'SQL = %$$', sSql ; 
      execute sSql ;
      exception when others then
          raise notice $$'error Index Create$$';
          raise notice $$'SQL = %$$', sSql ;
      end;
    

    return bRet ;     
    END ;
        
     $$ LANGUAGE 'plpgsql';     
     
CREATE OR REPLACE FUNCTION fct_create_trigger(sName text, sTable text , sAction text, sCursor text, sText text) returns bool as $$
 DECLARE
    bRet bool := False ;
    sSql text ;
    BEGIN
      begin
        sSql := $$'DROP TRIGGER $$' || sName || $$' ON $$' || sTable ;
        -- raise notice $$'sSql = %$$', sSql ;
        execute sSql ;
        exception when others then
          raise notice $$'error 1$$';
          raise notice $$'SQL = %$$', sSql ;
      end;
      begin
	sSql := $$'update $$' || sTable || $$' set status = $$' || quote_literal($$'insert$$') || $$' where status IS NULL $$' ;
	raise notice $$'sSql = %$$', sSql ;
	execute sSql ;	     
        exception when others then
          raise notice $$'error set status $$';
          raise notice $$'SQL = %$$', sSql ;
      end;

      begin
        sSql := $$'CREATE TRIGGER $$' || sName ||$$' $$' || sAction ||$$' ON $$' || sTable || $$' $$' ||sCursor || $$' $$' || sText ;
        raise notice $$'sSql = %$$', sSql ;
        execute sSql ;
        exception when others then
          raise notice $$'error 2$$';
          raise notice $$'SQL ERROR AT = %$$', sSql ;
      end;
         
    return bRet ;
    END ;
   
    $$ LANGUAGE 'plpgsql';     
    
       
CREATE OR REPLACE FUNCTION fct_create_allindex()    returns bool as $$
  DECLARE
    bRet bool := True ;
    
    BEGIN 
    
        -- cuon_user 
        perform fct_create_index($$'cuon_user_idx_name$$', $$'cuon_user$$', $$'username$$', $$' $$' );

        -- address 
        perform fct_create_index($$'address_idx_uuid$$', $$'address$$', $$'uuid$$', $$' $$' );
        perform fct_create_index($$'address_idx_lastname$$', $$'address$$', $$'lastname$$', $$' $$' );
        perform fct_create_index($$'address_idx_firstname$$', $$'address$$', $$'firstname$$', $$' $$' );
        perform fct_create_index($$'address_idx_phone$$', $$'address$$', $$'phone$$', $$' $$' );
        perform fct_create_index($$'address_idx_caller_id$$', $$'address$$', $$'caller_id$$', $$' $$' );
        perform fct_create_index($$'address_idx_rep_id$$', $$'address$$', $$'rep_id$$', $$' $$' );
        perform fct_create_index($$'address_idx_salesman_id$$', $$'address$$', $$'salesman_id$$', $$' $$' );
        perform fct_create_index($$'address_idx_status_info$$', $$'address$$', $$'status_info$$', $$' $$' );
        perform fct_create_index($$'address_idx_status_client$$', $$'address$$', $$'status,client$$', $$' $$' );
        perform fct_create_index($$'address_idx_newsletter$$', $$'address$$', $$'newsletter$$', $$' $$' );
        
        -- partner
        perform fct_create_index($$'partner_idx_uuid$$', $$'partner$$', $$'uuid$$', $$' $$' );
        perform fct_create_index($$'partner_idx_addressid$$', $$'partner$$', $$'addressid$$', $$' $$' );
        perform fct_create_index($$'partner_idx_status_client$$', $$'partner$$', $$'status,client$$', $$' $$' );

        -- partner_schedul
        perform fct_create_index($$'partner_schedul_idx_schedul_insert_time_day$$', $$'partner_schedul$$', $$'date_part($$'$$'day$$'$$',insert_time)$$', $$' $$' );
        
         perform fct_create_index($$'partner_schedul_idx_schedul_insert_time_week$$', $$'partner_schedul$$', $$'date_part($$'$$'week$$'$$',insert_time)$$', $$' $$' );
        
        perform fct_create_index($$'partner_schedul_idx_schedul_insert_time_month$$', $$'partner_schedul$$', $$'date_part($$'$$'month$$'$$',insert_time)$$', $$' $$' );
      perform fct_create_index($$'partner_schedul_idx_schedul_insert_time_quarter$$', $$'partner_schedul$$', $$'date_part($$'$$'quarter$$'$$',insert_time)$$', $$' $$' );
      perform fct_create_index($$'partner_schedul_idx_schedul_insert_time_year$$', $$'partner_schedul$$', $$'date_part($$'$$'year$$'$$',insert_time)$$', $$' $$' );
        perform fct_create_index($$'partner_schedul_idx_sep_info_3$$', $$'partner_schedul$$', $$'sep_info_3$$', $$' $$' );
        perform fct_create_index($$'partner_schedul_idx_date$$', $$'partner_schedul$$', $$'schedul_date$$', $$' $$' );
        perform fct_create_index($$'partner_schedul_idx_uuid$$', $$'partner_schedul$$', $$'uuid$$', $$' $$' );
        perform fct_create_index($$'partner_schedul_idx_partnerid$$', $$'partner_schedul$$', $$'partnerid$$', $$' $$' );
        perform fct_create_index($$'partner_schedul_idx_process_status$$', $$'partner_schedul$$', $$'process_status$$', $$' $$' );
        perform fct_create_index($$'partner_schedul_idx_status_client$$', $$'partner_schedul$$', $$'status,client$$', $$' $$' );
        perform fct_create_index($$'partner_schedul_idx_user_id$$', $$'partner_schedul$$', $$'user_id$$', $$' $$' );
        perform fct_create_index($$'partner_schedul_idx_schedul_staff_id$$', $$'partner_schedul$$', $$'schedul_staff_id$$', $$' $$' );

        -- address_notes
        perform fct_create_index($$'address_notes_idx_uuid$$', $$'address_notes$$', $$'uuid$$', $$' $$' );
        perform fct_create_index($$'address_notes_idx_address_id$$', $$'address_notes$$', $$'address_id$$', $$' $$' );
        perform fct_create_index($$'address_notes_idx_status_client$$', $$'address_notes$$', $$'status,client$$', $$' $$' );

        perform fct_create_index($$'bank_idx_bcn$$', $$'bank$$', $$'bcn$$', $$' $$' );

	-- articles
        perform fct_create_index($$'articles_idx_designation$$', $$'articles$$', $$'designation$$', $$' $$' );
	perform fct_create_index($$'articles_idx_number$$', $$'articles$$', $$'number$$', $$' $$' );

	-- cash_desk
	 perform fct_create_index($$'cash_desk_idx_date$$', $$'cash_desk$$', $$'cash_date$$', $$' $$' );
 	 perform fct_create_index($$'cash_desk_idx_time$$', $$'cash_desk$$', $$'cash_time$$', $$' $$' );
	 perform fct_create_index($$'cash_desk_idx_date_time$$', $$'cash_desk$$', $$'cash_date,cash_time$$', $$' $$' ); 
	 perform fct_create_index($$'cash_desk_idx_desk_number$$', $$'cash_desk$$', $$'cash_desk_number$$', $$' $$' );
	 perform fct_create_index($$'cash_desk_idx_user_short_cut$$', $$'cash_desk$$', $$'cash_desk_user_short_cut$$', $$' $$' );
	 perform fct_create_index($$'cash_desk_idx_procedure$$', $$'cash_desk$$', $$'cash_procedure$$', $$' $$' );			


	 --orderbook



	 -- list_of_invoice


	 --in_payment
	  perform fct_create_index($$'in_payment_idx_invoice_number$$', $$'in_payment$$', $$'invoice_number$$', $$' $$' );


	 -- special indexe for regex
	 begin
		create extension pg_trgm ;
	exception when others then
	  raise notice $$'error extension pg_trgm, perhaps exist $$';
      	end ;  
	 
 	perform fct_create_index_gin($$'address_idx_gin_lastname$$', $$'address$$', $$'lastname$$', $$' $$' );
	perform fct_create_index_gin($$'address_idx_gin_lastname2$$', $$'address$$', $$'lastname2$$', $$' $$' );
        perform fct_create_index_gin($$'address_idx_gin_firstname$$', $$'address$$', $$'firstname$$', $$' $$' );
	perform fct_create_index_gin($$'address_idx_gin_newsletter$$', $$'address$$', $$'newsletter$$', $$' $$' );
	perform fct_create_index_gin($$'address_idx_gin_city$$', $$'address$$', $$'city$$', $$' $$' );
	perform fct_create_index_gin($$'address_idx_gin_zip$$', $$'address$$', $$'zip$$', $$' $$' );	

	perform fct_create_index_gin($$'dms_idx_gin_title1$$', $$'dms$$', $$'title$$', $$' $$' );
	perform fct_create_index_gin($$'dms_idx_gin_sub1$$', $$'dms$$', $$'sub1$$', $$' $$' );
	perform fct_create_index_gin($$'dms_idx_gin_sub2$$', $$'dms$$', $$'sub2$$', $$' $$' );
	perform fct_create_index_gin($$'dms_idx_gin_sub3$$', $$'dms$$', $$'sub3$$', $$' $$' );
	perform fct_create_index_gin($$'dms_idx_gin_sub4$$', $$'dms$$', $$'sub4$$', $$' $$' );
	perform fct_create_index_gin($$'dms_idx_gin_sub5$$', $$'dms$$', $$'sub5$$', $$' $$' );

	perform fct_create_index_gin($$'dms_idx_gin_search1$$', $$'dms$$', $$'search1$$', $$' $$' );
	perform fct_create_index_gin($$'dms_idx_gin_search2$$', $$'dms$$', $$'search2$$', $$' $$' );
	perform fct_create_index_gin($$'dms_idx_gin_search3$$', $$'dms$$', $$'search3$$', $$' $$' );
	perform fct_create_index_gin($$'dms_idx_gin_search4$$', $$'dms$$', $$'search4$$', $$' $$' );
				

	perform fct_create_index_gin($$'articles_idx_gin_designation$$', $$'articles$$', $$'designation$$', $$' $$' );
	perform fct_create_index_gin($$'articles_idx_gin_number$$', $$'articles$$', $$'number$$', $$' $$' );

	perform fct_create_index_gin($$'orderbook_idx_gin_designation$$', $$'orderbook$$', $$'designation$$', $$' $$' );
	perform fct_create_index_gin($$'orderbook_idx_gin_number$$', $$'orderbook$$', $$'number$$', $$' $$' );


	perform fct_create_index_gin($$'proposal_idx_gin_designation$$', $$'proposal$$', $$'designation$$', $$' $$' );
	perform fct_create_index_gin($$'proposal_idx_gin_number$$', $$'proposal$$', $$'number$$', $$' $$' );

	perform fct_create_index_gin($$'enquiry_idx_gin_designation$$', $$'enquiry$$', $$'designation$$', $$' $$' );
	perform fct_create_index_gin($$'enquiry_idx_gin_number$$', $$'enquiry$$', $$'number$$', $$' $$' );

	perform fct_create_index_gin($$'projects_idx_gin_designation$$', $$'projects$$', $$'designation$$', $$' $$' );
	perform fct_create_index_gin($$'projects_idx_gin_name$$', $$'projects$$', $$'name$$', $$' $$' );
	perform fct_create_index_gin($$'projects_idx_gin_internal_project_number$$', $$'projects$$', $$'internal_project_number$$', $$' $$' );
	
        return bRet ;
    END ;
   
    $$ LANGUAGE 'plpgsql'; 
  

DROP FUNCTION fct_create_list_of_all_table() ;
CREATE OR REPLACE FUNCTION fct_create_list_of_all_table() returns text [] as $$
  DECLARE
    bRet bool := True ;
    vList text[]  ;
    array_size integer ;
    sSql text ;
    BEGIN
   
	vList[1] := $$'address$$' ;
        vList := array_append(vList,$$'partner$$') ;
        vList := array_append(vList,$$'address_notes$$') ;
	vList := array_append(vList,$$'address_bank$$') ;
	
        vList := array_append(vList,$$'bank$$') ;
        
        vList := array_append(vList,$$'articles$$') ;
        vList := array_append(vList,$$'articles_parts_list$$') ;
	vList := array_append(vList,$$'articles_purchase$$') ;
        vList := array_append(vList,$$'articles_sales$$') ;
	vList := array_append(vList,$$'articles_stock$$') ;
	vList := array_append(vList,$$'articles_webshop$$') ;


        vList := array_append(vList,$$'dms$$') ;

    vList := array_append(vList,$$'clients$$') ;
    vList := array_append(vList,$$'biblio$$') ;
    vList := array_append(vList,$$'staff$$') ;
          
    	vList := array_append(vList,$$'enquiry$$') ;
        vList := array_append(vList,$$'enquiryget$$') ;    
        vList := array_append(vList,$$'enquiryinvoice $$') ;    
        vList := array_append(vList,$$'enquirymisc$$') ;
	vList := array_append(vList,$$'enquiryposition$$') ;
	vList := array_append(vList,$$'enquirysupply$$') ;

        vList := array_append(vList,$$'list_of_deliveries$$') ;
        vList := array_append(vList,$$'proposal$$') ;
        vList := array_append(vList,$$'proposalposition$$') ;
        vList := array_append(vList,$$'proposalget$$') ;
        vList := array_append(vList,$$'proposalmisc$$') ;
        vList := array_append(vList,$$'proposalsupply$$') ;

    	vList := array_append(vList,$$'orderbook$$') ;
	vList := array_append(vList,$$'orderposition$$') ;
        vList := array_append(vList,$$'stock_goods$$') ;
	vList := array_append(vList,$$'orderinvoice$$') ;
       	vList := array_append(vList,$$'cash_desk_book_number$$') ;
        vList := array_append(vList,$$'list_of_invoices$$') ;
        vList := array_append(vList,$$'in_payment$$') ;	   
	vList := array_append(vList,$$'cash_desk$$') ;
	
       vList := array_append(vList,$$'support_ticket$$') ;
  vList := array_append(vList,$$'support_project$$') ;

        vList := array_append(vList,$$'projects$$') ;       
	vList := array_append(vList,$$'project_phases $$') ;
        vList := array_append(vList,$$'project_tasks$$') ;
        vList := array_append(vList,$$'project_task_material_res$$') ;
        vList := array_append(vList,$$'projects_task_staff_res$$') ;
       
        vList := array_append(vList,$$'sourcecode_file$$') ;
	vList := array_append(vList,$$'sourcecode_module$$') ;
	vList := array_append(vList,$$'sourcecode_module_staff_res$$') ;
	vList := array_append(vList,$$'sourcecode_modul_material_res$$') ;
	vList := array_append(vList,$$'sourcecode_modul_material_res$$') ;
        vList := array_append(vList,$$'sourcecode_parts$$') ;
	vList := array_append(vList,$$'sourcecode_projects$$') ;

  vList := array_append(vList,$$'hibernation$$') ;
  vList := array_append(vList,$$'hibernation_plant$$') ;
       

  vList := array_append(vList,$$'graveyard$$') ;
        vList := array_append(vList,$$'grave$$') ;
        vList := array_append(vList,$$'grave_work_maintenance$$') ;
        vList := array_append(vList,$$'grave_work_spring$$') ;
        vList := array_append(vList,$$'grave_work_summer$$') ;
        vList := array_append(vList,$$'grave_work_autumn$$') ;
        vList := array_append(vList,$$'grave_work_winter$$') ;
        vList := array_append(vList,$$'grave_work_single$$') ;
        vList := array_append(vList,$$'grave_work_holiday$$') ;
        vList := array_append(vList,$$'grave_work_year$$') ;
  vList := array_append(vList,$$'grave_service_notes$$') ;
        
         vList := array_append(vList,$$'botany$$') ;
       vList := array_append(vList,$$'botany_class$$') ;
  vList := array_append(vList,$$'botany_divisio$$') ;
  vList := array_append(vList,$$'botany_family$$') ;
  vList := array_append(vList,$$'botany_genus$$') ;
  vList := array_append(vList,$$'botany_ordo$$') ;

     return vlist ;
   END ;
   
    $$ LANGUAGE 'plpgsql';     
    
CREATE OR REPLACE FUNCTION fct_create_alltrigger()    returns bool as $$
  DECLARE
    bRet bool := True ;
    vList text[]  ;
    array_size integer ;
    sSql text ;
    BEGIN
   
       vlist := fct_create_list_of_all_table() ;
       
        array_size := array_length(vList,1) ;
       begin 
            sSql := $$'DROP TRIGGER trg_updatelistofinvoices  ON  list_of_invoices $$' ;
            -- raise notice $$'sSql = %$$', sSql ;
            execute sSql ;
            exception when others then
                raise notice $$'error 1$$';
                raise notice $$'SQL = %$$', sSql ;
        end ;
        
        for ct in 1..array_size loop
            -- raise notice $$' ct = %, array = % $$', ct,vList[ct] ;
            sSql := $$'select * from fct_create_Trigger($$'$$'trg_insert$$' || vList[ct] || $$'$$'$$',$$' || quote_literal(vList[ct]) ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_insert()$$'$$' )$$' ;  -- EXECUTE PROCEDURE fct_insert()
            -- raise notice $$'alltrigger sSql = %$$', sSql ;
            execute sSql ;
            sSql := $$'select * from fct_create_Trigger($$'$$'trg_delete$$' || vList[ct] || $$'$$'$$',$$' || quote_literal(vList[ct]) ||$$', $$'$$'before delete$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_delete()$$'$$' )$$' ;  -- EXECUTE PROCEDURE fct_insert()
            -- raise notice $$'alltrigger sSql = %$$', sSql ;
            execute sSql ;
         
            sSql := $$'select * from fct_create_Trigger($$'$$'trg_update$$' || vList[ct] || $$'$$'$$',$$' || quote_literal(vList[ct]) ||$$', $$'$$'before update$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_update()$$'$$' )$$' ;  -- EXECUTE PROCEDURE fct_insert()
         -- raise notice $$'alltrigger sSql = %$$', sSql ;
            execute sSql ;    
         
         
        
        
        end loop;  
    
    -- Special trigger

    -- lock process only insert
    
 sSql := $$'select * from fct_create_Trigger($$'$$'trg_t_new_cash_desk_special_id_$$'$$',$$' || quote_literal($$' cash_desk_book_number$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_new_cash_desk_special_id() $$'$$' )$$' ;
 
      -- raise notice $$'alltrigger Special  sSql = %$$', sSql ;
       execute sSql ;

   sSql := $$'select * from fct_create_Trigger($$'$$'trg_t_insert_cash_desk_book_number_$$'$$',$$' || quote_literal($$' cash_desk_book_number$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_insert() $$'$$' )$$' ;
 
      -- raise notice $$'alltrigger Special  sSql = %$$', sSql ;
       execute sSql ;

     sSql := $$'select * from fct_create_Trigger($$'$$'trg_insert_cuon$$'$$',$$' || quote_literal($$'cuon$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_cuon_insert()$$'$$' )$$' ;
      -- raise notice $$'alltrigger Special  sSql = %$$', sSql ;
       execute sSql ;

     sSql := $$'select * from fct_create_Trigger($$'$$'trg_update_cuon$$'$$',$$' || quote_literal($$'cuon$$') ||$$', $$'$$'before update$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_cuon_insert()$$'$$' )$$' ;
      -- raise notice $$'alltrigger Special  sSql = %$$', sSql ;
       execute sSql ;


     sSql := $$'select * from fct_create_Trigger($$'$$'trg_insert_partner_schedul$$'$$',$$' || quote_literal($$'partner_schedul$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_insert()$$'$$' )$$' ;
      -- raise notice $$'alltrigger Special  sSql = %$$', sSql ;
       execute sSql ;
       
     sSql := $$'select * from fct_create_Trigger($$'$$'trg_update_partner_schedul$$'$$',$$' || quote_literal($$'partner_schedul$$') ||$$', $$'$$'before update$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_partner_schedul_change( )$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;

       sSql := $$'select * from fct_create_Trigger($$'$$'trg_insert_pick_schedul$$'$$',$$' || quote_literal($$'orderget$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE  fct_pickup_schedul_insert( )$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;

            sSql := $$'select * from fct_create_Trigger($$'$$'trg_update_pick_schedul$$'$$',$$' || quote_literal($$'orderget$$') ||$$', $$'$$'before update$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE  fct_pickup_schedul_update( )$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;




 sSql := $$'select * from fct_create_Trigger($$'$$'trg_new_orderpositon$$'$$',$$' || quote_literal($$'orderposition$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE  fct_orderposition_insert () $$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;





 sSql := $$'select * from fct_create_Trigger($$'$$'trg_new_stock_goods$$'$$',$$' || quote_literal($$'stock_goods$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE  fct_stock_goods_insert() $$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;



 sSql := $$'select * from fct_create_Trigger($$'$$'trg_delete_pick_schedul$$'$$',$$' || quote_literal($$'orderget$$') ||$$', $$'$$'before delete$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE  fct_pickup_schedul_delete( )$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;
        
    sSql := $$'select * from fct_create_Trigger($$'$$'trg_update_orderbook_gift1$$'$$',$$' || quote_literal($$'orderbook$$') ||$$', $$'$$'before update$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_getOrderGifts()$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;
        
      sSql := $$'select * from fct_create_Trigger($$'$$'trg_insert_orderbook_gift1$$'$$',$$' || quote_literal($$'orderbook$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_getOrderGifts()$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;  
        
    sSql := $$'select * from fct_create_Trigger($$'$$'trg_update_dms_pairedID$$'$$',$$' || quote_literal($$'dms$$') ||$$', $$'$$'before update$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_setDMSPairedID()$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;


      sSql := $$'select * from fct_create_Trigger($$'$$'trg_insert_dms_pairedID$$'$$',$$' || quote_literal($$'dms$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_setDMSPairedID()$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;  
        
     sSql := $$'select * from fct_create_Trigger($$'$$'trg_orderbook_delete_invoice$$'$$',$$' || quote_literal($$'orderbook$$') ||$$', $$'$$'after delete$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_orderbook_delete_invoice()$$'$$' )$$' ;  
     -- raise notice $$'alltrigger Special sSql = %$$', sSql ;
            execute sSql ;  



     sSql := $$'select * from fct_create_Trigger($$'$$'trg_insert_lock_process$$'$$',$$' || quote_literal($$'lock_process$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_insert()$$'$$' )$$' ;  -- EXECUTE PROCEDURE fct_insert()


      raise notice $$'alltrigger Special lock process  sSql = %$$', sSql ;
  execute sSql ;


   sSql := $$'select * from fct_create_Trigger($$'$$'trg_insertz1_inpayment$$'$$',$$' || quote_literal($$'in_payment$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_beforeInpayment()$$'$$' )$$' ;  -- EXECUTE PROCEDURE fct_insert()


      raise notice $$'alltrigger Special insert inpayment  sSql = %$$', sSql ;
  execute sSql ;


   sSql := $$'select * from fct_create_Trigger($$'$$'trg_insertz1_cashdesk$$'$$',$$' || quote_literal($$'cash_desk$$') ||$$', $$'$$'before insert$$'$$', $$'$$'FOR EACH ROW$$'$$',$$'$$'EXECUTE PROCEDURE fct_beforeCashDesk()$$'$$' )$$' ;  -- EXECUTE PROCEDURE fct_insert()


      raise notice $$'alltrigger Special insert inpayment  sSql = %$$', sSql ;
  execute sSql ;

    
        
      return bRet ;  
    
    END ;
   
   
    $$ LANGUAGE 'plpgsql';

  
CREATE OR REPLACE FUNCTION fct_test_array()    returns bool as $$
  DECLARE
    bRet bool := True ;
    vList integer[]  ;
    array_size integer ;
    ct integer ;
    BEGIN   
    vList[1] := 1 ;
    vList[2] := 7 ;
    vList[3] := 9 ;
    vList[4] := 11 ;
    
    array_size := 4;
    for ct in 1..array_size loop
      raise notice $$' ct = %, array = % $$', ct,vList[ct] ;
    end loop;  
    return bRet ;
    END ;
   
    $$ LANGUAGE 'plpgsql';  
