#!/usr/bin/env python
# -*- coding: utf-8 -*-


##		    GNU GENERAL PUBLIC LICENSE
##		       Version 2, June 1991

## Copyright (C) 1989, 1991 Free Software Foundation, Inc.
##                       59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
## Everyone is permitted to copy and distribute verbatim copies
## of this license document, but changing it is not allowed.

##			    Preamble

##  The licenses for most software are designed to take away your
##freedom to share and change it.  By contrast, the GNU General Public
##License is intended to guarantee your freedom to share and change free
##software--to make sure the software is free for all its users.  This
##General Public License applies to most of the Free Software
##Foundation's software and to any other program whose authors commit to
##using it.  (Some other Free Software Foundation software is covered by
##the GNU Library General Public License instead.)  You can apply it to
##your programs, too.

##  When we speak of free software, we are referring to freedom, not
##price.  Our General Public Licenses are designed to make sure that you
##have the freedom to distribute copies of free software (and charge for
##this service if you wish), that you receive source code or can get it
##if you want it, that you can change the software or use pieces of it
##in new free programs; and that you know you can do these things.

##  To protect your rights, we need to make restrictions that forbid
##anyone to deny you these rights or to ask you to surrender the rights.
##These restrictions translate to certain responsibilities for you if you
##distribute copies of the software, or if you modify it.

##  For example, if you distribute copies of such a program, whether
##gratis or for a fee, you must give the recipients all the rights that
##you have.  You must make sure that they, too, receive or can get the
##source code.  And you must show them these terms so they know their
##rights.

##  We protect your rights with two steps: (1) copyright the software, and
##(2) offer you this license which gives you legal permission to copy,
##distribute and/or modify the software.

##  Also, for each author's protection and ours, we want to make certain
##that everyone understands that there is no warranty for this free
##software.  If the software is modified by someone else and passed on, we
##want its recipients to know that what they have is not the original, so
##that any problems introduced by others will not reflect on the original
##authors' reputations.

##  Finally, any free program is threatened constantly by software
##patents.  We wish to avoid the danger that redistributors of a free
##program will individually obtain patent licenses, in effect making the
##program proprietary.  To prevent this, we have made it clear that any
##patent must be licensed for everyone's free use or not licensed at all.

##  The precise terms and conditions for copying, distribution and
##modification follow.

##		    GNU GENERAL PUBLIC LICENSE
##   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

##  0. This License applies to any program or other work which contains
##a notice placed by the copyright holder saying it may be distributed
##under the terms of this General Public License.  The "Program", below,
##refers to any such program or work, and a "work based on the Program"
##means either the Program or any derivative work under copyright law:
##that is to say, a work containing the Program or a portion of it,
##either verbatim or with modifications and/or translated into another
##language.  (Hereinafter, translation is included without limitation in
##the term "modification".)  Each licensee is addressed as "you".

##Activities other than copying, distribution and modification are not
##covered by this License; they are outside its scope.  The act of
##running the Program is not restricted, and the output from the Program
##is covered only if its contents constitute a work based on the
##Program (independent of having been made by running the Program).
##Whether that is true depends on what the Program does.

##  1. You may copy and distribute verbatim copies of the Program's
##source code as you receive it, in any medium, provided that you
##conspicuously and appropriately publish on each copy an appropriate
##copyright notice and disclaimer of warranty; keep intact all the
##notices that refer to this License and to the absence of any warranty;
##and give any other recipients of the Program a copy of this License
##along with the Program.

##You may charge a fee for the physical act of transferring a copy, and
##you may at your option offer warranty protection in exchange for a fee.

##  2. You may modify your copy or copies of the Program or any portion
##of it, thus forming a work based on the Program, and copy and
##distribute such modifications or work under the terms of Section 1
##above, provided that you also meet all of these conditions:

##    a) You must cause the modified files to carry prominent notices
##    stating that you changed the files and the date of any change.

##    b) You must cause any work that you distribute or publish, that in
##    whole or in part contains or is derived from the Program or any
##    part thereof, to be licensed as a whole at no charge to all third
##    parties under the terms of this License.

##    c) If the modified program normally reads commands interactively
##    when run, you must cause it, when started running for such
##    interactive use in the most ordinary way, to print or display an
##    announcement including an appropriate copyright notice and a
##    notice that there is no warranty (or else, saying that you provide
##    a warranty) and that users may redistribute the program under
##    these conditions, and telling the user how to view a copy of this
##    License.  (Exception: if the Program itself is interactive but
##    does not normally print such an announcement, your work based on
##    the Program is not required to print an announcement.)

##These requirements apply to the modified work as a whole.  If
##identifiable sections of that work are not derived from the Program,
##and can be reasonably considered independent and separate works in
##themselves, then this License, and its terms, do not apply to those
##sections when you distribute them as separate works.  But when you
##distribute the same sections as part of a whole which is a work based
##on the Program, the distribution of the whole must be on the terms of
##this License, whose permissions for other licensees extend to the
##entire whole, and thus to each and every part regardless of who wrote it.

##Thus, it is not the intent of this section to claim rights or contest
##your rights to work written entirely by you; rather, the intent is to
##exercise the right to control the distribution of derivative or
##collective works based on the Program.

##In addition, mere aggregation of another work not based on the Program
##with the Program (or with a work based on the Program) on a volume of
##a storage or distribution medium does not bring the other work under
##the scope of this License.

##  3. You may copy and distribute the Program (or a work based on it,
##under Section 2) in object code or executable form under the terms of
##Sections 1 and 2 above provided that you also do one of the following:

##    a) Accompany it with the complete corresponding machine-readable
##    source code, which must be distributed under the terms of Sections
##    1 and 2 above on a medium customarily used for software interchange; or,

##    b) Accompany it with a written offer, valid for at least three
##    years, to give any third party, for a charge no more than your
##    cost of physically performing source distribution, a complete
##    machine-readable copy of the corresponding source code, to be
##    distributed under the terms of Sections 1 and 2 above on a medium
##    customarily used for software interchange; or,

##    c) Accompany it with the information you received as to the offer
##    to distribute corresponding source code.  (This alternative is
##    allowed only for noncommercial distribution and only if you
##    received the program in object code or executable form with such
##    an offer, in accord with Subsection b above.)

##The source code for a work means the preferred form of the work for
##making modifications to it.  For an executable work, complete source
##code means all the source code for all modules it contains, plus any
##associated interface definition files, plus the scripts used to
##control compilation and installation of the executable.  However, as a
##special exception, the source code distributed need not include
##anything that is normally distributed (in either source or binary
##form) with the major components (compiler, kernel, and so on) of the
##operating system on which the executable runs, unless that component
##itself accompanies the executable.

##If distribution of executable or object code is made by offering
##access to copy from a designated place, then offering equivalent
##access to copy the source code from the same place counts as
##distribution of the source code, even though third parties are not
##compelled to copy the source along with the object code.

##  4. You may not copy, modify, sublicense, or distribute the Program
##except as expressly provided under this License.  Any attempt
##otherwise to copy, modify, sublicense or distribute the Program is
##void, and will automatically terminate your rights under this License.
##However, parties who have received copies, or rights, from you under
##this License will not have their licenses terminated so long as such
##parties remain in full compliance.

##  5. You are not required to accept this License, since you have not
##signed it.  However, nothing else grants you permission to modify or
##distribute the Program or its derivative works.  These actions are
##prohibited by law if you do not accept this License.  Therefore, by
##modifying or distributing the Program (or any work based on the
##Program), you indicate your acceptance of this License to do so, and
##all its terms and conditions for copying, distributing or modifying
##the Program or works based on it.

##  6. Each time you redistribute the Program (or any work based on the
##Program), the recipient automatically receives a license from the
##original licensor to copy, distribute or modify the Program subject to
##these terms and conditions.  You may not impose any further
##restrictions on the recipients' exercise of the rights granted herein.
##You are not responsible for enforcing compliance by third parties to
##this License.

##  7. If, as a consequence of a court judgment or allegation of patent
##infringement or for any other reason (not limited to patent issues),
##conditions are imposed on you (whether by court order, agreement or
##otherwise) that contradict the conditions of this License, they do not
##excuse you from the conditions of this License.  If you cannot
##distribute so as to satisfy simultaneously your obligations under this
##License and any other pertinent obligations, then as a consequence you
##may not distribute the Program at all.  For example, if a patent
##license would not permit royalty-free redistribution of the Program by
##all those who receive copies directly or indirectly through you, then
##the only way you could satisfy both it and this License would be to
##refrain entirely from distribution of the Program.

##If any portion of this section is held invalid or unenforceable under
##any particular circumstance, the balance of the section is intended to
##apply and the section as a whole is intended to apply in other
##circumstances.

##It is not the purpose of this section to induce you to infringe any
##patents or other property right claims or to contest validity of any
##such claims; this section has the sole purpose of protecting the
##integrity of the free software distribution system, which is
##implemented by public license practices.  Many people have made
##generous contributions to the wide range of software distributed
##through that system in reliance on consistent application of that
##system; it is up to the author/donor to decide if he or she is willing
##to distribute software through any other system and a licensee cannot
##impose that choice.

##This section is intended to make thoroughly clear what is believed to
##be a consequence of the rest of this License.

##  8. If the distribution and/or use of the Program is restricted in
##certain countries either by patents or by copyrighted interfaces, the
##original copyright holder who places the Program under this License
##may add an explicit geographical distribution limitation excluding
##those countries, so that distribution is permitted only in or among
##countries not thus excluded.  In such case, this License incorporates
##the limitation as if written in the body of this License.

##  9. The Free Software Foundation may publish revised and/or new versions
##of the General Public License from time to time.  Such new versions will
##be similar in spirit to the present version, but may differ in detail to
##address new problems or concerns.

##Each version is given a distinguishing version number.  If the Program
##specifies a version number of this License which applies to it and "any
##later version", you have the option of following the terms and conditions
##either of that version or of any later version published by the Free
##Software Foundation.  If the Program does not specify a version number of
##this License, you may choose any version ever published by the Free Software
##Foundation.

##  10. If you wish to incorporate parts of the Program into other free
##programs whose distribution conditions are different, write to the author
##to ask for permission.  For software which is copyrighted by the Free
##Software Foundation, write to the Free Software Foundation; we sometimes
##make exceptions for this.  Our decision will be guided by the two goals
##of preserving the free status of all derivatives of our free software and
##of promoting the sharing and reuse of software generally.

##			    NO WARRANTY

##  11. BECAUSE THE PROGRAM IS LICENSED FREE OF CHARGE, THERE IS NO WARRANTY
##FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.  EXCEPT WHEN
##OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES
##PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED
##OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  THE ENTIRE RISK AS
##TO THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU.  SHOULD THE
##PROGRAM PROVE DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING,
##REPAIR OR CORRECTION.

##  12. IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING
##WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MAY MODIFY AND/OR
##REDISTRIBUTE THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR DAMAGES,
##INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES ARISING
##OUT OF THE USE OR INABILITY TO USE THE PROGRAM (INCLUDING BUT NOT LIMITED
##TO LOSS OF DATA OR DATA BEING RENDERED INACCURATE OR LOSSES SUSTAINED BY
##YOU OR THIRD PARTIES OR A FAILURE OF THE PROGRAM TO OPERATE WITH ANY OTHER
##PROGRAMS), EVEN IF SUCH HOLDER OR OTHER PARTY HAS BEEN ADVISED OF THE
##POSSIBILITY OF SUCH DAMAGES.

##		     END OF TERMS AND CONDITIONS



##Copyright (C) [2003]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
import os
import sys

        
sys.path.append('/usr/lib/python/')
sys.path.append('/usr/lib64/python2.4/site-packages')
sys.path.append('/usr/lib64/python2.4/site-packages/gtk-2.0')  
#sys.path.append(os.environ['CUON_PATH'])

#try:
import pygtk
#except:
#    print 'No python-module pygtk found. please install first'
#    sys.exit(0)
    
import os.path

pygtk.require('2.0')

import gtk
import gtk.glade

  


import cuon.Addresses.addresses
import cuon.Articles.articles
import cuon.Bank.bank
try:
    import cuon.Clients.clients
except Exception, params:
    print 'import failed'
    print Exception, params

import cuon.Biblio.biblio
import cuon.Order.order
import cuon.User
import cuon.Preferences.preferences
import cuon.PrefsFinance.prefsFinance

import cuon.Stock.stock
import cuon.DMS.dms


import  cuon.Databases.databases
import cuon.XML.MyXML
from cuon.TypeDefs.typedefs import typedefs
from cuon.Windows.windows  import windows
import cuon.Login.login
import cPickle
import cuon.Databases.dumps
from cuon.TypeDefs.typedefs_server import typedefs_server
import cuon.Databases.cyr_load_table
import threading
import cuon.VTK.mainLogo
import cuon.VTK.test
import cuon.WebShop.webshop
import cuon.AI.ai
import cuon.Staff.staff
import cuon.Project.project
import cuon.Finances.cashAccountBook
import cuon.Databases.import_generic1
import cuon.Databases.import_generic2
import commands
import cuon.Help.help


 
# localisation
import locale, gettext
import time


#http connections
import httplib, urllib
try:
    import profile
except:
    print "no Profile"

class MainWindow(windows):
    """
    @author: Juergen Hamel
    @organization: Cyrus-Computer GmbH, D-32584 Loehne
    @copyright: by Juergen Hamel
    @license: GPL ( GNU GENERAL PUBLIC LICENSE )
    @contact: jh@cyrus.de
    """

   
    def __init__(self, sT):
        windows.__init__(self)
        self.sStartType = sT
        self.Version = {'Major': 0, 'Minor': 32, 'Rev': 4, 'Species': 0, 'Maschine': 'Linux,Windows'}
        
        self.sTitle = _("Client PyCuon for C.U.O.N. Version ") + `self.Version['Major']` + '.' + `self.Version['Minor']` + '.' + `self.Version['Rev']` 
        self.allTables = {}
        self.sDebug = 'NO'
        self.ModulNumber = self.MN['Mainwindow']
        self.extMenucommand = {}
        #self.extMenucommand['ext1'] = 'Test'
    #set this Functions to None
    def loadUserInfo(self):
        pass
        
    def checkClient(self):
        pass 
    
    def on_end1_activate(self,event):
        print "exit cuon"
        #clean up the tmp-files
        try:
            os.system( 'rm ' + os.path.normpath(os.environ['CUON_HOME'] + '/cuon__*' ))
        except Exception, params:
            #print Exception, params
            pass
        try:
            os.system( 'rm ' + os.path.normpath(self.dicUser['prefPath']['tmp'] + '/*__dms*' ))
        except Exception, params:
            #print Exception, params
            pass
        try:
            os.system( 'rm ' + os.path.normpath( os.environ['CUON_HOME'] + '/*__dms*' ))
        except Exception, params:
            #print Exception, params
            pass
            
            
            
        except Exception, params:
            #print Exception, params
            pass
            
        self.gtk_main_quit()

    def on_databases1_activate(self,event):
        daba = cuon.Databases.databases.databaseswindow()
   
    def on_login1_activate(self,event):
        lgi = cuon.Login.login.loginwindow( [self.getWidget('eUserName')])
        
        self.openDB()
        self.oUser = self.loadObject('User')
        self.closeDB()
        if self.oUser.getUserName()== 'EMPTY':
            pass
        else:
            self.getWidget('eServer').set_text(self.td.server)
            #choose the client 
            self.on_clients1_activate(None)
            self.checkMenus()

    def checkMenus(self):
        liModullist = self.rpc.callRP('User.getModulList', self.oUser.getSqlDicUser())
        print liModullist
        if self.sStartType == 'server':
            self.enableMenuItem('serverMode')

        self.disableMenuItem('user')
        self.enableMenuItem('login')
        misc_menu = False
        print 'LI_MODULELIST'
        print `liModullist`
        for iL in  liModullist:
            print iL
            if iL.has_key('all'):
                print 'key all found'
                #data
                self.addEnabledMenuItems('work','mi_addresses1')
                self.addEnabledMenuItems('work','mi_articles1')
                self.addEnabledMenuItems('work','mi_bibliographic')
                self.addEnabledMenuItems('work','mi_clients1')
                print 'enableMenuItem staff'
                self.addEnabledMenuItems('work','mi_staff1')
                print 'enableMenuItem staff end'
                #action
                self.addEnabledMenuItems('work','mi_order1')
                self.addEnabledMenuItems('work','mi_stock1')
                self.addEnabledMenuItems('work','mi_dms1')
                #accounting
                self.addEnabledMenuItems('work','mi_cash_account_book1')
                # tools
                self.addEnabledMenuItems('work','mi_expert_system1')
                self.addEnabledMenuItems('work','mi_project1')
                #extras
                self.addEnabledMenuItems('work','mi_preferences1')
                self.addEnabledMenuItems('work','mi_user1')
                self.addEnabledMenuItems('work','mi_finances1')
                self.addEnabledMenuItems('work','mi_project1')
                self.addEnabledMenuItems('work','mi_import_data1')
                self.enableMenuItem('work')

          
            if iL.has_key('addresses'):
                self.addEnabledMenuItems('misc','mi_addresses1')
                misc_menu = True
            if iL.has_key('articles'):
                self.addEnabledMenuItems('misc','mi_articles1')
                misc_menu = True
            if iL.has_key('biblio'):
                self.addEnabledMenuItems('misc','mi_bibliographic')
                misc_menu = True
            if iL.has_key('clients'):
                self.addEnabledMenuItems('misc','mi_clients1')
                misc_menu = True
            if iL.has_key('staff'):
                self.addEnabledMenuItems('misc','mi_staff1')
                misc_menu = True
            if iL.has_key('order'):
                self.addEnabledMenuItems('misc','mi_order1')
                misc_menu = True
            if iL.has_key('stock'):
                self.addEnabledMenuItems('misc','mi_stock1')
                misc_menu = True
            if iL.has_key('dms'):
                self.addEnabledMenuItems('misc','mi_dms1')
                misc_menu = True
            if iL.has_key('account_book'):
                self.addEnabledMenuItems('misc','mi_cash_account_book1')
                misc_menu = True

            if iL.has_key('expert_system'):
                self.addEnabledMenuItems('misc','mi_expert_system1')
                misc_menu = True
            if iL.has_key('project'):
                print 'key project found '
                self.addEnabledMenuItems('misc','mi_project1')
                misc_menu = True
                print '-----------------------'
            if iL.has_key('experimental'):
                print 'key experimental found'
                self.addEnabledMenuItems('experimental','mi_mayavi1')
                self.addEnabledMenuItems('experimental','mi_test1')
                
                self.enableMenuItem('experimental')

            if iL.has_key('extendet_gpl'):

                liExtGpl = iL['extendet_gpl']
                print 'Ext.GPL =', liExtGpl
                
                for newProgram in liExtGpl:
                    mi1 = self.addMenuItem(self.getWidget(newProgram['MenuItem']['Main']),newProgram['MenuItem']['Sub'])
                    
                    print 'new Item = ', `mi1`
                    if newProgram['MenuItem']['ExternalNumber'] == 'ext1':
                        mi1.connect("activate", self.on_ext1_activate)
                    elif newProgram['MenuItem']['ExternalNumber'] == 'ext2':
                        mi1.connect("activate", self.on_ext2_activate)
                    elif newProgram['MenuItem']['ExternalNumber'] == 'ext3':
                        mi1.connect("activate", self.on_ext3_activate)
                    
                    if newProgram.has_key('Imports'):
                        newImports = newProgram['Imports']
                        for nI in newImports:
                            try:
                                exec('import ' + nI)
                                print 'import', nI
                            except:
                                pass

                        if newProgram.has_key('MenuStart'):
                            print 'MenuStart = ', newProgram['MenuItem']['ExternalNumber']
                            self.extMenucommand[newProgram['MenuItem']['ExternalNumber']] =  newProgram['MenuStart']
                            
                            
                        if newProgram.has_key('Start'):
                             exec(newProgram['Start'])
                             print 'EXEC = ', newProgram['Start']
                        
                             
        if misc_menu:
                self.enableMenuItem('misc')
    def on_logout1_activate(self, event):
        print 'Logout'
        try:
            self.rpc.callRP('Databases.logout', self.oUser.getUserName()) 
        except:
            print 'Exception'
                
        self.disableMenuItem('login')
        self.enableMenuItem('user')
 
    def on_eUserName_changed(self, event):
        if self.getWidget('eUserName').get_text() != 'EMPTY':
            print 'User changed 22'
            self.openDB()
            self.oUser = self.loadObject('User')
            print 'sDebug (Cuon) = '  + self.sDebug
            self.oUser.setDebug(self.sDebug)
            self.saveObject('User', self.oUser)
            self.closeDB()
                          
            

           # self.openDB()
       
                
            #if self.startProgressBar():
            if not self.allTables:
                self.generateLocalSqlObjects()
            #    self.stopProgressBar()
            #print self.oUser.getDicUser()
        
        
    def generateSqlObjects(self):
        #self.rpc.callRP('src.Databases.py_getInfoOfTable', 'allTables')
        at = self.rpc.callRP('Database.getInfo', 'allTables')
        #print 'at23 = ', `at`
        liAllTables = cPickle.loads(eval(self.doDecode(at)))
        #sys.exit(0)
        #print 'liAllTables = '
        #print liAllTables
        iCount = len(liAllTables)
        for i in range(iCount):
            self.loadSqlDefs(liAllTables, i)
            self.setProgressBar(float(i) * 1.0/float(iCount) * 100.0)
            #print 'Progress-Value = ' + str(float(i) * 1.0/float(iCount) * 100.0)
        #print self.allTables

    def generateLocalSqlObjects(self):
        at = self.rpc.callRP('Database.getInfo', 'allTables')
        print 'at24 = ', `at`
        liAllTables = cPickle.loads(eval(self.doDecode(at)))
        #liAllTables = cPickle.loads(self.rpc.callRP('src.Databases.py_getInfoOfTable', 'allTables'))
        print 'liAllTables = ', liAllTables
        #print liAllTables
        iCount = len(liAllTables)
        print 'iCount = ', iCount
        for i in range(iCount):
            self.loadLocalSqlDefs(liAllTables, i)
            #self.setProgressBar(float(i) * 1.0/float(iCount) * 100.0)
            #print 'Progress-Value = ' + str(float(i) * 1.0/float(iCount) * 100.0)
        #print self.allTables

    def loadSqlDefs(self, liAllTables, i ):
        try:
            clt = cuon.Databases.cyr_load_table.cyr_load_table()
            self.allTables[liAllTables[i]] =  clt.loadTable(liAllTables[i])
        except:
            print 'ERROR'
            
    def loadLocalSqlDefs(self, liAllTables, i ):
        #print 'loadLocalSQL1 ', liAllTables
        #print 'loadLocalSQL2 ', i
        
        clt = cuon.Databases.cyr_load_table.cyr_load_table()
        self.allTables[liAllTables[i]] =  clt.loadLocalTable(liAllTables[i])
        #print 'loadLocalSQL3 ', `self.allTables`

      
    # Data-Menu
    #--> 
    def on_addresses1_activate(self,event):
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        

    def on_articles1_activate(self,event):
        art = cuon.Articles.articles.articleswindow(self.allTables)

    def on_bank1_activate(self,event):
        bank = cuon.Bank.bank.bankwindow(self.allTables)


    #-->
    def on_bibliographic_activate(self, event):
        bib = cuon.Biblio.biblio.bibliowindow(self.allTables)
    def on_clients1_activate(self, event):
        cli = cuon.Clients.clients.clientswindow(self.allTables)
        
    def on_staff1_activate(self, event):
        staff = cuon.Staff.staff.staffwindow(self.allTables) 
        
    # Action-Menu
    
    def on_order1_activate(self,event):
        ord = cuon.Order.order.orderwindow(self.allTables)


    def on_stock1_activate(self,event):
        ord = cuon.Stock.stock.stockwindow(self.allTables)
  
        
       
 
    def on_dms1_activate(self,event):
        dms = cuon.DMS.dms.dmswindow(self.allTables)
   
    # Finances
    
    # Cash Account Book
    def on_cash_account_book1_activate(self, event):
        cab = cuon.Finances.cashAccountBook.cashAccountBookwindow(self.allTables)

    # Extras
    def on_expert_system1_activate(self, event):
        cai = cuon.AI.ai.aiwindow(self.allTables)

    # Extras
    def on_project1_activate(self, event):
        cpro = cuon.Project.project.projectwindow(self.allTables)

    # Tools   

    def on_update1_activate(self, event):
        self.updateVersion()

    def on_pref_user1_activate(self,event):
        prefs = cuon.Preferences.preferences.preferenceswindow(self.allTables)

    def on_prefs_finances_activate(self,event):
        prefs = cuon.PrefsFinance.prefsFinance.prefsFinancewindow(self.allTables)


    def on_webshop1_activate(self,event):
        print 'Webshop'
        prefs = cuon.WebShop.webshop.webshopwindow(self.allTables)

        
    def updateVersion(self):
        if self.startProgressBar():
            self.generateSqlObjects()
            self.writeAllGladeFiles()
        self.stopProgressBar()
    
    def on_import_data1_activate(self, event):
        imp1 =  cuon.Databases.import_generic1.import_generic1(self.allTables)

        
    def on_test1_activate(self, event):
        te = cuon.VTK.test.test()
        te.show()
        
    def on_about1_activate(self, event):
        about1 = self.getWidget('aCuon')
        about1.show()
        
    def on_onlinehelp_activate(self, event):
        he1 = cuon.Help.help.helpwindow()
          
    
    # hide about-info
    def on_okAbout1_clicked(self, event):
        about1 = self.getWidget('aCuon')
        about1.hide()

    # extendet Menu
    # set by Zope user control
    def on_ext1_activate(self, event):
        print 'ext1 menu activated !!!!!'
        ext1 = eval(self.extMenucommand['ext1']) 
        try:
            ext1.start()
        except:
            print 'No StartModule'


    def on_ext2_activate(self, event):
        print 'ext2 menu activated !!!!!'
        ext2 = eval(self.extMenucommand['ext2']) 
        try:
            ext2.start()
        except:
            print 'No StartModule'

    def on_ext3_activate(self, event):
        print 'ext3 menu activated !!!!!'
        ext3 = eval(self.extMenucommand['ext3']) 
        try:
            ext3.start()
        except:
            print 'No StartModule'

    def getNewClientSoftware(self, id):
        cuonpath = self.td.cuon_path
        self.infoMsg('C.U.O.N. will now try to load the new Clientversion. ')
        shellcommand = 'rm ' + cuonpath + '/newclient'
        liStatus = commands.getstatusoutput(shellcommand)
        print shellcommand, liStatus
        shellcommand = 'rm -R ' + cuonpath + '/iClient'
        liStatus = commands.getstatusoutput(shellcommand)
        print shellcommand, liStatus

        sc = cuon.Databases.SingleCuon.SingleCuon(self.allTables)
        sc.saveNewVersion(id)
        
        shellcommand = 'cd '+cuonpath+' ;  tar -xvjf newclient'
        liStatus = commands.getstatusoutput(shellcommand)
        print shellcommand, liStatus
        #shellcommand = 'sh ' + cuonpath + '/iClient/iCuon  '
        #liStatus = commands.getstatusoutput(shellcommand)
        #print shellcommand, liStatus
        
        self.infoMsg('Update complete. Please start C.U.O.N. new  ')
        
        
           
    def startMain(self, sStartType, sDebug,sLocal='NO'):
        #ML = cuon.VTK.mainLogo.mainLogo()
        #ML.startLogo()

        
        if sDebug:
            self.sDebug = sDebug
        else:
            self.sDebug = 'NO'
            
        if sStartType == 'server':
            print 'Server-Modus'
            td = typedefs_server()
            # create widget tree ...
       
            self.gladeName = '/usr/share/cuon/glade/cuon.glade2'
            self.loadGladeFile(self.gladeName)
        else:
            id, version = self.rpc.callRP('Database.getLastVersion')
            print 'Version', version
            print 'id', id
            


             
##            self.openDB()
##            version = self.loadObject('ProgramVersion')
##            self.closeDB()
##            
            print 'Version:' + str(version)

            print self.Version['Major'], version['Major']
            print self.Version['Minor'], version['Minor'] 
            print self.Version['Rev'], version['Rev']
            print self.Version, version
            
            if not version:
                print 'no Version, please inform Cuon-Administrator'

                sys.exit(0)
            
            if self.rpc.callRP('Database.checkVersion', self.Version, version) == 'Wrong':
                print ' ungleiche Versionen'
                print 'load new version of pyCuon'
                self.getNewClientSoftware(id)
                self.openDB()
                version = self.saveObject('newClientVersion',True)
                self.closeDB()
                sys.exit(0)
                
            self.openDB()
            newClientExist = self.loadObject('newClientVersion')
            self.closeDB()
            if newClientExist:
                self.updateVersion()
                self.openDB()
                self.saveObject('ProgramVersion', self.Version)
                version = self.saveObject('newClientVersion',False)
                self.closeDB()
                version = self.rpc.callRP('Database.getLastVersion')
                print 'Version', version
                if sLocal != 'NO' and  self.rpc.callRP('Database.checkVersion', self.Version, version) == 'Wrong':
                    
                    self.getNewClientSoftware(id)
                    
                    sys.exit(0)
 
            
            # create widget tree ...

            # self.gladeName = td.main_glade_name

            self.loadGlade('main.xml')
 
        # Menu-items
       
        self.initMenuItems()
        self.disableAllMenuItems()
        self.addEnabledMenuItems('login','logout1')
        self.addEnabledMenuItems('login','data')
        self.addEnabledMenuItems('login','action1')
        self.addEnabledMenuItems('login','accounting1')
        self.addEnabledMenuItems('login','extras')        
        self.addEnabledMenuItems('login','tools')

        

        self.addEnabledMenuItems('serverMode','databases1')
        
        
        

        self.addEnabledMenuItems('user','login1')
        self.addEnabledMenuItems('user','tools')
        self.addEnabledMenuItems('user','update1')
        
        self.disableMenuItem('login')
        self.disableMenuItem('serverMode')
        self.enableMenuItem('user')

        self.setTitle('window1',self.sTitle)
        #self.updateVersion()


    def gtk_main_quit(self):
        gtk.main_quit()
                

sStartType = 'client'
sDebug = 'NO'
sLocal = 'NO'

print sys.argv

if len(sys.argv) > 4: 
    if len(sys.argv[4]) > 1:
        sLocal =  sys.argv[4]
        
if len(sys.argv) > 3: 
    if len(sys.argv[3]) > 1:
        sDebug =  sys.argv[3]
        
if len(sys.argv) > 2: 
    if len(sys.argv[2]) > 1:
        sStartType =  sys.argv[2]
        print sStartType
if sStartType == 'server':
    td = cuon.TypeDefs.typedefs_server.typedefs_server()
else:
    td = cuon.TypeDefs.typedefs.typedefs()


if len(sys.argv) > 1:
    if len(sys.argv[1]) > 1:
        td.server =  sys.argv[1]
        print 'td-server =', td.server   
d = cuon.Databases.dumps.dumps(td)
d.openDB()
d.saveObject('td', td)
d.closeDB()


if sLocal == 'NO':
    DIR = '/usr/share/locale'
else:
    DIR = sLocal
    
locale.setlocale (locale.LC_ALL, '')
APP = 'cuon'
gettext.bindtextdomain (APP, DIR)
gettext.textdomain (APP)
gettext.install (APP, DIR, unicode=1)
gtk.glade.bindtextdomain(APP,DIR)
gtk.glade.textdomain(APP)


print _('Debug by C.U.O.N. = ' ), sDebug

m = MainWindow(sStartType)
m.startMain(sStartType, sDebug,sLocal)

#profile.run('m.startMain(sStartType, sDebug,sLocal)','cuonprofile')
### Import Psyco if available
##try:
##    import psyco
##    psyco.full()
##except ImportError:
##    pass

gtk.main()


