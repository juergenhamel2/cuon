<?php


/*
 * @author Jürgen Hamel
 * @license GPL V3
 * @version 0.2
coding=utf-8
Copyright (C) [2003-2018]  [Jürgen Hamel, D-32584 Löhne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

*/
session_start();
?>

<html>
<head>
<link rel="stylesheet" href="styles.css">
     <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
     </head>


  
     <body>
  
<?php  
     





/*

    require_once( 'SingleMisc.php');
    require_once( 'SingleNotes.php');
    require_once( 'cuon_Bank_SingleBank.php');
    require_once( 'lists_addresses_phone1.php');
    require_once( 'lists_addresses_phone11.php');
    require_once( 'lists_addresses_phone12.php');
    require_once( 'lists_addresses_barcode.php');
    require_once( 'commands.php');
    require_once( 'logging.php');
*/



//require_once( 'cPickle.php');
//    require_once( 'locale.php');
//require_once( 'locale.php');
/*try {
        require_once( 'webbrowser.php');
    }
    catch(Exception $e) {
            }
    require_once( 'threading.php');
    require_once( 'time.php');
    require_once( 'datetime.php');
    require_once( 'cuon_DMS_documentTools.php');
    require_once( 'cuon_DMS_SingleDMS.php');
    require_once( 'cuon_DMS_dms.php');
    require_once( 'printAddress.php');
    require_once( 'cuon_Staff_staff.php');
    require_once( 'cuon_Staff_SingleStaff.php');
    require_once( 'contact.php');
    require_once( 'cuon_E_Mail_sendEmail.php');
    require_once( 'cuon_Misc_cuon_dialog.php');
    require_once( 'cuon_Misc_misc.php');
    require_once( 'cuon_Order_order.php');
    require_once( 'cuon_PrefsFinance_prefsFinance.php');
    require_once( 'cuon_PrefsFinance_SinglePrefsFinanceTop.php');
    require_once( 'cuon_Project_project.php');
    require_once( 'cuon_Finances_invoicebook.php');

}
catch(Exception $e) {
       print_r([$Exception, $param], true);
   print_r('......................................................................... Address import false ........');
}

try {
   print_r('import graves');
    require_once( 'cuon_Graves_address_graves.php');
    require_once( 'cuon_Graves_grave.php');
    require_once( 'cuon_Garden_address_hibernation.php');
    require_once( 'cuon_Garden_hibernation.php');
}
catch(Exception $e) {
       print_r('graves import not possible');
   print_r([$Exception, $params], true);
}

$bHtml = false;
try {
    require_once( 'webkit.php');
}
catch(Exception $e) {
       print_r('python webkit  not found');
}
*/

require_once( 'SingleAddress.php');
require_once( 'SingleBank.php');
require_once( 'SinglePartner.php');
require_once( 'SingleScheduling.php');



include_once ($_SERVER['DOCUMENT_ROOT'].'/TypeDefs/typedefs.php');

include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/windows.php');
require_once($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/chooseWindows.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');


class addresswindow extends chooseWindows {
     private $str_name;
   

     public function __construct() {

          $this->elog("addresses started 0 ");
          chooseWindows::__construct();
       
          $this->elog("addresses started 1 ");
     

          $this->tabOptions = 0;
          $this->tabAddress = 0;
          $this->tabBank = 1;
          $this->tabMisc = 2;
          $this->tabPartner = 3;
          $this->tabSchedul = 4;
          $this->tabNotes = 5;
          $this->tabProposal = 6;
          $this->tabOrder = 7;
          $this->tabInvoice = 8;
          $this->tabProject = 9;
          $this->tabHtml = 10;
          $this->tabGraves = 11;
          $this->tabHibernation = 12;


          $this->elog("---- globlavar address_main = " .  $this->getVar('address_main') );

          $main = new windows();

          $this->singleAddress = new SingleAddress(Null);
          
          $this->EntriesAddresses = 'addresses.xml';

     
          $this->loadEntries($this->EntriesAddresses);
          // $this->singleAddress->setEntries($this->getDataEntries('addresses.xml'));
          $this->singleAddress->setGladeXml($this->xml, $this->win1);
          $this->singleAddress->sSort = 'lastname2';

          $glx =  $main->loadGlade("address","AddressMainwindow");
          $this->singleBank = new SingleBank(Null);
          $this->singlePartner = new SinglePartner(Null);
          $this->singleSchedul = new SingleSchedul(Null);
         
        
          $this->EntriesAddressesMisc = 'addresses_misc.xml';
          $this->EntriesAddressesBank = 'addresses_bank.xml';
          $this->EntriesPartner = 'partner.xml';
          $this->EntriesPartnerSchedul = 'partner_schedul.xml';
          $this->EntriesNotes = 'address_notes.xml';

          $liFields = callRP('Misc.getTreeInfo', 'address');
          $this->elog("qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
          $this->elog('liFields = ', json_encode( $liFields) );
          $this->singleAddress->setTreeFields($liFields);
          $this->dicEntries = $this->loadEntries('addresses');
          $this->elog("dicEntries at address = " . json_encode($this->dicEntries) );
                             
          $this->singleAddress->setEntries($this->dicEntries);
          $this->elog($this->singleAddress->getListEntries() );
        
      

          $this->tabOptions = $this->tabAddress ;
          $this->tabChanged();

             if(isset($_GET['Addresses_new'])){
          $this->elog('kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
          $this->elog('call new1 ');
          $this->elog('kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
          $this->on_new1_activate(Null,Null);
          
             }
           else if(isset($_GET['Addresses_edit'])){
          $this->elog('kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
          $this->elog('call edit1 ');
          $this->elog('kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk');
          $this->on_edit1_activate(Null);
           }
         
          
          
//echo $glx[0] ;
//echo $glx[1];

     }
     
     function __construct_orig($allTables,$addrid=0,$partnerid=0) {
          chooseWindows::__construct();
          $this->InitForms = true;
          $this->connectSchedulTreeId = null;
          $this->connectOrderTreeId = null;
          $this->connectProposalTreeId = null;
          $this->firstWebkitStart = true;
          $this->connectInvoicesTreeId = null;
          $this->connectOfferTreeId = null;
          $this->connectProjectTreeId = null;
          $this->bHtml = $bHtml;
          $this->OrderID = 0;
          $this->ProposalID = 0;
          $this->elog(['1', $bHtml], true);
          $this->oDocumentTools = $cuon->DMS->documentTools->documentTools();
          $this->ModulNumber = $this->MN['Address'];
          $this->singleAddress = SingleAddress::SingleAddress($allTables);
          $this->singleBank = SingleAddressBank::SingleAddressBank($allTables);
          $this->singleMisc = SingleMisc::SingleMisc($allTables);
          $this->singlePartner = SinglePartner::SinglePartner($allTables);
          $this->singleBank = $cuon->Bank->SingleBank->SingleBank($allTables);
          $this->singleSchedul = SingleScheduling::SingleScheduling($allTables);
          $this->singleStaff = $cuon->Staff->SingleStaff->SingleStaff($allTables);
          $this->singleAddressNotes = SingleNotes::SingleNotes($allTables);
          $this->singleDMS = $cuon->DMS->SingleDMS->SingleDMS($allTables);
          $this->singlePrefsFinanceTop = $cuon->PrefsFinance->SinglePrefsFinanceTop->SinglePrefsFinanceTop($allTables);
          $this->allTables = $allTables;
          $this->loadGlade('address.xml', 'AddressMainwindow');
          $this->setStatusBar('vb_main');
          $this->treeProposal = $cuon->Misc->misc->Treeview();
          $this->treeOrder = $cuon->Misc->misc->Treeview();
          $this->treeInvoice = $cuon->Misc->misc->Treeview();
          $this->treeProjects = $cuon->Misc->misc->Treeview();
          $this->treeProposal->start($this->getWidget('tvAddressProposal'), 'Text', 'Proposal');
          $this->treeOrder->start($this->getWidget('tvAddressOrder'), 'Text', 'Order');
          $this->treeInvoice->start($this->getWidget('tvAddressInvoices'), 'Text', 'Invoices');
          $this->treeProjects->start($this->getWidget('tvAddressProject'), 'Text', 'Projects');
       
          list($liFashion, $liTrade, $liTurnover, $liLegalform, $this->liSchedulTime) = $this->rpc->callRP('Address.getComboBoxEntries', $this->dicUser);
          $cbFashion = $this->getWidget('cbFashion');
          if ($cbFashion) {
               $liststore = $gtk->ListStore($str);
               $liststore2 = $gtk->ListStore($str);
               foreach( $liFashion as $fashion ) {
                    $liststore[] = [$fashion];
                    $liststore2[] = [$fashion];
               }
               $cbFashion->set_model($liststore);
               $this->setComboBoxTextColumn($cbFashion);
               $cbFashion->show();
               $this->cbSearchFashion = $this->getWidget('cbSearchFashion');
               $this->elog(['cbSearchFashion = ', $this->cbSearchFashion], true);
               if ($this->cbSearchFashion) {
                    $this->cbSearchFashion->set_model($liststore2);
                    $this->setComboBoxTextColumn($this->cbSearchFashion);
                    $this->cbSearchFashion->show();
               }
          }
          $cbTrade = $this->getWidget('cbTrade');
          if ($cbTrade) {
               $liststore = $gtk->ListStore($str);
               foreach( $liTrade as $trade ) {
                    $liststore[] = [$trade];
               }
               $cbTrade->set_model($liststore);
               $cell = $gtk->CellRendererText();
               $cbTrade->pack_start($cell, true);
               $cbTrade->add_attribute($cell, 'text', 1);
               $this->setComboBoxTextColumn($cbTrade);
               $cbTrade->show();
          }
          $cbTurnover = $this->getWidget('cbTurnover');
          if ($cbTurnover) {
               $liststore = $gtk->ListStore($str);
               foreach( $liTurnover as $turnover ) {
                    $liststore[] = [$turnover];
               }
               $cbTurnover->set_model($liststore);
               $this->setComboBoxTextColumn($cbTurnover);
               $cbTurnover->show();
          }
          $cbLegalform = $this->getWidget('cbLegalForm');
          if ($cbLegalform) {
               $liststore = $gtk->ListStore($str);
               foreach( $liLegalform as $legalform ) {
                    $liststore[] = [$legalform];
               }
               $cbLegalform->set_model($liststore);
               $this->setComboBoxTextColumn($cbLegalform);
               $cbLegalform->show();
          }
          $cbSchedultime = $this->getWidget('cbeSchedulTimeBegin');
          $cbSchedultime2 = $this->getWidget('cbeSchedulTimeEnd');
          if ($cbSchedultime && $cbSchedultime2) {
               $liststore = $gtk->ListStore($str);
               if (($this->liSchedulTime != ['NONE'])) {
                    foreach( $this->liSchedulTime as $timeBegin ) {
                         $liststore[] = [$timeBegin];
                    }
                    $cbSchedultime->set_model($liststore);
                    $this->setComboBoxTextColumn($cbSchedultime);
                    $cbSchedultime->show();
                    $cbSchedultime2->set_model($liststore);
                    $this->setComboBoxTextColumn($cbSchedultime2);
                    $cbSchedultime2->show();
               }
          }



        
          $this->elog(['setStore = ', $this->getListStore($liFields[2])], true);
          if ($liFields) {
               $this->liSearchFields = $liFields[0];
               $this->singleAddress->setTreeFields($liFields[1]);
               $this->singleAddress->setStore($this->getListStore($liFields[2]));
               $this->singleAddress->setListHeader($liFields[3]);
               $this->singleAddress->setTreeOrder('lastname, firstname');
          }
          else {
               $this->singleAddress->setTreeFields(['lastname', 'firstname', 'city', 'phone', 'status_info']);
               $this->singleAddress->setStore($gtk->ListStore($gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_UINT));
               $this->singleAddress->setTreeOrder('lastname, firstname');
               $this->singleAddress->setListHeader([$_('Lastname'), $_('Firstname'), $_('City'), $_('Phone'), $_('Info')]);
          }
          if (($addrid > 0)) {
               $this->singleAddress->sWhere = ' where id = ' . pyjslib_repr($addrid);
          }
          $this->singleAddress->setTree($this->getWidget('tv_address'));
          $this->loadEntries($this->EntriesAddressesBank);
          $this->singleBank->setEntries($this->getDataEntries($this->EntriesAddressesBank));
          $this->singleBank->setGladeXml($this->xml);
          $this->singleBank->setTreeFields(['bank_ranking', 'depositor', 'account_number', 'address_id', 'id']);
          $this->singleBank->setStore($gtk->ListStore($gobject->TYPE_UINT, $gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_UINT, $gobject->TYPE_UINT));
          $this->singleBank->setTreeOrder('bank_ranking,depositor, account_number');
          $this->singleBank->setListHeader([$_('Ranking'), $_('Depositor'), $_('Account'), $_('Bank'), $_('id')]);
          $this->singleBank->sWhere = 'where address_id = ' . pyjslib_repr($this->singleAddress->ID);
          $this->singleBank->setTree($this->getWidget('tv_bank'));
          $this->loadEntries($this->EntriesAddressesMisc);
          $this->singleMisc->setEntries($this->getDataEntries('addresses_misc.xml'));
          $this->singleMisc->setGladeXml($this->xml);
          $this->singleMisc->setTreeFields(['id']);
          $this->singleMisc->setTreeOrder('id');
          $this->singleMisc->sWhere = 'where address_id = ' . pyjslib_repr($this->singleAddress->ID);
          $this->loadEntries($this->EntriesPartner);
          $this->singlePartner->setEntries($this->getDataEntries('partner.xml'));
          $this->singlePartner->setGladeXml($this->xml);
          $this->singlePartner->setTreeFields(['lastname', 'firstname', 'city', 'id']);
          $this->singlePartner->setStore($gtk->ListStore($gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_UINT));
          $this->singlePartner->setTreeOrder('lastname, firstname');
          $this->singlePartner->setListHeader([$_('Name of partner'), $_('Firstname of partner'), $_('City'), $_('id')]);
          $this->singlePartner->sWhere = 'where addressid = ' . pyjslib_repr($this->singleAddress->ID);
          $this->singlePartner->setTree($this->getWidget('tv_partner'));
          $this->loadEntries($this->EntriesPartnerSchedul);
          $this->singleSchedul->setEntries($this->getDataEntries('partner_schedul.xml'));
          $this->singleSchedul->setGladeXml($this->xml);
          $this->singleSchedul->setTreeFields(['schedul_date', 'to_char(round(schedul_time_begin/4),\'99\') || \':\' || to_char(round(mod(schedul_time_begin,4)*15,0),\'09MI\') as begindate ', 'to_char(round(schedul_time_end/4),\'99\') || \':\' || to_char(round(mod(schedul_time_end,4)*15,0),\'09MI\') as enddate ', 'short_remark', 'priority', 'process_status', 'id']);
          $this->singleSchedul->setStore($gtk->ListStore($gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_STRING, $gobject->TYPE_UINT, $gobject->TYPE_UINT, $gobject->TYPE_UINT));
          $this->singleSchedul->setTreeOrder('to_date(schedul_date,' . '\'' . $this->dicUser['SQLDateFormat'] . '\')' . ',schedul_time_begin');
          $this->singleSchedul->setListHeader([$_('Date '), $_('begin'), $_('end'), $_('short Remark'), $_('Priority'), $_('Status'), $_('id')]);
          $this->singleSchedul->sWhere = 'where partnerid = ' . pyjslib_repr($this->singlePartner->ID) . ' and process_status != 999 ';
          $this->singleSchedul->setTree($this->getWidget('tv_scheduls'));
          $this->loadEntries($this->EntriesNotes);
          $this->singleAddressNotes->setEntries($this->getDataEntries('address_notes.xml'));
          $this->singleAddressNotes->setGladeXml($this->xml);
          $this->singleAddressNotes->setTreeFields(['id']);
          $this->singleAddressNotes->setTreeOrder('id');
          $this->singleAddressNotes->sWhere = 'where address_id = ' . pyjslib_repr($this->singleAddress->ID);
          $this->cbSearchPartner = $this->getWidget('cbSearchPartner');
          $this->liSearchWidgets = ['eFindName', 'eFindName2', 'eFindCity', 'eFindZipcode', 'eFindFirstname', 'eFindID', 'eFindStreet', 'eFindPhone', 'eFindFax', 'eFindNewsletter', 'eFindInfo', 'eFindEmail'];
          list($setGraveButton, $setGraveButtonPosition) = $this->rpc->callRP('Address.getButtonGrave', $this->dicUser);
          $this->elog(['gravebutton =', $setGraveButton, $setGraveButtonPosition], true);
          if (($setGraveButton == 'YES')) {
               $this->graves = $cuon->Graves->address_graves->address_graves();
               $this->elog($this->graves);
               $this->elog('grave button found');
               $tb1 = $this->getWidget('toolbar1');
               $this->elog(['toolbar =', $tb1], true);
               //$button1 = callProc_kwargs_method_call($gtk, 'ToolButton', [], ["icon_widget" => null,"label" => $_('Graves')]);
               $this->elog(['button1', $button1], true);
               $tb1->insert($button1, -1);
               $button1->connect('clicked', $this->on_tbGrave_clicked);
               $this->elog('button added');
               $button1->show();
               $nb1 = $this->getWidget('notebook1');
               $scGrave = $gtk->ScrolledWindow();
               //$this->tvGrave = callProc_kwargs_method_call($gtk, 'TreeView', [], ["model" => null]);
               $this->elog(['tvGrave', $this->tvGrave], true);
               $scGrave->add($this->tvGrave);
               //$vbGrave = callProc_kwargs_method_call($gtk, 'VBox', [], ["homogeneous" => false,"spacing" => 0]);
               $vbGrave->add($scGrave);
               $lb = $gtk->Label($_('Graves'));
               //$iNr = callProc_kwargs_method_call($nb1, 'insert_page', [$vbGrave], ["tab_label" => $lb,"position" => -1]);
               $this->elog(['page = ', $iNr], true);
               $vbGrave->show_all();
          }
          list($setHibernationButton, $setHibernationButtonPosition) = $this->rpc->callRP('Address.getButtonHibernation', $this->dicUser);
          $this->elog(['Hibernationbutton =', $setHibernationButton, $setHibernationButtonPosition], true);
          if (($setHibernationButton == 'YES')) {
               $this->hibernation = $cuon->Garden->address_hibernation->address_hibernation();
               $this->elog($this->hibernation);
               $this->elog('Hibernation button found');
               $tb1 = $this->getWidget('toolbar1');
               $this->elog(['toolbar =', $tb1], true);
               //$button2 = callProc_kwargs_method_call($gtk, 'ToolButton', [], ["icon_widget" => null,"label" => $_('Hibernations')]);
               $this->elog(['button2', $button2], true);
               $tb1->insert($button2, -1);
               $button2->connect('clicked', $this->on_tbHibernation_clicked);
               $this->elog('button added');
               $button2->show();
               $nb1 = $this->getWidget('notebook1');
               $scHibernation = $gtk->ScrolledWindow();
               //$this->tvHibernation = callProc_kwargs_method_call($gtk, 'TreeView', [], ["model" => null]);
               $this->elog(['tvHibernation', $this->tvHibernation], true);
               $scHibernation->add($this->tvHibernation);
               //$vbHibernation = callProc_kwargs_method_call($gtk, 'VBox', [], ["homogeneous" => false,"spacing" => 0]);
               $vbHibernation->add($scHibernation);
               $lb = $gtk->Label($_('Hibernation'));
               //$iNr = callProc_kwargs_method_call($nb1, 'insert_page', [$vbHibernation], ["tab_label" => $lb,"position" => -1]);
               $this->elog(['page = ', $iNr], true);
               $vbHibernation->show_all();
          }
          list($this->textbufferMisc, $this->viewMisc) = $this->getNotesEditor();
          $Vbox = $this->getWidget('vbox10');
          $oldScrolledwindow = $this->getWidget('scrolledwindow6');
          $oldScrolledwindow->remove($this->getWidget('tvNotesMisc'));
          $oldScrolledwindow->add($this->viewMisc);
          $this->viewMisc->show_all();
          $oldScrolledwindow->show_all();
          $this->singleAddressNotes->NotesMisc = $this->viewMisc->get_buffer();
          list($this->textbufferContact, $this->viewContact) = $this->getNotesEditor();
          $Vbox = $this->getWidget('vbox11');
          $oldScrolledwindow = $this->getWidget('scrolledwindow7');
          $oldScrolledwindow->remove($this->getWidget('tvNotesContacter'));
          $oldScrolledwindow->add($this->viewContact);
          $this->viewContact->show_all();
          $oldScrolledwindow->show_all();
          $this->singleAddressNotes->NotesContact = $this->textbufferContact;
          list($this->textbufferRep, $this->viewRep) = $this->getNotesEditor();
          $Vbox = $this->getWidget('vbox12');
          $oldScrolledwindow = $this->getWidget('scrolledwindow8');
          $oldScrolledwindow->remove($this->getWidget('tvNotesRep'));
          $oldScrolledwindow->add($this->viewRep);
          $this->viewRep->show_all();
          $oldScrolledwindow->show_all();
          $this->singleAddressNotes->NotesRep = $this->textbufferRep;
          list($this->textbufferSalesman, $this->viewSalesman) = $this->getNotesEditor();
          $Vbox = $this->getWidget('vbox13');
          $oldScrolledwindow = $this->getWidget('scrolledwindow9');
          $oldScrolledwindow->remove($this->getWidget('tvNotesSalesman'));
          $oldScrolledwindow->add($this->viewSalesman);
          $this->viewSalesman->show_all();
          $oldScrolledwindow->show_all();
          $this->singleAddressNotes->NotesSalesman = $this->textbufferSalesman;
          list($this->textbufferOrganisation, $this->viewOrganisation) = $this->getNotesEditor();
          $Vbox = $this->getWidget('vbox17');
          $oldScrolledwindow = $this->getWidget('scrolledwindow15');
          $oldScrolledwindow->remove($this->getWidget('tvNotesOrganisation'));
          $oldScrolledwindow->add($this->viewOrganisation);
          $this->viewOrganisation->show_all();
          $oldScrolledwindow->show_all();
          $this->singleAddressNotes->NotesOrganisation = $this->textbufferOrganisation;
          $this->initMenuItems();
          $this->addEnabledMenuItems('window', 'quit1', 'q');
          $this->notebook2 = $this->getWidget('notebook2');
          $this->addEnabledMenuItems('tabs', 'mi_address1');
          $this->addEnabledMenuItems('tabs', 'mi_bank1');
          $this->addEnabledMenuItems('tabs', 'mi_misc1');
          $this->addEnabledMenuItems('tabs', 'mi_partner1');
          $this->addEnabledMenuItems('tabs', 'mi_schedul1');
          $this->addEnabledMenuItems('tabs', 'mi_notes1');
          $this->addEnabledMenuItems('tabs', 'mi_order1');
          $this->addEnabledMenuItems('tabs', 'mi_projects1');
          $this->addEnabledMenuItems('address', 'mi_address1');
          $this->addEnabledMenuItems('partner', 'mi_partner1');
          $this->addEnabledMenuItems('schedul', 'mi_schedul1');
          $this->addEnabledMenuItems('bank', 'mi_bank1');
          $this->addEnabledMenuItems('misc', 'mi_misc1');
          $this->addEnabledMenuItems('notes', 'mi_notes1');
          $this->addEnabledMenuItems('order', 'mi_order1');
          $this->addEnabledMenuItems('project', 'mi_projects1');
          $this->addEnabledMenuItems('editAddress', 'mi_new1', $this->dicUserKeys['address_new']);
          $this->addEnabledMenuItems('editAddress', 'mi_clear1', $this->dicUserKeys['address_delete']);
          $this->addEnabledMenuItems('editAddress', 'mi_print1', $this->dicUserKeys['address_print']);
          $this->addEnabledMenuItems('editAddress', 'mi_edit1', $this->dicUserKeys['address_edit']);
          $this->addEnabledMenuItems('editMisc', '');
          $this->addEnabledMenuItems('editPartner', 'PartnerNew1', $this->dicUserKeys['address_partner_new']);
          $this->addEnabledMenuItems('editPartner', 'PartnerDelete1', $this->dicUserKeys['address_partner_delete']);
          $this->addEnabledMenuItems('editPartner', 'PartnerEdit1', $this->dicUserKeys['address_partner_edit']);
          $this->addEnabledMenuItems('editSchedul', 'SchedulNew', $this->dicUserKeys['address_new']);
          $this->addEnabledMenuItems('editSchedul', 'SchedulEdit1', $this->dicUserKeys['address_edit']);
          $this->addEnabledMenuItems('editNotes', 'NotesEdit1', $this->dicUserKeys['address_edit']);
          $this->addEnabledMenuItems('editSave', 'mi_save1', $this->dicUserKeys['address_save']);
          $this->addEnabledMenuItems('editSave', 'PartnerSave1', $this->dicUserKeys['address_partner_save']);
          $this->addEnabledMenuItems('editSave', 'NotesSave', $this->dicUserKeys['address_save']);
          $this->addEnabledMenuItems('editSave', 'schedulSave', $this->dicUserKeys['address_save']);
          $this->addEnabledMenuItems('editSave', 'mi_MiscSave1', $this->dicUserKeys['address_save']);
  
          $ts = $this->getWidget('treeScheduls');
          $renderer = $gtk->CellRendererText();
          //$column = callProc_kwargs_method_call($gtk, 'TreeViewColumn', [$_('Scheduls'),$renderer], ["text" => 0]);
          $ts->append_column($column);
          $this->notebook2 = $this->getWidget('notebook2');
          $this->swMap = $this->getWidget('swMap');
          $this->webview = null;
          $this->win1->add_accel_group($this->accel_group);
          if (($partnerid > 0)) {
               $this->tabChanged();
               $this->singlePartner->sWhere .= ' and id = ' . pyjslib_repr($partnerid);
               $this->setMainwindowNotebook('F4');
          }
          else {
               $this->tabChanged();
          }
     }
     public function __destruct() {
          // echo 'Destroying: ', $this->name, PHP_EOL;
     }


     function CleanUp() {
          $this->elog('cleanUp');
          try {
               if ($this->webview) {
                    $this->swMap->remove($this->webview);
               }
          }
          catch(Exception $e) {
               $this->elog([$Exception, $param], true);
          }
     }
     function on_quit1_activate($event) {
          $this->elog('exit addresses v2');
          $this->closeWindow();
     }
     function on_newsletter_email_activate($event) {
          $dicV = [];
          $dicV['From'] = $this->dicUser['Email']['From'];
          $dicV['To'] = 'Newsletter: ';
          $dicV['Signatur'] = '

 -- 
' . $this->dicUser['Email']['Signatur'];
          $this->elog($dicV);
          $em = $cuon->E_Mail->sendEmail->sendEmail($dicV);
     }
     function on_newsletter_print_activate($event) {
          $Dms = $cuon->DMS->dms->dmswindow($this->allTables, $this->MN['Newsletter']);
     }
     function on_save1_activate($event) {
          $this->elog('save addresses v2');
          if (($this->singleAddress->save() == 0)) {
               $this->errorMsg($_('saving this Address failed. Please check'));
          }
          else {
               $this->doEdit = $this->noEdit;
               $this->setEntriesEditable($this->EntriesAddresses, false);
               $this->endEdit();
               $this->tabChanged();
          }
     }

  

     function on_new1_activate($obj_subject, $parr_params) {
          $this->elog('new addresses v2');
          $this->doEdit = $this->tabAddress;
          $this->singleAddress->newRecord();
          $this->setEntriesEditable($this->EntriesAddresses, true);
          $this->getWidget('eAddress')->grab_focus();
          $this->startEdit();
     }
     function on_edit1_activate($event) {
          $this->elog('edit addresses v2');
          $this->doEdit = $this->tabAddress;
          $this->setEntriesEditable($this->EntriesAddresses, true);
          //$this->getWidget('eAddress')->grab_focus();
          //$this->startEdit();
          //$adrdetail = new address_detail();
          //$adrdetail->fillFields($this->getVar("address_id") );
     }
     function on_print1_activate($event) {
          $this->elog('print addresses v2');
          $p = $printAddress->printAddress($this->singleAddress->getFirstRecord());
     }
     function on_delete1_activate($event) {
          $this->elog('delete addresses v2');
          $this->singleAddress->deleteRecord();
     }
     function on_bank_save1_activate($event) {
          $this->elog('save Bank addresses v2');
          $this->singleBank->addressId = $this->singleAddress->ID;
          if (($this->singleBank->save() == 0)) {
               $this->errorMsg($_('saving this bank failed. Please check'));
          }
          else {
               $this->doEdit = $this->noEdit;
               $this->setEntriesEditable($this->EntriesAddressesBank, false);
               $this->endEdit();
               $this->tabChanged();
          }
     }
     function on_bank_new1_activate($event) {
          $this->elog('bank new activate');
          $this->doEdit = $this->tabBank;
          $this->singleBank->newRecord();
          $this->setEntriesEditable($this->EntriesAddressesBank, true);
          $this->startEdit();
     }
     function on_bank_edit1_activate($event) {
          $this->elog('bank edit activate');
          $this->elog('edit addresses v2');
          $this->doEdit = $this->tabBank;
          $this->setEntriesEditable($this->EntriesAddressesBank, true);
          $this->startEdit();
     }
     function on_bank_delete1_activate($event) {
          $this->elog('bank delete activate');
          $this->elog('delete addresses v2');
          $this->singleBank->deleteRecord();
     }
     function on_MiscSave1_activate($event) {
          $this->elog('save Misc addresses v2');
          $this->doEdit = $this->noEdit;
          $this->singleMisc->addressId = $this->singleAddress->ID;
          $this->singleMisc->save();
          $this->setEntriesEditable($this->EntriesAddressesMisc, false);
          $this->tabChanged();
     }
     function on_MiscEdit1_activate($event) {
          $this->elog('edit addresses v2');
          $this->doEdit = $this->tabMisc;
          $this->setEntriesEditable($this->EntriesAddressesMisc, true);
     }
     function on_PartnerSave1_activate($event) {
          $this->elog('save Partner addresses v2');
          $this->singlePartner->addressId = $this->singleAddress->ID;
          if (($this->singlePartner->save() == 0)) {
               $this->errorMsg($_('saving this partner failed. Please check'));
          }
          else {
               $this->doEdit = $this->noEdit;
               $this->setEntriesEditable($this->EntriesPartner, false);
               $this->endEdit();
               $this->tabChanged();
          }
     }
     function on_PartnerNew1_activate($event) {
          $this->elog('new Partner addresses v2');
          $this->doEdit = $this->tabPartner;
          $this->singlePartner->newRecord();
          $this->setEntriesEditable($this->EntriesPartner, true);
          $this->startEdit();
     }
     function on_PartnerEdit1_activate($event) {
          $this->doEdit = $this->tabPartner;
          $this->setEntriesEditable($this->EntriesPartner, true);
          $this->startEdit();
     }
     function on_PartnerDelete1_activate($event) {
          $this->elog('delete Partner addresses v2');
          $this->singlePartner->deleteRecord();
     }
     function on_SchedulSave_activate($event) {
          $nID = 0;
          $id = 0;
          $this->elog('save Schedul addresses v2');
          $this->singleSchedul->partnerId = $this->singlePartner->ID;
          $this->elog(['ID = ', $this->singleSchedul->ID], true);
          $id = $this->singleSchedul->save();
          if (($id == 0)) {
               $this->errorMsg($_('saving this schedul failed. Please check'));
          }
          else {
               $this->doEdit = $this->noEdit;
               $this->elog('save ready');
               $this->singleSchedul->load($id);
               $sCalendar = 'iCal_' . $this->dicUser['Name'];
               $this->rpc->callRP('Web.addCalendarEvent', $sCalendar, $this->singleSchedul->firstRecord, $this->dicUser);
               $this->setEntriesEditable($this->EntriesPartnerSchedul, false);
               $this->endEdit();
               $this->tabChanged();
          }
     }
     function on_SchedulEdit1_activate($event) {
          $this->doEdit = $this->tabSchedul;
          $this->setEntriesEditable($this->EntriesPartnerSchedul, true);
          $this->startEdit();
     }
     function on_SchedulNew_activate($event) {
          $this->elog('new Schedul for partner v2');
          $this->doEdit = $this->tabSchedul;
          $this->singleSchedul->newRecord();
          $this->setEntriesEditable($this->EntriesPartnerSchedul, true);
          $this->startEdit();
     }
     function on_SchedulDelete_activate($event) {
          $this->elog('delete Schedul addresses v2');
          $this->singleSchedul->deleteRecord();
     }
     function on_NotesSave_activate($event) {
          $this->elog('save Notes addresses v2');
          $this->singleAddressNotes->addressId = $this->singleAddress->ID;
          $this->singleAddressNotes->NotesMisc = $this->viewMisc->get_buffer();
          $this->singleAddressNotes->NotesContact = $this->viewContact->get_buffer();
          $this->singleAddressNotes->NotesOrganisation = $this->viewOrganisation->get_buffer();
          $this->singleAddressNotes->NotesRep = $this->viewRep->get_buffer();
          $this->singleAddressNotes->NotesSalesman = $this->viewSalesman->get_buffer();
          if (($this->singleAddressNotes->save() == 0)) {
               $this->errorMsg($_('save this notes failed. Please check'));
          }
          else {
               $this->doEdit = $this->noEdit;
               $this->viewMisc->set_editable(false);
               $this->viewContact->set_editable(false);
               $this->viewRep->set_editable(false);
               $this->viewSalesman->set_editable(false);
               $this->viewOrganisation->set_editable(false);
               $this->setEntriesEditable($this->EntriesNotes, false);
               if ($this->rpc->callRP('Misc.sendNotes0', $this->dicUser, $this->notebook2->get_current_page())) {
                    $this->liEmailAddresses = $this->rpc->callRP('Misc.getAdditionalEmailAddressesNotes0', $this->singleAddress->ID, $this->dicUser);
                    if ($this->liEmailAddresses && !in_array($this->liEmailAddresses, ['NONE', 'ERROR'])) {
                         $this->singleDMS->loadNotes0SaveDocument();
                         list($dicVars, $dicExtInfo) = $this->getAddressInfos();
                         $dicVars['email_subject'] = pyjslib_repr($dicVars['id']) . ', ' . $_('CUON-ID NOTES-01');
                         $dicVars['Body'] = $_('Notes are changed !') . '
 ID = ' . pyjslib_repr($dicVars['id']) . '

 ' . $dicVars['lastname'] . '
' . $dicVars['lastname2'] . '
';
                         $dicVars['Body'] .= '

' . $dicVars['street'] . '
';
                         $dicVars['Body'] .= $dicVars['zip'] . ' ' . $dicVars['city'] . '

';
                         $dicVars['Body'] += $_('Infos are generated by C.U.O.N.');
                         $this->oDocumentTools->viewDocument($this->singleDMS, $this->dicUser, $dicVars, 'sentAutomaticEmail', $this->liEmailAddresses);
                    }
               }
               $this->tabChanged();
          }
     }
     function on_NotesEdit1_activate($event) {
          $this->elog('edit notes v2');
          $this->doEdit = $this->tabNotes;
          $this->setEntriesEditable($this->EntriesNotes, true);
          $this->viewMisc->set_editable(true);
          $this->viewContact->set_editable(true);
          $this->viewRep->set_editable(true);
          $this->viewSalesman->set_editable(true);
          $this->viewOrganisation->set_editable(true);
     }
     function on_notes_undo1_activate($event) {
          $iPage = $this->notebook2->current_page();
          $this->elog($iPage);
          if (($iPage == 0)) {
               $this->viewMisc->get_buffer()->undo();
          }
          else if (($iPage == 1)) {
               $this->textbufferContact->undo();
          }
          else if (($iPage == 2)) {
               $this->textbufferRep->undo();
          }
          else if (($iPage == 3)) {
               $this->textbufferSalesman->undo();
          }
          else if (($iPage == 4)) {
               $this->textbufferOrganisation->undo();
          }
     }
     function on_notes_redo1_activate($event) {
          $iPage = $this->notebook2->current_page();
          $this->elog($iPage);
          if (($iPage == 0)) {
               $this->viewMisc->get_buffer()->redo();
          }
          else if (($iPage == 1)) {
               $this->textbufferContact->redo();
          }
          else if (($iPage == 2)) {
               $this->textbufferRep->redo();
          }
          else if (($iPage == 3)) {
               $this->textbufferSalesman->redo();
          }
          else if (($iPage == 4)) {
               $this->textbufferOrganisation->redo();
          }
     }
     function on_notes_cut1_activate($event) {
          $iPage = $this->notebook2->current_page();
          $this->elog($iPage);
          if (($iPage == 0)) {
               $this->viewMisc->get_buffer()->cut_clipboard($this->clipboard, $this->viewMisc->get_editable());
          }
          else if (($iPage == 1)) {
               $this->textbufferContact->cut_clipboard($this->clipboard, $this->viewContact->get_editable());
          }
          else if (($iPage == 2)) {
               $this->textbufferRep->cut_clipboard($this->clipboard, $this->viewRep->get_editable());
          }
          else if (($iPage == 3)) {
               $this->textbufferSalesman->cut_clipboard($this->clipboard, $this->viewSalesman->get_editable());
          }
          else if (($iPage == 4)) {
               $this->textbufferOrganisation->cut_clipboard($this->clipboard, $this->viewOrganisation->get_editable());
          }
     }
     function on_notes_copy1_activate($event) {
          $iPage = $this->notebook2->current_page();
          $this->elog($iPage);
          if (($iPage == 0)) {
               $this->viewMisc->get_buffer()->copy_clipboard($this->clipboard);
          }
          else if (($iPage == 1)) {
               $this->textbufferContact->copy_clipboard($this->clipboard);
          }
          else if (($iPage == 2)) {
               $this->textbufferRep->copy_clipboard($this->clipboard);
          }
          else if (($iPage == 3)) {
               $this->textbufferSalesman->copy_clipboard($this->clipboard);
          }
          else if (($iPage == 4)) {
               $this->textbufferOrganisation->copy_clipboard($this->clipboard);
          }
     }
     function on_notes_paste1_activate($event) {
          $iPage = $this->notebook2->current_page();
          $this->elog($iPage);
          if (($iPage == 0)) {
               $this->viewMisc->get_buffer()->paste_clipboard($this->clipboard, null, $this->viewMisc->get_editable());
          }
          else if (($iPage == 1)) {
               $this->textbufferContact->paste_clipboard($this->clipboard, null, $this->viewContact->get_editable());
          }
          else if (($iPage == 2)) {
               $this->textbufferRep->paste_clipboard($this->clipboard, null, $this->viewRep->get_editable());
          }
          else if (($iPage == 3)) {
               $this->textbufferSalesman->paste_clipboard($this->clipboard, null, $this->viewSalesman->get_editable());
          }
          else if (($iPage == 4)) {
               $this->textbufferOrganisation->paste_clipboard($this->clipboard, null, $this->viewOrganisation->get_editable());
          }
     }
     function on_calendar1_day_selected_double_click($event) {
          $this->elog($event);
          $cal = $this->getWidget('calendar1');
          if ($cal) {
               $this->elog($cal->get_date());
               $t0 = $cal->get_date();
               $this->elog($t0);
               $t1 = pyjslib_repr($t0[0]) . ' ' . pyjslib_repr(($t0[1] + 1)) . ' ' . pyjslib_repr($t0[2]);
               $this->elog($t1);
               $t2 = $time->localtime($time->mktime($time->strptime($t1, '%Y %m %d')));
               $sTime = $time->strftime($this->dicUser['DateformatString'], $t2);
               $this->elog($sTime);
               if ($this->getWidget('rbBeginDate')->get_active()) {
                    $eDate = $this->getWidget('eSchedulDateBegin');
               }
               else {
                    $eDate = $this->getWidget('eSchedulDateEnd');
               }
               $eDate->set_text($sTime);
          }
     }
     function on_bPartnerSip_clicked($event) {
          $this->elog('Sip dial startet');
          $cmd1 = $this->dicUser['prefApps']['SIP'];
          $this->elog(['Address cmd1', $cmd1], true);
          $this->elog(['1', $this->dicUser['prefApps']['SIP_PARAMS']], true);
          $this->elog(['2', $this->singlePartner->firstRecord['sip']], true);
          $this->startExternalPrg($cmd1, $this->dicUser['prefApps']['SIP_PARAMS'], $this->singlePartner->firstRecord['sip']);
     }
     function on_eSchedulDate_changed($event) {
          $this->elog($event);
          $this->setDateToCalendar($event->get_text(), 'calendar1');
     }
     function on_liAddressesPhone1_activate($event) {
          $Pdf = $lists_addresses_phone1->lists_addresses_phone1();
     }
     function on_liAddressesPhone12_activate($event) {
          $this->elog('lists 12 startet');
          $Pdf = $lists_addresses_phone12->lists_addresses_phone12();
     }
     function on_liAddressesPhone11_activate($event) {
          $this->elog('lists startet');
          $Pdf = $lists_addresses_phone11->lists_addresses_phone11();
     }
     function on_barcode_activate($event) {
          $this->elog('barcode startet');
          $Pdf = $lists_addresses_barcode->lists_addresses_barcode();
     }
     function on_newletter1_activate($event) {
          $this->elog('writer startet ');
          $fkey = 'cuonAddress' . pyjslib_repr($this->singleAddress->ID);
          $this->elog($fkey);
          $this->pickleObject($fkey, $this->singleAddress->getAddress($this->singleAddress->ID));
          $sExec = $os->environ['CUON_OOEXEC'];
          $os->system($sExec . ' cuon/OpenOffice/ooMain.py ' . $fkey);
     }
     function on_bShowDMS_clicked($event) {
          $this->elog('dms clicked');
          if (($this->singleAddress->ID > 0)) {
               $this->elog(['ModulNumber', $this->ModulNumber], true);
               //$Dms = callProc_kwargs_method_call($cuon->DMS->dms, 'dmswindow', [$this->allTables], ["module" => $this->ModulNumber,"sep_info" => ['1' => $this->singleAddress->ID]]);
          }
     }
     function on_bGeneratePartner_clicked($event) {
          $this->activateClick('PartnerNew1');
          try {
               $this->getWidget('ePartnerLastname')->set_text($this->singleAddress->getLastname());
               $this->getWidget('ePartnerFirstname')->set_text($this->singleAddress->getFirstname());
               $this->getWidget('ePartnerStreet')->set_text($this->singleAddress->getStreet());
               $this->getWidget('ePartnerZip')->set_text($this->singleAddress->getZip());
               $this->getWidget('ePartnerCity')->set_text($this->singleAddress->getCity());
               $this->getWidget('ePartnerCountry')->set_text($this->singleAddress->getCountry());
               $this->getWidget('ePartnerLetterAddress')->set_text($this->singleAddress->getLetterAddress());
          }
          catch(Exception $e) {
               $this->elog([$Exception, $params], true);
          }
          $this->activateClick('PartnerSave1');
     }
     function on_bContact_clicked($event) {
          $this->elog('Contact pressed');
          if (($this->tabOption == $this->tabAddress)) {
               $con1 = $contact->contactwindow($this->allTables, $this->singleAddress->ID, 0);
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->on_bPartnerContact_clicked($event);
          }
     }
     function on_bPartnerContact_clicked($event) {
          $con1 = $contact->contactwindow($this->allTables, $this->singleAddress->ID, $this->singlePartner->ID);
     }
     function on_bLetter_clicked($event) {
          $this->elog('bLetter clicked');
          if (($this->singleAddress->ID > 0)) {
               list($firstRecord, $dicExtInfo) = $this->getAddressInfos();
               $this->setClipboard($this->singleAddress->getEmail());
               $Dms = $cuon->DMS->dms->dmswindow($this->allTables, $this->MN['Address_info'], ['1' => -101], $firstRecord, $dicExtInfo);
          }
     }
     function on_bShowPartnerDMS_clicked($event) {
          $this->elog('dms Partner clicked');
          if (($this->singlePartner->ID > 0)) {
               $this->elog(['ModulNumber', $this->MN['Partner']], true);
               $Dms = $cuon->DMS->dms->dmswindow($this->allTables, $this->MN['Partner'], ['1' => $this->singlePartner->ID]);
          }
     }
     function on_bPartnerLetter_clicked($event) {
          $this->elog('bPartnerLetter clicked');
          if (($this->singleAddress->ID > 0)) {
               $dicExtInfo = ['sep_info' => ['1' => $this->singlePartner->ID], 'Modul' => $this->MN['Partner']];
               $dicPartner = $this->singlePartner->firstRecord;
               foreach( $this->singleAddress->firstRecord->keys() as $key ) {
                    $dicPartner['address_' . $key] = $this->singleAddress->firstRecord[$key];
               }
               $dicInternInformation = $this->rpc->callRP('Database.getInternInformation', $this->dicUser);
               if (!in_array($dicInternInformation, ['NONE', 'ERROR'])) {
                    foreach( $dicInternInformation as $key ) {
                         $dicPartner[$key] = $dicInternInformation[$key];
                    }
               }
               $dicNotes = $this->rpc->callRP('Address.getNotes', $this->singleAddress->ID, $this->dicUser);
               if ($dicNotes && !in_array($dicNotes, ['NONE', 'ERROR'])) {
                    foreach( $dicNotes as $key ) {
                         $dicPartner['notes_' . $key] = $dicNotes[$key];
                    }
               }
               $dicPartner = $this->addDateTime($dicPartner);
               $this->elog($dicPartner);
               $this->setClipboard($this->singlePartner->getEmail());
               $Dms = $cuon->DMS->dms->dmswindow($this->allTables, $this->MN['Partner_info'], ['1' => -102], $dicPartner, $dicExtInfo);
          }
     }
     function on_bSchedulLetter_clicked($event) {
          $this->elog('bSchulLetter clicked');
          if (($this->singleSchedul->ID > 0)) {
               list($dicSchedul, $dicExtInfo) = $this->getSchedulInfos();
               $Dms = $cuon->DMS->dms->dmswindow($this->allTables, $this->MN['Partner_Schedul_info'], ['1' => -103], $dicSchedul, $dicExtInfo);
          }
     }
     function on_bPhone_clicked($event) {
     }
     function getAddressInfos() {
          $firstRecord = null;
          if (($this->singleAddress->ID > 0)) {
               $firstRecord = $this->singleAddress->firstRecord;
               $this->elog(['ModulNumber', $this->ModulNumber], true);
               $dicNotes = $this->rpc->callRP('Address.getNotes', $this->singleAddress->ID, $this->dicUser);
               if ($dicNotes && !in_array($dicNotes, ['NONE', 'ERROR'])) {
                    foreach( $dicNotes as $key ) {
                         $firstRecord['notes_' . $key] = $dicNotes[$key];
                    }
               }
               $dicMisc = $this->rpc->callRP('Address.getMisc', $this->singleAddress->ID, $this->dicUser);
               if ($dicMisc && !in_array($dicMisc, ['NONE', 'ERROR'])) {
                    foreach( $dicMisc as $key ) {
                         $firstRecord['misc_' . $key] = $dicMisc[$key];
                    }
               }
               $firstRecord = $this->addDateTime($firstRecord);
               $dicExtInfo = ['sep_info' => ['1' => $this->singleAddress->ID], 'Modul' => $this->ModulNumber];
               $this->elog($firstRecord);
               $dicInternInformation = $this->rpc->callRP('Database.getInternInformation', $this->dicUser);
               if (!in_array($dicInternInformation, ['NONE', 'ERROR'])) {
                    foreach( $dicInternInformation as $key ) {
                         $firstRecord[$key] = $dicInternInformation[$key];
                    }
               }
          }
          return [$firstRecord, $dicExtInfo];
     }
     function getSchedulInfos() {
          $dicSchedul = [];
          $dicExtInfo = [];
          if (($this->singleSchedul->ID > 0)) {
               $dicExtInfo = ['sep_info' => ['1' => $this->singleSchedul->ID], 'Modul' => $this->MN['Partner_Schedul']];
               $dicSchedul = $this->singleSchedul->firstRecord;
               foreach( $this->singleAddress->firstRecord->keys() as $key ) {
                    $dicSchedul['address_' . $key] = $this->singleAddress->firstRecord[$key];
               }
               foreach( $this->singlePartner->firstRecord->keys() as $key ) {
                    $dicSchedul['partner_' . $key] = $this->singlePartner->firstRecord[$key];
               }
               $dicInternInformation = $this->rpc->callRP('Database.getInternInformation', $this->dicUser, $dicSchedul['schedul_staff_id']);
               if (!in_array($dicInternInformation, ['NONE', 'ERROR'])) {
                    foreach( $dicInternInformation as $key ) {
                         $dicSchedul[$key] = $dicInternInformation[$key];
                    }
               }
               $dicNotes = $this->rpc->callRP('Address.getNotes', $this->singleAddress->ID, $this->dicUser);
               if ($dicNotes && !in_array($dicNotes, ['NONE', 'ERROR'])) {
                    foreach( $dicNotes as $key ) {
                         $dicSchedul['notes_' . $key] = $dicNotes[$key];
                    }
               }
               $dicSchedul['schedul_time_begin'] = $this->getTimeString($dicSchedul['schedul_time_begin']);
               $dicSchedul = $this->addDateTime($dicSchedul);
          }
          return [$dicSchedul, $dicExtInfo];
     }
     function on_tree1_row_activated($event,$data1,$data2) {
          $this->elog('DoubleClick tv_address');
          $this->activateClick('chooseAddress', $event);
     }
     function on_chooseAddress_activate($event) {
          if (($this->tabOption == $this->tabAddress)) {
               $this->elog(['############### Address choose ID ###################', $this->singleAddress->ID], true);
               $this->setChooseValue($this->singleAddress->ID);
               $this->closeWindow();
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->elog(['############### Address choose ID ###################', $this->singlePartner->ID], true);
               $this->setChooseValue($this->singlePartner->ID);
               $this->closeWindow();
          }
     }
     function on_set_ready1_activate($event) {
          $this->activateClick('SchedulEdit1');
          $sp = $this->getWidget('eSchedulProcess');
          $sp->set_text('999');
          $this->activateClick('schedulSave');
     }
     function on_bSearch_clicked($event) {
          $this->findAddress();
     }
     function on_eSearch_key12_press_event($entry,$event) {
          $this->elog('eSearch_key_press_event');
          if ($this->checkKey($event, 'NONE', 'Return')) {
               $this->findAddress();
          }
     }
     function on_bClearSearch_clicked($event) {
          foreach( $this->liSearchWidgets as $sName ) {
               $this->getWidget($sName)->set_text('');
          }
     }
     function findAddress() {
          $this->elog('findAddress');
          $this->elog('Searching ....', $this->ERROR);
          $sName = $this->getWidget('eFindName')->get_text();
          $sName2 = $this->getWidget('eFindName2')->get_text();
          $sCity = $this->getWidget('eFindCity')->get_text();
          $sZip = $this->getWidget('eFindZipcode')->get_text();
          $sFirstname = $this->getWidget('eFindFirstname')->get_text();
          $sID = $this->getWidget('eFindID')->get_text();
          $sStreet = $this->getWidget('eFindStreet')->get_text();
          $sPhone = $this->getWidget('eFindPhone')->get_text();
          $sFax = $this->getWidget('eFindFax')->get_text();
          $sNewsletter = $this->getWidget('eFindNewsletter')->get_text();
          $sInfo = $this->getWidget('eFindInfo')->get_text();
          $sEmail = $this->getWidget('eFindEmail')->get_text();
          $liSearch = [];
          if ($sName) {
               $liSearch[] = 'lastname';
               $liSearch[] = $sName;
          }
          if ($sName2) {
               $liSearch[] = 'lastname2';
               $liSearch[] = $sName2;
          }
          if ($sID) {
               $newID = '';
               $newOp = '';
               try {
                    $sID = $sID->strip();
                    foreach( $sID as $c1 ) {
                         if ($c1->isdigit()) {
                              $newID += $c1;
                         }
                         else {
                              $newOp += $c1;
                         }
                    }
                    if (!($newOp)) {
                         $newOp = '=';
                    }
                    $liSearch[] = 'id ' . $newOp;
                    $liSearch[] = pyjslib_int($newID);
               }
               catch(Exception $e) {
                    $liSearch[] = 'id =';
                    $liSearch[] = 0;
               }
               $this->elog(['liSearch = ', $liSearch], true);
          }
          if ($sFirstname) {
               $liSearch[] = 'firstname';
               $liSearch[] = $sFirstname;
          }
          if ($sCity) {
               $liSearch[] = 'city';
               $liSearch[] = $sCity;
          }
          if ($sZip) {
               $liSearch[] = 'zip';
               $liSearch[] = $sZip;
          }
          if ($sStreet) {
               $liSearch[] = 'street';
               $liSearch[] = $sStreet;
          }
          if ($sPhone) {
               $liSearch[] = 'phone';
               $liSearch[] = $sPhone;
          }
          if ($sFax) {
               $liSearch[] = 'fax';
               $liSearch[] = $sFax;
          }
          if ($sNewsletter) {
               $liSearch[] = 'newsletter';
               $liSearch[] = $sNewsletter;
          }
          if ($sInfo) {
               $liSearch[] = 'status_info';
               $liSearch[] = $sInfo;
          }
          if ($sEmail) {
               $liSearch[] = 'email';
               $liSearch[] = $sEmail;
          }
          if ($liSearch) {
               if ($this->cbSearchPartner->get_active()) {
                    $this->singleAddress->sWhere = $this->rpc->callRP('Address.getAllAddressForThisPartner', $this->getWhere($liSearch), $this->dicUser);
               }
               else {
                    $this->singleAddress->sWhere = $this->getWhere($liSearch);
               }
               if ($this->cbSearchFashion && ($this->cbSearchFashion->get_active() > 0)) {
                    $this->singleAddress->sWhere .= ' and fct_getAddressFashion(' . pyjslib_repr($this->cbSearchFashion->get_active()) . ', address.id ) ';
               }
          }
          else {
               if ($this->cbSearchFashion && ($this->cbSearchFashion->get_active() > 0)) {
                    $this->singleAddress->sWhere .= ' where fct_getAddressFashion(' . pyjslib_repr($this->cbSearchFashion->get_active()) . ', address.id ) ';
               }
               else {
                    $this->singleAddress->sWhere = ' ';
               }
          }
          $this->oldTab = -1;
          $this->refreshTree();
     }
     function on_bChooseBank_clicked($event) {
          $bank = $cuon->Bank->bank->bankwindow($this->allTables);
          $bank->setChooseEntry('chooseBank', $this->getWidget('eBankID'));
     }
     function on_eBankID_changed($event) {
          $this->elog('eBankID changed');
          $eAdrField = $this->getWidget('tvBank');
          try {
               $liAdr = $this->singleBank->getAddress(long($this->getWidget('eBankID')->get_text()));
               $this->setTextbuffer($eAdrField, $liAdr);
          }
          catch(Exception $e) {
               $this->setTextbuffer($eAdrField, ' ');
               $this->elog([$Exception, $param], true);
          }
     }
     function on_bSearchTOP_clicked($event) {
          //$top = callProc_kwargs_method_call($cuon->PrefsFinance->prefsFinance, 'prefsFinancewindow', [$this->allTables], ["preparedTab" => 1]);
          $top->setChooseEntry('chooseTOP', $this->getWidget('eTOPID'));
     }
     function on_eTOPID_changed($event) {
          $this->elog('eTOPID changed');
          $eTopField = $this->getWidget('tvTOP');
          try {
               $liTop = $this->singlePrefsFinanceTop->getTOP(long($this->getWidget('eTOPID')->get_text()));
               $this->setTextbuffer($eTopField, $liTop);
          }
          catch(Exception $e) {
               $this->setTextbuffer($eTopField, ' ');
               $this->elog([$Exception, $param], true);
          }
     }
     function on_bChooseCaller_clicked($event) {
          $staff = $cuon->Staff->staff->staffwindow($this->allTables);
          $staff->setChooseEntry('chooseStaff', $this->getWidget('eAddressCallerID'));
     }
     function on_bChooseRep_clicked($event) {
          $staff = $cuon->Staff->staff->staffwindow($this->allTables);
          $staff->setChooseEntry('chooseStaff', $this->getWidget('eAddressRepID'));
     }
     function on_bChooseSalesman_clicked($event) {
          $staff = $cuon->Staff->staff->staffwindow($this->allTables);
          $staff->setChooseEntry('chooseStaff', $this->getWidget('eAddressSalesmanID'));
     }
     function on_eAddressCallerID_changed($event) {
          $this->elog('eCallerID changed');
          try {
               $eAdrField = $this->getWidget('eAddressCaller');
               $cAdr = $this->singleStaff->getAddressEntry(long($this->getWidget('eAddressCallerID')->get_text()));
               $eAdrField->set_text($cAdr);
          }
          catch(Exception $e) {
               $eAdrField->set_text('');
          }
     }
     function on_eAddressRepID_changed($event) {
          $this->elog('eRepID changed');
          try {
               $eAdrField = $this->getWidget('eAddressRep');
               $cAdr = $this->singleStaff->getAddressEntry(long($this->getWidget('eAddressRepID')->get_text()));
               $eAdrField->set_text($cAdr);
          }
          catch(Exception $e) {
               $eAdrField->set_text('');
          }
     }
     function on_eAddressSalesmanID_changed($event) {
          $this->elog('eSalesmanID changed');
          try {
               $eAdrField = $this->getWidget('eAddressSalesman');
               $cAdr = $this->singleStaff->getAddressEntry(long($this->getWidget('eAddressSalesmanID')->get_text()));
               $eAdrField->set_text($cAdr);
          }
          catch(Exception $e) {
               $eAdrField->set_text('');
          }
     }
     function on_bSchedulFor_clicked($event) {
          $staff = $cuon->Staff->staff->staffwindow($this->allTables);
          $staff->setChooseEntry('chooseStaff', $this->getWidget('eSchedulFor'));
     }
     function on_newEnquiry_activate($event) {
          $this->elog('new enquiry');
          $dicOrder = [];
          $dicOrder['addressnumber'] = $this->singleAddress->ID;
          $dicOrder['ModulNumber'] = $this->ModulNumber;
          $dicDate = $this->getActualDateTime();
          $dicOrder['orderedat'] = $dicDate['date'];
          $dicOrder['deliveredat'] = $dicDate['date'];
          $dicOrder['process_status'] = $this->OrderStatus['EnquiryStart'];
          $enquirywindow = $cuon->Enquiry->enquiry->enquirywindow($this->allTables, $dicOrder, true);
     }
     function on_new_proposal_activate($event) {
          $this->elog('new proposal');
          $dicOrder = [];
          $dicOrder['addressnumber'] = $this->singleAddress->ID;
          $dicOrder['ModulNumber'] = $this->ModulNumber;
          $dicDate = $this->getActualDateTime();
          $dicOrder['orderedat'] = $dicDate['date'];
          $dicOrder['deliveredat'] = $dicDate['date'];
          $dicOrder['process_status'] = $this->OrderStatus['ProposalStart'];
          $proposalwindow = $cuon->Proposal->proposal->proposalwindow($this->allTables, $dicOrder, true);
     }
     function on_new_order_activate($event) {
          $this->elog('new order');
          $dicOrder = [];
          $dicOrder['addressnumber'] = $this->singleAddress->ID;
          $dicOrder['ModulNumber'] = $this->ModulNumber;
          $dicDate = $this->getActualDateTime();
          $dicOrder['orderedat'] = $dicDate['date'];
          $dicOrder['deliveredat'] = $dicDate['date'];
          $orderwindow = $cuon->Order->order->orderwindow($this->allTables, $dicOrder, true);
     }
     function on_new_project_activate($event) {
          $this->elog('new project');
          $dicProject = [];
          $dicProject['addressid'] = $this->singleAddress->ID;
          $dicProject['ModulNumber'] = $this->ModulNumber;
          $projectwindow = $cuon->Project->project->projectwindow($this->allTables, $dicProject, true);
     }
     function on_tbNewEnquiry_clicked($event) {
          $this->elog('new TB enquiry');
          $this->activateClick('newEnquiry');
     }
     function on_tbNewProposal_clicked($event) {
          $this->elog('new TB proposal');
          $this->activateClick('new_proposal');
     }
     function on_tbNewOrder_clicked($event) {
          $this->elog('new order toolbar ');
          $this->activateClick('new_order');
     }
     function on_tbNewProject_clicked($event) {
          $this->elog('new order toolbar ');
          $this->activateClick('new_project');
     }
     function on_bNotesCut_clicked($event) {
          $this->activateClick('notes_cut1');
     }
     function on_bNotesCopy_clicked($event) {
          $this->activateClick('notes_copy1');
     }
     function on_bNotesPaste_clicked($event) {
          $this->activateClick('notes_paste1');
     }
     function on_bNotesUndo_clicked($event) {
          $this->activateClick('notes_undo1');
     }
     function on_bNotesRedo_clicked($event) {
          $this->activateClick('notes_redo1');
     }
     function disconnectSchedulTree() {
          try {
               $this->getWidget('treeScheduls')->get_selection()->disconnect($this->connectSchedulTreeId);
          }
          catch(Exception $e) {
          }
     }
     function connectSchedulTree() {
          try {
               $this->connectSchedulTreeId = $this->getWidget('treeScheduls')->get_selection()->connect('changed', $this->SchedulTree_select_callback);
          }
          catch(Exception $e) {
          }
     }
     function SchedulTree_select_callback($treeSelection) {
          list($listStore, $iter) = $treeSelection->get_selected();
          $this->elog([$listStore, $iter], true);
          if ($listStore && (count($listStore) > 0)) {
               $row = $listStore[0];
          }
          else {
               $row = -1;
          }
          if (($iter != null)) {
               $sNewId = $listStore->get_value($iter, 0);
               $this->elog($sNewId);
               try {
                    $newID = pyjslib_int(array_slice($sNewId, ($sNewId->find('###') + 3), null));
               }
               catch(Exception $e) {
               }
          }
     }
     function on_eSchedulFor_changed($event) {
          $this->elog('eSchedulfor changed');
          try {
               $eAdrField = $this->getWidget('eSchedulForName');
               $cAdr = $this->singleStaff->getAddressEntry(long($this->getWidget('eSchedulFor')->get_text()));
               $eAdrField->set_text($cAdr);
               $ts = $this->getWidget('treeScheduls');
               $this->elog(['ts = ', $ts], true);
               $treestore = $gtk->TreeStore($object);
               $treestore = $gtk->TreeStore($str);
               $ts->set_model($treestore);
               list($liDates, $newHash) = $this->rpc->callRP('Address.getAllActiveSchedul', $this->dicUser, 'Schedul', $this->getWidget('eSchedulFor')->get_text());
               $this->elog(['Schedul by schedul_date: ', $liDates, $newHash], true);
               if ($liDates && !in_array($liDates, ['NONE', 'ERROR'])) {
                    $lastRep = null;
                    $lastSalesman = null;
                    $Schedulname = null;
                    $lastSchedulname = null;
                    $iter2 = null;
                    $iter3 = null;
                    $liDates->reverse();
                    foreach( $liDates as $oneDate ) {
                         try {
                              $Schedulname = $oneDate['date'];
                              if (($lastSchedulname != $Schedulname)) {
                                   $lastSchedulname = $Schedulname;
                                   //$iter = $treestore[] = null, [$lastSchedulname];
                              }
                              $sTime = $this->getTimeString($oneDate['time_begin']);
                              $sTimeEnd = $this->getTimeString($oneDate['time_end']);
                              $iter2 = $treestore->insert_before($iter, null, [$oneDate['a_zip'] . ' ' . $oneDate['a_city'] . ', ' . $oneDate['a_lastname'] . ', ' . $sTime . ' - ' . $sTimeEnd . ' ###' . pyjslib_repr($oneDate['id'])]);
                         }
                         catch(Exception $e) {
                              $this->elog([$Exception, $params], true);
                         }
                    }
               }
               $ts->show();
               $this->connectSchedulTree();
          }
          catch(Exception $e) {
               $this->elog([$Exception, $params], true);
          }
     }
     function on_bAddNameMisc_clicked($event) {
          $this->addName2Note($this->viewMisc->get_buffer());
     }
     function on_bAddNameContacter_clicked($event) {
          $this->addName2Note($this->viewContact->get_buffer());
     }
     function on_bAddNameRep_clicked($event) {
          $this->addName2Note($this->viewRep->get_buffer());
     }
     function on_bAddNameSalesman_clicked($event) {
          $this->addName2Note($this->viewSalesman->get_buffer());
     }
     function on_bAddNameOrganisation_clicked($event) {
          $this->addName2Note($this->viewOrganisation->get_buffer());
     }
     function on_bAddFormular2NotesMisc_clicked($event) {
          $this->elog('AddFormular2NoticesMisc clicked');
          $this->addForm2Note('cbeNotesMisc', $this->viewMisc->get_buffer());
     }
     function on_bAddFormular2NotesContacter_clicked($event) {
          $this->elog('AddFormular2NoticesContacter clicked');
          $this->addForm2Note('cbeNotesContacter', $this->textbufferContact);
     }
     function on_bAddFormular2NotesRep_clicked($event) {
          $this->elog('AddFormular2NoticesRep clicked');
          $this->addForm2Note('cbeNotesRep', $this->textbufferRep);
     }
     function on_bAddFormular2NotesSalesman_clicked($event) {
          $this->elog('AddFormular2NoticesSalesman clicked');
          $this->addForm2Note('cbeNotesSalesman', $this->textbufferSalesman);
     }
     function on_bAddFormular2NotesOrganisation_clicked($event) {
          $this->elog('AddFormular2NoticesSalesman clicked');
          $this->addForm2Note('cbeNotesOrganisation', $this->textbufferOrganisation);
     }
     function on_bViewHomepage_clicked($event) {
          $sUrl = $this->singleAddress->firstRecord['homepage_url'];
          if (!in_array(array_slice($sUrl, 0, 3 - 0), ['htt', 'HTT', 'FTP', 'ftp'])) {
               $sUrl = 'http://' . $sUrl;
          }
          $this->startExternalPrg($this->dicUser['prefDMS']['exe']['internet'], $sUrl);
     }
     function on_bWebSearch_clicked($event) {
          $sSearch = '/search?q=' . $this->singleAddress->firstRecord['lastname'];
          $sSearch .= '+' . $this->singleAddress->firstRecord['city'];
          $sSearch .= '&ie=utf-8&oe=utf-8';
          $sUrl = 'http://google.de';
          if (!in_array(array_slice($sUrl, 0, 3 - 0), ['htt', 'HTT', 'FTP', 'ftp'])) {
               $sUrl = 'http://' . $sUrl;
          }
          $sUrl += $sSearch;
          $this->startExternalPrg($this->dicUser['prefDMS']['exe']['internet'], $sUrl);
     }
     function on_bSendMail_clicked($event) {
          $dicV = [];
          $dicV['From'] = $this->dicUser['Email']['From'];
          $dicV['To'] = $this->singleAddress->getEmail();
          $dicV['sm'] = $this->singleAddress->getFirstRecord();
          $dicV['Signatur'] = '

-- 
' . $this->dicUser['Email']['Signatur'];
          $this->elog($dicV);
          $em = $cuon->E_Mail->sendEmail->sendEmail($dicV);
     }
     function on_bSendExternEmail_clicked($event) {
          $Emailprg = $this->dicUser['Email']['extPrg'];
          $this->setClipboard($this->singleAddress->getEmail());
          $liEmail = Emailprg::split(' ');
          $s = 'self.startExternalPrg(liEmail[0],';
          if ((count($liEmail) > 1)) {
               foreach( pyjslib_range(1, count($liEmail)) as $i ) {
                    if ($liEmail[$i] && !in_array($liEmail[$i], [' '])) {
                         $s .= '\'' . $liEmail[$i] . '\', ';
                    }
                    $this->elog($s);
               }
          }
          $s .= '\'' . $this->singleAddress->getEmail() . '\')';
          $this->elog($s);
          eval($s);
     }
     function on_bSendPartnerEmail_clicked($event) {
          $dicV = [];
          $dicV['From'] = $this->dicUser['Email']['From'];
          $dicV['To'] = $this->singlePartner->getEmail();
          $dicV['Signatur'] = '

-- 
' . $this->dicUser['Email']['Signatur'];
          $this->elog($dicV);
          $em = $cuon->E_Mail->sendEmail->sendEmail($dicV);
     }
     function addForm2Note($sInput,$sOutput) {
          $s = $this->getActiveText($this->getWidget($sInput));
          $this->elog(['ActiveText', $s], true);
          if ($s) {
               $iNr = 0;
               try {
                    $iFind = $s->find('###');
                    $iNr = pyjslib_int(array_slice($s, ($iFind + 3), null));
               }
               catch(Exception $e) {
                    $this->elog([$Exception, $param], true);
               }
               if ($iNr) {
                    $Formular = $this->rpc->callRP('Misc.getForm', $iNr, $this->dicUser);
                    $this->elog($Formular);
                    if ($Formular && !in_array($Formular, ['NONE', 'ERROR'])) {
                         $newForm = $this->doUncompress($this->doDecode($Formular[0]['document_image']));
                         $this->elog(['newForm', $newForm], true);
                         $this->add2Textbuffer($sOutput, $newForm, 'Head');
                    }
               }
          }
     }
     function addName2Note($sWidget) {
          $t1 = $this->rpc->callRP('User.getDate', $this->dicUser);
          $t2 = $this->rpc->callRP('User.getStaffAddressString', $this->dicUser);
          $text = $t1 . ' : ' . $t2 . '

';
          $this->elog(['addtoNote text = ', $text], true);
          $this->add2Textbuffer($sWidget, $text, 'Head');
     }
     function saveData() {
          $this->elog('save Addresses');
          if (($this->doEdit == $this->tabAddress)) {
               $this->elog('save 1');
               $this->on_save1_activate(null);
          }
          else if (($this->doEdit == $this->tabBank)) {
               $this->elog('save 2');
               $this->on_bank_save1_activate(null);
          }
          else if (($this->doEdit == $this->tabMisc)) {
               $this->on_MiscSave1_activate(null);
          }
          else if (($this->doEdit == $this->tabPartner)) {
               $this->on_PartnerSave1_activate(null);
          }
          else if (($this->doEdit == $this->tabSchedul)) {
               $this->on_SchedulSave_activate(null);
          }
          else if (($this->doEdit == $this->tabNotes)) {
               $this->on_NotesSave_activate(null);
          }
     }
     function fillComboboxForms($sName,$liCBE) {
          $widget = $this->getWidget($sName);
          if ($widget) {
               $this->elog($sName);
          }
          $this->elog($liCBE);
          if ($liCBE && !in_array($liCBE, ['NONE', 'ERROR'])) {
               foreach( $liCBE as $sColumn ) {
                    $this->elog($sColumn);
                    $widget->append_text($sColumn);
               }
               $model = $widget->get_model();
               $this->elog($model);
               $this->elog(pyjslib_repr($model));
               $widget->show();
               $widget->set_active(0);
          }
     }
     function on_tbSave_clicked($event) {
          $this->elog('save Addresses');
          if (($this->tabOption == $this->tabAddress)) {
               $this->elog('save 1');
               $this->on_save1_activate(null);
          }
          else if (($this->tabOption == $this->tabBank)) {
               $this->elog('save 2');
               $this->on_bank_save1_activate(null);
          }
          else if (($this->tabOption == $this->tabMisc)) {
               $this->on_MiscSave1_activate(null);
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->on_PartnerSave1_activate(null);
          }
          else if (($this->tabOption == $this->tabSchedul)) {
               $this->on_SchedulSave_activate(null);
          }
          else if (($this->tabOption == $this->tabNotes)) {
               $this->on_NotesSave_activate(null);
          }
     }
     function on_tbNew_clicked($event) {
          $this->elog('new Addresses');
          if (($this->tabOption == $this->tabAddress)) {
               $this->on_new1_activate(null);
          }
          else if (($this->tabOption == $this->tabBank)) {
               $this->on_bank_new1_activate(null);
          }
          else if (($this->tabOption == $this->tabMisc)) {
               $this->on_MiscNew1_activate(null);
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->on_PartnerNew1_activate(null);
          }
          else if (($this->tabOption == $this->tabSchedul)) {
               $this->on_SchedulNew_activate(null);
          }
          else if (($this->tabOption == $this->tabNotes)) {
               $this->on_NotesNew_activate(null);
          }
     }
     function on_tbEdit_clicked($event) {
          $this->elog('edit Addresses');
          if (($this->tabOption == $this->tabAddress)) {
               $this->on_edit1_activate(null);
          }
          else if (($this->tabOption == $this->tabBank)) {
               $this->on_bank_edit1_activate(null);
          }
          else if (($this->tabOption == $this->tabMisc)) {
               $this->on_MiscEdit1_activate(null);
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->on_PartnerEdit1_activate(null);
          }
          else if (($this->tabOption == $this->tabSchedul)) {
               $this->on_SchedulEdit1_activate(null);
          }
          else if (($this->tabOption == $this->tabNotes)) {
               $this->on_NotesEdit1_activate(null);
          }
     }
     function on_tbExtendetInfo_clicked($event) {
          if (($this->tabOption == $this->tabAddress)) {
               $this->on_bShowDMS_clicked(null);
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->on_bShowPartnerDMS_clicked(null);
          }
     }
     function on_tbLetter_clicked($event) {
          if (($this->tabOption == $this->tabAddress)) {
               $this->on_bLetter_clicked(null);
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->on_bPartnerLetter_clicked(null);
          }
          else if (($this->tabOption == $this->tabSchedul)) {
               $this->on_bSchedulLetter_clicked(null);
          }
     }
     function on_tbAllContact_clicked($event) {
          $con1 = $contact->contactwindow($this->allTables, 0, 0);
     }
     function on_tbContact_clicked($event) {
          $this->on_bContact_clicked(null);
     }
     function disconnectProposalTree() {
          try {
               $this->getWidget('tvAddressProposal')->get_selection()->disconnect($this->connectProposalTreeId);
          }
          catch(Exception $e) {
          }
     }
     function connectProposalTree() {
          try {
               $this->connectProposalTreeId = $this->getWidget('tvAddressProposal')->get_selection()->connect('changed', $this->ProposalTree_select_callback);
          }
          catch(Exception $e) {
          }
     }
     function ProposalTree_select_callback($treeSelection) {
          list($listStore, $iter) = $treeSelection->get_selected();
          $this->ProposalID = 0;
          $this->elog([$listStore, $iter], true);
          if ($listStore && (count($listStore) > 0)) {
               $row = $listStore[0];
          }
          else {
               $row = -1;
          }
          if (($iter != null)) {
               $sNewId = $listStore->get_value($iter, 0);
               $this->elog($sNewId);
               try {
                    $this->ProposalID = pyjslib_int(array_slice($sNewId, ($sNewId->find('###') + 3), null));
               }
               catch(Exception $e) {
               }
          }
     }
     function on_bShowProposal_clicked($event) {
          $this->on_tvAddressProposal_row_activated($event, null, null);
     }
     function on_tvAddressProposal_row_activated($event,$data1,$data2) {
          if ($this->ProposalID) {
               $proposalwindow = $cuon->Proposal->proposal->proposalwindow($this->allTables, null, false, $this->ProposalID);
          }
     }
     function setProposalValues() {
          $liGroup = $this->rpc->callRP('Order.getOrderForAddress', $this->singleAddress->ID, $this->dicUser, $this->OrderStatus['ProposalStart'], $this->OrderStatus['ProposalEnd']);
          if ($liGroup && !in_array($liGroup, ['NONE', 'ERROR'])) {
               $this->treeOrder->fillTree($this->getWidget('tvAddressProposal'), $liGroup, ['number', 'designation', 'orderedat'], 'self.connectProposalTree()');
               $this->connectProposalTree();
          }
          else {
               $this->treeProposal->fillTree($this->getWidget('tvAddressProposal'), [], ['number', 'designation', 'orderedat'], 'self.connectProposalTree()');
               $this->connectProposalTree();
          }
     }
     function disconnectOrderTree() {
          try {
               $this->getWidget('tvAddressOrder')->get_selection()->disconnect($this->connectOrderTreeId);
          }
          catch(Exception $e) {
          }
     }
     function connectOrderTree() {
          try {
               $this->connectOrderTreeId = $this->getWidget('tvAddressOrder')->get_selection()->connect('changed', $this->OrderTree_select_callback);
          }
          catch(Exception $e) {
          }
     }
     function OrderTree_select_callback($treeSelection) {
          list($listStore, $iter) = $treeSelection->get_selected();
          $this->OrderID = 0;
          $this->elog([$listStore, $iter], true);
          if ($listStore && (count($listStore) > 0)) {
               $row = $listStore[0];
          }
          else {
               $row = -1;
          }
          if (($iter != null)) {
               $sNewId = $listStore->get_value($iter, 0);
               $this->elog($sNewId);
               try {
                    $this->OrderID = pyjslib_int(array_slice($sNewId, ($sNewId->find('###') + 3), null));
               }
               catch(Exception $e) {
               }
          }
     }
     function on_tvAddressOrder_row_activated($event,$data1,$data2) {
          if ($this->OrderID) {
               $orderwindow = $cuon->Order->order->orderwindow($this->allTables, null, false, $this->OrderID);
          }
     }
     function setOrderValues() {
          $liGroup = $this->rpc->callRP('Order.getOrderForAddress', $this->singleAddress->ID, $this->dicUser);
          if ($liGroup && !in_array($liGroup, ['NONE', 'ERROR'])) {
               $this->treeOrder->fillTree($this->getWidget('tvAddressOrder'), $liGroup, ['number', 'designation', 'orderedat'], 'self.connectOrderTree()');
               $this->connectOrderTree();
          }
          else {
               $this->treeOrder->fillTree($this->getWidget('tvAddressOrder'), [], ['number', 'designation', 'orderedat'], 'self.connectOrderTree()');
               $this->connectOrderTree();
          }
     }
     function disconnectInvoiceTree() {
          try {
               $this->getWidget('tvAddressInvoices')->get_selection()->disconnect($this->connectInvoicesTreeId);
          }
          catch(Exception $e) {
          }
     }
     function connectInvoiceTree() {
          try {
               $this->connectInvoicesTreeId = $this->getWidget('tvAddressInvoices')->get_selection()->connect('changed', $this->InvoiceTree_select_callback);
          }
          catch(Exception $e) {
          }
     }
     function InvoiceTree_select_callback($treeSelection) {
          list($listStore, $iter) = $treeSelection->get_selected();
          $this->InvoiceID = 0;
          $this->elog([$listStore, $iter], true);
          if ($listStore && (count($listStore) > 0)) {
               $row = $listStore[0];
          }
          else {
               $row = -1;
          }
          if (($iter != null)) {
               $sNewId = $listStore->get_value($iter, 0);
               $this->elog($sNewId);
               try {
                    $this->InvoiceID = pyjslib_int(array_slice($sNewId, ($sNewId->find('###') + 3), null));
               }
               catch(Exception $e) {
               }
          }
     }
     function on_tvAddressInvoices_row_activated($event,$data1,$data2) {
          if ($this->InvoiceID) {
               $invoicewindow = $cuon->Finances->invoicebook->invoicebookwindow($this->allTables, null, false, $this->InvoiceID);
          }
     }
     function setInvoiceValues() {
          $liGroup = $this->rpc->callRP('Order.getInvoicesForAddress', $this->singleAddress->ID, $this->dicUser);
          if ($liGroup && !in_array($liGroup, ['NONE', 'ERROR'])) {
               $this->treeInvoice->fillTree($this->getWidget('tvAddressInvoices'), $liGroup, ['number', 'designation', 'date'], 'self.connectInvoiceTree()');
               $this->connectInvoiceTree();
          }
          else {
               $this->treeInvoice->fillTree($this->getWidget('tvAddressInvoices'), [], ['number', 'designation', 'date'], 'self.connectInvoiceTree()');
               $this->connectInvoiceTree();
          }
     }
     function disconnectProjectTree() {
          try {
               $this->getWidget('tvAddressProject')->get_selection()->disconnect($this->connectProjectTreeId);
          }
          catch(Exception $e) {
          }
     }
     function connectProjectTree() {
          try {
               $this->connectProjectTreeId = $this->getWidget('tvAddressProject')->get_selection()->connect('changed', $this->ProjectTree_select_callback);
          }
          catch(Exception $e) {
          }
     }
     function ProjectTree_select_callback($treeSelection) {
          list($listStore, $iter) = $treeSelection->get_selected();
          $this->ProjectID = 0;
          $this->elog([$listStore, $iter], true);
          if ($listStore && (count($listStore) > 0)) {
               $row = $listStore[0];
          }
          else {
               $row = -1;
          }
          if (($iter != null)) {
               $sNewId = $listStore->get_value($iter, 0);
               $this->elog($sNewId);
               try {
                    $this->ProjectID = pyjslib_int(array_slice($sNewId, ($sNewId->find('###') + 3), null));
               }
               catch(Exception $e) {
               }
          }
     }
     function on_tvAddressProject_row_activated($event,$data1,$data2) {
          if ($this->ProjectID) {
               $Projectwindow = $cuon->Project->project->projectwindow($this->allTables, null, false, $this->ProjectID);
          }
     }
    
     function setProjectValues() {
          $liGroup = $this->rpc->callRP('Projects.getProjectsForAddress', $this->singleAddress->ID, $this->dicUser);
          if ($liGroup && !in_array($liGroup, ['NONE', 'ERROR'])) {
               $this->treeProjects->fillTree($this->getWidget('tvAddressProject'), $liGroup, ['name', 'designation', 'date'], 'self.connectProjectTree()');
               $this->connectProjectTree();
          }
          else {
               $this->treeProjects->fillTree($this->getWidget('tvAddressProject'), [], ['name', 'designation', 'date'], 'self.connectProjectTree()');
               $this->connectProjectTree();
          }
     }
     function on_tbGrave_clicked($event) {
          $this->elog('tbGrave clicked');
          //$gaves = callProc_kwargs_method_call($cuon->Graves->grave, 'graveswindow', [$this->allTables], ["addressid" => $this->singleAddress->ID,"newGrave" => true]);
     }
     function on_tbHibernation_clicked($event) {
          $this->elog('tbHibernation clicked');
          //$hib = callProc_kwargs_method_call($cuon->Garden->hibernation, 'hibernationwindow', [$this->allTables], ["addressid" => $this->singleAddress->ID,"newHibernation" => true]);
     }
     function fetchMainwindowKeys($sKey) {
          if (in_array($this->tabOption, [$this->tabAddress, $this->tabPartner])) {
               if (in_array($this->ControlKey1, $this->ControlKeysSuper)) {
                    if (($sKey == '1')) {
                         $this->elog('activate 1');
                         $this->getWidget('eFindName')->grab_focus();
                         $this->getWidget('eFindName')->set_text('');
                    }
                    else if (($sKey == '2')) {
                         $this->elog('activate 2');
                         $this->getWidget('eFindName2')->grab_focus();
                         $this->getWidget('eFindName2')->set_text('');
                    }
               }
               else {
                    if (($sKey == 'Return')) {
                         $ok = false;
                         foreach( $this->liSearchWidgets as $sName ) {
                              if ($this->getWidget($sName)->has_focus()) {
                                   $ok = true;
                                   break;
                              }
                         }
                         if ($ok) {
                              $this->findAddress();
                         }
                    }
               }
          }
     }
     function refreshTree() {
          $this->singleAddress->disconnectTree();
          $this->singlePartner->disconnectTree();
          $this->singleSchedul->disconnectTree();
          $this->singleBank->disconnectTree();
          if (($this->tabOption == $this->tabAddress)) {
               $this->singleAddress->connectTree();
               if (($this->oldTab < 1)) {
                    $this->singleAddress->refreshTree();
               }
               else {
                    $this->singleAddress->refreshTree(true); // set later to false
                 
               }
          }
          else if (($this->tabOption == $this->tabBank)) {
               $this->singleBank->sWhere = 'where address_id = ' . $this->singleAddress->ID ;
               $this->singleBank->connectTree();
               $this->singleBank->refreshTree();
          }
          else if (($this->tabOption == $this->tabMisc)) {
               $this->singleMisc->sWhere = 'where address_id = ' . $this->singleAddress->ID;
               $misc_id = $this->singleMisc->findSingleId();
               if (($misc_id < 0)) {
                    $misc_id = $this->rpc->callRP('Address.createMiscEntry', $this->singleAddress->ID, $this->dicUser);
                    $this->elog(['new misc_id = ', $misc_id], true);
                    $this->singleMisc->load($misc_id);
               }
               $this->singleMisc->fillEntries($misc_id);
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->singlePartner->sWhere = 'where addressid = ' . pyjslib_repr(pyjslib_int($this->singleAddress->ID));
               $this->singlePartner->connectTree();
               $this->singlePartner->refreshTree();
          }
          else if (($this->tabOption == $this->tabSchedul)) {
               $this->singleSchedul->sWhere = 'where partnerid = ' . pyjslib_repr(pyjslib_int($this->singlePartner->ID)) . ' and process_status != 999 ';
               $this->singleSchedul->connectTree();
               $this->singleSchedul->refreshTree();
          }
          else if (($this->tabOption == $this->tabNotes)) {
               $this->singleAddressNotes->sWhere = 'where address_id = ' . pyjslib_repr(pyjslib_int($this->singleAddress->ID));
               $this->singleAddressNotes->fillEntries($this->singleAddressNotes->findSingleId());
               $this->viewMisc->set_buffer($this->singleAddressNotes->NotesMisc);
               $this->viewContact->set_buffer($this->singleAddressNotes->NotesContact);
               $this->viewOrganisation->set_buffer($this->singleAddressNotes->NotesOrganisation);
               $this->viewRep->set_buffer($this->singleAddressNotes->NotesRep);
               $this->viewSalesman->set_buffer($this->singleAddressNotes->NotesSalesman);
               if ($this->InitForms) {
                    try {
                         $liCBE = $this->rpc->callRP('Misc.getFormsAddressNotes', $this->MN['Forms_Address_Notes_Misc'], $this->dicUser);
                         $this->fillComboboxForms('cbeNotesMisc', $liCBE);
                         $liCBE = $this->rpc->callRP('Misc.getFormsAddressNotes', $this->MN['Forms_Address_Notes_Contacter'], $this->dicUser);
                         $this->fillComboboxForms('cbeNotesContacter', $liCBE);
                         $liCBE = $this->rpc->callRP('Misc.getFormsAddressNotes', $this->MN['Forms_Address_Notes_Rep'], $this->dicUser);
                         $this->fillComboboxForms('cbeNotesRep', $liCBE);
                         $liCBE = $this->rpc->callRP('Misc.getFormsAddressNotes', $this->MN['Forms_Address_Notes_Salesman'], $this->dicUser);
                         $this->fillComboboxForms('cbeNotesSalesman', $liCBE);
                    }
                    catch(Exception $e) {
                    }
                    $this->InitForms = false;
               }
          }
          $this->oldTab = $this->tabOption;
     }
     function tabChanged() {
          $this->elog('tab changed to :' . $this->tabOption );
       
       
       
          if (($this->tabOption == $this->tabAddress)) {
               $this->singleAddress->createListbox(Null);
               $this->singleAddress->loadGladeFields("address","AddressMainwindow",$this->tabOption +1) ;
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('address');
               $this->actualEntries = $this->singleAddress->getEntries();
               $this->elog("actual_entries " . var_dump( $this->actualEntries ) );
               $this->editAction = 'editAddress';
               //$this->setStatusbarText(['']);
               $this->NameOfTree = 'tv_address';
               $this->singleAddress->setTreeSensitive(true);
               $this->elog('Seite 0');
          }
          else if (($this->tabOption == $this->tabBank)) {
               $this->elog('Seite 2');
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('bank');
               $this->editAction = 'editBank';
               $this->singleBank->setTreeSensitive(true);
               $this->setStatusbarText([$this->singleAddress->sStatus]);
          }
          else if (($this->tabOption == $this->tabMisc)) {
               $this->elog('Seite 3');
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('misc');
               $this->editAction = 'editMisc';
               $this->setStatusbarText([$this->singleAddress->sStatus]);
          }
          else if (($this->tabOption == $this->tabPartner)) {
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('partner');
               $this->elog('Seite 1');
               $this->editAction = 'editPartner';
               $this->NameOfTree = 'tv_partner';
               $this->setTreeVisible(true);
               $this->setStatusbarText([$this->singleAddress->sStatus]);
          }
          else if (($this->tabOption == $this->tabSchedul)) {
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('schedul');
               $this->elog('Seite 4');
               $this->editAction = 'editSchedul';
               $this->NameOfTree = 'tv_scheduls';
               $this->setTreeVisible(true);
               $this->setStatusbarText([$this->singlePartner->sStatus]);
               $this->getWidget('rbBeginDate')->set_active(true);
          }
          else if (($this->tabOption == $this->tabNotes)) {
               $this->elog('Seite 5');
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('notes');
               $this->editAction = 'editNotes';
               $this->viewMisc->set_editable(false);
               $this->viewContact->set_editable(false);
               $this->viewRep->set_editable(false);
               $this->viewSalesman->set_editable(false);
               $this->viewOrganisation->set_editable(false);
               $this->setStatusbarText([$this->singleAddress->sStatus]);
          }
          else if (($this->tabOption == $this->tabOrder)) {
               $this->elog('Seite 7');
               $this->elog('site 7');
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('order');
               $this->editAction = 'editOrder';
               $this->setStatusbarText([$this->singleAddress->sStatus]);
               $this->setOrderValues();
          }
          else if (($this->tabOption == $this->tabProposal)) {
               $this->elog('Seite 7');
               $this->elog('site 7');
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('order');
               $this->editAction = 'editOrder';
               $this->setStatusbarText([$this->singleAddress->sStatus]);
               $this->setProposalValues();
          }
          else if (($this->tabOption == $this->tabInvoice)) {
               $this->elog('Seite 8');
               $this->disableMenuItem('tabs');
               $this->editAction = 'editInvoice';
               $this->setStatusbarText([$this->singleAddress->sStatus]);
               $this->setInvoiceValues();
          }
          else if (($this->tabOption == $this->tabProject)) {
               $this->elog('Seite 9');
               $this->disableMenuItem('tabs');
               $this->enableMenuItem('project');
               $this->editAction = 'editProject';
               $this->setStatusbarText([$this->singleAddress->sStatus]);
               $this->setProjectValues();
          }
          else if (($this->tabOption == $this->tabHtml)) {
               $this->elog([$this->webview, $this->firstWebkitStart], true);
               if ($this->firstWebkitStart) {
                    $this->firstWebkitStart = false;
               }
               else {
                    try {
                         if ($this->webview) {
                              $this->swMap->remove($this->webview);
                         }
                    }
                    catch(Exception $e) {
                         $this->elog([$Exception, $param], true);
                    }
               }
               $this->webview = $webkit->WebView();
               if ($this->webview) {
                    $this->swMap->add($this->webview);
                    $sUrl1 = 'http://maps.google.de/maps?f=q&hl=de&geocode=&time=&date=&ttype=&q=';
                    $sUrl2 = '&ie=UTF8&t=m';
                    $sUrl = ($sUrl1 + $this->singleAddress->getStreet()) . ',' . $this->singleAddress->getZip() . $sUrl2;
                    $this->elog([$this->singleAddress->getStreet(), $this->singleAddress->getZip()], true);
                    $this->elog($sUrl);
                    $this->webview->open($sUrl);
                    $this->webview->show();
               }
          }
          else if (($this->tabOption == $this->tabGraves)) {
               $this->elog('Site graves');
               $this->disableMenuItem('tabs');
               $this->setTreeVisible(false);
               $this->setStatusbarText([$this->singleAddress->sStatus]);
               $liData = $this->rpc->callRP('Grave.getGravesForAddress', $this->singleAddress->ID, $this->dicUser);
               $this->elog($liData);
               $this->graves->setTree($this->tvGrave);
               $this->graves->fillAddressGraves($liData, $this);
               $this->elog('tabOptions graves end');
          }
          else if (($this->tabOption == $this->tabHibernation)) {
               $this->elog('Site graves');
               $this->disableMenuItem('tabs');
               $this->setTreeVisible(false);
               $this->setStatusbarText([$this->singleAddress->sStatus]);
               $liData = $this->rpc->callRP('Garden.getHibernationForAddress', $this->singleAddress->ID, $this->dicUser);
               $this->elog($liData);
               $this->hibernation->setTree($this->tvHibernation);
               $this->hibernation->fillAddressHibernation($liData, $this);
               $this->elog('tabOptions graves end');
          }
          $this->refreshTree();
          $this->enableMenuItem($this->editAction);
          $this->editEntries = false;
     }



      
}


$address = new addresswindow();



?>

 
</body>
</html>
