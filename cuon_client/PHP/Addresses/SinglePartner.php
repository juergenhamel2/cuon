<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/Databases/SingleData.php');

class SinglePartner extends SingleData {
    function __construct($allTables) {
        SingleData::__construct();
        $this->sNameOfTable = 'partner';
        $this->xmlTableDef = 0;
        $this->loadTable($allTables);
        $this->listHeader['names'] = ['name', 'zip', 'city', 'Street', 'ID'];
        $this->listHeader['size'] = [25, 10, 25, 25, 10];
        $this->elog('number of Columns ');
        $this->elog(count($this->table->Columns));
        $this->addressId = 0;
        $this->statusfields = ['lastname', 'firstname'];
    }
    function getPartnerPhone1($id) {
        $Phone = '';
        assert(($id > 0));
        try {
            $id = long($id);
            $dicRecords = $this->load($id);
        }
        catch(Exception $e) {
                       $this->elog('Exception by getAddressPhone1-1');
            $id = 0;
            $dicRecords = [];
        }
        try {
            if ($dicRecords) {
                $Phone = $dicRecords[0]['phone'];
            }
        }
        catch(Exception $e) {
                       $this->elog('Exception by getAddressPhone1-2');
            $Phone = '';
        }
        return $Phone;
    }
    function readNonWidgetEntries($dicValues) {
       $this->elog('readNonWidgetEntries(self) by SinglePartner');
        $dicValues['addressid'] = [$this->addressId, 'int'];
       $this->elog(['dicValues = ', $dicValues], true);
        return $dicValues;
    }
    function getAddress($id) {
        $dicRecords = $this->load($id);
        $liAddress = [];
        if ($dicRecords) {
            $dicRecord = $dicRecords[0];
            $liAddress[] = $dicRecord['lastname'];
            $liAddress[] = $dicRecord['lastname2'];
            $liAddress[] = $dicRecord['firstname'];
            $liAddress[] = $dicRecord['street'];
            $liAddress[] = $dicRecord['country'] . '-' . $dicRecord['zip'] . ' ' . $dicRecord['city'];
        }
        if (!($liAddress)) {
            $liAddress[] = ' ';
            $liAddress[] = ' ';
            $liAddress[] = ' ';
            $liAddress[] = ' ';
            $liAddress[] = ' ';
        }
        return $liAddress;
    }
    function getAddressFirstlast($id) {
        $dicRecords = $this->load($id);
        $sAdr = ' ';
        if ($dicRecords) {
            try {
                $dicRecord = $dicRecords[0];
                $sAdr = $dicRecord['firstname'] . ' ' . $dicRecord['lastname'];
            }
            catch(Exception $e) {
                            }
        }
        return $sAdr;
    }
    function getAddressID() {
        $id = 0;
        if ($this->firstRecord->has_key('addressid')) {
            $id = $this->firstRecord['addressid'];
        }
        return $id;
    }
    function getMailAddress() {
        $s = null;
        try {
            $s = $this->firstRecord['lastname'] . '
';
            $s .= $this->firstRecord['lastname2'] . '
';
            $s .= $this->firstRecord['firstname'] . '

';
            $s .= $this->firstRecord['street'] . '
';
            $s .= $this->firstRecord['country'] . '-' . $this->firstRecord['zip'] . ' ' . $this->firstRecord['city'];
        }
        catch(Exception $e) {
                    }
        return $s;
    }
    function getEmail() {
        $s = null;
        try {
            $s = $this->firstRecord['email'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
}

