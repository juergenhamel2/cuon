<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/Databases/SingleData.php');

class SingleAddress extends SingleData {
    function __construct($allTables) {
        SingleData::__construct();
        $this->sNameOfTable = 'address';
        //$this->elog("Name of table = ", 
        $this->xmlTableDef = 0;
        $this->loadTable($allTables);
        $this->listHeader['names'] = ['name', 'zip', 'city', 'Street', 'ID'];
        $this->listHeader['size'] = [25, 10, 25, 25, 10];
        $this->statusfields = ['lastname', 'city'];
    }
    function getAddressPhone1($id) {
        try {
            $id = long($id);
            $dicRecords = $this->load($id);
        }
        catch(Exception $e) {
                       $this->elog('Exception by getAddressPhone1-1');
            $id = 0;
            $dicRecords = [];
        }
        $Phone = '';
        try {
            if ($dicRecords) {
                $Phone = $dicRecords[0]['phone'];
            }
        }
        catch(Exception $e) {
                       $this->elog('Exception by getAddressPhone1-2');
            $Phone = '';
        }
        return $Phone;
    }
    function getAddress($id) {
        $liAddress = [];
        if (($id > 0)) {
            try {
                $id = long($id);
                $dicRecords = $this->load($id);
            }
            catch(Exception $e) {
                                $id = 0;
                $dicRecords = [];
            }
            if ($dicRecords && !in_array($dicRecords, ['ERROR', 'NONE'])) {
                $dicRecord = $dicRecords[0];
                try {
                    $liAddress[] = $dicRecord['lastname'];
                    $liAddress[] = $dicRecord['lastname2'];
                    $liAddress[] = $dicRecord['firstname'];
                    $liAddress[] = $dicRecord['street'];
                    $liAddress[] = $dicRecord['country'] . '-' . $dicRecord['zip'] . ' ' . $dicRecord['city'];
                }
                catch(Exception $e) {
                                    }
            }
            if (!($liAddress)) {
                $liAddress[] = ' ';
                $liAddress[] = ' ';
                $liAddress[] = ' ';
                $liAddress[] = ' ';
                $liAddress[] = ' ';
            }
        }
        return $liAddress;
    }
    function getMailAddress() {
        $s = null;
        try {
            $s = $this->firstRecord['lastname'] . '
';
            $s .= $this->firstRecord['lastname2'] . '
';
            $s .= $this->firstRecord['firstname'] . '

';
            $s .= $this->firstRecord['street'] . '
';
            $s .= $this->firstRecord['country'] . '-' . $this->firstRecord['zip'] . ' ' . $this->firstRecord['city'];
        }
        catch(Exception $e) {
                    }
        return $s;
    }
    function getLastname() {
        $s = null;
        try {
            $s = $this->firstRecord['lastname'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
    function getLastname2() {
        $s = null;
        try {
            $s = $this->firstRecord['lastname2'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
    function getFirstname() {
        $s = null;
        try {
            $s = $this->firstRecord['firstname'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
    function getStreet() {
        $s = null;
        try {
            $s = $this->firstRecord['street'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
    function getCity() {
        $s = null;
        try {
            $s = $this->firstRecord['city'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
    function getZip() {
        $s = null;
        try {
            $s = $this->firstRecord['zip'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
    function getCountry() {
        $s = null;
        try {
            $s = $this->firstRecord['country'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
    function getEmail() {
        $s = null;
        try {
            $s = $this->firstRecord['email'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
    function getLetterAddress() {
        $s = null;
        try {
            $s = $this->firstRecord['letter_address'];
        }
        catch(Exception $e) {
                    }
        if (!($s) || ($s == 'NONE')) {
            $s = '';
        }
        return $s;
    }
}

