<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/Databases/SingleData.php');

class SingleSchedul extends SingleData {
    function __construct($allTables) {
        SingleData::__construct();
        $this->sNameOfTable = 'partner_schedul';
        $this->xmlTableDef = 0;
        $this->loadTable($allTables);
        $this->listHeader['names'] = ['name', 'zip', 'city', 'Street', 'ID'];
        $this->listHeader['size'] = [25, 10, 25, 25, 10];
        $this->elog('number of Columns ');
        $this->elog(count($this->table->Columns));
        $this->partnerId = 0;
        $this->liSchedulTime = null;
    }
    function readNonWidgetEntries($dicValues) {
        $dicValues['partnerid'] = [$this->partnerId, 'int'];
        return $dicValues;
    }
    function getPartnerID() {
        $id = 0;
        if ($this->firstRecord->has_key('partnerid')) {
            $id = $this->firstRecord['partnerid'];
        }
        return $id;
    }
    function getShortRemark() {
        $s = null;
        if ($this->firstRecord->has_key('short_remark')) {
            $s = $this->firstRecord['short_remark'];
        }
        return $s;
    }
 
    function getNotes() {
        $s = null;
        if ($this->firstRecord->has_key('notes')) {
            $s = $this->firstRecord['notes'];
        }
        return $s;
    }
    function fillExtraEntries($oneRecord) {
        if ($oneRecord->has_key('schedul_datetime')) {
           $this->elog('-----------------------------------------------------');
           $this->elog(['Schedul-Time: ', $oneRecord['schedul_datetime']], true);
            $liDate = string::split($oneRecord['schedul_datetime']);
            if ($liDate) {
                try {
                    assert((count($liDate) == 2));
                    $eDate = $this->getWidget('eDate');
                    $eTime = $this->getWidget('eTime');
                    $eDate->set_text($liDate[0]);
                    $eTime->set_text($liDate[1]);
                }
                catch(Exception $e) {
                                       $this->elog('error in Date');
                }
            }
        }
        else {
           $this->elog(pyjslib_repr($oneRecord));
        }
    }
}

