<?php

require_once($_SERVER['DOCUMENT_ROOT'].'/Databases/SingleData.php');

class SingleBank extends SingleData {
    function __construct($allTables) {
        SingleData::__construct();
        $this->sNameOfTable = 'address_bank';
        $this->xmlTableDef = 0;
        $this->loadTable($allTables);
        $this->listHeader['names'] = [];
        $this->listHeader['size'] = [];
        $this->elog('number of Columns ');
        $this->elog(count($this->table->Columns));
        $this->addressId = 0;
    }
    function readNonWidgetEntries($dicValues) {
       $this->elog('readNonWidgetEntries(self) by SingleBank');
        $dicValues['address_id'] = [$this->addressId, 'int'];
        return $dicValues;
    }
}

