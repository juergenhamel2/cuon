<?php
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . DIRECTORY_SEPARATOR . 'libpy2php');
require_once('libpy2php.php');
require_once( 'os_path.php');
require_once( 'cuon_TypeDefs.php');
/**
 * @author: Juergen Hamel
 * @organization: Cyrus-Computer GmbH, D-32584 Loehne
 * @copyright: by Juergen Hamel
 * @license: GPL ( GNU GENERAL PUBLIC LICENSE )
 * @contact: jh@cyrus.de
 */
class User extends defaultValues {
    /**
     * Variables:
     * 1. self.userName: Name of the User
     */
    function __construct() {
        defaultValues::__construct();
        $this->userName = 'EMPTY';
        $this->dicTest = [];
        $this->sessionID = 0;
        $this->Database = 'cuon';
        $this->userLocales = 'de';
        $this->userEncoding = 'utf-8';
        $this->userShowNews = true;
        $this->Encode = true;
        $this->userPdfEncoding = 'latin-1';
        $this->userType = 'cuon';
        $this->userDateFormatString = '%d.%m.%Y';
        $this->userDateTimeFormatString = '%d.%m.%Y %H:%M';
        $this->userDateTimeFormatEncoding = '%Y.%m.%d %H:%M:%S';
        $this->userTimeFormatString = '%H:%M:S';
        $this->sDebug = 'NO';
        $this->XMLRPC_TRY = 0;
        $this->serverAddress = null;
        $this->userSQLDateFormat = 'DD.MM.YYYY';
        $this->userSQLTimeFormat = 'HH24:MI:SS';
        $this->userSQLDateTimeFormat = 'DD.MM.YYYY HH24:MI';
        $this->userTimeOffset = '+0';
        $this->prefPath = [];
        $this->prefApps = [];
        $this->prefLocale = [];
        $this->Twitter = [];
        $this->serverSqlDateFormat = '%Y-%m-%d';
        $this->client = 0;
        $this->contact_id = 0;
        $this->prefPath['tmp'] = os_path::normpath($this->td->cuon_path) . '/';
        $this->prefPath['pwd'] = os_path::normpath($this->td->cuon_path) . '/work';
        $this->prefPath['templates'] = os_path::normpath($this->td->cuon_path) . '/work/templates';
        $this->prefPath['StandardInvoice1'] = os_path::normpath($this->td->cuon_path . '/' . 'Invoice');
        $this->prefPath['StandardSupply1'] = os_path::normpath($this->td->cuon_path . '/' . 'Delivery');
        $this->prefPath['StandardPickup1'] = os_path::normpath($this->td->cuon_path . '/' . 'Pickup');
        $this->prefPath['AddressLists'] = os_path::normpath($this->td->cuon_path . '/' . 'address');
        $this->prefPath['ArticleLists'] = os_path::normpath($this->td->cuon_path . '/' . 'article');
        $this->prefPath['StandardCAB1'] = os_path::normpath($this->td->cuon_path . '/' . 'address');
        $this->prefPath['ReportStandardInvoice1'] = os_path::normpath($this->td->cuon_path . '/' . 'Reports');
        $this->prefPath['ReportStandardSupply1'] = os_path::normpath($this->td->cuon_path . '/' . 'Reports');
        $this->prefPath['ReportStandardPickup1'] = os_path::normpath($this->td->cuon_path . '/' . 'Reports');
        $this->prefPath['ReportAddressLists'] = os_path::normpath($this->td->cuon_path . '/' . 'Reports');
        $this->prefPath['ReportArticleLists'] = os_path::normpath($this->td->cuon_path . '/' . 'Reports');
        $this->prefPath['ReportStockGoodsLists'] = os_path::normpath($this->td->cuon_path . '/' . 'Reports');
        $this->prefPath['ReportStandardFinancesCAB'] = os_path::normpath($this->td->cuon_path . '/' . 'Reports');
        $this->prefColor = ['FG' => '#000000', 'BG' => '#FFFFFF', 'DUTY_FG' => '#af0000', 'DUTY_BG' => '#5feeec'];
        $this->prefDMS = [];
        $this->prefDMS['scan_device'] = 'plustek:libusb:002:002';
        $this->prefDMS['scan_r'] = ['x' => 1024.0, 'y' => 768.0];
        $this->prefDMS['scan_mode'] = 'color';
        $this->prefDMS['scan_contrast'] = 0.0;
        $this->prefDMS['scan_brightness'] = 0.0;
        $this->prefDMS['scan_white_level'] = 0.0;
        $this->prefDMS['scan_depth'] = 24;
        $this->prefDMS['scan_resolution'] = 300;
        $this->prefDMS['exe'] = [];
        $this->prefDMS['exe']['writer'] = '/usr/bin/oowriter';
        $this->prefDMS['exe']['calc'] = '/usr/bin/oocalc';
        $this->prefDMS['exe']['draw'] = '/usr/bin/oodraw';
        $this->prefDMS['exe']['impress'] = '/usr/bin/ooimpress';
        $this->prefDMS['exe']['image'] = '/usr/bin/gimp';
        $this->prefDMS['exe']['music'] = '/usr/bin/xmms';
        $this->prefDMS['exe']['ogg'] = '/usr/bin/xmms';
        $this->prefDMS['exe']['wav'] = '/usr/bin/xmms';
        $this->prefDMS['exe']['pdf'] = '/usr/bin/evince';
        $this->prefDMS['exe']['tex'] = '/usr/bin/xemacs';
        $this->prefDMS['exe']['ltx'] = '/usr/bin/xemacs';
        $this->prefDMS['exe']['txt'] = '/usr/bin/gedit';
        $this->prefDMS['exe']['flowchart'] = '/usr/bin/dia';
        $this->prefDMS['exe']['googleearth'] = 'googleearth';
        $this->prefDMS['exe']['internet'] = '/usr/bin/firefox';
        $this->prefDMS['exe']['html'] = '/usr/bin/bluefish';
        $this->prefDMS['exe']['python'] = '/usr/bin/gedit';
        $this->prefDMS['exe']['mindmap'] = '/usr/bin/vym';
        $this->prefDMS['exe']['CAD'] = '/usr/bin/qcad';
        $this->prefDMS['exe']['EPUB'] = '/usr/bin/calibre';
        $this->prefDMS['exe']['VECTOR'] = '/usr/bin/inkscape';
        $this->prefApps['PDF'] = $this->prefDMS['exe']['pdf'];
        $this->prefApps['printPickup'] = 'lpr';
        $this->prefApps['printSupply'] = 'lpr';
        $this->prefApps['printInvoice'] = 'lpr';
        $this->prefApps['printNewsletter'] = 'lpr';
        $this->prefApps['SIP'] = 'ekiga';
        $this->prefApps['SIP_PARAMS'] = '-c';
        $this->prefApps['printCash1'] = 'lpr';
        $this->prefApps['printCash2'] = 'lpr';
        $this->prefApps['printCash3'] = 'lpr';
        $this->prefDMS['fileformat'] = [];
        $this->setFileFormats();
        $this->Email = [];
        $this->Email['sendInvoice'] = false;
        $this->Email['sendSupply'] = false;
        $this->Email['sendGet'] = false;
        $this->Email['SSL'] = false;
        $this->Email['From'] = 'MyAddress@mail_anywhere.com';
        $this->Email['Host'] = 'mail_anywhere.com';
        $this->Email['Port'] = '25';
        $this->Email['LoginUser'] = 'login';
        $this->Email['Password'] = 'secret';
        $this->Email['Signatur'] = 'NONE';
        $this->Email['extPrg'] = 'thunderbird';
        $this->Email['check_imap'] = false;
        $this->Email['ImapSSL'] = false;
        $this->Email['ImapHost'] = 'mail_anywhere.com';
        $this->Email['ImapPort'] = '25';
        $this->Email['ImapLoginUser'] = 'login';
        $this->Email['ImapPassword'] = 'secret';
        $this->Email['ImapCrypt'] = 0;
        $this->Email['Crypt'] = 0;
        $this->Twitter = [];
        $this->Twitter['TwitterName'] = 'Test';
        $this->Twitter['TwitterPassword'] = 'Test';
        $this->Conference = [];
        $this->Conference['Name'] = 'Test';
        $this->Conference['Grid'] = 'SecondLife';
        $this->Conference['Password'] = 'Test';
        $this->Communications = [];
        $this->Communications['textChat'] = 'OWN';
        $this->Communications['3DChat'] = '/usr/bin/phoenix';
        $this->Communications['emailPrg'] = '/usr/bin/thunderbird';
        $this->prefFinances = [];
        $this->prefFinances['cash1'] = '16000';
        $this->prefFinances['cash2'] = '16100';
        $this->prefFinances['bank1'] = '18100';
        $this->prefFinances['bank2'] = '18200';
        $this->prefFinances['bank3'] = '18300';
        $this->prefFinances['directDebit1'] = '18100';
        $this->prefFinances['directDebit2'] = '18200';
        $this->prefFinances['directDebit3'] = '18300';
        $this->prefFinances['creditCard1'] = '18100';
        $this->prefFinances['creditCard'] = '18101';
        $this->prefFinances['debits1'] = '12210';
        $this->prefFinances['payable1'] = '33000';
        $this->prefWindow = [];
        $this->prefWindow['MainMaximize'] = true;
        $this->prefLocale['TimeOffset'] = '+0';
        $this->Params = [];
        $this->dicUser = [];
        $this->sqlDicUser = [];
        $this->dicUserKeys = [];
        $this->pathAddressPhoneListing1 = os_path::abspath('.');
        pyjslib_printnl($this->pathAddressPhoneListing1);
        $this->setDicUserKeys('edit', 'e');
        $this->setDicUserKeys('delete', 'd');
        $this->setDicUserKeys('new', 'n');
        $this->setDicUserKeys('print', 'p');
        $this->setDicUserKeys('save', 's');
        $this->setDicUserKeys('address_edit', 'e');
        $this->setDicUserKeys('address_delete', 'd');
        $this->setDicUserKeys('address_new', 'n');
        $this->setDicUserKeys('address_save', 's');
        $this->setDicUserKeys('address_print', 'p');
        $this->setDicUserKeys('address_partner_edit', 'e');
        $this->setDicUserKeys('address_partner_delete', 'd');
        $this->setDicUserKeys('address_partner_new', 'n');
        $this->setDicUserKeys('address_partner_print', 'p');
        $this->setDicUserKeys('address_partner_save', 's');
        $this->setDicUserKeys('articles_edit', 'e');
        $this->setDicUserKeys('articles_delete', 'd');
        $this->setDicUserKeys('articles_new', 'n');
        $this->setDicUserKeys('articles_print', 'p');
        $this->setDicUserKeys('articles_save', 's');
        $this->setDicUserKeys('articles_purchase_edit', 'e');
        $this->setDicUserKeys('articles_purchase_delete', 'd');
        $this->setDicUserKeys('articles_purchase_new', 'n');
        $this->setDicUserKeys('articles_purchase_print', 'p');
        $this->setDicUserKeys('articles_purchase_save', 's');
        $this->setDicUserKeys('leasing_edit', 'e');
        $this->setDicUserKeys('leasing_delete', 'd');
        $this->setDicUserKeys('leasing_new', 'n');
        $this->setDicUserKeys('leasing_print', 'p');
        $this->setDicUserKeys('leasing_save', 's');
        $this->setDicUserKeys('staff_edit', 'e');
        $this->setDicUserKeys('staff_delete', 'd');
        $this->setDicUserKeys('staff_new', 'n');
        $this->setDicUserKeys('staff_print', 'p');
        $this->setDicUserKeys('staff_save', 's');
        $this->setDicUserKeys('staff_fee_edit', 'e');
        $this->setDicUserKeys('staff_fee_delete', 'd');
        $this->setDicUserKeys('staff_fee_new', 'n');
        $this->setDicUserKeys('staff_fee_print', 'p');
        $this->setDicUserKeys('staff_fee_save', 's');
        $this->setDicUserKeys('staff_misc_edit', 'e');
        $this->setDicUserKeys('staff_misc_delete', 'd');
        $this->setDicUserKeys('staff_misc_new', 'n');
        $this->setDicUserKeys('staff_misc_print', 'p');
        $this->setDicUserKeys('staff_misc_save', 's');
        $this->setDicUserKeys('staff_vacation_edit', 'e');
        $this->setDicUserKeys('staff_vacation_delete', 'd');
        $this->setDicUserKeys('staff_vacation_new', 'n');
        $this->setDicUserKeys('staff_vacation_print', 'p');
        $this->setDicUserKeys('staff_vacation_save', 's');
        $this->setDicUserKeys('staff_disease_edit', 'e');
        $this->setDicUserKeys('staff_disease_delete', 'd');
        $this->setDicUserKeys('staff_disease_new', 'n');
        $this->setDicUserKeys('staff_disease_print', 'p');
        $this->setDicUserKeys('staff_disease_save', 's');
        $this->setDicUserKeys('project_edit', 'e');
        $this->setDicUserKeys('project_delete', 'd');
        $this->setDicUserKeys('project_new', 'n');
        $this->setDicUserKeys('project_print', 'p');
        $this->setDicUserKeys('project_save', 's');
        $this->setDicUserKeys('botany_edit', 'e');
        $this->setDicUserKeys('botany_delete', 'd');
        $this->setDicUserKeys('botany_new', 'n');
        $this->setDicUserKeys('botany_print', 'p');
        $this->setDicUserKeys('botany_save', 's');
        $this->setDicUserKeys('hibernation_edit', 'e');
        $this->setDicUserKeys('hibernation_delete', 'd');
        $this->setDicUserKeys('hibernation_new', 'n');
        $this->setDicUserKeys('hibernation_save', 's');
        $this->setDicUserKeys('hibernation_print', 'p');
        $this->setDicUserKeys('hibernation_plant_edit', 'f');
        $this->setDicUserKeys('hibernation_plant_delete', 'g');
        $this->setDicUserKeys('hibernation_plant_new', 'h');
        $this->setDicUserKeys('hibernation_plant_save', 'a');
        $this->setDicUserKeys('hibernation_plant_print', 't');
        $this->setDicUserKeys('grave_edit', 'e');
        $this->setDicUserKeys('grave_delete', 'd');
        $this->setDicUserKeys('grave_new', 'n');
        $this->setDicUserKeys('grave_save', 's');
        $this->setDicUserKeys('grave_print', 'p');
        $this->setDicUserKeys('graveSpring_edit', 'e');
        $this->setDicUserKeys('graveSpring_delete', 'd');
        $this->setDicUserKeys('graveSpring_new', 'n');
        $this->setDicUserKeys('graveSpring_save', 's');
        $this->setDicUserKeys('graveSpring_print', 'p');
        $this->setDicUserKeys('graveSummer_edit', 'e');
        $this->setDicUserKeys('graveSummer_delete', 'd');
        $this->setDicUserKeys('graveSummer_new', 'n');
        $this->setDicUserKeys('graveSummer_save', 's');
        $this->setDicUserKeys('graveSummer_print', 'p');
        $this->setDicUserKeys('graveAutumn_edit', 'e');
        $this->setDicUserKeys('graveAutumn_delete', 'd');
        $this->setDicUserKeys('graveAutumn_new', 'n');
        $this->setDicUserKeys('graveAutumn_save', 's');
        $this->setDicUserKeys('graveAutumn_print', 'p');
        $this->setDicUserKeys('graveWinter_edit', 'e');
        $this->setDicUserKeys('graveWinter_delete', 'd');
        $this->setDicUserKeys('graveWinter_new', 'n');
        $this->setDicUserKeys('graveWinter_save', 's');
        $this->setDicUserKeys('graveWinter_print', 'p');
        $this->setDicUserKeys('graveHolidays_edit', 'e');
        $this->setDicUserKeys('graveHolidays_delete', 'd');
        $this->setDicUserKeys('graveHolidays_new', 'n');
        $this->setDicUserKeys('graveHolidays_save', 's');
        $this->setDicUserKeys('graveHolidays_print', 'p');
        $this->setDicUserKeys('graveAnnual_edit', 'e');
        $this->setDicUserKeys('graveAnnual_delete', 'd');
        $this->setDicUserKeys('graveAnnual_new', 'n');
        $this->setDicUserKeys('graveAnnual_save', 's');
        $this->setDicUserKeys('graveAnnual_print', 'p');
        $this->setDicUserKeys('graveUnique_edit', 'e');
        $this->setDicUserKeys('graveUnique_delete', 'd');
        $this->setDicUserKeys('graveUnique_new', 'n');
        $this->setDicUserKeys('graveUnique_save', 's');
        $this->setDicUserKeys('graveUnique_print', 'p');
        $this->refreshDicUser();
    }
    function setFileFormats() {
        $this->prefDMS['fileformat'] = [];
        $this->prefDMS['fileformat']['scanImage'] = ['format' => 'Image Scanner', 'suffix' => ['NONE'], 'executable' => 'INTERN'];
        $this->prefDMS['fileformat']['LINK'] = ['format' => 'LINK', 'suffix' => ['NONE'], 'executables' => 'INTERN'];
        $this->prefDMS['fileformat']['oow'] = ['format' => 'Open Office Writer', 'suffix' => ['sxw', 'sdw', 'odt', 'ott', 'doc', 'rtf'], 'executable' => $this->prefDMS['exe']['writer']];
        $this->prefDMS['fileformat']['ooc'] = ['format' => 'Open Office Calc', 'suffix' => ['sxc', 'sdc', 'ods', 'ots', 'xls'], 'executable' => $this->prefDMS['exe']['calc']];
        $this->prefDMS['fileformat']['ood'] = ['format' => 'Open Office Draw', 'suffix' => ['sxd', 'odg', 'otg'], 'executable' => $this->prefDMS['exe']['draw']];
        $this->prefDMS['fileformat']['ooi'] = ['format' => 'Open Office Impress', 'suffix' => ['sti', 'sxi', 'odp', 'otp'], 'executable' => $this->prefDMS['exe']['impress']];
        $this->prefDMS['fileformat']['gimp'] = ['format' => 'Gimp', 'suffix' => ['xcf', 'jpg', 'gif', 'png', 'tif', 'tiff'], 'executable' => $this->prefDMS['exe']['image']];
        $this->prefDMS['fileformat']['mp3'] = ['format' => 'MP3', 'suffix' => ['mp3'], 'executable' => $this->prefDMS['exe']['music']];
        $this->prefDMS['fileformat']['ogg'] = ['format' => 'OGG', 'suffix' => ['ogg'], 'executable' => $this->prefDMS['exe']['ogg']];
        $this->prefDMS['fileformat']['wav'] = ['format' => 'WAV', 'suffix' => ['wav'], 'executable' => $this->prefDMS['exe']['wav']];
        $this->prefDMS['fileformat']['txt'] = ['format' => 'Text', 'suffix' => ['txt'], 'executable' => $this->prefDMS['exe']['txt']];
        $this->prefDMS['fileformat']['tex'] = ['format' => 'TEX', 'suffix' => ['tex'], 'executable' => $this->prefDMS['exe']['tex']];
        $this->prefDMS['fileformat']['latex'] = ['format' => 'LATEX', 'suffix' => ['ltx'], 'executable' => $this->prefDMS['exe']['ltx']];
        $this->prefDMS['fileformat']['pdf'] = ['format' => 'Adobe PDF', 'suffix' => ['pdf'], 'executable' => $this->prefDMS['exe']['pdf']];
        $this->prefDMS['fileformat']['dia'] = ['format' => 'DIA', 'suffix' => ['dia'], 'executable' => $this->prefDMS['exe']['flowchart']];
        $this->prefDMS['fileformat']['googleearth'] = ['format' => 'KMZ', 'suffix' => ['kmz', 'kml', 'eta'], 'executable' => $this->prefDMS['exe']['googleearth']];
        $this->prefDMS['fileformat']['html'] = ['format' => 'HTML', 'suffix' => ['html', 'htm'], 'executable' => $this->prefDMS['exe']['html']];
        $this->prefDMS['fileformat']['python'] = ['format' => 'PYTHON', 'suffix' => ['py'], 'executable' => $this->prefDMS['exe']['python']];
        $this->prefDMS['fileformat']['mindmap'] = ['format' => 'MINDMAP', 'suffix' => ['vym', 'mm', 'mmp', 'emm', 'nmind', 'TWD'], 'executable' => $this->prefDMS['exe']['mindmap']];
        $this->prefDMS['fileformat']['cad'] = ['format' => 'CAD', 'suffix' => ['dwb', 'stp', 'step', 'dwg', 'dxf', 'igs', 'iges'], 'executable' => $this->prefDMS['exe']['CAD']];
        $this->prefDMS['fileformat']['bin'] = ['format' => 'BINARY', 'suffix' => ['bin'], 'executable' => $this->prefDMS['exe']['txt']];
        $this->prefDMS['fileformat']['ebook'] = ['format' => 'EBOOK', 'suffix' => ['epub', 'mobi'], 'executable' => $this->prefDMS['exe']['EPUB']];
        $this->prefDMS['fileformat']['vector'] = ['format' => 'VECTOR', 'suffix' => ['svg'], 'executable' => $this->prefDMS['exe']['VECTOR']];
    }
    /**
     * set self.dicuser to actual values
     */
    function refreshDicUser() {
        $this->dicUser['Locales'] = $this->userLocales;
        $this->dicUser['showNews'] = $this->userShowNews;
        $this->dicUser['Database'] = $this->Database;
        $this->dicUser['Encoding'] = $this->userEncoding;
        $this->dicUser['Encode'] = $this->Encode;
        $this->dicUser['DateTimeformatString'] = $this->userDateTimeFormatString;
        $this->dicUser['DateformatString'] = $this->userDateFormatString;
        $this->dicUser['DateTimeformatEncoding'] = $this->userDateTimeFormatEncoding;
        $this->dicUser['TimeformatString'] = $this->userTimeFormatString;
        $this->dicUser['TimeOffset'] = $this->userTimeOffset;
        $this->dicUser['SQLDateFormat'] = $this->userSQLDateFormat;
        $this->dicUser['SQLTimeFormat'] = $this->userSQLTimeFormat;
        $this->dicUser['SQLDateTimeFormat'] = $this->userSQLDateTimeFormat;
        $this->dicUser['Name'] = $this->userName;
        $this->dicUser['Debug'] = $this->sDebug;
        $this->dicUser['prefPath'] = $this->prefPath;
        $this->dicUser['SessionID'] = $this->getSessionID();
        $this->dicUser['userType'] = $this->userType;
        $this->dicUser['prefColor'] = $this->prefColor;
        $this->dicUser['prefDMS'] = $this->prefDMS;
        $this->dicUser['prefApps'] = $this->prefApps;
        $this->dicUser['client'] = $this->client;
        $this->dicUser['Email'] = $this->Email;
        $this->dicUser['Twitter'] = $this->Twitter;
        $this->dicUser['Conference'] = $this->Conference;
        $this->dicUser['prefFinances'] = $this->prefFinances;
        $this->dicUser['prefLocale'] = $this->prefLocale;
        $this->dicUser['Communications'] = $this->Communications;
        $this->dicUser['prefWindow'] = $this->prefWindow;
        $this->refreshSqlDicUser();
    }
    function refreshSqlDicUser() {
        $this->sqlDicUser['Name'] = $this->userName;
        $this->sqlDicUser['SessionID'] = $this->getSessionID();
        $this->sqlDicUser['userType'] = $this->userType;
        $this->sqlDicUser['client'] = $this->client;
        $this->sqlDicUser['Locales'] = $this->userLocales;
        $this->sqlDicUser['Database'] = $this->Database;
        $this->sqlDicUser['Encoding'] = $this->userEncoding;
        $this->sqlDicUser['Encode'] = $this->Encode;
        $this->sqlDicUser['DateTimeformatString'] = $this->userDateTimeFormatString;
        $this->sqlDicUser['DateformatString'] = $this->userDateFormatString;
        $this->sqlDicUser['Twitter'] = $this->Twitter;
        $this->sqlDicUser['Conference'] = $this->Conference;
        $this->sqlDicUser['DateTimeformatEncoding'] = $this->userDateTimeFormatEncoding;
        $this->sqlDicUser['SQLDateFormat'] = $this->userSQLDateFormat;
        $this->sqlDicUser['SQLTimeFormat'] = $this->userSQLTimeFormat;
        $this->sqlDicUser['SQLDateTimeFormat'] = $this->userSQLDateTimeFormat;
    }
    function getUser($result) {
        try {
            $this->userLocales = $result['locales'];
            if (($result['user_show_news'] == 'f')) {
                $this->userShowNews = false;
            }
            else {
                $this->userShowNews = true;
            }
            if (($result['user_win_max'] == 'f')) {
                $this->prefWindow['MainMaximize'] = false;
            }
            else {
                $this->prefWindow['MainMaximize'] = true;
            }
            $this->prefPath['StandardInvoice1'] = $result['path_to_docs_invoices'];
            $this->prefPath['StandardSupply1'] = $result['path_to_docs_supply'];
            $this->prefPath['StandardPickup1'] = $result['path_to_docs_pickup'];
            $this->prefPath['AddressLists'] = $result['path_to_docs_address_lists'];
            $this->prefPath['ReportStandardInvoice1'] = $result['path_to_report_invoices'];
            $this->prefPath['ReportStandardSupply1'] = $result['path_to_report_supply'];
            $this->prefPath['ReportStandardPickup1'] = $result['path_to_report_pickup'];
            $this->prefPath['ReportAddressLists'] = $result['path_to_report_address_lists'];
            $this->prefDMS['scan_device'] = $result['scanner_device'];
            $this->prefDMS['scan_r'] = ['x' => $result['scanner_brx'], 'y' => $result['scanner_bry']];
            $this->prefDMS['scan_mode'] = $result['scanner_mode'];
            $this->prefDMS['scan_contrast'] = $result['scanner_contrast'];
            $this->prefDMS['scan_brightness'] = $result['scanner_brightness'];
            $this->prefDMS['scan_white_level'] = $result['scanner_white_level'];
            $this->prefDMS['scan_depth'] = $result['scanner_depth'];
            $this->prefDMS['scan_resolution'] = $result['scanner_resolution'];
            $this->prefDMS['exe']['writer'] = $result['exe_oowriter'];
            $this->prefDMS['exe']['calc'] = $result['exe_oocalc'];
            $this->prefDMS['exe']['draw'] = $result['exe_oodraw'];
            $this->prefDMS['exe']['impress'] = $result['exe_ooimpress'];
            $this->prefDMS['exe']['image'] = $result['exe_image'];
            $this->prefDMS['exe']['music'] = $result['exe_music'];
            $this->prefDMS['exe']['ogg'] = $result['exe_ogg'];
            $this->prefDMS['exe']['wav'] = $result['exe_wav'];
            $this->prefDMS['exe']['pdf'] = $result['exe_pdf'];
            $this->prefDMS['exe']['tex'] = $result['exe_tex'];
            $this->prefDMS['exe']['ltx'] = $result['exe_ltx'];
            $this->prefDMS['exe']['txt'] = $result['exe_txt'];
            $this->prefDMS['exe']['flowchart'] = $result['exe_flowchart'];
            $this->prefDMS['exe']['googleearth'] = $result['exe_googleearth'];
            $this->prefDMS['exe']['internet'] = $result['exe_internet'];
            $this->prefDMS['exe']['html'] = $result['exe_html'];
            $this->prefDMS['exe']['python'] = $result['exe_python'];
            $this->prefDMS['exe']['mindmap'] = $result['exe_mindmap'];
            $this->prefDMS['exe']['CAD'] = $result['exe_cad'];
            $this->prefColor['BG'] = $result['color_bg'];
            $this->prefColor['FG'] = $result['color_fg'];
            $this->prefColor['DUTY_BG'] = $result['color_duty_bg'];
            $this->prefColor['DUTY_FG'] = $result['color_duty_fg'];
            $this->prefLocale['TimeOffset'] = $result['time_offset'];
            pyjslib_printnl(['User set email values: ', $result['user_mail_supply']], true);
            if (in_array($result['user_mail_pickup'], ['t', true, 'T', 'true'])) {
                $this->Email['sendGet'] = true;
            }
            else {
                $this->Email['sendGet'] = false;
            }
            if (in_array($result['user_mail_supply'], ['t', true, 'T', 'true'])) {
                $this->Email['sendSupply'] = true;
                pyjslib_printnl(['Set sendSupply totrue,  ', $this->Email['sendSupply']], true);
            }
            else {
                $this->Email['sendSupply'] = false;
            }
            if (in_array($result['user_mail_invoice'], ['t', true, 'T', 'true'])) {
                $this->Email['sendInvoice'] = true;
            }
            else {
                $this->Email['sendInvoice'] = false;
            }
            $this->Email['From'] = $result['email_user_address'];
            $this->Email['Host'] = $result['email_user_host'];
            $this->Email['Port'] = $result['email_user_port'];
            $this->Email['LoginUser'] = $result['email_user_loginname'];
            $this->Email['Password'] = $result['email_user_password'];
            $this->Email['ImapHost'] = $result['email_user_imap_host'];
            $this->Email['ImapPort'] = $result['email_user_imap_port'];
            $this->Email['ImapLoginUser'] = $result['email_user_imap_loginname'];
            $this->Email['ImapPassword'] = $result['email_user_imap_password'];
            $this->Email['Crypt'] = $result['email_user_crypt'];
            $this->Email['ImapCrypt'] = $result['email_user_imap_crypt'];
            if (($result['user_imap_email_ssl'] == 't')) {
                $this->Email['ImapSSL'] = true;
            }
            else {
                $this->Email['ImapSSL'] = false;
            }
            if (($result['user_email_ssl'] == 't')) {
                $this->Email['SSL'] = true;
            }
            else {
                $this->Email['SSL'] = false;
            }
            $this->Email['Signatur'] = $result['email_user_signatur'];
            $this->Email['extPrg'] = $result['email_ext_prg'];
            if (($result['user_check_imap'] == 't')) {
                $this->Email['check_imap'] = true;
            }
            else {
                $this->Email['check_imap'] = false;
            }
            $this->Twitter['TwitterName'] = $result['twitter_user_name'];
            $this->Twitter['TwitterPassword'] = $result['twitter_user_password'];
            $this->Conference['Name'] = $result['grid_user_name']->strip();
            $this->Conference['Grid'] = $result['user_grid'];
            $this->Conference['Password'] = $result['grid_user_password']->strip();
            $this->Communications['textChat'] = $result['std_text_chat'];
            $this->Communications['3DChat'] = $result['std_3d_chat'];
            $this->Communications['emailPrg'] = $result['std_email_prg'];
            $this->prefApps['PDF'] = $this->prefDMS['exe']['pdf'];
            pyjslib_printnl(['prefApps[\'PDF\'] 0=', $this->prefDMS['exe']['pdf']], true);
            pyjslib_printnl(['prefApps[\'PDF\'] 1= ', $this->prefApps['PDF']], true);
            $this->prefApps['printPickup'] = $result['exe_print_pickup'];
            $this->prefApps['printSupply'] = $result['exe_print_supply'];
            $this->prefApps['printInvoice'] = $result['exe_print_invoice'];
            $this->prefApps['printNewsletter'] = $result['exe_print_newsletter'];
            $this->prefApps['printCash1'] = $result['exe_print_cash1'];
            $this->prefApps['printCash2'] = $result['exe_print_cash2'];
            $this->prefApps['printCash3'] = $result['exe_print_cash3'];
        }
        catch(Exception $e) {
                        pyjslib_printnl('user.py,  getuser');
            pyjslib_printnl($Exception);
            pyjslib_printnl($param);
        }
        $this->setFileFormats();
        $this->refreshDicUser();
        return $this;
    }
    function setParams($dicParams) {
        if ($dicParams) {
            foreach( $dicParams->keys() as $key ) {
                $this->Params[$key] = $dicParams[$key];
            }
        }
    }
    /**
     * @return: Dictionary with user-infos
     */
    function getDicUser() {
        $this->refreshDicUser();
        return $this->dicUser;
    }
    function getSqlDicUser() {
        $this->refreshSqlDicUser();
        return $this->sqlDicUser;
    }
    function getDicUserKeys() {
        return $this->dicUserKeys;
    }
    function setDicUserKeys($dKey,$sKey) {
        $this->dicUserKeys[$dKey] = $sKey;
    }
    function getInternetUser() {
        $sqlDicUser2 = [];
        $sqlDicUser2['Name'] = $this->userName;
        $sqlDicUser2['SessionID'] = $this->getSessionID();
        $sqlDicUser2['userType'] = $this->userType;
        $sqlDicUser2['Database'] = $this->Database;
        $sqlDicUser2['client'] = $this->client;
        return $sqlDicUser2;
    }
    /**
     * @param s: Name of the User
     */
    function setUserName($s) {
        $this->userName = $s;
        $this->refreshDicUser();
    }
    /**
     * @return: Name of the user
     */
    function getUserName() {
        return $this->userName;
    }
    /**
     * set the sessionID 
     * @param sid: session-id
     */
    function setSessionID($sid) {
        $this->sessionID = $sid;
        $this->refreshDicUser();
    }
    function getSessionID() {
        return $this->sessionID;
    }
    function setDebug($sDebug='NO') {
        $this->sDebug = $sDebug;
        pyjslib_printnl('sDebug(User)  = ' . $sDebug);
        $this->refreshDicUser();
    }
    function getDebug() {
        if (in_array($this->sDebug->upper(), ['YES', 'Y', 'JA'])) {
            return true;
        }
        else {
            return false;
        }
    }
}

