<?php

trait  defaultValues {
    /* py2php : PHP does not support multiple inheritance.
     * Consider defining the referenced classes as traits instead.
     * See: http://php.net/manual/en/language.oop5.traits.php
     */
   
    function __construct() {
       
        constants::__construct();
    }
    function checkMimeType($sFile) {
        $sSuffix = array_slice($sFile, ($sFile->rfind('.') + 1), null)->lower();
        //pyjslib_printnl(['sSuffix', $sSuffix], true);
        if (($sSuffix == 'xml')) {
            return 'text/x-text';
        }
        foreach( $this->MimeType->keys() as $key ) {
            if (in_array($sSuffix, $this->MimeType[$key])) {
                 //pyjslib_printnl(['new MimeType = ', $key], true);
                return $key;
            }
        }
        return 'text/plain';
    }
}

