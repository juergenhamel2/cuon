<html>
<head>
  <link rel="stylesheet" href="login.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
  
  <body>

      
<?php
   
include ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');
 
function prefered_language(array $available_languages, $http_accept_language) {

    $available_languages = array_flip($available_languages);

    $langs;
    preg_match_all('~([\w-]+)(?:[^,\d]+([\d.]+))?~', strtolower($http_accept_language), $matches, PREG_SET_ORDER);
    foreach($matches as $match) {

        list($a, $b) = explode('-', $match[1]) + array('', '');
        $value = isset($match[2]) ? (float) $match[2] : 1.0;

        if(isset($available_languages[$match[1]])) {
            $langs[$match[1]] = $value;
            continue;
        }

        if(isset($available_languages[$a])) {
            $langs[$a] = $value - 0.1;
        }

    }
    arsort($langs);

    return $langs;
}


$supported_languages = array("en","de","nl","it");
$supported_languages = array_flip($supported_languages);
   
//print_r($supported_languages);

// array(2) { ["en"]=> int(0) ["nl"]=> int(1) }

$http_accept_language = $_SERVER["HTTP_ACCEPT_LANGUAGE"]; // es,nl;q=0.8,en-us;q=0.5,en;q=0.3

//echo  "accepted language = " . $http_accept_language . "</br>" ;


preg_match_all('~([\w-]+)(?:[^,\d]+([\d.]+))?~', strtolower($http_accept_language), $matches, PREG_SET_ORDER);

$available_languages = array();

foreach ($matches as $match)
{
    list($language_code,$language_region) = explode('-', $match[1]) + array('', '');

    $priority = isset($match[2]) ? (float) $match[2] : 1.0;

    $available_languages[][$language_code] = $priority;
}

//echo "</br> available: ";
//echo " " . var_dump($available_languages) . "</br>" ;


$default_priority = (float) 0;
$default_language_code = 'en';

foreach ($available_languages as $key => $value)
{
    $language_code = key($value);
    $priority = $value[$language_code];

    if ($priority > $default_priority && array_key_exists($language_code,$supported_languages))
    {
        $default_priority = $priority;
        $default_language_code = $language_code;

  //      echo "def. prio = " . var_dump($default_priority). "</br>" ; // float(0.8)
  //      echo "def code = " . var_dump($default_language_code). "</br>" ; // string(2) "nl"
    }
}





//echo "</br>";

//echo "default. lang = " . var_dump($default_language_code) . "</br>" ; // string(2) "nl"

      
//$langs = prefered_language($supported_languages,$http_accept_language);


//echo "avail.Lang = " . var_dump($available_languages) . "</br>";

//echo "default. lang = " . $default_language_code . "</br>" ;

putenv('LC_ALL='.$default_language_code);
setlocale(LC_ALL, $default_language_code);

// Path to mo data
bindtextdomain("cuon", "./locale");

// choose Domain 
textdomain("cuon");

// translate is in  /locale/de_DE/LC_MESSAGES/cuon.mo 

// echo _("test");

//$test = callRP('Database.is_running');


echo '<form name="cuonUser" method="post" form action="libs/PHP/Login/login.php">';
echo '	<div class="message"><?php if($message!="") { echo $message; } ?></div>';
echo '		<table border="0" cellpadding="10" cellspacing="1" width="500" align="center" class="tblLogin">';
echo '			<tr class="tableheader">';
echo '			<td align="center" colspan="2">' . _("Enter Login Details") . '</td>';
echo '			</tr>';
echo '			<tr class="tablerow">';
echo '			<td>';
echo '			<input type="text" name="userName" placeholder=' ._("User Name") .' class="login-input"></td>';
echo '			</tr>';
echo '			<tr class="tablerow">';
echo '			<td>';
echo '			<input type="password" name="password" placeholder=' . _("Password") . ' class="login-input"></td>';
echo '			</tr>';
echo '          	<tr class="tablerow">';
echo '			<td>';
echo '			<input type="client_id" name="client_id" placeholder=' . _("Client-ID") . ' class="login-input"></td>';
echo '			</tr>';
echo '			<tr class="tableheader">';
echo '			<td align="center" colspan="2"><input type="submit" name="submit" value="Submit" class="btnSubmit"></td>';
echo '			</tr>';
echo '		</table>';
echo '</form>';



?>
		
