<?php

class cyr_column {
    function __construct() {
        $this->Name = 'EMPTY';
        $this->nameOfVariable = 'EMPTY';
        $this->sizeOfDatafield = '0';
        $this->Type = null;
        $this->defaultValue = null;
    }
    function setName($s) {
        $this->Name = $s;
        $this->nameOfVariable = 'e_' . $s;
    }
    function getName() {
        return $this->Name;
    }
    function setType($s) {
        $this->Type = $s;
    }
    function getType() {
        return $this->Type;
    }
    function getSizeOfDatafield() {
        return $this->sizeOfDatafield;
    }
    function setSizeOfDatafield($sSize) {
        $this->sizeOfDatafield = $sSize;
    }
    function setAllowNull($b) {
        $this->allowNull = ~$b;
    }
    function isAllowNull() {
        return $this->allowNull;
    }
    function setPrimaryKey($s) {
        $this->primaryKey = $s;
    }
    function getPrimaryKey() {
        return $this->primaryKey;
    }
    function setDefaultValue($s=null) {
        $this->defaultValue = $s;
    }
    function getDefaultValue() {
        return $this->defaultValue;
    }
}

