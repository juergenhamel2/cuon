<?php

require_once( 'dumps.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');



class cyr_load_table extends dumps  {
   
  
    function __construct() {
        //defaultValues::__construct();
        //MyXML::__construct();
        //$oDumps = new dumps();
         
         dumps::__construct(Null);
        
        $this->configPath = '/etc/cuon/';
    }
    function getListOfTableNames($sFile) {
        $this->elog(['load xml = ', $sFile], true);
        $doc = $this->getDatabaseDescription($sFile);
        $allLists = $this->getListOfTables($doc);
        return $allLists;
    }
    function getListOfSequenceNames($sFile) {
        $doc = $this->getDatabaseDescription($sFile);
        $allLists = $this->getListOfSequences($doc);
        return $allLists;
    }
    function getListOfForeignKeyNames($sFile) {
        $doc = $this->getDatabaseDescription($sFile);
        $allLists = $this->getListOfForeignKeys($doc);
        return $allLists;
    }
    function getForeignKeyDefinition($sFile,$nameOfForeignKey) {
        $dicForeign = [];
        $doc = $this->getDatabaseDescription($sFile);
        $this->elog($doc->toxml());
        $dicForeign['name'] = $nameOfForeignKey->encode('ascii');
        $cyRootNode = $this->getRootNode($doc);
        $cyForeignNode = $this->getForeignKey($cyRootNode[0], 'foreign_key', $nameOfForeignKey);
        $this->elog('cyForeignNode');
        $this->elog($cyForeignNode->toxml());
        $dicForeign['name'] = $this->getData($this->getNodes($cyForeignNode, 'foreign_key_name')[0])->encode('ascii');
        $dicForeign['sql'] = $this->getData($this->getNodes($cyForeignNode, 'foreign_key_sql')[0])->encode('ascii');
        $dicForeign['table'] = $this->getData($this->getNodes($cyForeignNode, 'foreign_table')[0])->encode('ascii');
        $this->elog($dicForeign);
        return $dicForeign;
    }
    function getDatabaseDescription($sFile) {
        return $this->readDocument($this->nameOfXmlTableFiles[$sFile]);
    }
    function getSequenceDefinition($sFile,$nameOfSequence) {
        $dicSeq = [];
        $doc = $this->getDatabaseDescription($sFile);
        $this->elog($doc->toxml());
        $dicSeq['name'] = $nameOfSequence->encode('ascii');
        $cyRootNode = $this->getRootNode($doc);
        $cySeqNode = $this->getSequence($cyRootNode[0], 'database_sequence', $nameOfSequence);
        $dicSeq['increment'] = $this->getData($this->getNodes($cySeqNode, 'sequence_increment')[0])->encode('ascii');
        $dicSeq['start'] = $this->getData($this->getNodes($cySeqNode, 'sequence_start')[0])->encode('ascii');
        $dicSeq['minvalue'] = $this->getData($this->getNodes($cySeqNode, 'sequence_minvalue')[0])->encode('ascii');
        $dicSeq['maxvalue'] = $this->getData($this->getNodes($cySeqNode, 'sequence_maxvalue')[0])->encode('ascii');
        $dicSeq['cycle'] = $this->getData($this->getNodes($cySeqNode, 'sequence_cycle')[0])->encode('ascii');
        $dicSeq['cache'] = $this->getData($this->getNodes($cySeqNode, 'sequence_cache')[0])->encode('ascii');
        $this->elog($dicSeq);
        return $dicSeq;
    }
    function getTableDefinition($sFile,$sNameOfTable) {
        $doc = $this->getDatabaseDescription($sFile);
        $this->elog($doc->toxml());
        $cyRootNode = $this->getRootNode($doc);
        $cyTableNode = $this->getTable($cyRootNode[0], 'table', $sNameOfTable);
        $table = cyr_table::cyr_table();
        $table->setName($sNameOfTable);
        $table->setSpecials($this->getTableSpecification($cyTableNode, 'specials'));
        $this->elog('searching Subtable');
        $subtable = re::search('inherits.*\((.*?)\)', ($table->getSpecials()), re::IGNORECASE);
        if ($subtable) {
            $sNameOfSubtable = $subtable->group(1);
            $this->elog('----------------------------------Subtable found-----------------------------------------------');
            $this->elog('Subtable -> ' . ($subtable));
            $this->elog('Subtable -> ' . $subtable->group(0));
            $this->elog('Subtable -> ' . $subtable->group(1));
            $subTable = $this->loadTable($sNameOfSubtable);
            $xColumns = $subTable->getColumns();
            foreach( $xColumns as $i ) {
                $table->addColumn($i);
                $table->nameOfColumns[] = $i->getName();
            }
        }
        $iNr = $this->getNumberOfFields($cyTableNode);
        $this->elog('Number of Columns %i ' . ($iNr));
        $iCol = 0;
        while (($iCol < $iNr)) {
            $xmlCol = $this->getColumnAt($cyTableNode, $iCol);
            $column = $cyr_column->cyr_column();
            $column->setName($this->getColumnSpecification($xmlCol, 'name'));
            $this->elog('column-name = ' . $column->getName());
            $table->nameOfColumns[] = $column->getName();
            $column->setType($this->getColumnSpecification($xmlCol, 'type'));
            $sSize = string::strip($this->getColumnSpecification($xmlCol, 'size'));
            if ($sSize) {
                $column->setSizeOfDatafield(string::strip($this->getColumnSpecification($xmlCol, 'size')));
            }
            else {
                $column->setSizeOfDatafield('18');
            }
            $column->setAllowNull(string::atoi($this->getColumnSpecification($xmlCol, 'notnull')));
            $column->setPrimaryKey(string::atoi($this->getColumnSpecification($xmlCol, 'pkey')));
            $column->setDefaultValue($this->getColumnSpecification($xmlCol, 'default'));
            $table->addColumn($column);
            $iCol += 1;
        }
        return $table;
    }
    function saveTable($sNameOfTable,$table) {
        $this->elog($sNameOfTable);
        $this->elog($table->getName());
        
        callRP('Database.saveLiInfo', $sNameOfTable, $this->doEncode((($table))));
        $liColumns = $table->getColumns();
        foreach( $liColumns as $i ) {
            $this->elog('save table-columns TTTZZ');
            $this->elog($i->getName());
            $co_name = $sNameOfTable . '_' . $i->getName();
            //callRP('Database.saveInfo', $co_name, $this->doEncode(pyjslib_repr(cPickle::dumps($i))));
            $this->elog($co_name);
            $this->elog(' TTTTUU');
        }
    }
    function loadTable($sNameOfTable) {
        
        
        $dictTable = $this->doDecode(callRP('Database.getLiInfo', $sNameOfTable));
        $this->elog('*****************************************************************************ZZ');
        $this->elog('cyr_load_table = ' .  $dictTable);
        $this->elog('****************************************************************************UU');
        $table = Null ; 
        //$table = cPickle::loads($dictTable);
        #$this->saveObject('table_' . iconv('ASCII', 'UTF-8//IGNORE', $sNameOfTable), $table);
        $this->elog('table of Columns --> ' . $table->nameOfColumns);
        foreach( $table->nameOfColumns as $i ) {
            $sColumn = eval($this->doDecode(callRP('Database.getInfo', $sNameOfTable . '_' . $i)));
            //$coColumn = cPickle::loads($sColumn);
            $table->addColumn($coColumn);
            
        }
        
        return $table;
    }
    function loadLocalTable($sNameOfTable) {
        $this->openDB();
        $table = $this->loadObject('table_' . $sNameOfTable->encode('ascii'));
        if ($table) {
            foreach( $table->nameOfColumns as $i ) {
                $coColumn = $this->loadObject('column_' . $sNameOfTable->encode('ascii') . '_' . $i->encode('ascii'));
                $table->addColumn($coColumn);
            }
        }
        $this->closeDB();
        return $table;
    }
    function loadTableOld($sNameOfTable) {
        $storage = FileStorage::FileStorage('cuon3.fs');
        $db = $DB($storage);
        $connection = $db->open();
        $root = $connection->root();
        $this->elog($root->items());
        $this->elog('-------------------------------------------------------------------------------------------------------');
        $t2 = $root[$sNameOfTable];
        $connection->close();
        $db->close();
        return $t2;
    }
}

