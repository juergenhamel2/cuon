<?php

require_once( 'cyr_column.php');
require_once( 'cyr_load_table.php');
class cyr_table {
    function __construct() {
        $this->Adabas = 1;
        $this->mSql = 2;
        $this->Mysql = 3;
        $this->Sybase = 4;
        $this->Interbase = 5;
        $this->Oracle = 6;
        $this->Informix = 7;
        $this->Microsoft = 8;
        $this->nameOfColumns = [];
        $this->Columns = [];
        $this->oneColumn = new cyr_column();
    }
    function setName($name) {
        $this->Name = $name;
    }
    function getName() {
        return $this->Name;
    }
    function setSpecials($s) {
        $this->Specials = $s;
    }
    function getSpecials() {
        return $this->Specials;
    }
    function getForeignKey($sName) {
        return $this->ForeignKey[$sName];
    }
    function setNameOfForeignKey($sName,$sValue) {
        $this->ForeignKey[$sName] = $sValue;
    }
    function addColumn($di_column) {
        $this->Columns[] = $di_column;
    }
    function getColumnAtIndex($i1) {
        return $this->Columns[$i1];
    }
    function getCountOfColumns() {
        return count($this->Columns);
    }
    function getColumns() {
        return $this->Columns;
    }
}

