<?php


/*
 * @author Jürgen Hamel
 * @license GPL V3
 * @version 0.2
coding=utf-8
Copyright (C) [2003-2018]  [Jürgen Hamel, D-32584 Löhne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

*/
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/gladexml.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/setOfEntries.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');
include_once ('cyr_table.php');


class SingleData  extends gladexml {

    
    function __construct() {
        gladeXml::__construct();
        
        $this->table = new cyr_table();
        
        $this->xmlTableDef = 1;
        $this->sNameOfTable = 'EMPTY';
        $this->withoutColumns = null;
 
        $this->listHeader = [];
        
        $this->ID = 0;
        $this->sWhere = ' ';
        $this->liFields = [];
        $this->liTreeFields = [] ;
        $this->sSort = '';
        $this->store = null;
        $this->connectTreeId = 0;
        $this->sCoding = 'utf-8';
        $this->sDateFormat = '%d.%m.%Y';
        //$this->dicUser = $this->oUser->getDicUser();
        //$this->sqlDicUser = $this->oUser->getSqlDicUser();
        //$this->dicInternetUser = $this->oUser->getInternetUser();
        $this->path = null;
        $this->statusfields = [];
        $this->sStatus = '';
        $this->firstRecord = null;
        $this->p1 = Null ; // re::compile('\(select .*\) as');
        $this->iter = null;
        $this->liItems = null;
        $this->bDistinct = false;
        $this->xml = null;
        $this->win1 = null;
        $this->TreePos = 0;
        $this->TREEFIRST = -100;
        $this->TREELAST = -1000;
        $this->loading = false;
}
  /* function old__contruct(){
        $this->oUser = $this->loadObject('User');
        if (!($this->oUser)) {
            $this->oUser = cuon_User::user->User();
        }
        $this->rpc = $cuon->XMLRPC->xmlrpc->myXmlRpc();
*/
    function beforeLoad() {
    }
    /**
     * @param record: id of the record
     * @param dicDetail: details for statusbar
     * @return: list of records
     */
    function load($record,$dicDetail=null) {
         //$this->loading = true;
         //$this->setWaitCursor();
        $this->ID = -1;
        $liRecords = [];
        $this->beforeLoad();
        $this->elog('client load = ' . $this->sNameOfTable  );
        $liRecords = callRP('Database.loadDataSL', $this->sNameOfTable, $record, $dicDetail);
        $this->loadOtherDatatable($this->ID);
        //$this->setNormalCursor();
        $this->elog("data load for 1 record = " . json_encode($liRecords) ) ;
        return $liRecords;
    }
    function getFirstRecord() {
        return $this->firstRecord;
    }
    function findSingleId() {
        $this->elog(['lifields = ', $this->liFields], true);
        $liItems = $this->getListEntries();
        $this->elog(['liItems = ', $liItems], true);
        if ($liItems) {
            $this->ID = $liItems[0][0];
        }
        else {
            $this->newRecord();
        }
        return $this->ID;
    }
    function save($liBigEntries='NO') {
        $this->setWaitCursor();
        $dicValues = $this->readEntries();
        $id = $this->saveValues($dicValues, $liBigEntries);
        $this->setNormalCursor();
        return $id;
    }
    function saveExternalData($dicValues,$liBigEntries='NO') {
        $id = $this->saveValues($dicValues, $liBigEntries);
        return $id;
    }
    function saveValues($dicValues,$liBigEntries='NO') {
        $id = 0;
        if (($liBigEntries != 'NO')) {
            foreach( $liBigEntries as $lb ) {
                $dicValues[$lb][0] = base64::encodestring($dicValues[$lb][0]);
            }
        }
        $this->elog('save record');
        $this->elog([$this->ID, $dicValues], true);
        $this->elog('-----------------------------------------------------------------------_');
        $liResult = $this->rpc->callRP('Database.saveRecord', $this->sNameOfTable, $this->ID, $dicValues, $this->sqlDicUser, $liBigEntries);
        if (($this->ID < 0) && $liResult) {
            try {
                $id = $liResult;
            }
            catch(Exception $e) {
                            }
        }
        else if (($this->ID > 0)) {
            $id = $this->ID;
        }
        if (($liResult == 0)) {
            $id = 0;
        }
        $this->saveOtherDatatable($id);
        return $id;
    }
    function saveOtherDatatable($id) {
    }
    function loadOtherDatatable($id) {
    }
    function deleteRecord() {
        $this->rpc->callRP('Database.deleteRecord', $this->sNameOfTable, $this->ID, $this->sqlDicUser);
        $this->refreshTree();
    }
    function loadCompleteTable() {
        return $this->rpc->callRP('Database.loadCompleteTable', $this->sNameOfTable, $this->sqlDicUser);
    }
    function saveTable() {
        $clt = new cyr_load_table();
        $this->table = $clt->getTableDefinition($this->xmlTableDef, $this->sNameOfTable);
        $clt->saveTable($this->sNameOfTable, $this->table);
    }
    function loadTable($allTables=null) {
        if ($allTables) {
            $this->table = $allTables[$this->sNameOfTable];
        }
        else {
            $clt = new cyr_load_table();
            $this->table = $clt->loadTable($this->sNameOfTable);
        }
    }
    function setTree($tree01) {
        $this->tree1 = $tree01;
    }
    function disconnectTree() {
        if ($this->connectTreeId) {
             //$this->tree1->get_selection()->disconnect($this->connectTreeId);
            $this->connectTreeId = null;
        }
    }
    function connectTree() {
        if (!($this->connectTreeId)) {
             //$this->connectTreeId = $this->tree1->get_selection()->connect('changed', $this->tree_select_callback);
        }
    }
    function setTreeSensitive($ok) {
        try {
            if ($ok) {
                 //  $this->tree1->set_sensitive(true);
            }
            else {
                 // $this->tree1->set_sensitive(false);
            }
        }
        catch(Exception $e) {
                        $this->elog([$Exception, $param], true);
        }
    }
    function refreshTree($Full=true) {
        $this->elog('#################### Single Data refresh Tree #########################');
        $this->elog("var full = " . $Full);  
        if ($Full) {
            $this->setEmptyEntries();
            try {
                 // assert($this->tree1);
                 $this->fillTree($this->tree1, $this->getListEntries());
                 $this->elog("filled tree");
                
                $this->treeSelectRow();
            }
            catch(Exception $e) {
                 $this->elog("EXCEPTION !!! " . $e);
                            }
        }
        else {
            try {
                assert($this->tree1);
                $this->fillTree($this->tree1, $this->liItems);
                $this->treeSelectRow();
            }
            catch(Exception $e) {
                            }
        }
    }
    function refreshTreeWithoutNewItems() {
        try {
            $this->fillTree($this->tree1, $this->getListEntries());
            $this->treeSelectRow();
        }
        catch(Exception $e) {
                    }
    }
    function tree_select_callback($treeSelection) {
        list($listStore, $this->iter) = $treeSelection->get_selected();
        if ($listStore && (count($listStore) > 0)) {
            $this->row = $listStore[0];
        }
        else {
            $this->row = -1;
        }
        if (($this->iter != null)) {
            $this->path = $listStore->get_path($this->iter);
            $this->elog(['self.listboxId at callback', $this->listboxId], true);
            $newId = $listStore->get_value($this->iter, ($this->listboxId - 1));
            if ($this->iter && $this->path) {
                $this->tree1->scroll_to_cell($this->path);
            }
            $this->fillEntries($newId);
        }
    }
    function treeSelectRowById() {
    }
    function treeSelectRow() {
        if (($this->TreePos == $this->TREEFIRST)) {
            try {
                $this->iter = $this->tree1->get_model()->get_iter_root();
                $this->path = $this->tree1->get_model()->get_path($this->iter);
            }
            catch(Exception $e) {
                            }
            $this->TreePos = 0;
        }
        else if (($this->TreePos == $this->TREELAST)) {
            $treeModel = $this->tree1->get_model();
            try {
                $this->iter = $treeModel->get_iter_root();
                $nextIter = true;
                while ($nextIter) {
                    $this->elog('goto next Iter ');
                    $nextIter = $treeModel->iter_next($this->iter);
                    if ($nextIter) {
                        $this->iter = $nextIter;
                    }
                }
                $this->path = $this->tree1->get_model()->get_path($this->iter);
            }
            catch(Exception $e) {
                                $this->elog([$Exception, $param], true);
            }
            $this->TreePos = 0;
        }
        else {
            if (!($this->iter)) {
                 // $this->iter = $this->tree1->get_model()->get_iter_root();
            }
            if (!($this->path)) {
                 //$this->path = $this->tree1->get_model()->get_path($this->iter);
            }
        }
        if ($this->iter && $this->path) {
             //$this->tree1->scroll_to_cell($this->path);
            //$this->tree1->get_selection()->select_path($this->path);
        }
    }
    function treeSelectRowByIter() {
        if ($this->iter) {
             //$this->tree1->get_selection()->select_iter($this->iter);
        }
    }
    function setStore($store01) {
        $this->store = $store01;
    }
    function fillTree($tree1=null,$listEntries=null) {
        try {
            /* $tree1->freeze_child_notify(); */
             $this->liValues = $listEntries;
             $this->elog(var_dump($listEntries) );
            /* $model->setColumns($tree1, $this->listHeader); */
            /* $tree1->thaw_child_notify(); */
        }
        catch(Exception $e) {
                        $this->elog([$Exception, $params], true);
        }
    }
    function getTreeModel($listEntries) {
        $model = $cuon->Databases->SingleDataTreeModel->SingleDataTreeModel();
        if ($this->store) {
            $model->setStore($this->store);
            $this->tree1->set_model($model->createModel($listEntries));
        }
        return $model;
    }
    function fillExternalWidget($value,$id) {
        return '';
    }
    function getFirstListRecord() {
        $liEntries = $this->rpc->callRP('Database.getListEntries', ['id' => 'int'], $this->table->getName(), 'id', $this->sWhere, $this->bDistinct, ['id'], $this->sqlDicUser);
        try {
            $dicEntry = $liEntries[0];
            $id = $dicEntry['id'];
            $this->load($id);
        }
        catch(Exception $e) {
                        $this->ID = 0;
        }
    }
    function clearAllFields() {
        
    }
    function clearAllOtherFields() {
    }
    function fillEntries($id) {
        /* $this->ID = $id; */
        /* if (($id < 1)) { */
        /*     $this->clearAllFields(); */
        /* } */
        /* else { */
        /*     $oneRecord = []; */
        /*     $dicRecord = $this->load($id); */
        /*     if ($dicRecord) { */
        /*         $oneRecord = $dicRecord[0]; */
        /*     } */
        /*     if (!($oneRecord)) { */
        /*         $this->clearAllFields(); */
        /*     } */
        /*     foreach( pyjslib_range(count($oneRecord)) as $i ) { */
        /*         $sValue = $oneRecord[$oneRecord->keys()[$i]]; */
        /*         if (($this->dicEntries->getEntryByName($oneRecord->keys()[$i]) == null)) { */
        /*             $this->fillExternalWidget($sValue, $oneRecord); */
        /*         } */
        /*         else if (($this->dicEntries->getEntryByName($oneRecord->keys()[$i])->getCreateSql() == '0')) { */
        /*             $this->fillExtraEntries($oneRecord); */
        /*         } */
        /*         else { */
        /*             $entry = $this->dicEntries->getEntryByName($oneRecord->keys()[$i]); */
        /*             if (isinstance($sValue, types::ClassType) || isinstance($sValue, types::InstanceType)) { */
        /*                 $sValue = pyjslib_repr($sValue); */
        /*             } */
        /*             if (($entry->getVerifyType() == 'string') && isinstance($sValue, types::StringType)) { */
        /*                 $sValue = $this->getCheckedValue($sValue, 'string'); */
        /*             } */
        /*             else if (($entry->getVerifyType() == 'int')) { */
        /*                 $sValue = pyjslib_repr($this->getCheckedValue($sValue, 'int')); */
        /*             } */
        /*             else if (($entry->getVerifyType() == 'float') && isinstance($sValue, types::FloatType)) { */
        /*                 try { */
        /*                     $iR = $entry->getRound(); */
        /*                     if (($iR == -1)) { */
        /*                         $iR = 2; */
        /*                     } */
        /*                     $sValue = sprintf('%0.*f', [$iR, $sValue]); */
        /*                 } */
        /*                 catch(Exception $e) { */
        /*                                             } */
        /*                 $sValue = $this->getCheckedValue($sValue, 'toStringFloat'); */
        /*             } */
        /*             else if (($entry->getVerifyType() == 'float') && isinstance($sValue, types::StringType)) { */
        /*                 $sValue = $this->getCheckedValue($sValue, 'toStringFloat'); */
        /*             } */
        /*             else if (($entry->getVerifyType() == 'numeric') && isinstance($sValue, types::FloatType)) { */
        /*                 $sValue = pyjslib_repr(round($sValue, $entry->getRound())); */
        /*             } */
        /*             else if (($entry->getVerifyType() == 'date') && isinstance($sValue, types::StringType)) { */
        /*                 $sValue = $this->getCheckedValue($sValue, 'formatedDate'); */
        /*             } */
        /*             else if (($entry->getVerifyType() == 'bool')) { */
        /*             } */
        /*             else { */
        /*                 $sValue = pyjslib_str($sValue); */
        /*             } */
        /*             $widget = $this->getWidget($entry->getName()); */
        /*             if ((string::count(pyjslib_str($widget), 'GtkEntry') > 0)) { */
        /*                 try { */
        /*                     $widget->set_text($sValue); */
        /*                 } */
        /*                 catch(Exception $e) { */
        /*                                             } */
        /*             } */
        /*             else if ((string::count(pyjslib_str($widget), 'GtkTextView') > 0)) { */
        /*                 $sValue = $this->normalizeXML($sValue, false); */
        /*                 try { */
        /*                     $buffer = $gtk->TextBuffer(null); */
        /*                 } */
        /*                 catch(Exception $e) { */
        /*                                                 $buffer = $gtk->TextBuffer(); */
        /*                 } */
        /*                 $buffer->set_text($sValue); */
        /*                 $widget->set_buffer($buffer); */
        /*             } */
        /*             else if ((string::count(pyjslib_str($widget), 'GtkComboBox') > 0)) { */
        /*                 if ($sValue && (pyjslib_int($sValue) > -1)) { */
        /*                     $widget->set_active(pyjslib_int($sValue)); */
        /*                 } */
        /*                 else { */
        /*                     $widget->set_active(-1); */
        /*                 } */
        /*             } */
        /*             else if ((string::count(pyjslib_str($widget), 'GtkCheckButton') > 0)) { */
        /*                 $this->elog(['Bool-Value from Database', $sValue], true); */
        /*                 $this->elog(['GtkCheckButton ', $entry->getName()], true); */
        /*                 $bValue = false; */
        /*                 try { */
        /*                     if (($sValue == '1') || ($sValue == 1) || ($sValue == 't') || ($sValue == 'True')) { */
        /*                         $bValue = true; */
        /*                         $this->elog(['GtkCheckButton set to ', $sValue, $bValue], true); */
        /*                     } */
        /*                 } */
        /*                 catch(Exception $e) { */
        /*                                                 $this->elog([$Exception, $params], true); */
        /*                 } */
        /*                 $sValue = $bValue; */
        /*                 $widget->set_active($sValue); */
        /*             } */
        /*             else if ((string::count(pyjslib_str($widget), 'GtkRadioButton') > 0)) { */
        /*                 if (($sValue == 1) || ($sValue == 't') || ($sValue == 'True')) { */
        /*                     $sValue = true; */
        /*                 } */
        /*                 else { */
        /*                     $sValue = false; */
        /*                 } */
        /*                 if ($sValue) { */
        /*                     $widget->set_active($sValue); */
        /*                 } */
        /*             } */
        /*             else if ((string::count(pyjslib_str($widget), 'GnomeDateEdit') > 0)) { */
        /*                 try { */
        /*                     $newDate = time::strptime($sValue, $this->sqlDicUser['DateTimeformatString']); */
        /*                     $widget->set_time(pyjslib_int(time::mktime($newDate))); */
        /*                 } */
        /*                 catch(Exception $e) { */
        /*                                             } */
        /*             } */
        /*             else if ((string::count(pyjslib_str($widget), 'GtkFileChooserButton') > 0)) { */
        /*                 $widget->set_filename($sValue); */
        /*             } */
        /*         } */
        /*     } */
        /*     $this->fillOtherEntries($oneRecord); */
        /* } */
    }
    function fillOtherEntries($oneRecord) {
    }
    function fillExtraEntries($oneRecord) {
    }
    function readEntries() {
        try {
            assert(($this->dicEntries != null));
            $dicValues = [];
            foreach( pyjslib_range($this->dicEntries->getCountOfEntries()) as $i ) {
                $entry = $this->dicEntries->getEntryAtIndex($i);
                try {
                    $this->elog(['Entry2 = ', $entry->getName()], true);
                    $widget = $this->getWidget($entry->getName());
                    if ((string::count(pyjslib_str($widget), 'GtkEntry') > 0)) {
                        $sValue = $widget->get_text();
                        $this->elog(['sValue at ', $widget, $sValue], true);
                    }
                    else if ((string::count(pyjslib_str($widget), 'GtkTextView') > 0)) {
                        $buffer = $widget->get_buffer();
                        $sValue = $this->normalizeXML($buffer->get_text($buffer->get_start_iter(), $buffer->get_end_iter(), 1));
                    }
                    else if ((string::count(pyjslib_str($widget), 'GtkCheckButton') > 0)) {
                        $this->elog(['verify value = ', $widget->get_active()], true);
                        $sValue = $widget->get_active() ? 1 : 0;
                        $this->elog(['GtkCheckButton = ', $sValue], true);
                    }
                    else if ((string::count(pyjslib_str($widget), 'GtkRadioButton') > 0)) {
                        $sValue = pyjslib_repr($widget->get_active());
                    }
                    else if ((string::count(pyjslib_str($widget), 'GnomeDateEdit') > 0)) {
                        $newTime = time::localtime($widget->get_time());
                        $sValue = time::strftime($this->sqlDicUser['DateTimeformatString'], $newTime);
                    }
                    else if ((string::count(pyjslib_str($widget), 'GtkComboBox') > 0)) {
                        $sValue = $widget->get_active();
                    }
                    else if ((string::count(pyjslib_str($widget), 'GtkFileChooserButton') > 0)) {
                        $sValue = $widget->get_filename();
                    }
                    else {
                        $sValue = $widget->get_text();
                    }
                }
                catch(Exception $e) {
                                        $sValue = '';
                }
                $this->elog(['Value at Entry = ', $sValue], true);
                try {
                    assert(($entry->getCreateSql() == '1'));
                    $dicValues[$entry->getSqlField()] = [$sValue, $entry->getVerifyType()];
                    $this->elog(['dicValues ', $dicValues], true);
                }
                catch(Exception $e) {
                                    }
            }
            $dicValues = $this->readNonWidgetEntries($dicValues);
        }
        catch(AssertionError $e) {
                        $dicValues = null;
        }
        $dicValues['client'] = [$this->sqlDicUser['client'], 'int'];
        $dicValues = $this->readExtraEntries($dicValues);
        $dicValues = $this->verifyValues($dicValues);
        return $dicValues;
    }
    function readExtraEntries($dicValues) {
        return $dicValues;
    }
    function verifyValues($dicValues) {
        try {
            assert(($dicValues != null));
            foreach( $dicValues->keys() as $i ) {
                $oValue = $dicValues[$i][0];
                $sVerify = $dicValues[$i][1];
                if (($sVerify == 'string')) {
                    if ($oValue) {
                    }
                }
                else if (in_array($sVerify, ['int', 'integer'])) {
                    $oValue = $this->getCheckedValue($oValue, 'int');
                }
                else if (($sVerify == 'float')) {
                    $this->elog(['verify float = ', $oValue], true);
                    $oValue = $this->getCheckedValue($oValue, 'float');
                    if (($oValue == null)) {
                        $oValue = 0.0;
                    }
                    $this->elog(['verify float 2= ', $oValue], true);
                }
                else if (($sVerify == 'date')) {
                    if (($oValue == '')) {
                        $oValue = '01.01.1900';
                    }
                    else {
                        try {
                            $oDate = time::strptime($oValue, $this->sqlDicUser['DateformatString']);
                        }
                        catch(Exception $e) {
                                                        try {
                                if (($oValue->find('-') == 4)) {
                                    $oDate = time::strptime($oValue, 'YYYY-MM-DD');
                                }
                            }
                            catch(Exception $e) {
                                                                $oValue = '1900/01/01';
                            }
                        }
                        $oValue = time::strftime('%Y/%m/%d', $oDate);
                    }
                }
                $dicValues[$i][0] = $oValue;
                $dicValues[$i][1] = $sVerify;
            }
        }
        catch(AssertionError $e) {
                        $dicValues = null;
        }
        return $dicValues;
    }
    function readNonWidgetEntries($dicValues) {
        return $dicValues;
    }
    function setEntries($dicEntries01) {
        $this->dicEntries = $dicEntries01;
    }
    function getEntries() {
        return $this->dicEntries;
    }
    function setGladeXml($xml01,$win1=null) {
        $this->setXml($xml01);
        $this->win1 = $win1;
    }
    function setTreeFields($liFields01) {
         $this->elog("settreefields **********************************************************************");
        /* $this->listboxId = count($liFields01); */
        /* $this->elog(['Sifeof lifields = ', $this->listboxId, $liFields01], true); */
        /* if (($this->listboxId > 0)) { */
        /*      $this->elog(['listboxID = ', $this->listboxId, $liFields01[($this->listboxId - 1)]], true); */
        /*     if (($liFields01[($this->listboxId - 1)] != 'id')) { */
        /*         $liFields01[] = 'id'; */
        /*         $this->listboxId += 1; */
        /*     } */
        /* } */
         $this->liFields = $liFields01[1];
         
         $this->elog('lifield singleData = ' . $this->liFields);
         $this->liTreeFields = [] ;
         $number = sizeof( $this->liFields );
         for ($i = 0; $i <  $number; $i++){
              array_push( $this->liTreeFields,  $this->liFields[$i]);
              array_push( $this->liTreeFields,  $this->liFields01[2][$i]);
         }
         $this->elog('liTreeField singleData = ', json_encode($this->liTreeFields) ); 
         
    }
    function setTreeOrder($sSort01) {
        $this->sSort = $sSort01;
    }
    function setListHeader($liNames01) {
        if ($liNames01) {
            $iLen = count($liNames01);
            if (($iLen > 0)) {
                if (($liNames01[($iLen - 1)]->upper() != 'ID')) {
                    $liNames01[] = 'id';
                }
            }
            $this->listHeader['names'] = $liNames01;
            $this->elog('liNames singleData = ' . pyjslib_repr($this->listHeader));
        }
    }
    function getListEntries() {
         $liItems = [];
         $dicFields = [];
         $this->elog('dicEntries=' . json_encode($this->dicEntries) );
         $this->elog('liFields by getListEntries' . json_encode($this->liFields) );
         foreach( $this->liFields as $i ) {
              $this->elog("lifields as i = " . $i) ;
              if($this->dicEntries){
                   /* $entry = $this->dicEntries[$i]; */
                   /* if ($entry) { */
                   /*      $dicFields[$i] = $entry->getVerifyType(); */
                   /* } */
                   /* else if (($i == 'id')) { */
                   /*      $dicFields[$i] = 'int'; */
                   /* } */
                   /* else { */
                   /*      $dicFields[$i] = 'string'; */
                   /* } */
                   //$dicFields
              }
         }
         $dicFields = $this->liTreeFields ;
         $this->elog('dicFields = ', $dicFields);
         //$dicFields = [1,2,3];
        
         if ($dicFields) {
              $this->elog("getListentries for :  ".  $this->sNameOfTable );
              $this->elog("getListentries for : " . json_encode($dicFields) );
              $this->elog("getListentries for :  ".  $this->sSort );
              $dicLists = callRP('Database.getListEntries', $dicFields, $this->sNameOfTable, $this->sSort, $this->sWhere, $this->bDistinct, $this->liFields );
              $this->elog("dicLists = " . json_encode($dicLists) );

         }
         else {
              $dicLists = [];
         }
         $this->elog('dicLists =', json_encode( $dicLists) );
         /* try { */
         /*      foreach( $dicLists as $i ) { */
         /*           $liSubItems = []; */
         /*           foreach( $this->liFields as $j ) { */
         /*                $m = $this->p1->match($j); */
         /*                if ($m) { */
         /*                     $m1 = array_slice($j, $m->end(), null); */
         /*                     $j = $m1->strip(); */
         /*                } */
         /*                $zAs = $j->find(' as '); */
         /*                if (($zAs > 0)) { */
         /*                     $j = array_slice($j, ($zAs + 4), null); */
         /*                     $j = $j->strip(); */
         /*                } */
         /*                $sValue = $i[$j]; */
         /*                if (isinstance($sValue, types::UnicodeType)) { */
         /*                     $sValue = $sValue->encode($this->sCoding); */
         /*                } */
         /*                if (($j != 'id')) { */
         /*                     $entry = $this->dicEntries->getEntryByName($j); */
         /*                } */
         /*                $liSubItems[] = $sValue; */
         /*           } */
         /*           $liItems[] = $liSubItems; */
         /*      } */
         /* } */
         /* catch(Exception $e) { */
         /* } */
         $_SESSION['liTreeEntries'] =  $dicLists;
         $this->liItems = $liItems;
         return $liItems;
    }
    function newRecord() {
        $this->ID = -1;
        $this->setEmptyEntries();
    }
    function isNewRecord() {
        $ok = false;
        if (($this->ID == -1)) {
            $ok = true;
        }
        return $ok;
    }
    function setEmptyEntries() {
        $this->clearAllFields();
        $this->setOtherEmptyEntries();
    }
    function setOtherEmptyEntries() {
    }
    function getStaffID($dicUser,$setBraces=true) {
        $sSql = '';
        if ($setBraces) {
            $sSql .= '(';
        }
        $sSql .= 'select id from staff where staff.cuon_username = \'' . $dicUser['Name'] . '\' and staff.client = ' . pyjslib_repr($dicUser['client']);
        if ($setBraces) {
            $sSql .= ') ';
        }
        else {
            $sSql .= ' ';
        }
        return $sSql;
    }
}


?>