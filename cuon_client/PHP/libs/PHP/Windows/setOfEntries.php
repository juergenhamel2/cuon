<?php


class setOfEntries {
    function __construct() {
        $this->nameOfSet = 'EMPTY';
        $this->EntrySet = [];
        $this->iOldTab = 0;
        $this->xml = null;
        $this->za1 = 0;
    }
    function setXml($xml) {
        $this->xml = $xml;
    }
    function setName($s) {
        $this->nameOfSet = $s;
    }
    function getName() {
        return $this->nameOfSet;
    }
    function addEntry($entry) {
        $this->EntrySet[] = $entry;
    }
    function getEntryAtIndex($i) {
        return $this->EntrySet[$i];
    }
    function getCountOfEntries() {
        return count($this->EntrySet);
    }
    function getEntryByName($sName) {
        $entry = null;
        foreach( pyjslib_range($this->getCountOfEntries()) as $i ) {
            $entry = $this->getEntryAtIndex($i);
            if (($entry->getSqlField() == $sName)) {
                return $entry;
            }
        }
        return;
    }
}

