<?php


/*
 * @author Jürgen Hamel
 * @license GPL V3
 * @version 0.2
coding=utf-8
Copyright (C) [2003-2018]  [Jürgen Hamel, D-32584 Löhne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

*/
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/gladexml.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');


class dataEntry {

     public   $nameOfEntry="EMPTY";
     public   $typeOfEntry="EMPTY";
     public   $bDuty = False;
     public   $iRound = 0;
     public   $nextWidget = 'EMPTY';


     
    public function setName($s){
          $nameOfEntry = $s;
     }

    public function getName(){
         return $nameOfEntry;
    }



    public function setType($s){
         $typeOfEntry = $s;
    }



    public function getType(){
         return $typeOfEntry;
    }


    public function setSizeOfEntry($s){
         $sizeOfEntryOfEntry = $s;
    }

    
    public function getSizeOfEntry(){
         return $sizeOfEntryOfEntry;
    }


    public function setVerifyType($s){
         $verifyType = $s;
    }
    public function getVerifyType(){
         return $verifyType;
    }

    public function setCreateSql($s){
         $createSql = $s;
    }

    public function getCreateSql(){
         return $createSql;
    }


    public function setSqlField($s){
         $sqlFieldOfEntry = $s;
    }

    public function getSqlField(){
         return $sqlFieldOfEntry.encode("utf-8");
    }


    public function setBgColor($Color){
         $bgColor = $sColor;
    }

      
    public function getBgColor(){
         return $bgColor;
    }
    

    public function setFgColor($sColor){
         $fgColor = $sColor;
    }
      
    public function getFgColor(){
         return $fgColor;
    }
    public function setDuty( $bDuty = False){
         if ($bDuty == 'EMPTY'){
              $bDuty = False;
         }
            
         $bDuty = $bDuty;
         
    }
    
    public function getDuty(){
         return $bDuty;
    }
    

    public function setRound($Round){
        $iRound1 = -1;
        if ($sRound != 'EMPTY'){
             try{
                  $iRound1 = int($sRound);
             }
             catch( Exception $e) {
                  echo 'Exception abgefangen: ',  $e->getMessage(), "\n";
                  
             }
                          
        } 
        $iRound = $iRound1;
    }
        
    public function getRound(){
         return $iRound;
    }
    
    public function setNextWidget($sName){
         $nextWidget = $sName;
    }
        
    public function getNextWidget(){ 
         return $nextWidget;
    }
        
        



     


}