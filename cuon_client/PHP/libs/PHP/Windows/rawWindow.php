<?php


include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/gladexml.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/dataEntry.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/cyr_load_entries.php');




class rawWindow extends gladeXml {
    function __construct($servermod=false) {
        gladeXml::__construct($servermod);
        $this->oUser = null;
        $this->dicUser = null;
        $this->dicSqlUser = null;
        $this->dicInternetUser = null;
        $this->dicUserKeys = null;
        /*$this->openDB();
        $this->loadUserInfo();
        try {
            $this->DEBUG = $this->oUser->getDebug();
        }
        catch(Exception $e) {
                       print_r('No User Debug found');
        }
        $this->closeDB();

        $this->cpParser = ConfigParser::ConfigParser();*/
        
    }
    function loadUserInfo() {
        $this->oUser = $this->loadObject('User');
        if ($this->oUser) {
            $this->dicUser = $this->oUser->getDicUser();
            $this->dicSqlUser = $this->oUser->getSqlDicUser();
            $this->dicInternetUser = $this->oUser->getInternetUser();
            $this->debug($this->oUser);
        }
        else {
            $this->dicUser = [];
        }
        try {
            $this->dicUserKeys = $this->oUser->getDicUserKeys();
        }
        catch(Exception $e) {
                       print_r([$Exception, $params], true);
        }
    }
    function getConfigOption($section,$option) {
        $value = null;
        if ($this->cpParser->has_option($section, $option)) {
            $value = $this->cpParser->get($section, $option);
            $value = $value->strip();
        }
        return $value;
    }
    function getListOfParserItems($section) {
        return $this->cpParser->items($section);
    }
    function getListOfParserSections() {
        return $this->cpParser->sections();
    }
}

