 
<?php
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/rawWindow.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/gladexml.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/dataEntry.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/cyr_load_entries.php');

/*try {

   require_once( 'cuon.Misc.messages.php');}
catch(Exception $e) {
    }
*/




class windows extends rawWindow {
    /* PHP does not support multiple inheritance.
     * Consider defining the referenced classes as traits instead.
     * See: http://php.net/manual/en/language.oop5.traits.php
     */
    //use rawWindow, MyXML, messages, constants {
    // }
    
    function __construct($servermod=false) {
         // constants::__construct();
        rawWindow::__construct($servermod);
        $this->Find = false;
        $this->lastTab = -1;
        $this->servermod = $servermod;
        //cuon_xmlrpc::__construct();
        //messages::__construct();
        $this->Search = false;
        $this->cursor = null;
        $this->FirstInit = true;
        $this->ModulNumber = 1;
        $this->AutoInsert = false;
        $this->ControlKey1 = null;
        $this->ControlKey2 = null;
        $this->OrderStatus = [];
        $this->OrderStatus['EnquiryStart'] = 100;
        $this->OrderStatus['EnquiryEnd'] = 199;
        $this->OrderStatus['ProposalStart'] = 300;
        $this->OrderStatus['ProposalEnd'] = 399;
        $this->OrderStatus['OrderStart'] = 500;
        $this->OrderStatus['OrderEnd'] = 799;
        $this->doEdit = -1;
        $this->noEdit = -1;
        $this->oldTab = -1;
        $this->sWhereStandard = '';
        $this->sWhereSearch = null;
        $this->sepInfo = [];
        //$this->checkClient();
        $this->liSearchFields = [];
        $this->testV = 'Hallo';
        //$this->rpc = $cuon->XMLRPC->xmlrpc->myXmlRpc();
        $this->DataEntries = new dataEntry();
        $this->tabOption = 0;
        $this->tabOption2 = 100;
        $this->tabOption3 = 1000;
        $this->tabOption4 = 10000;
        $this->actualEntries = null;
        $this->editAction = 'closeMenuItemsForEdit';
        $this->editEntries = false;
        $this->dicKeys = [];
        /*$this->dicKeys['CTRL'] = $gtk->gdk->CONTROL_MASK;
        $this->dicKeys['SHIFT'] = $gtk->gdk->SHIFT_MASK;
        $this->dicKeys['ALT'] = $gtk->gdk->MOD1_MASK;*/
        $this->dicKeys['NONE'] = 0;
        $this->NameOfTree = 'tree1';
        $this->sb_id = null;
    }
   function closeWindow() {
        $this->saveDataQuestion();
        $this->CleanUp();
        $this->win1->hide();
    }
    function CleanUp() {
       $this->elog('CleanUp');
    }
    function setWinProperty($Main=false) {
        if ($Main) {
            try {
                if ($this->dicUser['prefWindow']['MainMaximize']) {
                    $this->win1->maximize();
                }
            }
            catch(Exception $e) {
                            }
        }
    }
    function delete_event($widget,$event,$data=null) {
       $this->elog('delete event occurred');
        $this->closeWindow();
        return true;
    }
    function openWindow() {
        $this->win1->show();
        return false;
    }
    function setWinAccelGroup() {
        if ($this->win1) {
           $this->elog('add accelGroup');
            try {
                $this->win_accel_group->connect_group(ord('f'), $gtk->gdk->CONTROL_MASK, $gtk->ACCEL_VISIBLE, $this->startFind);
                $this->win1->add_accel_group($this->win_accel_group);
            }
            catch(Exception $e) {
                                $this->win_accel_group->connect(ord('f'), $gtk->gdk->CONTROL_MASK, $gtk->ACCEL_VISIBLE, $this->startFind);
                $this->win1->add_accel_group($this->win_accel_group);
            }
        }
    }
    function startFind(...$args) {
       $this->elog('Find something');
        $liStatus = commands::getstatusoutput('tracker-search-tool');
        return true;
    }
    function checkClient() {
        if (!($this->dicUser->has_key('client')) || !(($this->dicUser['client'] > 0))) {
            $this->dicUser = [];
        }
    }
    function clearSearch($event) {
       $this->elog('Clear Search activate');
        $this->sWhereSearch = null;
        $this->tabChanged();
    }
    function activateClick($sItem,$event=null,$sAction='activate') {
       $this->elog(['activate click', $sItem], true);
        $item = $this->getWidget($sItem);
       $this->elog(['item = ', $item], true);
        if ($item) {
            $item->emit($sAction);
        }
    }
    function setDataEntries($sName,$dicDE) {
        $this->DataEntries[$sName] = $dicDE;
    }
    function getDataEntries($sName) {
        return $this->DataEntries[$sName];
    }
 
    function readSearchDatafields($dicWidgets) {
        $dicSearchfields = [];
        foreach( $dicWidgets->keys() as $key ) {
             //$this->elog($key);
            $text = $this->getWidget($dicWidgets[$key])->get_text();
            if ($text) {
                $dicSearchfields[$key] = $text;
            }
        }
        return $dicSearchfields;
    }
    function setEntriesEditable($sName,$ok) {
        $this->actualEntries = $sName;
        $entries = $this->getDataEntries($sName);
        if($entries != Null){
        foreach( $entries->getCountOfEntries() as $i ) {
            $entry = $entries->getEntryAtIndex($i);
            $this->elog('entry = ' . $entry->getName());
            $e1 = $this->getWidget($entry->getName());
            if ($e1) {
                try {
                    $e1->set_editable($ok);
                }
                catch(Exception $e) {
                                    }
            }
        }
        }
        if ($ok) {
            $this->closeMenuEntries();
        }
    }
    function saveCursor() {
        $this->cursor = $this->win1->get_cursor();
    }
    function setEditCursor() {
        $this->saveCursor();
        $this->win1->set_cursor($gtk->gdk->PENCIL);
    }
    function restoreCursor() {
        if ($this->cursor) {
            $this->win1->set_cursor($this->cursor);
        }
    }
    function startEdit() {
    }
    function endEdit() {
    }
    function setPageVisible($sNotebook,$page_num,$visible=true) {
        $nb = $this->getWidget($sNotebook);
    }
    function on_notebook1_switch_page($notebook1,$page,$page_num) {
        if (($this->doEdit > $this->noEdit)) {
            if ($this->QuestionMsg('Unsaved Data ! Wish you save them ?')) {
                $this->saveData();
                $this->doEdit = $this->noEdit;
            }
            else {
                $this->doEdit = $this->noEdit;
                $this->endEdit();
            }
        }
        $this->tabOption = $page_num;
        $this->tabChanged();
    }
    function on_notebook2_switch_page($notebook2,$page,$page_num) {
        if (($this->doEdit > $this->noEdit)) {
            if ($this->QuestionMsg('Unsaved Data ! Wish you save them ?')) {
                $this->saveData();
                $this->doEdit = $this->noEdit;
            }
            else {
                $this->doEdit = $this->noEdit;
                $this->endEdit();
            }
        }
        $this->tabOption2 = ($page_num + 100);
        $this->tabChanged();
    }
    function on_notebook3_switch_page($notebook3,$page,$page_num) {
        if (($this->doEdit > $this->noEdit)) {
            if ($this->QuestionMsg('Unsaved Data ! Wish you save them ?')) {
                $this->saveData();
                $this->doEdit = $this->noEdit;
            }
            else {
                $this->doEdit = $this->noEdit;
                $this->endEdit();
            }
        }
        $this->tabOption3 = ($page_num + 1000);
        $this->tabChanged();
    }
    function on_notebook4_switch_page($notebook4,$page,$page_num) {
        if (($this->doEdit > $this->noEdit)) {
            if ($this->QuestionMsg('Unsaved Data ! Wish you save them ?')) {
                $this->saveData();
                $this->doEdit = $this->noEdit;
            }
            else {
                $this->doEdit = $this->noEdit;
                $this->endEdit();
            }
        }
        $this->tabOption4 = ($page_num + 10000);
        $this->tabChanged();
    }
    function saveData() {
    }
    function closeMenuEntries($sendEntry=null,$event=null) {
        if (($this->editEntries == false)) {
            $this->editEntries = true;
            $this->disableMenuItem($this->editAction);
        }
    }
    function setStatusBar($sVbox='vbox1') {
       $this->elog('###----> Statusbar <----###');
        $this->statusbar = $gtk->Statusbar();
        $vbox = $this->getWidget($sVbox);
        if ($vbox) {
           $this->elog('###----> vbox exist');
            try {
                 //callProc_kwargs_method_call($vbox, 'pack_end', [$this->statusbar], ["expand" => false]);
            }
            catch(Exception $e) {
                                $vbox->pack_end($this->statusbar, 0, 0, 0);
            }
            $this->sb_id = $this->statusbar->get_context_id('general_info');
            $this->statusbar->show();
        }
        else {
           $this->elog('###----> vbox do not exist <---###');
        }
    }
    function setStatusbarText($liText) {
       $this->elog(['text for the statusbar = ', $liText], true);
        $this->statusbar->push($this->sb_id, $liText[0]);
    }
    function setTreeVisible($ok) {
        try {
            if ($ok) {
                $this->getWidget($this->NameOfTree)->set_sensitive(true);
            }
            else {
                $this->getWidget($this->NameOfTree)->set_sensitive(false);
            }
        }
        catch(Exception $e) {
                    }
    }
    function startProgressBar($Pulse=false,$Title=null) {
        if ($this->servermod) {
            try {
                $this->progressbarWindowXML = $gtk->glade->XML('../usr/share/cuon/glade/sqlprogressbar.glade2');
            }
            catch(Exception $e) {
                                $this->progressbarWindowXML = $gtk->Builder();
                $this->progressbarWindowXML->add_from_file('../usr/share/cuon/glade/sqlprogressbar.glade2');
            }
        }
        else {
            try {
                 //$fname = os::path::normpath($this->cuon_path . '/' . 'glade_sqlprogressbar.xml');
                 $fname = ($this->cuon_path . '/' . 'glade_sqlprogressbar.xml');
                $this->progressbarWindowXML = $gtk->glade->XML($fname);
            }
            catch(Exception $e) {
                                try {
                    $this->progressbarWindowXML = $gtk->Builder();
                    $fname = ( '/cuon/' . 'glade_sqlprogressbar.xml');
                   $this->elog(['fname = ', $fname], true);
                    $this->progressbarWindowXML->add_from_file($fname);
                }
                catch(Exception $e) {
                                       $this->elog([$Exception, $params], true);
                }
            }
        }
        try {
            $this->PBW = $this->progressbarWindowXML->get_widget('SqlProgressBar');
            if ($Title) {
                $this->PBW->set_title($Title);
            }
            $this->PBW->show();
            $this->progressbar = $this->progressbarWindowXML->get_widget('progressbar1');
            if ($Pulse) {
                $this->progressbar->pulse();
            }
            $this->progressbar->show();
        }
        catch(Exception $e) {
                       $this->elog([$Exception, $params], true);
            try {
                $this->PBW = $this->progressbarWindowXML->get_object('SqlProgressBar');
               $this->elog(['self.PBW  = ', $this->PBW], true);
                if ($Title) {
                    $this->PBW->set_title($Title);
                }
                $this->PBW->show();
                $this->progressbar = $this->progressbarWindowXML->get_object('progressbar1');
                if ($Pulse) {
                    $this->progressbar->pulse();
                }
                $this->progressbar->show();
            }
            catch(Exception $e) {
                               $this->elog([$Exception, $params], true);
            }
        }
        return true;
    }
    function stopProgressBar() {
        try {
            $this->progressbarWindowXML->get_widget('SqlProgressBar')->hide();
        }
        catch(Exception $e) {
                        $this->progressbarWindowXML->get_object('SqlProgressBar')->hide();
        }
    }
    function setProgressBar($fPercent,$Pulse=false) {
        try {
            if ($this->progressbar) {
                if ($Pulse) {
                    $this->progressbar->pulse();
                }
                else {
                    if (($fPercent > 100)) {
                        $fPercent = 0;
                    }
                    $this->progressbar->set_fraction(($fPercent / 100.0));
                    while ($gtk->events_pending()) {
                        $gtk->main_iteration(false);
                    }
                }
            }
            else {
                 //$this->elog('no progressbar');
            }
        }
        catch(Exception $e) {
                    }
        return true;
    }
    function checkKey($event,$sState,$cKey,$cKey2=null,$cKey3=null) {
        $ok = false;
        $cKeyR1 = null;
        $cKeyR2 = null;
        $cKeyR3 = null;
        if (($cKey == 'Return')) {
            $cKeyR1 = 'KP_Enter';
            $cKeyR2 = 'ISO_Enter';
        }
        if ($event->state && $this->dicKeys[$sState] || ($sState == 'NONE')) {
            $this->elog('state is found');
            if (($gtk->gdk->keyval_name($event->keyval) == $cKey)) {
                $ok = true;
            }
            else if ($cKey2 && ($gtk->gdk->keyval_name($event->keyval) == $cKey2)) {
                $ok = true;
            }
            else if ($cKey3 && ($gtk->gdk->keyval_name($event->keyval) == $cKey3)) {
                $ok = true;
            }
            else if ($cKeyR1 && ($gtk->gdk->keyval_name($event->keyval) == $cKeyR1)) {
                $ok = true;
            }
            else if ($cKeyR2 && ($gtk->gdk->keyval_name($event->keyval) == $cKeyR2)) {
                $ok = true;
            }
            else if ($cKeyR3 && ($gtk->gdk->keyval_name($event->keyval) == $cKeyR3)) {
                $ok = true;
            }
        }
        return $ok;
    }
    function getDate($entry) {
        $newTime = time::strptime($entry->get_text(), $this->dicSqlUser['DateformatString']);
        return $newTime;
    }
    function setDate($entry,$newTime) {
        $entry->set_text(time::strftime($this->dicSqlUser['DateformatString'], $newTime));
    }
    function addOneDay($entry,$event) {
        $oldTime = $this->getDate($entry);
        //$aDay = callProc_kwargs_function_call('datetime::timedelta', [], ["days" => 1]);
        $newTime = (datetime::datetime($oldTime[0], $oldTime[1], $oldTime[2]) + $aDay);
        $this->setDate($entry, $newTime->timetuple());
    }
    function removeOneDay($entry,$event) {
        $oldTime = $this->getDate($entry);
        //$aDay = callProc_kwargs_function_call('datetime::timedelta', [], ["days" => -1]);
        $newTime = (datetime::datetime($oldTime[0], $oldTime[1], $oldTime[2]) + $aDay);
        $this->setDate($entry, $newTime->timetuple());
    }
    function getWhere($args) {
        $sWhere = null;
        $args->reverse();
        $firstWhere = true;
        while ($args) {
            $s = $args->pop();
            $v = $args->pop();
            if ($s) {
                if ((count($s) > 2) && (array_slice($s, 0, 3 - 0) == '###')) {
                    if ($firstWhere) {
                        $sWhere = ' where ' . array_slice($s, 3, null);
                        $firstWhere = false;
                    }
                    else {
                        $sWhere = $sWhere . ' and ' . array_slice($s, 3, null);
                    }
                }
                else if (isinstance($v, types::BooleanType)) {
                    $this->elog('sWhere = ', $v);
                    if ($firstWhere) {
                        $sWhere = ' where ' . $s . ' = ' .($v);
                        $firstWhere = false;
                    }
                    else {
                        $sWhere = $sWhere . ' and ' . $s . ' = ' .($v);
                    }
                }
                else if (isinstance($v, types::IntType)) {
                    $this->elog('sWhere = ', $v);
                    if ($firstWhere) {
                        $sWhere = ' where ' . $s . ' ' .($v);
                        $firstWhere = false;
                    }
                    else {
                        $sWhere = $sWhere . ' and ' . $s . ' ' .($v);
                    }
                }
                else if (($v[0] == '#')) {
                    $v = array_slice($v, 1, null);
                    $v = $v->replace('?', '~');
                    $v = $v->replace('>', '^');
                    if ($firstWhere) {
                        $sWhere = ' where ' . $s . ' ' . $v;
                        $firstWhere = false;
                    }
                    else {
                        $sWhere = $sWhere . ' and ' . $s . ' ' . $v;
                    }
                }
                else {
                    if ($firstWhere) {
                        $sWhere = ' where ' . $s . ' ~* \'.*' . $v . '.*\'';
                        $firstWhere = false;
                    }
                    else {
                        $sWhere = $sWhere . ' and ' . $s . ' ~* \'.*' . $v . '.*\'';
                    }
                }
            }
        }
        return $sWhere;
    }
    /**
     * sets the property (colors,etc.) of an gtk-widget
     * @param sName: The name of the widget
     */
    function setProperties($sName) {
         $this->elog('entries for property  = ' . $sName);
         $entries = $this->getDataEntries($sName);
         if($entries != Null){
         try{
              foreach( range(0,$entries->getCountOfEntries()) as $i ) {
                   $entry = $entries->getEntryAtIndex($i);
                   $this->elog('entry = ' . $entry->getName());
                   if ($entry) {
                        $e1 = $this->getWidget($entry->getName());
                        if ($entry->getDuty()) {
                             try {
                                  $e1->set_style($this->getStyle('duty', 'entry', $entry->getFgColor(), $entry->getBgColor()));
                             }
                             catch(Exception $e) {
                             }
                        }
                        else {
                             try {
                                  $e1->set_style($this->getStyle('standard', 'entry', $entry->getFgColor(), $entry->getBgColor()));
                             }
                             catch(Exception $e) {
                                  $this->elog('Style info error');
                             }
                        }
                   }
              }
         }
         catch(Exception $e) {
              $this->elog('error' . $e); 
         }
         }
         $this->setEntriesEditable($sName, false);
    }
    /**
     * define an new Style for gtk-widgets
     * thanks to
     * /-----------------------------------------------------------------------        | Tony Denault                     | Email: denault@irtf.ifa.hawaii.edu |
     * | Institute for Astronomy          |              Phone: (808) 932-2378 |
     * | 640 North Aohoku Place           |                Fax: (808) 933-0737 |
     * | Hilo, Hawaii 96720               |                                    |
     * \-----------------------------------------------------------------------/
     * @param cSType: a switch for the applied item ( ex. duty )
     * 
     * f = foreground
     * b = background
     * l = light
     * d = dark
     * m = mid
     * t = text
     * s = base
     * 
     * 
     * @return: the new style
     */
    function getStyle($cType,$cWidget,$numberFG,$numberBG) {
        $defaultStyle = $this->win1->get_style();
        $map = $this->win1->get_colormap();
        $pColor = $this->dicUser['prefColor'];
        $colorFG = $map->alloc_color(0, 0, 0);
        $colorBG = $map->alloc_color((255 * 255), (255 * 255), (255 * 255));
        $colorBase = $map->alloc_color((130 * 255), (180 * 255), (220 * 255));
        $colorText = $map->alloc_color((180 * 255), (180 * 255), (70 * 255));
        $colorLight = $map->alloc_color('#FF9999');
        $colorMid = $map->alloc_color('#A199D1');
        $colorDark = $map->alloc_color('#FFEEEE');
        $newStyle = $defaultStyle->copy();
        try {
            $sTypeBG = 'BG';
            $sTypeFG = 'FG';
            if (($cType == 'duty')) {
                $sTypeBG = 'DUTY_BG';
                $sTypeFG = 'DUTY_FG';
            }
            if (($numberBG[0] == '#')) {
                $colorBase = $map->alloc_color($numberBG);
            }
            else {
                $colorBase = $map->alloc_color($pColor[$sTypeBG]);
            }
            if (($numberFG[0] == '#')) {
                $colorText = $map->alloc_color($numberFG);
            }
            else {
                $colorText = $map->alloc_color($pColor[$sTypeFG]);
            }
            /*
            entry : s (base) for background, t ( text) for foreground
            button: b (BG) for background, f (FG) for foreground
            */
            $cSwitch = [];
            if (($cType == 'duty')) {
                if (($cWidget == 'entry')) {
                    $cSwitch[] = 's';
                    $cSwitch[] = 't';
                    $cSwitch[] = 'l';
                    $cSwitch[] = 'd';
                    $cSwitch[] = 'm';
                }
            }
            else {
                if (($cWidget == 'entry')) {
                    $cSwitch[] = 's';
                    $cSwitch[] = 't';
                    $cSwitch[] = 'l';
                    $cSwitch[] = 'd';
                    $cSwitch[] = 'm';
                }
            }
            foreach( $cSwitch as $cs ) {
                if (($cs == 'f')) {
                     foreach(range(0,5) as $i ) {
                        $newStyle->fg[$i] = $colorFG;
                    }
                }
                else if (($cs == 'b')) {
                    foreach( 5 as $i ) {
                        $newStyle->bg[$i] = $colorBG;
                    }
                }
                else if (($cs == 's')) {
                     foreach( range(0,5) as $i ) {
                        $newStyle->base[$i] = $colorBase;
                    }
                }
                else if (($cs == 'l')) {
                     foreach( range(0,5) as $i ) {
                        $newStyle->light[$i] = $colorLight;
                    }
                }
                else if (($cs == 'd')) {
                     foreach( range(0,5) as $i ) {
                        $newStyle->dark[$i] = $colorDark;
                    }
                }
                else if (($cs == 'm')) {
                     foreach( range(0,5) as $i ) {
                        $newStyle->mid[$i] = $colorMid;
                    }
                }
                else if (($cs == 't')) {
                    foreach( range(0,5) as $i ) {
                        $newStyle->text[$i] = $colorText;
                    }
                }
            }
        }
        catch(Exception $e) {
                        $newStyle = $defaultStyle->copy();
        }
        return $newStyle;
    }
    function loadProfile($sProfile=null) {
        if (!($sProfile)) {
            $this->elog($this->oUser->getDicUser()['Name']);
            $sProfile = $this->rpc->callRP('User.getNameOfStandardProfile', $this->oUser->getDicUser());
            $this->elog('Profile = ');
            $this->elog($sProfile);
        }
        $this->elog('-----------------------------------------------------------------------------------');
        $this->elog('Profile = ', $sProfile);
        if ($sProfile) {
            $result = $this->rpc->callRP('User.getStandardProfile', $sProfile, $this->oUser->getDicUser());
            $this->elog('Result Profile');
            if (!in_array($result, ['NONE', 'ERROR'])) {
                $this->oUser->userLocales = 'de';
                if ($result->has_key('encoding')) {
                    $this->oUser->userEncoding = $result['encoding'];
                }
                $this->oUser->userPdfEncoding = 'latin-1';
                $this->oUser->serverAddress = null;
                $this->oUser->userSQLDateFormat = 'DD.MM.YYYY';
                $this->oUser->userSQLTimeFormat = 'HH24:MI';
                $this->loadUserFromPreferences($result);
            }
        }
        else {
            $this->elog('no standard-Profile defined');
        }
        $this->elog('__________________________________________________________');
        $this->elog('Profile loaded');
        $this->elog($this->oUser->getDicUser());
        $this->elog('__________________________________________________________');
    }
    function setDateToEntry($event,$entryName) {
        $ok = false;
        try {
           $this->elog($event);
            $t0 = $event->get_date();
           $this->elog($t0);
            $t1 =($t0[0]) . ' ' .(($t0[1] + 1)) . ' ' .($t0[2]);
           $this->elog($t1);
            $t2 = time::localtime(time::mktime(time::strptime($t1, '%Y %m %d')));
            $sTime = time::strftime($this->dicUser['DateformatString'], $t2);
           $this->elog($sTime);
            $eDate = $this->getWidget($entryName);
            $eDate->set_text($sTime);
            $ok = true;
        }
        catch(Exception $e) {
                    }
        return $ok;
    }
    function setDateToCalendar($sDate,$entryName) {
        $ok = false;
        try {
            $Cal = $this->getWidget($entryName);
            $dt = time::strptime($sDate, $this->dicUser['DateformatString']);
           $this->elog(['Date', $dt], true);
           $this->elog($dt[0]);
           $this->elog($dt[1]);
            Cal::select_month(($dt[1] - 1), $dt[0]);
            Cal::select_day($dt[3]);
        }
        catch(Exception $e) {
                    }
        return $ok;
    }
    function on_eSearch_key12_press_event($entry,$event) {
       $this->elog('eSearch_key_press_event');
        if ($this->checkKey($event, 'NONE', 'Return')) {
            $this->preFindData();
            $this->findData();
        }
    }
    function preFindData() {
    }
    function findData($singleDataDB) {
       $this->elog(['find', $this->liSearchFields], true);
        $liSearch = [];
        foreach( $this->liSearchFields as $liValue ) {
            if (($liValue[1] == 'STRING')) {
                $sText = $this->getWidget($liValue[0])->get_text();
                if ($sText) {
                    $liSearch[] = $liValue[2]->replace('%FIELD%', $sText);
                    $liSearch[] = $sText;
                }
            }
            if (($liValue[1] == 'INT')) {
                $sText = $this->getWidget($liValue[0])->get_text();
                if ($sText) {
                    $newID = '';
                    $newOp = '';
                    try {
                        $sID = $sText->strip();
                        foreach( $sID as $c1 ) {
                            if ($c1->isdigit()) {
                                $newID += $c1;
                            }
                            else {
                                $newOp += $c1;
                            }
                        }
                        if (!($newOp)) {
                            $newOp = '=';
                        }
                        $liSearch[] = $liValue[2]->replace('%FIELD%', $sText) . ' ' . $newOp;
                        $liSearch[] = pyjslib_int($newID);
                    }
                    catch(Exception $e) {
                                                $liSearch[] = 'id =';
                        $liSearch[] = 0;
                    }
                }
            }
            if (($liValue[1] == 'CHECKBOX')) {
                $bok = $this->getWidget($liValue[0])->get_active();
               $this->elog(['bok = ', $bok], true);
                if (($bok > -1)) {
                    $liSearch[] = $liValue[2];
                    $liSearch[] = 0;
                }
            }
           $this->elog(['liSearch = ', $liSearch], true);
        }
        $this->postFindData();
        if ($liSearch) {
            $this->singleOrder->sWhere = $this->getWhere($liSearch);
        }
        $this->oldTab = -1;
        $this->refreshTree();
    }
    function postFindData() {
    }
    function on_key_press_event($oEntry,$data) {
        if (($gtk->gdk->keyval_name($data->keyval) == 'Return')) {
            $sEntryName = $oEntry->get_name();
            $entries = $this->getDataEntries($this->actualEntries);
            foreach( range(0,0, $entries->getCountOfEntries()) as $i ) {
                $entry = $entries->getEntryAtIndex($i);
                if (($entry->getName() == $sEntryName)) {
                    try {
                        $sNextWidget = $entry->getNextWidget();
                        if (($sNextWidget == 'LAST')) {
                            $this->saveDataQuestion();
                        }
                        else {
                            $this->getWidget($sNextWidget)->grab_focus();
                        }
                    }
                    catch(Exception $e) {
                                            }
                }
            }
        }
    }
    function saveDataQuestion() {
        $this->saveData();
        $this->doEdit = $this->noEdit;
    }
    function loadUserFromPreferences($result) {
        $this->oUser = $this->oUser->getUser($result);
        $this->openDB();
        $this->saveObject('User', $this->oUser);
        $this->closeDB();
    }
    /**
     * convert an (R, G, B) tuple to #RRGGBB
     */
    function RGBToHTMLColor($rgb_tuple) {
        $hexcolor = sprintf('#%04x%04x%04x', $rgb_tuple);
        return $hexcolor;
    }
    /**
     * convert #RRGGBB to an (R, G, B) tuple
     */
    function HTMLColorToRGB($colorstring) {
        $colorstring = $colorstring->strip();
        if (($colorstring[0] == '#')) {
            $colorstring = array_slice($colorstring, 1, null);
        }
        if ((count($colorstring) != 6)) {
            throw new ValueError;
        }
        list($r, $g, $b) = [array_slice($colorstring, null, 2), array_slice($colorstring, 2, 4 - 2), array_slice($colorstring, 4, null)];
        //list($r, $g, $b) = ????;
        return [$r, $g, $b];
    }
    function setColor2Text($event,$sEntry) {
        $Color = $event->get_color();
        $eColor = $this->getWidget($sEntry);
        $eColor->set_text($this->RGBToHTMLColor([$Color->red, $Color->green, $Color->blue]));
    }
    function on_Mainwindow_key_press_event($oEntry,$data) {
        $this->MainwindowEventHandling($oEntry, $data);
    }
    function MainwindowEventHandling($oEntry,$data) {
        $sKey = $gtk->gdk->keyval_name($data->keyval);
        if (in_array($sKey, $this->ControlKeys)) {
            if ($this->ControlKey1) {
                $this->ControlKey2 = $sKey;
            }
            else {
                $this->ControlKey1 = $sKey;
            }
        }
        else {
            $this->fetchMainwindowKeys($sKey);
            $this->setMainwindowNotebook($sKey);
        }
    }
    function fetchMainwindowKeys($sKey) {
    }
    function setMainwindowNotebook($sKey) {
        $page_num = -1;
        $addPage = 0;
        if (in_array($this->ControlKey1, $this->ControlKeysSuper)) {
            $addPage = 11;
        }
        if (($sKey == 'F1')) {
            $page_num = (0 + $addPage);
        }
        else if (($sKey == 'F2')) {
            $page_num = (1 + $addPage);
        }
        else if (($sKey == 'F3')) {
            $page_num = (2 + $addPage);
        }
        else if (($sKey == 'F4')) {
            $page_num = (3 + $addPage);
        }
        else if (($sKey == 'F5')) {
            $page_num = (4 + $addPage);
        }
        else if (($sKey == 'F6')) {
            $page_num = (5 + $addPage);
        }
        else if (($sKey == 'F7')) {
            $page_num = (6 + $addPage);
        }
        else if (($sKey == 'F8')) {
            $page_num = (7 + $addPage);
        }
        else if (($sKey == 'F9')) {
            $page_num = (8 + $addPage);
        }
        else if (($sKey == 'F11')) {
            $page_num = (9 + $addPage);
        }
        else if (($sKey == 'F12')) {
            $page_num = (10 + $addPage);
        }
        if (($page_num >= 0)) {
            $notebook = $this->getWidget('notebook1');
            if ($notebook) {
                try {
                    $notebook->set_current_page($page_num);
                    $gtk->gdk->beep();
                }
                catch(Exception $e) {
                                    }
            }
        }
        $this->ControlKey1 = null;
        $this->ControlKey2 = null;
        return true;
    }
    function addDateTime($firstRecord) {
        $dicTime = $this->getActualDateTime();
        if ($dicTime) {
            foreach( $dicTime as $key ) {
                $firstRecord['date_' . $key] = $dicTime[$key];
            }
        }
        return $firstRecord;
    }
    function setArticlePrice($sModul,$sArtikelwidget,$sPricewidget,$singleID) {
        try {
            $fPrice = float($this->getWidget($sPricewidget)->get_text()->replace(',', '.'));
        }
        catch(Exception $e) {
                       $this->elog('no correct price');
            $fPrice = 0.0;
        }
       $this->elog(['fPrice = ', $fPrice], true);
        if (!($fPrice)) {
            $fPrice = $this->singleArticle->getPrice($sModul, $singleID, pyjslib_int($this->getWidget($sArtikelwidget)->get_text()));
        }
       $this->elog(['Price = ', $fPrice, $this->getCheckedValue($fPrice, 'toLocaleString')], true);
        $this->getWidget($sPricewidget)->set_text($this->getCheckedValue($fPrice, 'toLocaleString')->replace('\'', ''));
       $this->elog(['widget = ', $this->getWidget($sPricewidget)->get_text()], true);
        return true;
    }
}


?>
