<?php

require_once( 'dataEntry.php');
require_once( 'setOfEntries.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/Databases/dumps.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');


class cyr_load_entries extends dumps {
    /* py2php : PHP does not support multiple inheritance.
     * Consider defining the referenced classes as traits instead.
     * See: http://php.net/manual/en/language.oop5.traits.php
     */

    function __construct() {

        

        $this->configPath = '/etc/cuon/';
    }

    
    function getListOfEntriesNames($sFile) {
        $allLists = [];
        $doc = $this->getEntriesDescription($sFile);
        if ($doc) {
            $allLists = $doc->getElementsByTagName('table');
        }
        return $allLists;
    }
    function getEntriesDescription($sFile) {
        return $this->readDocument($this->td->nameOfXmlEntriesFiles[$sFile]);
    }
    function getEntriesDefinition($sFile,$sNameOfTable,$sNameOfEntries) {
        $doc = $this->getEntriesDescription($sFile);
        $this->out($doc->toxml());
        $cyRootNode = $this->getRootNode($doc);
        $this->out($cyRootNode[0]->toxml());
        $this->out('sNameOfTable : ' . pyjslib_str($sNameOfTable));
        $this->out('sNameOfentries : ' . pyjslib_str($sNameOfEntries));
        $cyEntriesNode = $this->getEntry($cyRootNode, $sNameOfTable, $sNameOfEntries);
        $entrySet = setOfEntries::setOfEntries();
        $entrySet->setName($sNameOfEntries);
        $iNr = $this->getNumberOfEntries($cyEntriesNode);
        $this->out('Number of Columns %i ' . pyjslib_repr($iNr));
        $iCol = 0;
        while (($iCol < $iNr)) {
            $xmlCol = $this->getEntryAt($cyEntriesNode, $iCol);
            $entry = $cuon->Windows->dataEntry->dataEntry();
            $entry->setName($this->getEntrySpecification($xmlCol, 'name'));
            $entry->setType($this->getEntrySpecification($xmlCol, 'type'));
            $entry->setSizeOfEntry($this->getEntrySpecification($xmlCol, 'size'));
            $entry->setVerifyType($this->getEntrySpecification($xmlCol, 'verify_type'));
            $entry->setCreateSql($this->getEntrySpecification($xmlCol, 'create_sql'));
            $entry->setSqlField($this->getEntrySpecification($xmlCol, 'sql_field'));
            $entry->setBgColor($this->getEntrySpecification($xmlCol, 'bg_color'));
            $entry->setFgColor($this->getEntrySpecification($xmlCol, 'fg_color'));
            $entry->setDuty($this->getEntrySpecification($xmlCol, 'duty'));
            $entry->setRound($this->getEntrySpecification($xmlCol, 'round'));
            $entry->setNextWidget($this->getEntrySpecification($xmlCol, 'next_widget'));
            $this->out('entry-gets = ' . pyjslib_str($entry->getName()) . ', ' . pyjslib_str($entry->getType()));
            $entrySet->addEntry($entry);
            $iCol += 1;
        }
        return $entrySet;
    }

}

