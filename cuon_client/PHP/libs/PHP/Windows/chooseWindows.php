<?php



/*
 * @author Jürgen Hamel
 * @license GPL V3
 * @version 0.2
coding=utf-8
Copyright (C) [2003-2018]  [Jürgen Hamel, D-32584 Löhne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

*/

include_once($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Windows/windows.php');


class chooseWindows extends windows {
    function __construct() {
         //windows::__construct();
        $this->chooseEntry = null;
         $this->chooseMenuItem = null;
        $this->specialValue = null;
        $this->chooseButton = null;
    }
    function setChooseButton($button) {
        $this->chooseButton = $button;
    }
    function setChooseEntry($sName,$entry) {
        pyjslib_printnl('<<<<<<<<<<<<<<< setCooseEntry <<<<<<<<<<<<<<<<<<<<<');
        $this->out($sName . ' ' . pyjslib_repr($entry));
        $this->chooseEntry = $entry;
        $this->chooseMenuitem = $this->getWidget($sName);
        $this->chooseMenuitem->set_sensitive(true);
    }
    function pressChoosebutton() {
        if ($this->chooseButton) {
            $this->activateClick($this->chooseButton, 'clicked');
        }
    }
    function setChooseValue($chooseValue) {
        pyjslib_printnl('<<<<<<<<<<<<<<< setCooseValue <<<<<<<<<<<<<<<<<<<<<');
        pyjslib_printnl(['value= ', $chooseValue], true);
        $this->chooseEntry->set_text(pyjslib_repr($chooseValue));
        if ($this->chooseMenuitem) {
            $this->chooseMenuitem->set_sensitive(false);
            $this->closeWindow();
        }
    }
    function getChangedValue($sName) {
        $iNumber = 0;
        $s = $this->getWidget($sName)->get_text();
        if ($s) {
            if ((count(string::strip($s)) > 0)) {
                if ($s->isdigit()) {
                    $iNumber = long($s);
                }
                else {
                    $s = string::strip($s);
                    if (($s[(count($s) - 1)] == 'L')) {
                        $iNumber = long(array_slice($s, 0, (count($s) - 1) - 0));
                    }
                }
            }
        }
        return $iNumber;
    }
}

