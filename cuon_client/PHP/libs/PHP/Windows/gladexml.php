
<?php



/*
 * @author Jürgen Hamel
 * @license GPL V3
 * @version 0.2
coding=utf-8
Copyright (C) [2003-2018]  [Jürgen Hamel, D-32584 Löhne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

*/

include_once ($_SERVER['DOCUMENT_ROOT'].'/libs/PHP/xmlrpc/cuon_xmlrpc.php');
include_once ($_SERVER['DOCUMENT_ROOT'].'/Databases/dumps.php');

class gladexml extends dumps {

     function __construct($servermod=false) {
            dumps::__construct();
               $this->servermod = $servermod;
        $this->xml = null;
        $this->win1 = null;
        $this->liAllMenuItems = [];
        $this->dictEnabledMenuItems = [];
        $this->mainwindowTitle = 'C.U.O.N.';
        $this->xmlAutoconnect = false;
        // $this->rpc = ;
        //$this->accel_group = $gtk->AccelGroup();
        //$this->win_accel_group = $gtk->AccelGroup();
        $this->accel_groups = [];
        $this->dicAccelKeys = [];
        $this->dicAccelKeys['edit'] = 'e';
        $this->dicAccelKeys['save'] = 's';
        $this->dicAccelKeys['new'] = 'n';
        $this->dicAccelKeys['print'] = 'p';
        $this->lm_manager = null;
        $this->dicEntries = [];
        $this->dicGladeFields = [];
     }
     
     public function createMainMenu($liMenu, $gladename){

          $this->elog("*************************************************************************************");
          
          $this->elog("liMenu at createMainMenu " .  json_encode($liMenu) );
          $this->elog("*************************************************************************************");
          
          $sTop='';
        $s = '' ;
        
        $s = $s .  ' <div class="topnav">';
        $s = $s .  '         <ul id="nav">';

        for($main = 0, $length = count($liMenu); $main < $length; ++$main) { 
             if (gettype($liMenu[$main]) == 'string'){
                  $liMenu[$main] =  str_replace("1","",$liMenu[$main]) ;
                  $liMenu[$main] =  str_replace("_","",$liMenu[$main]) ;
                  $sTop=$liMenu[$main] ;
                  $s = $s .  '   <li><a class="active" >' . strtoupper($liMenu[$main][0]) . substr($liMenu[$main],1)  . '</a>';
             }
             else    {
                  $s = $s . '<ul>';
                  for($j = 0, $sublength = count($liMenu[$main]); $j < $sublength; ++$j) {

                       $liMenu[$main][$j] =  str_replace("mi_","",$liMenu[$main][$j]) ;
                       $liMenu[$main][$j] =  str_replace("1","",$liMenu[$main][$j]) ;
                       $liMenu[$main][$j] =  str_replace("_","",$liMenu[$main][$j]) ;
                       $liMenu[$main][$j] =  str_replace("gtk-","",$liMenu[$main][$j]) ;
                       
                            if( stripos ( $liMenu[$main][$j] , "trennli") !== false or stripos ( $liMenu[$main][$j] ,"separator") !== false ) {
                                 $s = $s .  '	 <li><a tabindex="0" href="">' . '--------' . '</a></li>';
                            }
                            else{
                                 if ($gladename == 'cuon') {
                                      $prefix = '/' ;
                                 }
                                 else if ($gladename == 'address'){
                                      $gladename = 'addresses' ;
                                      $prefix = '/' .  strtoupper($gladename[0]) . substr($gladename,1) . '/' . $sTop . '/' ;
                                     
                                 }
                                 else{
                                      $prefix = '/' .  strtoupper($gladename[0]) . substr($gladename,1) . '/' . $sTop  . '/' ;
                                 }
                                                
                                 if ($gladename == 'cuon'){    
                                      $s = $s .  '	 <li><a tabindex="0" href="' . $prefix .  strtoupper($liMenu[$main][$j][0]) . substr($liMenu[$main][$j],1) .'/' . strtolower($liMenu[$main][$j]) . '.php">' .  strtoupper($liMenu[$main][$j][0]) . substr($liMenu[$main][$j],1)  . '</a></li>';
                                 }
                                 else{

                                  $s = $s .  '	 <li><a tabindex="0" href="?' .  $sTop .'_' . strtolower($liMenu[$main][$j]) . '">' .  strtoupper($liMenu[$main][$j][0]) . substr($liMenu[$main][$j],1)  . '</a></li>';
                                 }
                            }
                      
                               
   
                  }
              $s = $s .  '  </ul>';
             }
            
            

        }
        $s = $s .  '  </ul>';
        $s = $s .  '  </div>';
        $s = $s .  '  <br/>';
        print_r($s) ;
        return 1;
   }

   public function createFindFields($liFinds){

        
          

        $s = '<form action="findData.php" method="post">' ;
        $s = $s . '<table border=8 cellspacing=6>';
    
        for ($i=0;$i<4;$i++){
             $s = $s . "<tr>";
             for ($j=0;$j<7;$j++){
                  if($liFinds[2 + ($i*7+$j)][0] == "GtkLabel"){
                       $s = $s . '<td>' . _($liFinds[2 + ($i*7+$j)][2]) . '</td>';
                       
                  }
                  else if($liFinds[2 + ($i*7+$j)][0] == "GtkEntry"){
                       $s = $s . '<td><input type="text" name=$liFinds[2 + ($i*7+$j)][1] value=""/></td>';


                       
                  }
                  else if($liFinds[2 + ($i*7+$j)][0] == "GtkButton"){
                       if($liFinds[2 + ($i*7+$j)][1] == "bSearch"){
                            $s .= '<td><Button  type="submit" name="Search" value="Search" >'.$liFinds[2 + ($i*7+$j)][1] .'</Button> </td>';    
                       }
                       else if ($liFinds[2 + ($i*7+$j)][1] == "bClearSearch"){
                            $s .= '<td><Button  type="submit" name="ClearSearch" value="ClearSearch" >'.$liFinds[2 + ($i*7+$j)][2] . '</Button> </td>';    
                       }
                      
                  }    
                  else{
                       $s .=  '<td>' . _($liFinds[2 + ($i*7+$j)][2]) . '</td>';
                  }
             }
             $s = $s . '</tr>';
        }
             
               
          
        $s = $s . '</tr>';
        $s = $s . '</table>';
        

        print_r($s) ;
        return 1;
   }

   public function createListHeader( $liHeader){
        $s = '<table cellspacing="0" cellpadding="0" border="0" width="100%">  <tr>   <td>    <table cellspacing="0" cellpadding="1" border="1" width="100%" >      <tr style="color:white;background-color:grey"> ';

     
        foreach ($liHeader as &$Header) {
             $s = $s . '<th>' . $Header . '</th>';
             

        }

        $s = $s . '    </tr>    </table>   </td>  </tr> </table> ' ;



      


        

        
        print_r($s);
        return 1;
        

   }

   public function createListbox($liItems){

       
        $_SESSION['items'] = $liItems;

        
        $s =  ' <iframe height="320" width="100%" src="Listbox.php">

   <a href="Listbox.php"> </a> 
</iframe>  ';

        
        print_r($s);
  
    
        return 1;
        
  
   }



   
   public function loadGlade($gladename,$mainwindow){

        $newglade = [] ;
        $sPrefix = "gtk3_";
        $s = ("glade_" . $sPrefix . $gladename  . ".xml") ;
        //echo "</br>load this: " + $s ;

        $liGlade =  callRP("Database.getLiGlade",$s,$mainwindow);

        $this->elog( "liGlade is = "  . json_encode($liGlade) );

        
        $menu =  $this->createMainMenu($liGlade[0],$gladename);
        
        $find = $this->createFindFields($liGlade[1]);
        $this->elog("menu items = " . json_encode($menu) );
        $this->elog("find items = " . json_encode($find) );
        
        array_push($newglade,$menu);
        array_push($newglade,$find);

        

        $liFields = callRP("Misc.getTreeInfo",$gladename);
        $this->elog($liFields[3]);
        $ok = $this->createListHeader($liFields[3]);

        //$ok = $this->createListbox(Null);
        

        
        return 1 ;
        
        
   }
   function loadGladeFields($gladename,$mainwindow,$iNoteTab){

        $newGlade = [] ;
        $sPrefix = "gtk3_";
        $s = ("glade_" . $sPrefix . $gladename  . ".xml") ;
        //echo "</br>load this: " + $s ;

        $newGlade =  callRP("Database.getLiGladeFields",$s,$mainwindow, $iNoteTab);

        $this->elog("Gladefields = ". json_encode($newGlade)) ;

        $this->dicGladeFields = $newGlade ;
        
        $_SESSION['lidetailfields'] =   $newGlade ;       
    $s =  ' <iframe name="iframe_b" height="1500" width="100%" src="detail.php">

   <a href="detail.php?id=0"> </a> 
</iframe>  ';
        print_r($s);
        
        //exit("stop");

   }
 
   function setTitle($sName,$sTitle) {
        $this->getWidget($sName)->set_title($sTitle);
    }
    function initMenuItemsMain() {
        $this->liAllMenuItems = $this->getWidgets('mi_');
    }
    function initMenuItems() {
        $this->liAllMenuItems = $this->getWidgets('mi_');
        $this->addEnabledMenuItems('window', 'quit1', 'q');
        $this->addEnabledMenuItems('window', 'mi_quit1', 'q');
    }
    function enableAllMenuItems() {
        foreach( $this->liAllMenuItems as $i ) {
            if (($i != null)) {
                 //$i->set_sensitive(true);
            }
        }
    }
    function disableAllMenuItems() {
        foreach( $this->liAllMenuItems as $i ) {
            if (($i != null)) {
                 //$i->set_sensitive(false);
            }
        }
    }
    function addEnabledMenuItems($sName,$sMenuItem,$cKey=null) {
         //if ($this->dictEnabledMenuItems->has_key($sName)) {
         //  $liMenuItems = $this->dictEnabledMenuItems[$sName];
         //}
         //else {
            $liMenuItems = [];
            //}
        $item = $this->getWidget($sMenuItem);
        $this->printOut('item by Enable Menu', pyjslib_repr($item));
        if ($cKey) {
            $item = $this->addKeyToItem($sName, $item, $cKey);
        }
        $liMenuItems[] = $item;
        $this->dictEnabledMenuItems[$sName] = $liMenuItems;
    }
   function removeEnabledMenuItems($sName) {
        /* if ($this->dictEnabledMenuItems->has_key($sName)) { */
        /*     unset($this->dictEnabledMenuItems[$sName]); */
        /* } */
    }
    function enableMenuItem($sName,$sAccel=null,$sMenuItem=null) {
        /* if ($this->dictEnabledMenuItems->has_key($sName)) { */
        /*     $liMenuItems = $this->dictEnabledMenuItems[$sName]; */
        /*     foreach( $liMenuItems as $i ) { */
        /*         if (($i != null)) { */
        /*             $this->printOut('GladeXML-Name = ', $sName); */
        /*             $i->set_sensitive(true); */
        /*         } */
        /*         else { */
        /*             $this->printOut('No Menuitem'); */
        /*         } */
        /*     } */
        /* } */
    }
    function disableMenuItem($sName) {
        try {
            $liMenuItems = $this->dictEnabledMenuItems[$sName];
            foreach( $liMenuItems as $i ) {
                if (($i != null)) {
                     //$i->set_sensitive(false);
                }
                else {
                    $this->printOut('No Menuitem');
                }
            }
        }
        catch(Exception $e) {
                        $this->printOut('No Menuitem');
        }
    }

   function setXml($xml) {
        $this->xml = $xml;
   }
      function loadEntries($sName) {
           //callRP("Database.getEntryKey",['entry_gtk3_'.$sName,'entry_' . $sName,$this->getNullUUID()] );
           $this->elog("name at sl get entry: " . $sName);
           $dicEntries = callRP("Database.sl_getEntry",$sName);
           $this->elog("dicentries : " . json_encode($dicEntries) );
           return $dicEntries ;
           
    }
}

?>


<?php
/*
set_include_path(get_include_path() . PATH_SEPARATOR . dirname(__FILE__) . DIRECTORY_SEPARATOR . 'libpy2php');
require_once('libpy2php.php');
require_once( 'sys.php');
try {
    require_once( 'pygtk.php');
    $pygtk->require('2.0');
    require_once( 'gtk.php');
    require_once( 'gobject.php');
}
catch(Exception $e) {
    }
require_once( 'cPickle.php');
require_once( 'cuon_TypeDefs.php');
require_once( 'os.php');
require_once( 'os_path.php');
require_once( 'types.php');
try {
    require_once( 'textEditor.php');
}
catch(Exception $e) {
    }
class gladeXml extends defaultValues {
    function __construct($servermod=false) {
        defaultValues::__construct();
   
        try {
            $this->clipboard = $gtk->clipboard_get();
        }
        catch(Exception $e) {
                        try {
                $this->clipboard = $gtk->Clipboard();
            }
            catch(Exception $e) {
                                $this->clipboard = null;
            }
        }
    }
    function setClipboard($text) {
        $this->clipboard->set_text($text);
        $this->clipboard->store();
    }
    function getClipboard($iNumber=0) {
        return $this->clipboard->wait_for_text();
    }
    function getNotesEditor($mime_type='text/x-tex',$highlight=true) {
        $te = $textEditor->textEditor();
        list($textbufferMisc, $viewMisc) = $te->getEditor($mime_type, $highlight);
        return [$textbufferMisc, $viewMisc];
    }
    function setTextbuffer($widget,$liField) {
        try {
            $buffer = $gtk->TextBuffer(null);
        }
        catch(Exception $e) {
                        $buffer = $gtk->TextBuffer();
        }
        $text = '';
        $this->debug($this->oUser->userEncoding);
        foreach( pyjslib_range(count($liField)) as $i ) {
            pyjslib_printnl(type($liField[$i]));
            pyjslib_printnl($liField[$i]);
            if (isinstance($liField[$i], types::StringType)) {
                $text = ($text + $liField[$i]) . '
';
            }
            else if (isinstance($liField[$i], types::UnicodeType)) {
                $text = ($text + $liField[$i]) . '
';
            }
            else if (isinstance($liField[$i], types::ClassType) || isinstance($liField[$i], types::InstanceType)) {
                $text = ($text + pyjslib_repr($sValue));
            }
            else if (isinstance($liField[$i], types::IntType)) {
                $text = ($text + pyjslib_repr($liField[$i])) . '
';
            }
            else if (isinstance($liField[$i], types::FloatType)) {
                $text = ($text + pyjslib_repr($liField[$i])) . '
';
            }
            else {
                $text = ($text + pyjslib_repr($liField[$i])) . '
';
            }
        }
        $buffer->set_text($text);
        $widget->set_buffer($buffer);
    }
    function clearTextBuffer($widget) {
        $bText = '';
        $buffer = $widget->get_buffer();
        $buffer->set_text($bText);
        $widget->set_buffer($buffer);
    }
    function readTextBuffer($widget) {
        $bText = '';
        $buffer = $widget->get_buffer();
        $bText = $buffer->get_text($buffer->get_start_iter(), $buffer->get_end_iter(), 1);
        return $bText;
    }
    function getActiveText($combobox) {
        $model = $combobox->get_model();
        $active = $combobox->get_active();
        if (($active < 0)) {
            return;
        }
        return $model[$active][0];
    }
    function add2Textbuffer($widget,$text,$direction=null) {
        if ($text) {
            try {
                $buffer = $widget->get_buffer();
            }
            catch(Exception $e) {
                                $buffer = $widget;
            }
            $bText = $buffer->get_text($buffer->get_start_iter(), $buffer->get_end_iter(), 1);
            if (!($bText)) {
                $bText = '';
            }
            if ($direction && ($direction == 'Head')) {
                $bText = ($text + $bText);
                $buffer->set_text($bText);
            }
            else if ($direction && ($direction == 'Tail')) {
                $bText += $text;
                $buffer->set_text($bText);
            }
            else if ($direction && ($direction == 'Overwrite')) {
                $buffer = $gtk->TextBuffer(null);
                $buffer->set_text($text);
            }
            else {
                $bText += $text;
                $buffer->set_text($bText);
            }
            try {
                $widget->set_buffer($buffer);
            }
            catch(Exception $e) {
                            }
        }
    }
    function setXml($xml) {
        $this->xml = $xml;
    }
    function loadGlade($gladeName,$sMainWindow=null,$gladePath=null) {
        pyjslib_printnl(['glade-path,  td = ', $gladePath, $this->cuon_path], true);
        if ($gladePath) {
            $fname = os::path::normpath($gladePath);
        }
        else {
            if ($this->SystemName) {
                if (($this->SystemName == 'LINUX-Standard')) {
                    $fname = os::path::normpath($this->cuon_path . '/' . 'glade_' . $gladeName);
                }
                else {
                    $fname = os::path::normpath($this->cuon_path . '/' . 'glade_' . $this->SystemName . '_' . $gladeName);
                }
            }
            else {
                $fname = os::path::normpath($oself->cuon_path . '/' . 'glade_' . $gladeName);
            }
        }
        $fnameAlternate = os::path::normpath($this->cuon_path . '/' . 'glade_' . $gladeName);
        pyjslib_printnl(['fname ', $fname], true);
        pyjslib_printnl(['fname_Alternate ', $fnameAlternate], true);
        try {
            $this->xml = $gtk->Builder();
            $this->xml->add_from_file($fname);
            $this->xml->set_translation_domain('cuon');
            pyjslib_printnl(['loaded Builder ', $fname], true);
        }
        catch(Exception $e) {
                        pyjslib_printnl([$Exception, $params], true);
            try {
                $this->xml = $gtk->glade->XML($fname);
                pyjslib_printnl(['loaded Glade ', $fname], true);
            }
            catch(Exception $e) {
                                try {
                    $this->xml = $gtk->Builder();
                    $this->xml->add_from_file($fnameAlternate);
                    $this->xml->set_translation_domain('cuon');
                    pyjslib_printnl(['loaded Builder ', $fnameAlternate], true);
                }
                catch(Exception $e) {
                                        $this->xml = $gtk->glade->XML($fnameAlternate);
                    pyjslib_printnl(['loaded Glade ', $fnameAlternate], true);
                }
            }
        }
        pyjslib_printnl('glade loaded');
        if ($sMainWindow) {
            $this->win1 = $this->getWidget($sMainWindow);
            if ($this->win1 && ($sMainWindow->find('Mainwindow') > 0)) {
                py2php_kwargs_method_call($this, 'setWinProperty', [], ["Main" => true]);
                $this->win1->connect('delete-event', $this->delete_event);
            }
        }
        pyjslib_printnl('connect');
        $this->setXmlAutoconnect();
    }
    function setWinProperty($Main=false) {
    }
    function writeGlade($fname) {
        $xml1 = eval($this->doDecode($this->rpc->callRP('Database.getInfo', $fname)));
        $d1 = pyjslib_open(os::path::normpath($this->cuon_path . '/' . $fname), 'w');
        $d1->write(cPickle::loads($xml1));
        $d1->close();
    }
    function loadGladeComplete($gladeName) {
        $fname = 'glade_' . $gladeName;
        $xml1 = eval($this->decode($this->rpc->callRP('Database.getInfo', $fname)));
        $this->rpc = $cuon->XMLRPC->xmlrpc->myXmlRpc();
        $d1 = pyjslib_open($fname, 'w');
        $d1->write(cPickle::loads($xml1));
        $d1->close();
        $this->xml = $gtk->glade->XML($fname);
        $this->setXmlAutoconnect();
    }
    function loadGladeFile($fname,$sMainWindow=null) {
        try {
            pyjslib_printnl(['load GtkBuilder', os::system('pwd')], true);
            $this->xml = $gtk->Builder();
            pyjslib_printnl(['loadFile = ', $fname], true);
            $this->xml->add_from_file($fname);
            pyjslib_printnl('file loaded');
            $this->xml->set_translation_domain('cuon');
            pyjslib_printnl(['loaded Builder ', $fname], true);
        }
        catch(Exception $e) {
                        try {
                $this->xml = $gtk->glade->XML($fname);
                pyjslib_printnl(['loaded Glade ', $fname], true);
            }
            catch(Exception $e) {
                                pyjslib_printnl([$Exception, $params], true);
            }
        }
        $this->setXmlAutoconnect();
    }
    function setXmlAutoconnect() {
        if ($this->xmlAutoconnect) {
        }
        else {
            try {
                $this->xml->connect_signals($this);
            }
            catch(Exception $e) {
                                pyjslib_printnl([$Exception, $params], true);
                $nameFuncMap = [];
                foreach( pyjslib_dir($this->__class__) as $key ) {
                    $nameFuncMap[$key] = pyjslib_getattr($this, $key);
                }
                if ($nameFuncMap) {
                    $this->xml->signal_autoconnect($nameFuncMap);
                }
            }
            $this->xmlAutoconnect = true;
            $this->setWinAccelGroup();
        }
    }
    function setWinAccelGroup() {
    }
    function getWidget($sName) {
        try {
            return $this->xml->get_object($sName);
        }
        catch(Exception $e) {
                        pyjslib_printnl([$Exception, $params], true);
            try {
                return $this->xml->get_widget($sName);
            }
            catch(Exception $e) {
                                return;
            }
        }
    }
    function getWidgets($sPrefix) {
        $liW = [];
        try {
            $liW = $this->xml->get_objects();
        }
        catch(Exception $e) {
                        pyjslib_printnl([$Exception, $params], true);
            $liW = $this->xml->get_widget_prefix('');
        }
        $liW2 = [];
        foreach( $liW as $i ) {
            try {
                if ((array_slice($i->get_name(), 0, 3 - 0) == 'mi_')) {
                    $liW2[] = $i;
                }
            }
            catch(Exception $e) {
                            }
        }
        $this->printOut('Widgets = ', pyjslib_repr($liW2));
        return $liW2;
    }

    function addKeyToItem($sName,$item,$cKey) {
        try {
            $item->add_accelerator('activate', $this->accel_group, ord($cKey), $gtk->gdk->CONTROL_MASK, $gtk->ACCEL_VISIBLE);
        }
        catch(Exception $e) {
                        pyjslib_printnl([$Exception, $params], true);
            pyjslib_printnl([$sName, $item, $cKey], true);
        }
        return $item;
    }
 
    function writeAllGladeFiles() {
        $nameOfGladeFiles = cPickle::loads(eval($this->doDecode($this->rpc->callRP('Database.getInfo', 'nameOfGladeFiles'))));
        $this->printOut('nameOfGladefiles' . pyjslib_repr($nameOfGladeFiles));
        $this->printOut(count($nameOfGladeFiles));
        foreach( pyjslib_range(0, count($nameOfGladeFiles)) as $i ) {
            $this->writeGlade($nameOfGladeFiles[$i]);
        }
    }
    function addMenuItem($item,$sMenue) {
        $sub1 = $item->get_submenu();
        $this->printOut('sub1 = ', pyjslib_repr($sub1));
        $newItem = py2php_kwargs_method_call($gtk, 'MenuItem', [], ["label" => $sMenue]);
        $sub1[] = $newItem;
        $item->set_submenu($sub1);
        $newItem->show();
        return $newItem;
    }
    function setText2Widget($sText,$sWidget) {
        if ((count($sText) > 0)) {
            $this->getWidget($sWidget)->set_text($sText);
        }
        else {
            $this->getWidget($sWidget)->set_text('');
        }
    }
    
    // * go to Tree Item
    // * @param iItem: int to jump to TreeItem, 0 to first, -1 to last Item
     
    function gotoTreeItem($iItem,$tree1) {
        $treeModel = $tree1->get_moedel();
        $firstIter = $treeModel->get_iter_first();
        $treeSelection = $tree1->get_selection();
        $iter = $firstIter;
        if (($iItem == 0)) {
            $treeSelection->select_iter($firstIter);
        }
        else if (($iItem == -1)) {
            while ($true) {
                $iter0 = $treeModel->iter_next($iter);
                if ($iter0) {
                    $iter = $iter0;
                }
            }
            if ($iter) {
                $treeSelection->select_iter($iter);
            }
        }
        else {
        }
    }
    function setWaitCursor() {
    }
    function setNormalCursor() {
    }
    function setComboBoxTextColumn($widget,$iCol=0) {
        try {
            if ((type($widget) == $gtk->ComboBox)) {
                $cell = $gtk->CellRendererText();
                $widget->pack_start($cell, true);
                $widget->add_attribute($cell, 'text', 0);
            }
        }
        catch(Exception $e) {
                        pyjslib_printnl([$Exception, $params], true);
        }
        try {
            if ((type($widget) == $gtk->ComboBoxEntry)) {
                $widget->set_text_column($iCol);
            }
        }
        catch(Exception $e) {
                        pyjslib_printnl([$Exception, $params], true);
        }
    }
    function get_active_text($combobox) {
        $model = $combobox->get_model();
        $active = $combobox->get_active();
        if (($active < 0)) {
            return;
        }
        return $model[$active][0];
    }
    function getListStore($liFields) {
        $ls = null;
        $s = 'ls = gtk.ListStore(';
        foreach( $liFields as $i ) {
            pyjslib_printnl(['i = ', $i], true);
            if (($i->lower() == 'string')) {
                $s .= 'gobject.TYPE_STRING,';
            }
            else if (($i->lower() == 'uint')) {
                $s .= 'gobject.TYPE_UINT,';
            }
            else if (($i->lower() == 'float')) {
                $s .= 'gobject.TYPE_FLOAT,';
            }
        }
        $s .= 'gobject.TYPE_UINT)';
        pyjslib_printnl(['s = ', $s], true);
        eval($s);
        pyjslib_printnl(['ls = ', $ls], true);
        return $ls;
    }
}

*/
     

?>
     