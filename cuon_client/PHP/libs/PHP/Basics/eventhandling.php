
<?php
 require_once( $_SERVER['DOCUMENT_ROOT'].'/libs/PHP/Basics/socket_service.php');


class EventHandler extends CuonSocketServer {

    const REGEX_VALID_EVENT_NAME = '/^[a-zA-Z][a-zA-Z0-9]*$/';

    private $arr_events = array();

    public function addEvent($pstr_event, $pcb_callback) {

        if ( is_callable($pcb_callback) !== true ) {

            throw new InvalidArgumentException(
                'Der angegebene Callback ist nicht gültig!');

        }

        if ( !is_string($pstr_event)
            OR preg_match(self::REGEX_VALID_EVENT_NAME, $pstr_event) === 0 ) {

            throw new InvalidArgumentException(
                'Der angegebene Eventname ist ungültig!');

        }

        $this->arr_events[$pstr_event][] = $pcb_callback;
        error_log("EVENT START : " . json_encode($this->arr_events) . "\n" ,3,"/var/tmp/cuon_php.log"  );
    }

    public function bindEvents(array $arr_events, $pcb_callback) {

        foreach ( $arr_events as $str_event ) {

            $this->addEvent($str_event, $pcb_callback);

        }

    }

    public function triggerEvent($pstr_event, $obj_subject = null, array $parr_params = array()) {
         error_log("EVENT" ." " . $pstr_event . " -- " .   json_encode($obj_subject ) . " -- " .  json_encode( $parr_params) . "\n" ,3,"/var/tmp/cuon_php.log"  );
        if ( is_string($pstr_event) ) {
             error_log("EVENT 2 : " . json_encode($this->arr_events) . "\n" ,3,"/var/tmp/cuon_php.log"  );
            if ( isset($this->arr_events[$pstr_event]) ) {

                foreach ( $this->arr_events[$pstr_event] as $cb_callback ) {

                    $arr_params = array($obj_subject, $parr_params);

                    call_user_func_array($cb_callback, $arr_params);

                }

            }

        } else {

            throw new InvalidArgumentException(
                'Eventname muss vom Datentyp "String" sein!');

        }

    }

}

?>
