/** 
 * @file   cuonClient.cpp
 * @author  <jhamel@x6>
 * @date   Wed Mar 11 10:51:54 2015
 * 
 * @brief  
 * CPP Client Mainprogram
 * 
 */
#include <os_info.hpp>
#ifdef PLUMA
   #include <Pluma/Pluma.hpp>
#else
   #include <cuon/PI/pi_control.hpp>
#endif

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <libintl.h>
#include <locale.h>
#include <thread> 
#include <chrono>


#include <cuonClient.hpp>
#include <global.hpp>

//#include <xmlrpc-c/base.hpp>
//#include <xmlrpc-c/registry.hpp>
//#include <xmlrpc-c/server_abyss.hpp>

#include <cuon_window.hpp>

#include <cuon/Databases/datatypes.hpp>
#include <cuon/Databases/localCache.hpp>
#include <cuon/Databases/data.hpp>


#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/User/User.hpp>
#include <cuon/Windows/gladeXml.hpp>
#include <cuon/Clients/cuonclients.hpp>
#include <cuon/Login/login.hpp>
#include <cuon/TypeDefs/iniReader.hpp>
#include <cuon/TypeDefs/basics.hpp>

#include <cuon/Addresses/addresses.hpp>
#include <cuon/Articles/articles.hpp>


// #include <cuon/Stock/stock.hpp>
//#include <cuon/Order/order.hpp>

#include <cuon/DMS/dms.hpp>
#include <cuon/DMS/SingleDMS.hpp>


//#include <cuon/Project/project.hpp>
// #include <cuon/Sourcenavigator/sourcenavigator.hpp>



// special modules
//#include <cuon/Garden/botany.hpp>




using namespace std;
using namespace std::chrono;


char cCurrentPath[FILENAME_MAX];
string DebugLevel = "SEARCH";
User oUser ;                    
bool gtk_is_running = false ;

map <string,string> cuonSystem;

int ipc_port1 = 0;

localCache* lCache = new localCache();
/** 
 * MainWindow() set the Version of C.U.O.N.
 * 
 */
MainWindow::MainWindow()
{

     /// set the scheduling times
     time_contact = 2*60*1000 ;
     time_schedul = 15*60*1000;
     time_imap_dms =27*60*1000;
     
     Version.Major = 16; // Year
     Version.Minor = 5 ; // Month
     Version.Rev = 17 ; // Day
     Version.Spec = 0; // Special of a day

    

     
     cout << "Mainwindow start at  " << cCurrentPath  << endl ;
}

MainWindow::~MainWindow()
{
}
void MainWindow::start(int argc, char* argv[])
{

    
#ifdef PLUMA
      pluma::Pluma plugins;
     // Tell plugins manager to accept providers of the type DeviceProvider
     plugins.acceptProviderType<cuon_windowProvider>();
     // Load library "standard_devices" from folder "plugins"
     string plPath = "/usr/share/cuon/plugins";
     //map < string,string > nPlugin ;
     vector < map < string,string > > vPlugin = oUser.vUser["plugins"] ;
     
     typedef:: vector <map <string,string> >::const_iterator iV ;
     myPluginsName  = {} ;
     for (iV iP = vPlugin.begin(); iP != vPlugin.end(); iP++) {
          
          //auto nPlugin = vPlugin[0];
          typedef map<string,string>::const_iterator iM ;
          for (iM i =iP->begin(); i != iP->end(); i++) {
                cout << i->first << " - " << i->second << endl ;



     
               string plMenu = i->first ;
               string plName = i->second ; // "libSourcenavigator.so" ;
               cout << "--------------------------------------------------------- " << plMenu << ", " << plName << endl;
               
               plugins.load(plPath, plName);
               myPluginsName.push_back(plMenu);
               
               
               
    
          }
     }


     cout << "********************************************************************************** Get device providers into a vector" << endl ;
    
     plugins.getProviders(providers);
      cout << "********************************************************************************** Get device providers into a vector 2" << endl ;
     for(int iZ=0; iZ < providers.size(); iZ++){ 
          //     if (!providers.empty()){


          plIndex[myPluginsName[iZ]] = iZ;
          
          // if(myPluginsName[iZ] == "Projects"){
          
          //      plIndex["Projects"] = iZ;

          // }
          

          //  else if(myPluginsName[iZ] == "Sourcenavigator"){
          
          //      plIndex["Sourcenavigator"] = iZ;
          //  }
          
          //  else if(myPluginsName[iZ] == "Stock"){
          
          //      plIndex["Stock"] = iZ;
          // }
          
          // else if(myPluginsName[iZ] == "Botany"){
          
          //      plIndex["Botany"] = iZ;
          // }
        
          // else if(myPluginsName[iZ] == "Hibernation"){
          
          //      plIndex["Hibernation"] = iZ;
          // }
          // else if(myPluginsName[iZ] == "Order"){
          //      plIndex["Order"] = iZ;
              
          // }
          //  else if(myPluginsName[iZ] == "Enquiry"){
          //       plIndex["Enquiry"] = iZ;
               
          //  }
          //  else if(myPluginsName[iZ] == "Proposal"){
          //       plIndex["Proposal"] = iZ;

          //  }
          //   else if(myPluginsName[iZ] == "Staff"){
          //       plIndex["Staff"] = iZ;

          //  }
   
          // aw->initAll();
     }

#endif
     
      cout << "********************************************************************************** Get device providers into a vector - GTK starts" << endl ;
     Gtk::Main gtkmain(argc, argv);
     int myInt = 0;
   
      cout << "********************************************************************************** Connect to AppServer " << endl ;

      
     x1.callRP("Database.is_running",0, myInt);
     cout << "test in main myInt = " << myInt << endl ;
     v_end.push_back("end1");
     menuItems["quit"] = v_end ;
     cout << "test in main 0  "  << endl ;
     // CHECK THE VERSION
     cout << "System = " <<  OS_INFO_PLATFORM << " " <<  OS_INFO_CPU << endl ;
     loadGlade("cuon","window1");
     setTitle("cuonClient V " + toString(Version.Major) + "." + toString(Version.Minor) + "-" + toString(Version.Rev)) ;

     cout << "test in main 1  "  << endl ;
     Gtk::Entry *pEntryServer ;
     window->get_widget("eServer",pEntryServer);
     pEntryServer->set_text ( cuonSystem["server"] );

     //window->get_widget("window1",pWin);
     window->get_widget("aCuon", pDlg);

     //get eUserName widget
     Gtk::Entry *pEUserName ;
      window->get_widget("eUserName", pEUserName);
     pEUserName->signal_changed().connect(sigc::mem_fun(this, &MainWindow::on_eUserName_changed));
      
     cout << "test in main 2 "  << cuonSystem["User"] <<  cuonSystem["Password"] << endl ;




     if (cuonSystem["User"] != "EMPTY" && cuonSystem["Password"] != "TEST" ) {

          oUser.sUser["Name"] = cuonSystem["User"] ;
          login l1 = login();
          l1.setMainwindow(window);
          oUser.sUser["sessionID"] = l1.sendLoginData(cuonSystem["User"],cuonSystem["Password"]);
          
     }



     cout << "Main3" << endl;
     //gtk_builder_connect_signals (Main, Main);

     window->get_widget("end1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_end_activate));



     //connectMenuItem("end1");
     


     // Menu Data
     window->get_widget("login1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_login_activate));


     window->get_widget("mi_addresses1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_addresses1_activate));

     window->get_widget("mi_articles1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_articles1_activate));

     window->get_widget("mi_staff1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_mi_staff1_activate));
     
     window->get_widget("mi_botany", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_botany1_activate));


     
     // Menu work
     window->get_widget("mi_dms1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_dms1_activate));
     
     window->get_widget("mi_stock1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_stock1_activate));

     window->get_widget("mi_order1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_order1_activate));

        window->get_widget("proposal1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_proposal1_activate));

        window->get_widget("enquiry", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_enquiry_activate));

     window->get_widget("mi_hibernation", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_hibernation_activate));

     window->get_widget("mi_graves", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_graves_activate));

     

     // menu Misc
     window->get_widget("mi_project1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_project1_activate));

     window->get_widget("mi_sourcenavigator", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_mi_sourcenavigator_activate));


     // menu Tools

      window->get_widget("mi_import_data1", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_import_data1_activate));

     window->get_widget("deleteCache", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_delete_cache_activate));

#ifndef PLUMA

     // Enable for Raspberry Pi
     
     enableMenuItem("PI_Control");
     window->get_widget("PI_Control", pItem);
     pItem->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_PI_Control_activate));
     

#endif
     
     

     /**
     * slot for the timer
     * 
     */

     sigc::slot<bool>contact_slot = sigc::mem_fun(*this, &MainWindow::checkContact);
     /// connect slot to signal
     Glib::signal_timeout().connect(contact_slot, time_contact);
     

     sigc::slot<bool>schedul_slot = sigc::mem_fun(*this, &MainWindow::checkSchedul);
     /// connect slot to signal
     Glib::signal_timeout().connect(schedul_slot, time_schedul);
     
     sigc::slot<bool>imap_slot = sigc::mem_fun(*this, &MainWindow::checkImap);
     /// connect slot to signal
     Glib::signal_timeout().connect(imap_slot, time_imap_dms);
     
     
     startTiming();
     

     // Start the GUI
     gtk_is_running = true ;
     Gtk::Main::run(*pWin);
   
 
}

bool MainWindow::on_delete_event(GdkEventAny *event){
     printlog("window button close window");
     on_end_activate();
     return false;
}


void MainWindow::on_end_activate()
{
     cout << "cuon end called" << endl ;
     // clean dirs, this is the LINUX way, windows later
     
    string file_name =  (string) getenv("HOME") + "/cuondata/*__dms.*";;
    int ret_code = system( ("rm " + file_name).c_str() );
    if (ret_code == 0) {
        std::cout << "File was successfully deleted\n";
    } else {
        std::cerr << "Error during the deletion: " << ret_code << '\n';
    }
    
     Gtk::Main::quit();
}


void MainWindow::on_login_activate()
{
     login l1 ;
     l1.initLogin();

}

// Data
void MainWindow::on_addresses1_activate(){
//  auto f1 = async( launch::async, find_matches, pattern, backlog )

      addresses *ar1 =  new addresses() ;
     

}
void MainWindow::on_articles1_activate(){
     articles *art1 = new articles();

}
void MainWindow::on_mi_staff1_activate(){

     cuon_window* st ;
     #ifdef PLUMA
     st = providers.at(plIndex["Staff"])->create();
     int iOK = st->initAll();
     #endif

}
void MainWindow::on_botany1_activate(){
     printlog("activate botatany window");
#ifdef PLUMA
     bo = providers.at(plIndex["Botany"])->create();
     bo->initAll();
#endif
}


// Action
void MainWindow::on_dms1_activate(){
     dms *dms1 = new dms();
}

void MainWindow::on_order1_activate(){

     map <string,string>* dicOrder = {} ;
     bool newOrder = false;
     int orderid = 0 ;
     string OrderType = "";
     cuon_window* ob ;
          #ifdef PLUMA
     ob = providers.at(plIndex["Order"])->create();
     int iOK = ob->initOrder(dicOrder,newOrder,orderid,OrderType);
#endif
     
}

void MainWindow::on_proposal1_activate()
{
     cout << "on_proposal_activate" << endl;
     
     map <string,string>* dicOrder = {} ;
     bool newOrder = false;
     int orderid = 0 ;
     string OrderType = "";
     cuon_window* pro ;
          #ifdef PLUMA
     pro = providers.at(plIndex["Proposal"])->create();
     int iOK = pro->initOrder(dicOrder,newOrder,orderid,OrderType);
     #endif
}


void MainWindow::on_enquiry_activate()
{
     cout << "on_enquiry_activate" << endl;
     
     map <string,string>* dicOrder = {} ;
     bool newOrder = false;
     int orderid = 0 ;
     string OrderType = "";
     cuon_window* en ;
          #ifdef PLUMA
     en = providers.at(plIndex["Enquiry"])->create();
     int iOK =en->initOrder(dicOrder,newOrder,orderid,OrderType);
     #endif
}




void MainWindow::on_stock1_activate()
{
     cuon_window* st ;
          #ifdef PLUMA
      st = providers.at(plIndex["Stock"])->create();
      st->initAll();
      #endif
}

void MainWindow::on_hibernation_activate()
{
     printlog("activate Hibernation");
     cuon_window* hi ;
          #ifdef PLUMA
      hi = providers.at(plIndex["Hibernation"])->create();
      hi->initAll();
      #endif
}


void MainWindow::on_graves_activate()
{
     cuon_window* gr ;
          #ifdef PLUMA
      gr = providers.at(plIndex["Grave"])->create();
      gr->initAll();
      #endif
}

// Misc
void MainWindow::on_project1_activate()
{
    
     cuon_window* pr ;
          #ifdef PLUMA
      pr = providers.at(plIndex["Projects"])->create();
     
      pr->initAll();
      #endif
}

  // cuon_window* pr ;
  //     for(int iZ=0; iZ < providers.size(); iZ++){ 
  //         if(myPluginsName[iZ] == "Projects"){
  //              pr = providers.at(iZ)->create();
  //         }
  //     }
  //    pr->initAll();


void MainWindow::on_mi_sourcenavigator_activate()
{
     cuon_window* sn ;
          #ifdef PLUMA
      sn = providers.at(plIndex["Sourcenavigator"])->create();
      sn->initAll();
      #endif
}


// Tools

void MainWindow::on_import_data1_activate()
{
     data *imp = new data();
}

void MainWindow::on_delete_cache_activate(){

     deleteCache();
}


void MainWindow::on_PI_Control_activate()
{
     
          #ifndef PLUMA
     pi_control*  pi = new pi_control();
     pi->initAll();
     

      #endif
}

void MainWindow::on_eUserName_changed(){
     vector < Record > v1;
     printlog("Username at changed " + oUser.getName());
     printlog("Username at changed " + oUser.sSqlUser["Name"], oUser.getClient());

      // x1.callRP("User.setUserData",x1.add(oUser.sSqlUser),v1);
      cuonclients *cl = new cuonclients();
      if (cl->setClientID(oUser.getClient())){
           cl->pWin->hide();
           
           //if (cl) delete cl;
           printlog("Username at changed3 " + oUser.sSqlUser["Name"], oUser.getClient());
           checkMenus();
      }
      
}
     
void MainWindow::checkMenus(){
     vector <string> liModule ;
     
     x1.callRP("User.getEnabledMenus",x1.add(1),liModule);
     for (int i=0; i<liModule.size();i++){
          cout << liModule[i] << endl;
          enableMenuItem(liModule[i]);
          
          
     }

     
}

void MainWindow::startTiming(){
     checkSchedul();

}

bool MainWindow::checkContact(){
     vector<Record> vResultSet ;
     cout << "check contact after " << time_contact << " ms" << endl ;
     x1.callRP("Address.getAllActiveContacts",x1.add(),vResultSet );
     
     return true;
}

bool MainWindow::checkImap(){
     printlog("start Imap");

     // add emailValues to dicUser
     
     // now call deferred services
     int ok ;
     x1.callRP("Email.checkImap",x1.add(), ok);
     
     return true;
}
bool MainWindow::checkSchedul(){

     string sChoice = "All";
     Gtk::RadioButton* pRB1;

     vector <map<string,vector<string> > > liDates;
     window->get_widget("rbSchedulsNew",pRB1);
          if (pRB1->get_active()){
          sChoice = "New";
     }
        // elif self.getWidget('rbSchedulsCancel').get_active():
        //     sChoice = 'Cancel'
        // elif self.getWidget('rbSchedulsActualWeek').get_active():
        //     sChoice = 'actualWeek'    
        // print 'sChoice = ', sChoice

          
          vector <string> _values = {"Name","All",sChoice};
          
          x1.callRP("Address.getAllActiveSchedulMeta",x1.add(_values), liDates);

          for (auto i = liDates.begin(); i != liDates.end(); i++){

               map <string, vector <string> > mDates = *i ;
               
               for(auto iMap = mDates.begin(); iMap != mDates.end(); iMap++){

                    vector <string> vSingleDates = iMap->second ;
                    for (auto iDate = vSingleDates.begin(); iDate != vSingleDates.end(); iDate++){
                         cout << "found this: " << iMap->first << " with this date " << *iDate << endl ;
                    }
               }
          }
          return true ;
     
}
void  MainWindow::startXmlRpcServer(){
     /*  try {
          cout <<  "starting xmlrpc" << endl ;
          xmlrpc_c::registry myRegistry;
                    
          xmlrpc_c::methodPtr const pSourcenavigator(new sourcenavigator );
          myRegistry.addMethod("sourcenavigator.initSN", pSourcenavigator );
          xmlrpc_c::serverAbyss myAbyssServer(
               xmlrpc_c::serverAbyss::constrOpt()
               .registryP(&myRegistry)
               .portNumber(ipc_port1)
               .logFileName("/tmp/xmlrpc_log"));
          // while (true) {
               cout << "Waiting for next RPC..." << endl;
               myAbyssServer.runOnce();
 This waits for the next connection, accepts it, reads the
HTTP POST request, executes the indicated RPC, and closes
the connection.

               // }
     } catch (exception const& e) {
          cerr << "Something failed. " << e.what() << endl;
     } */
}



int main (int argc, char* argv[])
{

     oUser.init();
     basics* ba = new basics();
      char *cvalue = NULL;
     int index;
     int c;
     
     cout << "OS-Version cmakelist.txt = " << OS_VERSION << endl;
     setlocale( LC_ALL, "" );
     bindtextdomain( "cuon", "/usr/share/locale" );
     textdomain( "cuon" );
    
     cuonSystem["User"] = "EMPTY";
     
    
    
     
     opterr = 0;
    
     cout << "argc = " << argc << endl ;
     if (argc == 1) {
          iniReader* inR = new iniReader();


          map <string,string> newPlugin ;
          
          if (ba->file_exists("cuon.ini") ) {
               ba->printlog("ini in pwd exists");
               inR->parseIniFile("cuon.ini");
          } else {
              ba->printlog("search for .cuon.ini in home ");
               string path = getenv("HOME");
               string sFile = ".cuon.ini";
               if (ba->file_exists(path+"/"+sFile) ) {
                    ba->printlog("ini in home exists");
                    inR->parseIniFile(path+"/"+sFile);
               } else {
                    ba->printlog("no ini file found");
                    exit (EXIT_FAILURE);
               }
          }

          cout << "length of ini-file = " << inR->sizeOfItems << endl;
          for (int i=0;i<inR->sizeOfItems;i++){
               cout << "Ini-file " << inR->iniItem[i]->key << endl ;
               if (inR->iniItem[i]->key == "username"){
                    cuonSystem["User"] = inR->iniItem[i]->value ;
               }
               if (inR->iniItem[i]->key == "host"){
                    cuonSystem["server"] = inR->iniItem[i]->value ;
               }
               
               if (inR->iniItem[i]->key == "protocol"){
                    cuonSystem["protocol"] = inR->iniItem[i]->value ;
               }
               
               if (inR->iniItem[i]->key == "port"){
                    cuonSystem["port"] = inR->iniItem[i]->value ;
               }
               if (inR->iniItem[i]->key == "password"){
                    cuonSystem["Password"] = inR->iniItem[i]->value ;
               }
               
               if (inR->iniItem[i]->key == "client_id"){
                    oUser.iUser["client"] = ba->String2Int(inR->iniItem[i]->value) ;
               }
               
               if (inR->iniItem[i]->key == "startCash1"){
                    oUser.sUser["startCash1"] = inR->iniItem[i]->value ;
               }
                if (inR->iniItem[i]->key == "startCash2"){
                    oUser.sUser["startCash2"] = inR->iniItem[i]->value ;
               }
                if (inR->iniItem[i]->key == "startCash3"){
                    oUser.sUser["startCash3"] = inR->iniItem[i]->value ;
                }
                
               if (inR->iniItem[i]->key == "plugin"){
                   
                    
                    string sPl = inR->iniItem[i]->value ;
                    vector <string> liPlugin = ba->split(inR->iniItem[i]->value,",");
                    cout << "testPL  = " << liPlugin[0] << endl ;
                    map<string,string> newPlugin = { {liPlugin[0],liPlugin[1] } };
                    oUser.vUser["plugins"].push_back(newPlugin) ;
               }


               
          }

          
          // cuonSystem["User"] = inR->getOptionToString("username");
           cout << "cuonSystem[User] " << cuonSystem["User"]  << endl;
          // cuonSystem["server"] = inR->getOptionToString("host");
          // cuonSystem["port"] = inR->getOptionToString("port");
          // cuonSystem["protocol"] = inR->getOptionToString("protocol");
          // cuonSystem["Password"] = inR->getOptionToString("password");
          // oUser.iUser["client"] = inR->getOptionToInt("client_id");
         
          
          cout << "cuonSystem[server] " << cuonSystem["server"]  << endl;
          cout << "cuonSystem[port] " << cuonSystem["port"]  << endl;
          cout << "cuonSystem[protocol] " << cuonSystem["protocol"]  << endl;
         
          
          inR->cleanupIniReader();
          cout << "clean Ini Reader" << endl ;
     } else {


          while ((c = getopt (argc, argv, "s:p:c:U:P:C:")) != -1) {
               switch (c) {
               case 's':
                    cout << string(optarg) << endl ;
                    cuonSystem["server"] = string(optarg);
                    break;
               case 'p':
                    cout << string(optarg) << endl ;
                    cuonSystem["port"] = string(optarg);
                    break;
               case 'c':
                    cout << string(optarg) << endl ;
                    cuonSystem["protocol"] = string(optarg);
                    break;
               case 'U':
                    cout << string(optarg) << endl ;
                    cuonSystem["User"] = string(optarg);
                    break;
               case 'P':
                    cout << string(optarg) << endl ;
                    cuonSystem["Password"] = string(optarg);
                    break;
               case 'C':
                    cout << string(optarg) << endl ;
                    oUser.iUser["client"] = atoi(optarg);
                    break;
               default:
                    cout << "No Parameter" << endl;
                    //normaly nothing

               }
          }
     }
     cout << "Data = " << endl;
     cout << oUser.iUser["client"]   << endl;
     cout << cuonSystem["server"]   << endl;
     // do some file ops
     int check = 0;
     string dirname = (string) getenv("HOME") + "/cuondata";
     check = mkdir(dirname.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH );

     if (!check)
          cout << "Directory created" << endl ;

     else
     {
          cout << "Unable to create directory" << endl;
          // exit(1);
     }

   


    
   
 
   
    
     MainWindow* m = new MainWindow();
     m->start(argc,argv);
     
     return  EXIT_SUCCESS;

}

