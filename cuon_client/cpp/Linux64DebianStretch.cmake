



set(L_PATH "/usr")
set(L_PATH_LIB "/usr/lib")
set(L_PATH_X64 "/usr/lib/x86_64-linux-gnu")
set (CUON_OUTPUTDIR "${CMAKE_BINARY_DIR}/bin64" )
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CUON_OUTPUTDIR})
set (CMAKE_CXX_FLAGS " -g -m64 -std=c++11 " )
set (GCRYPT_LIBRARIES ${L_PATH_X64}/libgcrypt.so )
find_package (BZip2)	   


find_package(PkgConfig)
PKG_CHECK_MODULES(GTK, gtk+-3.0 REQUIRED)
pkg_check_modules(GTKMM  gtkmm-3.0 REQUIRED )
#pkg_check_modules(GTKSOURCEVIEWMM gtksourceviewmm REQUIRED )
set (GTKSOURCEVIEWMM_INCLUDE /usr/include/gtksourceviewmm-3.0  /usr/include/gtksourceview-3.0/ /usr/lib/x86_64-linux-gnu/gtksourceviewmm-3.0/include/)

set (GTKSOURCEVIEWMM_LIBRARIES )

include(FindXMLRPCC.cmake)
set (GCRYPT_LIBRARY_DIRS ${GCRYPT_LIBRARIES} )

#FIND_PACKAGE(XMLRPC REQUIRED c++)
message( "------------------------------- Debian Stretch ----------------------------------" )
message( "local home dir " ${CMAKE_HOME_DIRECTORY}  )
message( "Find xmlrpc libs " ${XMLRPC_LIBRARIES} )
message( "and this xmlrpc includes "  ${XMLRPC_INCLUDE_DIRS} )
message( "this is gtksourceviev: " ${GTKSOURCEVIEWMM_LIBRARIES} )
message( "this is gcrypt: " ${LIBGCRYPT_LIBS} )



#
#set (GCRYPT_LIBRARIES ${LIBGCRYPT_LIBS} )

#add_library (leveldb SHARED IMPORTED )
#set (localLib  ${CMAKE_HOME_DIRECTORY}/leveldb/libleveldb.so)
#set_property(TARGET leveldb  PROPERTY IMPORTED_LOCATION ${locallib} )
#set (localInc  ${CMAKE_HOME_DIRECTORY}/leveldb/include ${CMAKE_HOME_DIRECTORY}/leveldb  ${CMAKE_HOME_DIRECTORY}/Pluma/include )

#set (localLib ${CMAKE_HOME_DIRECTORY}/leveldb/libleveldb.so)
#set (localInc ${CMAKE_HOME_DIRECTORY}/leveldb/include  ${CMAKE_HOME_DIRECTORY}/Pluma/include/ )

set (localLib )
set (localInc  ${CMAKE_HOME_DIRECTORY}/Pluma/include/ )

# STRING(REGEX REPLACE "/usr/lib/libxmlrpc.so" "/usr/lib64/libxmlrpc.so" ${XMLRPC_LIBRARIES})
if(EXISTS "/usr/lib/x86_64-linux-gnu/libxmlrpc_abyss.so" )


  set ( XMLRPCPATH /usr/lib/x86_64-linux-gnu )

else()

  set ( XMLRPCPATH /usr/lib )

endif()

set (XMLRPC_LIBRARIES ${XMLRPCPATH}/libxmlrpc_abyss.so            ${XMLRPCPATH}/libxmlrpc_server_pstream++.so
  ${XMLRPCPATH}/libxmlrpc_client.so           ${XMLRPCPATH}/libxmlrpc_server.so
  ${XMLRPCPATH}/libxmlrpc_client++.so         ${XMLRPCPATH}/libxmlrpc_server++.so
  ${XMLRPCPATH}/libxmlrpc_cpp.so              ${XMLRPCPATH}/libxmlrpc.so
  ${XMLRPCPATH}/libxmlrpc_packetsocket.so     ${XMLRPCPATH}/libxmlrpc++.so
  ${XMLRPCPATH}/libxmlrpc_server_abyss.so     ${XMLRPCPATH}/libxmlrpc_util.so
  ${XMLRPCPATH}/libxmlrpc_server_abyss++.so   ${XMLRPCPATH}/libxmlrpc_xmlparse.so
  ${XMLRPCPATH}/libxmlrpc_server_cgi.so       ${XMLRPCPATH}/libxmlrpc_xmltok.so
  ${XMLRPCPATH}/libxmlrpc_server_cgi++.so

  )


if(EXISTS   /usr/lib/libtcmalloc.so )
  set (LBTC   /usr/lib/libtcmalloc.so )
else()
  set (LBTC     /usr/lib/libtcmalloc_minimal.so.4 )
endif()





set (GCRYPT_LIBRARY_DIRS ${GCRYPT_LIBRARIES} )

set (GCRYPT_INCLUDE_DIRS ${L_PATH}/include )

SET (SPECIAL_INCLUDES /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/gtkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/gdkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/sigc++-2.0/include/ /usr/include/sigc++-2.0/sigc++/  /usr/lib/x86_64-linux-gnu/glib-2.0/include/ /usr/lib/x86_64-linux-gnu/glibmm-2.4/include/  /usr/include/gtksourceviewmm-3.0 /usr/include/gtksourceview-3.0/)

set (SPECIAL_LIBS   ${LBTC} /usr/lib/x86_64-linux-gnu/libleveldb.so /usr/lib/x86_64-linux-gnu/libdl.so  /usr/lib/x86_64-linux-gnu/libm.so /usr/lib/x86_64-linux-gnu/libgtksourceview-3.0.so /usr/lib/x86_64-linux-gnu/libgtksourceviewmm-3.0.so )
