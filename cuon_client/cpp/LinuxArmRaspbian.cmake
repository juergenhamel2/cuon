



set(L_PATH "/usr")
set(L_PATH_LIB "/usr/lib")
set (CUON_OUTPUTDIR "${CMAKE_BINARY_DIR}/arm" )
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CUON_OUTPUTDIR})
set (CMAKE_CXX_FLAGS "-g -std=c++0x " )
set (GCRYPT_LIBRARIES ${L_PATH_LIB}/libgcrypt.so )
find_package (BZip2)	   


find_package(PkgConfig)
PKG_CHECK_MODULES(GTK, gtk+-3.0 REQUIRED)
pkg_check_modules(GTKMM  gtkmm-3.0 REQUIRED )
#pkg_check_modules(GTKSOURCEVIEWMM gtksourceviewmm REQUIRED )
set (GTKSOURCEVIEWMM_INCLUDE /usr/include/gtksourceviewmm-3.0 /usr/lib/gtksourceviewmm-3.0/include /usr/include/gtksourceview-3.0/ )

set (localLib )

include(FindXMLRPCC.cmake)


#FIND_PACKAGE(XMLRPC REQUIRED c++)
message( "Find xmlrpc libs " ${XMLRPC_LIBRARIES} )
message( "and this xmlrpc includes "  ${XMLRPC_INCLUDE_DIRS} )
message( "this is gtksourceviev: " ${GTKSOURCEVIEWMM_LIBRARIES} )
message( "this is gcrypt: " ${LIBGCRYPT_LIBS} )




set (GCRYPT_LIBRARY_DIRS ${LIBGCRYPT_LIBS} )
set (GCRYPT_LIBRARIES ${LIBGCRYPT_LIBS} )

set (GCRYPT_INCLUDE_DIRS ${L_PATH}/include )

SET (SPECIAL_INCLUDES /usr/include/gtkmm-3.0 /usr/lib/arm-linux-gnueabihf/gtksourceviewmm-3.0/include )
message( "this is special Includes: " ${SPECIAL_INCLUDES} )
set (SPECIAL_LIBS  /usr/lib/arm-linux-gnueabihf/libgcrypt.so  /lib/arm-linux-gnueabihf/libdl.so.2 /usr/lib/libgtksourceviewmm-3.0.so /usr/lib/arm-linux-gnueabihf/libgtksourceview-3.0.so /usr/lib/arm-linux-gnueabihf/libleveldb.so /usr/lib/arm-linux-gnueabihf/libpthread.so wiringPi)


message( "this is special Libs: " ${SPECIAL_LIBS} )



set (pi_control cuon/PI/pi_control.cpp cuon/PI/SinglePi.cpp cuon/PI/Steppermotor.cpp cuon/PI/lcd2x16.cpp  )