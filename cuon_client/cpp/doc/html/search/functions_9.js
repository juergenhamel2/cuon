var searchData=
[
  ['lcd2x16',['lcd2x16',['../d5/d96/classlcd2x16.html#a5954aedae42956e07331c5773da4b0fa',1,'lcd2x16::lcd2x16(int fd)'],['../d5/d96/classlcd2x16.html#a5954aedae42956e07331c5773da4b0fa',1,'lcd2x16::lcd2x16(int fd)']]],
  ['listentry',['listEntry',['../d6/d73/classlistEntry.html#ad92225c628f5677a0d0badc3374ee5db',1,'listEntry']]],
  ['listfiles',['listFiles',['../da/dd7/namespacepluma_1_1dir.html#aadb4326a70faccc39c99ab1a325c8af9',1,'pluma::dir']]],
  ['lists_5faddresses',['lists_addresses',['../de/dd4/classlists__addresses.html#a6b8aae3ab54b1f455e65ecb767295c4b',1,'lists_addresses']]],
  ['lists_5farticles',['lists_articles',['../d4/d82/classlists__articles.html#a5dd2aae0c6f9ea2ad3320217541573ec',1,'lists_articles']]],
  ['load',['load',['../d2/d58/classSingledata.html#a2fd81004c1019456ec02566eca6110ae',1,'Singledata::load()'],['../d3/df9/classpluma_1_1PluginManager.html#aa00400d23efa8a8f94e44dd1c5bf54e6',1,'pluma::PluginManager::load(const std::string &amp;path)'],['../d3/df9/classpluma_1_1PluginManager.html#a866127044950094bb789260bc15a2874',1,'pluma::PluginManager::load(const std::string &amp;folder, const std::string &amp;pluginName)'],['../d2/d1f/classpluma_1_1DLibrary.html#a32400d8e50c0f03cb525ca7500b4516e',1,'pluma::DLibrary::load()']]],
  ['loaddocument',['loadDocument',['../d5/db3/classSingleDMS.html#a8585619ded749c08a9b6e0ae419ffb92',1,'SingleDMS']]],
  ['loadentries',['loadEntries',['../d9/d64/classgladeXml.html#a1b8833ecd48f7996a185766d1c445590',1,'gladeXml']]],
  ['loadentryfield',['loadEntryField',['../d2/d58/classSingledata.html#a1be9ce4d66928a681c162b8555be608f',1,'Singledata']]],
  ['loadfromfolder',['loadFromFolder',['../d3/df9/classpluma_1_1PluginManager.html#a4d892e345288c26dea091d62ee2b03eb',1,'pluma::PluginManager']]],
  ['loadglade',['loadGlade',['../d9/d64/classgladeXml.html#a6420e3a43644e49e565c0242de4fc3c2',1,'gladeXml::loadGlade(string gladeName, string sMainWindow, string gladePath)'],['../d9/d64/classgladeXml.html#a7bb9a0023f6036396eeb90f6cdeb3ada',1,'gladeXml::loadGlade(string gladeName, string sMainWindow, int iType=0)']]],
  ['loadmainlogo',['loadMainLogo',['../d5/db3/classSingleDMS.html#aa464a2899098cbd9412c317afd39bf00',1,'SingleDMS']]],
  ['loadnotes0savedocument',['loadNotes0SaveDocument',['../d5/db3/classSingleDMS.html#ab00c94ce36002219df9af493cf58ce71',1,'SingleDMS']]],
  ['loadprofileid',['loadProfileID',['../d4/db5/classcuonclients.html#a97419139453868cf96d24fb2f1a3d206',1,'cuonclients']]],
  ['localcache',['localCache',['../da/d70/classlocalCache.html#aa5ecbbc7d3c6f9fef997bf323e9896ab',1,'localCache']]],
  ['login',['login',['../d8/da6/classlogin.html#a017b3b2bb35d937a40972a8de394829d',1,'login']]],
  ['ltrim',['ltrim',['../d3/d15/classbasics.html#a79b0a7267c285687e03c3f02241bc14e',1,'basics::ltrim()'],['../df/d79/classiniReader.html#a5d62f8c45a4ed8921ec36427f8f6b828',1,'iniReader::ltrim()']]]
];
