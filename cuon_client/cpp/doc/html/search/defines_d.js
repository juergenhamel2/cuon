var searchData=
[
  ['par_5fc_5ftag',['PAR_C_TAG',['../d0/d03/folderman_8cpp.html#a03116878745fb91904340d722f6357ff',1,'folderman.cpp']]],
  ['par_5fo_5ftag',['PAR_O_TAG',['../d0/d03/folderman_8cpp.html#a6004c988a79b17d1424692872a919963',1,'folderman.cpp']]],
  ['percent_5ftag',['PERCENT_TAG',['../d0/d03/folderman_8cpp.html#ac63275f99769f93f5a435edfddcb6965',1,'folderman.cpp']]],
  ['pipe_5ftag',['PIPE_TAG',['../d0/d03/folderman_8cpp.html#adf34676df4198b5d16e655ce01193178',1,'folderman.cpp']]],
  ['pipe_5ftimeout',['PIPE_TIMEOUT',['../da/dec/OCClientInterface_8cpp.html#a1362a695a8fe97fdb4342e23144cc944',1,'OCClientInterface.cpp']]],
  ['platform_5fid',['PLATFORM_ID',['../d1/d83/CMakeFiles_22_88_810_82_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp'],['../d1/dca/CMakeFiles_22_88_811_82_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp'],['../df/d99/CMakeFiles_22_88_812_82_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp'],['../d4/de5/CMakeFiles_23_82_82_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp'],['../dc/de3/CMakeFiles_23_83_81_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp'],['../d5/d83/CMakeFiles_23_84_81_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp'],['../da/da1/CMakeFiles_23_85_80-rc3_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp'],['../dc/d1e/CMakeFiles_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp'],['../dd/de2/owncloud__client_2client_2CMakeFiles_23_82_82_2CompilerIdCXX_2CMakeCXXCompilerId_8cpp.html#adbc5372f40838899018fadbc89bd588b',1,'PLATFORM_ID():&#160;CMakeCXXCompilerId.cpp']]],
  ['pluma',['PLUMA',['../d7/dfc/os__info_8hpp.html#acfbf7b5a4e7138b753c99a0389ec1dee',1,'os_info.hpp']]],
  ['pluma_5f2string',['PLUMA_2STRING',['../d3/da1/Pluma_8hpp.html#a655a9b4cc67a05b387bbda2a4076c8b7',1,'Pluma.hpp']]],
  ['pluma_5fapi',['PLUMA_API',['../df/db4/Config_8hpp.html#a9c660ce6f4d4b2ef609b4a538508e44f',1,'Config.hpp']]],
  ['pluma_5fconnector',['PLUMA_CONNECTOR',['../dd/d44/Connector_8hpp.html#a290c9e7ae48b173be5ea2a164002cace',1,'Connector.hpp']]],
  ['pluma_5finherit_5fprovider',['PLUMA_INHERIT_PROVIDER',['../d3/da1/Pluma_8hpp.html#a69931e81aba0a33e3dea85bba5023621',1,'Pluma.hpp']]],
  ['pluma_5fprovider_5fheader',['PLUMA_PROVIDER_HEADER',['../d3/da1/Pluma_8hpp.html#aa944fd24ba7a3b090d1289f6cb67451a',1,'Pluma.hpp']]],
  ['pluma_5fprovider_5fheader_5fbegin',['PLUMA_PROVIDER_HEADER_BEGIN',['../d3/da1/Pluma_8hpp.html#a43b704416a79999c19d962e5d9809184',1,'Pluma.hpp']]],
  ['pluma_5fprovider_5fheader_5fend',['PLUMA_PROVIDER_HEADER_END',['../d3/da1/Pluma_8hpp.html#a78a8297ace06e3944e9f4d850f160a1f',1,'Pluma.hpp']]],
  ['pluma_5fprovider_5fsource',['PLUMA_PROVIDER_SOURCE',['../d3/da1/Pluma_8hpp.html#aa18146c723deba3982670f8faf7a74c8',1,'Pluma.hpp']]]
];
