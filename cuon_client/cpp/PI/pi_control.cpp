#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include "string.h"
#include <sqlite3.h>

// install: git clone git://git.drogon.net/wiringPi and do ./build there
#include <wiringPi.h> // Include WiringPi library!

#include <wiringPiI2C.h>// Include WiringPi I2C library!

#include "lcd2x16.hpp"

using namespace std;

// test
// Pin number declarations. We're using the Broadcom chip pin numbers.

const int pwmPin = 18; // PWM LED - Broadcom pin 18, P1 pin 12
const int ledPin = 23; // Regular LED - Broadcom pin 23, P1 pin 16
const int butPin = 17; // Active-low button - Broadcom pin 17, P1 pin 11

const int pwmValue = 75; // Use this to set an LED brightness


lcd2x16* lcd; 
int main(void)
{



     int _lcd =0x27 ;
     lcd = new lcd2x16(_lcd);
   
     
     // Setup stuff:
     wiringPiSetupGpio(); // Initialize wiringPi -- using Broadcom pin numbers


     pinMode(pwmPin, PWM_OUTPUT); // Set PWM LED as PWM output
     pinMode(ledPin, OUTPUT);     // Set regular LED as output
     pinMode(butPin, INPUT);      // Set button as INPUT
     pullUpDnControl(butPin, PUD_UP); // Enable pull-up resistor on button

     printf("Blinker is running! Press CTRL+C to quit.\n");
     int fd  = wiringPiI2CSetup(_lcd );
     cout << "Init result: "<< fd << endl;


     lcd->init();
     
lcd->write(0, 0, "Greetings!");

     // Loop (while(1)):
     while(1)
     {
      
          pwmWrite(pwmPin, 1024 - pwmValue); // PWM LED at dim setting
          // Do some blinking on the ledPin:
          digitalWrite(ledPin, HIGH); // Turn LED ON
          cout << "LED is ON" << endl ;

          delay(1000); // Wait 1500ms
          digitalWrite(ledPin, LOW); // Turn LED OFF
          cout << "LED is OFF" << endl ;
          delay(1000); // Wait 1500ms again
      
     }

     return 0;
}
