#ifndef LCD2X16_HPP_INCLUDED     
#define LCD2X16_HPP_INCLUDED

#include <string>
using namespace std;

class lcd2x16 {
public:

       lcd2x16(int fd);
     ~lcd2x16();

     void write_word(int data);
     void send_command(int comm);
     void send_data(int data);
     void init();
     void clear();
     void write(int x, int y, string sData);
     
};

#endif // LCD2X16_HPP_INCLUDED
