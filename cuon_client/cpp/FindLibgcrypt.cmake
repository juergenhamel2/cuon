# - Try to find the GNU Libgcrypt library               
# Once done this will define
#
#  LIBGCRYPT_FOUND - system has the Libgcrypt library
#  LIBGCRYPT_CFLAGS - the required gcrypt compilation flags
#  LIBGCRYPT_LIBS - The libraries needed to use Libgcrypt
#
# Redistribution and use is allowed according to the terms of the BSD license.
# For details see the accompanying LICENSE.BSD file.
#


# use old style libgrypt detection
# libgcrypt is moving to pkg-config, but earlier version don't have it


message("system at gcrypt = " ${CROSS_SYSTEM}  ${CMAKE_SYSTEM_NAME})
message ("os = " ${OS_NAME} ${OS_SPEC} )

# CLEAR THE vars
set(LIBGCRYPT_LIBS)
set(LIBGCRYPT_CFLAGS)

if(${CROSS_SYSTEM}   STREQUAL "MY_LINUX64")

    if(${OS_NAME} STREQUAL "Debian")

        if(${OS_SPEC} STREQUAL "Wheezy")
            find_program(LIBGCRYPTCONFIG_EXECUTABLE NAMES libgcrypt-config)

            # if libgcrypt-config has been found
            if(LIBGCRYPTCONFIG_EXECUTABLE)

                exec_program(${LIBGCRYPTCONFIG_EXECUTABLE} ARGS --libs RETURN_VALUE _return_VALUE OUTPUT_VARIABLE LIBGCRYPT_LIBS)

                exec_program(${LIBGCRYPTCONFIG_EXECUTABLE} ARGS --cflags RETURN_VALUE _return_VALUE OUTPUT_VARIABLE LIBGCRYPT_CFLAGS)

            endif()


        endif()


     


    else()






            include(CheckIncludeFiles)

            check_include_files(gcrypt.h HAVE_GCRYPT_H)

            if (HAVE_GCRYPT_H)
                set(LIBGCRYPT_HEADERS_FOUND TRUE)
            endif (HAVE_GCRYPT_H)

            if (LIBGCRYPT_HEADERS_FOUND)
                find_library(LIBGCRYPT_LIBS NAMES gcrypt )
            endif (LIBGCRYPT_HEADERS_FOUND)

            if (LIBGCRYPT_LIBS)

                set(LIBGCRYPT_FOUND TRUE)
                message(STATUS "Libgcrypt found: ${LIBGCRYPT_LIBS}")

            elseif (Libgcrypt_FIND_REQUIRED)

                #search in typical paths for libgcrypt-config
                find_program(LIBGCRYPTCONFIG_EXECUTABLE NAMES libgcrypt-config)

                #reset variables
                set(LIBGCRYPT_LIBS)
                set(LIBGCRYPT_CFLAGS)

                # if libgcrypt-config has been found
                if(LIBGCRYPTCONFIG_EXECUTABLE)

                    exec_program(${LIBGCRYPTCONFIG_EXECUTABLE} ARGS --libs RETURN_VALUE _return_VALUE OUTPUT_VARIABLE LIBGCRYPT_LIBS)

                    exec_program(${LIBGCRYPTCONFIG_EXECUTABLE} ARGS --cflags RETURN_VALUE _return_VALUE OUTPUT_VARIABLE LIBGCRYPT_CFLAGS)

                    if(${LIBGCRYPT_CFLAGS} MATCHES "\n")
                        set(LIBGCRYPT_CFLAGS " ")
                    endif(${LIBGCRYPT_CFLAGS} MATCHES "\n")

                    if(LIBGCRYPT_LIBS AND LIBGCRYPT_CFLAGS)
                        set(LIBGCRYPT_FOUND TRUE)
                    endif(LIBGCRYPT_LIBS AND LIBGCRYPT_CFLAGS)

                endif(LIBGCRYPTCONFIG_EXECUTABLE)

                if (LIBGCRYPT_FOUND)
                    if (NOT LibGcrypt_FIND_QUIETLY)
                        message(STATUS "Found libgcrypt: ${LIBGCRYPT_LIBS}")
                    endif (NOT LibGcrypt_FIND_QUIETLY)
                else (LIBGCRYPT_FOUND)
                    if (LibGcrypt_FIND_REQUIRED)
                        message(FATAL_ERROR "Could not find libgcrypt libraries")
                    endif (LibGcrypt_FIND_REQUIRED)
                endif (LIBGCRYPT_FOUND)

                mark_as_advanced(LIBGCRYPT_CFLAGS LIBGCRYPT_LIBS)

            endif (LIBGCRYPT_LIBS)

        endif()   

    endif()

