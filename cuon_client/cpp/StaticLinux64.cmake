 
 message("do the statical link")
 SET(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
 #SET(BUILD_SHARED_LIBRARIES OFF)
# SET(CMAKE_EXE_LINKER_FLAGS "-mostly-static")

 #set(CMAKE_EXE_LINKER_FLAGS -static)

 #set(CMAKE_EXE_LINK_DYNAM IC_C_FLAGS)       # remove -Wl,-Bdynamic
 set(CMAKE_EXE_LINK_DYNAMIC_CXX_FLAGS)
 #set(CMAKE_SHARED_LIBRARY_C_FLAGS)         # remove -fPIC
 #set(CMAKE_SHARED_LIBRARY_CXX_FLAGS)
 set(CMAKE_SHARED_LIBRARY_LINK_C_FLAGS)    # remove -rdynamic
 set(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS)
 # Maybe this works as well, haven't tried yet.
 # set_property(GLOBAL PROPERTY TARGET_SUPPORTS_SHARED_LIBS FALSE)
 set(L_PATH "/usr")
 set(L_PATH_LIB "/usr/lib64")
 set (CUON_OUTPUTDIR "${CMAKE_BINARY_DIR}/bin64" )
 set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CUON_OUTPUTDIR})
 set (CMAKE_CXX_FLAGS "-g -m64  -std=c++0x" )
set ( LDFLAGS "-Wl,--as-needed" )
 find_package (BZip2)	   
 
 set (BZIP2_LIBRARIES /usr/lib/x86_64-linux-gnu/libbz2.so )

set (GCRYPT_LIBRARIES /usr/lib/x86_64-linux-gnu/libgcrypt.so )

find_package(PkgConfig)
PKG_CHECK_MODULES(GTK, gtk+-3.0 REQUIRED)
pkg_check_modules(GTKMM  gtkmm-3.0 REQUIRED )
#pkg_check_modules(GTKSOURCEVIEWMM gtksourceviewmm REQUIRED )
set (GTKSOURCEVIEWMM_INCLUDE /usr/include/gtksourceviewmm-3.0 /usr/lib/gtksourceviewmm-3.0/include /usr/include/gtksourceview-3.0/ )

set (GTKMM_LIBRARIES /usr/local/lib/libfreetype.a
/usr/lib/x86_64-linux-gnu/libglibmm_generate_extra_defs-2.4.a
/usr/lib/x86_64-linux-gnu/libpng12.a
/usr/lib/x86_64-linux-gnu/libcairo-gobject.a
/usr/lib/x86_64-linux-gnu/libpango-1.0.a
/usr/lib/x86_64-linux-gnu/libpangocairo-1.0.a
/usr/lib/x86_64-linux-gnu/libgobject-2.0.a

/usr/lib/x86_64-linux-gnu/libcairo.a
/usr/lib/x86_64-linux-gnu/libxml2.a
/usr/lib/x86_64-linux-gnu/libgiomm-2.4.a
/usr/lib/x86_64-linux-gnu/libcrypt.a
/usr/lib/x86_64-linux-gnu/libglibmm-2.4.a
/usr/lib/x86_64-linux-gnu/libpng.a
/usr/lib/x86_64-linux-gnu/libz.a
/usr/lib/x86_64-linux-gnu/libXft.a
)


include(FindXMLRPCC.cmake)


set (XMLRPC_LIBRARIES /usr/lib/libxmlrpc_server_abyss.a
/usr/lib/libxmlrpc_server++.a
/usr/lib/libxmlrpc++.a
/usr/lib/libxmlrpc_abyss.a
/usr/lib/libxmlrpc_packetsocket.a
/usr/lib/libxmlrpc_server_abyss++.a
/usr/lib/libxmlrpc_client++.a
/usr/lib/libxmlrpc_client.a
/usr/lib/libxmlrpc_server_cgi++.a
/usr/lib/libxmlrpc.a
/usr/lib/libxmlrpc_server_cgi.a
/usr/lib/libxmlrpc_server.a
/usr/lib/libxmlrpc_xmltok.a
/usr/lib/libxmlrpc_cpp.a
/usr/lib/libxmlrpc_xmlparse.a
/usr/lib/libxmlrpc_util.a
/usr/lib/libxmlrpc_server_pstream++.a
)

set (GCRYPT_LIBRARIES /usr/lib/x86_64-linux-gnu/libgcrypt.a)

add_library (leveldb STATIC IMPORTED ) 
 set (localLib  ${CMAKE_HOME_DIRECTORY}/leveldb/libleveldb.a)
 set_property(TARGET leveldb  PROPERTY IMPORTED_LOCATION ${locallib} )
set (localInc  ${CMAKE_HOME_DIRECTORY}/leveldb/include/levldb/ ${CMAKE_HOME_DIRECTORY}/leveldb  ${CMAKE_HOME_DIRECTORY}/Pluma/include/ )



#FIND_PACKAGE(XMLRPC REQUIRED c++)
message( "Find xmlrpc libs " ${XMLRPC_LIBRARIES} )
message( "and this xmlrpc includes "  ${XMLRPC_INCLUDE_DIRS} )
message( "this is gtksourceviev: " ${GTKSOURCEVIEWMM_LIBRARIES} )
message( "this is gcrypt: " ${LIBGCRYPT_LIBS} )




set (GCRYPT_INCLUDE_DIRS ${L_PATH}/include )

SET (SPECIAL_INCLUDES /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/gtkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/gdkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/sigc++-2.0/include/ /usr/include/sigc++-2.0/sigc++/  /usr/lib/x86_64-linux-gnu/glib-2.0/include/ /usr/lib/x86_64-linux-gnu/glibmm-2.4/include/  /usr/include/gtksourceviewmm-3.0 /usr/include/gtksourceview-3.0/ /usr/lib/x86_64-linux-gnu/glibmm-2.4/include/ /usr/lib/x86_64-linux-gnu/giomm-2.4/include/ /usr/lib/x86_64-linux-gnu/pangomm-1.4/include/ /usr/lib/x86_64-linux-gnu/gtksourceviewmm-3.0/include/ )

set (SPECIAL_LIBS /usr/lib/libtcmalloc.a /usr/lib/x86_64-linux-gnu/libdl.a /usr/lib/x86_64-linux-gnu/libm.a /usr/lib/x86_64-linux-gnu/libgtksourceview-3.0.a /usr/lib/libgtksourceviewmm-3.0.a)

set (TARGET_LIBS /usr/lib/x86_64-linux-gnu/libpthread.so /usr/lib/libsnappy.so /usr/lib/x86_64-linux-gnu/libleveldb.a )