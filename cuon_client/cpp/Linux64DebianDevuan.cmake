



set(L_PATH "/usr")
set(L_PATH_LIB "/usr/lib")
set (CUON_OUTPUTDIR "${CMAKE_BINARY_DIR}/bin64" )
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CUON_OUTPUTDIR})
set (CMAKE_CXX_FLAGS "-Wall -g -m64 -std=c++11 " )
#add_definitions (-D_GLIBCXX_USE_CXX11_ABI=0)


set (GCRYPT_LIBRARIES /usr/lib/x86_64-linux-gnu/libgcrypt.so )
find_package (BZip2)	   
set (BZIP2_LIBRARIES /usr/lib/x86_64-linux-gnu/libbz2.so )

find_package(PkgConfig)
PKG_CHECK_MODULES(GTK, gtk+-3.0 REQUIRED)
pkg_check_modules(GTKMM  gtkmm-3.0 REQUIRED )

#pkg_check_modules(GTKSOURCEVIEWMM gtksourceviewmm REQUIRED )
set (GTKSOURCEVIEWMM_INCLUDE /usr/include/gtksourceviewmm-3.0 /usr/lib/gtksourceviewmm-3.0/include /usr/include/gtksourceview-3.0/ )



#set (LevelDB  ${CMAKE_HOME_DIRECTORY}/leveldb/db/builder.cc          ${CMAKE_HOME_DIRECTORY}/leveldb/db/log_reader.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/c.cc                ${CMAKE_HOME_DIRECTORY}/leveldb/db/log_test.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/corruption_test.cc  ${CMAKE_HOME_DIRECTORY}/leveldb/db/log_writer.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/c_test.c            ${CMAKE_HOME_DIRECTORY}/leveldb/db/memtable.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/db_bench.cc         ${CMAKE_HOME_DIRECTORY}/leveldb/db/repair.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/dbformat.cc         ${CMAKE_HOME_DIRECTORY}/leveldb/db/skiplist_test.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/dbformat_test.cc    ${CMAKE_HOME_DIRECTORY}/leveldb/db/table_cache.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/db_impl.cc          ${CMAKE_HOME_DIRECTORY}/leveldb/db/version_edit.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/db_iter.cc          ${CMAKE_HOME_DIRECTORY}/leveldb/db/version_edit_test.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/db_test.cc          ${CMAKE_HOME_DIRECTORY}/leveldb/db/version_set.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/filename.cc         ${CMAKE_HOME_DIRECTORY}/leveldb/db/version_set_test.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/filename_test.cc    ${CMAKE_HOME_DIRECTORY}/leveldb/db/write_batch.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/leveldb_main.cc     ${CMAKE_HOME_DIRECTORY}/leveldb/db/write_batch_test.cc
 #)


#add_library (leveldb SHARED IMPORTED ) 
# set (localLib  ${CMAKE_HOME_DIRECTORY}/leveldb/libleveldb.so)
# set_property(TARGET leveldb  PROPERTY IMPORTED_LOCATION ${locallib} )
#set (localInc  ${CMAKE_HOME_DIRECTORY}/leveldb/include/levldb/ ${CMAKE_HOME_DIRECTORY}/leveldb  ${CMAKE_HOME_DIRECTORY}/Pluma/include/ )


#include(FindXMLRPCC.cmake)
find_package(XMLRPC REQUIRED c++)

#PKG_CHECK_MODULES(XMLRPC libxmlrpc-c  REQUIRED )

# STRING(REGEX REPLACE "/usr/lib/libxmlrpc.so" "/usr/lib64/libxmlrpc.so" ${XMLRPC_LIBRARIES})
if(EXISTS "/usr/lib/x86_64-linux-gnu/libxmlrpc_abyss.so" )


  set ( XMLRPCPATH /usr/lib/x86_64-linux-gnu )
  
else()
  
set ( XMLRPCPATH /usr/lib )

endif()

set (XMLRPC_LIBRARIES ${XMLRPCPATH}/libxmlrpc_abyss.so            ${XMLRPCPATH}/libxmlrpc_server_pstream++.so
 ${XMLRPCPATH}/libxmlrpc_client.so           ${XMLRPCPATH}/libxmlrpc_server.so
 ${XMLRPCPATH}/libxmlrpc_client++.so         ${XMLRPCPATH}/libxmlrpc_server++.so
 ${XMLRPCPATH}/libxmlrpc_cpp.so              ${XMLRPCPATH}/libxmlrpc.so
 ${XMLRPCPATH}/libxmlrpc_packetsocket.so     ${XMLRPCPATH}/libxmlrpc++.so
 ${XMLRPCPATH}/libxmlrpc_server_abyss.so     ${XMLRPCPATH}/libxmlrpc_util.so
 ${XMLRPCPATH}/libxmlrpc_server_abyss++.so   ${XMLRPCPATH}/libxmlrpc_xmlparse.so
 ${XMLRPCPATH}/libxmlrpc_server_cgi.so       ${XMLRPCPATH}/libxmlrpc_xmltok.so
${XMLRPCPATH}/libxmlrpc_server_cgi++.so

)


if(EXISTS   /usr/lib/libtcmalloc.so )
  set (LBTC   /usr/lib/libtcmalloc.so )
else()
  set (LBTC     /usr/lib/libtcmalloc_minimal.so.4 )
endif()



message( "Find xmlrpc libs " ${XMLRPC_LIBRARIES} )
message( "and this xmlrpc includes "  ${XMLRPC_INCLUDE_DIRS} )
message( "this is gtksourceviev: " ${GTKSOURCEVIEWMM_LIBRARIES} )
message( "this is gcrypt: " ${LIBGCRYPT_LIBS} )
message( "this is gtkmm: " ${GTKMM_LIBRARIES} )

#set (GTKMM_LIBRARIES  )
set (GCRYPT_LIBRARY_DIRS ${LIBGCRYPT_LIBS} )
#set (GCRYPT_LIBRARIES ${LIBGCRYPT_LIBS} )

set (GCRYPT_INCLUDE_DIRS ${L_PATH}/include )

SET (SPECIAL_INCLUDES /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/gtkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/gdkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/sigc++-2.0/include/ /usr/include/sigc++-2.0/sigc++/  /usr/lib/x86_64-linux-gnu/glib-2.0/include/ /usr/lib/x86_64-linux-gnu/glibmm-2.4/include/  /usr/include/gtksourceviewmm-3.0 /usr/include/gtksourceview-3.0/ /usr/lib/x86_64-linux-gnu/glibmm-2.4/include/ /usr/lib/x86_64-linux-gnu/giomm-2.4/include/ /usr/lib/x86_64-linux-gnu/pangomm-1.4/include/ /usr/lib/x86_64-linux-gnu/gtksourceviewmm-3.0/include/  )

set (SPECIAL_LIBS   ${LBTC}   /usr/lib/x86_64-linux-gnu/libdl.so /usr/lib/x86_64-linux-gnu/libm.so /usr/lib/x86_64-linux-gnu/libgtksourceview-3.0.so /usr/lib/libgtksourceviewmm-3.0.so )