  #ifndef SINGLEDATA_HPP        
#define SINGLEDATA_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>
#include <cuon/Databases/datatypes.hpp>
#include <cuon/Windows/setOfEntries.hpp>
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/dataEntry.hpp>
#include <cuon/Windows/gladeXml.hpp>
#include <cuon/Encoder/smallBase64.hpp>

class Singledata : public datatypes {

public:
     string sNameOfTable ;
     string sSort;
     string sWhere;
           int sep_info_1 ;
     int sep_info_2 ;
     int sep_info_3 ;
     int tmp_ID ;
     int ModulNumber ;
     sigc::connection selection_signal_handler ;
      smallBase64* base64 ;
   
     //names of database fields
    
     vector < string> a1;
     //tree header
     
     vector < string> h1;
     // type
     
     vector < string> t1;


     // names of tree colums
    
     vector < string> c1;
     //Searchfields
     vector <vector <string> > liSearchfields ;
     string* sData ;
     myXmlRpc x1;
     Record firstRecord ;
     int ID;
     string sTree;
     map<string,vector <string> > dicFields ;
     map <string,string> dicEntries ;
     vector <string> liHeader ;
     GtkCellRenderer     *renderer;
     gladeXml glx ;
     GtkWidget           *view;
     Gtk::TreeView *pTree1;
     setOfEntries oEntries;
     map<string, dataEntry> dataEntries ;
     listEntry newEntry;
     dataEntry _entry;
     Glib::RefPtr<Gtk::ListStore> listStore;
     Glib::RefPtr<Gtk::TreeView::Selection> selection ;

     Gtk::TreeModel::Row row ;
     Columns cols ;
     Gtk::TreeIter   iter;
     Gtk::TreePath path;
     Gtk::Statusbar *pStatusbar ;
     Gtk::TreeViewColumn *pColumn ;
     
     vector<string> aFields ;

     Singledata();
     ~Singledata();


     void loadEntryField(string sName);
     void load(int id);
     void newData();
     void save( vector <string> liBigEntries = {});
     void saveAM( vector <string> liBigEntries = {});
     
     void newRecord();
    
     virtual void init();
     void saveValues();
     void saveValuesBE();
     void deleteRecord();
     void setDicEntries(setOfEntries _entries);
     void getListEntries();
     void setLiFields();
     void setTree1();
     void setListEntries();
     virtual void clearAllFields();
     virtual void clearAllOtherFields();

     void fillEntries(int i);
     virtual void fillOtherEntries();
     virtual void fillExtraEntries();
     void treeSelectRow();
     void setNullIter();
     void readEntries();
     virtual void readExtraEntries();
     void on_selection_changed();
     void setWindow(Glib::RefPtr<Gtk::Builder>) ;
     virtual void setTreeFields();
     virtual void setStatusbar();
     virtual string getStatusbar();
     virtual void setSearchString();
     virtual string postFindData(string searchString);
     virtual void readNonWidgetEntries();

     string showPDF(string* pdf_file, string sType="PDF",string sPrinter="Standard");
     void setListstore();
	 string getFileExe(string sSuffix = "pdf");
     string* decodeData(string *sFile);
};

#endif

