 #ifndef LOCALCACHE_HPP
#define LOCALCACHE_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <leveldb/db.h>

#include <cuon/Databases/datatypes.hpp>

using namespace leveldb ;


class localCache : public datatypes {

public:

     bool cacheOK ;
  
     Options options;
     Status dbStatusKeys;
     Status dbStatus;
     string path2CacheKeys ;
     string path2Cache ;
     ReadOptions rO ;
     WriteOptions wO;
     ReadOptions rOKeys ;
     WriteOptions wOKeys;
     localCache();
     ~localCache();

       
     string* getCache(string sKey);
     bool writeCache(string sKey, string sData);
     
     string getKCache(string sKey);
     bool writeKCache(string sKey, string sData);
     
     string* getSCache(string sKey);
     bool writeSCache(string sKey, string sData);
   
};

#endif
