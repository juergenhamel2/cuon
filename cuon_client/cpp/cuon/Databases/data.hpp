#ifndef DATA_HPP_INCLUDED
#define  DATA_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
//#include <cuon/Clients/SingleClient.hpp>
//#include <cuon/Addresses/SingleAddress.hpp>
#define tab_Data 0


class data:public windows
{
    public:


        data();
         ~data();

     void on_bImport_clicked();
     void on_bExport_clicked();
     void on_filechooser_ctrl_file_activated();
     void on_filechooser_ctrl_selection_changed();
     void setNewFilename();
     string getFileData(string sFileName);
     
     string sFile;

    


    protected:

    private:

};

#endif //  DATA_HPP_INCLUDED
