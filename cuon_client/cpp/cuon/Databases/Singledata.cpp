         /*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]            

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

#include <map>
#include <list>
#include <gtkmm.h>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <cuon/Databases/Singledata.hpp>
#include <cuon/Windows/setOfEntries.hpp>
#include <cuon/Windows/dataEntry.hpp>
#include <cuon/Databases/datatypes.hpp>
#include <chrono>
#include <cuon/Encoder/smallBase64.hpp>

Singledata::Singledata()
{

     base64 = new smallBase64();
     sNameOfTable = "EMPTY";
     sSort ;
     a1 = {} ;
     h1 = {} ;
     t1 = {} ;
     c1 = {} ;
     
     ModulNumber = 0;
     sep_info_1 = 0;
     sep_info_2 = 0;
     sep_info_3 = 0;
     
     
     //firstRecord = new Record();
     ID = 0;
     printlog( " Test init singledata ") ;
     renderer = gtk_cell_renderer_text_new ();

}
Singledata::~Singledata()
{
     if (base64) delete base64;
     
}

void Singledata::newRecord(){
     ID = -1;
     
}
void Singledata::init(){
     setTreeFields();
     setLiFields();
}

void Singledata::loadEntryField(string sName)
{

     printlog(".....................................loadEntry..............................................");
     
     setOfEntries _entries = glx.loadEntries(sName);
     setDicEntries(_entries);
     printlog(".....................................End loadEntry..............................................");

}
void  Singledata::load(int _id)
{
     // liRecords, self.firstRecord,  self.ID = self.rpc.callRP('Database.loadData', self.sNameOfTable, record, self.dicInternetUser, dicDetail)
     if(_id>0){
          vector<Record> oResultSet ;
          x1.callRP("Database.loadOneData",x1.add(sNameOfTable, _id), oResultSet);
          printlog ("load new data id 0 = " + toString(_id) );
          firstRecord = oResultSet.at(0);
          ID = firstRecord.row_int["id"];
          printlog("load new data id = " ,ID );
     }
     

}
void  Singledata::newData()
{

     ID = -1;
     clearAllFields();

}
void  Singledata::save(vector<string> liBigEntries)
{

     printlog( "Save Data = " ,ID ) ;
     readEntries();
     printlog( "Save Data = ", ID ) ;
     saveValues();

}

void  Singledata::saveAM(vector<string> liBigEntries)
{

     
  
  
     saveValues();

}

void Singledata::deleteRecord(){
     string s;
     x1.callRP("Database.deleteRecord",x1.add(sNameOfTable,ID),s);

}
void  Singledata::saveValues()
{

     //liResult = self.rpc.callRP('Database.saveRecord',self.sNameOfTable, self.ID, dicValues, self.sqlDicUser
     sendValues sv ;
     sv.sNameOfTable = sNameOfTable ;
     sv.id = ID ;
     sv.Values = dicValues ;

     x1.callRP("Database.saveRecord",x1.add(sv), ID);
     printlog( "ID by SingleData = ", ID ) ;

}

void  Singledata::saveValuesBE()
{
     
     sendValues sv ;
     sv.sNameOfTable = sNameOfTable ;
     sv.id = ID ;
     sv.Values = dicValuesBE ;

     x1.callRP("Database.saveRecordBE",x1.add(sv), ID);
     
}

void Singledata::setLiFields()
{


     // printlog( "size setLifields Names: " ,a1.size() );
     // printlog(" - Header: ",  h1.size() );
     // printlog(" - Type: " , t1.size()  );
     // printlog(" - Colums: " , c1.size()  );
     
     for (int j=0; j<a1.size(); j++) {
          //cout << j << ",--, " << a1[j] << ", " << c1[j]  << ", " << t1[j]  << ", " << h1[j] <<   endl ;
          
          newEntry.liFields.push_back(a1[j]);
         
          vector <string> vF ;
          vF.push_back(h1[j]);
          vF.push_back(t1[j]);
          vF.push_back(c1[j]);
          vF.push_back(a1[j]);
          dicFields[c1[j]] = vF ;
          aFields.push_back(c1[j]);
          
          cols.addColumn(c1[j],t1[j]);
     }




//     cols.addColumn("name2","string");
     printlog( "end setLifields with size " + to_string(cols.iCol.size()) ) ;

}
void Singledata::setDicEntries(setOfEntries _entries)
{

     oEntries = _entries ;
     newEntry.dicEntries = oEntries.getDicEntries();
     for (auto it = newEntry.dicEntries.begin(); it != newEntry.dicEntries.end();it++){
          printlog("--setDicEntries ---> NewEntry-> " + it->first + " = " + it->second );
     }

}

void Singledata::getListEntries()
{




     // First refresh newEntry
     newEntry.sWhere = sWhere ;
     newEntry.sSort = sSort ;

     printlog("get list entries");
     
     for (auto it = newEntry.dicEntries.begin(); it != newEntry.dicEntries.end();it++){
          printlog("--getListEntries ---> NewEntry-> " + it->first + " = " + it->second );
     }

     ResultSet.clear();
     x1.callRP("Database.getListEntries",x1.add(newEntry),ResultSet );
     // printlog ( "get list entries 2 Size = " ,ResultSet.size() );
     setListEntries();

}

void Singledata::setWindow(Glib::RefPtr<Gtk::Builder> _window)
{
     glx.window = _window ;
     
     ///get statusbar
     try {
          Glib::RefPtr< Glib::Object > o = glx.window->get_object("statusbar1");
          if(o) {
               glx.window->get_widget("statusbar1", pStatusbar);
              
          }
     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }

//    setTree1();
}

void Singledata::setTree1()
{
     glx.window->get_widget(sTree,pTree1);
     pTree1->set_model(listStore = Gtk::ListStore::create(cols));
     selection = pTree1->get_selection() ;
     selection_signal_handler = selection->signal_changed().connect(sigc::mem_fun(this, &Singledata::on_selection_changed) );
     int iNumber = 0;
     typedef vector<string>::const_iterator iS ;
     printlog( "start function setTree1  "  );

     for (iS j= aFields.begin(); j != aFields.end(); j++) {
          string sColumnName = dicFields[*j][2];
          string sHeader = dicFields[*j][0];
          string sType = dicFields[*j][1];
          printlog( "set Column name = " + sColumnName + " with Header = " + sHeader );
          if (sType == "int") {
               //cout << "int-colum " << sColumnName << endl;
              
               pTree1->append_column(sHeader, cols.getIntColumn(sColumnName));
          } else if (sType == "float") {
               pTree1->append_column(sHeader, cols.getFloatColumn(sColumnName));
          }


          else {
               pTree1->append_column(sHeader, cols.getStringColumn(sColumnName));
          }

          // cout << "set Column name = " << sColumnName << endl ;
          pColumn = pTree1->get_column(iNumber);
          pColumn->set_sort_column(iNumber);
          iNumber ++;
          
     }

}
void Singledata::treeSelectRow()
{
     if (!iter) {
          setNullIter();

     }
     pTree1->scroll_to_row(path,0.0);
     //Gtk::TreeModel::Row row = *iter;
     selection->select(iter);

}
void Singledata::setNullIter()
{
     iter = pTree1->get_model()->get_iter("0");
     path = pTree1->get_model()->get_path(iter);

}
void Singledata::on_selection_changed()
{
     //cout << "selected 0.1 "  << endl ;
     Gtk::TreeModel::iterator iter = selection->get_selected();
     //cout << "selected 0.2"  << endl ;
     if(iter) { //If anything is selected
          //cout << "selected 1 "  << endl ;
          Gtk::TreeModel::Row row = *iter;
          // cout << "selected 2 "  << endl ;
          // cout << row[cols.getIntColumn("id")] << " is selected " << endl ;
          fillEntries(row[cols.getIntColumn("id")]);
          setStatusbar();
     }

}

void Singledata::clearAllFields()
{
     printlog("Clear all fields");
     
     typedef map<string,dataEntry>::const_iterator iM ;
     dataEntries = oEntries.Entries;
     for(iM i = dataEntries.begin(); i != dataEntries.end(); i++) {
          _entry = i->second ;
         
          glx.setText(_entry,"");
         
     }
     clearAllOtherFields();
}

void Singledata::clearAllOtherFields(){

}

void Singledata::fillEntries(int i)
{
     load(i);
     printlog("fill Entries for " + toString(i) );
     clearAllFields();
     Gtk::TreeIter _newIter ;
     iter = _newIter ;

     typedef map<string,dataEntry>::const_iterator iM ;
     dataEntries = oEntries.Entries;

     for(iM i = dataEntries.begin(); i != dataEntries.end(); i++) {
          _entry = i->second ;
          printlog( "_entry " +  _entry.get_nameOfEntry() + " verify_type = " + _entry.get_verifyTypeOfEntry() +" type = " + _entry.get_typeOfEntry() ) ;
          // cout << "entry Value = " <<  firstRecord.row_string[_entry.get_nameOfSql()] << endl ;
          if(_entry.get_verifyTypeOfEntry() == "string") {
               string _Value = firstRecord.row_string[_entry.get_nameOfSql()];

               if(_Value == "NONE") _Value = "";
               //cout << "stringValue = " << _Value << endl ;
               glx.setText(_entry,_Value);

          }
         
          else if (_entry.get_verifyTypeOfEntry() == "int") {
               int _Value = firstRecord.row_int[_entry.get_nameOfSql()];
               //cout << "intValue = " << _Value << endl ;
              
               
                    glx.setText(_entry,_Value);
               
          }
          else if (_entry.get_verifyTypeOfEntry() == "float") {
               float _Value = firstRecord.row_float[_entry.get_nameOfSql()];
               //cout << "floatValue = " << _Value << endl ;
              
               
               glx.setText(_entry,toString(_Value) );
               
          }
          else if (_entry.get_verifyTypeOfEntry() == "datetime") {
               string _Value = firstRecord.row_string[_entry.get_nameOfSql()];
               //cout << "datetime Value = " << _Value << endl ;
              
               
                    glx.setText(_entry,_Value);
               
          }


     }
     // printlog("now fill other and extra entries");
     fillOtherEntries();
     fillExtraEntries();
}

void Singledata::fillOtherEntries()
{

}
void Singledata::fillExtraEntries() {}

/** 
 * void Singledata::readEntries()
 * 
 */
void Singledata::readEntries()
{
     selection_signal_handler.block();
     dicValues.clear();
     typedef map<string,dataEntry>::const_iterator iM ;
     dataEntries = oEntries.Entries;

     for(iM i = dataEntries.begin(); i != dataEntries.end(); i++) {
          _entry = i->second ;
          printlog("Entry type at readEntries = ",_entry.get_typeOfEntry() ) ;
          printlog("Name of Entry = ",_entry.get_nameOfEntry()) ;
          vector <string> v1 ;
          if(_entry.get_typeOfEntry() == "combobox") {
               v1.push_back(toString(glx.getCBActive(_entry) ) );
               v1.push_back(_entry.get_verifyTypeOfEntry());
          }

          else if(_entry.get_typeOfEntry() == "checkbox") {
               if(_entry.get_verifyTypeOfEntry() == "bool"){
                    if(toString(glx.getCBState(_entry))== "0"){
                         v1.push_back("false");
                    }
                    else{
                         v1.push_back("true"); 
                    }
                    v1.push_back(_entry.get_verifyTypeOfEntry());         
               }
               else{
                    v1.push_back(toString(glx.getCBState(_entry)) );
                    v1.push_back(_entry.get_verifyTypeOfEntry());
               }
          }
          else if(_entry.get_typeOfEntry() == "radiobutton") {
                if(_entry.get_verifyTypeOfEntry() == "bool"){
                    if(toString(glx.getCBState(_entry))== "0"){
                         v1.push_back("False");
                    }
                    else{
                         v1.push_back("True"); 
                    }
                    v1.push_back(_entry.get_verifyTypeOfEntry());
                }
          }
          else if (_entry.get_typeOfEntry() == "textview"){

               v1.push_back(glx.getText(_entry) );
               v1.push_back(_entry.get_verifyTypeOfEntry());
               
          }
          else {
               v1.push_back(glx.getText(_entry) );
               v1.push_back(_entry.get_verifyTypeOfEntry());
          }
          dicValues[_entry.get_nameOfSql()] = v1;
     }

     readExtraEntries();
     selection_signal_handler.unblock();
     readNonWidgetEntries();
     
}

void Singledata::readNonWidgetEntries(){


}
void Singledata::readExtraEntries()
{
}

void Singledata::setListstore(){

     pTree1->set_model(listStore);
}
void Singledata::setListEntries()
{
     auto start = std::chrono::system_clock::now();
        
     if(sTree.empty()){
          printlog("sTree is empty");
          typedef vector<Record>::const_iterator iV ;
          for(iV i = ResultSet.begin(); i!= ResultSet.end(); i++ ) {
               map <string,string> m1 = i->row_string ;
               map <string,int> m2 = i->row_int ;
               map <string,float> m3 = i->row_float ;
               typedef vector<string>::const_iterator iS ;

               for (iS j= aFields.begin(); j != aFields.end(); j++) {



                    string sSearch = *j;
                    printlog("sSearch = ", sSearch);

                    if (dicFields[sSearch][1] == "int") {
                         string sColumnName = dicFields[sSearch][2];
                         int i1 = m2[dicFields[sSearch][2]] ;
                         if(sColumnName == "id"){
                              tmp_ID = i1;
                         }
                    }
               }
          }
     }
     else{
          
          printlog("sTree is NOT empty");

          tmp_ID = 0;
          // Header = 0, Type = 1, col = 2
          // cout << "dicEntries 0" <<  endl;
          selection_signal_handler.block();
          listStore->clear();
          //pTree1->set_model(listStore);
          
         
     
          // cout << "dicEntries 1" <<  endl;
          typedef vector<Record>::const_iterator iV ;
          for(iV i = ResultSet.begin(); i!= ResultSet.end(); i++ ) {
               map <string,string> m1 = i->row_string ;
               map <string,int> m2 = i->row_int ;
               map <string,float> m3 = i->row_float ;


               row = *(listStore->append());


               typedef vector<string>::const_iterator iS ;
               try{
                    for (iS j= aFields.begin(); j != aFields.end(); j++) {



                         string sSearch = *j;
                         //cout << "string 0 " << sSearch << endl;
                         //cout << "string 2 a0 " << dicFields[sSearch][0] << endl;
                         //cout << "string 2 a1 " << dicFields[sSearch][1] << endl;
                         //cout << "string 2 a2 " << dicFields[sSearch][2] << endl;
                         //cout << "string 2 a3 " << dicFields[sSearch][3] << endl;

                         if (dicFields[sSearch][1] == "string" || dicFields[sSearch][1] == "text") {
                              string s1 = m1[dicFields[sSearch][2]];
                              //cout << "string S1 " << s1 << endl;
                              if (!s1.empty()) {
                                   // cout << "string 1 " << s1 << endl;

                                   string sColumnName = dicFields[sSearch][2];
                                   row[cols.getStringColumn(sColumnName)] = m1[dicFields[sSearch][2]] ;
                              }
                         } else if (dicFields[sSearch][1] == "float") {

                              float f1 = m3[dicFields[sSearch][2]] ;
                              // cout << "float value = " << f1 << endl ;
                              if (!f1) f1 = 0.00;

                              // cout << "string float 1 : " << f1 << endl;
                              string sColumnName = dicFields[sSearch][2];

                              row[cols.getFloatColumn(sColumnName)] = m3[dicFields[sSearch][2]] ;
                         }

                         else {
                              int i1 = m2[dicFields[sSearch][2]] ;
                              printlog("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
                              printlog(  "int value = " + to_string( i1)) ;
                              if (!i1) i1 = 0;

                              // cout << "string int 1 : " << i1 << endl;
                              string sColumnName = dicFields[sSearch][2];
                              // cout << "sColumnName " <<  dicFields[sSearch][2] << endl;
                              row[cols.getIntColumn(sColumnName)] = m2[dicFields[sSearch][2]] ;
                              // cout << "row = " <<  m2[dicFields[sSearch][2]] << endl;
                              printlog("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
     
                         }

                    }
               }
               catch (...) {
                    cerr << "List Entry could not set " << endl;
               }

          }


          selection_signal_handler.unblock();
     
          treeSelectRow();
         
          pTree1->show_all_children();
          auto end = std::chrono::system_clock::now();
          auto elapsed =std::chrono::duration_cast<std::chrono::milliseconds>( end - start) ;
          //cout << "time for setTreefields  = " << elapsed.count()/1000.0 << " sec" << endl ;

     }
}

void Singledata::setTreeFields(){
    
//	vector<string> DatabaseCols ;
     // vector<string> HeaderCols ;
     //vector<string> TreeCols ;
     a1 = {};
     t1 = {};
     h1 = {};
     c1 = {};
     
     x1.callRP("Misc.getTreeInfo",x1.add(sNameOfTable), liSearchfields, a1, t1,h1);
     int iS = 1;
     int iF = 1;
     int iI = 1;
     int iB = 1;
     int iZ = 0;
     typedef vector<string>::const_iterator iterS ;
     printlog( "start check for treecolumns  "  );

     for (iterS j= a1.begin(); j != a1.end(); j++) {

          string sSearch =  *j;
         
          unsigned found = sSearch.rfind(" as ") ;
          string s;
          printlog("****************************************************************************");
          printlog("string value = " + sSearch );
          
          printlog("found = " + toString( found));
          if (found < sSearch.size()){
               s =  sSearch.substr(found+4);
               printlog("substring found  s = " + s);        

          }
          else{
               printlog("substring s = " + sSearch); 
               s =  sSearch;
          }

          printlog("stripchars = " + s );
          // if(*j == "string"){
          c1.push_back(stripchars(s));
          printlog("****************************************************************************");
              
               
          // }
          //else if (*j == "int"){
              
          //    c1.push_back(stripchars(s));
           
               
          // }
          // else if (*j == "float"){
              
          //      f1.push_back(stripchars(s));
           
               
          // }
          // else if (*j == "bool"){
              
          //      b1.push_back(stripchars(s));
           
               
          // }       
     
          //iZ++;
     }
   
	
}

void Singledata::setStatusbar(){
     if(pStatusbar){
          string s1 = getStatusbar();
          if(!s1.empty()){
               pStatusbar->pop(0);
               pStatusbar->push(s1,0);
          }
     }
}
string Singledata::getStatusbar(){
     
     string s1 = "";
     if(ID > 0 && ModulNumber > 0){
          x1.callRP("Misc.getStatusbar",x1.add(ModulNumber,ID),s1);
          if (s1 == "NONE"){
               s1 = "";
          }
     }
     return s1 ;
}

void Singledata::setSearchString(){
     string searchString;
     
     typedef vector< vector<string> >::const_iterator iterS ;
     printlog( "start set Searchstring  "  );
     vector <string> liSearch ;
     for (iterS j= liSearchfields.begin(); j != liSearchfields.end(); j++) {
          vector<string> liValue = *j ;
          if (liValue[1] == "STRING"){
                Gtk::Entry *e1 ;
                glx.window->get_widget(liValue[0], e1);
          
                string _sText = e1->get_text();
                                
                if (! _sText.empty()){
                     int iFound = liValue[2].find("%FIELD%");
                     if (iFound < liValue[2].size()){
                          liSearch.push_back(liValue[2].replace(iFound,7,_sText));
                     }
                     else{
                          liSearch.push_back(liValue[2]);
                     }
                     liSearch.push_back(_sText);
                }
          }
          else if  (liValue[1] == "INT"){
                Gtk::Entry *e1 ;
                glx.window->get_widget(liValue[0], e1);
          
                string _sText = e1->get_text();
                                
                if (! _sText.empty()){
                     string newID = "";
                     string newOp = " ";
                     string sID = stripchars(_sText);
                      for ( std::string::iterator it=sID.begin(); it!=sID.end(); ++it){
                        
                           if (isdigit(*it)){
                                newID += *it ;
                           }
                           else{
                                newOp += *it;
                           }
                      }
                     int iFound = liValue[2].find("%FIELD%");
                     if (iFound < liValue[2].size()){
                          liSearch.push_back(liValue[2].replace(iFound,7,_sText) + newOp);
                     }
                     else{
                          liSearch.push_back(liValue[2] + newOp);
                     }
                     liSearch.push_back(newID);
                }
          }
           else if  (liValue[1] == "CHECKBOX"){
                Gtk::ComboBox *cb1 ;
                glx.window->get_widget(liValue[0], cb1);
                int bNr = cb1->get_active_row_number();
                if (bNr > -1){
                     liSearch.push_back(liValue[2]);
                     liSearch.push_back(toString(bNr));
                }
           }
     }
     
    
     x1.callRP("Misc.getSearchWhere",x1.add(liSearch,FALSE),searchString);
     sWhere = postFindData(searchString);
     
     getListEntries();
     
}

string Singledata::postFindData(string searchString){

     return searchString ;
}


string Singledata::getFileExe(string sSuffix){
     string sExe ;
     x1.callRP("DMS.getFileExePrg", x1.add(sSuffix),sExe);

     return sExe;
}

string* Singledata::decodeData(string *sFile){


     if(sData) delete sData ;
     try{
          sData =  new string(base64->base64_decode(*sFile )) ;
     }
     catch(...){
          printlog("Something wrong with base64 decoding");
          sData = new string("") ;
     }
     

     return sData;

}

string Singledata::showPDF(string* pdf_file,string sType, string sPrinter){

     // printlog("PDF = " + *pdf_file) ;
     string sFilename = getRandomFilename(".pdf");
     printlog("Filename = " + sFilename);
     string* decodeFile =  new string(base64->base64_decode(*pdf_file ));
     printlog("decodeData done");
     writeFile(sFilename, decodeFile);
     string sPrg = getFileExe();
     printlog("exe = " + sPrg);
     vector <string> vPrg {sPrg,sFilename};
     executePrg(vPrg);
     printlog ("back from execute PDF-Viewer with File " + sFilename);
     return sFilename ;
     


}
