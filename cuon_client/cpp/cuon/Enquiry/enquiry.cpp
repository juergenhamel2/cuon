/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>


#include <cuon/Enquiry/enquiry.hpp>

#include <cuon/Articles/SingleArticle.hpp>
#include <Pluma/Connector.hpp>




PLUMA_CONNECTOR


  bool connect(pluma::Host& host){
     // add a  provider to host
     host.add( new enquiryProvider() );
     return true;
 }



enquiry::enquiry()
{
}

int enquiry::initOrder( map <string,string>* dicOrder, bool newOrder, int orderid ,string OrderType){
     tree1Name = "tree1";
     cout << "start enquiry0" << endl ;

     v_new.push_back( "new1");


     menuItems["new"] = v_new;

     v_save.push_back( "save1");

     menuItems["save"] = v_save;

     // v_delete.push_back("Delete1");

     //menuItems["delete"] = v_delete ;

     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;

// Toolbar Buttons
	v_tnew.push_back("tbNew");
	toolbarItems["new"] = v_tnew;

	v_tsave.push_back("tbSave");
	toolbarItems["save"] = v_tsave;

//	v_tdelete.push_back("tbDelete");
//	toolbarItems["delete"] = v_tdelete;



     cout << "start enquiry" << endl ;
     init("enquiry","enquiryMainwindow","enquiry");

     // init db
     singleArticle = new SingleArticle();
     
     singleEnquiry = new SingleEnquiry();
     

     singleEnquiry->setWindow(window) ;
     singleEnquiry->setTree1();

   
     
     cout << "start enquiry tab changed" << endl ;
     tabChanged();

     return 1;
     
}

void enquiry::on_new_activate()
{
     printlog("stock new 0","") ;
     switch (tabOption) {

     case tabenquiry_Enquiry:

          singleEnquiry->newData();
          break;
    
     }
}
void enquiry::on_save_activate()
{
     printlog ("stock save 0" );
     switch (tabOption) {
     case tabenquiry_Enquiry:
          singleEnquiry->save();
          break;
      
     }
}

void enquiry::on_delete_activate() {}
void enquiry::on_bSearch_clicked() {}

void enquiry::tabChanged()
{

     cout << "tab changed at stock" << tabOption << " = " << tabenquiry_Enquiry << endl;
     switch(tabOption) {

     case tabenquiry_Enquiry:
          cout << "first enquiry tab " << endl ;
          if(oldTab < 1) {
               singleEnquiry->getListEntries();
          }
          break;
  
     }
     
     oldTab = tabOption;
}




enquiry::~enquiry() {}
