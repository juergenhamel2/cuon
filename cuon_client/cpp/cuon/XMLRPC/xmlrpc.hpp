#ifndef XMLRPC_HPP
#define XMLRPC_HPP

using namespace std;
#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/client_simple.hpp>
#include <iostream>
#include <string>
#include <map>

#include <cuon/Databases/datatypes.hpp>
#include <global.hpp>

class mXmlRpc: public datatypes {
public:

     //xmlrpc_c::clientSimple myClient;
     
   
     string serverUrl;
     std::vector<xmlrpc_c::value> a1;
     std::vector <void*> v1 ;

     mXmlRpc();

     xmlrpc_c::value callrp(string rp,xmlrpc_c::paramList const& paramList);

};

class myXmlRpc : public mXmlRpc {
private:
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, map <string,string> m1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, map <string,int> m1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, map <string,float> m1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, map <string,tm*> m1 );
     
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, map<string,vector <string> > m1);
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, int i1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, string s1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, double d1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, bool v1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, vector <string> v1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, vector <int> vi1 );


public:
     myXmlRpc();
     ~myXmlRpc();
     xmlrpc_c::paramList addUser( xmlrpc_c::paramList dataServ);


     xmlrpc_c::paramList add(map <string,string> m1 ,bool setUserDic = true);

     xmlrpc_c::paramList add(map <string,string> m1, map <string,string> m2,bool setUserDic= true );
     xmlrpc_c::paramList add(int i1,bool setUserDic = true);
     xmlrpc_c::paramList add(int i1,string s1, bool setUserDic = true);
     xmlrpc_c::paramList add(vector <int > vi1, bool setUserDic = true);
     xmlrpc_c::paramList add(int i1,int i2, bool setUserDic = true);
     xmlrpc_c::paramList add(int i1,vector <string> v1, bool setUserDic = true);
     
     xmlrpc_c::paramList add(string s1,bool setUserDic = true);
     xmlrpc_c::paramList add(string s1, int i1,bool setUserDic = true);
     xmlrpc_c::paramList add(string s1, string s2,bool setUserDic = true);
     xmlrpc_c::paramList add(string s1,Record r1,bool setUserDic = true);
     xmlrpc_c::paramList add(vector <string> v1,bool setUserDic = true);

     xmlrpc_c::paramList add(listEntry e1,bool setUserDic = true);
     xmlrpc_c::paramList add(sendValues sv,bool setUserDic = true);
     xmlrpc_c::paramList add(bool setUserDic = true);
     

     void callRP(string rp,xmlrpc_c::paramList const& paramList,vector <Record> &rs1);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,Record &r1);

     
     void callRP(string rp,xmlrpc_c::paramList const& paramList,string &s1);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,vector <string> &v1);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,int &i1);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,vector <int> &v1);
      void callRP(string rp,xmlrpc_c::paramList const& paramList,float &f1);
      void callRP(string rp,xmlrpc_c::paramList const& paramList,bool &ok);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,map <string,string> &m);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,vector<map <string,string> > &v);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,vector<map <string, vector<string> > > &v);
	 void callRP(string rp,xmlrpc_c::paramList const& paramList,vector <vector <string> > &v1, vector <string> &v2,vector <string> &v3,vector <string> &v4 );

    
} ;




#endif
