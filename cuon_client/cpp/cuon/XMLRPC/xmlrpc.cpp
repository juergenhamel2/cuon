/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/client_simple.hpp>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <map>
#include <list>
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Databases/datatypes.hpp>
#include <global.hpp>
#include <chrono>
xmlrpc_c::clientXmlTransport_curl myTransport(xmlrpc_c::clientXmlTransport_curl::constrOpt().no_ssl_verifyhost(true).no_ssl_verifypeer(true));
xmlrpc_c::client_xml myClient(&myTransport) ;

mXmlRpc::mXmlRpc()
{
     serverUrl = "http://192.168.17.152:7080" ;
     //set the limit of a result to 95 MB
     xmlrpc_limit_set(XMLRPC_XML_SIZE_LIMIT_ID,  (99626731));
     //xmlrpc_c::clientXmlTransport_curl myTransport
               (xmlrpc_c::clientXmlTransport_curl::constrOpt()
                .no_ssl_verifyhost(true)
                .no_ssl_verifypeer(true)
               );
     //myClient(&myTransport) ;
}



xmlrpc_c::value mXmlRpc::callrp(string rp,xmlrpc_c::paramList const& paramList)
{
     xmlrpc_c::value result1;

     xmlrpc_c::rpcOutcome outcome;
     auto start = std::chrono::system_clock::now();
     try {
         
          // myClient(&myTransport);
          
          serverUrl = cuonSystem["protocol"] + "://" + cuonSystem["server"] +":" +  cuonSystem["port"] ;
           xmlrpc_c::carriageParm_curl0 myCarriageParm(serverUrl);
           // myClient.call(myCarriageParm, rp, paramList,  &result1);
           myClient.call(&myCarriageParm, rp, paramList, &outcome);
           result1 = outcome.getResult();

     } catch (exception const& e) {
          cerr << "Client threw error: " << e.what() << endl;
     } catch (...) {
          cerr << "Client threw unexpected error." << endl;
     }
     auto end = std::chrono::system_clock::now();
     auto elapsed =std::chrono::duration_cast<std::chrono::milliseconds>( end - start) ;
     // cout << "time for the Sql-Result = " << elapsed.count()/1000.0 << " sec" << endl ;
     
     return result1 ;
}
myXmlRpc::myXmlRpc()
{
     vector<Record> ResultSet;
}
myXmlRpc::~myXmlRpc(){
}
xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,int i1)
{
     dataServ.add(xmlrpc_c::value_int(i1));
     return dataServ;
}
xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,string s1)
{
     //if(s1 != ""){
          dataServ.add(xmlrpc_c::value_string(s1));
          //}
     return dataServ;
}

xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,double d1)
{

     dataServ.add(xmlrpc_c::value_double(d1));
     return dataServ;
}
xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,vector<string> v1)
{
     vector<xmlrpc_c::value> arrayData;
     typedef::vector <string>::const_iterator iV ;

     for (iV i =v1.begin(); i != v1.end(); i++) {
          arrayData.push_back(xmlrpc_c::value_string(*i));
     }

     //xmlrpc_c::value_array array1(v1);

     dataServ.add(xmlrpc_c::value_array(arrayData));
     return dataServ;
}

xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,vector<int> vi1)
{
     vector<xmlrpc_c::value> arrayData;
     typedef::vector <int>::const_iterator iV ;

     for (iV i =vi1.begin(); i != vi1.end(); i++) {
          arrayData.push_back(xmlrpc_c::value_int(*i));
     }

     //xmlrpc_c::value_array array1(v1);

     dataServ.add(xmlrpc_c::value_array(arrayData));
     return dataServ;
}

xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,bool b1)
{
     dataServ.add(xmlrpc_c::value_boolean(b1));
     return dataServ;
}


xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,map <string,vector <string> > m1)
{

     map<string,xmlrpc_c::value> xml_map1 ;
     typedef map<string, vector <string> >::const_iterator iM ;
     for (iM i =m1.begin(); i != m1.end(); i++) {
          //cout << i->first << " - " << i->second << endl ;
          vector<xmlrpc_c::value> arrayData;
          typedef::vector <string>::const_iterator iV ;

          for (iV j =i->second.begin(); j != i->second.end(); j++) {
               arrayData.push_back(xmlrpc_c::value_string(*j));
          }

          //xmlrpc_c::value_array array1(v1);


          xml_map1[i->first] = xmlrpc_c::value_array(arrayData);

     }

     xmlrpc_c::value_struct const param1(xml_map1);




     dataServ.add(param1);
     return dataServ ;
}

xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,map <string,string> m1)
{

     map<string,xmlrpc_c::value> xml_map1 ;
     typedef map<string,string>::const_iterator iM ;
     for (iM i =m1.begin(); i != m1.end(); i++) {
           // cout << i->first << " _add - " << i->second << endl ;

          xml_map1[i->first] = xmlrpc_c::value_string(i->second);
          
     }

     xmlrpc_c::value_struct const param1(xml_map1);
     // cout << "size of = " << xml_map1.size() << ", old = " << m1.size() << endl ;



     dataServ.add(param1);
     return dataServ ;
}

xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,map <string,int> m1)
{

     map<string,xmlrpc_c::value> xml_map1 ;

     for (auto i : m1 ) {
          //cout << i->first << " _add - " << i->second << endl ;

          xml_map1[i.first] = xmlrpc_c::value_int(i.second);
          
     }

     xmlrpc_c::value_struct const param1(xml_map1);
     //cout << "size of = " << xml_map1.size() << ", old = " << m1.size() << endl ;



     dataServ.add(param1);
     return dataServ ;
}

xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,map <string,float> m1)
{

     map<string,xmlrpc_c::value> xml_map1 ;

     for (auto i : m1) {
          //cout << i->first << " _add - " << i->second << endl ;

          xml_map1[i.first] = xmlrpc_c::value_double(i.second);
          
     }

     xmlrpc_c::value_struct const param1(xml_map1);
     //cout << "size of = " << xml_map1.size() << ", old = " << m1.size() << endl ;



     dataServ.add(param1);
     return dataServ ;
}

xmlrpc_c::paramList  myXmlRpc::_add(xmlrpc_c::paramList dataServ,map <string,tm*> m1)
{

     map<string,xmlrpc_c::value> xml_map1 ;

     for (auto i : m1) {
          //cout << i->first << " _add - " << i->second << endl ;

          xml_map1[i.first] = xmlrpc_c::value_datetime(mktime(i.second));
          
     }

     xmlrpc_c::value_struct const param1(xml_map1);
     //cout << "size of = " << xml_map1.size() << ", old = " << m1.size() << endl ;



     dataServ.add(param1);
     return dataServ ;
}

xmlrpc_c::paramList myXmlRpc::addUser( xmlrpc_c::paramList dataServ)
{

      // cout << "client id = " <<  oUser.sSqlUser["client"] << endl ;



     dataServ = _add(dataServ, oUser.sSqlUser);

     return dataServ ;


}


xmlrpc_c::paramList myXmlRpc::add(sendValues sv,bool setUserDic )
{

     xmlrpc_c::paramList dataServ;
     dataServ = _add(dataServ, sv.sNameOfTable);

     dataServ = _add(dataServ, sv.id);
     dataServ = _add(dataServ, sv.Values);

     dataServ = addUser(dataServ);

     return dataServ;
}

xmlrpc_c::paramList myXmlRpc::add(listEntry e1,bool setUserDic )
{

     xmlrpc_c::paramList dataServ;
     dataServ = _add(dataServ, e1.dicEntries);
     //cout << "add listEntry 1" << endl;
     dataServ = _add(dataServ, e1.sNameOfTable);
     //cout << "add listEntry 2" << endl;
     dataServ = _add(dataServ, e1.sSort);
     //cout << "add listEntry 3" << endl;
     dataServ = _add(dataServ, e1.sWhere);
     dataServ = _add(dataServ,e1.bDistinct) ;
     dataServ = _add(dataServ,e1.liFields);
     dataServ = addUser(dataServ);
     //cout << "add listEntry 9" << endl;



     //if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;

}

xmlrpc_c::paramList  myXmlRpc::add(map <string,string> m1,map <string,string> m2,bool setUserDic  )
{



     xmlrpc_c::paramList dataServ;

     dataServ = _add(dataServ,m1);
     dataServ = _add(dataServ,m2);
     if(setUserDic) dataServ = addUser(dataServ);

     return dataServ;
}

xmlrpc_c::paramList  myXmlRpc::add(map <string,string> m1,bool setUserDic)
{
     typedef map<string,string>::const_iterator iM ;
     for (iM i =m1.begin(); i != m1.end(); i++) {
          // cout << i->first << " = " <<  i->second << endl ;
     }

     // cout << "setuserdic = " << setUserDic << endl ;
     // cout << "endUserdic" << endl ;
     
     xmlrpc_c::paramList dataServ;
     _add(dataServ,m1);
     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;
}


xmlrpc_c::paramList  myXmlRpc::add(string s1,bool setUserDic)
{


     xmlrpc_c::paramList dataServ;
     if (s1 != "NO_ENTRY") {
          dataServ.add(xmlrpc_c::value_string(s1));
     }
     if(setUserDic) dataServ = addUser(dataServ);

     return dataServ;
}

xmlrpc_c::paramList  myXmlRpc::add(string s1,Record r1, bool setUserDic)
{


     xmlrpc_c::paramList dataServ;

     dataServ.add(xmlrpc_c::value_string(s1));

     dataServ = _add(dataServ,r1.row_string);
     dataServ = _add(dataServ,r1.row_int);
     dataServ = _add(dataServ,r1.row_float);
     dataServ = _add(dataServ,r1.row_date);
     
     
     if(setUserDic) dataServ = addUser(dataServ);

     return dataServ;
}


xmlrpc_c::paramList  myXmlRpc::add(bool setUserDic)
{


     xmlrpc_c::paramList dataServ;
   
     if(setUserDic) dataServ = addUser(dataServ);

     return dataServ;
}
xmlrpc_c::paramList  myXmlRpc::add(int i1,bool setUserDic)
{

     xmlrpc_c::paramList dataServ;

     dataServ = _add(dataServ,i1);

     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;
}

xmlrpc_c::paramList  myXmlRpc::add(int i1,string s, bool setUserDic)
{

     xmlrpc_c::paramList dataServ;

     dataServ = _add(dataServ,i1);
     dataServ = _add(dataServ,s);

     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;
}


xmlrpc_c::paramList  myXmlRpc::add(int i1,vector <string> v1 , bool setUserDic)
{

     xmlrpc_c::paramList dataServ;

     dataServ = _add(dataServ,i1);
     dataServ = _add(dataServ,v1);

     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;
}
xmlrpc_c::paramList  myXmlRpc::add(int i1,int i2, bool setUserDic)
{

     xmlrpc_c::paramList dataServ;

     dataServ = _add(dataServ,i1);
     dataServ = _add(dataServ,i2);

     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;
}

xmlrpc_c::paramList  myXmlRpc::add(string s1, string s2,bool setUserDic)
{

     xmlrpc_c::paramList dataServ;

     dataServ = _add(dataServ,s1);

     dataServ = _add(dataServ, s2);
     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;
}
xmlrpc_c::paramList  myXmlRpc::add(string s1, int i1,bool setUserDic)
{

     xmlrpc_c::paramList dataServ;

     dataServ = _add(dataServ,s1);

     dataServ = _add(dataServ, i1);
     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;
}


xmlrpc_c::paramList myXmlRpc::add(vector <int > vi1, bool setUserDic ){
     xmlrpc_c::paramList dataServ;
     dataServ = _add(dataServ,vi1);
     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;

}
xmlrpc_c::paramList  myXmlRpc::add(vector <string> v1,bool setUserDic)
{

     xmlrpc_c::paramList dataServ;

     dataServ = _add(dataServ,v1);

     if(setUserDic) dataServ = addUser(dataServ);
     return dataServ;
}

// get 1 record
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,Record &r1)
{

     xmlrpc_c::value result;

     result = callrp( rp, paramList);

     //int const sum = xmlrpc_c::value_int(result);

     //a1 = xmlrpc_c::value_array(result).cvalue();

     // Assume the method returned an integer; throws error if not
     try {
          //i = xmlrpc_c::value_string(result) ;
          
          map <string,xmlrpc_c::value> mResult = xmlrpc_c::value_struct(result);


          // cout <<  "Size of Map " << mResult.size() << endl;
          typedef map<string,xmlrpc_c::value>::const_iterator iM ;
          for (iM j =mResult.begin(); j != mResult.end(); j++) {

//                cout << "j->first = " << j->first << endl;

               //m[i->first] = xmlrpc_c::value_string(i->second) ;
               try {


                    r1.row_string[j->first] = xmlrpc_c::value_string(j->second)  ;
                    //                   cout << "string Value = " <<  j->first << " = " << r1.row_string[j->first] << endl ;

               } catch(...) {
                    // cout << "record value not string: " <<  endl;
                    try {

                         r1.row_int[j->first] = xmlrpc_c::value_int(j->second) ;
                         //   cout << "int Value = " <<  j->first << " = " << r1.row_int[j->first] << endl ;

                    } catch(exception const& e) {
                         //cout << "record value not int: " << e.what() <<  endl;
                         try {

                              r1.row_float[j->first] = xmlrpc_c::value_double(j->second) ;
                              //   cout << "float Value = " <<  j->first << " = " << r1.row_float[j->first] << endl ;

                         } catch(exception const& e) {
                              // cout << "record value not float - not solution found " << e.what() << endl;
                         }
                    }


               }
          }//for
//           cout << "all ok, r1 is set  " << endl;

     } catch(exception const& e) {
          //i = "";
          cerr << "Client threw error at 1 Record: " << e.what() << endl;
          r1.row_string["ERROR"] ="NONE";
          //        cerr << "row string = " +  r1.row_string["ERROR"] << endl ;
          
     }
}

// get a resultset
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,vector <Record> &rs1)
{

     xmlrpc_c::value result;

     result = callrp( rp, paramList);


     auto start = std::chrono::system_clock::now();



     //int const sum = xmlrpc_c::value_int(result);

     //a1 = xmlrpc_c::value_array(result).cvalue();

     // Assume the method returned an integer; throws error if not
     try {

          vector <xmlrpc_c::value> v1 = xmlrpc_c::value_array(result).vectorValueValue();
          //xmlrpc_c::value_array v1  =  xmlrpc_c::value_array(result);
          typedef vector <xmlrpc_c::value>::const_iterator iV ;




          for (iV i =v1.begin(); i != v1.end(); i++) {


               map <string,xmlrpc_c::value> mResult = xmlrpc_c::value_struct(*i);

               Record newRecord ;
               // cout <<  "Size of Map" << mResult.size() << endl;
               typedef map<string,xmlrpc_c::value>::const_iterator iM ;
               for (iM j =mResult.begin(); j != mResult.end(); j++) {

                    // cout << "j->first = " << j->first << endl;

                    //m[i->first] = xmlrpc_c::value_string(i->second) ;
                    try {

                         newRecord.row_string[j->first] = xmlrpc_c::value_string(j->second) ;
                         // cout << "string Value = " <<  j->first << " = " << newRecord.row_string[j->first] << endl ;

                    } catch(exception const& e) {
                         //cout << "record value not string: " << e.what() << endl;
                         try {

                              newRecord.row_int[j->first] = xmlrpc_c::value_int(j->second) ;
                              //cout << "int Value = " <<  j->first << " = " << newRecord.row_int[j->first] << endl ;

                         } catch(exception const& e) {
                              // cout << "record value not int: " << e.what() <<  endl;
                              try {

                                   newRecord.row_float[j->first] = xmlrpc_c::value_double(j->second) ;
                                   //cout << "float Value = " <<  j->first << " = " << newRecord.row_float[j->first] << endl ;

                              } catch(exception const& e) {
                                    // cout << "record value not float: " << e.what() << endl;
                              }
                         }


                    }
               }//for
               rs1.push_back(newRecord);
               //cout <<  "Size of Vector" << rs1.size() << endl;

               //i = xmlrpc_c::value_string(result) ;

          }// for
     } catch(exception const& e) {
          //i = "";
          cerr << "Client threw error: " << e.what() << endl;
     }
     auto end = std::chrono::system_clock::now();
     auto elapsed =std::chrono::duration_cast<std::chrono::milliseconds>( end - start) ;
     // cout << "time for the parsing of the resultset  = " << elapsed.count()/1000.0 << " sec" << endl ;
}

// return string
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,string &s1)
{

     //cout << "string length0 = " <<  endl ;
     xmlrpc_c::value result;
     result = callrp( rp, paramList);
     //cout << "string length1 = " << endl ; 
   
    
     try {
          s1 = xmlrpc_c::value_string(result) ;
          //cout << "string length = " << s1.length() << endl ;
     } catch(exception const& e) {
          s1 = "";
          cerr << "Client threw error: " << e.what() << endl;
     }
}

// return integer
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,int &i)
{

     xmlrpc_c::value result;

     result = callrp( rp, paramList);

     //cout << xmlrpc_c::value_int(result) << endl;

     //int const sum = xmlrpc_c::value_int(result);

     //a1 = xmlrpc_c::value_array(result).cvalue();

     // Assume the method returned an integer; throws error if not
     try {
          i = xmlrpc_c::value_int(result) ;
     } catch(exception const& e) {
          i = 0;
          cerr << "Client threw error: " << e.what() << endl;
     }
}

// return float
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,float &f1)
{

     xmlrpc_c::value result;

     result = callrp( rp, paramList);

     //cout << xmlrpc_c::value_int(result) << endl;

     //int const sum = xmlrpc_c::value_int(result);

     //a1 = xmlrpc_c::value_array(result).cvalue();

     // Assume the method returned an integer; throws error if not
     try {
          f1 = xmlrpc_c::value_double(result) ;
     } catch(exception const& e) {
          f1 = 0.00;
          cerr << "Client threw error: " << e.what() << endl;
     }
}


void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,bool &ok)
{

     xmlrpc_c::value result;

     result = callrp( rp, paramList);

     //cout << xmlrpc_c::value_int(result) << endl;

     //int const sum = xmlrpc_c::value_int(result);

     //a1 = xmlrpc_c::value_array(result).cvalue();

     // Assume the method returned an integer; throws error if not
     try {
          ok = xmlrpc_c::value_boolean(result) ;
     } catch(exception const& e) {
          ok = false;
          cerr << "Client threw error: " << e.what() << endl;
     }
}

// return vector <string>

void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,vector <string> &v)
{

     xmlrpc_c::value result;

     result = callrp( rp, paramList);

     try {
          vector<xmlrpc_c::value> b = xmlrpc_c::value_array(result).vectorValueValue();

      
          for (auto &i : b) {

               //cout << "get a vector of stringdata " << i << endl;
               //cout << "get a vector of stringdata " <<  s1 << endl ;
               v.push_back( xmlrpc_c::value_string(i) );


          
          }
     } catch(exception const& e) {
          //m = {};
          cerr << "Client threw error: " << e.what() << endl;
     }

}
// return vector <int>

void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,vector <int> &v)
{

     xmlrpc_c::value result;

     result = callrp( rp, paramList);

     try {
          vector<xmlrpc_c::value> b = xmlrpc_c::value_array(result).vectorValueValue();

          
          for (auto &i :b) {


               v.push_back(xmlrpc_c::value_int(i) );
          }
          
     } catch(exception const& e) {
          //m = {};
          cerr << "Client threw error: " << e.what() << endl;
     }

}
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,vector <vector <string> > &v1, vector <string> &v2,vector <string> &v3,vector <string> &v4 ){
     xmlrpc_c::value result;

     result = callrp( rp, paramList);

     try {
          vector<xmlrpc_c::value> b = xmlrpc_c::value_array(result).vectorValueValue();

          typedef vector<xmlrpc_c::value>::const_iterator iV ;
          int z1 = 0;
          for (iV i =b.begin(); i != b.end(); i++) {

               printlog( "get a vector of vector data ");
               
               
               // first is a vector < vector < string> >
               vector < xmlrpc_c::value > b1 =  xmlrpc_c::value_array(*i).vectorValueValue();
               
               typedef vector<xmlrpc_c::value>::const_iterator iVb1 ;
               for (iVb1 ib1 = b1.begin(); ib1 != b1.end();ib1 ++){
                    printlog("get vector at ib1");
                    if (z1 == 0){
                         vector < xmlrpc_c::value > b1_1 =  xmlrpc_c::value_array(*ib1).vectorValueValue();
                         typedef vector<xmlrpc_c::value>::const_iterator iVb1_1 ;
                         vector <string> vs1 ;
                         for (iVb1_1 ib1_1 = b1_1.begin(); ib1_1 != b1_1.end();ib1_1 ++){
                              printlog("get vector at  ib1_1");
                              string s3 =  xmlrpc_c::value_string(*ib1_1);
                              printlog("s ib1_1 = " + s3);
                              vs1.push_back(s3);
                         }
                         v1.push_back(vs1);
                    }
                    else if (z1 == 1) {
                         string s4 =  xmlrpc_c::value_string(*ib1);
                         v2.push_back(s4);
                    }
                    else if (z1 == 2) {
                         string s4 =  xmlrpc_c::value_string(*ib1);
                         v3.push_back(s4);
                    }
                    else if (z1 == 3) {
                         string s4 =  xmlrpc_c::value_string(*ib1);
                         v4.push_back(s4);
                    }

               }
               //string s1 = xmlrpc_c::value_string(*i) ;
               
               //cout << "get a vector of stringdata " <<  s1 << endl ;

               //v.push_back(xmlrpc_c::value_string(*i) );
               z1 ++;
                
          }
     } catch(exception const& e) {
          //m = {};
          cerr << "Client threw error: " << e.what() << endl;
     }
}
// return map
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,map <string,string> &m)
{

     //cout <<  "call a struct" <<  endl;
     xmlrpc_c::value result;
     result = callrp( rp, paramList);

     //int const sum = xmlrpc_c::value_int(result);

     //a1 = xmlrpc_c::value_array(result).cvalue();

     // Assume the method returned an integer; throws error if not
     try {
          map<string,xmlrpc_c::value> b = xmlrpc_c::value_struct(result);
          //cout <<  "Size of Map" << b.size() << endl;
          typedef map<string,xmlrpc_c::value>::const_iterator iM ;
          for (iM i =b.begin(); i != b.end(); i++) {

               //cout << i->first << endl;

               m[i->first] = xmlrpc_c::value_string(i->second) ;
          }
     } catch(exception const& e) {
          //m = {};
          cerr << "Client threw error: " << e.what() << endl;
     }
}



// return vector< map>
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,vector <map <string,string> > &v)
{

     //cout <<  "call a struct" <<  endl;
     xmlrpc_c::value result;
     result = callrp( rp, paramList);

     //int const sum = xmlrpc_c::value_int(result);

     //a1 = xmlrpc_c::value_array(result).cvalue();

     // Assume the method returned an integer; throws error if not

     try {
          vector <xmlrpc_c::value> a1;
          map <string,string> m;
          a1 = xmlrpc_c::value_array(result).vectorValueValue();
          //cout <<  "Size of Array" << a1.size() << endl;
          for (vector<xmlrpc_c::value>::iterator it = a1.begin(); it!=a1.end(); ++it) {

               map<string,xmlrpc_c::value> b = xmlrpc_c::value_struct(*it);
               // cout <<  "Size of Map" << b.size() << endl;
               typedef map<string,xmlrpc_c::value>::const_iterator iM ;
               for (iM i =b.begin(); i != b.end(); i++) {

                    //  string s3 = xmlrpc_c::value_string(i->second) ;
                    // cout << i->first << " = " << s3 << endl;

                    m[i->first] = xmlrpc_c::value_string(i->second) ;

               }

               v.push_back(m);
          }
     } catch(exception const& e) {
          //m = {};
          cerr << "Client threw error: " << e.what() << endl;
     }
}


// return vector< map>
void myXmlRpc::callRP(string rp,xmlrpc_c::paramList const& paramList,vector <map <string, vector <string> > > &v)
{

     //cout <<  "call a struct" <<  endl;
     xmlrpc_c::value result;
     result = callrp( rp, paramList);

     //int const sum = xmlrpc_c::value_int(result);

     //a1 = xmlrpc_c::value_array(result).cvalue();

     // Assume the method returned an integer; throws error if not

     try {
          vector <xmlrpc_c::value> a1;
          map <string, vector <string > > m;
          a1 = xmlrpc_c::value_array(result).vectorValueValue();
          //cout <<  "Size of Array" << a1.size() << endl;
          for (vector<xmlrpc_c::value>::iterator it = a1.begin(); it!=a1.end(); ++it) {

               map<string,xmlrpc_c::value> b = xmlrpc_c::value_struct(*it);
               // cout <<  "Size of Map" << b.size() << endl;
               typedef map<string,xmlrpc_c::value>::const_iterator iM ;
               for (iM i =b.begin(); i != b.end(); i++) {

                    //  string s3 = xmlrpc_c::value_string(i->second) ;
                    // cout << i->first << " = " << s3 << endl;
                    vector<xmlrpc_c::value> c = xmlrpc_c::value_array(i->second).vectorValueValue() ;
                    vector <string> newS ; 
                    for (auto j = c.begin();j != c.end(); j++ ){

                         newS.push_back( xmlrpc_c::value_string(*j ) );
                    }
                         
                    
                    m[i->first] = newS ;

               }

               v.push_back(m);
          }
     } catch(exception const& e) {
          //m = {};
          cerr << "Client threw error: " << e.what() << endl;
     }
}



//
//        string s1 = xmlrpc_c::value_string(a1[1]) ;
//        cout << "Result of RPC 1: " << xmlrpc_c::value_int(a1[0]) << endl;
//        cout << "Result of RPC 2: " << s1 << endl;
//
//        //cout << "type of RPC 3: " << a1[2].cvalue() << endl;
//        map<string,xmlrpc_c::value> b = xmlrpc_c::value_struct(a1[2]).cvalue();
//
//        v1.push_back(&s0);
//        v1.push_back(&s1);
//
//         cout <<  b.size() << endl;
//         typedef map<string,xmlrpc_c::value>::const_iterator iM ;
//
//         for (iM i =b.begin(); i != b.end(); i++) {
//
//            cout << i->first << endl;
//            cout << xmlrpc_c::value_int(i->second) << endl;
//         }
//        //c[xmlrpc_c::value_string(b1[0])] =  xmlrpc_c::value_int(b1[1]);
//cout << "Result of RPC 3: " << c["t1"] << endl;



//return v1 ;
//}


