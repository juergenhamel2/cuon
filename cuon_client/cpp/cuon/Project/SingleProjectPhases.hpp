#ifndef SINGLEPROJECTPHASES_HPP_INCLUDED
#define SINGLEPROJECTPHASES_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>


class SingleProjectPhases:public Singledata
{
    public:
        int projectID ;

        SingleProjectPhases();
        ~SingleProjectPhases();
        virtual void readExtraEntries();
    protected:

    private:

};

#endif // SINGLEPROJECTPHASES_HPP_INCLUDED
