#ifndef SINGLEPROJECT_HPP_INCLUDED
#define SINGLEPROJECT_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>


class SingleProject:public Singledata
{
    public:
        SingleProject();
        ~SingleProject();
    protected:

    private:

};

#endif // SINGLEPROJECT_HPP_INCLUDED
