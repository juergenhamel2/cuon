#ifndef SINGLEPROJECTMATERIALRESOURCES_HPP_INCLUDED
#define SINGLEPROJECTMATERIALRESOURCES_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>


class SingleProjectMaterialResources:public Singledata
{
    public:
        SingleProjectMaterialResources();
        ~SingleProjectMaterialResources();
    protected:

    private:

};

#endif // SINGLEPROJECTMATERIALRESOURCES_HPP_INCLUDED
