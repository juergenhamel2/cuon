#ifndef SINGLESOURCEFILE_HPP_INCLUDED
#define SINGLESOURCEFILE_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>


class SingleSourcefile:public Singledata

{
    public:
        SingleSourcefile();
        ~SingleSourcefile();
    protected:

    private:

};

#endif // SINGLESOURCEFILE_HPP_INCLUDED
