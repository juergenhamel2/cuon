/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]     

  This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
  published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
  warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.

  You should have received a copy of the GNU General Public License along with this program; if not, write to the
  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
  */

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>


#include <cuon/Stock/stock.hpp>

#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Articles/articles.hpp>

#include <Pluma/Connector.hpp>




PLUMA_CONNECTOR


bool connect(pluma::Host& host){
    // add a  provider to host
    host.add( new stockProvider() );
    return true;
}



stock::stock()
{
}

int stock::initAll(){
    tree1Name = "tv_stock";


    v_new.push_back( "New1");


    menuItems["new"] = v_new;

    v_save.push_back( "Save1");

    menuItems["save"] = v_save;

    v_delete.push_back("Delete1");

    menuItems["delete"] = v_delete ;

    v_end.push_back("quit1");
    menuItems["quit"] = v_end ;

    // Toolbar Buttons
    v_tnew.push_back("tbNew");
    toolbarItems["new"] = v_tnew;

    v_tsave.push_back("tbSave");
    toolbarItems["save"] = v_tsave;

    //	v_tdelete.push_back("tbDelete");
    //	toolbarItems["delete"] = v_tdelete;



    cout << "start article" << endl ;
    init("stock","StockMainwindow","stock");



    // special Menu entry
    //
    window->get_widget("reorganize_stock", chooseMenuItem);
    chooseMenuItem->signal_activate().connect(sigc::mem_fun(this, &stock::on_reorganize_stock_activate));



    //connect Button Signals

    window->get_widget("bArticleSearch", pButton);

    pButton->signal_clicked().connect(sigc::mem_fun(this, &stock::on_bArticleSearch_clicked));
    cout << "-------------------------------------------------------------------------------------- Button signal set" << endl ;


    // connect changed signal

    window->get_widget("eArticleID", pEntry);
    pEntry->signal_changed().connect(sigc::mem_fun(this, &stock::on_eArticleID_changed));

    // init db
    singleArticle = new SingleArticle();

    singleStock = new SingleStock();


    singleStock->setWindow(window) ;
    singleStock->setTree1();

    singleStockGoods = new SingleStockGoods();
    singleStockGoods->setWindow(window);
    singleStockGoods->setTree1();

    cout << "--------------------------------------------------------------------------------------start stock tab changed" << endl ;
    tabChanged();

    return 1;

}

void stock::on_new_activate()
{
    printlog("stock new 0","") ;
    switch (tabOption) {

        case tabstock_Stock:

            singleStock->newData();
            break;
        case tabstock_Goods:
            singleStockGoods->newData();
            break;
    }
}
void stock::on_save_activate()
{
    printlog ("stock save 0" );
    switch (tabOption) {
        case tabstock_Stock:
            singleStock->save();
            break;
        case tabstock_Goods:
            singleStockGoods->save();
            break;   
    }
}

void stock::on_delete_activate() {}

void stock::on_reorganize_stock_activate(){
    printlog ("reorganize the stock" );

    vector<Record> oResultSet ;
    x1.callRP("Stock.reorganize_stock",x1.add(), oResultSet);



}
void stock::on_bSearch_clicked() {}

void stock::on_bArticleSearch_clicked(){

    articles *art1 = new articles();
    Gtk::Entry* arID;
    window->get_widget("eArticleID",arID);
    art1->setChooseEntry(arID);

}

void stock::on_eArticleID_changed(){
    Gtk::Entry* arID ;
    window->get_widget("eArticleID",arID);
    int newID = String2Int(arID->get_text()) ;
    if(newID > 0){
        singleArticle->load(newID);
        Gtk::Entry* arDes ;
        window->get_widget("eArticleNumber",arDes);
        arDes->set_text(singleArticle->firstRecord.row_string["number"] + " " + singleArticle->firstRecord.row_string["designation"]);
    }
}

void stock::tabChanged(){

    cout << "tab changed at stock" << tabOption << " = " << tabstock_Stock << endl;
    switch(tabOption) {

        case tabstock_Stock:
            cout << "first stock tab " << endl ;
            if(oldTab < 1) {
                singleStock->getListEntries();
            }
            break;
        case tabstock_Goods:
            singleStockGoods->stock_id = singleStock->ID ;
            singleStockGoods->sWhere = " where stock_id = " + toString(singleStock->ID);
            printlog("tab changed, load stockgoods");
            singleStockGoods->getListEntries();
            break;
    }

    oldTab = tabOption;
}




stock::~stock() {}
