 #ifndef SINGLESTOCKGOODS_HPP_INCLUDED        
#define SINGLESTOCKGOODS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>

class SingleStockGoods:public Singledata {
public:
     int stock_id  ;
     SingleStockGoods();
     ~SingleStockGoods();
      virtual void readNonWidgetEntries();
      virtual void fillOtherEntries();
protected:

private:

};

#endif // SINGLESTOCKGOODS_HPP_INCLUDED
