#ifndef STOCK_HPP_INCLUDED
#define STOCK_HPP_INCLUDED


#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Stock/SingleStock.hpp>
#include <cuon/Stock/SingleStockGoods.hpp>

#define tabstock_Stock 0
#define tabstock_Goods 1


class stock:public windows, public cuon_window {
     
    public:

     
     
     SingleStock *singleStock ;
     SingleStockGoods *singleStockGoods ;
     SingleArticle *singleArticle;
     Gtk::Button* pButton1;

        stock();
        ~stock();
     virtual int initAll();
     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;

     void on_reorganize_stock_activate();
     // Buttons
     void on_bArticleSearch_clicked();

     void on_eArticleID_changed();
     
     
     void on_bSearch_clicked();
     void tabChanged();


    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(stock, cuon_window);



#endif // STOCK_HPP_INCLUDED
