#ifndef ADDRESSES_HPP_INCLUDED
#define ADDRESSES_HPP_INCLUDED
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Addresses/SingleAddress.hpp>
#include <cuon/Addresses/SinglePartner.hpp>
#include <cuon/Addresses/SingleMisc.hpp>
#include <cuon/Addresses/SingleBank.hpp>
#include <cuon/Addresses/SingleNote.hpp>
#include <cuon/Addresses/SingleSchedul.hpp>
#include <cuon/Windows/gtk_extensions.hpp>
#include <cuon/DMS/SingleDMS.hpp>

#define tabadr_Address 0
#define tabadr_Bank 1
#define tabadr_Misc 2
#define tabadr_Partner 3


#define tabadr_Schedul 4
#define tabadr_Notes 5
#define tabadr_Proposal 6
#define tabadr_Order 7
#define tabadr_Invoice 8
#define tabadr_Project 9
#define tabadr_Html 10

//#define tabadr_Graves 11
//#define tabadr_Hibernation 12


class addresses:public windows,gtk_extensions {

public:

     int addressID ;
     Gtk::TreeView* tvGrave;
     Gtk::TreeView* tvHibernation;
     int tabadr_Graves = 11;
     int tabadr_Hibernation = 12;
     
     
     SingleAddress *singleAddress ;
     SinglePartner *singlePartner ;
     SingleMisc *singleMisc ;
     SingleBank *singleBank ;
     SingleNote *singleNote ;
     SingleSchedul *singleSchedul ;
     SingleDMS* singleDMS;
     
     addresses();
     ~addresses();


     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     void on_chooseAddress_activate();
     void on_row_activated(const Gtk::TreeModel::Path& path, Gtk::TreeViewColumn* column);
     void on_newsletter_email_activate();
     void on_bSearch_clicked();
     void on_bWebSearch_clicked();
     void tabChanged();
     void on_newsletter_print_activate();
     void on_bLetter_clicked();
     void on_liAddressesPhone1_activate();
     void on_bShowDMS_clicked();
     void  on_bGeneratePartner_clicked();
     void on_tbGrave_clicked();
     void on_tbHibernation_clicked();
     
     
     map <string,int>* getExternInfo();
     Record* getAddressInfos();
     

};



#endif // ADDRESSES_HPP_INCLUDED
