#ifndef LISTS_ADDRESSES_HPP_INCLUDED
#define LISTS_ADDRESSES_HPP_INCLUDED
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Addresses/SingleAddress.hpp>


class lists_addresses:public windows {

public:

     vector <string> values ;

     
     lists_addresses();
     ~lists_addresses();

     vector<string> getValues();
     

};



#endif // LISTS_ADDRESSES_HPP_INCLUDED
