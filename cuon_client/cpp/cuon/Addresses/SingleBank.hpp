#ifndef SINGLEBANK_HPP_INCLUDED
#define SINGLEBANK_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>


class SingleBank:public Singledata {
public:
     int addressID ;
     SingleBank();
     ~SingleBank();
    virtual void readExtraEntries();
protected:

private:

};



#endif // SINGLEBANK,HPP_INCLUDED
