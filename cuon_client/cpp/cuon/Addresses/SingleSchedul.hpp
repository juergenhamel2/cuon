#ifndef SINGLESCHEDUL_HPP_INCLUDED
#define SINGLESCHEDUL_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>
#include <gtksourceviewmm.h>


class SingleSchedul: public Singledata {
public:
     int addressID ;
     int partnerID ;
     string SchedulMisc ;
     string SchedulContacter ;
     string SchedulRep ;
     string SchedulSalesman ;
     string SchedulCompany ;
     
          Gsv::View* svMisc;
     Gsv::View* svContacter;
     Gsv::View* svRep;
     Gsv::View* svSalesman;
     Gsv::View* svCompany;
     SingleSchedul();
     ~SingleSchedul();
     virtual void readNonWidgetEntries();
     virtual void fillOtherEntries();
protected:

private:

};

#endif // SINGLESCHEDULy_HPP_INCLUDED
