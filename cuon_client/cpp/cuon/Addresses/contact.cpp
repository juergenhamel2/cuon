/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modeify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <libintl.h>
#include <locale.h>

#include  <cuon/Addresses/contact.hpp>
#include  <cuon/Addresses/SingleContact.hpp>


#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Addresses/SingleAddress.hpp>
#include <cuon/Addresses/SinglePartner.hpp>
#include <cuon/Addresses/SingleBank.hpp>
#include <cuon/Addresses/SingleMisc.hpp>
#include <cuon/Addresses/SingleNote.hpp>
#include <cuon/Addresses/SingleContact.hpp>

#include <cuon/Editor/editor_view.hpp>




contact::contact()
{

     //myXmlRpc x1 = myXmlRpc();
     tree1Name = "tv_contact";
     ModulNumber = MN["Contact"] ;

     v_new.push_back( "mi_new1");
     v_new.push_back("bank_new1");
     v_new.push_back("PartnerNew1");

     menuItems["new"] = v_new;

     v_save.push_back( "mi_save1");
     v_save.push_back("bank_save1");
     v_save.push_back("PartnerSave1");
     menuItems["save"] = v_save;

     v_delete.push_back("mi_clear1");
     v_delete.push_back("bank_delete1");
     v_delete.push_back("PartnerDelete1");
     menuItems["delete"] = v_delete ;

     v_end.push_back("mi_quit1");
     menuItems["quit"] = v_end ;


     // Toolbar Buttons
     v_tnew.push_back( "tbNew");
     toolbarItems["new"] = v_tnew;

     v_tsave.push_back( "tbSave");
     toolbarItems["save"] = v_tsave;


     init("contact","ContactMainwindow","contact");

     printlog("start singleAddress");
     singleAddress = new SingleAddress();
     printlog("end singleAddress");
     singlePartner = new SinglePartner();
     singleContact = new SingleContact();
     

     singleAddress->setWindow(window) ;
      printlog("start singleAddress-->setTree1");
     singleAddress->setTree1();
     printlog("end singleAddress-->setTree1");
     singlePartner->setWindow(window) ;
     singlePartner->setTree1();
     singleContact->setTree1();
     


     
     singleBank->setWindow(window) ;
      singleBank->setTree1();

      singleMisc->setWindow(window) ;
      //singleMisc->setTree1();
      singleNote->setWindow(window) ;
      

      // connect Menu-items

     
      
      window->get_widget("chooseAddress", chooseMenuItem);
      chooseMenuItem->signal_activate().connect(sigc::mem_fun(this, &contact::on_chooseAddress_activate));


     //connect Button Signals

      window->get_widget("bSearch", pButton);
      pButton->signal_clicked().connect(sigc::mem_fun(this, &contact::on_bSearch_clicked));


//      liFashion, liTrade,liTurnover,liLegalform, self.liSchedulTime = self.rpc.callRP('Address.getComboBoxEntries',self.dicUser)

     setComboBoxEntries("cbFashion","Address.getComboBoxEntriesFashion");
     setComboBoxEntries("cbTrade","Address.getComboBoxEntriesTrade");
     setComboBoxEntries("cbTurnover","Address.getComboBoxEntriesTurnover");
     setComboBoxEntries("cbLegalForm","Address.getComboBoxEntriesLegalform");
     setComboBoxEntries("cbeSchedulTimeBegin","Address.getComboBoxEntriesSchedultime");
     setComboBoxEntries("cbeSchedulTimeEnd","Address.getComboBoxEntriesSchedultime");
     

//    set the editor to the notes
     Gtk::ScrolledWindow* sw = NULL ;
     string sLM = mime_type["txt"];
     editor_view* ev = new editor_view();
     printlog("1");
     singleNote->svMisc = ev->getView(sLM);
     printlog("2");
      
     window->get_widget("swMisc",sw);
     printlog("3");

     sw->add(*singleNote->svMisc);
     sw->show_all();
     
     printlog("4");

     singleNote->svContacter = ev->getView(sLM);
     window->get_widget("swContacter",sw);
     sw->add(* singleNote->svContacter);
     sw->show_all();



     singleNote->svRep = ev->getView(sLM);
     window->get_widget("swRep",sw);
     sw->add(* singleNote->svRep);
     sw->show_all();


     singleNote->svSalesman = ev->getView(sLM);
     window->get_widget("swSalesman",sw);
     sw->add(* singleNote->svSalesman);
     sw->show_all();

     singleNote->svCompany = ev->getView(sLM);
     window->get_widget("swCompany",sw);
     sw->add(* singleNote->svCompany);
     sw->show_all();


     
      
     tabChanged();
}


contact::~contact()
{
     if(singleAddress) delete singleAddress ;
     if(singleBank) delete singleBank ;
     if(singleMisc) delete singleMisc ;
     
     if(singlePartner) delete singlePartner ;
     if(singleContact) delete singleContact ;

}


void contact::on_new_activate()
{
     printlog ("addresses new 0") ;
     switch (tabOption) {

     case tabadr_Address:

          singleAddress->newData();
          break;
     case tabadr_Bank:

          singleBank->newData();
          break;
     case tabadr_Misc:

          singleMisc->newData();
          break;
     case tabadr_Partner:
          singlePartner->newData();
     }

}

void contact::on_save_activate()
{
     printlog ("addresses save 0" );
     switch (tabOption) {
     case tabadr_Address:
          singleAddress->save();
          break;

     case tabadr_Bank:
          singleBank->save();
          break;

     case tabadr_Misc:
          singleMisc->save();
          break;

     case tabadr_Partner:
          singlePartner->addressID = singleAddress->ID ;
          singlePartner->save();
          break;
     case tabadr_Notes:

          break;
          
     }
     
     oldTab = 1;
     tabChanged();
     
}

void contact::on_delete_activate()
{
     printlog ("addresses delete 0") ;

     switch (tabOption) {

     case tabadr_Address:
          singleAddress->deleteRecord();
          break;


     }
}
void contact::on_chooseAddress_activate(){
  switch(tabOption) {

  case tabadr_Address:
       setChooseValue(singleAddress->ID);
       break;
       
  }
}

// Search
void contact::on_bSearch_clicked(){

     singleAddress->setSearchString();

}
void contact::tabChanged()
{

     printlog ("tab changed at address", tabOption , " = " ,tabadr_Address ) ;
     switch(tabOption) {

     case tabadr_Address:
          if(oldTab < 1) {
               ModulNumber = MN["Address"] ;
               singleAddress->getListEntries();
          }
          break;
     case tabadr_Bank:

          singleBank->addressID = singleAddress->ID ;
          singleBank->sWhere = "where address_id = " + toString(singleAddress->ID) ;
          cout << 1 << endl;
          singleBank->getListEntries();
          break;

     case tabadr_Misc:
          singleMisc->addressID = singleAddress->ID ;
          singleMisc->sWhere = "where address_id = " + toString(singleAddress->ID) ;

          break;


     case tabadr_Partner:
          ModulNumber = MN["Partner"] ;

          singlePartner->addressID = singleAddress->ID ;
          singlePartner->sWhere = "where addressid = " + toString(singleAddress->ID) ;

          singlePartner->getListEntries();
          break;

          

     case tabadr_Notes:
         ModulNumber = MN["Notes"] ; 
         singleNote->addressID = singleAddress->ID ;
         singleNote->sWhere = "where addressid = " + toString(singleAddress->ID) ;
         singleNote->getListEntries();
         printlog("Notes-ID = ", singleNote->tmp_ID);
     }
     oldTab = tabOption;
}






