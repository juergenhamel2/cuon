/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Addresses/SingleNote.hpp>

SingleNote::SingleNote(){
 sNameOfTable = "addresses_notes";
    sTree = "";
    sSort = "id";
    sWhere = "";
    addressID = 0;


    newEntry.sNameOfTable = sNameOfTable ;
    newEntry.sWhere = sWhere ;
    newEntry.sSort = sSort ;
    loadEntryField("addresses_notes");
    setTreeFields();
    setLiFields();

    
}


void SingleNote::readBuffer(){
     vector <string>* liBuffer ;
     auto sourcebuffer =  svMisc->get_source_buffer();
     NotesMisc = sourcebuffer->get_text() ;
     
     sourcebuffer =  svContacter->get_source_buffer();
     NotesContacter = sourcebuffer->get_text() ;
     
     sourcebuffer =  svRep->get_source_buffer();
     NotesRep = sourcebuffer->get_text() ;

     sourcebuffer =  svSalesman->get_source_buffer();
     NotesSalesman = sourcebuffer->get_text() ;
     
     sourcebuffer =  svCompany->get_source_buffer();
     NotesCompany = sourcebuffer->get_text() ;
     


     
}
void SingleNote::readNonWidgetEntries(){
     vector <string> v1 ;
     
     auto sourcebuffer =  svMisc->get_source_buffer();
     v1.push_back( sourcebuffer->get_text() );
     v1.push_back("string");
     dicValues["notes_misc"]= v1 ;
     v1.clear();

     sourcebuffer =  svContacter->get_source_buffer();
     v1.push_back( sourcebuffer->get_text() );
     v1.push_back("string");
     dicValues["notes_contacter"]= v1 ;
     v1.clear();

     sourcebuffer =  svRep->get_source_buffer();
     v1.push_back( sourcebuffer->get_text() );
     v1.push_back("string");
     dicValues["notes_rep"]= v1 ;
     v1.clear();

     sourcebuffer =  svSalesman->get_source_buffer();
     v1.push_back( sourcebuffer->get_text() );
     v1.push_back("string");
     dicValues["notes_salesman"]= v1 ;
     v1.clear();

     sourcebuffer =  svCompany->get_source_buffer();
     v1.push_back( sourcebuffer->get_text() );
     v1.push_back("string");
     dicValues["notes_organisation"]= v1 ;
     v1.clear();

     v1.push_back(toString(addressID) );
     v1.push_back("int");
     dicValues["address_id"]=v1;
     
}

void SingleNote::fillOtherEntries(){
     auto sourcebuffer =  svMisc->get_source_buffer();
     string _Value = firstRecord.row_string[_entry.get_nameOfSql()];
     sourcebuffer->set_text(_Value);
     svMisc->set_buffer(sourcebuffer);
}
void SingleNote::clearAllOtherFields(){
     
     svMisc->get_source_buffer()->set_text("");
     svContacter->get_source_buffer()->set_text("");
     svCompany->get_source_buffer()->set_text("");
     svRep->get_source_buffer()->set_text("");
     svSalesman->get_source_buffer()->set_text("");
}


SingleNote::~SingleNote(){}
