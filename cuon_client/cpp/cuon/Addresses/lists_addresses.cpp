/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modeify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <libintl.h>
#include <locale.h>


#include <cuon/Addresses/lists_addresses.hpp>

lists_addresses::lists_addresses(){
     loadGlade("addresses_search1","dialog1",1);
     cout << "gl1" << endl ;
     values = {};
}



lists_addresses::~lists_addresses(){


}


vector<string> lists_addresses::getValues(){
     values.clear() ;
     
     values.push_back("lastname_from=" + ((getText("eLastnameFrom") != "") ? getText("eLastnameFrom"):"NONE" ) );
     
     values.push_back("lastname_to=" + ((getText("eLastnameTo") != "") ? getText("eLastnameTo"):"NONE" ));
     
     values.push_back("firstname_from=" + ((getText("eFirstnameFrom")!= "") ? getText("eFirstnameFrom"):"NONE"));
    
     values.push_back("firstname_to=" + ((getText("eFirstnameTo") != "") ? getText("eFirstnameTo"):"NONE"));

       
     values.push_back("city_from=" + ((getText("eCityFrom") != "") ? getText("eCityFrom"):"NONE"));
    
     values.push_back("city_to=" + ((getText("eCityTo") != "") ? getText("eCityTo"):"NONE" ));
     
     values.push_back("country_from=" + ( (getText("eCountryFrom") != "") ? getText("eCountryFrom"):"NONE"));

     values.push_back("country_to=" + ( (getText("eCountryTo") != "") ? getText("eContryTo"):"NONE"));

     values.push_back("info_contains=" +( (getText("eInfoContains") != "") ? getText("eInfoContains"):"NONE"));

     values.push_back("newsletter_contains=" +( (getText("eNewsletterContains") != "") ? getText("eNewsletterContains"):"NONE"));
     
     return values ;
}
