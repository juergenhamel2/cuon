#ifndef SINGLEADDRESS_HPP_INCLUDED
    #define SINGLEADDRESS_HPP_INCLUDED

    #include <cuon/Databases/Singledata.hpp>

class SingleAddress:public Singledata{
public:
     SingleAddress();
     ~SingleAddress();
     string getAddress(int _id);
     string getEmail();
     string getLetterAddress();
     string getCountry();
     string getZip();
     string getCity();
     string getStreet();
     string getFirstname();
     string getLastname();
     string getPhone();
     void getPhonelist1(vector <string> dicSearchfields);

     
     
    };

#endif // SINGLEADDRESS_HPP_INCLUDED
