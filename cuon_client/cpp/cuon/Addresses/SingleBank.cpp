/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Addresses/SingleBank.hpp>

SingleBank::SingleBank()
{

	sNameOfTable = "address_bank";
	sTree = "tv_bank";
	sSort = "bank_ranking,depositor, account_number";
	sWhere = "";
	addressID = 0;


	newEntry.sNameOfTable = sNameOfTable ;
	newEntry.sWhere = sWhere ;
	newEntry.sSort = sSort ;
	loadEntryField("addresses_bank");
	setTreeFields();
	setLiFields();
}
SingleBank::~SingleBank() {}

void SingleBank::readExtraEntries()
{
	vector <string> v1;
	v1.push_back(toString(addressID));
	v1.push_back("int");

	dicValues["address_id"] = v1;
}
