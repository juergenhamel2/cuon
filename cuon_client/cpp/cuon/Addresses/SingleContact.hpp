#ifndef SINGLECONTACT_HPP_INCLUDED
    #define SINGLECONTACT_HPP_INCLUDED

    #include <cuon/Databases/Singledata.hpp>

    class SingleContact:public Singledata{
        public:
            SingleContact();
            ~SingleContact();
            string getAddress(int _id);

    };

#endif // SINGLECONTACT_HPP_INCLUDED
