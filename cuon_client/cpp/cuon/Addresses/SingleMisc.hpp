#ifndef SINGLEMISC_HPP_INCLUDED
#define SINGLEMISC_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>

class SingleMisc:public Singledata {
public:
     int addressID ;
     SingleMisc();
     ~SingleMisc();
protected:

private:

};

#endif // SINGLEMISC_HPP_INCLUDED
