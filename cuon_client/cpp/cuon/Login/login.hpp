#ifndef LOGIN_HPP
#define LOGIN_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>
#include <gcrypt.h>
#include <cuon/Windows/windows.hpp>

class login:public windows {

public:
    Glib::RefPtr<Gtk::Builder> gLogin ;
     Gtk::Dialog* pDlg;
     Gtk::MenuItem* pEnd1;

     Gtk::Entry* pUserID;
     Gtk::Entry* pPassword;

     Gtk::Button* pOK;
     Gtk::Button* pCancel;
     Glib::RefPtr<Gtk::Builder> Mainwindow ;

     login();
     ~login();
     void initLogin();
     string encodeSha512(string Password);
     string sendLoginData(string Name, string Password);

     bool on_TUserID_key_press_event(GdkEventKey* event);
     bool on_TPassword_key_press_event(GdkEventKey* event);
     void on_okbutton1_clicked();
     void on_cancelbutton1_clicked();
     void setMainwindow ( Glib::RefPtr<Gtk::Builder> _window);

};

#endif
