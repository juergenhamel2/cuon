#ifndef SINGLESOURCENAVIGATOR_MODULE_HPP_INCLUDED
#define SINGLESOURCENAVIGATOR_MODULE_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>


class SingleSourcenavigatorModule:public Singledata
{
    public:

      int partID ;

        SingleSourcenavigatorModule();
        ~SingleSourcenavigatorModule();
        virtual void readExtraEntries();
    protected:

    private:

};

#endif // SINGLESOURCENAVIGATOR_MODULE_HPP_INCLUDED
