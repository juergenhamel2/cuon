#ifndef SINGLESOURCENAVIGATORFILE_HPP_INCLUDED
#define SINGLESOURCENAVIGATORFILE_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>

class SingleSourcenavigatorFile:public Singledata
{
    public:
        int modulID ;

        SingleSourcenavigatorFile();
        ~SingleSourcenavigatorFile();
        virtual void readExtraEntries();
    protected:

    private:

};

#endif // SINGLESOURCENAVIGATORFILE_HPP_INCLUDED
