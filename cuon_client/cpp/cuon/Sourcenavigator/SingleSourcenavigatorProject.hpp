#ifndef SINGLESOURCENAVIGATORPROJECT_HPP_INCLUDED
#define SINGLESOURCENAVIGATORPROJECT_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>


class SingleSourcenavigatorProject:public Singledata
{
    public:
        SingleSourcenavigatorProject();
        ~SingleSourcenavigatorProject();
    protected:

    private:

};

#endif // SINGLESOURCENAVIGATORPROJECT_HPP_INCLUDED
