#ifndef SINGLESOURCENAVIGATOR_PART_HPP_INCLUDED
#define SINGLE_SOURCENAVIGATOR_PARTHPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>


class SingleSourcenavigatorPart:public Singledata
{
    public:
        int projectID ;

        SingleSourcenavigatorPart();
        ~SingleSourcenavigatorPart();
        virtual void readExtraEntries();
    protected:

    private:

};

#endif // SINGLE_SOURCENAVIGATOR_PART_HPP_INCLUDED
