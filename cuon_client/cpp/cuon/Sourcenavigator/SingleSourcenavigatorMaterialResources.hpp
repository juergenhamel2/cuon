#ifndef SINGLESOURCENAVIGATORMATERIALRESOURCES_HPP_INCLUDED
#define SINGLESOURCENAVIGATORMATERIALRESOURCES_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>


class SingleSourcenavigatorMaterialResources:public Singledata
{
    public:
        SingleSourcenavigatorMaterialResources();
        ~SingleSourcenavigatorMaterialResources();
    protected:

    private:

};

#endif // SINGLESOURCENAVIGATORMATERIALRESOURCES_HPP_INCLUDED
