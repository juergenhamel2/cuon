/*Copyright (C) [2012] [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include "cuon/Sourcenavigator/sourcenavigator.hpp"
#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <libintl.h>
#include <locale.h>


#include <Pluma/Connector.hpp>

#include <cuon/Sourcenavigator/SingleSourcenavigatorProject.hpp>
#include <cuon/Addresses/SingleAddress.hpp>
#include <cuon/DMS/dms.hpp>


PLUMA_CONNECTOR


  bool connect(pluma::Host& host){
     // add a  provider to host
          host.add( new sourcenavigatorProvider() );
     return true;
 }


sourcenavigator::sourcenavigator() {
    
     cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++start Sourcenavigator Window 0" << endl ;
}

int sourcenavigator::initAll(){

     cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++start Sourcenavigator Window" << endl ;
	tree1Name = "tree1";

    ModulNumber = MN["Sourcecode_project"] ;
	v_new.push_back("new1");

	menuItems["new"] = v_new;

	v_save.push_back("save1");

	menuItems["save"] = v_save;

	v_delete.push_back("clear1");

	menuItems["delete"] = v_delete ;

	v_end.push_back("mi_quit1");
	menuItems["quit"] = v_end ;

// Toolbar Buttons
	v_tnew.push_back("tbNew");
	toolbarItems["new"] = v_tnew;

	v_tsave.push_back("tbSave");
	toolbarItems["save"] = v_tsave;

	v_tdelete.push_back("tbDelete");
	toolbarItems["delete"] = v_tdelete;



	cout << "start sourcenavigator" << endl ;

	init("sourcenav","SourcenavMainwindow","sourcenavigator");

	// init db
	singleSourcenavigator = new SingleSourcenavigatorProject();
	singleSourcenavigatorPart = new SingleSourcenavigatorPart() ;
	singleSourcenavigatorModule = new SingleSourcenavigatorModule();
	singleSourcenavigatorFile = new SingleSourcenavigatorFile();
	singleAddress = new SingleAddress();




	singleSourcenavigator->setWindow(window) ;
	singleSourcenavigator->setTree1();
    printlog("r1");
	singleSourcenavigatorPart->setWindow(window) ;
	singleSourcenavigatorPart->setTree1();
    printlog("r12");
	singleSourcenavigatorModule->setWindow(window) ;
	singleSourcenavigatorModule->setTree1();
    printlog("r3");
	singleSourcenavigatorFile->setWindow(window) ;
	singleSourcenavigatorFile->setTree1();
    printlog("r4");
	//connect Button Signals

	window->get_widget("bSearch", pButton);
	pButton->signal_clicked().connect(sigc::mem_fun(this, &sourcenavigator::on_bSearch_clicked));

	window->get_widget("bEdit", pButton);
	pButton->signal_clicked().connect(sigc::mem_fun(this, &sourcenavigator::on_bEdit_clicked));


	window->get_widget("bProjectDMS", pButton);
	pButton->signal_clicked().connect(sigc::mem_fun(this, &sourcenavigator::on_bProjectDMS_clicked));


    
	// connect tree signals

	window->get_widget("tvProject", pTree1);
	pTree1->signal_row_activated().connect(sigc::mem_fun(this, &sourcenavigator::on_tvProject_activated));


	window->get_widget("tvPart", pTree1);
	pTree1->signal_row_activated().connect(sigc::mem_fun(this, &sourcenavigator::on_tvPart_activated));


	window->get_widget("tvModul", pTree1);
	pTree1->signal_row_activated().connect(sigc::mem_fun(this, &sourcenavigator::on_tvModul_activated));


	window->get_widget("tvFiles", pTree1);
	pTree1->signal_row_activated().connect(sigc::mem_fun(this, &sourcenavigator::on_tvFiles_activated));



	//Entry signals
	window->get_widget("eAddressNumber", pEntry);
	pEntry->signal_changed().connect(sigc::mem_fun(this, &sourcenavigator::on_eAddressNumber_changed));

	// connect combobox
	setComboBoxEntries("cbStatus","Projects.getComboBoxEntries");

	liModule = {MN["Sourcecode_project"],MN["Sourcecode_part"],MN["Sourcecode_modul"],MN["Sourcecode_file"]} ;

	tabChanged();

    return 1;

}

void sourcenavigator::on_new_activate() {
	printlog("project new 0") ;
	switch (tabOption) {

	case tabpro_Sourcenavigator:

		singleSourcenavigator->newData();
		break;
	case tabpro_SourcenavigatorPart:
		singleSourcenavigatorPart->newData();
		break;
	case tabpro_SourcenavigatorModule:
		singleSourcenavigatorModule->newData();
		break;
	case tabpro_SourcenavigatorFile:
		singleSourcenavigatorFile->newData();
		break;

	}
}
void sourcenavigator::on_save_activate() {
	printlog("project save 0");
	switch (tabOption) {
	case tabpro_Sourcenavigator:
		singleSourcenavigator->save();
		break;

	case tabpro_SourcenavigatorPart:
		singleSourcenavigatorPart->projectID = singleSourcenavigator->ID ;
		singleSourcenavigatorPart->save();
		break;
	case tabpro_SourcenavigatorModule:

		singleSourcenavigatorModule->partID = singleSourcenavigatorPart->ID ;
		singleSourcenavigatorModule->save();
		break;
	case tabpro_SourcenavigatorFile:

		singleSourcenavigatorFile->modulID = singleSourcenavigatorModule->ID ;
		singleSourcenavigatorFile->save();
		break;
	}
	tabChanged() ;
}


void sourcenavigator::on_delete_activate() {


	printlog("project deleteRecord 0");
	switch (tabOption) {
	case tabpro_Sourcenavigator:
		singleSourcenavigator->deleteRecord();
		break;

	case tabpro_SourcenavigatorPart:
		singleSourcenavigatorPart->deleteRecord();
		break;
	case tabpro_SourcenavigatorModule:


		singleSourcenavigatorModule->deleteRecord();
		break;
	case tabpro_SourcenavigatorFile:


		singleSourcenavigatorFile->deleteRecord();
		break;
	}
	tabChanged() ;

}

//Buttons
void sourcenavigator::on_bSearch_clicked() {}

void sourcenavigator::on_bEdit_clicked() {

	string sFile ;
	x1.callRP("Projects.getSourceFile",x1.add(singleSourcenavigatorFile->ID), sFile);
	printlog("File = " + sFile);
	string sPrg ;
	x1.callRP("Projects.getEditor",x1.add(singleSourcenavigatorFile->ID), sPrg);
	printlog("Prg = " + sPrg);

	vector<string> vPrg {sPrg,sFile} ;
	executePrg(vPrg);

}


void sourcenavigator::on_bProjectDMS_clicked(){
     

     if (singleSourcenavigator->ID > 0){
          map <string,int>* sep_info = new map <string,int> {{"1",singleSourcenavigator->ID}} ;
          dms* dmswindow = new dms(ModulNumber,sep_info);
          
          //dms(int module, map <string,int>* sep_info, map <string,string>* dicVars, map <string,string>* dicExtInfo )
     } //Dms = cuon.DMS.dms.dmswindow(self.allTables, self.ModulNumber, {'1':self.singleProject.ID})
}


void sourcenavigator::on_tvProject_activated(const Gtk::TreeModel::Path &path,
        Gtk::TreeViewColumn *) {
	pNotebook->set_current_page(1);
}

void sourcenavigator::on_tvPart_activated(const Gtk::TreeModel::Path &path,
        Gtk::TreeViewColumn *) {
	pNotebook->set_current_page(2);
}

void sourcenavigator::on_tvModul_activated(const Gtk::TreeModel::Path &path,
        Gtk::TreeViewColumn *) {
	pNotebook->set_current_page(3);
}

void sourcenavigator::on_tvFiles_activated(const Gtk::TreeModel::Path &path,
        Gtk::TreeViewColumn *) {
	on_bEdit_clicked();
}
// entry changes
void sourcenavigator::on_eAddressNumber_changed() {
//eAdrField = self.getWidget('tvOrderer')
	//      liAdr = self.singleAddress.getAddress(self.getWidget( 'eAddressNumber').get_text())
	//    self.setTextbuffer(eAdrField,liAdr)

	window->get_widget("tvOrderer",pTextview);


}
void sourcenavigator::tabChanged() {

	cout << "tab changed at project " << tabOption << " = " <<  endl;
	switch (tabOption) {

	case tabpro_Sourcenavigator:
		cout << "first project tab , old tab = " <<  oldTab << endl ;
		if (oldTab < 1) {
			singleSourcenavigator->getListEntries();
		}
       
		break;

	case tabpro_SourcenavigatorPart:
		cout << "second project tab , old tab = " <<  oldTab << endl ;
		singleSourcenavigatorPart->projectID = singleSourcenavigator->ID ;
		singleSourcenavigatorPart->sWhere = "where project_id = " + toString(singleSourcenavigator->ID) ;

		singleSourcenavigatorPart->getListEntries();
       
		break;
	case tabpro_SourcenavigatorModule:
		cout << "third project tab , old tab = " <<  oldTab << endl ;
		singleSourcenavigatorModule->partID = singleSourcenavigatorPart->ID ;
		singleSourcenavigatorModule->sWhere = "where part_id = " + toString(singleSourcenavigatorPart->ID) ;

		singleSourcenavigatorModule->getListEntries();
       
		break;
	case tabpro_SourcenavigatorFile:

		singleSourcenavigatorFile->modulID = singleSourcenavigatorModule->ID ;
		singleSourcenavigatorFile->sWhere = "where modul_id = " + toString(singleSourcenavigatorModule->ID) ;

		singleSourcenavigatorFile->getListEntries();
       
		
		break;

	}
	ModulNumber = liModule[tabOption] ;

	oldTab = tabOption;
}
/* void sourcenavigator::execute(xmlrpc_c::paramList const& paramList,xmlrpc_c::value * const retvalP){
     initSN();
     *retvalP =  xmlrpc_c::value_int(1);
} */


sourcenavigator::~sourcenavigator(){} 
