/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <libintl.h>
#include <locale.h>

#include <cuon/Sourcenavigator/SingleSourcenavigatorModule.hpp>

SingleSourcenavigatorModule::SingleSourcenavigatorModule(){

 sNameOfTable = "sourcecode_module";
    sTree = "tvModul";
    sSort = "name,designation";
    sWhere = "";
    ModulNumber = MN["Sourcecode_modul"];


    newEntry.sNameOfTable = sNameOfTable ;
    newEntry.sWhere = sWhere ;
    newEntry.sSort = sSort ;
    loadEntryField("sourcenavigator_module");
    setTreeFields();
       //names of database fields
    //a1 = {"name", "designation", "starts_at",  "ends_at", "process_status as status ", "id"} ;

    //tree header
    // h1 = {_("Name"), _("Designation"),_("Start at"), _("End at"),  _("Status"), _("ID") };

    // type
    // t1 = {"string","string","string","string","int","int"};



    // names of tree colums
    c1 = {"name","designation","starts_at","days","ends_at","status","id"} ;

    setLiFields();
}

void SingleSourcenavigatorModule::readExtraEntries()
{
     vector <string> v1;
     v1.push_back(toString(partID));
     v1.push_back("int");

     dicValues["part_id"] = v1;
}

SingleSourcenavigatorModule::~SingleSourcenavigatorModule(){}
