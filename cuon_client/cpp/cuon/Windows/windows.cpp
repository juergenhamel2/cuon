#include <map>
#include <list>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>
#include <stdio.h>
#include <unistd.h>

#include <cuon/Windows/windows.hpp>
#include <cuon/Databases/Singledata.hpp>


using namespace std;
const char clipboard_target_custom[] = "gtkmmclipboard";
const char clipboard_target_text[]   = "UTF8_STRING";

windows::windows()
{
     oldTab = -1;
     ModulNumber = 0;
     sWhereStandard = NULL;
     //Connect a signal handler that will be called when the contents of
     //the clipboard change.
     

}

string windows::getList(vector <string> liSearch){
     

}


windows::~windows()
{
     if(sWhereStandard) delete sWhereStandard ;
}

bool windows::onKeyPressAction(GdkEventKey *event){
     printlog("key pressed" + toString(event->keyval) );
     setNotebookPages(event);
     doNormalKeyEvents(event);
     doKeyAction(event);

}

void windows::doNormalKeyEvents(GdkEventKey *event){
     if (event->state & GDK_CONTROL_MASK){
          switch (event->keyval){
          case GDK_KEY_s:
               on_save_activate();
               break;
          case GDK_KEY_e:
               on_edit_activate();
               break;
          case GDK_KEY_d:
               on_delete_activate();
               break;
          case GDK_KEY_n:
               on_new_activate();
               break;


          }
     }
}
void windows::doKeyAction(GdkEventKey *event){

}

void windows::setNotebookPages(GdkEventKey *event){
     if (event->keyval == GDK_KEY_F1 ){
          printlog ("F1 pressed");
          pNotebook->set_current_page(0);
     }
     else if (event->keyval == GDK_KEY_F2 ){
          printlog ("F2 pressed");
          pNotebook->set_current_page(1);
     }
     else if (event->keyval == GDK_KEY_F3 ){

          pNotebook->set_current_page(2);
     }
     else if (event->keyval == GDK_KEY_F4 ){

          pNotebook->set_current_page(3);
     }
     else if (event->keyval == GDK_KEY_F5 ){

          pNotebook->set_current_page(4);
     }
     else if (event->keyval == GDK_KEY_F6 ){

          pNotebook->set_current_page(5);
     }
     else if (event->keyval == GDK_KEY_F7 ){

          pNotebook->set_current_page(6);
     }
     else if (event->keyval == GDK_KEY_F8 ){

          pNotebook->set_current_page(7);
     }
     else if (event->keyval == GDK_KEY_F9 ){

          pNotebook->set_current_page(8);
     }
     else if (event->keyval == GDK_KEY_F10 ){

          pNotebook->set_current_page(9);
     }

     else if (event->keyval == GDK_KEY_F11 ){

          pNotebook->set_current_page(10);
     }
     else if (event->keyval == GDK_KEY_F12 ){

          pNotebook->set_current_page(11);
     }
}

bool windows::setMainwindowNotebook(string sKey){

     int page_num = -1 ;
     int addPage = 0;
     /*   if self.ControlKey1 in self.ControlKeysSuper:
            addPage = 11 */
     if (sKey == "F1"){
          page_num = 0 + addPage ;
     }

     else if(sKey == "F2"){
          page_num = 1 + addPage ;
     }
     else if(sKey == "F3"){
          page_num = 2 + addPage ;
     }

      else if(sKey == "F4"){
          page_num = 3 + addPage ;
      }
       else if(sKey == "F5"){
          page_num = 4 + addPage ;
       }
       else if(sKey == "F6"){
          page_num = 5 + addPage ;
       }
       else if(sKey == "F7"){
          page_num = 6 + addPage ;
       }
       else if(sKey == "F8"){
          page_num = 7 + addPage ;
       }
       else if(sKey == "F9"){
          page_num = 8 + addPage ;
       }
       else if(sKey == "F10"){
          page_num = 9 + addPage ;
       }
       else if(sKey == "F11"){
          page_num = 10 + addPage ;
       }
       else if(sKey == "F12"){
          page_num = 11 + addPage ;
     }
     if (page_num >= 0){

          window->get_widget("notebook1", pNotebook);
          if (pNotebook){
               pNotebook->set_current_page(page_num);
          }
          
     }
}
/*
 def setMainwindowNotebook(self, sKey):
        
        page_num = -1
        addPage = 0
        if self.ControlKey1 in self.ControlKeysSuper:
            addPage = 11
        if sKey == 'F1':
            page_num = 0 + addPage
            
        elif sKey == 'F2':
            page_num = 1 + addPage
        elif sKey == 'F3':
            page_num = 2 + addPage
        elif sKey == 'F4':
            page_num = 3 + addPage
        elif sKey == 'F5':
            page_num = 4 + addPage
        elif sKey == 'F6':
            page_num = 5 + addPage
        elif sKey == 'F7':
            page_num = 6 + addPage
        elif sKey == 'F8':
            page_num = 7 + addPage
        elif sKey == 'F9':
            page_num = 8 + addPage
        elif sKey == 'F11':
            page_num = 9 + addPage
        elif sKey == 'F12':
            page_num = 10 + addPage
            
        if page_num >= 0:
            
            notebook = self.getWidget('notebook1')   
            if notebook:
                try:
                    notebook.set_current_page(page_num)
                    gtk.gdk.beep()
                except:
                    pass
        self.ControlKey1 = None
        self.ControlKey2 = None
        return True
*/


void windows::initKeys(){
     pWin->add_events( Gdk::KEY_PRESS_MASK );

     pWin->signal_key_press_event().connect ( sigc::mem_fun(*this, &windows::onKeyPressAction) );
     

}



void windows::tabChanged()
{

     cout << "tab changed at window" << endl;
}
void windows::tabChanged2()
{
}


void windows::closeWindow(){
     if(pMainWindow){
          pMainWindow->hide();
     }
}

void windows::setChooseButton(GtkButton*  button){
     chooseButton = button ;
}


        
       
void windows::setChooseEntry( Gtk::Entry* entry){
     // sName = name of Menuitem for choose
     // entry = entry to set value
     printlog("<<<<<<<<<<<<<<< setCooseEntry <<<<<<<<<<<<<<<<<<<<<");

     chooseEntry = entry;
     //chooseEntry->set_text("Test");
     chooseMenuItem->set_sensitive(true);
}

/*

    def pressChoosebutton(self):
        if self.chooseButton:
            self.activateClick(self.chooseButton, 'clicked')
    
  def preFindData(self):         
        pass
        
    def findData(self, singleDataDB):
        
    
        print 'find', self.liSearchFields
        liSearch = []
        for liValue in self.liSearchFields:
            if liValue[1] == 'STRING':
                sText = self.getWidget(liValue[0]).get_text()
                if sText:
                    liSearch.append(liValue[2].replace('%FIELD%',sText))
                    liSearch.append(sText)
                    
            if liValue[1] == 'INT':
                sText = self.getWidget(liValue[0]).get_text()
                if sText:
                    newID = ''
                    newOp = ''
                    try:
                        sID = sText.strip()
                        for c1 in sID:
                            if c1.isdigit():
                                newID += c1
                            else:
                                newOp += c1
                        if not newOp:
                            newOp = '='
                        liSearch.append(liValue[2].replace('%FIELD%',sText) + " " + newOp)
                        liSearch.append(int(newID))
                    except:
                        liSearch.append('id =' )
                        liSearch.append(0)
                            
                    
                    
                    
                    
            if liValue[1] == 'CHECKBOX':
                bok = self.getWidget(liValue[0]).get_active()
                print 'bok = ',  bok
                
                if bok > -1:
                   liSearch.append(liValue[2])
                   liSearch.append(0)
            print "liSearch = ",  liSearch      
                   
 
        self.postFindData()
        
        if liSearch:     
            self.singleOrder.sWhere = self.getWhere(liSearch) 
            #self.singleOrder.sWhere += " and process_status between 500 and 599 "
        #else:
            #self.singleOrder.sWhere = " where process_status between 500 and 599 "
        self.oldTab = -1
        
        self.refreshTree()
     
    def postFindData(self):    
            pass
 */

void windows::preFindData(){
}
void windows::postFindData(){
}
void windows::findData(Singledata* singleDataDB){
     for (auto liValue : liSearchFields){
          if (liValue[1] == "STRING"){
               window->get_widget(liValue[0],pEntry);
               string sText = pEntry->get_text();
               
               if (!sText.empty()){
                    string s = liValue[2];
                    replaceAll(s,"%FIELD%",sText);
                    lisSearch[s ] = sText ;
                    }
          }
               
          if (liValue[1] == "INT"){
               window->get_widget(liValue[0], pEntry) ;
               string sText = pEntry->get_text();
          
               if (! sText.empty() ){
                    string newID = "";
                    string newOp = "";
                    try{
                         trim(sText);
                         for (auto c1 : sText){
                              if (isdigit(c1)){
                                   newID += c1;
                              }
                              else{
                                   newOp += c1 ;
                              }
                              if ( newOp.empty() ){
                                   newOp = "=";
                              }
                              string s = liValue[2];
                              replaceAll(s,"%FIELD%",sText);
                              liiSearch[s + " " + newOp] = String2Int(newID);
                         }
                    }
                    catch(...){
                         liiSearch["id =" ] = 0;
                         
                    }
                    
                    
               }
          }

                
                    
          if (liValue[1] == "CHECKBOX"){
               window->get_widget(liValue[0],pCheckbox);
               bool bOK =  pCheckbox->get_active() ;
               liiSearch[liValue[2] ] = bOK ;
          }
     }

}
void windows::setChooseValue(int chooseValue){
     if( chooseMenuItem->is_sensitive() ){
          printlog("chooseValue = " + toString(chooseValue) );
          chooseEntry->set_text(toString(chooseValue));
          printlog("set sensitive to false");
    
          chooseMenuItem->set_sensitive(false);
          printlog("close Window");
          closeWindow();
    
          printlog("done close Window");
     }

}

/*
    def getChangedValue(self, sName):
        iNumber = 0
        s = self.getWidget( sName).get_text()
        if s:
            if len(string.strip(s)) > 0:
                if s.isdigit():
                    iNumber = long(s)
                else:
                    s = string.strip(s)
                    if s[len(s) - 1] == 'L':
                        iNumber = long(s[0:len(s) -1])
                    
        return iNumber
    
    */

