#ifndef WINDOWS_HPP
#define WINDOWS_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>
#include <cuon/Windows/gladeXml.hpp>
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/setOfEntries.hpp>
#include <cuon/Databases/Singledata.hpp>


class windows : public gladeXml  {

public:

	int ModulNumber ;
	 vector <int> liModule;
     vector < vector<string> > liSearchFields ;
     map <string,string> lisSearch ;
     
       map <string,int> liiSearch ;
     int oldTab ;
     int currentID;
     string* sWhereStandard;
     Gtk::Entry* chooseEntry ;
     Gtk::MenuItem* chooseMenuItem;
     Gtk::MenuItem* pMenuItem;
      
     // specialValue ;
     GtkButton* chooseButton;
     Glib::ustring m_ClipboardStore; //Keep copied stuff here, until it is pasted. This could be a big complex data structure
                        
     windows();
     ~windows();

    
     
     /// @param gladeName Name of the the glade file
     /// @param sMainWindow Name of the top window widget in the glade file
     ///@param EntryName Name of the Entry file
    
     virtual void tabChanged();
     virtual void tabChanged2();
    
     virtual bool onKeyPressAction(GdkEventKey *event);
     virtual void setNotebookPages(GdkEventKey *event);
     virtual void doKeyAction(GdkEventKey *event);
     virtual void doNormalKeyEvents(GdkEventKey *event);
     virtual void initKeys();
     virtual void closeWindow();
     virtual string getList(vector <string> liSearch);
     bool setMainwindowNotebook(string sKey);
     virtual void preFindData();
     virtual void postFindData();
     virtual void findData(Singledata* singleDataDB);
     
     void setChooseButton(GtkButton*  button);
     void setChooseValue(int chooseValue) ;
     void setChooseEntry( Gtk::Entry* entry);



     
};

class dialog : public gladeXml  {

public:
     dialog();
     ~dialog();
};
#endif

