#include <map>
#include <list>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>
#include <stdio.h>
#include <unistd.h>

#include <cuon/Windows/gtk_extensions.hpp>

using namespace std;


const char clipboard_target_custom[] = "gtkmmclipboard";
const char clipboard_target_text[]   = "UTF8_STRING";

gtk_extensions::gtk_extensions()
{

     //Connect a signal handler that will be called when the contents of
     //the clipboard change.
     //Gtk::Clipboard::get()->signal_owner_change().connect(sigc::mem_fun(*this,
     //         &gtk_extensions::on_clipboard_owner_change) );


     //Glib::RefPtr<Gtk::Clipboard> refClipboard = Gtk::Clipboard::get();


}
gtk_extensions::~gtk_extensions(){}



void gtk_extensions::set_text2clipboard(string sText){
     // Gtk::SelectionData* selection_data;
     Glib::RefPtr<Gtk::Clipboard> refClipboard = Gtk::Clipboard::get();
     refClipboard->set_text(sText);
     //selection_data->set(sText);
}
void gtk_extensions::on_clipboard_owner_change(GdkEventOwnerChange*){
 
        
      update_paste_status();
 
}

void gtk_extensions::on_clipboard_get(Gtk::SelectionData& selection_data, guint){
 
          //info is meant to indicate the target, but it seems to be always 0,
          //so we use the selection_data's target instead.
          
          const std::string target = selection_data.get_target();
          
          if(target == clipboard_target_custom)
          {
               // This set() override uses an 8-bit text format for the data.
               selection_data.set(clipboard_target_custom, m_ClipboardStore);
          }
          else if(target == clipboard_target_text)
          {
               //Build some arbitrary text representation of the data,
               //so that people see something when they paste into a text editor:
               Glib::ustring text_representation;
               
               
               selection_data.set_text(text_representation);
          }
          else
          {
               g_warning("gtk_extensions::on_clipboard_get(): "
                         "Unexpected clipboard target format.");
          }
 
}

void gtk_extensions::on_clipboard_clear(){

 
          //This isn't really necessary. I guess it might save memory.
          m_ClipboardStore.clear();
 
}

void gtk_extensions::on_clipboard_received( const Gtk::SelectionData& selection_data){
 
          const std::string target = selection_data.get_target();

          //It should always be this, because that' what we asked for when calling
          //request_contents().
          if(target == clipboard_target_custom)
          {
               Glib::ustring clipboard_data = selection_data.get_data_as_string();
               
               //See comment in on_button_copy() about this silly clipboard format.
               if(clipboard_data.size() >= 4)
               {
                    // m_ButtonA1.set_active( clipboard_data[0] == '1' );
                    // m_ButtonA2.set_active( clipboard_data[1] == '1' );
                    // m_ButtonB1.set_active( clipboard_data[2] == '1' );
                    // m_ButtonB2.set_active( clipboard_data[3] == '1' );
               }
          }
 
}

void gtk_extensions::update_paste_status(){
 
          //Disable the paste button if there is nothing to paste.

          Glib::RefPtr<Gtk::Clipboard> refClipboard = Gtk::Clipboard::get();

          //Discover what targets are available:
          refClipboard->request_targets(sigc::mem_fun(*this,
                                                      &gtk_extensions::on_clipboard_received_targets) );
 
}

void gtk_extensions::on_clipboard_received_targets(const Glib::StringArrayHandle& targets_array){
 
           // Get the list of available clipboard targets:
           std::vector<std::string> targets = targets_array;
           
           const bool bPasteIsPossible =
                std::find(targets.begin(), targets.end(),
                          clipboard_target_custom) != targets.end();
           
           // Enable/Disable the Paste button appropriately:
           //m_Button_Paste.set_sensitive(bPasteIsPossible);
 
}
