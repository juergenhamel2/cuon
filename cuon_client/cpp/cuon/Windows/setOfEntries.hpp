#ifndef SETOFENTRIES_HPP_INCLUDED
#define SETOFENTRIES_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <global.hpp>
#include <cuon/Windows/dataEntry.hpp>


class setOfEntries
{
    public:


        map <string, dataEntry> Entries;

        setOfEntries();
        ~setOfEntries();

        void addEntry(map <string,string> newMap);
        void setDic();
        map<string,string> getDicEntries();
map<string,vector <string> > getEntries();

};


#endif // SETOFENTRIES_HPP_INCLUDED
