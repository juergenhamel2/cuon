#ifndef GTK_EXTENSIONS_HPP
#define GTK_EXTENSIONS_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>


class gtk_extensions  {

public:

      
     // specialValue ;

     Glib::ustring m_ClipboardStore; //Keep copied stuff here, until it is pasted. This could be a big complex data structure
                        
     gtk_extensions();
     ~gtk_extensions();


     //Signal handlers Clipboard:
     void on_button_copy();
     void on_button_paste();
     void set_text2clipboard(string sText);
     void on_clipboard_owner_change(GdkEventOwnerChange* event);
     void on_clipboard_get(Gtk::SelectionData& selection_data, guint info);
     void on_clipboard_clear();

     void on_clipboard_received(const Gtk::SelectionData& selection_data);
     void on_clipboard_received_targets(const Glib::StringArrayHandle& targets_array);
   
     virtual void update_paste_status(); //Disable the paste button if there is nothing to paste.


     
};

#endif

