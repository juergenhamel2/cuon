/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <bzlib.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/DMS/SingleDMS.hpp>
#include <cuon/Encoder/smallBase64.hpp>



SingleDMS::SingleDMS(){

     
     imageData = NULL;
     //fileSuffix = NULL;
     sNameOfTable = "dms";
     sTree = "tree1";
     sSort = "title";
     sWhere = "";
     imageData = new string();


     newEntry.sNameOfTable = sNameOfTable ;
     newEntry.sWhere = sWhere ;
     newEntry.sSort = sSort ;
     loadEntryField("dms");
     setTreeFields();
     setLiFields();
     liUserData = {"NONE"};

}

SingleDMS::~SingleDMS(){
     if (base64) delete base64;
     if (imageData) delete imageData;

}

void SingleDMS::loadDocument(){
    
     if(imageData) delete imageData ;
     int verbosity = 0;
     printlog("DMS:load data for ID ",ID);
    
     string* rawData = new string() ;
     
     x1.callRP("DMS.getData",x1.add(ID,liUserData ), *rawData);
     printlog("length of imagedata0 = " , toString(rawData->size()) );
     const string* cRawData = rawData ;
     
     //const string *cImageData = new string(base64->base64_decode(*cRawData )) ;
     // imageData = base64->decompress_string(*cImageData);
    
     imageData =  new string(base64->base64_decode(*cRawData )) ;
    
     printlog("length of imagedata = " , (int) imageData->size() );

//      char* dest ;
//      unsigned int* destLen;
//      char* source = const_cast<char*> ( imageData2->c_str()) ;
//      unsigned int iSourceSize =  imageData2->size() ;
//      printlog("SourceSize = ", (int)iSourceSize);
//      int iRes = BZ2_bzBuffToBuffDecompress( dest , destLen, source, iSourceSize,0,0 );
//      if(iRes == BZ_OK){
               
//           printlog("bz2 result = OK");
//           //if (imageData) delete imageData;
     
//           printlog ("Value of destlen = ", (int) *destLen, sizeof(dest));
          
//           //const char* dest2 = const_cast<char*> (dest) ;
//           imageData = new string();
//           //imageData->assign(dest,(int)*destLen);
//           imageDataZ.assign(dest,(int)*destLen);
//           //imageData = (string*)dest;
          
//           printlog("length of imagedata orig = " ,(int) imageDataZ.size() );
          
//           //dest = NULL ;
//           //if (source) delete source ;
//           //if(imageData2) delete imageData2 ;
//           // if(cRawData) delete cRawData ;
//            // if(rawData) delete rawData ;
          
//           printlog("all ok at load document");
//      }
//      else{

//           printlog ("BZ Error code ",iRes);
//           printlog("BZ_CONFIG_ERROR",  BZ_CONFIG_ERROR);
//           printlog("BZ_PARAM_ERROR",BZ_PARAM_ERROR );
//           printlog("BZ_MEM_ERROR",BZ_MEM_ERROR );
//           printlog("BZ_OUTBUFF_FULL",BZ_OUTBUFF_FULL );
//           printlog("BZ_DATA_ERROR",BZ_DATA_ERROR );
//           printlog("BZ_DATA_ERROR_MAGIC",BZ_DATA_ERROR_MAGIC );
//           printlog("BZ_UNEXPECTED_EOF",BZ_UNEXPECTED_EOF );

          
          


          
//           /* BZ_CONFIG_ERROR
//   if the library has been mis-compiled
// BZ_PARAM_ERROR
//   if dest is NULL or destLen is NULL
//   or small != 0 && small != 1
//   or verbosity < 0 or verbosity > 4
// BZ_MEM_ERROR
//   if insufficient memory is available 
// BZ_OUTBUFF_FULL
//   if the size of the compressed data exceeds *destLen
// BZ_DATA_ERROR
//   if a data integrity error was detected in the compressed data
// BZ_DATA_ERROR_MAGIC
//   if the compressed data doesn't begin with the right magic bytes
// BZ_UNEXPECTED_EOF
//   if the compressed data ends unexpectedly
// BZ_OK
//   otherwise
// */
//           imageData = NULL ;

//      }
    

}
void SingleDMS::loadNotes0SaveDocument(){
     int nID;
     x1.callRP("Misc.getNotes0ID",x1.add(), nID);
     if (nID > 0){
          load(nID);
          fillOtherEntries();
     }
}


void  SingleDMS::saveBE(bool autoMode ){
     if (autoMode){
          printlog("automode, no Entries to read");
     }
     else{
          printlog("read entries in normal Mode");
          readEntries();
     }
     vector <string> v1 ;
     try{
           if (autoMode){
                saveAM();
           }
           else{
                save();
           }
           
          unsigned long iLen =  imageData->size();
          if (iLen > 0){

               
               printlog("encode image");
               //char* imageData2 = new char[iLen] ;
               //const char * source = imageData->c_str();
               //int rValue =  compress ( (unsigned char *)imageData2,&iLen ,(const unsigned char *) source,iLen );
               v1.push_back( base64->base64_encode( imageData->c_str() )) ;
               v1.push_back("string");
               printlog("length of v1 ",(int)v1.size());
               dicValuesBE["document_image"] = v1;
               saveValuesBE();
               
          }
     }
     catch(...){
          printlog ("No image_data found");
     }
}
   
void SingleDMS::loadMainLogo(){
     imageData = NULL;
     int id ;
     x1.callRP("Misc.getIdFromTitle",x1.add("cuon_mainwindow_logo"),id);
     printlog("mainlogo id = " + toString(id));
     if (id > 0)
     {
          load(id);
          loadDocument();
     }

     
     
     
     // self.imageData = None
     // id = self.rpc.callRP('Misc.getIdFromTitle', 'cuon_mainwindow_logo',  self.dicUser)
     // print "id for the logo = ",  id
     // if id > 0:
     //     self.load(id)
            
            
     //     self.loadDocument()
     //          return self.imageData
}

string SingleDMS::getFileFormatType(){
     string sFileFormat ;
     printlog("file format at DMS = " + firstRecord.row_string["file_format"] );
     x1.callRP("DMS.checkFileFormat", x1.add(firstRecord.row_string["file_format"]),sFileFormat);
     return sFileFormat ;
}

string SingleDMS::getFileExePrg(string sSuffix){
     printlog("Test");
     string sExe  ;
    
     printlog("file format at DMS = " + firstRecord.row_string["file_format"] );

     if(!sSuffix.empty()){
             x1.callRP("DMS.getFileExePrg", x1.add(sSuffix ),sExe);
     }
     else{
          x1.callRP("DMS.getFileExePrg", x1.add(firstRecord.row_string["file_format"] ),sExe);
     }
     printlog("dms exe ",sExe);
     // if (sExe == NULL)  sExe = "libreoffice" ;
     return sExe ;
}

string  SingleDMS::getFileSuffix(){

     string sSuffix =  firstRecord.row_string["file_suffix"] ;
     printlog("File-Suffix ",sSuffix);
     return sSuffix ;

}
