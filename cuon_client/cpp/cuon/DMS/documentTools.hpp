#ifndef DOCUMENTTOOLS_HPP_INCLUDED
#define DOCUMENTTOOLS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/DMS/SingleDMS.hpp>
#include <cuon/TypeDefs/basics.hpp>


class documentTools : basics
{
public:

    
     
     documentTools();
     ~documentTools();
     void viewDocument(SingleDMS* singleDMS,Record* dicVars,string* Action =NULL, vector <map<string,string> >* liEmailAddresses=NULL ) ;   
     SingleDMS* importDocument(SingleDMS* singleDMS, string sFile);
     void replaceValues(Record dicVars, string* s);
     
protected:

private:

};


#endif // DOCUMENTTOOLS_HPP_INCLUDED
