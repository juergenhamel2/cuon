#ifndef SINGLEDMS_HPP_INCLUDED
#define SINGLEDMS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>
#include <cuon/Encoder/smallBase64.hpp>


class SingleDMS : public Singledata
{
public:
     
     string* imageData ;
     string fileFormat;
     string fileSuffix;
     string newCategory ;
     string newDate ;
     string newTitle ;
     string Rights;
     string imageDataZ ;
        SingleDMS();
     ~SingleDMS();
     void loadDocument();
     void loadMainLogo();
     string getFileFormatType();
     string getFileExePrg(string sSuffix = "");
     string  getFileSuffix();
     void saveBE(bool autoMode=false );
     void loadNotes0SaveDocument();

     vector <string> liUserData;
    protected:

    private:

};

#endif // SINGLEDMS_HPP_INCLUDED
