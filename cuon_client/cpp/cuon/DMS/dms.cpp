/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/DMS/dms.hpp>
#include <cuon/DMS/SingleDMS.hpp>
#include <cuon/DMS/documentTools.hpp>

dms::dms(int module, map <string,int>* sep_info_1, Record* dicVars1, map <string,int>* dicExtInfo )
{

     //myXmlRpc x1 = myXmlRpc();
     documentTools*  oDocumentTools = new documentTools() ;
     dicVars = dicVars1 ;
     sepInfo = *sep_info_1 ;
     if(sepInfo.empty() ){
          sepInfo["1"] = MN["DMS"];
          
     }
     tree1Name = "tv_address";

     
     v_new.push_back( "new1");


     menuItems["new"] = v_new;

     v_save.push_back( "save1");

     menuItems["save"] = v_save;

     v_delete.push_back("clear1");

     menuItems["delete"] = v_delete ;

     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;


     // Toolbar Buttons
     v_tnew.push_back( "tbNew");
     toolbarItems["new"] = v_tnew;

     v_tsave.push_back( "tbSave");
     toolbarItems["save"] = v_tsave;

     v_tdelete.push_back( "tbDelete");
     toolbarItems["delete"] = v_tdelete;
     
     v_tend.push_back( "tbExit");
     toolbarItems["quit"] = v_tend;
     
     

     init("dms","DMSMainwindow","dms");

     singleDMS = new SingleDMS();

    

     singleDMS->setWindow(window) ;
     singleDMS->setTree1() ;
     
     //signals from buttons
     window->get_widget("bView", pButton);
     pButton->signal_clicked().connect(sigc::mem_fun(this, &dms::on_bView_clicked));

     window->get_widget("bImport", pButton);
     pButton->signal_clicked().connect(sigc::mem_fun(this, &dms::on_bImport_clicked));

     
     window->get_widget("bSearch", pButton);
     pButton->signal_clicked().connect(sigc::mem_fun(this, &dms::on_bSearch_clicked));

     
     // signals from the Tree
      window->get_widget("tree1", pTreeAll);
      pTreeAll->signal_row_activated().connect(sigc::mem_fun(this, &dms::on_row_activated));
     //connectSignal("mi_new1",new1);
      ModulNumber = MN["DMS"] ;


      window->get_widget("bSearch", pButton);
     pButton->signal_clicked().connect(sigc::mem_fun(this, &dms::on_bSearch_clicked));

      
      if(module >0){
           ModulNumber = module;
      }
      printlog("ModulNumber = ", ModulNumber);
      
      if (ModulNumber != MN["DMS"]){

           singleDMS->sWhere  =  " where insert_from_module = " + toString(ModulNumber) + " and  sep_info_1 = " +  toString(sepInfo["1"] )  ;
           printlog ("sWhere = ", singleDMS->sWhere );
      }
      else{
                //scd = cuon.Misc.cuon_dialog.cuon_dialog()

      }
      if (dicExtInfo != NULL && dicExtInfo->find("LastDoc") != dicExtInfo->end() ){
           
     }

      
     tabChanged();

}
dms::~dms() {

     if( singleDMS) delete  singleDMS;
     if (oDocumentTools) delete  oDocumentTools;
     if (sWhereStandard) delete sWhereStandard ;
     
}



void dms::on_new_activate()
{
     printlog ("dms new 0") ;
     switch (tabOption) {

     case tabDMS:

          singleDMS->newData();
          string sDate = getDate();
          
          // dicDate = self.getActualDateTime()

          Gtk::Entry* pE1 ;
          window->get_widget("eDocumentDate",pE1);
          pE1->set_text(sDate);
          break;
     }
}

void dms::on_save_activate(){
  printlog ("dms save 0") ;
     switch (tabOption) {

     case tabDMS:
          //singleDMS->save();
          auto oldID = singleDMS->ID;
          singleDMS->sep_info_1 = sepInfo["1"];
          singleDMS->ModulNumber = ModulNumber;
          printlog("oldID  = ", oldID);
          
          singleDMS->saveBE();
          auto newID = singleDMS->ID;

          printlog("oldID, newID ", oldID, newID);

          if( oldID == -1 and newID > 0){
               //is a new entry
               printlog( "file suffix = ", singleDMS->fileSuffix);
               string* s;
               x1.callRP("Misc.getTextExtract", x1.add(newID, singleDMS->fileSuffix ) ,*s);
          }
          tabChanged();


          
     }
}



void dms::on_delete_activate(){
  printlog ("dms delete 0") ;
     switch (tabOption) {

     case tabDMS:
          singleDMS->deleteRecord();
     }

}

void dms::on_bView_clicked(){
    
     // singleDMS->loadDocument();
     oDocumentTools->viewDocument(singleDMS, dicVars);
     printlog("started external prg in foreground and back");
     
}


void dms:: on_row_activated(const Gtk::TreeModel::Path& path , Gtk::TreeViewColumn* ){
     on_bView_clicked();

}
     
        
void dms::on_bImport_clicked(){
     string sFilename ;
     if(lastDoc.size() > 0){
          sFilename = lastDoc;

     }
     else{
          printlog("start search for filename");
          sFilename = getFilename("gfcb_ImportFile");

          printlog("sfilename ", sFilename);
     }
     singleDMS->imageData = readFile(sFilename);
     printlog("new imagedata size ", (int) singleDMS->imageData->size() );
}

void dms::on_bSearch_clicked(){
     singleDMS->setSearchString();
}




void dms::tabChanged()
{
     switch(tabOption) {

     case tabDMS:
           printlog("start tab dms");

            if (singleDMS->imageData){
                 delete singleDMS->imageData;
                 singleDMS->imageData = NULL ;
            }
           
           printlog("start tab dms2");
           singleDMS->getListEntries();
          
          break;
     }
     oldTab = tabOption;

}

/*
     # -*- coding: utf-8 -*-
##Copyright (C) [2003-2005]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

import sys
sys.path.append('/usr/lib/python/')
sys.path.append('/usr/lib/python/site-packages/PIL')

from types import *
import pygtk
pygtk.require('2.0')
import gtk
import locale
from locale import gettext as _


#import gtk.glade
import gobject
import string
import commands
import logging
from cuon.Windows.chooseWindows  import chooseWindows
#import cuon.Login.User
import SingleDMS
import dms 

import cuon.Misc.misc
import cuon.Misc.cuon_dialog

import os

try:
    import Image
except:
    print "no package Image found, try PIL"
    try:
        from PIL import Image
    except:
         print "last Try: no package Image found"
    
import bz2
import re
import binascii
import cuon.DMS.documentTools
import base64
import cuon.Project.project
import cuon.Order.order
import cuon.Addresses.addresses



class dmswindow(chooseWindows):

    
    def __init__(self, allTables, module = 0, sep_info = None, dicVars={}, dicExtInfo={}):
        
        chooseWindows.__init__(self)

        self.ModulNumber = self.MN['DMS']
        self.dicVars = dicVars
        self.dicExtInfo = dicExtInfo
        self.dicExtInfo['Modul'] = module 
        self.dicExtInfo['Sep_Info'] = sep_info 
        
            
        self.allTables = allTables
        
        self.openDB()
        self.oUser = self.loadObject('User')
        self.closeDB()
        #print self.oUser
        #print '-.............................'
        self.oDocumentTools = cuon.DMS.documentTools.documentTools()
        
        #print '1 --'
        self.singleDMS = SingleDMS.SingleDMS(allTables)
        #print '2 --'
        self.singleDMS.username = self.oUser.getUserName()
        self.loadGlade('dms.xml', 'DMSMainwindow')
        #self.win1 = self.getWidget('DMSMainwindow')
        #self.win1 = self.getWidget('DMSMainwindow')
        self.diaLink = self.getWidget('diaLink')
        self.diaLink.hide()
        
        self.scanfile = None
        self.liPrintNewsletter = None
        #print '3 --'
        self.EntriesPreferences = 'dms.xml'
        
        if sep_info:
            try:
                if sep_info.has_key('1'):
                    self.sepInfo['1'] = sep_info['1']
                if sep_info.has_key('2'):
                    self.sepInfo['2'] = sep_info['2']
                if sep_info.has_key('3'):
                    self.sepInfo['3'] = sep_info['3']
            except:
                print 'Error by sep-info'
        else:
            self.sepInfo['1'] = self.MN['DMS']

        ##print "Sep-Info 1 ",  self.sepInfo['1']
        #print '4 --'   
        if module > 0:
            self.ModulNumber = module
        if  self.ModulNumber != self.MN['DMS'] :
            self.sWhereStandard = ' where insert_from_module = ' + `self.ModulNumber`
            self.sWhereStandard += ' and  sep_info_1 = ' +  `self.sepInfo['1']`            
        else:
            scd = cuon.Misc.cuon_dialog.cuon_dialog()
        ##print '5 --'        
        if self.ModulNumber == self.MN['Newsletter']:
            cd = cuon.Misc.cuon_dialog.cuon_dialog()
            ok, res = cd.inputLine( _('Print Newsletter'), _('insert label(s) for newsletter'))
            print 'ok = ',  ok, 'Res = ',  res
            if ok and res:
                self.liPrintNewsletter = self.rpc.callRP('Address.getNewsletterAddresses', res, self.dicUser)
                print 'self.liPrintNewsletter = ',  self.liPrintNewsletter
                if self.liPrintNewsletter and self.liPrintNewsletter not in ['NONE','ERROR']:
                    self.getWidget('bFaxNewsletter').set_sensitive(True)
                    self.getWidget('bPrintNewsletter').set_sensitive(True)
    
        
        #print '6 --'
        self.loadEntries(self.EntriesPreferences)
        
        
        self.singleDMS.sWhere = self.sWhereStandard
        self.singleDMS.setEntries(self.getDataEntries('dms.xml') )
        self.singleDMS.setGladeXml(self.xml)
        self.singleDMS.ModulNumber = self.ModulNumber
        print 'self.singleDMS.ModulNumber', self.singleDMS.ModulNumber
        self.singleDMS.setTreeFields( ['title', 'category','document_date',"id"] )
        self.singleDMS.setStore( gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING,  gobject.TYPE_UINT) ) 
        self.singleDMS.setTreeOrder('title, document_date')
        self.singleDMS.setListHeader([_('Title'), _('Category'), _('Doc.-Date'),_("ID")])
        self.singleDMS.setTree(self.getWidget('tree1') )
        #print '7 --'
        self.singleDMS.imageWidget = self.getWidget('iThumbnail')
        #print '7 -0'
        # Menu-items
        self.initMenuItems()

        #print '7 05'
        
        # Close Menus for Tab
        self.addEnabledMenuItems('tabs','')
        print '7 -1'
        # enabledMenues for Preferences
        #self.addEnabledMenuItems('editProfile','profile1')
        self.addEnabledMenuItems('editDMS','clear1',self.dicUserKeys['delete'])
        #print '7 -2'
        #self.addEnabledMenuItems('editProfile','save1')
        self.addEnabledMenuItems('editDMS','new1',self.dicUserKeys['new'])
        #print '7 -3'
        self.addEnabledMenuItems('editDMS','edit1',self.dicUserKeys['edit'])
        #print '7 -4'
        # enabledMenues for Save 
        self.addEnabledMenuItems('editSave','save1', self.dicUserKeys['save'])
        # tabs from notebook
        self.tabDocument = 0
        self.tabAcc = 1
        self.tabExtract = 2
        
          # add keys
        print "accelgroup = ",  self.accel_group
        try:
            self.win1.add_accel_group(self.accel_group)
        except:
            pass
            
            
        #Now check for automatic-Actions
        self.LastDoc = None
        #print '9 --'
        try:
            if self.dicExtInfo and self.dicExtInfo.has_key('LastDoc'):
                print 'lastdoc found'
                if self.dicExtInfo['Save'] == 'NEW': 
                    self.activateClick('new1')
                    self.LastDoc =self.dicExtInfo['LastDoc']
                    print 'lastDoc = ',  self.LastDoc
                    self.activateClick('bImport',None,'clicked')
                    self.getWidget('eTitle').set_text('NEWLASTDOCUMENT')
                    if  dicExtInfo['Modul'] == self.MN['Address_info']:
                        self.ModulNumber =  self.MN['Address']
                        
                   
                        
                    self.activateClick('save1')
                    self.sWhereStandard = ' where insert_from_module = ' + `self.ModulNumber`
                    self.sWhereStandard += ' and  sep_info_1 = ' +  `self.sepInfo['1']`  
                    self.getWidget('eSearchTitle').set_text('NEWLASTDOCUMENT')
                    self.activateClick('bSearch', 'clicked')
                    del self.dicExtInfo['LastDoc']
                    del self.dicExtInfo['Save'] 
                    
        except Exception,  params:
            print 'error at dicExtInfo',  Exception,  params
            
        
        # notes
        
            
        self.textbufferMisc,  self.viewMisc = self.getNotesEditor()
        Vbox = self.getWidget('vbExtract')
        oldScrolledwindow = self.getWidget('scExtract')
        #oldScrolledwindow.remove(self.getWidget('tvNotesMisc'))
        oldScrolledwindow.add(self.viewMisc)
        self.viewMisc.show_all()
        oldScrolledwindow.show_all()
        
        
        
        #Vbox.remove(oldScrolledwindow)
        #Vbox.add(self.viewMisc)
        #Vbox.show_all()
        self.singleDMS.Extract = self.textbufferMisc



        #print '8 --'
        self.tabOption = self.tabDocument
        self.tabChanged()
        
      
    # Menu items
        

    def on_save1_activate(self, event):
        print 'save1'
        oldID = self.singleDMS.ID
        self.singleDMS.sep_info_1 = self.sepInfo['1']
        self.singleDMS.ModulNumber = self.ModulNumber
        newID = self.singleDMS.save(['document_image'])
        
        self.setEntriesEditable(self.EntriesPreferences, False)
        print 'oldID + and new = ',  oldID, newID
        if oldID == -1 and newID > 0:
            # is a new entry
            print 'file suffix = ',  self.singleDMS.fileSuffix
            s = self.rpc.callRP('Misc.getTextExtract', newID , self.singleDMS.fileSuffix, self.dicUser)
        self.tabChanged()
        
    def on_new1_activate(self, event):
        print 'DMS NEW1 activated'
        self.singleDMS.newRecord()
        dicDate = self.getActualDateTime()
        self.getWidget('eDocumentDate').set_text(dicDate['date'])
        
        self.setEntriesEditable(self.EntriesPreferences, True)
        

    def on_edit1_activate(self, event):
        if self.tabOption == self.tabDocument:
            self.setEntriesEditable(self.EntriesPreferences, True)
##        elif self.tabOption == self.tabPrinting:
##            self.setEntriesEditable(self.EntriesPreferencesPrinting, True)
##        elif self.tabOption == self.tabPathToReports:
##            self.setEntriesEditable(self.EntriesPreferencesPathToReports, True)
##        elif self.tabOption == self.tabPathToDocs:
##            self.setEntriesEditable(self.EntriesPreferencesPathToDocs, True)


    def on_clear1_activate(self, event):
        self.singleDMS.deleteRecord()

    def on_quit1_activate(self, event):
        self.closeWindow() 

    def on_chooseDMS_activate(self, event):
        # choose DMS from other Modul
        if self.tabOption == self.tabDocument:
            print '############### DMS choose ID ###################'
            self.setChooseValue(self.singleDMS.ID)
            self.closeWindow()
       

    def on_eDMSSearch_key_press_event(self, entry, event):
        print 'eSearch_key_press_event'
        if self.checkKey(event,'NONE','Return'):
            self.findDMS()
        
    def on_bSearch_clicked(self, event):
        self.findDMS()
        
    def findDMS(self):
        print 'Search'
        dicSearchfields = self.readSearchDatafields(  {'title':'eSearchTitle', 'category':'eSearchCategory',  'sub1':'eSearchSub1',  'sub2':'eSearchSub2',  'sub3': 'eSearchSub3',  'sub4':'eSearchSub4',  'sub5':'eSearchSub5',  'search1':'eSearchSearch1',  'search2': 'eSearchSearch2',   'search3':'eSearchSearch3',  'search4': 'eSearchSearch4', 'FullText':'eFindFullText'})

        print dicSearchfields
        
        sWhere = ''
        if dicSearchfields:
            for key in dicSearchfields.keys():
                if key == 'FullText':
                    if dicSearchfields[key]:
                        liFullText = dicSearchfields[key].split(' ')
                        if liFullText:
                            if sWhere:
                                sWhere +=  "and  ( "
                            else:
                                sWhere = " where   ("
                        
                            for sSearch in liFullText:
                                sWhere +=  " dms_extract  ~* \'"  + sSearch + "\'  and "
                            
                            sWhere = sWhere[:len(sWhere)-4] + " ) " 
                        
                else:
                    if dicSearchfields[key]:
                        if sWhere:
                            sWhere = sWhere + ' and ' +  key+ " ~* \'"  + dicSearchfields[key] + "\' "
                        else:
                            sWhere = 'where  ' +  key + " ~* \'"  + dicSearchfields[key] + "\' "
                            
            if self.ModulNumber != self.MN['DMS']:
                sWhere += ' and insert_from_module = ' + `self.ModulNumber` 
            
        else:
            sWhere = self.sWhereStandard
            
        print sWhere
        
        self.singleDMS.sWhere = sWhere
        self.refreshTree()
        
        
    def on_bScan_clicked(self, event):
        self.scanDocument()
        self.singleDMS.fileFormat = self.dicUser['prefDMS']['fileformat']['scanImage']['format']

    def on_bScanMulti_clicked(self,event):
        print 'Multi-Scan'
        try:
            status,data = commands.getstatusoutput('scan.sh')
            if status == 0:
                if data.find('FILENAME:') > -1:
                    filename = data[data.find('FILENAME:')+9:data.find('###')]
                    print 'Filename = ', filename
                    self.LastDoc = filename
                    self.on_bImport_clicked(None)
                
        except Exception, params:
            print Exception, params
            
        
    
    def on_bImport_clicked(self, event):
        print 'bImport'
        if self.LastDoc:
            filename = self.LastDoc
            self.LastDoc = None
        else:
            filename = self.getWidget("gfcb_ImportFile").get_filename()
            
        self.singleDMS = self.oDocumentTools.importDocument( self.singleDMS, self.dicUser, filename )
        print 'singleDMS= ',  self.singleDMS.fileSuffix,  self.singleDMS.fileFormat

    def on_bLink_clicked(self, event):
        print 'bLink'
        self.diaLink.show()
    def on_okbutton1_clicked(self, event):
        print 'ok clicked'
        sLink = self.getWidget('eLink').get_text()
        print sLink
        self.diaLink.hide()
        self.singleDMS.imageData = sLink
        self.singleDMS.fileFormat = self.dicUser['prefDMS']['fileformat']['LINK']['format']

    def on_cancelbutton1_clicked(self, event):
        print 'cancel clicked'
        self.diaLink.hide()
        
    def on_bFaxNewsletter_clicked(self, event):
        pass
    def on_bPrintNewsletter_clicked(self, event):
        print 'print Newsletter'
        print self.liPrintNewsletter
        for onePrint in self.liPrintNewsletter:
          self.oDocumentTools.viewDocument(self.singleDMS, self.dicUser, onePrint, Action='PrintNewsletter')  
        
    def on_bFaxLastDocument_clicked(self, event):
        if self.singleDMS.tmpFile:
            cDiag = cuon.Misc.cuon_dialog.cuon_dialog()
            ok, phone_number = cDiag.inputLine('Fax','Input Phone-Number')
            print ok 
            if ok:
                print self.singleDMS.tmpFile
                filename = self.singleDMS.tmpFile[:len(self.singleDMS.tmpFile)-3] + 'pdf'
                print filename
                singleDMS2 = SingleDMS.SingleDMS(self.allTables)
                
                self.oDocumentTools.importDocument( singleDMS2, self.dicUser, filename)
                #phone_number = '05744 511750'
                self.rpc.callRP('Misc.faxData',self.dicUser, base64.encodestring(singleDMS2.imageData),phone_number)
    def on_bWriteLastDocument_clicked(self, event):
        print 'write last document back'
        try:
            self.dicExtInfo['LastDoc'] = self.singleDMS.tmpFile
            self.dicExtInfo['Save'] = 'OVERWRITE'
            self.LastDoc = self.singleDMS.tmpFile
        except:
            print 'error at setting self.dicExtInfo',  self.dicExtInfo

        self.on_edit1_activate(None)
        
        self.on_bImport_clicked(None)
        self.on_save1_activate(None)
        
            
    def on_bWriteLastDocumentAs_clicked(self, event):
        print 'write last doc as',  self.dicExtInfo
        if self.dicExtInfo:
            self.dicExtInfo['LastDoc'] = self.singleDMS.tmpFile
            self.dicExtInfo['Save'] = 'NEW'
           
            print 'now open new Window ',  self.dicExtInfo
            dm2 = cuon.DMS.dms.dmswindow(self.allTables, self.dicExtInfo['Modul'], self.dicExtInfo['sep_info'],None,self.dicExtInfo)
  
    def on_tree1_columns_changed(self, event=None, data=None):
        print event, data
        
    def on_bJumpToOrigin_clicked(self, event):
        print 'jump to Origin'
        if self.singleDMS.ID > 0:
            OriginModulNumber,  OriginID = self.singleDMS.getOrigin()
            print OriginModulNumber, OriginID
            if OriginModulNumber == self.MN['Project']:
                proj = cuon.Project.project.projectwindow(self.allTables,  project_id = OriginID)
            elif OriginModulNumber == self.MN['Order'] :
                ord = cuon.Order.order.orderwindow(self.allTables,  orderid = OriginID)
            elif OriginModulNumber == self.MN['Address'] :
                adr = cuon.Addresses.addresses.addresswindow(self.allTables,  addrid = OriginID)
                    
        
    def on_tbJumpToOrigin_clicked(self, event):
        self.on_bJumpToOrigin_clicked(event)
        
    # toolbar buttons
   
    def on_tbNew_clicked(self, event):
        print "tbnew"
        if self.tabOption >= self.tabDocument:
            self.on_new1_activate(event)
        
    def on_tbEdit_clicked(self, event):
        if self.tabOption >= self.tabDocument:
            self.on_edit1_activate(event)
            
    def on_tbSave_clicked(self, event):
        if self.tabOption >= self.tabDocument:
            self.on_save1_activate(event)
            
            
    def on_tbDelete_clicked(self, event):
        if self.tabOption >= self.tabDocument:
            self.on_clear1_activate(event)
            
    def on_tbExit_clicked(self, event):
        print 'close'
        if self.tabOption >= self.tabDocument:
            self.on_quit1_activate(event)
       
    def on_bSearchPairedID_clicked(self, event):
       
        dms3 = cuon.DMS.dms.dmswindow(self.allTables,  0)
            
        dms3.setChooseEntry('chooseDMS', self.getWidget( 'ePairedID'))

    def on_ePairedID_changed(self, event):
        print 'ePairedID changed'
        iDMSID = self.getChangedValue('ePairedID')
        print iDMSID
        self.singleDMS.pairedID = iDMSID
    
    def refreshTree(self):
        self.singleDMS.disconnectTree()
    
        
        if self.tabOption == self.tabDocument:
            
            #self.singleDMS.sWhere = " where username = \'" + self.oUser.getUserName() + "\'"
            self.singleDMS.connectTree()
            self.singleDMS.refreshTree()
            #self.ModulNumber = self.MN['DMS']
            
    def tabChanged(self):
        self.out( 'tab changed to :'  + str(self.tabOption))
        
        if self.tabOption == self.tabDocument:
            #Preferences
            self.disableMenuItem('tabs')
            self.enableMenuItem('editDMS')

            self.actualEntries = self.singleDMS.getEntries()
            self.editAction = 'editDMS'
            self.setTreeVisible(True)
            self.out( 'Seite 0')
            
            self.singleDMS.setEntries(self.getDataEntries(self.EntriesPreferences) )
            # set the Entries manually, because there is no tree event
            self.singleDMS.fillEntries(self.singleDMS.ID)
           
        else:
            
            self.setTreeVisible(False)

        # refresh the Tree
        self.refreshTree()
        self.enableMenuItem(self.editAction)
        self.editEntries = False
        
    
    def scanDocument(self):
    
        self.oDocumentTools.scanDocument(self.singleDMS, self.dicUser)
        
   
                        
*/
