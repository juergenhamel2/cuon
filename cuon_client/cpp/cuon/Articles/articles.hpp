#ifndef ARTICLE_HPP_INCLUDED
#define ARTICLE_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Addresses/SingleAddress.hpp>
#define tabart_Articles 0


class articles:public windows
{
    public:

    SingleArticle *singleArticle ;
    SingleAddress *singleAddress ;
        articles();
        ~articles();

     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;

     void on_chooseArticle_activate();
     void on_liArticlesNumber1_activate();
     
     void on_bSearch_clicked();
     void tabChanged();


    protected:

    private:

};

#endif // ARTICLE_HPP_INCLUDED
