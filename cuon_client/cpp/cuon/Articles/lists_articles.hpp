#ifndef LISTS_ARTICLES_HPP_INCLUDED
#define LISTS_ARTICLES_HPP_INCLUDED
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Addresses/SingleAddress.hpp>


class lists_articles:public windows {

public:

     vector <string> values ;

     
     lists_articles();
     ~lists_articles();

     vector<string> getValues();
     

};



#endif // LISTS_ARTICLES_HPP_INCLUDED
