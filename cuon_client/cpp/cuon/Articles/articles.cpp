/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Articles/articles.hpp>
#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Articles/lists_articles.hpp>

articles::articles()
{

     tree1Name = "tv_article";


     v_new.push_back( "new1");


     menuItems["new"] = v_new;

     v_save.push_back( "save1");

     menuItems["save"] = v_save;

     v_delete.push_back("delete1");

     menuItems["delete"] = v_delete ;

     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;


     cout << "start article" << endl ;
     init("articles","ArticlesMainwindow","articles");


     window->get_widget("chooseArticle", chooseMenuItem);
     chooseMenuItem->signal_activate().connect(sigc::mem_fun(this, &articles::on_chooseArticle_activate));
     
     window->get_widget("articlenumber1", pMenuItem);
     pMenuItem->signal_activate().connect(sigc::mem_fun(this, &articles::on_liArticlesNumber1_activate));

     // init db
     singleArticle = new SingleArticle();
     singleAddress = new SingleAddress();

     singleArticle->setWindow(window) ;
     singleArticle->setTree1();
     cout << "start article tab changed" << endl ;
     tabChanged();

}

void articles::on_new_activate()
{
     printlog("articles new 0","") ;
     switch (tabOption) {

     case tabart_Articles:

          singleArticle->newData();
          break;
     }
}
void articles::on_save_activate()
{
     printlog ("articles save 0" );
     switch (tabOption) {
     case tabart_Articles:
          singleArticle->save();
          break;
     }
}

void articles::on_delete_activate() {}

void articles::on_chooseArticle_activate(){
     printlog("ID = " + toString(singleArticle->ID));
     setChooseValue(singleArticle->ID);
}

// menu List 
void articles::on_liArticlesNumber1_activate(){

     vector <string> dicSearchfields = {};
     lists_articles liArt = lists_articles();
     cout << "test glade" << endl;
     switch (liArt.pDia->run() ){
     case Gtk::RESPONSE_OK:
          std::cout << "OK was clicked\n";

          dicSearchfields = liArt.getValues();
          printlog(dicSearchfields[1]);
          liArt.pDia->hide();

          singleArticle->getArticlelist1(dicSearchfields);
          
          
          break;
     }

     
}
void articles::on_bSearch_clicked() {}

void articles::tabChanged()
{

     cout << "tab changed at articles" << tabOption << " = " << tabart_Articles << endl;
     switch(tabOption) {

     case tabart_Articles:
          cout << "first articles tab " << endl ;
          if(oldTab < 1) {
               singleArticle->getListEntries();
          }
     }
     oldTab = tabOption;
}




articles::~articles() {}
