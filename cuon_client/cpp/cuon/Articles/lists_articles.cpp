/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modeify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <libintl.h>
#include <locale.h>


#include <cuon/Articles/lists_articles.hpp>

lists_articles::lists_articles(){
     loadGlade("lists_articles","dialog1",1);
     cout << "gl1" << endl ;
     values = {};
}



lists_articles::~lists_articles(){


}


vector<string> lists_articles::getValues(){
     values.clear() ;
     
     values.push_back("number_from=" + ((getText("eNumberFrom") != "") ? getText("eNumberFrom"):"NONE" ) );
     
     values.push_back("number_to=" + ((getText("eNumberTo") != "") ? getText("eNumberTo"):"NONE" ));
     
     values.push_back("designation_from=" + ((getText("eDesignationFrom")!= "") ? getText("eDesignationFrom"):"NONE"));
    
     values.push_back("designation_to=" + ((getText("eDesignationTo") != "") ? getText("eDesignationTo"):"NONE"));

       
     values.push_back("materialgroup_from=" + ((getText("eMGFrom") != "") ? getText("eMGFrom"):"NONE"));
    
     values.push_back("materialgroup_to=" + ((getText("eMGTo") != "") ? getText("eMGTo"):"NONE" ));
     
     values.push_back("norm_from=" + ( (getText("eNormFrom") != "") ? getText("eNormFrom"):"NONE"));

     values.push_back("norm_to=" + ( (getText("eNormTo") != "") ? getText("eNormTo"):"NONE"));

     values.push_back("number_contains=" +( (getText("eNumberContains") != "") ? getText("eNumberContains"):"NONE"));

     values.push_back("designation_contains=" +( (getText("eDesignationContains") != "") ? getText("eDesignationContains"):"NONE"));

     values.push_back("materialgroup_contains=" +( (getText("eMGContains") != "") ? getText("eMGContains"):"NONE"));
     
     return values ;
}
