/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Articles/SingleArticle.hpp>

SingleArticle::SingleArticle()
{
     sNameOfTable = "articles";
     sTree = "tv_article";
     sSort = "number,designation";
     sWhere = "";



     newEntry.sNameOfTable = sNameOfTable ;
     newEntry.sWhere = sWhere ;
     newEntry.sSort = sSort ;
     loadEntryField("articles");


     //names of database fields
    a1= {"number as name", "designation", "fct_getValueAsCurrency(sellingprice1) as s1", "fct_getValueAsCurrency(sellingprice2) as s2", "fct_getValueAsCurrency(sellingprice3) as s3", "fct_getValueAsCurrency(sellingprice4) as s4", "unit as s5", "weight as f1","id"};

     //tree header
    h1 = {_("Number"), _("Designation"),_("Price 1"),_("Price 2"),_("Price 3"),_("Price 4"),_("Unit"),  _("Weight"), _("ID") };

     // type
    t1= {"string","string","string","string","string","string","string","float","int"};



     // names of tree colums
     c1 = {"name","designation","s1","s2","s3","s4","s5","f1","id"} ;

     setLiFields();
}

void SingleArticle::getArticlelist1(vector <string> dicSearchfields){
     string* pdf_file = new string("");
     int _id1 = 7;
    
     x1.callRP("Report.server_article_articlelist1",x1.add(dicSearchfields), *pdf_file );
     
     // cout << "pdf-file = " << *pdf_file << endl ;

     string sFilename = getRandomFilename(".pdf");
     string* decodeFile = decodeData(pdf_file);
     writeFile(sFilename, decodeFile);
     string sPrg = getFileExe();
     printlog("exe = " + sPrg);
     vector <string> vPrg {sPrg,sFilename};
     executePrg(vPrg);
     

}
SingleArticle::~SingleArticle() {}


/*        
    def readNonWidgetEntries(self, dicValues): 
        print "textbuffer object = ",self.NotesArticles 
        
        dicValues['articles_notes'] = [self.normalizeXML(self.NotesArticles.get_text(self.NotesArticles.get_start_iter(), self.NotesArticles.get_end_iter(), 1)) , 'text']
        print "articles text = ", dicValues['articles_notes']  
        return dicValues
        
        
    def fillOtherEntries(self, oneRecord):
        print 'oneRecord = ',  oneRecord
        self.NotesArticles.set_text(self.normalizeXML(oneRecord['articles_notes'], False) )
        dicPrices = self.rpc.callRP('Article.getTotalSellingPrices', oneRecord['id'], 0,  self.dicUser)
        print dicPrices
        self.getWidget('eTotalSellingPrice1').set_text(self.getCheckedValue(`dicPrices['ts1']`,  'toStringFloat'))
        self.getWidget('eTotalSellingPrice2').set_text(self.getCheckedValue(`dicPrices['ts2']`,  'toStringFloat'))
        self.getWidget('eTotalSellingPrice3').set_text(self.getCheckedValue(`dicPrices['ts3']`,  'toStringFloat'))
        self.getWidget('eTotalSellingPrice4').set_text(self.getCheckedValue(`dicPrices['ts4']`,  'toStringFloat'))
        
    def clearAllOtherFields(self):
        self.NotesArticles.set_text('')
   */     
vector <string> SingleArticle::getArticle(int _id){
     load(_id);
     
     vector <string> liArticle ;
     
       
     if (firstRecord.is_empty() ){
          try{
      
               liArticle.push_back(firstRecord.getStringValue("number") );
               liArticle.push_back(firstRecord.getStringValue("designation"));
               liArticle.push_back (" ");
               liArticle.push_back(" ");
               liArticle.push_back(" " );
          }
          catch(...) {
               /* */
                    
          }
                                
          if ( liArticle.empty() ){
               liArticle.push_back(" ");
               liArticle.push_back(" ");
               liArticle.push_back(" ");
               liArticle.push_back(" ");
               liArticle.push_back(" ");
          }
     }
     return liArticle;
                    
}

/*
    def getArticleFields(self, id):
        dicRecords = self.load(id)

    def getArticleNumber(self, id):
        dicRecords = self.load(id)
        return dicRecords[0]["number"]
    
    def getArticleDesignation(self, id):
        print "id = ",  id
        sDesignation = " "
        try:
            dicRecords = self.load(id)
            print dicRecords
            sDesignation = dicRecords[0]["designation"]
        except:
            pass
        return sDesignation
            
    def getArticleShort(self, id):
        dicRecords = self.load(id)
        sReturn = " "
        try:
            sReturn = dicRecords[0]["number"] + "," + dicRecords[0]["designation"]
        except:
            sReturn = " "
            
        return sReturn
        
    def getPrice(self, Modul, iModulID, ArticleID):
        return self.rpc.callRP("Article.getPrice", Modul,iModulID, ArticleID,  self.dicUser)
        
    def getSellingPrices(self, ArticleID,  PartID):
        return self.rpc.callRP("Article.getSellingPrices", ArticleID,  PartID,  self.dicUser)
        
    def getArticleAssociatedWith(self):
        return self.firstRecord["associated_with"]
        
*/
