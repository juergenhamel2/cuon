#ifndef SINGLEMATERIALGROUPS_HPP_INCLUDED
#define SINGLEMATERIALGROUPS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>

class SingleMaterialgroups:public Singledata
{
    public:
        SingleMaterialgroups();
        ~SingleMaterialgroups();
    protected:

    private:

};

#endif // SINGLEMATERIALGROUPS_HPP_INCLUDED
