#ifndef SINGLEARTICLESALE_HPP_INCLUDED
#define SINGLEARTICLESALE_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>

class SingleArticleSale:public Singledata
{
    public:
        SingleArticleSale();
        ~SingleArticleSale();
    protected:

    private:

};

#endif // SINGLEARTICLESALE_HPP_INCLUDED
