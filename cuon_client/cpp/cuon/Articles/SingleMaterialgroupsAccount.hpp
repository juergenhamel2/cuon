#ifndef SINGLEMATERIALGROUPSACCOUNT_HPP_INCLUDED
#define SINGLEMATERIALGROUPSACCOUNT_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>

class SingleMaterialgroupsAccount:public Singledata
{
    public:
        SingleMaterialgroupsAccount();
        ~SingleMaterialgroupsAccount();
    protected:

    private:

};

#endif // SINGLEMATERIALGROUPSACCOUNT_HPP_INCLUDED
