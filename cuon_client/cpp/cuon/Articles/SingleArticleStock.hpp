#ifndef SINGLEARTICLESTOCK_HPP_INCLUDED
#define SINGLEARTICLESTOCK_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Databases/Singledata.hpp>

class SingleArticleStock:public Singledata
{
    public:
        SingleArticleStock();
        ~SingleArticleStock();
    protected:

    private:

};

#endif // SINGLEARTICLESTOCK_HPP_INCLUDED
