#ifndef SINGLEORDERGET_HPP_INCLUDED
#define SINGLEORDERGET_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>

class SingleOrderGet:public Singledata {
public:
     int orderID ;
     int pickupNumber;
     int addressNumber;
     int partnerNumber;
     int forwardingAgencyNumber;
      int contactPersonNumber;
     SingleOrderGet();
     ~SingleOrderGet();
     virtual void readNonWidgetEntries();
     int getPickupNumber(int iOrderId);
     int getAddressNumber(int iOrderId);
          int getPartnerNumber(int iOrderId);
     int getForwardingAgencyNumber(int iOrderId);
     int getContactPersonNumber(int iOrderId);
protected:

private:

};

#endif // SINGLEORDERGET_HPP_INCLUDED
