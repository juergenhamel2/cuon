#ifndef SINGLEORDERINVOICE_HPP_INCLUDED
#define SINGLEORDERINVOICE_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>

class SingleOrderInvoice:public Singledata {
public:
     int orderID ;
     SingleOrderInvoice();
     ~SingleOrderInvoice();
     virtual void readNonWidgetEntries();
     
protected:

private:

};

#endif // SINGLEORDERINVOICE_HPP_INCLUDED
