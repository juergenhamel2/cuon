#ifndef SINGLEORDER_HPP_INCLUDED
#define SINGLEORDER_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>

class SingleOrder:public Singledata {
public:
     int supplyNumber;
     int deliveryNumber;
     int pickupNumber;
     
     SingleOrder();
     ~SingleOrder();

     string getOrderNumber(int id);
     int getInvoiceNumber();
     int getProposalNumber();
     int getPickupNumber();
     int getDeliveryNumber();
     
protected:

private:

};

#endif // SINGLEORDER_HPP_INCLUDED
