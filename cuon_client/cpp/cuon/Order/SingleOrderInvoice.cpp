/*Copyright (C) [2015]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. */


using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Order/SingleOrderInvoice.hpp>


 


SingleOrderInvoice::SingleOrderInvoice()
{

/*!
    \author: Jürgen Hamel
    \organization: Cyrus-Computer GmbH, D-32584 Löhne
    \copyright: by Jürgen Hamel
    \license: GPL ( GNU GENERAL PUBLIC LICENSE )
    \contact: jh@cyrus.de
         */

     orderID = 0;
     sNameOfTable = "orderinvoice";
     sTree = "tree1";
     sSort = "designation";
     sWhere = "";



     newEntry.sNameOfTable = sNameOfTable ;
     newEntry.sWhere = sWhere ;
     newEntry.sSort = sSort ;
     loadEntryField("orderinvoice");
     setTreeFields();


     setLiFields();
}

void SingleOrderInvoice::readNonWidgetEntries(){
      vector <string> v1;
     v1.push_back(toString(orderID));
     v1.push_back("int");

     dicValues["orderID"] = v1;
}


SingleOrderInvoice::~SingleOrderInvoice() {}

