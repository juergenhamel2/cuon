/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Order/SingleOrder.hpp>


 


SingleOrder::SingleOrder()
{

/*!
    \author: Jürgen Hamel
    \organization: Cyrus-Computer GmbH, D-32584 Löhne
    \copyright: by Jürgen Hamel
    \license: GPL ( GNU GENERAL PUBLIC LICENSE )
    \contact: jh@cyrus.de
         */
  
     sNameOfTable = "orderbook";
     sTree = "tvOrder";
     sSort = "designation";
     sWhere = "";



     newEntry.sNameOfTable = sNameOfTable ;
     newEntry.sWhere = sWhere ;
     newEntry.sSort = sSort ;
     loadEntryField("order");
     setTreeFields();


     setLiFields();
}

SingleOrder::~SingleOrder() {}



int SingleOrder::getInvoiceNumber(){
     int invoiceNumber = 0;
     x1.callRP("Order.getInvoiceNumber", x1.add(ID),invoiceNumber);
     printlog ("Invoice-Number",invoiceNumber );
     return invoiceNumber ;
}



int SingleOrder::getProposalNumber(){
     int proposalNumber = 0;
     x1.callRP("Order.getProposalNumber", x1.add(ID), proposalNumber);
     
     return proposalNumber;
}



    
int SingleOrder::getDeliveryNumber(){
     x1.callRP("Order.getDeliveryNumber", x1.add(ID),supplyNumber);

     return supplyNumber;
}


    
    
int SingleOrder::getPickupNumber(){
     //self.invoiceNumber =  self.rpc.callRP('Order.getInvoiceNumber', self.ID, self.dicUser)
     //   print 'Invoice-Number' + `self.invoiceNumber`
     return pickupNumber;
}


string SingleOrder::getOrderNumber(int id){
     string ordernr = "NULL";
     load(id);
        
     if (!firstRecord.row_string.empty()){
          ordernr = firstRecord.row_string["number"];

     }

     return ordernr;
}


        
/*    def fillOtherEntries(self, oneRecord):
        try:
            self.getWidget('eInvoiceNumber').set_text(`self.getInvoiceNumber()`)
            self.getWidget('eTotalSum').set_text( self.rpc.callRP('Order.getTotalSumString', self.ID, self.dicUser))
        except Exception, params:
            print Exception, params
            
        try:
            self.getWidget('ePaidAt').set_text( self.rpc.callRP('Order.getPaidAt', self.ID, self.dicUser))
        except Exception, params:
            print Exception, params    
        
        try:
            iGetNumber,  iSupplyNumber = self.rpc.callRP('Order.getSupply_GetNumber', self.ID, self.dicUser)
            print 'get/supply = ',  iGetNumber,  iSupplyNumber
            self.getWidget('eSupplyNumber').set_text(`iSupplyNumber`)
            self.getWidget('eGetsNumber').set_text(`iGetNumber`)
        except Exception,  params:
            print Exception,  params
            
    def setOtherEmptyEntries(self):
        self.getWidget('eInvoiceNumber').set_text('')
    
    def readNonWidgetEntries(self, dicValues):
        print "Non widget Ordertype = ", self.OrderType
        dicValues['process_status'] = [self.processStatus, 'int']
        dicValues['order_type'] = [self.OrderType, 'int']
        return dicValues
  
    
*/
