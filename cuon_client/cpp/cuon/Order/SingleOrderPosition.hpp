#ifndef SINGLEORDERPOSITION_HPP_INCLUDED
#define SINGLEORDERPOSITION_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>

class SingleOrderPosition:public Singledata {
public:
     int orderID ;
     SingleOrderPosition();
     ~SingleOrderPosition();
     virtual void readNonWidgetEntries();
     
protected:

private:

};

#endif // SINGLEORDERPOSITION_HPP_INCLUDED
