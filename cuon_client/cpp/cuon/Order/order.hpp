#ifndef ORDER_HPP_INCLUDED
#define ORDER_HPP_INCLUDED


#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Addresses/SingleAddress.hpp>
#include <cuon/Order/SingleOrder.hpp>
#include <cuon/Order/SingleOrderGet.hpp>
#include <cuon/Order/SingleOrderPosition.hpp>
#include <cuon/Order/SingleOrderPayment.hpp>
#include <cuon/DMS/documentTools.hpp>
#include <cuon/DMS/SingleDMS.hpp>
#define tabOrder_Order 0
#define tabOrder_Supply 1
#define tabOrder_Gets 2
#define tabOrder_Position 3
#define tabOrder_Specs 4
#define tabOrder_Misc 5
#define tabOrder_Payment 6
#define tabOrder_AddInfo 7




//#define tabstock_Goods 1


class order:public windows, public cuon_window {
     
    public:

     
     
     SingleOrder *singleOrder ;
     SingleArticle *singleArticle;
     SingleAddress *singleAddress ;
     SingleOrderPosition *singleOrderPosition;
     SingleOrderPayment *singleOrderPayment;
     SingleOrderGet* singleOrderGet;
     SingleDMS* singleDMS;
     
     documentTools* dT ;
     bool cashUser = false ;
     bool newList = false;
     
     string cashDeskPrinter ;
        order();
     ~order();
     //virtual int initAll();
     virtual int initOrder( map <string,string>* dicOrder, bool newOrder, int orderid ,string OrderType="Order");
     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     void on_print_invoice1_activate();
     
     void printInvoice(string sType);
     void on_bSearch_clicked();
     void on_rb_toggled();
     void tabChanged();

     void on_PositionSave1_activate();
     void on_PositionEdit1_activate();
     void on_PositionNew1_activate();
     void on_PositionDelete1_activate();


     void on_JournalAccount_activate();
     void on_order_dup_activate();

     
     void on_bArticleSearch_clicked();
     void on_eArticleID_changed();

     void on_bSearchCustom_clicked();
     void on_eAddressNumber_changed();
     void on_all_open_invoice1_activate();
     void on_print_delivery_note1_activate();
     void on_print_pickup_note1_activate();

     void on_DMS_activate();
     void on_bSearchStaff_clicked();
     void on_eStaffNumber_changed();

     void on_payment_new_activate();
     
     
     void createSimplePayment(string sType);

     


     
    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(order, cuon_window);



#endif // ORDER_HPP_INCLUDED
