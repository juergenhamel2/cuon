#ifndef USER_HPP
#define USER_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>
#include <cuon/Databases/datatypes.hpp>

//#include <cuon/Windows/gladeXml.hpp>

class User : public basics {

public:
     map <string,string> sUser ;
     map <string,int> iUser ;
     map <string,float> fUser;
     map <string,string> sSqlUser ;
     map <string,int> iSqlUser ;
     map <string,float> fSqlUser;
     map < string,vector< map <string,string> > > vUser;
     map <string,map <string,string> > mUser ;
     
     map <string,string> sServerData ;
     //Locale Map Struct:
     struct localeMap{
          int ID;
          string DateFormat;
     } ;

          
     map <string,localeMap*> Nations;
     
     User();
     ~User();
     void init();
     void update();
     void refreshSqlUser();
     void clearServerData();
     string getName();
     int getClient();
     int getProfileID();
     string getLocales();
     int getLocalesNumber();
     string getLocaleDateFormat();
     
};

#endif
