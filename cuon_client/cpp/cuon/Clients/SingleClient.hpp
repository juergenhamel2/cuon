#ifndef SINGLECLIENTS_HPP_INCLUDED
#define SINGLECLIENTS_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Databases/Singledata.hpp>

class SingleClient:public Singledata {
public:
     SingleClient();

     ~SingleClient();
protected:

private:

};

#endif // SINGLECLIENTS_HPP_INCLUDED
