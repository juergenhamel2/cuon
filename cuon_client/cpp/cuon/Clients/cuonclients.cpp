/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Clients/cuonclients.hpp>
#include <cuon/Clients/SingleClient.hpp>


 cuonclients:: cuonclients()
{

     tree1Name = "tv_clients";


     v_new.push_back( "new1");


     menuItems["new"] = v_new;

     v_save.push_back( "save1");

     menuItems["save"] = v_save;

     v_delete.push_back("delete1");

     menuItems["delete"] = v_delete ;

     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;


     cout << "start clients" << endl ;
     init("clients","ClientMainwindow","clients");

     // init db
     singleClient = new SingleClient();
     //singleAddress = new SingleAddress();

     singleClient->setWindow(window) ;
     singleClient->setTree1();
     cout << "start clients tab changed" << endl ;
     tabChanged();

}

void cuonclients::on_new_activate()
{
     printlog("clients new 0","") ;
     switch (tabOption) {

     case tabart_Clients:

          singleClient->newData();
          break;
     }
}
void  cuonclients::on_save_activate()
{
     printlog ("clients save 0" );
     switch (tabOption) {
     case tabart_Clients:
          singleClient->save();
          break;
     }
}

void  cuonclients::on_delete_activate() {}
void  cuonclients::on_bSearch_clicked() {}

void  cuonclients::tabChanged()
{

     cout << "tab changed at clients" << tabOption << " = " << tabart_Clients << endl;
     switch(tabOption) {

     case tabart_Clients:
          cout << "first clients tab " << endl ;
          if(oldTab < 1) {
               singleClient->getListEntries();
          }
     }
     oldTab = tabOption;
}


bool cuonclients::checkClientID(int clientID){

     bool ok ;
     x1.callRP("Database.checkUserClient", x1.add(oUser.getName(),clientID,false),ok );
     printlog("return of checkUserclient = " + toString(ok));
     return ok;
      
          
}

bool cuonclients::setClientID(int clientID){
     printlog("start the clients ID setting");
     bool ok = true;
     if ( checkClientID(clientID)){
          printlog("checkclient is ok");
          vector < Record > v1; 
          oUser.refreshSqlUser();
          x1.callRP("User.setUserData", x1.add(),v1 );
          loadProfileID();
          return ok;  
     }

}


void cuonclients::loadProfileID(){
     int i = 0;
      printlog("Profile-ID 0 = " + toString(i)) ;
     x1.callRP("User.getIDOfStandardProfile",x1.add(),i);
     printlog("Profile-ID = " + toString(i)) ;
     if (i>0){
          oUser.iUser["standard_profile"]=i;  
     }
     else{
          i=0;
     }
     
      printlog("Profile-ID2 = " + toString(i)) ;
}
cuonclients::~cuonclients() {
     
//     if (singleClient) delete  singleClient;
     printlog("client deletet");
//     if (singleAddress) delete singleAddress;
     printlog("addresses deletet");
}
