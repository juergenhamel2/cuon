#ifndef CUONCLIENT_HPP_INCLUDED
#define  CUONCLIENT_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Clients/SingleClient.hpp>
#include <cuon/Addresses/SingleAddress.hpp>
#define tabart_Clients 0


class cuonclients:public windows
{
    public:

SingleClient:pointer(0) *singleClient ;
    SingleAddress:pointer(0) *singleAddress ;
        cuonclients();
         ~cuonclients();

     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     void on_bSearch_clicked();
     void tabChanged();


    protected:

    private:

};

#endif //  CUONCLIENT_HPP_INCLUDED
