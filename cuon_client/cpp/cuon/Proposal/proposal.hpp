#ifndef PROPOSAL_HPP_INCLUDED
#define PROPOSAL_HPP_INCLUDED


#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Proposal/SingleProposal.hpp>


#define tabproposal_Proposal 0



class proposal:public windows, public cuon_window {
     
    public:

     
     
     SingleProposal *singleProposal ;

     SingleArticle *singleArticle;
     

        proposal();
        ~proposal();
     virtual int initAll();
     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     void on_bSearch_clicked();
     void tabChanged();


    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(proposal, cuon_window);



#endif // PROPOSAL_HPP_INCLUDED
