/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>


#include <cuon/Proposal/proposal.hpp>

#include <cuon/Articles/SingleArticle.hpp>
#include <Pluma/Connector.hpp>




PLUMA_CONNECTOR


  bool connect(pluma::Host& host){
     // add a  provider to host
     host.add( new proposalProvider() );
     return true;
 }



proposal::proposal()
{
}

int proposal::initAll(){
     tree1Name = "tv_proposal";


     v_new.push_back( "New1");


     menuItems["new"] = v_new;

     v_save.push_back( "Save1");

     menuItems["save"] = v_save;

     v_delete.push_back("Delete1");

     menuItems["delete"] = v_delete ;

     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;

// Toolbar Buttons
	v_tnew.push_back("tbNew");
	toolbarItems["new"] = v_tnew;

	v_tsave.push_back("tbSave");
	toolbarItems["save"] = v_tsave;

//	v_tdelete.push_back("tbDelete");
//	toolbarItems["delete"] = v_tdelete;



     cout << "start article" << endl ;
     init("proposal","ProposalMainwindow","proposal");

     // init db
     singleArticle = new SingleArticle();
     
     singleProposal = new SingleProposal();
     

     singleProposal->setWindow(window) ;
     singleProposal->setTree1();

   
     
     cout << "start proposal tab changed" << endl ;
     tabChanged();

     return 1;
     
}

void proposal::on_new_activate()
{
     printlog("proposal new 0","") ;
     switch (tabOption) {

     case tabproposal_Proposal:

          singleProposal->newData();
          break;
    
     }
}
void proposal::on_save_activate()
{
     printlog ("proposal save 0" );
     switch (tabOption) {
     case tabproposal_Proposal:
          singleProposal->save();
          break;
        
     }
}

void proposal::on_delete_activate() {}
void proposal::on_bSearch_clicked() {}

void proposal::tabChanged()
{

     cout << "tab changed at proposal" << tabOption << " = " << tabproposal_Proposal << endl;
     switch(tabOption) {

     case tabproposal_Proposal:
          cout << "first proposal tab " << endl ;
          if(oldTab < 1) {
               singleProposal->getListEntries();
          }
          break;
  
     }
     
     oldTab = tabOption;
}




proposal::~proposal() {}
