

#ifndef STLENCODERS_BASE64_HPP
#define STLENCODERS_BASE64_HPP

using namespace std;
#include <iostream>
#include <string>

class smallBase64 {

public:
     smallBase64();
     ~smallBase64();
     string base64_encode(const string &bindata);
     string base64_decode(const string &ascdata);
};



#endif
