#ifndef ORDER_HPP_INCLUDED
#define ORDER_HPP_INCLUDED


#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Order/SingleOrder.hpp>
#include <cuon/Order/SingleOrderPosition.hpp>


#define tabOrder_Order 0
#define tabOrder_Supply 1
#define tabOrder_Gets 2
#define tabOrder_Position 3
#define tabOrder_Specs 4
#define tabOrder_Misc 5
#define tabOrder_Payment 6
#define tabOrder_AddInfo 7




//#define tabstock_Goods 1


class order:public windows, public cuon_window {
     
    public:

     
     
     SingleOrder *singleOrder ;
     SingleArticle *singleArticle;
     SingleOrderPosition *singleOrderPosition;
     

        order();
     ~order();
     //virtual int initAll();
     virtual int initOrder( map <string,string>* dicOrder, bool newOrder, int orderid ,string OrderType);
     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     void on_bSearch_clicked();
     void tabChanged();

     void on_PositionSave1_activate();
     void on_PositionEdit1_activate();
     void on_PositionNew1_activate();
     void on_PositionDelete1_activate();
     

    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(order, cuon_window);



#endif // ORDER_HPP_INCLUDED
