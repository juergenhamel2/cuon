/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>


#include <cuon/Order/order.hpp>
#include <cuon/Order/SingleOrder.hpp>
#include <cuon/Order/SingleOrderPosition.hpp>


#include <cuon/Articles/SingleArticle.hpp>
#include <Pluma/Connector.hpp>




PLUMA_CONNECTOR


 bool connect(pluma::Host& host){
     // add a  provider to host
     host.add( new orderProvider() );
     return true;
 }



order::order(){
}


int order::initOrder( map <string,string>* dicOrder, bool newOrder, int orderid ,string OrderType){
     printlog("plugin initOrder starts over");
     tree1Name = "tree1";

    
     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;

     
    
     
     v_new.push_back( "new1");
     v_new.push_back( "PositionNew1");
     menuItems["new"] = v_new;

     v_save.push_back( "save1");
     v_save.push_back( "PositionSave1");
     menuItems["save"] = v_save;


     v_delete.push_back("PositionDelete1");

      menuItems["delete"] = v_delete ;


     
   

      
// Toolbar Buttons
	v_tnew.push_back("tbNew");
	toolbarItems["new"] = v_tnew;

	v_tsave.push_back("tbSave");
	toolbarItems["save"] = v_tsave; 

//	v_tdelete.push_back("tbDelete");
//	toolbarItems["delete"] = v_tdelete;

 

     cout << "start article" << endl ;
     init("order","OrderMainwindow","order");

     // init db
     singleArticle = new SingleArticle();
     
    
     
     singleOrderPosition = new SingleOrderPosition();
     

     singleOrderPosition->setWindow(window) ;
     singleOrderPosition->setTree1();
  
      singleOrder = new SingleOrder();
     

     singleOrder->setWindow(window) ;
     singleOrder->setTree1();
     
     cout << "start order tab changed" << endl ;
     tabChanged();

     return 1;
     
}
/*********************** Menu File ***************/




/*********************** Menu Order ***************/

void order::on_new_activate()
{
     printlog("stock new 0","") ;
     switch (tabOption) {

     case tabOrder_Order:

          singleOrder->newData();
          break;

     }
}
void order::on_save_activate()
{
     printlog ("stock save 0" );
     switch (tabOption) {
     case tabOrder_Order:
          singleOrder->save();
          break;

     }
}

void order::on_delete_activate() {}




void order::on_bSearch_clicked() {}

void order::tabChanged()
{

     cout << "tab changed at stock" << tabOption << " = " << tabOrder_Order << endl;
     switch(tabOption) {

     case tabOrder_Order:
          cout << "first order tab " << endl ;
          if(oldTab < 1) {
               singleOrder->getListEntries();
          }
          break;

     
     case tabOrder_Position:
          cout << "4'th tab Position " << endl ;
          singleOrderPosition->orderID = singleOrder->ID ;
          
          singleOrderPosition->getListEntries();
         
          break;

     }
     
     oldTab = tabOption;
}




order::~order() {}
