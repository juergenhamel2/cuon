/* -*- coding: utf-8 -*-
##Copyright (C) [2016]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

*/


using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Garden/SingleHibernation.hpp>


SingleHibernation::SingleHibernation(){
 
     
     sNameOfTable = "hibernation";
     sTree = "tree1";
     sSort = "hibernation_number";
     sWhere = "";
     


     newEntry.sNameOfTable = sNameOfTable ;
     newEntry.sWhere = sWhere ;
     newEntry.sSort = sSort ;
     loadEntryField("hibernation");
     setTreeFields();
     setLiFields();
   

}

SingleHibernation::~SingleHibernation(){

}



/*

        
    def getHibernation(self, id):
        dicRecords = self.load(id)
        liHibernation = []
        if dicRecords:
            dicRecord = dicRecords[0]
            liHibernation.append(dicRecord['number'])
            liHibernation.append(dicRecord['designation'])
            liHibernation.append (' ')
            liHibernation.append(' ')
            liHibernation.append(' ' )
        if not liHibernation:
            liHibernation.append(' ')
            liHibernation.append(' ')
            liHibernation.append(' ')
            liHibernation.append(' ')
            liHibernation.append(' ')
            
        return liHibernation

       
    def getHibernationFields(self, id):
        dicRecords = self.load(id)
        
        
    def getHibernationRecord(self, id):
        
        dicRecords = self.load(id)
        print 'dicRecords = ', dicRecords
        
        if dicRecords and len(dicRecords) >= 1:
            return dicRecords[0]
        else:
            return {}
            
        
    def getHibernationNumber(self, id):
        dicRecords = self.load(id)
        return dicRecords[0]['number']
    
    def getHibernationAddressNumber(self, id):
        dicRecords = self.load(id)
        return dicRecords[0]['addressnumber']
    
    def getBeginEndDate(self, id):
        dicRecords = self.load(id)
        return [dicRecords[0]['begin_date'],dicRecords[0]['end_date']]
        
    def fillOtherEntries(self, oneRecord):
        print 'fill SumTotal'
        totalSum =  self.rpc.callRP('Garden.getSum', self.ID, self.dicUser)
        
        self.getWidget('eTotalSum').set_text(totalSum)
    def setOtherEmptyEntries(self) :
        self.getWidget('eAddressNumber').set_text('0')
        self.getWidget('eBeginStaffName').set_text('0')
        self.getWidget('eEndsStaffName').set_text('0')
        
        
*/
