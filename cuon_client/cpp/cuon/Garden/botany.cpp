/*Copyright (C) [2016]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Garden/botany.hpp>
#include <cuon/Garden/SingleBotanyDivisio.hpp>

#include <Pluma/Connector.hpp>




PLUMA_CONNECTOR



  bool connect(pluma::Host& host){
     // add a  provider to host
     host.add( new botanyProvider() );
     return true;
 }



botany::botany()
{

}
int botany::initAll() {

     printlog("start botany Mainwindow");
     tree1Name = "tree1";

     ModulNumber = MN["Divisio"] ;
     
     v_new.push_back( "divisio_new1");
     menuItems["new"] = v_new;

     v_save.push_back( "divisio_save1");
     menuItems["save"] = v_save;

     v_delete.push_back("divisio_clear1");
     menuItems["delete"] = v_delete ;

     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;


     cout << "start botany" << endl ;
     init("botany","BotanyMainwindow","botany_divisio");

     // init db
     singleBotanyDivisio = new SingleBotanyDivisio();


     singleBotanyDivisio->setWindow(window) ;
     singleBotanyDivisio->setTree1();
     cout << "start Botany tab changed" << endl ;
     tabChanged();
     return 1;
}

void botany::on_new_activate()
{
     printlog("botany new 0","") ;
     switch (tabOption) {

     case tabart_Divisio:

          singleBotanyDivisio->newData();
          break;
     }
}
void botany::on_save_activate()
{
     printlog ("botany save 0" );
     switch (tabOption) {
     case tabart_Divisio:
          singleBotanyDivisio->save();
          break;
     }
}

void botany::on_delete_activate() {}
void botany::on_bSearch_clicked() {}

void botany::tabChanged()
{

     cout << "tab changed at botany" << tabOption << " = " << tabart_Divisio << endl;
     switch(tabOption) {

     case tabart_Divisio:
          cout << "first botany tab " << endl ;
          if(oldTab < 1) {
               singleBotanyDivisio->getListEntries();
          }
     }
     oldTab = tabOption;
}




botany::~botany() {}
