#ifndef BOTANY_HPP_INCLUDED
#define BOTANY_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Garden/SingleBotanyDivisio.hpp>

#define tabart_Divisio 0

#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"


class botany:public windows,public cuon_window{
    public:

    SingleBotanyDivisio *singleBotanyDivisio ;

        botany();
        ~botany();

     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     virtual int  initAll();
     void on_bSearch_clicked();
     void tabChanged();


    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(botany, cuon_window);


#endif // BOTANY_HPP_INCLUDED
