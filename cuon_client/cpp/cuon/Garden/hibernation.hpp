#ifndef HIBERNATION_HPP_INCLUDED
#define HIBERNATION_HPP_INCLUDED


#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Articles/SingleArticle.hpp>
#include <cuon/Addresses/SingleAddress.hpp>
#include <cuon/Order/SingleOrder.hpp>
#include <cuon/Order/SingleOrderGet.hpp>
#include <cuon/Order/SingleOrderPosition.hpp>
#include <cuon/Order/SingleOrderPayment.hpp>
#include <cuon/DMS/documentTools.hpp>
#include <cuon/DMS/SingleDMS.hpp>
#include <cuon/Garden/hibernation.hpp>
#include <cuon/Garden/SingleHibernation.hpp>

#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"




#define tabHibernation_Hibernation 0
#define tabHibernation_Supply 1
#define tabHibernation_Gets 2
#define tabHibernation_Position 3
#define tabHibernation_Specs 4
#define tabHibernation_Misc 5
#define tabHibernation_Payment 6
#define tabHibernation_AddInfo 7




//#define tabstock_Goods 1


class hibernation:public windows, public cuon_window {
     
    public:

     hibernation();
     ~hibernation();
     
     int addressid;
     bool newHibernation;
     int hibernation_id;
     bool allInvoices;
     
     
     SingleHibernation *singleHibernation ;
     SingleOrder *singleOrder ;
     
     SingleArticle *singleArticle;
     SingleAddress *singleAddress ;
     SingleOrderPosition *singleOrderPosition;
     SingleOrderPayment *singleOrderPayment;
     SingleOrderGet* singleOrderGet;
     SingleDMS* singleDMS;
     
     documentTools* dT ;

     virtual void tabChanged();
     
     virtual int initHibernation( int iaddressid = 0,  bool  inewHibernation=false,  int ihibernation_id = 0);
   
   
      virtual void on_new_activate() ;
      virtual void on_save_activate();
      virtual void on_delete_activate() ;


     
    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(hibernation, cuon_window);


#endif // HIBERNATION_HPP_INCLUDED

