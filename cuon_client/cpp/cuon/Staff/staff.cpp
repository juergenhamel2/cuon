
using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <algorithm>

#include  <cuon/DMS/dms.hpp>
#include <cuon/DMS/SingleDMS.hpp>
#include <cuon/Staff/staff.hpp>
#include <cuon/Staff/SingleStaff.hpp>
#include <Pluma/Connector.hpp>




PLUMA_CONNECTOR


 bool connect(pluma::Host& host){
     // add a  provider to host
     host.add( new staffProvider() );
     return true;
 }

staff::staff(){
}

int staff::initAll(){
      v_new.push_back( "new1");
     // v_new.push_back( "PositionNew1");
     // v_new.push_back( "GetsNew1");
      menuItems["new"] = v_new;
     
      v_save.push_back( "save1");
     // v_save.push_back( "PositionSave1");
     // v_save.push_back( "GetsSave1");
      menuItems["save"] = v_save;

      v_delete.push_back("clear1");
      menuItems["delete"] = v_delete ; 
     
     init("staff","StaffMainwindow","staff");
     
     ModulNumber = MN["staff"];

     singleStaff = new SingleStaff();
     singleStaff->setWindow(window);
     singleStaff->setTree1();

     tabChanged();
     
}




void staff::tabChanged()
{

     printlog ("tab changed at staff", tabOption , " = " ,tabStaff_Staff ) ;
     switch(tabOption) {

     case tabStaff_Staff:
          if(oldTab < 1) {
               ModulNumber = MN["Staff"] ;
               singleStaff->getListEntries();
          }
          break;

     }
}










staff::~staff(){

}
