#ifndef STAFF_HPP_INCLUDED
#define STAFF_HPP_INCLUDED


#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>

#include <cuon/DMS/documentTools.hpp>
#include <cuon/DMS/SingleDMS.hpp>
#include <cuon/Staff/SingleStaff.hpp>




#define tabStaff_Staff 0




//#define tabstock_Goods 1


class staff:public windows, public cuon_window {
     
public:
     SingleStaff* singleStaff ;
     staff();
     ~staff();
     virtual int initAll();
     virtual void tabChanged();
     
    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(staff, cuon_window);



#endif // STAFF_HPP_INCLUDED
