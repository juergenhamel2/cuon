 #ifndef BASICS_HPP_INCLUDED     
#define BASICS_HPP_INCLUDED
#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <iostream>
#include <unistd.h>
#include <string>
#include <map>
#include <vector>
#include <locale.h>
#include <ctime>

using namespace std;

#define NULL_UUID "00000000-0000-0000-0000-000000000000"


class basics {
public:
     basics();
     ~basics();

     map <string,int> MN ;
     map <string,string> mime_type ;
	 map  <string,int> mLogLevel ;
     map <string, vector <string> > defLocales ;
     
     void printlog(string s1,map< string,int> mLevel = {{"LogLevel",9}} );
     void printlog(string s1, string s2,map< string,int> mLevel = {{"LogLevel",9}});
     void printlog(string s1, string s2, int i1,map< string,int> mLevel = {{"LogLevel",9}});
     void printlog(string s1, int i1,map< string,int> mLevel = {{"LogLevel",9}});
     void printlog(string s1, int i1, int i2,map< string,int> mLevel = {{"LogLevel",9}});
     void printlog(string s1, int i1, string s2, int i2,map< string,int> mLevel = {{"LogLevel",9}});
     void printlog(string s1, float f1,map< string,int> mLevel = {{"LogLevel",9}});

     bool file_exists (string name) ;
     string stripchars(std::string str, const std::string &chars=" ");
     void ltrim(std::string &s);
     void rtrim(std::string &s);
     void trim(std::string &s) ;
     string upper(string cmd);
     void replaceAll( string &s, const string &search, const string &replace ) ;
     string getNullUUID();
     string getNewUUID();
     int executePrg(vector <string> prg);
     string  getRandomFilename(string sPrefix=".tmp");
     void writeFile(string sName, string* sData);
     string* readFile(string sName);
     string getDateTime();
     string getDate();
     string getDate( tm* _date);
     int String2Int(string sInt);
     string Int2String(int i);
     map <string,string> getActualDateTime();
     vector <string> map2vector(map <string,string> mMap);
     vector <string> split(const  string  &theString,
       const  string  &theDelimiter);

     // normal functions
     void deleteCache();
     string getLocalizedString(float value);
     string getLocalizedString(int  value);
     
protected:

private:

};
//
//// string
//char string[] = "938";
//
//// string -> int
//int i = fromString<int>(string);
//
//// string -> double
//double d = fromString<double>(string);
//
//// int -> string
//std::string str1 = toString<int>(i);
//
//// double -> string
//std::string str2 = toString<double>(d);

template<class T> std::string toString(const T& t)
{
     std::ostringstream stream;
     stream << t;
     return stream.str();
}

template<class T> T fromString(const std::string& s)
{
     std::istringstream stream (s);
     T t;
     stream >> t;
     return t;
}


#endif // BASICS_HPP_INCLUDED
