#include <cuon/TypeDefs/basics.hpp>     
#include <algorithm>
#include <iostream>
#include <sstream>
#include <vector>
#include <fstream>
#include <global.hpp>
#include <locale.h>
#include <ctime>

#include <Pluma/src/Pluma/uce-dirent.h>

basics::basics() {
    
	 MN["Mainwindow"] = 10;
     MN["Client"] = 1000;
     
     MN["Address"] = 2000;
     MN["Address_info"] = 2001;
     MN["Address_stat_caller"] = 2002;
     MN["Address_stat_rep"] = 2003;
     MN["Address_stat_salesman"] = 2004;
     MN["Address_stat_schedul"] = 2005;
     
     MN["Partner"] = 2100;
     MN["Partner_info"] = 2101;
      
     MN["Notes"] = 2110;
     MN["Partner_Schedul"] = 2200;
     MN["Partner_Schedul_info"] = 2201;
     
     MN["Articles"] = 3000;
     MN["Articles_stat_misc1"] = 3001;
     
     MN["MaterialGroups"] = 23000;
     
     
     
     MN["Order"] = 4000;
     
     MN["Order_stat_misc1"] = 4001;
     MN["Order_stat_global1"] = 4002;
     MN["Order_stat_caller1"] = 4003;
     MN["Order_stat_rep1"] = 4004;
     MN["Order_stat_salesman1"] = 4005;
     MN["PROPOSAL"] = 4500;
     
     MN["Stock"] = 5000;
     MN["Staff"] = 6000;
     MN["StaffFee"] = 6100;
     
     MN["Invoice"] = 7000;
     MN["Invoice_info"] = 7001;
     MN["Invoice_stat_taxvat1"] = 7101;
     
     
     MN["DMS"] = 11000;
     MN["Forms_Address_Notes_Misc"] = 11010;
     MN["Forms_Address_Notes_Contacter"] = 11011;
     MN["Forms_Address_Notes_Rep"] = 11012;
     MN["Forms_Address_Notes_Salesman"] = 11013;
     MN["Newsletter"] = 11100;
     
     MN["Biblio"] = 12000;
     MN["AI"] = 13000;
     
     MN["Project"] = 14000;
     MN["Project_info"] = 14001;
     MN["Project_stat_misc1"] = 14011;
     MN["Project_phase"] = 14100;
     MN["Project_task"] = 14200;
     MN["Project_staff_resources"] = 14300;
     MN["Project_material_resources"] = 14400;
     MN["Project_programming"] = 14500;
     
     MN["Sourcecode_project"] = 15000;
     MN["Sourcecode_part"] = 15100;
     MN["Sourcecode_modul"] = 15200;
     MN["Sourcecode_file"] = 15300;
     
     
     MN["SupportTicket"]  = 17000;
     
     
     MN["Web2"] = 20000;
     MN["Stats"] = 22000;
     MN["Calendar"] = 28000;
     MN["Think"] = 29000;
     
     MN["Leasing"] = 30001;;
     
     MN["Grave"] = 40000;
     MN["GraveServiceNotesService"] = 40100;
     MN["GraveServiceNotesSpring"] = 40101;
     MN["GraveServiceNotesSummer"] = 40102;
     MN["GraveServiceNotesAutumn"] = 40103;
     MN["GraveServiceNotesWinter"] = 40104;
     MN["GraveServiceNotesAnnual"] = 40105;
     MN["GraveServiceNotesUnique"] = 40106;
     MN["GraveServiceNotesHolliday"] = 40107;
     
     MN["Graveyard"] = 41000;
     MN["Garden"] = 42000;
     
     MN["Hibernation"] = 110000  ;
     MN["HibernationPlantNumber"] = 110100;
     
     
     MN["Botany"] = 110500;
     MN["Divisio"] = 110510;  



     mime_type["txt"] = "text/plain";
     mime_type["html"] = "text/html";
     mime_type["sql"] = "text/x-sql";
     mime_type["ini"] = "text/x-ini";
     mime_type["c"] = "text/x-csrc";
     mime_type["h"] = "text/x-csrc";
     
     mime_type["py"] = "text/x-python";
     mime_type["lua"] = "text/x-lua";
     mime_type["java"] = "text/x-java";
     mime_type["hpp"] = "text/x-c++src";
     mime_type["cpp"] = "text/x-c++src";
     
     mLogLevel  = { {"ALL",1},{"DEBUG",2},{"WARNING",5},{"SEARCH",4} };

     defLocales = {  {"coma",{"de","fr","es","nl","au","pt"} } } ;
}
basics::~basics() {
    
}

void basics::printlog(string s1,map< string,int> mLevel )
{

     //  cout << "Loglevel = " << mLevel["LogLevel"]  << " -- DebugLevel = " <<  DebugLevel << " mit Wert " <<  mLogLevel[ DebugLevel ] << endl;
     
     if( mLevel["LogLevel"] <= mLogLevel[ DebugLevel ]){
          cout << s1 << endl ;
     }
}

void basics::printlog(string s1, string s2,map< string,int> mLevel )
{
      if( mLevel["LogLevel"] <= mLogLevel[ DebugLevel ]){
           cout << s1 << ": " << s2 << endl ;
      }
}

void basics::printlog(string s1, string s2, int i1,map< string,int> mLevel )
{
      if( mLevel["LogLevel"] <= mLogLevel[ DebugLevel ]){
           cout << s1 << ": " << s2 << ": " << i1 << endl ;
      }
}

void basics::printlog(string s1, int i1,map< string,int> mLevel )
{
     //cout << "Loglevel = " << mLevel["LogLevel"]  << " -- DebugLevel = " <<  DebugLevel << " mit Wert " <<  mLogLevel[ DebugLevel ] << endl;
      if( mLevel["LogLevel"] <= mLogLevel[ DebugLevel ]){
           cout << s1 << ": " << i1 << endl ;
      }
}
void basics::printlog(string s1, int i1, int i2,map< string,int> mLevel )
{
      if( mLevel["LogLevel"] <= mLogLevel[ DebugLevel ]){
           cout << s1 << ": " << i1 << ", " << i2 <<endl ;
      }
     
}
void basics::printlog(string s1, int i1, string s2, int i2,map< string,int> mLevel )
{
      if( mLevel["LogLevel"] <= mLogLevel[ DebugLevel ]){
           cout << s1 << ": " << i1 << s2 << i2 << endl ;
      }
}
void basics::printlog(string s1, float f1,map< string,int> mLevel )
{
      if( mLevel["LogLevel"] <= mLogLevel[ DebugLevel ]){
           cout << s1 << ": " << f1 << endl ;
      }
}

vector <string> basics::map2vector(map <string,string> mMap){

  vector < string > vMap ;

     for (auto i : mMap){
          vMap.push_back( i.first) ;
          vMap.push_back( i.second) ;

     }

     return vMap;

}

bool basics::file_exists (string name)
{
     if (FILE *file = fopen(name.c_str(), "r")) {
          fclose(file);
          return true;
     }
     else {
          return false;
     }
}

string basics::stripchars(std::string str, const std::string &chars)
{
    str.erase(
        std::remove_if(str.begin(), str.end(), [&](char c){
            return chars.find(c) != std::string::npos;
        }),
        str.end()
    );
    return str;
}


void basics::replaceAll( string &s, const string &search, const string &replace ) {
    for( size_t pos = 0; ; pos += replace.length() ) {
        // Locate the substring to replace
        pos = s.find( search, pos );
        if( pos == string::npos ) break;
        // Replace by erasing and inserting
        s.erase( pos, search.length() );
        s.insert( pos, replace );
    }
}

// trim from start (in place)
void basics::ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
            std::not1(std::ptr_fun<int, int>(std::isspace))));
}

// trim from end (in place)
void basics::rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
            std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

// trim from both ends (in place)
void basics::trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

string basics::getNullUUID(){
     return "00000000-0000-0000-0000-000000000000" ;
}
string basics::getNewUUID(){
     int i;
     char* c;		
     string sUUID  ;


     srand(static_cast<unsigned int> (time(NULL)));  /*Randomize based on time.*/
     
     
     
     /*Data1 - 8 characters.*/

     for(i = 0; i < 8; i++){
          
          
          int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
          
          
          
          
     }
     /*Data2 - 4 characters.*/
	
     sUUID += '-' ;
     
     for(i = 0; i < 4; i++){
          
		  int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
     }
     
     /*Data3 - 4 characters.*/
     
     sUUID += '-' ;    
     for(i = 0; i < 4; i++){
          //	((*pGuidStr = (rand() % 16)) < 10) ? *pGuidStr += 48 : *pGuidStr += 55;
          int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
     }
     
     /*Data4 - 4 characters.*/
     
     sUUID += '-' ;
     for(i = 0; i < 4; i++){
          //	((*pGuidStr = (rand() % 16)) < 10) ? *pGuidStr += 48 : *pGuidStr += 55;
          int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
     }
     
     /*Data5 - 12 characters.*/
     
     sUUID += '-' ;   
     for(i = 0; i < 12; i++){
          
          int iZ =  (rand()%16);
          if (iZ < 10){
               iZ += 48 ;
          }
          else{
               iZ += 55;
          }
          
          
          sUUID += (char)iZ;           
     }
     
     
     
     
     
     cout <<  sUUID << endl ;
     
     return sUUID ;
     
}
string basics::getRandomFilename(string sPrefix){
     // cout << getNewUUID() << sPrefix << endl ;
     string dirname = (string) getenv("HOME") + "/cuondata/";
     string sFilename = dirname + getNewUUID() + sPrefix ;
     cout << sFilename << endl;
     return sFilename ;
     

}

void basics::writeFile(string sName, string* sData) {

     
     printlog("length of sData = " , toString(sData->size()) );
     ofstream myfile (sName,ofstream::binary);
     if (myfile.is_open())
     {
          cout << "myfile is open " << endl ;
          myfile << *sData ;
           cout << "myfile is open2 " << endl ;
           myfile.close();
            cout << "myfile is open3 " << endl ;
     }
     else cout << "Unable to open file";
     
}


string* basics::readFile(string sName) {

     
     string* sData = new string();
     
     ifstream myfile (sName,ios::ate | ios::binary);
     if (myfile.is_open())
     {
          ifstream::pos_type pos = myfile.tellg();

          std::vector<char>  result(pos);

          myfile.seekg(0, ios::beg);
          myfile.read(&result[0], pos);
          
          
          sData = new string(result.begin(),result.end() ) ;
          cout <<"length of file : "<< pos << endl;
          myfile.close();

          
         
     }
     else cout << "Unable to open file";
    
     
     return sData;
}


string basics::getDate(){

     string sDateTime = getDateTime();
     printlog (sDateTime );
     string sDate = sDateTime.substr(0,10);
     printlog (sDate );
     

     return sDate;

}


string basics::getDateTime(){
     
 printlog ("getDateTime" );

  time_t rawtime;
  struct tm * timeinfo;
  char buffer[80];

  time (&rawtime);
  timeinfo = localtime(&rawtime);
   printlog ("getDateTime2" );

 
  string sFormat = oUser.getLocaleDateFormat();
  printlog ("getDateTime3 = ", sFormat );

  
  strftime(buffer,80,sFormat.c_str(),timeinfo);
  std::string str(buffer);

  printlog( str) ;

  return str;

}

string basics::getDate(tm* _date){
     char buffer[80];
     string sFormat = oUser.getLocaleDateFormat();
     printlog ("getDateTime3 = ", sFormat );

  
     strftime(buffer,80,sFormat.c_str(),_date);
     std::string str(buffer);
     
     printlog( str) ;
     
     return str;

}
int basics::executePrg(vector <string> prg)
{
     string sPrg ;
     string sPrg2 ;
     

     pid_t pid = vfork();
     if(pid == -1) {
          perror("vfork");
          return 1;
     } else if(pid == 0) {
          
          size_t lV = prg.size();
          cout << "size of the Vector: " << lV << endl ;
          switch (lV) {
          case 1:
               sPrg = prg[0];
               sPrg = "nohup " + sPrg + " & ";
               cout << "sPrg = " << sPrg << endl;
               if( execlp("nohup",sPrg.c_str(),sPrg.c_str(), NULL) == -1) {
                    perror("execlp");
                    return 1;

               }
               break;
          case 2:
               sPrg = prg[0];
               sPrg2 = prg[1];
              
               //cout << "sPrg 2 = " << sPrg << ", " << sPrg2 << endl;
               if(execlp("nohup", sPrg.c_str(),sPrg.c_str(),sPrg2.c_str(), NULL) == -1) {
                    perror("execlp");
                    return 1;

               }
               break;
          }
          return 1;

     }

   
}

int basics::String2Int(string sInt){
     
     int value = atoi(sInt.c_str());
     
     return value ;
}
string basics::Int2String(int i){

     return to_string(i);

}
map <string,string> basics::getActualDateTime(){
     map<string,string> dicTime ;
     dicTime["date"] = (getDate() );


     return dicTime ;
}

string basics::getLocalizedString(float value){

     



}

string basics::getLocalizedString(int value){

     



}

/*
 def getCheckedValue(self, value, type, min = None, max = None,iRound = -1):
        retValue = None
        try:
            assert type
            if type == 'int':
                if isinstance(value, types.IntType) or isinstance(value, types.LongType) :
                
                
                    try:
                        retValue = int(value)
                    except:
                        retValue = 0
                    

                else:
                    try:
                        assert value != None
                        if isinstance(value, types.StringType):
                            value = value.strip()
                            if value[0] == 'L'  or value[0] == 'l':
                                value = value[1:]
                            
                        retValue = int(value)
                    except:
                        retValue = 0
                
                        
            elif type == 'float':
                if not isinstance(value, types.FloatType):
                    try:
                        assert value != None

                        if isinstance(value, types.StringType):
                            
                            value = value.strip()
                            convert = False
                            #print 'convert userlocales = ', self.dicUser['Locales']
                            for sLocale in self.decimalLocale['coma']:
                                #print sLocale
                                if sLocale == self.dicUser['Locales']:
                                    convert = True
                            #print 'convert = ',  convert,  value
                            if convert:
                                #print 'convert to normal float'
                                #value = value.replace('.','')
                                value = value.replace(',','.')
                                
                                print 'convert2 = ',  convert,  value   
                            if value[0] == 'L'  or value[0] == 'l':
                                value = value[1:]
                            #print "value0 = ", value,  value[:-1]
                            while not value[-1].isdigit() :
                                if not value:
                                    break
                                try:
                                    #print "value = ", value,  value[:-1]
                                    value= value[:-1]
                                    
                                except:
                                    break
                                    
                        retValue = float(value)
                    except Exception, params:
                        #print Exception, params
                        retValue = 0.0
                else:
                    retValue =  value 
            elif type == 'toStringFloat':
                if isinstance(value, types.StringType):
                    if value == 'NONE':
                        value = '0.00'
                    elif value == 'None':
                        value = '0.00'
                    
                        
                    convert = False
                    print 'convert userlocales = ', self.dicUser['Locales']
                    for sLocale in self.decimalLocale['coma']:
                        #print sLocale
                        if sLocale == self.dicUser['Locales']:
                            convert = True
                    if convert:
                        #print 'convert to normal float'
                        value = value.replace('.',',')
                        #value = value.replace(',','.')
                    retValue = value 

            elif type == 'toLocaleString':
                retValue = value
                if iRound >= 0:
                    value = round(value,iRound)
                try:
                    if isinstance(value, types.FloatType):
                        convert = False
                        for sLocale in self.decimalLocale['coma']:
                            #print sLocale
                            if sLocale == self.dicUser['Locales']:
                                convert = True
                        print 'convert = ',  convert
                        print 'float = ',  value
                        value = "%f" % value
                        if convert:
                            value = value.replace('.',',') 
                        else:
                            value = value
                        print 'after convert = ',  value
                        retValue = value
                except:
                    retValue = '0'
                
            elif type == 'date':
                #print 'value by date', value
                print 'date1',  value
                retvalue = time.strptime(value, self.dicUser['DateformatString'])
                print 'dt2 = ', retvalue
                
                        
                    #elif entry.getVerifyType() == 'date' and isinstance(sValue, types.StringType):
                    #    dt = datetime.datetimeFrom(sValue)
                    #dt = datetime.strptime(sValue, "YYYY-MM-DD HH:MM:SS.ss")
                    #dt = datetime.datetime(1999)
                    #    # self.out( dt)
                    #    sValue = dt.strftime(self.sDateFormat)
                    
            elif type == 'formatedDate':
                print 'value by formatedDate', value
                checkvalue = []
                retValue = ''
                try:
                    checkvalue = time.strptime(value, self.dicUser['DateformatString'])
                    self.printOut( 'dtFormated2 = ', checkvalue)
                except:
                    retValue = ''
                    checkvalue = []
                if checkvalue and checkvalue[0] == 1900 and checkvalue[1] == 1 and checkvalue[2] == 1:
                    # 1900/1/1 --> set to empty
                    retvalue = ''
                elif checkvalue:
                    retValue = value
                    
                
                
                    
            elif type == 'toStringDate':
                print 'value by toStringDate', value
                retValue = time.strftime(self.dicUser['DateformatString'],value)
                self.printOut( 'dt5 = ', retValue) 
                
            elif type == 'string':    
                #print 'check string = ', value
                
                if not isinstance(value, types.StringType):
                    value = `value`
                if value == 'NONE':
                    value = ''
                elif value == 'None':
                    value = ''
                
                retValue = value
                
                
                
            else:
                retValue = value
        
        except AssertionError:
            print 'No type set '
            retValue = value
        except Exception,params:
            print Exception, params
            retValue = value
        
        #print 'retvalue = ', retValue
        
        return retValue

*/

vector <string> basics::split(const  string &theString,
       const  string  &theDelimiter)
{
     size_t  start = 0, end = 0;
     vector <string> vReturn ;
    while ( end != string::npos)
    {
        end = theString.find( theDelimiter, start);

        // If at end, use length=maxLength.  Else use length=end-start.
        vReturn.push_back( theString.substr( start,
                       (end == string::npos) ? string::npos : end - start));

        // If at end, use start=maxSize.  Else use start=end+delimiter.
        start = (   ( end > (string::npos - theDelimiter.size()) )
                  ?  string::npos  :  end + theDelimiter.size());
    }

    return vReturn ;
}

void basics::deleteCache(){
     cout << "Deleting cache files " << endl ;
         // These are data types defined in the "dirent" header
    struct dirent *next_file;
    DIR *theFolder;
    DIR *theFolder2;
    char filepath[256];
    char filepath2[256];
    

    theFolder = opendir( ( (string) getenv("HOME") + "/.cuoncacheKeys").c_str() );
    if (theFolder){
         while ( next_file = readdir(theFolder) )
         {
              // build the full path for each file in the folder
              sprintf(filepath, "%s/%s",( (string) getenv("HOME") + "/.cuoncacheKeys").c_str() , next_file->d_name);
              
              remove(filepath);
         }
    }
    
    // printlog("Deleted cuoncacheKeys, now delete cuoncache");
    theFolder2 = opendir( ( (string) getenv("HOME") + "/.cuoncache").c_str() );
    if (theFolder2){
         while ( next_file = readdir(theFolder2) )
         {
              // build the full path for each file in the folder
              sprintf(filepath2, "%s/%s",( (string) getenv("HOME") + "/.cuoncache").c_str() , next_file->d_name);
              
              remove(filepath2);
         }
    }
}

string basics::upper(string cmd){
     for_each(cmd.begin(), cmd.end(), [](char& in){ in = ::toupper(in); });
     return cmd ;
}
