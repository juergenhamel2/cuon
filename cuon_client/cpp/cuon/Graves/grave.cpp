/*Copyright (C) [2016]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>

#include <cuon/Graves/grave.hpp>
#include <cuon/Graves/SingleGrave.hpp>

#include <Pluma/Connector.hpp>




PLUMA_CONNECTOR



  bool connect(pluma::Host& host){
     // add a  provider to host
     host.add( new graveProvider() );
     return true;
 }



grave::grave()
{

}
int grave::initAll() {

     printlog("start grave Mainwindow");
     tree1Name = "tree1";

     ModulNumber = MN["Grave"] ;
     
     v_new.push_back( "new1");
     menuItems["new"] = v_new;

     v_save.push_back( "save1");
     menuItems["save"] = v_save;

     v_delete.push_back("clear1");
     menuItems["delete"] = v_delete ;

     v_end.push_back("quit1");
     menuItems["quit"] = v_end ;



     init("graves","GravesMainwindow","graves");

     // init db
     singleGrave = new SingleGrave();


     singleGrave->setWindow(window) ;
     singleGrave->setTree1();
     cout << "start Grave tab changed" << endl ;
     tabChanged();
     return 1;
}

void grave::on_new_activate()
{
     printlog("grave new 0","") ;
     switch (tabOption) {

     case tabgrave_Grave:

          singleGrave->newData();
          break;
     }
}
void grave::on_save_activate()
{
     printlog ("grave save 0" );
     switch (tabOption) {
     case tabgrave_Grave:
          singleGrave->save();
          break;
     }
}

void grave::on_delete_activate() {}
void grave::on_bSearch_clicked() {}

void grave::tabChanged()
{


     switch(tabOption) {

     case tabgrave_Grave:
         
          if(oldTab < 1) {
               singleGrave->getListEntries();
          }
     }
     oldTab = tabOption;
}




grave::~grave() {}
