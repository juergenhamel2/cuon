#ifndef GRAVE_HPP_INCLUDED
#define GRAVE_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Graves/SingleGrave.hpp>

#define tabgrave_Grave 0
#define tabgrave_Invoice 1
#define tabgrave_Service 2
#define tabgrave_Spring 3
#define tabgrave_Summer 4
#define tabgrave_Autumn 5
#define tabgrave_winter 6
#define tabgrave_Hollidays 7
#define tabgrave_Annual 8
#define tabgrave_Unique 9





#include <Pluma/Pluma.hpp>
#include "cuon_window.hpp"


class grave:public windows,public cuon_window{
    public:

    SingleGrave *singleGrave ;

        grave();
        ~grave();

     virtual void on_new_activate() ;
     virtual void on_save_activate();
     virtual void on_delete_activate() ;
     virtual int  initAll();
     void on_bSearch_clicked();
     void tabChanged();


    protected:

    private:

};


PLUMA_INHERIT_PROVIDER(grave, cuon_window);


#endif // GRAVE_HPP_INCLUDED
