/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

using namespace std;

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include "cuon/Editor/editor_view.hpp"
#include <gtksourceviewmm.h>


editor_view::editor_view(){
 Gsv::init();
}

editor_view::~editor_view(){

}

Gtk::ScrolledWindow* editor_view::getEditor(string sLM){

    
     /* Create a Scrolled Window that will contain the GtkSourceView */
     Gtk::ScrolledWindow* pScrollWin = new Gtk::ScrolledWindow ();
     pScrollWin->set_policy (Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
     //GtkSourceView* sv = new GtkSourceView();
     Gsv::View* sv = getView(sLM);
     //Gtk::VBox* vb = new Gtk::VBox();
     //vb->pack_start(*sv);
     pScrollWin->add(*sv);

     return pScrollWin ; 
}

Gsv::View* editor_view::getView(string sLM){
    
     Gsv::View* sv = new Gsv::View();

     auto lm = Gsv::LanguageManager::get_default ();
     
     //Gsv::SourceLanguage *python = Gsv::SourceLanguageManager().get_language(manager, "python");


     
  
    
     auto sourcebuffer = sv->get_source_buffer();
     auto lang = lm->get_language(sLM);
     
     sourcebuffer->set_language(lang);
     
     sourcebuffer->set_highlight_matching_brackets(true);
     sourcebuffer->set_highlight_syntax(true);
     

     sv->set_source_buffer(sourcebuffer);
     sv->set_show_line_numbers(true);
     return sv;

}
