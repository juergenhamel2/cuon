#ifndef EDITOR_VIEW_HPP_INCLUDED
#define EDITOR_VIEW_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>
#include <gtksourceviewmm.h>



class editor_view{

public:
     editor_view();
     ~editor_view();
     Gtk::ScrolledWindow* getEditor(string sLM);
     Gsv::View* getView(string sLM);
};






#endif // EDITOR_VIEW_HPP_INCLUDED


