
bool WiringPi_is_on = false ;

#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include <wiringPi.h> // Include WiringPi library!
#include <wiringPiI2C.h>// Include WiringPi I2C library!
#include <global.hpp>
#include "string.h"
#include "SinglePi.hpp"
#include "cuon/PI/SinglePi.hpp"
#include "pi_control.hpp"
#include "Steppermotor.hpp"
#include "lcd2x16.hpp"



#define MAXTIMINGS	85
#define DHTPIN		1  // Wiring 1 = PI12
int dht11_dat[5] = { 0, 0, 0, 0, 0 };


// install: git clone git://git.drogon.net/wiringPi and do ./build there









using namespace std;



/* Pin number declarations. We're using the Broadcom chip pin numbers.

const int pwmPin = 18; // PWM LED - Broadcom pin 18, P1 pin 12
const int ledPin = 23; // Regular LED - Broadcom pin 23, P1 pin 16
const int butPin = 17; // Active-low button - Broadcom pin 17, P1 pin 11

const int pwmValue = 75; // Use this to set an LED brightness
*/

lcd2x16* lcd;

pi_control::pi_control(){

     cout << "++++++++++++++++++++++++++++++++++++++start Raspberry Pi Window 0" << endl ;

}

int pi_control::initAll(){

     
     int _lcd =0x27 ;
     lcd = new lcd2x16(_lcd);
     SinglePi* singlePi = new SinglePi();
     
////  Setup stuff
//      // Initialize wiringPi -- using Broadcom pin numbers
//      int allOk = wiringPiSetupGpio();
//      if (allOk == -1){
// // SET HERE THE ERROR MESSAGE ROUTINE
//           printlog("Error at wiringPiGpioSetup = ",allOk);
//      }
     if (! WiringPi_is_on){
          int  allOk = wiringPiSetup();
          if (allOk == -1){
// SET HERE THE ERROR MESSAGE ROUTINE
               singlePi->printlog("Error at wiringPiSetup = ",allOk);
          }
          else{
               WiringPi_is_on = true;
          }
     }
     
     

     init("pi","PiMainwindow","pi");



     // Buttons
      window->get_widget("bTemp1Action", pButton);
      pButton->signal_clicked().connect(sigc::mem_fun(this, &pi_control::on_bTemp1Action_clicked));
      
      window->get_widget("bStartMotor1", pButton);
      pButton->signal_clicked().connect(sigc::mem_fun(this, &pi_control::on_bStartMotor1_clicked));
      


//      pinMode(pwmPin, PWM_OUTPUT); // Set PWM LED as PWM output
//      pinMode(ledPin, OUTPUT);     // Set regular LED as output
//      pinMode(butPin, INPUT);      // Set button as INPUT
//      pullUpDnControl(butPin, PUD_UP); // Enable pull-up resistor on button

//      printf("Blinker is running! Press CTRL+C to quit.\n");
//      int fd  = wiringPiI2CSetup(_lcd );
//      cout << "Init result: "<< fd << endl;


//      lcd->init();
     
// lcd->write(0, 0, "Greetings!");

//      // Loop (while(1)):
//      while(1)
//      {
      
//           pwmWrite(pwmPin, 1024 - pwmValue); // PWM LED at dim setting
//           // Do some blinking on the ledPin:
//           digitalWrite(ledPin, HIGH); // Turn LED ON
//           cout << "LED is ON" << endl ;

//           delay(1000); // Wait 1500ms
//           digitalWrite(ledPin, LOW); // Turn LED OFF
//           cout << "LED is OFF" << endl ;
//           delay(1000); // Wait 1500ms again
      
//      }

     return 0;
}

void pi_control::on_bTemp1Action_clicked(){

 read_dht11_dat();




}




void pi_control::read_dht11_dat(){
     uint8_t laststate	= HIGH;
     uint8_t counter		= 0;
     uint8_t j		= 0, i;
     float	f; 
 
     dht11_dat[0] = dht11_dat[1] = dht11_dat[2] = dht11_dat[3] = dht11_dat[4] = 0;
 
     pinMode( DHTPIN, OUTPUT );
     digitalWrite( DHTPIN, LOW );
     delay( 18 );
     digitalWrite( DHTPIN, HIGH );
     delayMicroseconds( 40 );
     pinMode( DHTPIN, INPUT );
 
     for ( i = 0; i < MAXTIMINGS; i++ )
     {
          counter = 0;
          while ( digitalRead( DHTPIN ) == laststate )
          {
               counter++;
               delayMicroseconds( 1 );
               if ( counter == 255 )
               {
                    break;
               }
          }
          laststate = digitalRead( DHTPIN );
 
          if ( counter == 255 )
               break;
 
          if ( (i >= 4) && (i % 2 == 0) )
          {
               dht11_dat[j / 8] <<= 1;
               if ( counter > 16 )
                    dht11_dat[j / 8] |= 1;
               j++;
          }
     }

     cout << " j = " << j << endl ;
     if ( (j >= 40) &&
          (dht11_dat[4] == ( (dht11_dat[0] + dht11_dat[1] + dht11_dat[2] + dht11_dat[3]) & 0xFF) ) )
     {
          f = dht11_dat[2] * 9. / 5. + 32;
          printf( "Humidity = %d.%d %% Temperature = %d.%d C (%.1f F)\n",
                  dht11_dat[0], dht11_dat[1], dht11_dat[2], dht11_dat[3], f );
     }
     else  {
          printf( "Data not good, skip\n" );
     }
}
 

void pi_control::on_bStartMotor1_clicked(){


     Steppermotor Motor1(2,3,4,5,1);
     Motor1.performDemo();
     

}



pi_control::~pi_control(){

}
