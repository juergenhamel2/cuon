#include <stdio.h>
#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdlib.h>
#include <sstream>
#include <fstream>
#include "string.h"
#include "lcd2x16.hpp"

// install: git clone git://git.drogon.net/wiringPi and do ./build there
#include <wiringPi.h> // Include WiringPi library!

#include <wiringPiI2C.h>// Include WiringPi I2C library!

using namespace std;

int BLEN = 1;
int _lcd =0x27 ;
int LCD ;
lcd2x16::lcd2x16(int fd){
     LCD = wiringPiI2CSetup(_lcd );

}


lcd2x16::~lcd2x16(){

}




void lcd2x16::write_word(int data){
     int temp = data;
     if ( BLEN == 1 )
          temp |= 0x08;
     else
          temp &= 0xF7;
     wiringPiI2CWrite(LCD, temp);
}

void lcd2x16::send_command(int comm){
     int buf;
// Send bit7-4 firstly
     buf = comm & 0xF0;
     buf |= 0x04; // RS = 0, RW = 0, EN = 1
     write_word(buf);
     delay(2);
     buf &= 0xFB; // Make EN = 0
     write_word(buf);

// Send bit3-0 secondly
     buf = (comm & 0x0F) << 4;
     buf |= 0x04; // RS = 0, RW = 0, EN = 1
     write_word(buf);
     delay(2);
     buf &= 0xFB; // Make EN = 0
     write_word(buf);
}

void lcd2x16::send_data(int data){
     int buf;
// Send bit7-4 firstly
     buf = data & 0xF0;
     buf |= 0x05; // RS = 1, RW = 0, EN = 1
     write_word(buf);
     delay(2);
     buf &= 0xFB; // Make EN = 0
     write_word(buf);

// Send bit3-0 secondly
     buf = (data & 0x0F) << 4;
     buf |= 0x05; // RS = 1, RW = 0, EN = 1
     write_word(buf);
     delay(2);
     buf &= 0xFB; // Make EN = 0
     write_word(buf);
}

void lcd2x16::init(){
     send_command(0x33); // Must initialize to 8-line mode at first
     delay(5);
     send_command(0x32); // Then initialize to 4-line mode
     delay(5);
     send_command(0x28); // 2 Lines & 5*7 dots
     delay(5);
     send_command(0x0C); // Enable display without cursor
     delay(5);
     send_command(0x01); // Clear Screen
     wiringPiI2CWrite(LCD, 0x08);
}

void lcd2x16::clear(){
     send_command(0x01); //clear Screen
}

void lcd2x16::write(int x, int y, string sData){
     int addr, i;
     int tmp;
     if (x < 0) x = 0;
     if (x > 15) x = 15;
     if (y < 0) y = 0;
     if (y > 1) y = 1;

     // Move cursor
     addr = 0x80 + 0x40 * y + x;
     send_command(addr);
     int iC = sData.length();
     const char *data = sData.c_str();
     tmp = strlen(data);
     for (i = 0; i < tmp; i++){
          send_data(data[i]);
     }
}
