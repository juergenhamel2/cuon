#ifndef PI_CONTROL_HPP_INCLUDED
#define PI_CONTROL_HPP_INCLUDED


#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <global.hpp>

#include "cuon_window.hpp"

#include "cuon/Windows/windows.hpp"

class pi_control:public windows, public cuon_window {

public:
     pi_control();
     ~pi_control();
     
     void on_bTemp1Action_clicked();
     void on_bStartMotor1_clicked();
     void read_dht11_dat();

      virtual  int initAll();
};


#endif // PI_CONTROL_HPP_INCLUDED
