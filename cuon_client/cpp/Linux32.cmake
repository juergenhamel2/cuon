message("test mit linux 32")      
set(L_PATH "/usr")

set(L_PATH_LIB "/usr/lib/i386-linux-gnu")
set(L_PATH_LIB_S1 "/usr/lib/debug/lib/i386-linux-gnu")
set(L_PATH_X1 "/usr/local")

set (CUON_OUTPUTDIR "${CMAKE_BINARY_DIR}/bin32" )
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CUON_OUTPUTDIR})
set (CMAKE_CXX_FLAGS "-m32 -std=c++0x")
#set (GCRYPT_LIBRARIES ${L_PATH_LIB}/libcrypto.so.1.0.0 )
set (LIBGCRYPT_LIBS ${L_PATH}/lib/libgcrypt.so )

#set for Precise pangolin
set (GCRYPT_LIBRARIES ${L_PATH_LIB}/libcrypt.so )

#set (XMLRPCC_LIBRARIES ${L_PATH}/lib/libxmlrpc_client++.so ${L_PATH}/lib/libxmlrpc.so ${L_PATH}/lib/libxmlrpc++.so )
find_package (BZip2)



find_package(PkgConfig)
PKG_CHECK_MODULES(GTK, gtk+-3.0 REQUIRED)
pkg_check_modules(GTKMM  gtkmm-3.0 REQUIRED )
#pkg_check_modules(GTKSOURCEVIEWMM gtksourceviewmm REQUIRED )
set (GTKSOURCEVIEWMM_INCLUDE /usr/include/gtksourceviewmm-3.0 /usr/lib/gtksourceviewmm-3.0/include /usr/include/gtksourceview-3.0/ )



include(FindXMLRPCC.cmake)


#FIND_PACKAGE(XMLRPC REQUIRED c++)
message( "Find xmlrpc libs " ${XMLRPC_LIBRARIES} )
message( "and this xmlrpc includes "  ${XMLRPC_INCLUDE_DIRS} )
message( "this is gtksourceviev: " ${GTKSOURCEVIEWMM_LIBRARIES} )
message( "this is gcrypt: " ${LIBGCRYPT_LIBS} )



set (GCRYPT_LIBRARY_DIRS ${LIBGCRYPT_LIBS} )
set (GCRYPT_LIBRARIES ${LIBGCRYPT_LIBS} )

set (GCRYPT_INCLUDE_DIRS ${L_PATH}/include )
