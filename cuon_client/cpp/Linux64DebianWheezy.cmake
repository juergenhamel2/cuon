



set(L_PATH "/usr")
set(L_PATH_LIB "/usr/lib")
set (CUON_OUTPUTDIR "${CMAKE_BINARY_DIR}/bin64" )
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CUON_OUTPUTDIR})
set (CMAKE_CXX_FLAGS "-g -m64 -std=c++0x " )
set (GCRYPT_LIBRARIES ${L_PATH_LIB}/libgcrypt.so )
find_package (BZip2)	   


find_package(PkgConfig)
PKG_CHECK_MODULES(GTK, gtk+-3.0 REQUIRED)
pkg_check_modules(GTKMM  gtkmm-3.0 REQUIRED )
#pkg_check_modules(GTKSOURCEVIEWMM gtksourceviewmm REQUIRED )
set (GTKSOURCEVIEWMM_INCLUDE /usr/include/gtksourceviewmm-3.0 /usr/lib/gtksourceviewmm-3.0/include /usr/include/gtksourceview-3.0/ )



include(FindXMLRPCC.cmake)


#FIND_PACKAGE(XMLRPC REQUIRED c++)
message( "Find xmlrpc libs " ${XMLRPC_LIBRARIES} )
message( "and this xmlrpc includes "  ${XMLRPC_INCLUDE_DIRS} )
message( "this is gtksourceviev: " ${GTKSOURCEVIEWMM_LIBRARIES} )
message( "this is gcrypt: " ${LIBGCRYPT_LIBS} )



set (GCRYPT_LIBRARY_DIRS ${LIBGCRYPT_LIBS} )
set (GCRYPT_LIBRARIES ${LIBGCRYPT_LIBS} )

set (GCRYPT_INCLUDE_DIRS ${L_PATH}/include )

SET (SPECIAL_INCLUDES /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/gtkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/gdkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/sigc++-2.0/include/ /usr/include/sigc++-2.0/sigc++/  /usr/lib/x86_64-linux-gnu/glib-2.0/include/ /usr/lib/x86_64-linux-gnu/glibmm-2.4/include/  /usr/include/gtksourceviewmm-3.0 /usr/include/gtksourceview-3.0/)

set (SPECIAL_LIBS /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/libgtksourceview-3.0.so /usr/lib/libgtksourceviewmm-3.0.so )