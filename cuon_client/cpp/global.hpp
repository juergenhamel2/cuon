
#ifndef GLOBAL_HPP // header guards
#define GLOBAL_HPP
#include <time.h>
#include <map>
#include <cuon/User/User.hpp>

#define MY_LINUX64 1
#define MY_LINUX32 301
#define MY_WIN32 1001
#define MY_ARM 2001


#include <stdio.h>  /* defines FILENAME_MAX */

#ifdef WINDOWS
    #include <direct.h>
    #define GetCurrentDir _getcwd
#else
    #include <unistd.h>
    #define GetCurrentDir getcwd
 #endif




// extern tells the compiler this variable is declared elsewhere
// used as global varibale

extern  std::string DebugLevel ;
extern  char cCurrentPath[FILENAME_MAX];
extern User oUser ;
extern map<string,string> cuonSystem ;
extern int ipc_port1 ;
extern bool gtk_is_running ;
extern bool WiringPi_is_on ;

#endif
