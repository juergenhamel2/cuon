#ifndef CUONCLIENT_HPP
#define CUONCLIENT_HPP

using namespace std;
#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>
#include <cuon/Windows/windows.hpp>
#include <cuon_window.hpp>
#include <os_info.hpp>




class MainWindow : public windows {
public:

      
     
     struct version {
          int Major ;
          int Minor ;
          int Rev ;
          int Spec ;
     };

    
     version Version;
     //Gtk::Window* pWin;
     Gtk::Dialog* pDlg;
     map <string,int> plIndex ;
     cuon_window* aw ;

     cuon_window* st ; // Pulugin Stock
     cuon_window* bo ; // Plugin Botany
     
     cuon_window* ob ; // Pugin Orderbook
     cuon_window* en ; // Pugin Enquiry
     cuon_window* pro ; // Pugin Proposal

     gint t1 ;
     int time_contact ;
     int time_schedul ;
     int time_imap_dms ;
     #ifdef PLUMA
         std::vector<cuon_windowProvider*> providers;
     #endif
     
     vector <string> myPluginsName ;

     
     MainWindow();
     ~MainWindow();
     void start(int argc, char* argv[]);
     virtual bool on_delete_event(GdkEventAny *event);
     //File
     void on_end_activate();
     void on_login_activate();
     //Data
     void on_addresses1_activate();
     void on_articles1_activate();
     void on_mi_staff1_activate();
     
     void on_botany1_activate();
     //work
     void on_dms1_activate();
     void on_stock1_activate();

     void on_enquiry_activate();
     void on_proposal1_activate();
     void on_order1_activate();
     void on_hibernation_activate();
     void on_graves_activate();
     
     
     //misc
     void on_project1_activate();
     void on_mi_sourcenavigator_activate();
     // Tools
     void on_import_data1_activate();
     void on_delete_cache_activate();
     void on_PI_Control_activate();
     
     
     void on_eUserName_changed();
     void checkMenus();


     void startTiming();
     bool checkContact();
     bool checkImap();
     bool checkSchedul();
     
     void startXmlRpcServer();
};


#endif
