
#ifndef CUON_WINDOW_HPP
#define CUON_WINDOW_HPP


#include <Pluma/Pluma.hpp>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include <os_info.hpp>

class cuon_window{
public:
     
     cuon_window();
     ~cuon_window();
     

     virtual int initAll();
     virtual int initOrder( map <string,string>* dicOrder=NULL, bool newOrder = false, int orderid = 0,string OrderType="Order");
     virtual int initHibernation( int iaddressid = 0,  bool  inewHibernation=false,  int ihibernation_id = 0);
     

};

#ifdef PLUMA
PLUMA_PROVIDER_HEADER(cuon_window);
#endif

#endif
