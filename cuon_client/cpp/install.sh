#/bin/bash                   
sourcePath=`pwd`
echo $sourcePath

printf  "select the OS *********  \n 1)  Debian Devuan/Jessie 64 bit AMD  STATC \n  10)  Debian Wheezy 64 bit AMD  no static \n  11)  Debian Stretch 64 bit AMD  no static \n  12)  Debian Devuan/Jessie 64 bit AMD  no static \n  13)  Debian Devuan/Jessie 64 bit AMD static \n  20)  Raspbian Wheezy ARM  no static \n  21)  Gentoo  ARM  32bit  no static \n  60) Gentoo Linux 64 bit AMD no static\n  80) Gentoo Linux 64 bit AMD static  \n  99 Quit\n\n"

read n
case $n in


    1) PLATFORM=MY_LINUX64
	OS_NAME="Debian"
	OS_SPEC="Devuan/Jessie"
	DO_STATIC="static"
	DO_CPU="amd"

	;;

    
    10) PLATFORM=MY_LINUX64
	OS_NAME="Debian"
	OS_SPEC="Wheezy"
	DO_STATIC="no_static"
	DO_CPU="amd"

	;;
    

    11) PLATFORM=MY_LINUX64
	OS_NAME="Debian"
	OS_SPEC="Stretch"
	DO_STATIC="no_static"
	DO_CPU="amd"

	;;
    



    12) PLATFORM=MY_LINUX64
	OS_NAME="Debian"
	OS_SPEC="Devuan/Jessie"
	DO_STATIC="no_static"
	DO_CPU="amd"

	;;

     13) PLATFORM=MY_LINUX64
	OS_NAME="Debian"
	OS_SPEC="Devuan/Jessie"
	DO_STATIC="static"
	DO_CPU="amd"

	;;

    



    20) PLATFORM=MY_ARM
	OS_NAME="Raspbian"
	OS_SPEC="Wheezy"
	DO_STATIC="no_static"
	DO_CPU="arm"

	;;
    21) PLATFORM=MY_ARM
	OS_NAME="Gentoo"
	OS_SPEC="xfce"
	DO_STATIC="no_static"
	DO_CPU="arm"

	;;

    
    60) PLATFORM=MY_LINUX64
	OS_NAME="Gentoo"
	OS_SPEC="all"
	DO_STATIC="no_static"
	DO_CPU="amd"
	;;
    80) PLATFORM=MY_LINUX64
	OS_NAME="Gentoo"
	OS_SPEC="all"
	DO_STATIC="static"
	DO_CPU="amd"
	;;
    
    99) exit ;;
	     
esac



    

echo "Platform      static      OS-Name     OS-Specific"
echo "$Platform     $2          $OS_NAME    $OS_SPEC"


# first criate 3'rd Level party
#
# leveldb (bsd licence
#
#

cd leveldb
make
cd ..

#
#now create the cuonClient program
#


#rm -Rf bin
#mkdir bin

printf "#ifndef OS_INFO_HPP\n#define OS_INFO_HPP\n\n " > os_info.hpp  
if [ $PLATFORM = "MY_WIN32" ] ; then 
    printf  "#define OS_INFO_PLATFORM  1\n#define OS_INFO_CPU 32\n#define PLUMA " >> os_info.hpp   

   
fi
if [ $PLATFORM = "MY_WIN32_MinGW" ] ; then 
    printf  "#define OS_INFO_PLATFORM  1\n#define OS_INFO_CPU 32\n#define PLUMA " >> os_info.hpp   

   
fi
 if [ $PLATFORM = "MY_LINUX32" ] ; then 
    printf  "#define OS_INFO_PLATFORM  2\n#define OS_INFO_CPU 32\n#define PLUMA " >> os_info.hpp   

   

fi 
 if [ $PLATFORM = "MY_LINUX64" ] ; then 
    printf  "#define OS_INFO_PLATFORM  2\n#define OS_INFO_CPU 64\n#define PLUMA " >> os_info.hpp   
    if [ $DO_STATIC =  "static" ] ; then 
	DBS=-DBUILD_STATIC_EXECUTABLES=1
    else
	DBS=-DBUILD_STATIC_EXECUTABLES=0
    fi

   

 fi 
 if [ $PLATFORM = "MY_ARM" ] ; then 
     printf  "#define OS_INFO_PLATFORM  2\n#define OS_INFO_CPU 32\n" >> os_info.hpp
     printf  "#define NOPLUMA 1" >> os_info.hpp
 fi

 
 

 cmake -DOS_NAME=$OS_NAME -DOS_SPEC=$OS_SPEC -DBUILD_SHARED=OFF $DBS -DCROSS_SYSTEM=$PLATFORM -DCMAKE_BUILD_TYPE="Debug" -G "Unix Makefiles"  $sourcePath


 
printf  "\n\n#define OS_WINDOWS  1\n#define OS_LINUX  2\n" >> os_info.hpp


printf  "\n\n#endif" >> os_info.hpp

#make clean
# make options
 if [ $PLATFORM = "MY_ARM" ] ; then 
     make -j2
 else
     make -j7
 fi


 # do something after compile
 
if [ $PLATFORM = "MY_LINUX32" ] ; then 

    cd bin32
    chmod a+x cuonClient
    cd ..
fi
if [ $PLATFORM = "MY_LINUX64" ] ; then 
    cd bin64
    chmod a+x cuonClient
    cd ..
fi
if [ $PLATFORM = "MY_ARM" ] ; then 
    cd arm
    chmod a+x cuonClient
    cd ..
fi

   
cpack -C CPackConfig.cmake
