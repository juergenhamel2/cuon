   
  #set(ALL_INCLUDE_DIR /usr/lib/gcc/i686-pc-mingw32/4.8.2/include)
  #set(ALL_INCLUDE_DIR2 /usr/lib/gcc/i686-pc-mingw32/4.8.2/include/g++-v4)
  #set(ALL_INCLUDE_DIR3 /usr/include )
  message("test mit win 32 ")
  SET(CMAKE_SYSTEM_NAME Windows)
  set(L_PATH "/usr")
  set(L_PATH_LIB "/usr/lib/i386-linux-gnu")
  set(L_PATH_LIB_S1 "/usr/lib/debug/lib/i386-linux-gnu")
  set(L_PATH_X1 "/usr/local")

  set (BZIP_INCLUDE_DIRS "" )

  set  (BZIP2_LIBRARIES "")
  
  #set (GCRYPT_LIBRARIES ${L_PATH_LIB}/libcrypto.so.1.0.0 )
  #set (GCRYPT_LIBRARIES ${L_PATH}/lib/libgcrypt.so )

  #set for Win32
  set (GCRYPT_LIBRARIES ${L_PATH_LIB}/libcrypt.so )

  set (CUON_OUTPUTDIR "${CMAKE_BINARY_DIR}/win32" )
  set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CUON_OUTPUTDIR})

  #set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
  #set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
  #set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

  set (CMAKE_CXX_FLAGS " -I ${ALL_INCLUDE_DIR} -I ${ALL_INCLUDE_DIR3} -static -static-libgcc -static-libstdc++ -m32 -std=c++0x -lpthreadGC2 ")
  message (${CMAKE_CXX_FLAGS})

  SET(CMAKE_CXX_COMPILER i686-w64-mingw32-g++)
  SET(CMAKE_RC_COMPILER i686-w64-mingw32-windres)
  # Debian  SET(CMAKE_CXX_COMPILER i586-mingw32msvc-c++)
  


