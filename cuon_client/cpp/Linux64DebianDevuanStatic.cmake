



set(L_PATH "/usr")
set(L_PATH_LIB "/usr/lib")
set (CUON_OUTPUTDIR "${CMAKE_BINARY_DIR}/bin64" )
set (CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CUON_OUTPUTDIR})
#set (CMAKE_CXX_FLAGS "-Wall -g -m64 -std=c++11 " )
set (CMAKE_CXX_FLAGS " -g -m64 -std=c++11 " )
#add_definitions (-D_GLIBCXX_USE_CXX11_ABI=0)
#set_target_properties(cuonClient PROPERTIES LINK_SEARCH_START_STATIC 1)
#set_target_properties(cuonClient PROPERTIES LINK_SEARCH_END_STATIC 1)
set(CMAKE_FIND_LIBRARY_SUFFIXES ".a")
#Static Libs
set (BUILD_SHARED_LIBS 0)
# add imported target
   add_library(imp_pthread STATIC IMPORTED)
   # point the imported target at the real file
   set_property(TARGET imp_pthread PROPERTY
     IMPORTED_LOCATION /usr/lib/x86_64-linux-gnu/libpthread.a)

   
set (GCRYPT_LIBRARIES /usr/lib/x86_64-linux-gnu/libgcrypt.a)


#Set Linker flags
set(CMAKE_EXE_LINKER_FLAGS "-static-libgcc -static-libstdc++ -static")


find_package (BZip2)	   
set (BZIP2_LIBRARIES /usr/lib/x86_64-linux-gnu/libbz2.a )

set(GTKMM_LIBRARIES 
/usr/lib/x86_64-linux-gnu/libgtkmm-3.0.so
/usr/lib/x86_64-linux-gnu/libatkmm-1.6.so
/usr/lib/x86_64-linux-gnu/libgdkmm-3.0.so
/usr/lib/x86_64-linux-gnu/libgiomm-2.4.so
/usr/lib/x86_64-linux-gnu/libpangomm-1.4.so
/usr/lib/x86_64-linux-gnu/libgtk-3.so
/usr/lib/x86_64-linux-gnu/libglibmm-2.4.so
/usr/lib/libcairomm-1.0.so
/usr/lib/x86_64-linux-gnu/libgdk-3.so
/usr/lib/x86_64-linux-gnu/libatk-1.0.so
/usr/lib/x86_64-linux-gnu/libgio-2.0.so
/usr/lib/x86_64-linux-gnu/libpangocairo-1.0.so
/usr/lib/x86_64-linux-gnu/libgdk_pixbuf-2.0.so
/usr/lib/x86_64-linux-gnu/libcairo-gobject.so
/usr/lib/x86_64-linux-gnu/libpango-1.0.so
/usr/lib/x86_64-linux-gnu/libcairo.a
/usr/lib/x86_64-linux-gnu/libsigc-2.0.a
/usr/lib/x86_64-linux-gnu/libgobject-2.0.so
/usr/lib/x86_64-linux-gnu/libglib-2.0.so )

#find_package(PkgConfig)
#PKG_CHECK_MODULES(GTK, gtk+-3.0 REQUIRED)
#pkg_check_modules(GTKMM  gtkmm-3.0 REQUIRED )

#pkg_check_modules(GTKSOURCEVIEWMM gtksourceviewmm REQUIRED )
set (GTKSOURCEVIEWMM_INCLUDE /usr/include/gtksourceviewmm-3.0 /usr/lib/gtksourceviewmm-3.0/include /usr/include/gtksourceview-3.0/ )



#set (LevelDB  ${CMAKE_HOME_DIRECTORY}/leveldb/db/builder.cc          ${CMAKE_HOME_DIRECTORY}/leveldb/db/log_reader.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/c.cc                ${CMAKE_HOME_DIRECTORY}/leveldb/db/log_test.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/corruption_test.cc  ${CMAKE_HOME_DIRECTORY}/leveldb/db/log_writer.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/c_test.c            ${CMAKE_HOME_DIRECTORY}/leveldb/db/memtable.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/db_bench.cc         ${CMAKE_HOME_DIRECTORY}/leveldb/db/repair.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/dbformat.cc         ${CMAKE_HOME_DIRECTORY}/leveldb/db/skiplist_test.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/dbformat_test.cc    ${CMAKE_HOME_DIRECTORY}/leveldb/db/table_cache.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/db_impl.cc          ${CMAKE_HOME_DIRECTORY}/leveldb/db/version_edit.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/db_iter.cc          ${CMAKE_HOME_DIRECTORY}/leveldb/db/version_edit_test.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/db_test.cc          ${CMAKE_HOME_DIRECTORY}/leveldb/db/version_set.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/filename.cc         ${CMAKE_HOME_DIRECTORY}/leveldb/db/version_set_test.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/filename_test.cc    ${CMAKE_HOME_DIRECTORY}/leveldb/db/write_batch.cc
#${CMAKE_HOME_DIRECTORY}/leveldb/db/leveldb_main.cc     ${CMAKE_HOME_DIRECTORY}/leveldb/db/write_batch_test.cc
 #)


#add_library (leveldb SHARED IMPORTED ) 
# set (localLib  ${CMAKE_HOME_DIRECTORY}/leveldb/libleveldb.so)
# set_property(TARGET leveldb  PROPERTY IMPORTED_LOCATION ${locallib} )
#set (localInc  ${CMAKE_HOME_DIRECTORY}/leveldb/include/levldb/ ${CMAKE_HOME_DIRECTORY}/leveldb  ${CMAKE_HOME_DIRECTORY}/Pluma/include/ )


#include(FindXMLRPCC.cmake)
find_package(XMLRPC REQUIRED c++)

#PKG_CHECK_MODULES(XMLRPC libxmlrpc-c  REQUIRED )

# STRING(REGEX REPLACE "/usr/lib/libxmlrpc.so" "/usr/lib64/libxmlrpc.so" ${XMLRPC_LIBRARIES})
if(EXISTS "/usr/lib/x86_64-linux-gnu/libxmlrpc_abyss.a" )


  set ( XMLRPCPATH /usr/lib/x86_64-linux-gnu )
  
else()
  
set ( XMLRPCPATH /usr/lib )

endif()

set (XMLRPC_LIBRARIES ${XMLRPCPATH}/libxmlrpc_abyss.a            ${XMLRPCPATH}/libxmlrpc_server_pstream++.a
 ${XMLRPCPATH}/libxmlrpc_client.a           ${XMLRPCPATH}/libxmlrpc_server.a
 ${XMLRPCPATH}/libxmlrpc_client++.a         ${XMLRPCPATH}/libxmlrpc_server++.a
 ${XMLRPCPATH}/libxmlrpc_cpp.a              ${XMLRPCPATH}/libxmlrpc.a
 ${XMLRPCPATH}/libxmlrpc_packetsocket.a     ${XMLRPCPATH}/libxmlrpc++.a
 ${XMLRPCPATH}/libxmlrpc_server_abyss.a     ${XMLRPCPATH}/libxmlrpc_util.a
 ${XMLRPCPATH}/libxmlrpc_server_abyss++.a   ${XMLRPCPATH}/libxmlrpc_xmlparse.a
 ${XMLRPCPATH}/libxmlrpc_server_cgi.a       ${XMLRPCPATH}/libxmlrpc_xmltok.a
${XMLRPCPATH}/libxmlrpc_server_cgi++.a

)


if(EXISTS   /usr/lib/libtcmalloc.a )
  set (LBTC   /usr/lib/libtcmalloc.a )
else()
  set (LBTC     /usr/lib/libtcmalloc_minimal.a.4 )
endif()


set (LIBGCRYPT_LIBS ${GCRYPT_LIBRARIES})

message( "Find xmlrpc libs " ${XMLRPC_LIBRARIES} )
message( "and this xmlrpc includes "  ${XMLRPC_INCLUDE_DIRS} )
message( "this is gtksourceviev: " ${GTKSOURCEVIEWMM_LIBRARIES} )
message( "this is gcrypt: " ${LIBGCRYPT_LIBS} )
message( "this is gtkmm: " ${GTKMM_LIBRARIES} )

#set (GTKMM_LIBRARIES  )
set (GCRYPT_LIBRARY_DIRS ${LIBGCRYPT_LIBS} )
#set (GCRYPT_LIBRARIES ${LIBGCRYPT_LIBS} )

set (GCRYPT_INCLUDE_DIRS ${L_PATH}/include )

SET (SPECIAL_INCLUDES /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-linux-gnu/gtkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/gdkmm-3.0/include/ /usr/lib/x86_64-linux-gnu/sigc++-2.0/include/ /usr/include/sigc++-2.0/sigc++/  /usr/lib/x86_64-linux-gnu/glib-2.0/include/ /usr/lib/x86_64-linux-gnu/glibmm-2.4/include/  /usr/include/gtksourceviewmm-3.0 /usr/include/gtksourceview-3.0/ /usr/lib/x86_64-linux-gnu/glibmm-2.4/include/ /usr/lib/x86_64-linux-gnu/giomm-2.4/include/ /usr/lib/x86_64-linux-gnu/pangomm-1.4/include/ /usr/lib/x86_64-linux-gnu/gtksourceviewmm-3.0/include/  )

set (SPECIAL_LIBS  /usr/lib/x86_64-linux-gnu/libc.a /usr/lib/x86_64-linux-gnu/libglib-2.0.a /usr/lib/x86_64-linux-gnu/libdl.a   /usr/lib/gcc/x86_64-linux-gnu/5/libstdc++.a  /usr/lib/x86_64-linux-gnu/libsigc-2.0.a  ${LBTC}   imp_pthread  /usr/lib/x86_64-linux-gnu/libgpg-error.a    /usr/lib/x86_64-linux-gnu/libm.a /usr/lib/x86_64-linux-gnu/libgtksourceview-3.0.a  /usr/lib/libgtksourceviewmm-3.0.a )