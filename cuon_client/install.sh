#/bin/sh
sourcePath=`pwd`
echo $sourcePath

#rm -Rf bin
#mkdir bin

cmake -DCROSS_SYSTEM="LINUX 64" -DCMAKE_BUILD_TYPE="Debug" -G "Unix Makefiles"  $sourcePath

make clean
make -j7


cpack -C CPackConfig.cmake
