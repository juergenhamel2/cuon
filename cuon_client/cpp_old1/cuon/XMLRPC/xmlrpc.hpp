#ifndef XMLRPC_HPP
#define XMLRPC_HPP

using namespace std;
#include <xmlrpc-c/girerr.hpp>
#include <xmlrpc-c/base.hpp>
#include <xmlrpc-c/client_simple.hpp>
#include <iostream>
#include <string>
#include <map>

#include <cuon/Databases/datatypes.hpp>
#include <global.hpp>

class mXmlRpc: public datatypes {
public:

     xmlrpc_c::clientSimple myClient;

     string serverUrl;
     std::vector<xmlrpc_c::value> a1;
     std::vector <void*> v1 ;

     mXmlRpc();

     xmlrpc_c::value callrp(string rp,xmlrpc_c::paramList const& paramList);

};

class myXmlRpc : public mXmlRpc {
    private:
    xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, map <string,string> m1 );
    xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, int i1 );
    xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, string s1 );
    xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, double d1 );
     xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, bool v1 );
      xmlrpc_c::paramList _add(xmlrpc_c::paramList dataServ, vector <string> v1 );



public:
     myXmlRpc();

     xmlrpc_c::paramList addUser( xmlrpc_c::paramList dataServ);


     xmlrpc_c::paramList add(map <string,string> m1 ,bool setUserDic = true);

     xmlrpc_c::paramList add(map <string,string> m1, map <string,string> m2,bool setUserDic= true );
     xmlrpc_c::paramList add(int i1,bool setUserDic = true);
     xmlrpc_c::paramList add(string s1,bool setUserDic = true);
     xmlrpc_c::paramList add(string s1, int i1,bool setUserDic = true);
     xmlrpc_c::paramList add(string s1, string s2,bool setUserDic = true);
     xmlrpc_c::paramList add(vector <string> v1,bool setUserDic = true);

     xmlrpc_c::paramList add(listEntry e1,bool setUserDic = true);


     void callRP(string rp,xmlrpc_c::paramList const& paramList,vector <Record> &rs1);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,Record &r1);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,string &s1);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,int &i1);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,map <string,string> &m);
     void callRP(string rp,xmlrpc_c::paramList const& paramList,vector<map <string,string> > &v);

} ;




#endif
