#ifndef BASICS_HPP_INCLUDED
#define BASICS_HPP_INCLUDED
#include <iostream>
#include <sstream>
class basics {
public:
     basics();
     ~basics();


};

//
//// string
//char string[] = "938";
//
//// string -> int
//int i = fromString<int>(string);
//
//// string -> double
//double d = fromString<double>(string);
//
//// int -> string
//std::string str1 = toString<int>(i);
//
//// double -> string
//std::string str2 = toString<double>(d);

template<class T> std::string toString(const T& t)
{
     std::ostringstream stream;
     stream << t;
     return stream.str();
}

template<class T> T fromString(const std::string& s)
{
     std::istringstream stream (s);
     T t;
     stream >> t;
     return t;
}

#endif // BASICS_HPP_INCLUDED
