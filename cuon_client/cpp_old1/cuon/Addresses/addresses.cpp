/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

#include <cuon/Addresses/addresses.hpp>

#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Addresses/SingleAddress.hpp>

addresses::addresses()
{
     //myXmlRpc x1 = myXmlRpc();
     tree1Name = "tv_address";
     menuItems["quit"] = "mi_quit1";
     init("address","AddressMainwindow","addresses");


     singleAddress.glx.window = window ;
 singleAddress.setTree1(pTree1);
     //singleAddress.loadEntryField("addresses");
     singleAddress.setDicEntries(oEntries);




     singlePartner.setTree1(pTree1);
     singlePartner.glx.window = window ;



     tabChanged();
}

addresses::~addresses()
{

}
void addresses::tabChanged()
{

     cout << "tab changed at address" << tabOption << " = " << tabAddress << endl;
     switch(tabOption) {

     case tabAddress:
          singleAddress.getListEntries();
          break;
     case tabPartner:
          //singlePartner.getListEntries();
          break;

     }

}



