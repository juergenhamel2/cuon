#ifndef SINGLEADDRESS_HPP_INCLUDED
    #define SINGLEADDRESS_HPP_INCLUDED

    #include <cuon/Databases/Singledata.hpp>

    class SingleAddress:public Singledata{
        public:
            SingleAddress();
            ~SingleAddress();
    };

#endif // SINGLEADDRESS_HPP_INCLUDED
