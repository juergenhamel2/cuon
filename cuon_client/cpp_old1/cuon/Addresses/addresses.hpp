#ifndef ADDRESSES_HPP_INCLUDED
#define ADDRESSES_HPP_INCLUDED
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/windows.hpp>
#include <cuon/Addresses/SingleAddress.hpp>
#include <cuon/Addresses/SinglePartner.hpp>

#define tabAddress 0
#define tabBank 1
#define tabMisc 2
#define tabPartner 3


#define tabSchedul 4
#define  tabNotes 5
#define tabProposal 6
#define tabOrder 7
#define tabInvoice 8
#define tabProject 9
#define tabHtml 10

#define tabGraves 11
#define tabHibernation 12


class addresses:public windows {

public:



     SingleAddress singleAddress ;
     SinglePartner singlePartner ;
     addresses();
     ~addresses();


     void tabChanged();


};



#endif // ADDRESSES_HPP_INCLUDED
