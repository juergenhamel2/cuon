/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

#include <cuon/Addresses/SinglePartner.hpp>

SinglePartner::SinglePartner(){
    sNameOfTable = "partner";
    sSort = "lastname,firstname,city";
    sWhere = "";



    newEntry.sNameOfTable = sNameOfTable ;
    newEntry.sWhere = sWhere ;
    newEntry.sSort = sSort ;
    // loadEntryField("partner");
       //names of database fields
    string a1[] = {"lastname","lastname2","firstname","street","zip","city","id"} ;

    //tree header
    string h1[] = {"Lastname","Lastname2","Firstname","Street","Zip","City","ID"};

    // type
    string t1[] = {"string","string","string","string","string","string","int"};



    // names of tree colums
    string c1[] = {"name","name2","firstname","street","zip","city","id"} ;
    setLiFields(7,a1,h1,t1, c1);

//    cols.addColumn("name","string");
//    cols.addColumn("name2","string");

}
SinglePartner::~SinglePartner(){
}
