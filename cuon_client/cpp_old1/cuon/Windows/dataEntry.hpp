#ifndef DATEENTRY_HPP_INCLUDED
#define DATEENTRY_HPP_INCLUDED

#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <global.hpp>


class dataEntry
{
    public:
        string nameOfEntry ;
        string typeOfEntry;
        bool bDuty ;
        int Round;
        string nextWidget ;
        string verifyType ;
        string SqlField ;
        dataEntry();
        ~dataEntry();
        // set/get routines
        void set_nameOfEntry(string _nameOfEntry);
        string get_nameOfEntry();

        void set_typeOfEntry(string _typeOfEntry);
        string get_typeOfEntry();

 void set_verifyTypeOfEntry(string _verifyTypeOfEntry);
        string get_verifyTypeOfEntry();

        void set_nameOfSql(string _nameOfSql);
        string get_nameOfSql();

        void set_Round(int _iRound);
        int get_Round();



};



#endif // DATEENTRY_HPP_INCLUDED
