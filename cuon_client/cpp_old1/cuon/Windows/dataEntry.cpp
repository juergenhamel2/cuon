/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <global.hpp>


#include <cuon/Windows/dataEntry.hpp>


dataEntry::dataEntry(){
    nameOfEntry="EMPTY";
    typeOfEntry="EMPTY";
    bDuty = false;
    Round = 0;
    nextWidget = "EMPTY";
    verifyType  = "EMPTY";
    SqlField = "EMPTY" ;
}
dataEntry::~dataEntry(){
}
// set the nameOfEntry
void dataEntry::set_nameOfEntry(string _nameOfEntry){
	nameOfEntry = _nameOfEntry ;

}
// get the nameOfEntry
string dataEntry::get_nameOfEntry(){
	return  nameOfEntry ;

}

// set the typeOfEntry
void dataEntry::set_typeOfEntry(string _typeOfEntry){
	typeOfEntry = _typeOfEntry ;

}
// get the typeOfEntry
string dataEntry::get_typeOfEntry(){
	return  typeOfEntry ;

}
// set the typeOfEntry
void dataEntry::set_verifyTypeOfEntry(string _verifyTypeOfEntry){
	verifyType = _verifyTypeOfEntry ;

}

// get the typeOfEntry
string dataEntry::get_verifyTypeOfEntry(){
	return  verifyType ;

}



// set the nameOfEntry
void dataEntry::set_nameOfSql(string _nameOfSql){
	SqlField = _nameOfSql ;

}
// get the nameOfEntry
string dataEntry::get_nameOfSql(){
	return  SqlField ;

}


// set the Round
void dataEntry::set_Round(int _iRound){
	Round = _iRound ;

}
// get the Round
int dataEntry::get_Round(){
	return  Round ;

}
