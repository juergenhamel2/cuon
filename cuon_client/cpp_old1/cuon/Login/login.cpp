#include <map>
#include <list>

/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/
#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>
#include <gcrypt.h>
#include <cuon/Login/login.hpp>
#include <cuon/Encoder/base16.hpp>
#include <global.hpp>

login::login()
{

}
login::~login()
{
}

void login::initLogin()
{
     cout << "start login" << endl;
     gLogin = loadGlade("login","");
     gLogin->get_widget("UserID_Dialog",pDlg);

     gLogin->get_widget("TUserID", pUserID);
     pUserID->signal_key_press_event().connect(sigc::mem_fun(this, &login::on_TUserID_key_press_event));

     gLogin->get_widget("TPassword", pPassword);
     pPassword->signal_key_press_event().connect(sigc::mem_fun(this, &login::on_TPassword_key_press_event));

     gLogin->get_widget("okbutton1", pOK);
     pOK->signal_clicked().connect(sigc::mem_fun(this, &login::on_okbutton1_clicked));

     gLogin->get_widget("cancelbutton1", pCancel);
     pCancel->signal_clicked().connect(sigc::mem_fun(this, &login::on_cancelbutton1_clicked));

     pDlg->run();


}


bool login::on_TUserID_key_press_event(GdkEventKey* event)
{
    return true;
}
bool login::on_TPassword_key_press_event(GdkEventKey* event)
{
 return true;
}


void login::on_okbutton1_clicked()
{
    cout << "okbutton" << endl;
     pDlg->hide();
}
void login::on_cancelbutton1_clicked()
{

}


string login::sendLoginData(string Name, string Password)
{
    string userUUID = "TEST" ;
//     x1.callRP("Database.test_s2",x1.add(oUser.sUser,oUser.sUser),userUUID);
//
//     cout << "UserUUID 0 = " << userUUID << endl;



     cout << "Login" << Name << " " << endl ;
     cout << "Passwd " << encodeSha512(Password) << endl ;

     x1.callRP("Database.createSessionID",x1.add(Name,Password,false),userUUID);

     cout << "UserUUID = " << userUUID << endl;
     oUser.sUser["Name"] = Name ;
     oUser.sUser["SessionID"] = userUUID ;


     oUser.refreshSqlUser();

     return userUUID ;
}
string login::encodeSha512(string Password)
{
     gcry_md_hd_t key512 ;
     string code ;
     char* cPassword = const_cast<char*>(Password.c_str());
     //gcry_control( GCRYCTL_DISABLE_SECMEM_WARN );
     //gcry_control( GCRYCTL_INIT_SECMEM, 16384, 0 );
     gcry_error_t err = gcry_md_open(&key512,GCRY_MD_SHA512,0);
     cout << "err " << err << endl ;
     gcry_md_write(key512, cPassword, Password.length());
     unsigned char* result = gcry_md_read(key512, GCRY_MD_SHA512);
     //cout << "Passwd-result " << result << endl ;
     int lenResult = sizeof(result);

     code.assign((char*) &result[0], sizeof(result));
     string resultHex ;
     //cout << "Passwd-code0 " << code.begin() << code.end() << endl ;
     stlencoders::base16<char>::encode(code.begin(), code.end(),  std::back_inserter(resultHex) );

     cout << "Passwd-code " << resultHex << endl ;
     gcry_md_close(key512);



     return resultHex;
}
