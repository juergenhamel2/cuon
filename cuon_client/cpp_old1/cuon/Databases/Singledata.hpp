#ifndef SINGLEDATA_HPP
#define SINGLEDATA_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>
#include <cuon/Databases/datatypes.hpp>
#include <cuon/Windows/setOfEntries.hpp>
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/dataEntry.hpp>
#include <cuon/Windows/gladeXml.hpp>

class Singledata : public datatypes {

public:
     string sNameOfTable ;
     string sSort;
     string sWhere;
     myXmlRpc x1;
     Record firstRecord ;
     int ID;

     map<string,vector <string> > dicFields ;
     map <string,string> dicEntries ;
     vector <string> liHeader ;
     GtkCellRenderer     *renderer;
     gladeXml glx ;
     GtkWidget           *view;
     Gtk::TreeView *pTree1;
     setOfEntries oEntries;
     map<string, dataEntry> dataEntries ;
     listEntry newEntry;
     dataEntry _entry;
     Glib::RefPtr<Gtk::ListStore> listStore;
     Glib::RefPtr<Gtk::TreeView::Selection> selection ;

     Gtk::TreeModel::Row row ;
     Columns cols ;

    vector<string> aFields ;

     Singledata();
     ~Singledata();


    void loadEntryField(string sName);
    void load(int id);
     void setDicEntries(setOfEntries _entries);
     void getListEntries();
     void setLiFields(int i, string sNames[],string sHeader[], string sType[], string sCols[]);
     void setTree1(Gtk::TreeView *_pTree1);
     void setListEntries();
     void setEntries(int i);
     void on_selection_changed();


};

#endif

