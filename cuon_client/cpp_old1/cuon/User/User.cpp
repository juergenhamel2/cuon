/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

#include <map>
#include <list>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>

#include <cuon/User/User.hpp>

User::User()
{

}

User::~User()
{
}
void User::init()
{


     sUser["Name"] = "EMPTY" ;
     sUser["SessionID"] = "0" ;
     sUser["Database"] = "cuon" ;
     sUser["userLocales"] = "de" ;
     sUser["userPdfEncoding"] = "latin-1" ;
     sUser["userEncoding"] = "utf-8" ;

     iUser["userShowNews"] = 1 ;
     iUser["Encode"] = 1 ;
     iUser["client"] = 0;



     // call the refresh
     refreshSqlUser();


}
void User::update()
{

}
void User::refreshSqlUser()
{
     sSqlUser["Name"] =  sUser["Name"] ;
     sSqlUser["SessionID"] = sUser["SessionID"] ;
     sSqlUser["Database"] = sUser["Database"] ;
     sSqlUser["userLocales"] =  sUser["userLocales"] ;
     sSqlUser["userPdfEncoding"] = sUser["userPdfEncoding"];
     sSqlUser["userEncoding"] = sUser["userEncoding"] ;


     sSqlUser["Encode"] = toString<int>(iUser["Encode"] );
     sSqlUser["client"] = toString<int>( iUser["client"] );


     cout << "User sql client = " + sSqlUser["client"] << endl ;
}

void User::clearServerData(){
    sServerData.clear();
}
