#include <gtkmm.h>
#include <iostream>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cuonClient.hpp>
#include <global.hpp>

#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/User/User.hpp>
#include <cuon/Windows/gladeXml.hpp>

#include <cuon/Login/login.hpp>
#include <cuon/TypeDefs/iniReader.hpp>

#include <cuon/Addresses/addresses.hpp>
using namespace std;


User oUser ;

map <string,string> cuonSystem;

MainWindow::MainWindow(){
    Version.Major = 12; // Year
    Version.Minor = 11 ; // Month
    Version.Rev = 3 ; // Day
    Version.Spec = 0; // Special of a day

}

MainWindow::~MainWindow(){
}
void MainWindow::start(int argc, char* argv[])
{

     Gtk::Main gtkmain(argc, argv);
     int myInt = 0;
     x1.callRP("Database.is_running",0, myInt);
     cout << "test in main myInt = " << myInt << endl ;
     menuItems["quit"] = "end1";
     cout << "test in main 0  "  << endl ;
    loadGlade("cuon","window1");
    setTitle("cuonClient V " + toString(Version.Major) + "." + toString(Version.Minor) + "-" + toString(Version.Rev)) ;

cout << "test in main 1  "  << endl ;


     //window->get_widget("window1",pWin);
     window->get_widget("aCuon", pDlg);


cout << "test in main 2 "  << endl ;




     if (cuonSystem["User"] != "EMPTY" && cuonSystem["Password"] != "TEST" ) {

          oUser.sUser["Name"] = cuonSystem["User"] ;
          login l1 = login();
          oUser.sUser["sessionID"] = l1.sendLoginData(cuonSystem["User"],cuonSystem["Password"]);

     }


     cout << "Main3" << endl;
     //gtk_builder_connect_signals (Main, Main);

     //Main->get_widget("end1", pEnd);
     //pEnd->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_end_activate));

     window->get_widget("login1", pLogin);
     pLogin->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_login_activate));
 ///Connect window quit signal

     window->get_widget("mi_addresses1", pAddresses);
     pAddresses->signal_activate().connect(sigc::mem_fun(this, &MainWindow::on_addresses1_activate));


     Gtk::Main::run(*pWin);



}

void MainWindow::on_end_activate()
{
        cout << "cuon end called" << endl ;
     Gtk::Main::quit();
}


void MainWindow::on_login_activate()
{
     login l1 ;
     l1.initLogin();

}

void MainWindow::on_addresses1_activate(){
    addresses *ar1 = new addresses();

}

int main (int argc, char* argv[])
{
     char *cvalue = NULL;
     int index;
     int c;
     cuonSystem["User"] = "EMPTY";
     oUser.sUser["client_id"] = "c++" ;
     opterr = 0;
     oUser.init();
     cout << "argc = " << argc << endl ;
     if (argc == 1) {
          iniReader inR;
          inR.parseIniFile("cuon.ini");
          cuonSystem["User"] = inR.getOptionToString("username");
          cout << "cuonSystem[User] " << cuonSystem["User"]  << endl;
          cuonSystem["server"] = inR.getOptionToString("host");
          cuonSystem["port"] = inR.getOptionToString("port");
          cuonSystem["protocol"] = inR.getOptionToString("protocol");
          cuonSystem["Password"] = inR.getOptionToString("password");
          oUser.iUser["client"] = inR.getOptionToInt("client_id");
          cout << "cuonSystem[server] " << cuonSystem["server"]  << endl;
          cout << "cuonSystem[port] " << cuonSystem["port"]  << endl;
          cout << "cuonSystem[protocol] " << cuonSystem["protocol"]  << endl;
          inR.cleanupIniReader();
     } else {


          while ((c = getopt (argc, argv, "s:p:c:U:P:C:")) != -1) {
               switch (c) {
               case 's':
                    cout << string(optarg) << endl ;
                    cuonSystem["server"] = string(optarg);
                    break;
               case 'p':
                    cuonSystem["port"] = string(optarg);
                    break;
               case 'c':
                    cuonSystem["protocol"] = string(optarg);
                    break;
               case 'U':
                    cuonSystem["User"] = string(optarg);
                    break;
               case 'P':
                    cuonSystem["Password"] = string(optarg);
                    break;
               case 'C':
                    oUser.iUser["client"] = atoi(optarg);
                    break;
               default:
                    cout << "No Parameter" << endl;
                    //normaly nothing

               }
          }
     }
     cout << "Data = " << endl;
     //cout << oUser.iUser["Client"]   << endl;

     MainWindow m;
     m.start(argc,argv);
     return  EXIT_SUCCESS;

}
