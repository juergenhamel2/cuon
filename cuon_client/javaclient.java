import java.util.Vector;
import java.util.Hashtable;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import java.util.*;
import java.net.*;

public class javaclient{

    // The location of our server.
    private final static String server_url =
        "http://xmlrpc-c.sourceforge.net/api/sample.php";

   
    public void startjavaclient(){
   
        XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
        try{
            config.setServerURL(new URL("http://127.0.0.1:7080"));
        } catch (java.net.MalformedURLException e) {
            System.out.println("Caught MalformedURLException\n");
            e.printStackTrace();
        }
        XmlRpcClient client = new XmlRpcClient();
        client.setConfig(config);
        Object[] params = new Object[]{new Integer(33), new Integer(9)};
        try{
            //sid = self.Server.Database.createSessionID( username, sPw)
            Integer result = (Integer) client.execute("Database.testMulti",params);
        } catch ( Exception e) {
            System.out.println("Caught XmlRpcException\n");
            e.printStackTrace();
        }
           
   
    }
     public static void main (String [] args) {
            javaclient jc = new javaclient();
            jc.startjavaclient() ;
    }
    
}
