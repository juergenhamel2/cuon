#ifndef CUONCLIENT_HPP
#define CUONCLIENT_HPP

using namespace std;
#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>
#include <cuon/Windows/windows.hpp>

class MainWindow : public windows {
public:

     //Gtk::Window* pWin;
     Gtk::Dialog* pDlg;

     //File
     Gtk::MenuItem* pLogin;
     Gtk::MenuItem* pLogout;

     //Data
     Gtk::MenuItem* pAddresses;
     Gtk::MenuItem* pArticles;


     MainWindow();
     ~MainWindow();
     void start(int argc, char* argv[]);

     void on_end_activate();
     void on_login_activate();
     void on_addresses1_activate();

};


#endif
