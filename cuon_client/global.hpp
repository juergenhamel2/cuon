 
#ifndef GLOBAL_HPP // header guards
#define GLOBAL_HPP
#include <time.h>
#include <map>
#include <cuon/User/User.hpp>
#include <cuon/Databses/localCache.hpp>


// extern tells the compiler this variable is declared elsewhere
// used as global varibale

extern User oUser ;
extern map<string,string> cuonSystem ;
extern localCache* lCache ;

#endif
