@ECHO OFF

set DRIVE=%~d0
set BASEPATH=%DRIVE%\path_to_python\
set PYTHONPATH=%BASEPATH%

PATH=%PYTHONPATH%;%PATH%;
PATH=%PYTHONPATH%\DLLs;%PATH%;
PATH=%PYTHONPATH%\Lib\site-packages\pythonwin;%PATH%;
PATH=%PYTHONPATH%\Lib\site-packages\pywin32_system32;%PATH%;
PATH=%PYTHONPATH%\Lib\site-packages\win32;%PATH%;
PATH=%BASEPATH%\gtk\bin;%PATH%;
script
%DRIVE%

python.exe %1 %2 %3 %4 %5 %6 %7 %8

Then start your program by running "portable_python.bat your_script.py".

