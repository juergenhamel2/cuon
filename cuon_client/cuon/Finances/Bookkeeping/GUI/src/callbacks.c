#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_delete1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_monthly1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


gboolean
on_eDate_key_press_event               (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_eShortKey_changed                   (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


gboolean
on_eShortKey_focus_out_event           (GtkWidget       *widget,
                                        GdkEventFocus   *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_eCAB1_activate                      (GtkEntry        *entry,
                                        gpointer         user_data)
{

}


void
on_eCAB1_changed                       (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_eDebi1_editing_done                 (GtkCellEditable *celleditable,
                                        gpointer         user_data)
{

}


void
on_eCredit1_editing_done               (GtkCellEditable *celleditable,
                                        gpointer         user_data)
{

}


void
on_eCAB2_activate                      (GtkEntry        *entry,
                                        gpointer         user_data)
{

}


void
on_eCAB2_changed                       (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_eCAB3_activate                      (GtkEntry        *entry,
                                        gpointer         user_data)
{

}


void
on_eCAB3_changed                       (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_eCAB4_activate                      (GtkEntry        *entry,
                                        gpointer         user_data)
{

}


void
on_eCAB4_changed                       (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bArticle_clicked                    (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_journal_monthly1_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_journal_all1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}

