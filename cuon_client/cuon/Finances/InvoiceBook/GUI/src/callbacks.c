#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_new_activate                        (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_edit_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_delete_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_this_month_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_last_month_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_custom_month_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quarter1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_half_year1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_year1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_new1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_edit1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_save1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_delete1_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_print1_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_this_month1_activate      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_last_month1_activate      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_custom_month1_activate    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_quarter1_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_half_year1_activate       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_inpayment_year1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_all_outstanding_accounts1_activate  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_residue_this_month_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_residue_last_month_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_residue_quarter1_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_residue_half_year1_activate         (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_residue_year1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_residue_custom_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_first_reminder1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_second_reminder1_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_last_reminder1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_summons1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_tree1_row_activated                 (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data)
{

}


void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data)
{

}


void
on_eAccountID_changed                  (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bGotoOrder_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_all_reminder1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


gboolean
on_bSearch_clicked                     (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_bDMS_clicked                        (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bLetter_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_terms_of_payment1_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_directdebit1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


gboolean
on_eSearch_key_press_event             (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}

