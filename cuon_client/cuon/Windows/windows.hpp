#ifndef WINDOWS_HPP
#define WINDOWS_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>
#include <cuon/Windows/gladeXml.hpp>
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/setOfEntries.hpp>
class windows : public gladeXml {

public:


     windows();
     ~windows();

     /// @param gladeName Name of the the glade file
     /// @param sMainWindow Name of the top window widget in the glade file
     ///@param EntryName Name of the Entry file
     void init(string gladeName,string MainWindow, string EntryName);



};

#endif

