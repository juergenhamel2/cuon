#ifndef GLADEXML_HPP
#define GLADEXML_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/setOfEntries.hpp>
#include <cuon/Databases/datatypes.hpp>

class gladeXml  {

public:
     setOfEntries oEntries;

     myXmlRpc x1 ;
     Glib::RefPtr<Gtk::Builder> window ;
     string tree1Name;
     map <string,string> menuItems ;

     Gtk::Window *pWin;
     Gtk::Notebook *pNotebook;
     Gtk::Notebook *pNotebook2;
     Gtk::TreeView *pTree1;
     Gtk::MenuItem* pEnd;
     Gtk::MenuItem* pNew;
     Gtk::MenuItem* pEdit;
     Gtk::MenuItem* pSave;
     Gtk::MenuItem* pDelete;

     gladeXml();
     ~gladeXml();
     /// @param gladeName Name of the the glade file
     /// @param sMainWindow Name of the top window widget in the glade file
     /// @param gladePath Path to the Glade file on the Harddisk
     /// @return the correspondending glade object
     Glib::RefPtr<Gtk::Builder> loadGlade(string gladeName, string sMainWindow, string gladePath)  ;
     Glib::RefPtr<Gtk::Builder> loadGlade(string gladeName, string sMainWindow);
     void loadEntries(string EntryName);
     void fetchDefaultGUI(string sMainWindow);
     void setText(dataEntry _entry, string _Value );
 void setText(dataEntry _entry, int _Value );
     virtual void onNotebook1PageSwitch ( Gtk::Widget *page, guint page_num );
     virtual void onNotebook2PageSwitch ( Gtk::Widget *page, guint page_num );
     virtual void on_tree1_row_activated(const Gtk::TreeModel::Path &, Gtk::TreeViewColumn*);
     virtual void on_end_activate();
     virtual void on_edit_activate();
     virtual void on_new_activate();
     virtual void on_save_activate();
     virtual void on_delete_activate();




};

#endif

