/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

#include <map>
#include <list>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <gtkmm.h>

#include <cuon/Windows/gladeXml.hpp>
#include <cuon/XMLRPC/xmlrpc.hpp>
#include <cuon/Windows/setOfEntries.hpp>

gladeXml::gladeXml()
{
     x1 = myXmlRpc();
     tree1Name = "tree1";
     menuItems["quit"] = "mi_quit1";
     menuItems["new"] = "mi_new1";
     menuItems["edit"] = "mi_edit1";
     menuItems["save"] = "mi_save1";
     menuItems["delete"] = "mi_clear1";

}

gladeXml::~gladeXml()
{
}
Glib::RefPtr<Gtk::Builder> gladeXml::loadGlade(string gladeName, string sMainWindow, string gladePath)
{
     window = Gtk::Builder::create_from_file("../" + gladeName + ".xml");
     return window ;

}

Glib::RefPtr<Gtk::Builder> gladeXml::loadGlade(string gladeName, string sMainWindow)
{
     string sGlade;
     x1.callRP("Database.getGlade",x1.add("glade_" + gladeName + ".xml",false),sGlade);

     //cout << sGlade << endl ;
     window = Gtk::Builder::create_from_string(sGlade);


          fetchDefaultGUI(sMainWindow);



     return window ;

}
void gladeXml::loadEntries(string EntryName)
{
     if (EntryName != "NONE") {

          EntryName = "entry_" + EntryName + ".xml" ;

          vector <map <string,string> > Entries ;
          x1.callRP("Database.getEntry",x1.add(EntryName,false),Entries);

          typedef vector < map<string,string> > ::const_iterator iV ;
          for (iV i =Entries.begin(); i != Entries.end(); i++) {

               map <string,string> newMap = *i;
               oEntries.addEntry(newMap);
          }

     }
}
void gladeXml::fetchDefaultGUI(string sMainWindow)
{
     try {


          //cout << "win 0" << endl ;
          window->get_widget(sMainWindow,pWin);
          //        cout << "win 1" << endl ;
         // if(pWin) pWin->show();
          cout << "win 2" << endl ;
     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }
     ///Connect Notebook signals
     try {
          Glib::RefPtr< Glib::Object > o = window->get_object("notebook1");

          cout << "notebook 0" << endl ;
          if (o) {
               cout << "notebook 1" << endl ;
               window->get_widget("notebook1", pNotebook);
               cout << "notebook 2" << endl ;
               if(pNotebook) pNotebook->signal_switch_page().connect(sigc::mem_fun(this, &gladeXml::onNotebook1PageSwitch));
               cout << "notebook 3" << endl ;
          }
     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }
     ///Connect Notebook2 signals
     try {
          Glib::RefPtr< Glib::Object > o = window->get_object("notebook2");
          if(o) {
               window->get_widget("notebook2", pNotebook2);
               if(pNotebook2) pNotebook2->signal_switch_page().connect(sigc::mem_fun(this, &gladeXml::onNotebook2PageSwitch));
          }
     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }


///Connect tree1 signals
     try {
          Glib::RefPtr< Glib::Object > o = window->get_object(tree1Name);
          if(o) {
               window->get_widget(tree1Name, pTree1);
               if(pTree1) pTree1->signal_row_activated().connect(sigc::mem_fun(this, &gladeXml::on_tree1_row_activated));
          }
     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }


  ///Connect window quit signal
     try {
          Glib::RefPtr< Glib::Object > o = window->get_object(menuItems["quit"]);
          if(o) {
            window->get_widget(menuItems["quit"], pEnd);
            pEnd->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_end_activate));
          }

     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }
     ///Connect  new signal
     try {
          Glib::RefPtr< Glib::Object > o = window->get_object(menuItems["new"]);
          if(o) {
            window->get_widget(menuItems["new"], pNew);
            pNew->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_new_activate));
          }

     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }
     ///Connect edit signal
     try {
          Glib::RefPtr< Glib::Object > o = window->get_object(menuItems["edit"]);
          if(o) {
            window->get_widget(menuItems["edit"], pEdit);
            pEdit->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_edit_activate));
          }

     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }
     ///Connect save signal
     try {
          Glib::RefPtr< Glib::Object > o = window->get_object(menuItems["save"]);
          if(o) {
            window->get_widget(menuItems["save"], pSave);
            pSave->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_save_activate));
          }

     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }
     ///Connect delete signal
     try {
          Glib::RefPtr< Glib::Object > o = window->get_object(menuItems["delete"]);
          if(o) {
            window->get_widget(menuItems["delete"], pDelete);
            pDelete->signal_activate().connect(sigc::mem_fun(this, &gladeXml::on_delete_activate));
          }

     } catch(exception const& e) {

          cerr << "Client threw error: " << e.what() << endl;
     }



}
void gladeXml::setText(dataEntry _entry, string _Value ){

    if(_entry.get_verifyTypeOfEntry() == "string"){

        Gtk::Entry *e1 ;
        window->get_widget(_entry.nameOfEntry, e1);
        if (_Value == "NONE"){
            _Value = "";
        }
        e1->set_text(_Value) ;
    }

}

void gladeXml::setText(dataEntry _entry, int _Value ){



        Gtk::Entry *e1 ;
        window->get_widget(_entry.nameOfEntry, e1);
        e1->set_text(toString(_Value) );


}

void gladeXml::onNotebook1PageSwitch ( Gtk::Widget *page, guint page_num )
{
     cout << "Page = " << page_num << endl;

}
void gladeXml::onNotebook2PageSwitch ( Gtk::Widget *page, guint page_num )
{
     cout << "Page = " << page_num << endl;

}

void gladeXml::on_tree1_row_activated(const Gtk::TreeModel::Path &, Gtk::TreeViewColumn*)
{

}


void gladeXml::on_end_activate()
{
    cout << "glade end called" << endl ;

     if(pWin) pWin->hide();


}

void gladeXml::on_edit_activate()
{
    cout << "glade edit called" << endl ;


}

void gladeXml::on_new_activate()
{
    cout << "glade new called" << endl ;


}

void gladeXml::on_save_activate()
{
    cout << "glade save called" << endl ;



}

void gladeXml::on_delete_activate()
{
    cout << "glade delete called" << endl ;


}
