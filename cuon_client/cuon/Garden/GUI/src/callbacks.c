#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gnome.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


gboolean
on_Mainwindow_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_chooseBotany_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_divisio_new1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_divisio_edit1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_divisio_save1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_divisio_clear1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_class_new1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_class_edit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_class_save1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_class_clear1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_ordo_new1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_ordo_edit1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_ordo_save1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_ordo_clear1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_family_new1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_family_edit1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_family_save1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_family_clear1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_genus_new1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_genus_edit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_genus_save1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_genus_clear1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_clear1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_eFindNumber_editing_done            (GtkCellEditable *celleditable,
                                        gpointer         user_data)
{

}


gboolean
on_eFindNumber_key_press_event         (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_eFindDesignation_editing_done       (GtkCellEditable *celleditable,
                                        gpointer         user_data)
{

}


gboolean
on_eFindDesignation_key_press_event    (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_eFindDescription_editing_done       (GtkCellEditable *celleditable,
                                        gpointer         user_data)
{

}


gboolean
on_eFindDescription_key_press_event    (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_tree1_row_activated                 (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data)
{

}


void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data)
{

}


void
on_bBotanyDMS_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eArticleID_changed                  (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bChooseArticle_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}

