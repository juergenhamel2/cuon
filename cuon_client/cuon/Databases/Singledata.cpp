/*Copyright (C) [2012]  [Juergen Hamel, D-32584 Loehne]

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the
Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
*/

#include <map>
#include <list>
#include <gtkmm.h>

#include <iostream>
#include <string>
#include <stdlib.h>
#include <cuon/Databases/Singledata.hpp>
#include <cuon/Windows/setOfEntries.hpp>
#include <cuon/Windows/dataEntry.hpp>
#include <cuon/Databases/datatypes.hpp>


Singledata::Singledata()
{

     sNameOfTable = "EMPTY";
     sSort ;
     //firstRecord = new Record();
     ID = 0;
     cout << " Test" << endl ;
     renderer = gtk_cell_renderer_text_new ();

}
Singledata::~Singledata()
{
}

void  Singledata::load(int _id)
{
     // liRecords, self.firstRecord,  self.ID = self.rpc.callRP('Database.loadData', self.sNameOfTable, record, self.dicInternetUser, dicDetail)

     vector<Record> oResultSet ;
     x1.callRP("Database.loadOneData",x1.add(sNameOfTable, _id), oResultSet);
     cout << "load new data id 0 = " <<  endl;
     firstRecord = oResultSet.at(0);
     ID = firstRecord.row_int["id"];
     cout << "load new data id = " << ID << endl;


}

void Singledata::setLiFields(int i, string sNames[],string sHeader[], string sType[], string sCols[])
{


     cout << "size setLifields " << i << endl ;
     for (int j=0; j<i; j++) {
          newEntry.liFields.push_back(sNames[j]);
          //cout << sNames[j] << endl ;
          vector <string> vF ;
          vF.push_back(sHeader[j]);
          vF.push_back(sType[j]);
          vF.push_back(sCols[j]);
          vF.push_back(sNames[j]);
          dicFields[sCols[j]] = vF ;
          aFields.push_back(sCols[j]);
          cols.addColumn(sCols[j],sType[j]);
     }




//     cols.addColumn("name2","string");
     cout << "end setLifields" << endl ;

}
void Singledata::setDicEntries(setOfEntries _entries)
{

     oEntries = _entries ;
     newEntry.dicEntries = oEntries.getDicEntries();

}

void Singledata::getListEntries()
{

// dicLists = self.rpc.callRP('Database.getListEntries',dicFields, self.table.getName() , self.sSort, self.sWhere, self.dicInternetUser,
//                           self.bDistinct,  self.liFields)




     cout << "get list entries" << endl ;



     x1.callRP("Database.getListEntries",x1.add(newEntry),ResultSet );
     setListEntries();

}
void Singledata::setTree1(Gtk::TreeView *_pTree1)
{
     pTree1 = _pTree1 ;
     pTree1->set_model(listStore = Gtk::ListStore::create(cols));
     selection = pTree1->get_selection() ;
     selection->signal_changed().connect(sigc::mem_fun(this, &Singledata::on_selection_changed) );
}

void Singledata::on_selection_changed()
{
     cout << "selected 0.1 "  << endl ;
     Gtk::TreeModel::iterator iter = selection->get_selected();
     cout << "selected 0.2"  << endl ;
     if(iter) { //If anything is selected
          cout << "selected 1 "  << endl ;
          Gtk::TreeModel::Row row = *iter;
          cout << "selected 2 "  << endl ;
          cout << row[cols.getIntColumn("id")] << " is selected " << endl ;
          setEntries(row[cols.getIntColumn("id")]);
     }

}
void Singledata::setEntries(int i)
{
     load(i);


     typedef map<string,dataEntry>::const_iterator iM ;
     dataEntries = oEntries.Entries;

     for(iM i = dataEntries.begin(); i != dataEntries.end(); i++) {
          _entry = i->second ;
          cout << "_entry " << _entry.get_nameOfEntry() << " type = " << _entry.get_verifyTypeOfEntry() << endl ;

          if(_entry.get_verifyTypeOfEntry() == "string") {
               string _Value = firstRecord.row_string[_entry.get_nameOfSql()];
               cout << "Value = " << _Value << endl ;
               glx.setText(_entry,_Value);
          } else if (_entry.get_verifyTypeOfEntry() == "int") {
               int _Value = firstRecord.row_int[_entry.get_nameOfSql()];
               cout << "Value = " << _Value << endl ;
               glx.setText(_entry,_Value);
          }


     }

}
void Singledata::setListEntries()
{
     // Header = 0, Type = 1, col = 2
     cout << "dicEntries 0" <<  endl;
     typedef vector<Record>::const_iterator iV ;
     for(iV i = ResultSet.begin(); i!= ResultSet.end(); i++ ) {
          map <string,string> m1 = i->row_string ;
          map <string,int> m2 = i->row_int ;
          row = *(listStore->append());


          typedef vector<string>::const_iterator iS ;

          for (iS j= aFields.begin(); j != aFields.end(); j++) {



               string sSearch = *j;
               cout << "string 0 " << sSearch << endl;
//               cout << "string 2 a0 " << dicFields[sSearch][0] << endl;
//               cout << "string 2 a1 " << dicFields[sSearch][1] << endl;
//               cout << "string 2 a2 " << dicFields[sSearch][2] << endl;
//               cout << "string 2 a3 " << dicFields[sSearch][3] << endl;


               string s1 = m1[dicFields[sSearch][3]];
               if (!s1.empty()) {
                    cout << "string 1 " << s1 << endl;

                    string sColumnName = dicFields[sSearch][2];
                    row[cols.getStringColumn(sColumnName)] = m1[dicFields[sSearch][3]] ;
               } else {
                    int i1 = m2[dicFields[sSearch][3]] ;
                    if (i1) {
                         cout << "string int 1 : " << i1 << endl;
                         string sColumnName = dicFields[sSearch][2];

                         row[cols.getIntColumn(sColumnName)] = m2[dicFields[sSearch][3]] ;
                    }
               }

          }

     }


     typedef vector<string>::const_iterator iS ;

     for (iS j= aFields.begin(); j != aFields.end(); j++) {
          string sColumnName = dicFields[*j][2];
          string sHeader = dicFields[*j][0];
          string sType = dicFields[*j][1];
          if (sType == "int") {
               cout << "int-colum " << sColumnName << endl;

               pTree1->append_column(sHeader, cols.getIntColumn(sColumnName));
          } else {
               pTree1->append_column(sHeader, cols.getStringColumn(sColumnName));
          }


     }



     pTree1->show_all_children();


}


