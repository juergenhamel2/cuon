#ifndef DATATYPES_HPP
#define DATATYPES_HPP

using namespace std;
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <ctime>
#include <gtkmm.h>
#include <cuon/TypeDefs/basics.hpp>


class Columns : public Gtk::TreeModel::ColumnRecord {
public:

     map <string, Gtk::TreeModelColumn<unsigned> > iCol ;
     map <string, Gtk::TreeModelColumn<Glib::ustring> > sCol;
     map <string,   Gtk::TreeModelColumn<float> > fCol;
     map <string,   Gtk::TreeModelColumn<bool> > bCol;
     Columns() ;




     ~Columns() ;
     void addColumn(string sName,string sType);
    Gtk::TreeModelColumn<Glib::ustring> getStringColumn(string sName);
     Gtk::TreeModelColumn<unsigned> getIntColumn(string sName);
      Gtk::TreeModelColumn<bool> getBoolColumn(string sName);
       Gtk::TreeModelColumn<float> getFloatColumn(string sName);
};


class Record {
public:
     Record();
     ~Record();
     map <string,string> info;
     map <string,int> row_int ;
     map <string,float> row_float ;
     map <string,string> row_string ;
     map <string,tm *> row_date ;

};

class listEntry {
public:
     listEntry();
     ~listEntry();
     string sNameOfTable ;
     string sWhere ;
     string sSort ;
     vector <string> liFields ;
     map <string,string> dicEntries ;
     bool bDistinct ;


};


class datatypes : public basics  {
public:
     struct liEntry {
          string verifyType ;


     };
     //Columns cols;
     vector<Record> ResultSet;
     datatypes();
     ~datatypes();

};


#endif

