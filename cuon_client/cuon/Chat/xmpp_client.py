# -*- coding: utf-8 -*-

##Copyright (C) [2009]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 


#from twisted.web import xmlrpc
import sys,os,string,threading,time,curses
from time import strftime
import xmlrpclib
from twisted.internet.protocol import Protocol
from twisted.words.protocols.jabber import client, jid,   xmlstream
from twisted.words.xish import domish
from twisted.words.xish.domish import Element
from cuon.TypeDefs.constants import constants
import time 
import shelve
   
        
class xmpp_client(constants):
    def __init__(self, userjid, password):
       
        
        
        constants.__init__(self)
       
        self.filename = 'dic_jabberusers'
        self.me = jid.JID(userjid)
        self.juser = userjid
        self.factory = client.basicClientFactory(self.me, password)
        #self.Server = xmlrpclib.ServerProxy(self.XMLRPC_PROTO + '://' + self.XMLRPC_HOST + ':' + `self.XMLRPC_PORT`)
        self.theXmlstream = None
        self.dicUsers = {}
        self.factory.addBootstrap('//event/stream/authd',self.authd)
       
        
        # Authorized
    def authd(self,  xmlstream):
        # need to send presence so clients know we're
        # actually online.
        print 'start authd'
        presence = domish.Element(('jabber:client', 'presence'))
        presence.addElement('status').addContent('Online')
        self.theXmlstream = xmlstream
        self.theXmlstream.send(presence)
        self.theXmlstream.addObserver('/message', self.gotMessage)
        print 'new xmlstream = ',  self.theXmlstream 
    
    def create_reply(self,  elem):
        """ switch the 'to' and 'from' attributes to reply to this element """
        # NOTE - see domish.Element class to view more methods 
        
        msg_frm = elem['from']
        msg_to = elem['to']
        
        message = domish.Element(('jabber:client','message'))
        message["to"] = msg_frm
        message["from"] = msg_to
        message["type"] = "chat"
        
        return message
  
    def buildProtocol(self, addr):
        print 'Connected.'
        return Echo()
        
    def send(self, to, body, subject=None, mtype=None, delay=None):
        print 'start sending'
        el = Element((None, "message"))
        el.attributes["to"] = to
        el.attributes["from"] = self.juser
        el.attributes["id"] = '111111'
        
        if(subject):
            subj = el.addElement("subject")
            subj.addContent(subject)
        
        if(mtype):
            el.attributes["type"] = mtype
        
        if(delay):
            x = el.addElement("x")
            x.attributes["xmlns"] = "jabber:x:delay"
            x.attributes["from"] = fro
            x.attributes["stamp"] = delay
        
        b = el.addElement("body")
        b.addContent(body)
              
        
        
        self.theXmlstream.send(el)    

        print 'done sending'
        
    def gotMessage(self,  message):
        # sorry for the __str__(), makes unicode happy
        print u"from1: %s" % message["from"]
        send_from = message["from"].strip()
        
       
        self.displayMessage(send_from,  message)
        
        
    def displayMessage(self,  send_from, message):
        password


    def getTime(self):
        return time.strftime(self.liTimes['hour24'],time.localtime())
