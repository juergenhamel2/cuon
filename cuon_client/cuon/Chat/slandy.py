from cuon.XMLRPC.xmlrpc import myXmlRpc
import hashlib 
import time
import gobject

class slandy(myXmlRpc):
    def __init__(self):
                
        myXmlRpc.__init__(self)
        
        # connection-Data                                                               
        #self.server="http://192.168.17.12:9012"
      
        self.dicStatusbar = {'Status':'Offline', 'Parcel':'','SimName':'', 'X':'0', 'Y':'0','Z':'0'}
        # connect to Server                                                             
        print '#################################### Test Server = ',  self.callRP('Slandy.is_running')
        self.t0 = None
        self.singleUser = {}
        self.singleUser['AvatarUUID'] = None
        self.singleUser['AvatarName'] = None
        
        
    def gridLogin(self,  dicUser):
        
        # use later self.dicUser
        #try:
        UUID = self.callRP("Slandy.Login", dicUser['Firstname'], dicUser['Lastname'], dicUser['Password'], dicUser['Grid'],dicUser['Location']  )
        self.singleUser['UUID']= UUID
        print UUID 
        
        allTime = 0
        
        #'print start Timer'
        # 60*1000 = 1 minute
        second = 1000
        minute = 60*second
        if self.t0:
            gobject.source_remove(self.t0)
            
        self.t0 = gobject.timeout_add(1*second, self.new_alive, UUID)
           
#        data = self.callRP('Slandy.Logout', UUID)
#        print 'return data alive = ',  data 
#        return allTime   
        #except Exception,  params:
        #    print 'Seems,  that the xmlrpc server is NOT running! Check ports and hostname and look if there is a program running with:'
        #    print Exception,  params
            
        return UUID
        
    def GridLogout(self, UUID):
        
        ok = self.callRP('Slandy.Logout', UUID)
        if self.t0:
            gobject.source_remove(self.t0)
        self.dicStatusbar['Status'] = 'Offline'
        self.setStatusbar()
        return True
            
    def new_alive(self, UUID):
        
        data = self.callRP('Slandy.get_alive', UUID)
        #print 'one data = ',  data.keys()
        for key in data.keys():
            if data[key] != ['NONE']:
               
                if key == 'Chat':
                    for msg in data[key]:
                        print 'msg = ',  msg
                        text_msg = msg['FromName'] +' '
                        if msg['ChatType'] == 'Normal':
                            text_msg += 'says: '
                        elif msg['ChatType'] == 'Shout':
                            text_msg += 'shouts: '
                            
                        elif msg['ChatType'] == 'Whisper':
                            text_msg += 'whispers: '
                        else:
                            text_msg += 'says: '
                        text_msg += msg['Message']
                        self.addText2LocalChat(text_msg)
                elif key == 'IM':
                    for msg in data[key]:
                        print 'received IM = ',  msg
                        self.getIM(msg)
                        
                elif key == 'CurrentParcel':
                    for parcel in data[key]:
                        self.dicStatusbar['Parcel'] = parcel['Name']
                        self.setStatusbar()
                        self.setObject(key, parcel)
                          
                elif key == 'CurrentLocation':
                    for location in data[key]:   
                        self.dicStatusbar['Location'] = location['SimName']
                        
                        self.dicStatusbar['X'] = location['X'][:location['X'].find('.')][:location['X'].find(',')]
                        self.dicStatusbar['Y'] = location['Y']
                        self.dicStatusbar['Z'] = location['Z']
                        self.setStatusbar()
                        
                elif key == 'StatusChange':
                    #print 'Status0 = ',  data[key]
                    for status in data[key]: 
                        #print 'Status = ',  status
                        if status['Status'] == 'Online':
                            print 'Online,  now get Friendslist'
                            self.dicStatusbar['Status'] = status['Status']
                            #self.singleUser['AvatarUUID'] = status['AvatarUUID']
                            self.setStatusbar()
                            print 'now ask for friends'
                            self.getFriendslist()
                            
                            
                elif key == 'FriendList':
                    print 'All Friends = ',  data[key] 
                    for friend in data[key]:
                         self.setFriend(friend)
                elif key == 'GroupList':
                    print 'All Groups = ',  data[key] 
                    for group in data[key]:
                         self.setGroup(group)         
                    
                elif key == 'NearbyAvatar':
                    print 'Nearby ',  data[key] 
                    for nearbyavatar in data[key]:
                        self.setNearBy(nearbyavatar)
        return True
        
        
    def sendLocal(self, uuid, msg,  Channel='0', Type='Normal'):
        ok = self.callRP('Slandy.sendLocal', uuid, msg, Channel,  Type)
        
        return ok 
        
    def sendIM(self,  uuid, msg, sendTo):
        ok = self.callRP('Slandy.sendIM', uuid, msg, sendTo) 
        return ok 

    def setStatusbar(self, msg = None):
        pass
        
    def addText2LocalChat(self,  msg):
        pass
    
    def getIM(self, IM):
        pass
        
    def setObject(self,key , dicObject):
        pass
        
    def getFriendslist(self):
        pass
        
    def setFriend(self,  friend):
        pass
    
    def setGroup(self, group):
        pass   
    def setNearBy(self, nearbyavatar):
        pass
