# -*- coding: utf-8 -*-

##Copyright (C) [2011]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 


import cuon.XMLRPC.xmlrpc
import uuid
from ConfigParser import ConfigParser
import gtk
from xmpp_client import xmpp_client
from slandy import slandy 
import cuon.Misc.misc
from cuon.Windows.windows import windows 



class chatWindow( windows,  xmpp_client,  slandy):
   
    
    def __init__(self):
        
        windows.__init__(self)
       
        user_jid = 'Test'
        password_jid = 'Test'
        # Read the real passwords
        
        xmpp_client.__init__(self,  user_jid, password_jid)
        slandy.__init__(self)
        self.loadGlade('chat.xml')
        self.win1 = self.getWidget('ChatWindow')
        
        
        self.itemFriends = []
        self.textbufferIM = {}
        self.viewIM = {}
        self.liTabs = []
        self.uuid = None
       
        #print self.dicUser
        print 'conferences = ',  self.dicUser['Conference']
        try:
           
            nm='Chat'
            mss = cf.get(nm, "MessageServer").strip()
            juser = cf.get(nm, "MessageUserName").strip()
            jpasswd = cf.get(nm, "MessageUserPassword").strip()
            xmpp_client.__init__(self, juser+'@'+mss,jpasswd)
            reactor.connectTCP(mss,5222,self.factory)
            print 'xmpp ok', mss,  juser
        except Exception,  params:
            print Exception,  params
              
      
#       

        # GUI
        self.winIsOn = True
        self.liChatTabs = ['LocalChat']
        
        # Grid
        
        # GridFriendslist
        self.GridFriendsID = []
        self.selectedGridFriendUUID = None
        self.selectedGridFriendName = None
        self.lboxGridFriends = self.getWidget('treeGridFriends')
        treestore = gtk.TreeStore(str)
        renderer = gtk.CellRendererText()
            
        column = gtk.TreeViewColumn('Friends', renderer, text=0)
        self.lboxGridFriends.append_column(column)
        self.lboxGridFriends.set_model(treestore)
        self.lboxGridFriends.show()

        #GridGroupList
        self.lboxGridGroups = self.getWidget('treeGridGroups')
        self.GridGroupsID = []
        self.selectedGridGroupUUID = None
        self.selectedGridGroupName = None
        
        treestore = gtk.TreeStore(str)
        renderer = gtk.CellRendererText()
            
        column = gtk.TreeViewColumn('Groups', renderer, text=0)
        self.lboxGridGroups.append_column(column)
        self.lboxGridGroups.set_model(treestore)
        self.lboxGridGroups.show()
        
         #NearByList
        self.lboxGridNearBy = self.getWidget('treeGridNearB')
        self.GridNearByID = []
        self.selectedGridNearByUUID = None
        self.selectedGridNearByName = None
        
        treestore = gtk.TreeStore(str)
        renderer = gtk.CellRendererText()
            
        column = gtk.TreeViewColumn('Near by', renderer, text=0)
        self.lboxGridNearBy.append_column(column)
        self.lboxGridNearBy.set_model(treestore)
        self.lboxGridNearBy.show()
        # Other widgets
        self.tvLocal = self.getWidget('tvLocal')
        self.tvObject = self.getWidget('tvObject')
        self.statusbar1 = self.getWidget('statusbar1')
        self.sb_id = self.statusbar1.get_context_id('general_info')
        self.tvLocal = self.getWidget('tvLocal')
        
        
        # XMPP
        self.lboxFriends = self.getWidget("friendsList")
        self.eIM = self.getWidget("eIM")
        
        
        self.bSendIM = self.getWidget("bSay")
        self.bSendIM = self.getWidget("bWhisper")
        self.bSendIM = self.getWidget("bShout")
        
        self.tvSend = self.getWidget('tvSend')
        
        self.notebook1 = self.getWidget("notebook1")
        self.notebook2 = self.getWidget("notebook2")
        self.notebook3 = self.getWidget("notebook3")
        self.notebook4 = self.getWidget("notebook4")
        

        
       
       
        
        self.rpc = cuon.XMLRPC.xmlrpc.myXmlRpc()
        
        # Variables
        
        self.localFriend = {}

        self.setListbox()
       
        self.rpc.server = 'http://vrapnet.com:8002'
#
#        # important widgets
#        self.eIM = self.getWidget('eMessage')
#        self.bSendIM = self.getWidget('bSendIM')
#        # treeview settings
#        
#        liFriends, liGroups = self.rpc.callRP('User.getXmpp',self.dicUser)
#        print liFriends,  liGroups
#        
#        
            
    # Buttons
    
        # start
    
        self.tabOption = 0
        self.tabOption2 = 100
        self.tabOption3= 1000
        self.tabOption4= 10000
        self.tabGrid = 0
        self.tabXmpp = 1
        self.tabIM = 0
        
        self.tabFriend = 100
        self.tabGroup = 101
        
        
        
        self.tabXmmpBuddy = 1000
        self.tabXmppGroup = 1001


        self.tabLocal = 10000
        
        self.setStatusbar('Client started')
        self.tabChanged()
    
        
#    def onOpenIM(self,  _obj, _prop):
#        #self.send('jhamel@cuonsim4.de', 'test chatwindow')
#        print self.sNewId
#         
#        print 'new IM'
#        if self.sNewId:
#            if self.sNewId in self.liTabs:
#                print 'Tab exists'
#            else:
#                print 'new tab with ',  self.sNewId
#                self.setNewTab(self.sNewId)
#            
#            self.tabOption = self.liTabs.index(self.sNewId)+3
#            print 'new tabOption = ',  self.tabOption
#            self.switchPage(self.tabOption)
#            #self.tabChanged()
    def on_tbLogin_clicked(self,  event):
        # to do, open Login Windows
        print 'login to SL'
        if self.tabOption == self.tabGrid:
            print 'conferences = ',  self.dicUser['Conference']
            fnln = self.dicUser['Conference']['Name'].split(' ')
            self.dicUser['Firstname'] = fnln[0].strip()
            self.dicUser['Lastname'] = fnln[1].strip() 
            self.dicUser['Password'] = self.dicUser['Conference']['Password']
            self.dicUser['Grid'] = self.dicUser['Conference']['Grid']
            self.dicUser['Location'] ='last'
            self.uuid = self.gridLogin(self.dicUser)
       
       
    def on_tbLogout_clicked(self, event):
        print 'click on logout'
        ok = self.GridLogout(self.uuid)
    
    def on_bOpenIM_clicked(self, event):
        im_uuid = self.selectedGridFriendUUID        
        im_name = self.selectedGridFriendName
        
        label_im_name = gtk.Label(im_name)
        isOk = False
        
        for i in range(len(self.liChatTabs)):
            
            
            if self.liChatTabs[i] == im_uuid:
                self.tabIM = 10000 + i
                self.notebook4.set_current_page(i)
                gtk.gdk.beep()
                isOk = True
                break

        if not isOk:
            self.textbufferIM[im_uuid],  self.viewIM[im_uuid] = self.getNotesEditor()
            sc = gtk.ScrolledWindow()
            sc.add(self.viewIM[im_uuid])
            sc.show_all()
            iNr = self.notebook4.insert_page(sc, tab_label=label_im_name, position=-1)
            self.notebook4.show_all()
            self.liChatTabs.append(im_uuid)
            self.tabIM = len(self.liChatTabs) + 10000
            self.notebook4.set_current_page(i)
            gtk.gdk.beep()
            print 'allTabs = ',  self.liChatTabs
    def on_tvSend_key_press_event(self,  event,  data1,  data2):
        print event,  data1,  data2
        
    def on_bSay_clicked(self, event):
        print 'send message',  self.tabOption2,  event 
        
        text = self.readTextBuffer(self.tvSend)
        self.clearTextBuffer(self.tvSend)
        print text,  self.tabOption4
        
        if self.tabOption4 == self.tabLocal:
            
            self.sendLocal(self.uuid, text)
            msg_text = self.dicUser['Firstname'] + ' ' + self.dicUser['Lastname'] + ' says: ' + text
            self.addText2LocalChat(text)
        else:
            print 'self.tabOption4 = ',  self.tabOption4
            
            sendTo = self.liChatTabs[self.tabOption4 - 10000] 
            self.sendIM(self.uuid, text, sendTo)
            self.add2Textbuffer(self.textbufferIM[sendTo], text +'\n', 'Tail') 
            print 'allTabs2 = ',  self.liChatTabs
#        elif self.tabOption == self.tabXmmpBuddy:
#      
#            sText = self.eIM.window.GetValue()
#            print 'sText = ',  sText
#            if sText:
#                if self.tabOption == self.tabLocal:
#                    self.sendLocal(sText)
#                    
#                    print 'local send'
#                else:
#                    print self.liTabs
#                    sName = self.liTabs[self.tabOption -3]
#                    #print 'name = ',  sAll,  sName
#                    sTo = self.localFriend[sName]['JID']
#                    #print sTo,  sText
#                    self.send(sTo, sText)
#                    
#                    tab = self.notebook1.window.GetPage(self.tabOption)
#                    print tab
#                    tb = tab.GetChildren()
#                    
#                    if tb:
#                        tb[0].AppendText(self.getTime() + ' ' + self.juser + ': ' + sText + '\n')
#                    self.eIM.window.Clear()
#        
    #override from slandy
    def addText2LocalChat(self,  msg):
        print 'add local chat = ',  msg
        self.add2Textbuffer(self.tvLocal,msg +'\n', 'Tail')
        return True

    def getIM(self, IM):
        print 'new IM',  IM
        im_uuid = IM['UUID']
        im_name = IM['Name']
        label_im_name = gtk.Label(im_name)
        isOk = False
        for i in range(len(self.liChatTabs)):
            
            
            if self.liChatTabs[i] == im_uuid:
                self.tabIM = 10000 + i
                isOk = True
                break 
                
        if not isOk:
            self.textbufferIM[im_uuid],  self.viewIM[im_uuid] = self.getNotesEditor()
            sc = gtk.ScrolledWindow()
            sc.add(self.viewIM[im_uuid])
            sc.show_all()
            iNr = self.notebook4.insert_page(sc, tab_label=label_im_name, position=-1)
            self.notebook4.show_all()
            self.liChatTabs.append(im_uuid)
            self.tabIM = len(self.liChatTabs) + 10000
        self.add2Textbuffer(self.textbufferIM[im_uuid], IM['Message']+'\n', 'Tail') 
        print 'allTabs3 = ',  self.liChatTabs
        #self.tabChanged()
        
        
    def displayMessage(self,  send_from, message):
  
        #print unicode(message.__str__())
        sender = None
        if self.winIsOn == False:
            self.onChatOn(None,  None)
        if send_from:
            for sName in self.localFriend:
                if send_from[:send_from.find('/')] == self.localFriend[sName]['JID']:
                    sender = sName
                    break ;
            #print 'sender = ',  sender
            if not sender:
                self.localFriend[send_from[:send_from.find('/')]] = { 'Alias':send_from[:send_from.find('/')], 'JID':send_from, 'localUUID':uuid.uuid4() }
                sender = send_from[:send_from.find('/')]
            if sender in self.liTabs:
                print 'Tab exists'
            else:
                print 'new tab with ',  send_from
                self.setNewTab(sender)
            
            self.tabOption = self.liTabs.index(sender)+3
            print 'new tabOption = ',  self.tabOption
            self.switchPage(self.tabOption)
            self.tabChanged()
            
            for e in message.elements():
                if e.name == "body":
                    #print unicode(e.__str__())
                    msg_body = unicode(e.__str__())
                    # echo message for testing
                    #self.send(send_from,  u'back = ' + msg_body, 'backtosender')
                    tab = self.notebook1.window.GetPage(self.tabOption)
                    print tab
                    tb = tab.GetChildren()
                    
                    if tb:
                        print 'tb id = ',  tb[0].GetId( ), tb[0].GetName(),  tb[0].GetLabel()
                        tb[0].AppendText(self.getTime() + ' ' + sender.decode('UTF-8') + ': ' + msg_body + '\n')
                    
        

    # Listboxes
        
    
        
    def onSelectFriend(self, _obj, selection):
        print 'selection = ',  selection
        self.sNewId = selection
                   
    def setListbox(self):
        
           
        # First look for local Friends
        cpServer = ConfigParser()
        self.localFriend = {}
        try:
            cpServer.readfp(open('local_friends.cfg'))
            for i in range(1, 50):
                try:
                    if cpServer.has_option('xmpp','friend'+`i`):
                        value = cpServer.get('xmpp','friend'+`i`).strip()
                        liValue = value.split(',')
                        self.localFriend[liValue[0]] = { 'Alias':liValue[1], 'JID':liValue[2], 'localUUID':uuid.uuid4() }
                    else:
                        break
                        
                except:
                    pass
        except:
            print 'no friendsList'
        print 'localFriends = ',  self.localFriend
        if self.localFriend:
            for friend in self.localFriend:
                print friend
                #self.lboxFriends._addItem(friend +' (' + self.localFriend[friend]['Alias'] +')')             
                self.itemFriends.append(friend )
      
        
        #self.lboxFriends.itemsUpdated()
            
   
    def setNewTab(self, newID):
        #print 'new tab',  newID,  self.localFriend[newID]
        newTab = Tab(parent=self.window, )
        newEdit = TextBox(parent=newTab, multiline=True, width=550,  height=200 )
        
        self.liTabs.append(newID)
        #newSizer = wx.BoxSizer()
        #newSizer.Add(newEdit)
        #newTab.window.Add(newEdit)
        self.notebook1.window.AddPage(newTab.window,newID )
        self.notebook1._pageUpdated(None, None)
        
        
    def setStatusbar(self,  msg = None):
        try:
            print "setStatusbar = ",  msg
            sText = self.dicStatusbar['Status'] + ' -- ' + self.dicStatusbar['SimName'] + ', ' +self.dicStatusbar['Parcel'] +' : ' + self.dicStatusbar['X'] +',  ' + self.dicStatusbar['Y'] +',  ' + self.dicStatusbar['Z'] +'  ' 
            if msg:
                sText += ' ' +  msg
            if not sText:
                sText = ''
                
            self.statusbar1.push(self.sb_id, sText)
            
        except Exception, params:
            print Exception, params
            
        return True
        
    def setObject(self, key,  dicObject):
        print 'setObject = ',  key,  dicObject
        try:
            if key == 'CurrentParcel':
                
                self.clearTextBuffer(self.tvObject)
                self.add2Textbuffer(self.tvObject, dicObject['Description'])
                
                
        except Exception, params:
            print Exception,  params
            
            return True 
      


    def getFriendslist(self):
        ok = self.callRP('Slandy.getFriendslist', self.uuid)        
     
  
    def setFriend(self,  friend):
        print 'friend = ',  friend
        self.addGridFriendslist(friend)
        self.GridFriendsID.append(friend)
        

    


    def disconnectGridFriends(self):
        try:
            
            self.getWidget('treeGridFriends').get_selection().disconnect(self.connectGridFriendsId)
        except:
            pass

    def connectGridFriends(self):
        try:
            self.connectGridFriendsId = self.getWidget('treeGridFriends').get_selection().connect("changed", self.GridFriends_select_callback)
        except:
            pass
   
    def GridFriends_select_callback(self, treeSelection):
        listStore, iter = treeSelection.get_selected()
        self.selectedGridFriendUUID = None
        print listStore,iter
        
        if listStore and len(listStore) > 0:
           row = listStore[0]
        else:
           row = -1
   
        if iter != None:
            sNewId = listStore.get_value(iter, 0)
            print sNewId
            try:
                for friend in self.GridFriendsID:
                    print 'Friend = ', friend['Name'], sNewId[8:]  
                    if friend['Name'] == sNewId[8:]:
                        self.selectedGridFriendUUID = friend['UUID']
                        self.selectedGridFriendName = sNewId[8:]
                        break
                        
                #self.setDateValues(newID)
                
            except:
                pass
   
            
    def on_treeGridFriends_row_activated(self,event,data1, data2):
        print 'activated = ', event,  data1,  data2 
        print 'select = ',  self.selectedGridFriendUUID
        
    def addGridFriendslist(self,  friend):
        
        treestore = self.lboxGridFriends.get_model()
        if friend['Status'] == 'Online':
            st1 = ' Online '
        else:
            st1 = 'Offline '
        iter = treestore.append(None,[st1 + friend['Name']]) 
        
        self.lboxGridFriends.show()
        self.connectGridFriends()
       
    
  
    def setGroup(self,  group):
        print 'group = ',  group
        self.addGridGroupslist(group)
        self.GridGroupsID.append(group)
        

    


    def disconnectGridGroups(self):
        try:
            
            self.getWidget('treeGridGroups').get_selection().disconnect(self.connectGridGroupsId)
        except:
            pass

    def connectGridGroups(self):
        try:
            self.connectGridGroupsId = self.getWidget('treeGridGroups').get_selection().connect("changed", self.GridGroups_select_callback)
        except:
            pass
   
    def GridGroups_select_callback(self, treeSelection):
        listStore, iter = treeSelection.get_selected()
        self.selectedGridGroupUUID = None
        print listStore,iter
        
        if listStore and len(listStore) > 0:
           row = listStore[0]
        else:
           row = -1
   
        if iter != None:
            sNewId = listStore.get_value(iter, 0)
            print sNewId
            try:
                for group in self.GridGroupsID:
                    if group['Name'] == sNewId:
                        self.selectedGridGroupUUID = group['UUID']
                        self.selectedGridGroupName = sNewId
                        break
                        
                #self.setDateValues(newID)
                
            except:
                pass
   
            
    def on_treeGridGroups_row_activated(self,event,data1, data2):
        print 'activated = ', event,  data1,  data2 
        print 'select = ',  self.selectedGridGroupUUID
        
    def addGridGroupslist(self,  group):
        
        treestore = self.lboxGridGroups.get_model()
        iter = treestore.append(None,[group['Name']]) 
        
        self.lboxGridGroups.show()
        self.connectGridGroups()
       
    def setNearBy(self, nearbyavatar):
        print nearbyavatar
        sOk = False
        for ava in self.GridNearByID:
            if ava['Name'] == nearbyavatar['Name']:
                # to do change later values like the coords
                sOk= True
        
        if not sOk:
            print 'NearbyName = ',  nearbyavatar['Name'],   self.singleUser['AvatarName']
            print 'NearbyUUID = ', nearbyavatar['UUID'],   self.singleUser['AvatarUUID']
            if nearbyavatar['UUID'] != self.singleUser['AvatarUUID']:
                self.addGridNearBylist(nearbyavatar)
                self.GridNearByID.append(nearbyavatar)




    def disconnectGridNearBy(self):
        try:
            
            self.getWidget('treeGridNearBy').get_selection().disconnect(self.connectGridNearById)
        except:
            pass

    def connectGridNearBy(self):
        try:
            self.connectGridNearById = self.getWidget('treeGridNearBy').get_selection().connect("changed", self.GridNearBy_select_callback)
        except:
            pass
   
    def GridNearBy_select_callback(self, treeSelection):
        listStore, iter = treeSelection.get_selected()
        self.selectedGridNearByUUID = None
        print listStore,iter
        
        if listStore and len(listStore) > 0:
           row = listStore[0]
        else:
           row = -1
   
        if iter != None:
            sNewId = listStore.get_value(iter, 0)
            print sNewId
            try:
                for nearby in self.GridNearByID:
                    if nearby['Name'] == sNewId[8:]:
                        self.selectedGridNearByUUID = nearby['UUID']
                        self.selectedGridNearByName = sNewId[8:]
                        break
                        
                #self.setDateValues(newID)
                
            except:
                pass
   
            
    def on_treeGridNearBy_row_activated(self,event,data1, data2):
        print 'activated = ', event,  data1,  data2 
        print 'select = ',  self.selectedGridFriendUUID
        
    def addGridNearBylist(self,  nearby):
        
        treestore = self.lboxGridNearBy.get_model()
        if nearby['Present'] == 'True':
            st1 = ' Online '
        else:
            st1 = 'Offline '
        iter = treestore.append(None,[st1 + nearby['Name']]) 
        
        self.lboxGridNearBy.show()
        self.connectGridNearBy()
       
    
    def tabChanged(self):
        print 'tab changed', self.tabOption
        
        
        if self.tabOption3 >= 1003 and self.tabOption3<= 2000:
            self.eIM.window.SetEditable(True)
            self.bSendIM.window.Enable()
        elif self.tabOption3 == self.tabFriend:
            self.eIM.window.SetEditable(False)
            self.bSendIM.window.Disable()
        elif self.tabOption3 == self.tabGroup:
            self.eIM.window.SetEditable(False)
            self.bSendIM.window.Disable()
        elif self.tabOption3 == self.tabLocal:
            self.eIM.window.SetEditable(True)
            self.bSendIM.window.Enable()

