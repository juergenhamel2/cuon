
import gtk, gobject, cairo
from pandac.PandaModules import WindowProperties

import direct.directbase.DirectStart
from direct.showbase.DirectObject import DirectObject
#from direct.showbase.ShowBase import ShowBase
from direct.task.Task import Task
from pandac.PandaModules import *
from direct.distributed.PyDatagram import PyDatagram
from direct.distributed.PyDatagramIterator import PyDatagramIterator 
from direct.actor.Actor import Actor
import client
import MapObjects
import sys


class chat():
    def __init__(self):

        #Just some groundwork
        #base = ShowBase()
        self.base = base
        #self.base.disableMouse()
       
        
        
        #establish connection > send/receive updates > update world
        worldClient = client.Client(9099,"192.168.17.12")
        self.myTerrain = client.Terrain()
        N = client.PlayerReg()
        me = client.Me(self.myTerrain)
        
        self.base.camera.setPos(0,0,10)
        #Attach camera to player box
        #self.base.camera.reparentTo(me.model)
        #self.base.camera.setPos(0, 0, 5)
      
        #self.base.camera.lookAt(me.model)
      
        #base.camera.setZ(self.player.getZ() + 2.0)
      
        #self.base.camera.setP(base.camera.getP()) 
        
        
        keys = client.Keys()
        w = client.World()
        chatReg = client.chatRegulator(worldClient,keys)
        
        taskMgr.add(N.updatePlayers,"keep every player where they are supposed to be",extraArgs = [me])
        taskMgr.add(me.move,"move our penguin", extraArgs = [keys,self.myTerrain])
        taskMgr.add(worldClient.tskReaderPolling,"Poll the connection reader",extraArgs = [me,N,chatReg])
        taskMgr.add(w.UpdateWorld,"keep the world up to date",extraArgs = [me,worldClient])
        
        #=============================================================================#
        #test code for lighting, normal mapping, etc...#
        #ambient light
        alight = AmbientLight('alight')
        alight.setColor(Vec4(0.2, 0.2, 0.2, 1))
        alnp = render.attachNewNode(alight)
        render.setLight(alnp)
        me.model.setShaderAuto()
        #me.model.setNormalMap("models/nskinrd-normal.jpg")
        
        
        lightpivot = render.attachNewNode("lightpivot")
        lightpivot.setPos(0,0,25)
        plight = PointLight('plight')
        plight.setColor(Vec4(1, 1, 1, 1))
        plnp = lightpivot.attachNewNode(plight)
        render.setLight(plnp)
        me.model.setShaderInput("light", plnp)
        #=============================================================================#
        Castle = MapObjects.Castle(Vec3(288.96,294.45,30.17), Vec3(119.05,270,0),0.08)
        
        window = gtk.Window()
        Vbox = gtk.VBox()
        panda = Panda3DWidget(window)
        panda.set_size_request(1000, 800)
        panda.showbase = self.load_showbase()
        
        panda.connect("button-press-event", self.button_press)
        panda.connect("button-release-event", self.button_release)
        panda.connect("motion-notify-event", self.pointer_motion)
        
        Vbox.add(gtk.Button('Test'))
        Vbox.add(panda)
        
        window.add(Vbox)
        window.show_all()
        window.connect("destroy", gtk.main_quit)
        

        gtk.main()          
         
    def load_showbase(self):
        showbase = base
        #environ = showbase.loader.loadModel("models/environment")
        #environ.reparentTo(showbase.render)
        #environ.setScale(0.25, 0.25, 0.25)
        #environ.setPos(-8, 42, 0)
        return showbase
    
    
    #_button_pressed = False
    
    
    def button_press(self, widget, signal):
        global _button_pressed
        
        if signal.button == 1:
            _button_pressed = True
    
    
    def button_release(self, widget, signal):
        global _button_pressed
        
        if signal.button == 1:
            _button_pressed = False
    
        
    def pointer_motion(self, widget, signal):
        global _button_pressed
       
        if _button_pressed == True:
            print widget, signal
        
     
#run()


class Panda3DWidget(gtk.DrawingArea):
    
    _showbase = None
        
    def __init__(self, gtk_window):
        super(Panda3DWidget, self).__init__()

        self.gtk_window = gtk_window
        self.gtk_window.set_app_paintable(True)
        
        self.set_events(gtk.gdk.BUTTON_PRESS_MASK | gtk.gdk.BUTTON_RELEASE_MASK | gtk.gdk.POINTER_MOTION_MASK)
        
        # Tell GTK+ that we want to draw the windows background ourself.
        # If we don't do this then GTK+ will clear the window to the
        # opaque theme default color, which isn't what we want.
        self.set_app_paintable(True)
        
        self.connect("screen-changed", self.screen_changed)
        self.connect("expose-event", self.expose)
        
        # capture when the window is moved around or resized
        self.gtk_window.connect("configure-event", self.configure)
        
        self.screen_changed(self)     


    def _get_showbase(self):
        return self._showbase
    
    
    def _set_showbase(self, showbase):
        #assert isinstance(showbase, ShowBase), "argument 'showbase' must be of type direct.showbase.ShowBase"
        
        self._showbase = showbase
        # imitate the panda3D main loop
        gobject.timeout_add(30, self._showbase_step)
        
        
    def _del_showbase(self):
        self._showbase = None
        
    
    showbase = property(_get_showbase, _set_showbase, _del_showbase)

    
    def _showbase_step(self):
        self.showbase.taskMgr.step()
        if self._showbase == None:
            return False
        return True
            
        
    def configure(self, widget, event):
        if self.showbase != None:
            x_offset, y_offset = self.translate_coordinates(widget, 0, 0)
            
            x, y = self.gtk_window.window.get_position()
            
            wp = WindowProperties().getDefault()
            wp.setUndecorated(True)
            wp.setOrigin(x + x_offset, y + y_offset) 
            wp.setSize(self.get_allocation().width , self.get_allocation().height ) 
           
            
            # apply window property changes to the window 
            self.showbase.win.requestProperties(wp)
            
            # for debugging purposes turn show the frame rate
            # to decide about the interval for calling self.step
            # with a gobject timeout
            self.showbase.setFrameRateMeter(True)
            
        
    def expose(self, widget, event):
        context = widget.window.cairo_create()
        
        # set a clip region for the expose event
        context.rectangle(event.area.x, event.area.y,
                    event.area.width, event.area.height)
        context.clip()
        
        context.set_source_rgba(1.0, 1.0, 1.0, 0.0) # Transparent
        
        # Draw the background
        context.set_operator(cairo.OPERATOR_SOURCE)
        context.paint()
        context.stroke()
        
          
    def screen_changed(self, widget, old_screen=None):                
        # To check if the display supports alpha channels, get the colormap
        screen = widget.get_screen()
        colormap = screen.get_rgba_colormap()
        if colormap == None:
            raise Exception('Your screen does not support alpha channels!')        
        
        # Now we have a colormap with alpha channel for the screen, use it
        #widget.set_colormap(colormap)
        self.gtk_window.set_colormap(colormap)
        return False
    
 
 
    
if __name__ == '__main__':
    c1 = chat()
   
    

 
