import numpy as np
import matplotlib.pyplot as plt


class chart():
    def __init__(self):
    

        self.charts = ['barchart', 'circle']
        
        self.N=5
        self.liX=[]
        self.liY = []
        self.width = 0.35
        self.ind = None
        self.fig = plt.figure()
        self.ax = fig.add_subplot(111)
        self.chartType = 0
        self.rects = []
        
    def setN(self, n):
        self.N = n
        
    def addX(self, liX):
        self.liX.append(liX)
        
    def addY(self, liY):
        self.liY.append(liY)
        
    def setWidth(self, w):
        self.width = w
        
    def newRange(self, n):
        self.ind =  np.arange(self.N)  # the x locations for the groups
        
    def setChartType(self, iType):
        self.chartType = iType
        
        
        
    def autolabel(self, rects):
        # attach some text labels
        for rect in rects:
            height = rect.get_height()
            ax.text(rect.get_x()+rect.get_width()/2., 1.05*height, '%d'%int(height),ha='center', va='bottom')
    def start(self):
        
        self.addX([20, 35, 30, 35, 27])
        self.addX   ([2, 3, 4, 1, 2])
        
        rects1 = self.ax.bar(self.ind, liX[0], self.width, color='r', yerr=liX[1])
        
        self.addY ([25, 32, 34, 20, 25])
        self.addY ([3, 5, 2, 3, 3])
        rects2 = self.ax.bar(self.ind+self.width, self.liX[0], self.width, color='y', yerr=self.liY[1])
        
        # add some
        self.ax.set_ylabel('Scores')
        self.ax.set_title('Scores by group and gender')
        self.ax.set_xticks(ind+width)
        self.ax.set_xticklabels( ('G1', 'G2', 'G3', 'G4', 'G5') )

        self.ax.legend( (rects1[0], rects2[0]), ('Men', 'Women') )
        self.show()
        

    def show(self):
        self.autolabel(rects1)
        self.autolabel(rects2)

        plt.show()
        
ch = chart()
ch.start()
