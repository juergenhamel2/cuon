#include <gnome.h>


void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_delete1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_newPurchase1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_savePurchase1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_clearPurchase1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_printPurchase1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data);
