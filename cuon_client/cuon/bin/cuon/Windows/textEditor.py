# -*- coding: utf-8 -*-

##Copyright (C) [2011-2012]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

import sys
from types import *
try:
    import pygtk
    pygtk.require('2.0')
    import gtk
#import gtk.glade
except:
    pass
import cPickle
import cuon.TypeDefs
from cuon.TypeDefs.defaultValues import defaultValues
import os
import os.path
import types
try:
    import pango
except:
    pass

try:
    import pygments
    from pygments.lexers import get_lexer_for_mimetype
    from pygments.lexers import TextLexer, IniLexer
    from pygments.styles import get_style_by_name
    from pygments.token import STANDARD_TYPES, Token
    from pygments.lexer import RegexLexer, bygroups
    
    from pygments.lexers import PythonLexer
    from pygments.token import *
    from pygments.formatters import *
    from pygments import highlight
    from pygments.formatter import Formatter
except:
    print 'no pygments found,  please install it first'
try:    
    STANDARD_TOKENS = STANDARD_TYPES.keys()

    tag_name = lambda sn, token: sn + '_' + str(token).replace('.', '_').lower()
except:
    pass

  
class textEditor(defaultValues):
    def __init__(self):
        defaultValues.__init__(self) 
        self.pygLexer = None
        self.textbufferMisc = None
        self.viewMisc = None
        self.hl_style = None
        self._generated_styles = set()
        
        
    def getEditor(self,  mime_type =  'text/x-tex',  highlight = True):

        
        
        
        try:
            self.textbufferMisc = gtk.TextBuffer(None)
        except:
            self.textbufferMisc = gtk.TextBuffer()
            
        self.View = gtk.TextView()
        self.View.set_wrap_mode(gtk.WRAP_WORD)
        self._changehandler = self.textbufferMisc.connect_after("changed", self._on_change)

        self.textbufferMisc = self.choose_lexer(self.textbufferMisc, mime_type)
        
        self.View.set_buffer(self.textbufferMisc)
        return self.textbufferMisc,  self.View
        
    def set_style(self, style):
        oldstyle = self.hl_style
        if isinstance(style, basestring):
            style = get_style_by_name(style)
        
        if style not in self._generated_styles:
            self._generate_tags(style)
        
        self.hl_style = style
        self.style_name = style.__name__
        
        bg_color = style.background_color
        self.View.modify_base(gtk.STATE_NORMAL, gtk.gdk.color_parse(bg_color))
        fg_style = style.style_for_token(Token.Name)
        fg_color = '#' + (fg_style['color'] or '000')
        self.View.modify_text(gtk.STATE_NORMAL, gtk.gdk.color_parse(fg_color)) 
      
      
    def _generate_tags(self, hl_style):
        buf = self.textbufferMisc
        
        style_name = hl_style.__name__
        for token in STANDARD_TOKENS:
            name = tag_name(style_name, token)
            style = hl_style.style_for_token(token)
            try:
                tag = buf.create_tag(name)
           
                if style['bgcolor']:
                    tag.set_property('background', '#' + style['bgcolor'])
                if style['color']:
                    tag.set_property('foreground', '#' + style['color'])
                if style['bold']:
                    tag.set_property('weight', pango.WEIGHT_BOLD)
                if style['italic']:
                    tag.set_property('style', pango.STYLE_ITALIC)
                if style['underline']:
                    tag.set_property('underline', pango.UNDERLINE_SINGLE)
            except:
                pass
        
    def choose_lexer(self, buf,  mime_type, bHighlight = True):
        formatter = None
        if mime_type in [ "text/plain","text/x-ini"]:
            self.pygLexer = myIniLexer(encoding='utf-8')
            
        else:
            
            self.pygLexer =  get_lexer_for_mimetype(mime_type)
            
            
        ''' formatter: BBCodeFormatter BmpImageFormatter GifImageFormatter HtmlFormatter ImageFormatter JpgImageFormatter LatexFormatter NullFormatter RawTokenFormatter RtfFormatter SvgFormatter Terminal256Formatter TerminalFormatter '''
        
        if mime_type in ["text/html"]:
            formatter = HtmlFormatter(noclasses=True, nowrap=False, encoding = "utf-8")
        elif mime_type in ["text/plain", "text/x-ini"]:
            print "text/plain formatter"
            formatter = NullFormatter(noclasses=True, encoding = "utf-8")
        else:
            formatter = HtmlFormatter(noclasses=True, encoding = "utf-8")
        
        self.set_style('default')
        self.set_lexer(buf,  self.pygLexer)
        buf.set_text( highlight(self.get_text(buf), self.pygLexer, formatter))
        return buf
        
        
    def set_lexer(self,buf,  lexer):
        
        self.lexer = lexer
        self.rehighlight(buf)    
        
    def get_text(self, textbufferMisc):
        "Returns the current text in the text area"
        #print "text = ",  self.textbufferMisc.get_text(self.textbufferMisc.get_start_iter(), self.textbufferMisc.get_end_iter())
        return textbufferMisc.get_text(textbufferMisc.get_start_iter(), textbufferMisc.get_end_iter(), 1)
        
    def _on_change(self, buf):
        buf.handler_block(self._changehandler)
        self.rehighlight(buf)
        buf.handler_unblock(self._changehandler)
    
    def rehighlight(self,  buf):
        start = buf.get_start_iter()
        end = buf.get_end_iter()
        text = self.get_text(buf)
        buf.remove_all_tags(start, end)
        end =buf.get_start_iter()
        style_name = self.style_name
        for token, value in self.pygLexer.get_tokens(text):
            ln = len(value)
            end.forward_cursor_positions(ln)
            tag = tag_name(style_name, token)
            buf.apply_tag_by_name(tag, start, end)
            start.forward_cursor_positions(ln)
    
    
class myIniLexer(IniLexer):
   
    name = 'INI'
    aliases = ['ini', 'cfg']
    filenames = ['*.ini', '*.cfg']
    encoding = "utf-8"
    tokens = {
        'root': [
            (r'\s+', Text),
            (r'#.*?$', Comment),
            (r'\[.*?\]$', Keyword),
            (r'(.*?)(\s*)(=)(\s*)(.*?)$', bygroups(Name.Attribute, Text, Operator, Text, String)), 
            (r'(.*?)(\s*)(:)(\s*)(.*?)$', bygroups(Name.Attribute, Text, Operator, Text, String))
        ]
    }




class MyTxtFormatter(Formatter):
    def __init__(self, **options):
        Formatter.__init__(self, **options)
        
    def format(self, tokensource, outfile):
        for ttype, value in tokensource:
            outfile.write(value)
