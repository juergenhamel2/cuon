# -*- coding: utf-8 -*-
##Copyright (C) [2003]  [Juergen Hamel, D-32584 Loehne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

import sys, time
from types import *
import pygtk
pygtk.require('2.0')
import gtk
#import gtk.glade
import gobject
#from gtk import True, False
import string
import locale
from locale import gettext as _


from cuon.Databases.SingleData import SingleData
import SingleOrder
import SingleOrderSupply
import SingleOrderGet
import SingleOrderPosition
import SingleOrderPayment
import SingleOrderMisc


import logging
from cuon.Windows.chooseWindows  import chooseWindows
import cuon.Articles.articles
import cuon.Addresses.addresses
import cuon.Addresses.SingleAddress
import cuon.Addresses.SinglePartner

import cuon.Articles.SingleArticle
import cuon.Order.standard_invoice
import cuon.Order.standard_delivery_note
import cuon.Order.standard_pickup_note
import cuon.PrefsFinance.prefsFinance
import cuon.Finances.SingleAccountInfo
import cuon.PrefsFinance.prefsFinance
import cuon.PrefsFinance.SinglePrefsFinanceTop
import cuon.Order.SingleOrderInvoice
import cuon.DMS.dms
import cuon.DMS.SingleDMS
import cuon.DMS.documentTools

import cuon.PrefsFinance.SinglePrefsFinanceVat
from cuon.Articles.ArticlesFastSelection import  ArticlesFastSelection
import cuon.Project.project
import cuon.Project.SingleProject
import cuon.Staff.staff 
import cuon.Staff.SingleStaff
import cuon.Misc.misc
import cuon.Misc.messages
import cuon.Misc.cuon_dialog


class orderwindow(chooseWindows,  ArticlesFastSelection):
    """
    @author: Juergen Hamel
    @organization: Cyrus-Computer GmbH, D-32584 Loehne
    @copyright: by Juergen Hamel
    @license: GPL ( GNU GENERAL PUBLIC LICENSE )
    @contact: jh@cyrus.de
    """
    
    
    def __init__(self, allTables, dicOrder=None,  newOrder = False, orderid = 0,OrderType='Order'):

        chooseWindows.__init__(self)
        ArticlesFastSelection.__init__(self)
        
        self.dicOrder = dicOrder
        self.fillArticlesNewID = 0
        self.cashUser = False
        self.cashDesk = None
        self.cashDeskPrinter=None
        self.dicUser["short_key"]="NONE"
        self.dicUser["cash_desk_number"]= 0
        self.iTogglePrice = 0
        self.iDefaultTogglePrice = 0
        if orderid != -555:
            
            # check User cash system
            for i in ["startCash1","startCash2", "startCash3" ]:
                print "param = ",  self.oUser.Params[i]
                if self.oUser.Params[i]:
                    print "check the rights"
                    
                    if self.rpc.callRP("User.getOrderCashInfo", i, self.dicUser):
                        self.cashUser = True
                        self.cashDesk = i
                        self.cashDeskCustomerID = self.rpc.callRP("User.getOrderCashDeskCustomer", i, self.dicUser)
                        self.cashDeskPrinter =  self.rpc.callRP("User.getOrderCashDeskPrinter", i, self.dicUser)
                        print "cashDeskPrinter = ",  self.cashDeskPrinter
                        
                        if not self.cashDeskPrinter:
                            self.cashDeskPrinter = "NONE"
                            
                        self.dicUser['cash_printer'] = self.cashDeskPrinter
                        print "customerAddressid = ", self.cashDeskCustomerID
                    
             
            if self.cashUser:
                self.loadGlade('cash_desk.xml','OrderMainwindow')
                self.disable_cash_desk_menu()

                self.ModulNumber = self.MN['Order_cash_desk']
                self.iDefaultTogglePrice =  self.rpc.callRP("Order.getOrderCashDeskDefaultPriceGroup", self.cashDesk, self.dicUser)
                self.iTogglePrice =  self.iDefaultTogglePrice
                self.setPriceGroupRadioButton()
            else:
                self.ModulNumber = self.MN['Order']
                self.loadGlade('order.xml','OrderMainwindow')
            #self.win1 = self.getWidget('OrderMainwindow')
            self.eResidue= self.getWidget("ePaymentResidue")

            self.FastSelectionStart()
        self.allTables = allTables
        self.singleOrder = SingleOrder.SingleOrder(allTables)
        self.singleOrderInfo = SingleOrder.SingleOrder(allTables)
        self.singleOrderSupply = SingleOrderSupply.SingleOrderSupply(allTables)
        self.singleOrderGet = SingleOrderGet.SingleOrderGet(allTables)
        self.singleOrderPosition = SingleOrderPosition.SingleOrderPosition(allTables)
        self.singleOrderMisc = SingleOrderMisc.SingleOrderMisc(allTables)
        self.singleAddress = cuon.Addresses.SingleAddress.SingleAddress(allTables)
        self.singlePartner = cuon.Addresses.SinglePartner.SinglePartner(allTables)
        self.singleOrderPayment = SingleOrderPayment.SingleOrderPayment(allTables)
        self.singleAccountInfo =cuon.Finances.SingleAccountInfo.SingleAccountInfo(allTables)
        self.singlePrefsFinanceTop = cuon.PrefsFinance.SinglePrefsFinanceTop.SinglePrefsFinanceTop(allTables)
        self.singlePrefsFinanceVat = cuon.PrefsFinance.SinglePrefsFinanceVat.SinglePrefsFinanceVat(allTables)
        self.singleOrderInvoice = cuon.Order.SingleOrderInvoice.SingleOrderInvoice(allTables)
        self.singleProject = cuon.Project.SingleProject.SingleProject(allTables)
        self.singleStaff = cuon.Staff.SingleStaff.SingleStaff(allTables)
        
        self.singleDMS = cuon.DMS.SingleDMS.SingleDMS(allTables)
        self.documentTools = cuon.DMS.documentTools.documentTools()
        
        self.singleArticle = cuon.Articles.SingleArticle.SingleArticle(allTables)
      
        if orderid != -555:
            # self.singleOrder.loadTable()
            if self.cashUser:      
                 self.EntriesOrder = 'cash_desk.xml'
            else:
                 self.EntriesOrder = 'order.xml'
             
            self.EntriesOrderSupply = 'order_supply.xml'
            self.EntriesOrderGet = 'order_get.xml'
            self.EntriesOrderPosition = 'order_position.xml'
            self.EntriesOrderMisc = 'order_misc.xml'
            self.EntriesOrderInvoice = 'order_invoice.xml'
            self.EntriesOrderPayment = 'order_inpayment.xml'
            self.EntriesOrderAdditionalInfo = 'order_additional_info.xml'
            
            
           
            #singleOrder
            
            self.loadEntries(self.EntriesOrder)
            self.singleOrder.setEntries(self.getDataEntries(self.EntriesOrder) )
            self.singleOrder.setGladeXml(self.xml)
            liFields = self.rpc.callRP('Misc.getTreeInfo', "orderbook",self.dicUser)
            
            print "liFields = ",  liFields
            print "setStore = ",self.getListStore(liFields[2])
            if liFields:
                self.liSearchFields = liFields[0]
                self.singleOrder.setTreeFields(liFields[1])
                self.singleOrder.setStore(self.getListStore(liFields[2]))
                self.singleOrder.setListHeader(liFields[3])
                
                self.singleOrder.setTreeOrder(liFields[4][0])
            else:
                
               self.liSearchFields = [ ['eFindOrderNumber', 'STRING', 'number'],['eFindOrderID', 'INT', 'id = '],  ['eFindOrderDesignation', 'STRING', 'designation'] , ['eFindOrderInvoiceNumber', 'STRING','###id = (select order_number from list_of_invoices where invoice_number = %FIELD%)' ], ['eFindOrderYear', 'STRING', 'date_part(orderedat,year)'],['cbFindOrderTypes','CHECKBOX','###(select * from fct_getUnreckonedOrder(id)) ']]

               self.singleOrder.setTreeFields( ['number', 'designation','id'] )
               self.singleOrder.setStore( gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_UINT) ) 
           
               self.singleOrder.setListHeader([_('number'), _('designation'),_("ID") ])
               self.singleOrder.setTreeOrder('number')


            self.singleOrder.setTree(self.getWidget('tree1') )
            self.singleOrder.sWhere  ='where process_status between 500 and 599'
            
            #singleOrderInfo (syn for singleOrder )
            self.loadEntries(self.EntriesOrderAdditionalInfo)
            self.singleOrderInfo.setEntries(self.getDataEntries(self.EntriesOrder) )
            self.singleOrderInfo.setGladeXml(self.xml)
            
             #singleOrderSupply
            
            self.loadEntries(self.EntriesOrderSupply)
            self.singleOrderSupply.setEntries(self.getDataEntries('order_supply.xml') )
            self.singleOrderSupply.setGladeXml(self.xml)
            self.singleOrderSupply.setTreeFields( ['designation',"id" ] )
            self.singleOrderSupply.setStore( gtk.ListStore(gobject.TYPE_STRING,  gobject.TYPE_UINT) ) 
            self.singleOrderSupply.setTreeOrder('designation')
            self.singleOrderSupply.setListHeader([_('Designation'),_("ID") ])
    
            self.singleOrderSupply.sWhere  ='where ordernumber = ' + `self.singleOrder.ID`
            self.singleOrderSupply.setTree(self.getWidget('tree1') )
      
            #singleOrderGet
            
            self.loadEntries(self.EntriesOrderGet)
            self.singleOrderGet.setEntries(self.getDataEntries('order_get.xml') )
            self.singleOrderGet.setGladeXml(self.xml)
            self.singleOrderGet.setTreeFields( ['designation',"id"] )
            self.singleOrderGet.setStore( gtk.ListStore(gobject.TYPE_STRING,  gobject.TYPE_UINT) ) 
            self.singleOrderGet.setTreeOrder('designation')
            self.singleOrderGet.setListHeader([_('Designation'),_("ID")])
    
            self.singleOrderGet.sWhere  ='where orderid = ' + `self.singleOrder.ID`
            self.singleOrderGet.setTree(self.getWidget('tree1') )
    
            # singlePositions
            
            self.loadEntries(self.EntriesOrderPosition)
            self.singleOrderPosition.setEntries(self.getDataEntries(self.EntriesOrderPosition) )
            self.singleOrderPosition.setGladeXml(self.xml)
            self.singleOrderPosition.setTreeFields( ['position','amount','articleid', 'articles.number as arnumber', "fct_getValueAsCurrency(fct_getPositionSingleNetPrice(orderid, orderposition.id, '"+OrderType + "')) as netprice","fct_getValueAsCurrency(fct_getPositionSingleNetPrice(orderid, orderposition.id, '"+OrderType + "') * amount) as total", 'articles.designation as ardesignation', 'orderposition.designation as designation2',"id"] )
            self.singleOrderPosition.setStore( gtk.ListStore(gobject.TYPE_UINT, gobject.TYPE_FLOAT,  gobject.TYPE_UINT ,gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_STRING , gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_UINT) ) 
            self.singleOrderPosition.setTreeOrder('position,articleid')
            self.singleOrderPosition.setListHeader([_('Pos.'),_('Amount'),_('Article-ID'), _('Number'),_('Net Price'), _('total sum'), _('Designation'),_('Designation2'),_("ID")])
    
            self.singleOrderPosition.sWhere  ='where orderid = ' + `self.singleOrder.ID` + ' and articleid = articles.id '
            self.singleOrderPosition.setTree(self.getWidget('tree1') )
      
            
            self.loadEntries(self.EntriesOrderMisc)
            
            # singleOrderInvoice
            self.loadEntries(self.EntriesOrderInvoice)
            self.singleOrderInvoice.sWhere  ='where orderid = ' + `self.singleOrder.ID`
            self.singleOrderInvoice.setEntries(self.getDataEntries(self.EntriesOrderInvoice) )
            self.singleOrderInvoice.setGladeXml(self.xml)
            self.singleOrderInvoice.setTreeFields(["id"])
            self.singleOrderInvoice.setTreeOrder('id')
            self.singleOrderInvoice.setTree(self.getWidget('tree1') )
            
             #singleOrderMisc
            
            self.loadEntries(self.EntriesOrderMisc)
            self.singleOrderMisc.setEntries(self.getDataEntries('order_misc.xml') )
            self.singleOrderMisc.setGladeXml(self.xml)
            self.singleOrderMisc.setTreeFields( ['designation',"id"] )
            self.singleOrderMisc.setStore( gtk.ListStore(gobject.TYPE_STRING,  gobject.TYPE_UINT) ) 
            self.singleOrderMisc.setTreeOrder('designation')
            self.singleOrderMisc.setListHeader([_('Designation'),_("ID")])
    
            self.singleOrderMisc.sWhere  ='where orderid = ' + `self.singleOrder.ID`
            self.singleOrderMisc.setTree(self.getWidget('tree1') )
    
            # singleOrderPayment
            
            self.loadEntries(self.EntriesOrderPayment)
            self.singleOrderPayment.setEntries(self.getDataEntries(self.EntriesOrderPayment) )
            self.singleOrderPayment.setGladeXml(self.xml)
            self.singleOrderPayment.setTreeFields( ['date_of_paid','invoice_number','inpayment','account_id',"id"] )
            self.singleOrderPayment.setStore( gtk.ListStore(gobject.TYPE_STRING, gobject.TYPE_STRING, gobject.TYPE_FLOAT, gobject.TYPE_STRING, gobject.TYPE_UINT) ) 
            self.singleOrderPayment.setTreeOrder('date_of_paid desc,id desc')
            self.singleOrderPayment.setListHeader([_('Date'),_('Invoice'),_('Inpayment'),_('account'),_("ID")])
    
            self.singleOrderPayment.sWhere  ='where order_id = ' + `self.singleOrder.ID`
            self.singleOrderPayment.setTree(self.getWidget('tree1') )
      
            # Menu-items
            self.initMenuItems()
            
            # Close Menus for Tab
    
            self.addEnabledMenuItems('tabs','order1')
            self.addEnabledMenuItems('tabs','supply1')
            self.addEnabledMenuItems('tabs','gets1')
            self.addEnabledMenuItems('tabs','position1')
            self.addEnabledMenuItems('tabs','invoice1')
            self.addEnabledMenuItems('tabs','misc1')
            self.addEnabledMenuItems('tabs','payments1')
    
    
            # seperate Menus
            self.addEnabledMenuItems('order','order1')
            self.addEnabledMenuItems('supply','supply1')
            self.addEnabledMenuItems('gets','gets1')
            self.addEnabledMenuItems('positions','position1')
            self.addEnabledMenuItems('payment','payments1')
            self.addEnabledMenuItems('invoice','invoice1')
            self.addEnabledMenuItems('misc','misc1')
            
    
            
            # enabledMenues for Order
            self.addEnabledMenuItems('editOrder','new1', self.dicUserKeys['new'])
            self.addEnabledMenuItems('editOrder','edit1', self.dicUserKeys['edit'])
            self.addEnabledMenuItems('editOrder','delete1', self.dicUserKeys['delete'])
            self.addEnabledMenuItems('editOrder','print1', self.dicUserKeys['print'])
    
            # enabledMenues for Supply
            self.addEnabledMenuItems('editSupply','SupplyNew1', self.dicUserKeys['new'])
            self.addEnabledMenuItems('editSupply','SupplyEdit1', self.dicUserKeys['edit'])
            self.addEnabledMenuItems('editSuppy','SupplyDelete1', self.dicUserKeys['delete'])
        
           # enabledMenues for Gets
            self.addEnabledMenuItems('editGets','GetsNew1', self.dicUserKeys['new'])
            self.addEnabledMenuItems('editGets','GetsEdit1', self.dicUserKeys['edit'])
            self.addEnabledMenuItems('editGets','GetsDelete1', self.dicUserKeys['delete'])
    
            # enabledMenues for Positions
            self.addEnabledMenuItems('editPositions','PositionNew1', self.dicUserKeys['new'])
            self.addEnabledMenuItems('editPositions','PositionEdit1', self.dicUserKeys['edit'])
            self.addEnabledMenuItems('editPositions','PositionDelete1', self.dicUserKeys['delete'])
            # enabledMenues for Invoice
            self.addEnabledMenuItems('editInvoice','InvoiceEdit1', self.dicUserKeys['edit'])
    
            # enabledMenues for Misc
            self.addEnabledMenuItems('editMisc','MiscEdit', self.dicUserKeys['edit'])
            
            
            
            # enabledMenues for Payment
            self.addEnabledMenuItems('editPayment','payment_new', self.dicUserKeys['new'])
            self.addEnabledMenuItems('editPayment','payment_edit', self.dicUserKeys['edit'])
    
            # to misc menu
            
            # enabledMenues for Save 
            self.addEnabledMenuItems('editSave','save1', self.dicUserKeys['save'])
            self.addEnabledMenuItems('editSave','SupplySave1', self.dicUserKeys['save'])
            self.addEnabledMenuItems('editSave','GetsSave1', self.dicUserKeys['save'])
            self.addEnabledMenuItems('editSave','PositionSave1', self.dicUserKeys['save'])
            self.addEnabledMenuItems('editSave','InvoiceSave1', self.dicUserKeys['save'])
            self.addEnabledMenuItems('editSave','MiscSave', self.dicUserKeys['save'])
            self.addEnabledMenuItems('editSave','payment_save', self.dicUserKeys['save'])
    
            #Comboboxes
            
            liOrderType = [_('All'), _('Unreckoned')]
            cbTypeOfOrder = self.getWidget('cbFindOrderTypes')
            if cbTypeOfOrder:
                liststore = gtk.ListStore(str)
                for TypeOfOrder in liOrderType:
                    liststore.append([TypeOfOrder])
                cbTypeOfOrder.set_model(liststore)
                self.setComboBoxTextColumn(cbTypeOfOrder)
                cbTypeOfOrder.show()
            
            #scheduler
            liFashion, liTrade,liTurnover,liLegalform, self.liSchedulTime = self.rpc.callRP('Address.getComboBoxEntries',self.dicUser)
            cbSchedultime = self.getWidget('cbeGetsSchedulTimeBegin')
           
            if cbSchedultime:
                liststore = gtk.ListStore(str)
                if self.liSchedulTime != ['NONE'] :
                    for timeBegin in self.liSchedulTime:
                        liststore.append([timeBegin])
                    cbSchedultime.set_model(liststore)
                    self.setComboBoxTextColumn(cbSchedultime)
                    cbSchedultime.show()
                   
        # tabs from notebook
        self.tabOrder = 0
        
        self.tabSupply = 1
        self.tabGet = 2
        self.tabPosition = 3
        self.tabInvoice = 4
        self.tabMisc = 5
        self.tabPayment = 6
        self.tabInfo = 7
        
        if orderid != -555:
            self.OrderID = 0
    
            # start
            self.OrderID = orderid
            if OrderType == 'Order':
                
                if self.dicOrder and not newOrder and self.OrderID == 0:
                    print self.dicOrder
                    existOrder = self.rpc.callRP('Order.checkExistModulOrder', self.dicUser,self.dicOrder)
                    print 'existOrder = ', existOrder
                    if not existOrder or existOrder in ['NONE', 'ERROR']:
                        print '~~~~~~~~~~~~~~~~~~~~~~~~~~~~ create new'
                        self.rpc.callRP('Order.createNewOrder', self.dicUser,self.dicOrder)
                    self.singleOrder.sWhere = ' where modul_order_number = ' + `self.dicOrder['ModulOrderNumber']` + ' and modul_number = ' + `self.dicOrder['ModulNumber']`
                elif self.dicOrder and newOrder and self.OrderID == 0:
                   self.createNewOrder()
                elif self.OrderID > 0:
                    self.singleOrder.sWhere = ' where id = ' + `self.OrderID`
                    
                        
            
            
            self.win1.add_accel_group(self.accel_group)    
            self.tabChanged()
    
          
        
        # paying window
            self.wPaying = self.getWidget("Paying")
            self.wNewCustomer = self.getWidget("NewCustomerID")
            self.wAddPosition =  self.getWidget("cash_desk_position")
            self.wShortcut = self.getWidget("ShortCut")
            # check the Ordertype
            self.OrderUser = self.rpc.callRP('Order.getOrderUserType', self.dicUser['Name'])
            print 'Orderuser = ',  self.OrderUser

            #Menu File
        try:
            self.getWidget("tbPay").set_sensitive(False)
        except:
            pass
        try:
            if self.cashUser and self.dicUser["short_key"] == "NONE":
                self.wShortcut.show()
                if (self.oUser.Params["cash_user_shortkey"] and  self.oUser.Params["cash_user_pincode"]):
                    
                    self.getWidget("eCDShortCutName").set_text( self.oUser.Params["cash_user_shortkey"])
                    self.getWidget("eCDShortCutPin").set_text( self.oUser.Params["cash_user_pincode"])
                    self.on_bShortCutOK_clicked(None)

        except:
            pass

        try:
            if self.rpc.callRP('User.checkSpecialCashDeskUser', self.dicUser):
                print "User.checkSpecialCashDeskUser = True "
                self.getWidget("cash_special_menu").set_sensitive(True)
                self.getWidget("mi_PayOut").set_sensitive(True)
            else:
                print "User.checkSpecialCashDeskUser = False "
                self.getWidget("cash_special_menu").set_sensitive(False)
                self.getWidget("mi_PayOut").set_sensitive(False)
        except Exception, params:
            print Exception, params
    def on_quit1_activate(self, event):
        print "exit order v2"
        self.closeWindow()


    #Menu Order
  
    def on_save1_activate(self, event):
        print "save order v2"
        if self.cashUser:
             self.singleOrder.OrderType = 2

        else:
             self.singleOrder.OrderType = 1

        if self.singleOrder.ID == -1:
            if self.checkIfOldOrderExist() == -1:
       
            
                self.singleOrder.save()
        else:
            
            self.singleOrder.save()

        self.setEntriesEditable(self.EntriesOrder, False)   

            
        self.tabChanged()
            
         
        
    def on_new1_activate(self, event):
        print "new order v2"
       
        if self.tabOption == self.tabOrder:
            self.singleOrder.newRecord()
            self.setEntriesEditable(self.EntriesOrder, True)
        
    def on_edit1_activate(self, event):
        if self.tabOption == self.tabOrder:
            self.setEntriesEditable(self.EntriesOrder, True)
        elif self.tabOption == self.tabInfo:
            self.setEntriesEditable(self.EntriesOrderAdditionalInfo, True)

    def on_print_invoice1_activate(self, event):
        self.printInvoice("standard")

    def printInvoice(self,sType):
        dicOrder = {}
        print ' start Invoice printing'
        dicOrder['orderid'] = self.singleOrder.ID
        dicOrder['orderNumber'] = self.singleOrder.getOrderNumber(self.singleOrder.ID)
        dicOrder['invoiceNumber'] = self.rpc.callRP('Order.setInvoiceNumber', dicOrder['orderid'], self.dicUser)
        print ' start Invoice printing 2'
        invoiceNumber = self.singleOrder.getInvoiceNumber() 
        dicOrder['invoiceNumber'] =  invoiceNumber        
        print ' start Invoice printing 3'
        
        #print dicOrder
        if sType == "standard":
            Pdf = self.rpc.callRP('Report.server_order_invoice_document', dicOrder, self.dicUser)
            fname = self.showPdf(Pdf, self.dicUser,'INVOICE')
        elif sType == "cash_desk":
            print "call reportserver for cash_desk invoice"
            Pdf = self.rpc.callRP('Report.server_order_cash_desk_document', dicOrder, self.dicUser)
            fname = self.showPdf(Pdf, self.dicUser,'CASH_DESK',self.cashDeskPrinter)
        elif sType == "credit_card":
            print "call reportserver for cash_desk invoice"
            Pdf = self.rpc.callRP('Report.server_order_cash_desk_credit_card', dicOrder, self.dicUser)
            fname = self.showPdf(Pdf, self.dicUser,'CASH_DESK',self.cashDeskPrinter)
        else:
            Pdf = self.rpc.callRP('Report.server_order_invoice_document', dicOrder, self.dicUser)
            fname = self.showPdf(Pdf, self.dicUser,'INVOICE')
       
        print "Now call ticket"
        ok = self.rpc.callRP('Finances.createTicketFromInvoice',invoiceNumber,self.dicUser)
        # insert invoice into dms 
        self.documentTools.importDocument(self.singleDMS,self.dicUser,fname)
        self.singleDMS.ModulNumber = self.MN['Order']
        self.singleDMS.sep_info_1 = self.singleOrder.ID    
        self.singleDMS.newRecord()
        self.singleDMS.newDate = self.getActualDateTime()['date']
        self.singleDMS.newTitle = _('invoice') + ' ' + `invoiceNumber`
        print self.singleDMS.newDate
        self.singleDMS.newCategory = _('payments')
        self.singleDMS.Rights = 'INVOICE'
        
        newID = self.singleDMS.save(['document_image'])
        
    def printCashJournalAccount(self,sType):
        dicOrder = {}
        print ' start journal printing'
       
        #print ' start Journal printing 2'
           
        # print ' start Journal printing 3'
        
        #print dicOrder
       
        print "call reportserver for cash_desk journal"
        Pdf = self.rpc.callRP('Report.server_order_cash_desk_journal', dicOrder, self.dicUser)
        fname = self.showPdf(Pdf, self.dicUser,'CASH_DESK',self.cashDeskPrinter)
       
       
        #print "Now call ticket"
        #ok = self.rpc.callRP('Finances.createTicketFromInvoice',invoiceNumber,self.dicUser)
        ## insert invoice into dms 
        #self.documentTools.importDocument(self.singleDMS,self.dicUser,fname)
        #self.singleDMS.ModulNumber = self.MN['Order']
        #self.singleDMS.sep_info_1 = self.singleOrder.ID    
        #self.singleDMS.newRecord()
        #self.singleDMS.newDate = self.getActualDateTime()['date']
        #self.singleDMS.newTitle = _('invoice') + ' ' + `invoiceNumber`
        #print self.singleDMS.newDate
        #self.singleDMS.newCategory = _('payments')
        #self.singleDMS.Rights = 'INVOICE'
        
        #newID = self.singleDMS.save(['document_image'])    

    def on_all_open_invoice1_activate(self, event):
        
        if self.QuestionMsg('All new invoices are printed ! Do you wish this ? Really ?'):
            oldID = self.singleOrder.ID
            
            
            liOrder = self.rpc.callRP('Order.getAllOrderWithoutInvoice',self.dicUser)
            if liOrder and liOrder not in ['NONE','ERROR']:
                print 'print new Invoices '
                for newID in liOrder:
                    print newID
                    self.singleOrder.load(newID)
                    self.on_print_invoice1_activate(None)
                    
                    
        self.singleOrder.load(oldID)    
        
    def on_print_delivery_note1_activate(self, event):
        sTo = [self.singleAddress.getEmail()]
        
        print 'delivery note'
        dicOrder = {}
        dicOrder['orderNumber'] = self.singleOrder.getOrderNumber(self.singleOrder.ID)
        #dicOrder['deliveryNumber'] =  self.singleOrderSupply.getDeliveryNumber(self.singleOrder.ID)        
        #invoice = cuon.Order.standard_delivery_note.standard_delivery_note(dicOrder)
              
             

        
        dicOrder['orderid'] = self.singleOrder.ID
        
        deliveryNumber = self.singleOrder.getSupplyNumber() 
        dicOrder['deliveryNumber'] =  deliveryNumber              
        print ' start delievery printing 3'
        
        print dicOrder
        
        Pdf = self.rpc.callRP('Report.server_order_supply_document', dicOrder, self.dicUser)
        fname = self.showPdf(Pdf, self.dicUser,'SUPPLY')
#        ok = self.rpc.callRP('Finances.createTicketFromInvoice',invoiceNumber,self.dicUser)
        # insert supply into dms 
        self.documentTools.importDocument(self.singleDMS,self.dicUser,fname)
        self.singleDMS.ModulNumber = self.MN['Order']
        self.singleDMS.sep_info_1 = self.singleOrder.ID    
        self.singleDMS.newRecord()
        self.singleDMS.newDate = self.getActualDateTime()['date']
        self.singleDMS.newTitle = _('delivery') + ' ' + `deliveryNumber`
        print self.singleDMS.newDate
        self.singleDMS.newCategory = _('delivery')
        self.singleDMS.Rights = 'DELIVERY'
        
        newID = self.singleDMS.save(['document_image'])
        dicVars = {}
        dicVars['sm'] = self.getOrderInfo()
        for key in dicOrder.keys():
            dicVars['sm']['delivery_' + key] = dicOrder[key]
            
        #print 'dicVars[sm] = ',  dicVars['sm']
        
        #print 'send email ok ',  newID,  self.dicUser['Email']['sendSupply']
        if self.dicUser['Email']['sendSupply'] and sTo[0]:
            
            cmm = cuon.Misc.messages.messages()
            if cmm.QuestionMsg(_('Would you send it as email ?')):
                self.singleDMS.load(newID)
                cme = cuon.Misc.misc.sendAsEmail()
                cme.sendNormal('Supply', sTo,  self.dicUser,  self.singleDMS,  dicVars)
        
             
    def on_print_pickup_note1_activate(self, event):
        print 'pickup note'
        
        dicOrder = {}
        dicOrder['ordernumber'] = self.singleOrder.getOrderNumber(self.singleOrder.ID)
        dicOrder['pickupNumber'] =  self.singleOrderGet.getPickupNumber(self.singleOrder.ID)
        dicOrder['addressNumber'] = self.singleOrderGet.getAddressNumber(self.singleOrderGet.ID)
        print 'Addressnumber by Orderget = ' + `dicOrder['addressNumber']`
        dicOrder['partnerNumber'] = self.singleOrderGet.getPartnerNumber(self.singleOrderGet.ID)
        print 'partnernumber by Orderget = ' + `dicOrder['partnerNumber']`
        dicOrder['forwardingAgencyNumber'] = self.singleOrderGet.getForwardingAgencyNumber(self.singleOrderGet.ID)
        print 'ForwardAgencynumber by Orderget = ' + `dicOrder['forwardingAgencyNumber']`
        dicOrder['contactPersonNumber'] = self.singleOrderGet.getContactPersonNumber(self.singleOrderGet.ID)
        print 'ContactPersonnumber by Orderget = ' + `dicOrder['contactPersonNumber']`
              
                
        
        pdf = cuon.Order.standard_pickup_note.standard_pickup_note(dicOrder)

##    def on_list_of_invoices1_activate(self, event):
##        dicOrder = {}
##        print ' start List of Invoices printing'
##        dicOrder['Year'] = '2007'
##        print 'dicOrder = ', dicOrder
##        
##        Pdf = self.rpc.callRP('Report.server_order_list_of_invoices', dicOrder, self.dicUser)
##        self.showPdf(Pdf, self.dicUser,'INVOICE')

    def on_delete1_activate(self, event):
        print "delete order v2"
        self.singleOrder.deleteRecord()

 
    def on_DMS_activate(self, event):
        print 'show ext. Infos '
        dms = cuon.DMS.dms.dmswindow(self.allTables,self.MN['Order'], {'1':self.singleOrder.ID})
        
    def on_order_dup_activate(self, event):
        print "order dup"
        newID = self.rpc.callRP('Order.dup_order',self.singleOrder.ID, self.dicUser)
        self.tabChanged()
        
    #Menu Gets
  
    def on_GetsSave1_activate(self, event):
        print "save order get v2"
        self.singleOrderGet.ordernumber = self.singleOrder.ID
        self.singleOrderGet.save()
        self.setEntriesEditable(self.EntriesOrderGet, False)   
        self.tabChanged()
         
        
    def on_GetsNew1_activate(self, event):
        print "new order v2"
        self.singleOrderGet.newRecord()
        self.setEntriesEditable(self.EntriesOrderGet, True)

    def on_GetsEdit1_activate(self, event):
        self.setEntriesEditable(self.EntriesOrderGet, True)

    def on_GetsDelete1_activate(self, event):
        print "delete order v2"
        self.singleOrderGet.deleteRecord()


 
    # Menu Supply

    def on_SupplyNew1_activate(self, event):
        self.singleOrderSupply.newRecord()
        self.setEntriesEditable(self.EntriesOrderSupply, True)

    def on_SupplyEdit1_activate(self, event):
        self.setEntriesEditable(self.EntriesOrderSupply, True)

    def on_SupplySave1_activate(self, event):
        print "save Supply v2"
        self.singleOrderSupply.ordernumber = self.singleOrder.ID
        self.singleOrderSupply.save()
        self.setEntriesEditable(self.EntriesOrderSupply, False)
        self.tabChanged()
        

    def on_SupplyDelete1_activate(self, event):
        print "delete Supply v2"
        self.singleOrderSupply.deleteRecord()

    #Menu Positions
    def on_PositionSave1_activate(self, event):
        print "save Positions v2"
        #self.getWidget('ePrice').set_text(self.getCheckedValue(self.rpc.callRP('Article.getArticlePrice', self.fillArticlesNewID,self.dicUser),'toStringFloat'))
        self.setArticlePrice('orderposition','eArticleID', 'ePrice',  self.singleOrder.ID)
        EndPosition = False
        self.singleOrderPosition.orderID = self.singleOrder.ID
        if self.singleOrderPosition.ID == -1:
            self.singleOrderPosition.TreePos = self.singleOrderPosition.TREELAST
        self.singleOrderPosition.save()
        self.setEntriesEditable(self.EntriesOrderPosition, False)
        
        self.tabChanged()
           

    def on_PositionEdit1_activate(self, event):
        print 'PositionEdit1'
        self.setEntriesEditable(self.EntriesOrderPosition, True)
    
    def on_PositionNew1_activate(self, event):
        print "new Partner articles v2"
        self.singleOrderPosition.newRecord()
        self.setEntriesEditable(self.EntriesOrderPosition, True)

    def on_PositionDelete1_activate(self, event):
        print "delete Partner articles v2"
        self.singleOrderPosition.deleteRecord()
    # Menu Invoice 
    def on_InvoiceSave1_activate(self, event):
        print "save Invoice v2"
        
        self.singleOrderInvoice.orderId = self.singleOrder.ID
        self.singleOrderInvoice.save()
        self.setEntriesEditable(self.EntriesOrderInvoice, False)
        
        #self.tabChanged()
            

    def on_InvoiceEdit1_activate(self, event):
        print 'invoiceEdit1'
        self.setEntriesEditable(self.EntriesOrderInvoice, True)
    # Menu Misc
    def on_MiscEdit_activate(self, event):
        print 'MiscEdit1'
        self.setEntriesEditable(self.EntriesOrderMisc, True)
    
    def on_MiscSave_activate(self, event):
        print "save misc v2"
        self.singleOrderMisc.Ordernumber = self.singleOrder.ID
        self.singleOrderMisc.save()
        self.setEntriesEditable(self.EntriesOrderMisc, False)
       
        self.tabChanged()
  
    #Menu Payment
    def on_payment_save_activate(self, event):
        print "save Payment v2"
       
        self.singleOrderPayment.orderID = self.singleOrder.ID
    
        self.singleOrderPayment.invoiceNumber = self.singleOrder.getInvoiceNumber()
        

        newid = self.singleOrderPayment.save()
        print 'newid = ', newid
        ok = self.rpc.callRP('Finances.createTicketFromInpayment',newid,self.dicUser)

        self.setEntriesEditable(self.EntriesOrderPayment, False)
        
        self.tabChanged()

    def on_payment_edit_activate(self, event):
        print 'PositionEdit1'
        self.setEntriesEditable(self.EntriesOrderPayment, True)
    
    def on_payment_new_activate(self, event):
        print "new Ppayment v2"
        self.singleOrderPayment.newRecord()
        self.getWidget('ePaymentInvoiceNumber').set_text(`self.singleOrder.getInvoiceNumber()`)
        
        self.setEntriesEditable(self.EntriesOrderPayment, True)


    # search button
    def on_bSearch_clicked(self, event, data=None):
        self.findData(self.singleOrder)
   
            
    def on_cbFindOrderTypes_changed(self, event):
        print "OrderType",  event
        iOrderType = self.getWidget("cbFindOrderTypes").get_active()
        self.findData(self.singleOrder)
        
    
    # TabOrder 
    def on_bSearchCustom_clicked(self, event):
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        adr.setChooseEntry('chooseAddress', self.getWidget( 'eAddressNumber'))

    # signals from entry eAddressNumber
    
    def on_eAddressNumber_changed(self, event):
        print 'eAdrnbr changed'
        if self.cashUser:
            iAdrNumber = self.getChangedValue('eAddressNumber')
            if iAdrNumber > 0:
                self.singleAddress.load(iAdrNumber)
                sAddress = self.singleAddress.getLastname() + ", " + self.singleAddress.getFirstname()
                self.getWidget("eAddressField").set_text(sAddress)
        else:
            iAdrNumber = self.getChangedValue('eAddressNumber')
            eAdrField = self.getWidget('tvAddress')
            liAdr = self.singleAddress.getAddress(iAdrNumber)
            self.setTextbuffer(eAdrField,liAdr)


    
    def on_eDiscountValue_changed(self, event):
        print 'eDiscountValue  changed'
        try:
            fDiscountValue = self.getCheckedValue(self.getWidget('eDiscountValue').get_text(),"float")
            if fDiscountValue:
                print "fDiscountValue = ",  fDiscountValue
                fTotalsum = self.getCheckedValue(self.getWidget('eTotalSum').get_text(),"float")
                print "fTotalsum = ",  fTotalsum
                fDiscountPercent = fDiscountValue*100/(fTotalsum) 
                print 'fDiscountPercent = ',  fDiscountPercent
                
                self.getWidget("eDiscount").set_text(self.getCheckedValue(fDiscountPercent, "toLocaleString") + " %")
           
        except Exception, param:
            print Exception,  param
    
    
    def on_eDiscount_key_press_event(self, entry, event):
        print 'eDiscount_key_press_event'
        if self.checkKey(event,'CTRL','K'):
            self.calcDiscount()
            
    def calcDiscount(self):
        pass
        
    # Tab Supply choose address 
    def on_bSearchSupply_clicked(self, event):
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        adr.setChooseEntry('chooseAddress', self.getWidget( 'eSupplyNumber2'))

    def on_eSupplyNumber_changed(self, event):
        print 'eSupply changed'
        iAdrNumber = self.getChangedValue('eSupplyNumber2')
        eAdrField = self.getWidget('tvSupply')
        liAdr = self.singleAddress.getAddress(iAdrNumber)
        self.setTextbuffer(eAdrField,liAdr)

        # Tab Gets  choose address 
    def on_bSearchGet_clicked(self, event):
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        adr.setChooseEntry('chooseAddress', self.getWidget( 'eGetsNumber1'))

    def on_eGetsNumber_changed(self, event):
        print 'eGets changed',  self.getChangedValue('eGetsNumber1')
        iAdrNumber = self.getChangedValue('eGetsNumber1')
        eAdrField = self.getWidget('tvGets')
        liAdr = self.singleAddress.getAddress(iAdrNumber)
        self.setTextbuffer(eAdrField,liAdr)

    def on_bSearchGetsPartner_clicked(self, event):
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        adr.setChooseEntry('chooseAddress', self.getWidget( 'eGetsPartner'))

    def on_eGetsPartner_changed(self, event):
        print 'eGetsPartner changed'
        iAdrNumber = self.getChangedValue('eGetsPartner')
        eAdrField = self.getWidget('tvbChooseDifferentBillingAddressGetsPartner')
        liAdr = self.singlePartner.getAddress(iAdrNumber)
        self.setTextbuffer(eAdrField,liAdr)

  


    def on_bSearchForwardingAgency_clicked(self, event):
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        adr.setChooseEntry('chooseAddress', self.getWidget( 'eForwardingAgency'))

    def on_eForwardingAgency_changed(self, event):
        print 'eForwardingAgency changed'
        iAdrNumber = self.getChangedValue('eForwardingAgency')
        eAdrField = self.getWidget('tvForwardingAgency')
        liAdr = self.singleAddress.getAddress(iAdrNumber)
        self.setTextbuffer(eAdrField,liAdr)


    def on_bContactPerson_clicked(self, event):
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        adr.setChooseEntry('chooseAddress', self.getWidget( 'eContactPerson'))

    def on_eContactPerson_changed(self, event):
        print 'eContactPerson changed'
        iAdrNumber = self.getChangedValue('eContactPerson')
        eAdrField = self.getWidget('tvContactPerson')
        liAdr = self.singlePartner.getAddress(iAdrNumber)
        self.setTextbuffer(eAdrField,liAdr)
        
        
    ## Invoice specs
    
    # Tax Vat choose
    def on_bTaxVatForAllPositions_clicked(self, event):
        print 'cbVat search'
        print event
        
        pf = cuon.PrefsFinance.prefsFinance.prefsFinancewindow(self.allTables)
        pf.setChooseEntry('chooseTaxVat', self.getWidget( 'eTaxVatForAllPositionsID'))
        
    def on_eTaxVatForAllPositionsID_changed(self, event):
        print 'eCategory changed'
        iTaxVat = self.getChangedValue('eTaxVatForAllPositionsID')
        sTaxVat = self.singlePrefsFinanceVat.getNameAndDesignation(iTaxVat)
        if sTaxVat:
            self.getWidget('eTaxVatForAllPositionsText').set_text(sTaxVat)
        else:
            self.getWidget('eTaxVatForAllPositionsText').set_text('')
 
    # different billing Address
    
    
    def on_bChooseDifferentBillingAddress_clicked(self, event):
        '''bChooseDifferentBillingAddress is clicked, choose a new address from addresswindow'''
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        adr.setChooseEntry('chooseAddress', self.getWidget( 'eDifferentBillingAddressID'))

    # signals from entry eAddressNumber
    
    def on_eDifferentBillingAddressID_changed(self, event):
        ''' eDifferentBillingAddressID has changed, search for the corresponded address and display it at textwindow'''
        iAdrNumber = self.getChangedValue('eDifferentBillingAddressID')
        eAdrField = self.getWidget('tvDifferentBillingaddress')
        liAdr = self.singleAddress.getAddress(iAdrNumber)
        self.setTextbuffer(eAdrField,liAdr)
     

        # staff search
    def on_bSearchStaff_clicked(self, event):
        staffw = cuon.Staff.staff.staffwindow(self.allTables)
        staffw.setChooseEntry('chooseStaff', self.getWidget( 'eStaffNumber'))

    def on_eStaffNumber_changed(self, event):
        print 'eStaffNumber changed'
        iStaffNumber = self.getChangedValue('eStaffNumber')
        if iStaffNumber > 0:
            eStaffField = self.getWidget('eStaffName')
            eStaffField.set_text(self.singleStaff.getFullName(iStaffNumber) )
    

        # Tab Positions choose article 
    def on_bArticleSearch_clicked(self, event):
        ar = cuon.Articles.articles.articleswindow(self.allTables)
        ar.setChooseEntry('chooseArticle', self.getWidget( 'eArticleID'))

                           

    def on_eArticleID_changed(self, event):
        print 'eArticle changed'
        iArtNumber = self.getChangedValue('eArticleID')
        eArtField = self.getWidget('tvArticle')
        liArt = self.singleArticle.getArticle(iArtNumber)
        self.setTextbuffer(eArtField,liArt)
        record = self.singleArticle.getFirstRecord()
        if record:
            print record
            self.getWidget('eOrderPositionsUnit').set_text(record['unit'])
            
        if self.singleOrderPosition.ID == -1 and record:
            self.getWidget('eOrderPositionsTaxVat').set_text(record['tax_vat'])
          
   
    
    
    
    def on_bPaymentSearchAccount_clicked(self, event):
        acc = cuon.PrefsFinance.prefsFinance.prefsFinancewindow(self.allTables, preparedTab = 3 )
        acc.setChooseEntry('choose_acct1', self.getWidget( 'ePaymentAccountID'))

                           

    def on_ePaymentAccountID_changed(self, event):
        print 'eAccountID changed'
        iAcctNumber = self.getChangedValue('ePaymentAccountID')
        eAcctField = self.getWidget('eAccountDesignation')
        cAcct = self.singleAccountInfo.getInfoLineForID(iAcctNumber)
        if cAcct and cAcct not in ['NONE','ERROR']:
            eAcctField.set_text(cAcct)
        
##        record = self.singleArticle.getFirstRecord()
##        if record:
##            print record
##            self.getWidget('eOrderPositionsUnit').set_text(record['unit'])
##            
##        if self.singleOrderPosition.ID == -1 and record:
##            self.getWidget('eOrderPositionsTaxVat').set_text(record['tax_vat'])
##          
    def on_eAccountNumber_changed(self, event):
        sAcct = event.get_text()
        
        id = self.rpc.callRP('Finances.getAcctID',sAcct, self.dicUser)
        if id > 0:
            self.getWidget('ePaymentAccountID').set_text(`id`)
            
    def on_ePaymentInpayment_changed(self, event):
        self.calcNew()
    def on_ePaymentPayments_changed(self, event):
        self.calcNew()
    def ePaymentInvoiceAmount_changed(self, event):
        self.calcNew()
    def on_ePaymentCashDiscount_changed(self, event):
        self.calcNew()
    def on_ePaymentInvoiceAmount_changed(self, event):
        self.calcNew()
        
    def calcNew(self):
        
        #self.singleOrderPayment.fillOtherEntries()
        if not self.singleOrderPayment.loading:
            self.eResidue.set_text( self.rpc.callRP('Finances.getResidueSumString', self.singleOrder.ID, self.dicUser))
        
    def createSimplePayment(self, sType):
        if self.cashUser:
            self.setMainwindowNotebook('F4')
        else:
            self.setMainwindowNotebook('F7')
        self.on_payment_new_activate(None)
        print "call getTotalAmount for order id = ",  self.singleOrder.ID
        invoice_sum = self.rpc.callRP('Finances.getTotalAmount', self.singleOrder.ID, self.dicUser)
        print 'inv sum 1 ',  invoice_sum
        if invoice_sum == 0:
            invoice_sum = "0"
        else:
            invoice_sum =  self.getCheckedValue(invoice_sum,'toLocaleString')
        print 'inv sum 2 ',  invoice_sum
        self.getWidget('ePaymentCashDiscount').set_text('0.00')
        self.getWidget('ePaymentInpayment').set_text(invoice_sum)
        dicDate = self.getActualDateTime()
        print dicDate
        print invoice_sum
        self.getWidget('ePaymentDate').set_text(dicDate['date']) 
        print 'sType = ', self.dicUser['prefFinances'][sType]
        self.getWidget('eAccountNumber').set_text(self.dicUser['prefFinances'][sType])
        if 'cash' in sType:
            print 'cash found'
            self.getWidget('rPaymentCash').set_active(True)
        elif 'bank' in sType:
            print 'bank found'
            self.getWidget('rPaymentTransfer').set_active(True)
            self.getWidget('ePaymentBankRanking').set_text(sType[4])
        elif 'Debit' in sType:
            print 'debit found'
            self.getWidget('rPaymentDirectDebit').set_active(True)
            self.getWidget('ePaymentBankRanking').set_text(sType[11])
        elif 'creditCard' in sType:
            print 'creditCard found'
            self.getWidget('rPaymentCreditCard').set_active(True)
        
        self.on_payment_save_activate(None)
        self.setMainwindowNotebook('F1')

    def on_bInvoiceTOP_clicked(self, event):
        print 'choose TOP'
        top = cuon.PrefsFinance.prefsFinance.prefsFinancewindow(self.allTables, preparedTab = 1 )
        top.setChooseEntry('chooseTOP', self.getWidget( 'eInvoiceTOPID'))
        
    def on_eInvoiceTOPID_changed(self, event):
        print 'eTOPID changed'
        eTopField = self.getWidget('tvInvoiceTOP')
        try:
            liTop = self.singlePrefsFinanceTop.getTOP(long(self.getWidget( 'eInvoiceTOPID').get_text()))
            self.setTextbuffer(eTopField, liTop)
            
        except Exception,param:
            self.setTextbuffer(eTopField, ' ')
            print Exception,param
            
    def on_bSearchPartnerID_clicked(self, event):
        print 'choose partner'
        adr = cuon.Addresses.addresses.addresswindow(self.allTables)
        adr.setChooseEntry('chooseAddress', self.getWidget( 'ePartnerID'))
        
    
    def on_ePartnerID_changed(self, event):
        print 'ePartner changed'
        iAdrNumber = self.getChangedValue('ePartnerID')
        eAdrField = self.getWidget('ePartnerInfo')
        sAdr = self.singlePartner.getAddressFirstlast(iAdrNumber)
        eAdrField.set_text(sAdr)
            
    def on_bShowPartner_clicked(self, event):
        print 'bshowPartner'
        iPID = int(self.getWidget('ePartnerID').get_text())
        print 'pid = ',  iPID

        if iPID > 0:
        
            self.singlePartner.load(iPID)
            print 'addressid = ',  self.singlePartner.getAddressID()
            
            adr = cuon.Addresses.addresses.addresswindow(self.allTables, self.singlePartner.getAddressID(), iPID)
        
        
        
    # Project
    def on_bSearchProject_activate(self, event):
         self.on_bSearchProject_clicked(event)
         
         
    def on_bSearchProject_clicked(self, event):
        print 'choose project'
        adr = cuon.Project.project.projectwindow(self.allTables)
        adr.setChooseEntry('chooseProject', self.getWidget( 'eProjectID'))
        
    def on_bGotoProject_activate(self, event):    
         self.on_bGotoProject_clicked(event)
        
    def on_bGotoProject_clicked(self, event):
        adr = cuon.Project.project.projectwindow(self.allTables,project_id = int(self.getWidget( 'eProjectID').get_text() ))
        
        
    def on_bSimpleCash1_clicked(self, event):
        self.createSimplePayment('cash1')
    def on_bSimpleCash2_clicked(self, event):
        self.createSimplePayment('cash2')
       
    def on_bSimpleBank1_clicked(self, event):
        self.createSimplePayment('bank1')
    def on_bSimpleBank2_clicked(self, event):
        self.createSimplePayment('bank2')
    def on_bSimpleBank3_clicked(self, event):
        self.createSimplePayment('bank3')
    def on_bDirectDebit1_clicked(self, event):
        self.createSimplePayment('directDebit1')
    def on_bDirectDebit2_clicked(self, event):
        self.createSimplePayment('directDebit2')
      
    def on_bDirectDebit3_clicked(self, event):
        self.createSimplePayment('directDebit3')
    
    def on_bCreditCard1_clicked(self, event):
        self.createSimplePayment('creditCard1')
      
    def on_bShowExtInfo_clicked(self, event ):
        self.on_DMS_activate(event)
        
        
    def on_bQuickAppend_clicked(self, event):
        # Qick append a positions
        if self.singleOrderPosition.ID != -1:
            self.on_PositionNew1_activate(event)
        if self.getWidget('eAmount').get_text() == '':
            print 'get_text none'
            self.getWidget('eAmount').set_text('1')
        self.getWidget('eArticleID').set_text(`self.fillArticlesNewID`)
        self.getWidget('ePosition').set_text(`self.rpc.callRP('Order.getNextPosition', self.singleOrder.ID,self.dicUser)`)
        
        self.on_PositionSave1_activate(event)


    def on_eAmount_key_press_event(self, oEntry, data):
        ''' Overwrite def '''
        sKey = gtk.gdk.keyval_name(data.keyval)
        #print 'sKey : ',sKey
        if self.tabOption == self.tabPosition:
            if sKey == 'KP_Add' :
                wAmount = self.getWidget('eAmount')

                self.on_PositionEdit1_activate(None)
                if wAmount.get_text() == '':
                    wAmount.set_text('1')
                else:
                    try:
                        f1 = float(wAmount.get_text())
                        f2 = f1 + 1.000
                        print f1, f2
                        wAmount.set_text( self.getCheckedValue(f2, 'toLocaleString'))
                        print 'gesetzte zahl = ', wAmount.get_text()
                    except Exception, params:
                        print Exception, params
                self.on_PositionSave1_activate(None)        

            elif sKey == 'KP_Subtract' :
                wAmount = self.getWidget('eAmount')

                self.on_PositionEdit1_activate(None)
                if wAmount.get_text() == '':
                    wAmount.set_text('0')
                else:
                    try:
                        
                        f1 = float(wAmount.get_text())
                        f2 = f1 - 1.000
                        print f1, f2
                        wAmount.set_text( self.getCheckedValue(f2, 'toLocaleString'))
                        print 'gesetzte zahl = ', wAmount.get_text()
                    except Exception, params:
                        print Exception, params            
                self.on_PositionSave1_activate(None)        
#            else:
#                self.MainwindowEventHandling(oEntry, data)
#        
#        else:
#            self.MainwindowEventHandling(oEntry, data)
      
    # Project for this Order
    
    def on_bMiscSearchProject_clicked(self, event):
        proj = cuon.Project.project.projectwindow(self.allTables)
        proj.setChooseEntry('chooseProject', self.getWidget( 'eMiscProjectID'))

                           
    def on_tbCDNewShortcut_clicked(self, event):
         self.getWidget("tbPay").set_sensitive(False)
        
         self.wShortcut.show()

    def on_eMiscProjectID_changed(self, event):
        print ' eMiscProjectID changed'
        iProjectNumber = self.getChangedValue('eMiscProjectID')
        eProjectField = self.getWidget('tvOrderMiscProject')
        sProject = self.singleProject.getInfoForID(iProjectNumber)
        if sProject :
            self.setTextbuffer(eProjectField,  sProject)
    
    
    def on_bMiscJumpToProject_clicked(self, event):
        iProjectNumber = self.getChangedValue('eMiscProjectID')
        proj = cuon.Project.project.projectwindow(self.allTables, project_id = iProjectNumber)
   
 #de f  on_tbCreateOrder_clicked (self, event):
     #   self.on_create_order1_activate(event)
  
     # Lists 

     # Cash Desk Lists




    def checkIfOldOrderExist(self):
        self.singleOrder.ID = -1
        try:
            print "checkIfOldOrderExist addressnumber = ",  self.dicOrder['addressnumber']
            liOrder = self.rpc.callRP('Order.checkOpenOrder', `self.dicOrder['addressnumber']`,self.dicUser)
            print "liOrder = ", liOrder
            if liOrder[0]:
                print "start the question"
                ok = False
                print 'QuestionMsg'
                dialog = gtk.MessageDialog(None, gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_QUESTION,gtk.BUTTONS_YES_NO,_("An open order for this Customer exist. Would you append positions ?") );
                response = dialog.run ();

                print 'Response', response
                if response == gtk.RESPONSE_YES:
                    ok = True
                dialog.destroy ();
                #ok = cuon.Misc.messages.QuestionMsg(_("An open order for this Customer exist. Would you append positions ?"))
                if ok:
                    self.singleOrder.load(liOrder[1])
                else:
                    self.singleOrder.ID = -1
 
        except Exception,params:
            print Exception, params
            self.singleOrder.ID = -1
        
        return self.singleOrder.ID 

                        
    def on_JournalAccount_activate(self, event):
        print "Journal account"

    def on_tbCD_New_clicked(self,event):
        if self.tabOption == self.tabOrder or self.tabOption == self.tabInfo :
            print "new cashdesk order"
            #get first the basic customers id for cash_desk
           
            # add new record and save
            #self.on_new1_activate(None)
            #self.getWidget("eAddressNumber").set_text(`self.cashDeskCustomerID`)
            #self.on_save1_activate(None)

            self.dicOrder = {}
            self.dicOrder['addressnumber'] = self.cashDeskCustomerID
            self.dicOrder['ModulNumber'] = self.ModulNumber
            dicDate = self.getActualDateTime()
            self.dicOrder['orderedat'] = dicDate['date']
            self.dicOrder['deliveredat'] = dicDate['date']
            self.dicOrder['order_type'] = 2
            print "dicOrder = ", self.dicOrder
            self.createNewOrder()
            self.activateClick("tbAddPosition",sAction="clicked")
            self.getWidget("eCDArticleID").grab_focus()
 
    def on_tbCD_NewCustomerCard_clicked(self, event):
        if self.tabOption == self.tabOrder or self.tabOption == self.tabInfo :
            print "customer authenthication"
            self.wNewCustomer.show()
            #cd = cuon.Misc.cuon_dialog.cuon_dialog()
            #ok,res = cd.inputLine("Customer Card ID","Input the Customer Card ID")



    def on_bNewCustomerOK_clicked(self, event):
        
          
        cd_id = int(self.getWidget("eNewCustomerID").get_text())

        print cd_id
        self.getWidget("eAddressNumber").set_text(`cd_id`)
        if cd_id > 0:
           
            self.dicOrder = {}
            self.dicOrder['addressnumber'] = cd_id
            self.dicOrder['ModulNumber'] = self.ModulNumber
            dicDate = self.getActualDateTime()
            self.dicOrder['orderedat'] = dicDate['date']
            self.dicOrder['deliveredat'] = dicDate['date']
            self.dicOrder['order_type'] = 2
            print "dicOrder = ", self.dicOrder
            self.createNewOrder()
            self.wNewCustomer.hide()
            self.getWidget("eNewCustomerID").set_text("")
            self.activateClick("tbAddPosition",sAction="clicked")
            self.getWidget("eCDArticleID").grab_focus()

             
    def on_bNewCustomerCancel_clicked(self,event):
        self.wNewCustomer.hide()

    def on_eNewCustomerID_key_press_event(self,entry, event):
        if self.checkKey(event,'NONE','Return'):
            self.on_bNewCustomerOK_clicked(event)     


    def on_tbAddPosition_clicked(self,event):
        self.wAddPosition.show()
        self.getWidget("eCDAmount").set_text( self.getCheckedValue ( 1,'toLocaleString',iRound=3))


    def on_bCDPay_clicked(self, event):
        self.wAddPosition.hide()
        self.on_bCDNext_clicked(event)
        self.activateClick("tbPay",sAction="clicked")

    def on_bCDEdit_clicked(self, event):
        pass

    def on_bCDCancel_clicked(self, event):
        self.wAddPosition.hide()

    def on_eCDArticleID_key_press_event(self,entry,event):
        if self.checkKey(event,'NONE','Return'):
           self.activateClick("bCDNext",sAction="clicked")
 
    def getArticleID(self):
        sBarcodeID = self.getWidget("eCDArticleID").get_text()
        return  `self.rpc.callRP('Article.getIdForBarcode', sBarcodeID,self.dicUser)`

    def on_bCDNext_clicked(self,event):
       
        #take data and save them in the position

        iAmount = self.getCheckedValue (self.getWidget("eCDAmount").get_text(),"float")
        if not iAmount:
            iAmount = 0.00
        

        fDiscount = self.getCheckedValue (self.getWidget("eCDArticleDiscount").get_text(),"float")
        if not fDiscount:
            fDiscount = 0.00
        
        fPrice = self.getCheckedValue (self.getWidget("eCDPrice").get_text(),"float")
        if not fPrice:
            fPrice = 0.00

       

        sAmount = self.getCheckedValue ( iAmount,'toLocaleString',iRound=3)
        #sBarcodeID = self.getWidget("eCDArticleID").get_text()
        sDiscount = self.getCheckedValue (fDiscount ,'toLocaleString',iRound=3)
        
        sPrice =  self.getCheckedValue (fPrice, 'toLocaleString',iRound=3)
        
        sArticleID = self.getArticleID()
        #sArticleID = `self.rpc.callRP('Article.getIdForBarcode', sBarcodeID,self.dicUser)`	
        print "Values = ", sAmount, sArticleID, sDiscount, sPrice

        # do some checks
        if sArticleID:
        
            # create new Position
            self.on_PositionNew1_activate(event)

            # get Price for pricegroup

            if fPrice == 0.00:
                self.iTogglePrice = self.iDefaultTogglePrice
                
            if self.iTogglePrice > 0:
                fPrice = self.rpc.callRP('Article.getPriceForPricegroup', int(sArticleID),self.iTogglePrice,self.dicUser)
                sPrice =  self.getCheckedValue (fPrice, 'toLocaleString',iRound=3)
            
            #save them
            
            self.setMainwindowNotebook('F4')

            self.getWidget("eAmount").set_text(sAmount)
            self.getWidget("eArticleDiscount").set_text(sDiscount)
            self.getWidget("eArticleID").set_text(sArticleID)
            if fPrice != 0.00:
                self.getWidget("ePrice").set_text(sPrice)
            self.getWidget('ePosition').set_text(`self.rpc.callRP('Order.getNextPosition', self.singleOrder.ID,self.dicUser)`)


            self.on_PositionSave1_activate(event)
            self.getWidget("eCDAmount").set_text( self.getCheckedValue ( 1,'toLocaleString',iRound=3))
            self.getWidget("eCDArticleID").set_text("")
            self.getWidget("eCDPrice").set_text("")
            self.getWidget("eCDArticleDiscount").set_text("")

            self.iTogglePrice =  self.iDefaultTogglePrice
            self.setPriceGroupRadioButton()
            self.getWidget("eCDArticleID").grab_focus()

    def on_tbNew_clicked(self,  event):

        if self.tabOption == self.tabOrder or self.tabOption == self.tabInfo :
           
            self.on_new1_activate(event)
        elif self.tabOption == self.tabSupply:
            self.on_SupplyNew1_activate(event) 
        elif self.tabOption == self.tabGet:
            self.on_GetsNew1_activate(event) 
        elif self.tabOption == self.tabPosition:
            self.on_PositionNew1_activate(event)
        elif self.tabOption == self.tabPayment:
            self.on_payment_new_activate(event)

	    
    def on_tbEdit_clicked(self,  event):
        if self.tabOption == self.tabOrder or self.tabOption == self.tabInfo :
            self.on_edit1_activate(event)
        elif self.tabOption == self.tabSupply:
            self.on_SupplyEdit1_activate(event)  
        elif self.tabOption == self.tabGet:
            self.on_GetsEdit1_activate(event)
        elif self.tabOption == self.tabPosition:
            self.on_PositionEdit1_activate(event)
        elif self.tabOption == self.tabPayment:
            self.on_payment_edit_activate(event)
    def on_tbSave_clicked(self,  event):
        if self.tabOption == self.tabOrder or self.tabOption == self.tabInfo :
            self.on_save1_activate(event)  
        elif self.tabOption == self.tabSupply:
            self.on_SupplySave1_activate(event)    
        elif self.tabOption == self.tabGet:
            self.on_GetsSave1_activate(event)    
       
        elif self.tabOption == self.tabPosition:
            self.on_PositionSave1_activate(event)
        elif self.tabOption == self.tabPayment:
            self.on_payment_save_activate(event)
            
    def on_tbPlus1_clicked(self,event):
        self.addAmount(event, 1)

            
    def on_tbPlus1_clicked(self,event):
        self.addAmount(event, 1)
            
    def on_tbPlus5_clicked(self,event):
        self.addAmount(event, 5)
            
    def on_tbPlus10_clicked(self,event):
        self.addAmount(event, 10)
            
    def on_tbPlus50_clicked(self,event):
        self.addAmount(event, 50)
            
    def on_tbMinus1_clicked(self,event):
        self.addAmount(event, -1)

            
    def on_tbMinus5_clicked(self,event):
        self.addAmount(event, -5)

            
    def on_tbMinus10_clicked(self,event):
        self.addAmount(event, -10)

            
    def on_tbMinus50_clicked(self,event):
        self.addAmount(event, -50)



        
    def on_tbInfo_clicked(self,  event):
        if self.tabOption == self.tabOrder:
            self.on_DMS_activate(event)    
    def on_tbDup_clicked(self,  event):


        if self.tabOption == self.tabOrder:
            self.on_order_dup_activate(event)
        


    # cash desk short adds

    def on_tbCDPlus1_clicked(self,event):
        self.addCDAmount(event, 1)

            
    def on_tbCDPlus1_clicked(self,event):
        self.addCDAmount(event, 1)
            
    def on_tbCDPlus5_clicked(self,event):
        self.addCDAmount(event, 5)
            
    def on_tbCDPlus10_clicked(self,event):
        self.addCDAmount(event, 10)
            
    def on_tbCDPlus50_clicked(self,event):
        self.addCDAmount(event, 50)
            
    def on_tbCDMinus1_clicked(self,event):
        self.addCDAmount(event, -1)

            
    def on_tbCDMinus5_clicked(self,event):
        self.addCDAmount(event, -5)

            
    def on_tbCDMinus10_clicked(self,event):
        self.addCDAmount(event, -10)

            
    def on_tbCDMinus50_clicked(self,event):
        self.addCDAmount(event, -50)

    def getOrderInfo(self):
        
        
        dicOrder = self.singleOrder.firstRecord
     
        for key in self.singleAddress.firstRecord.keys():
            dicOrder['address_' + key] = self.singleAddress.firstRecord[key]
        dicInternInformation = self.rpc.callRP('Database.getInternInformation',self.dicUser)
        if dicInternInformation not in ['NONE','ERROR']:
            for key in dicInternInformation:
                dicOrder[key] = dicInternInformation[key]
        if self.singlePartner.ID > 0:
            for key in self.singlePartner.firstRecord.keys():
                dicOrder['partner_' + key] = self.singlePartner.firstRecord[key]
       
        return dicOrder

    def on_tbPay_clicked(self,event):
        if self.cashUser:
           
            self.wPaying.show()
            self.getWidget('eTotalToPay').set_text( self.rpc.callRP('Order.getTotalSumString', self.singleOrder.ID, self.dicUser))
            self.getWidget("eCashPay").set_text("")
            self.getWidget('eBackPay').set_text("")
            #self.getWidget("eCashPay").grab_focus()

    def on_bCancel_Pay_activate(self, event):
       
        self.wPaying.hide()
        self.setMainwindowNotebook('F1')
    
	def on_Kassenbon_clicked (self, event):
		print "start cash invoice"
		self.printInvoice("cash_desk")
		print "cash invoice finished"

    def on_bPayCash_clicked(self, event):
        print "start cash invoice"
        self.printInvoice("cash_desk")
        print "cash invoice finished"
        self.wPaying.hide()
        paidMoney = self.getCheckedValue(self.getWidget("eCashPay").get_text(),"float")
        toPay =  self.getCheckedValue(self.getWidget('eTotalToPay').get_text(),"float")
        self.rpc.callRP('Order.setPreCashPaying', self.singleOrder.ID, self.dicUser)
        self.rpc.callRP('Order.saveCashPaying', self.singleOrder.ID,paidMoney,toPay, 1,self.dicUser)       
        self.createSimplePayment("cash1")
        self.setMainwindowNotebook('F1')

    def on_bPayCreditCard_clicked(self, event):
        cmm = cuon.Misc.messages.messages()
        if cmm.QuestionMsg(_('Is the Credit Card paying O.K. ?')):
            print "start cash invoice"
            self.printInvoice("credit_card")
            print "cash invoice finished"
            self.wPaying.hide()
            paidMoney = self.getCheckedValue(self.getWidget("eCashPay").get_text(),"float")
            toPay =  self.getCheckedValue(self.getWidget('eTotalToPay').get_text(),"float")
            #self.rpc.callRP('Order.setPreCashPaying', self.singleOrder.ID, self.dicUser)
            #self.rpc.callRP('Order.saveCashPaying', self.singleOrder.ID,paidMoney,toPay, 1,self.dicUser)       
            self.createSimplePayment("creditCard")
            self.setMainwindowNotebook('F1')


    def n_bPayBankTransfer_clicked(self, event):
        print "start cash invoice"
        self.printInvoice("cash_desk")
        print "cash invoice finished"
        self.wPaying.hide()
        paidMoney = self.getCheckedValue(self.getWidget("eCashPay").get_text(),"float")
        toPay =  self.getCheckedValue(self.getWidget('eTotalToPay').get_text(),"float")
        self.rpc.callRP('Order.setPreCashPaying', self.singleOrder.ID, self.dicUser)
        self.rpc.callRP('Order.saveCashPaying', self.singleOrder.ID,paidMoney,toPay, 1,self.dicUser)       
        self.createSimplePayment("cash1")
        self.setMainwindowNotebook('F1')


    def on_eCashPay_changed(self, event):
        print "cashPay changed"
        sTotal = self.getWidget('eTotalToPay').get_text()
        fTotal = float(sTotal[:sTotal.find(" ")])
        fCash = self.getCheckedValue(self.getWidget("eCashPay").get_text(),"float",iRound=2)

        fBack = (fTotal - fCash)*-1
        print "fBackPay = ", fBack
        self.getWidget('eBackPay').set_text(self.getCheckedValue(fBack, "toLocaleString") )

    def createNewOrder(self):
        print "create new Order"
        try:
            newID = self.checkIfOldOrderExist()
            print "newID by create new Order ", newID
            if newID > 0:
                pass
            else:
                newID = self.rpc.callRP('Order.createNewOrder', self.dicUser,self.dicOrder)
            print newID
            if newID > 0:
                self.OrderID = newID
                print 'Order-Id = ',  self.OrderID
                self.singleOrder.sWhere = ' where id = ' + `self.OrderID` 
                self.tabChanged()
        except:
            pass


    def addAmount(self,event, iAdd):
       
        iAmount = self.getCheckedValue (self.getWidget("eAmount").get_text(),"float")
        if iAmount:
            iAmount += iAdd
            self.getWidget("eAmount").set_text( self.getCheckedValue ( iAmount,'toLocaleString',iRound=3))
            self.on_PositionSave1_activate(event)

    def addCDAmount(self,event, iAdd):
        #print "fetch old Value"
        iAmount = self.getCheckedValue (self.getWidget("eCDAmount").get_text(),"float")
        if not iAmount:
            iAmount = 0.00
        #print "iAmount = ", iAmount
        iAmount += iAdd
        self.getWidget("eCDAmount").set_text( self.getCheckedValue ( iAmount,'toLocaleString',iRound=3))
            

    def on_eCDShortCutName_key_press_event(self,entry,event):
        if self.checkKey(event,'NONE','Return'):
           self.getWidget("eCDShortCutPin").grab_focus() 
           
    def on_eCDShortCutPin_key_press_event(self,entry,event):
        if self.checkKey(event,'NONE','Return'):
            self.activateClick("bShortCutOK")
    def on_bShortCutOK_clicked(self, event):
       
        try:
            shortcut=self.getWidget("eCDShortCutName").get_text().strip()
            pin = self.getWidget("eCDShortCutPin").get_text().strip()

            new_shortcut = self.rpc.callRP('User.checkCashDeskPin',shortcut,pin , self.dicUser)
            if new_shortcut and new_shortcut != "NONE":
                self.dicUser["short_key"]= new_shortcut
                
                self.getWidget("eCDShortCutName").set_text("")
                self.getWidget("eCDShortCutPin").set_text("")
                
                self.getWidget("tbPay").set_sensitive(True)
                self.enable_cash_desk_menu()
            else:

                self.disable_cash_desk_menu()

        except:
            print "wrong authentication"

        self.wShortcut.hide()


    def on_bShortCutCancel_clicked(self, event):
        self.wShortcut.hide()


    def on_tbCDClearShortcut_clicked(self, event):
        self.disable_cash_desk_menu()


    #####################################################
    # Special account activities
    #####################################################


    # Pay out

    def on_PayOut_activate(self,event):
        wPO = self.getWidget("PayOut")
        wPO.show()

    def on_bOK_PayOut_clicked(self,event):
        wPO = self.getWidget("PayOut")
        liCash = []
        liCash.append(self.getCheckedValue(self.getWidget("ePayOutCash").get_text(),"float",iRound=2))
        liCash.append( self.getWidget("ePayOutDescription").get_text())
        self.rpc.callRP('Order.savePayOut',liCash,self.dicUser) 
        self.getWidget("ePayOutCash").set_text("")
        self.getWidget("ePayOutDescription").set_text("")
        wPO.hide()

    def on_bCancel_PayOut_clicked(self,event):
        wPO = self.getWidget("PayOut")
        wPO.hide()


    # Pay in

    def on_PayIn_activate(self,event):
        wPI = self.getWidget("PayIn")
        wPI.show()

    def on_bOK_PayIn_clicked(self,event):
        wPI = self.getWidget("PayIn")
        liCash = []
        liCash.append(self.getCheckedValue(self.getWidget("ePayInCash").get_text(),"float",iRound=2))
        liCash.append( self.getWidget("ePayInDescription").get_text())
        self.rpc.callRP('Order.savePayIn',liCash,self.dicUser) 
        self.getWidget("ePayInCash").set_text("")
        self.getWidget("ePayInDescription").set_text("")
        wPI.hide()

    def on_bCancel_PayIn_clicked(self,event):
        wPI = self.getWidget("PayIn")
        wPI.hide()




    # Pay Supplier

    def on_PaySupplier_activate(self,event):
        wPO = self.getWidget("PaySupplier")
        wPO.show()

    def on_bOK_PaySupplier_clicked(self,event):
        wPO = self.getWidget("PaySupplier")
        liCash = []
        liCash.append(self.getCheckedValue(self.getWidget("ePaySupplierCash").get_text(),"float",iRound=2))
        liCash.append( self.getWidget("ePaySupplierDescription").get_text())
        liCash.append( self.getWidget("ePaySupplierID").get_text())
        self.rpc.callRP('Order.savePaySupplier',liCash,self.dicUser) 
        self.getWidget("ePaySupplierCash").set_text("")
        self.getWidget("ePaySupplierDescription").set_text("")
        wPO.hide()

    def on_bCancel_PayOut_clicked(self,event):
        wPO = self.getWidget("PayOut")
        wPO.hide()

    def on_bPaySupplierSearchID_clicked(self, event):
        padr = cuon.Addresses.addresses.addresswindow(self.allTables)
        padr.setChooseEntry('chooseSupplier', self.getWidget( 'ePaySupplierID'))


    def on_ePaySupplierID_changed(self, event):
        print 'ePySupplierID changed'
	iPSupNumber = self.getChangedValue('ePaySupplierID')
	eAdrField = self.getWidget('tvSupply')
        liPaySup = self.singleAddress.getAddress(iAdrNumber)
        self.setTextbuffer(ePSupNumber,liPaySup)







    #on_ePaySupplierID_changed
    #on_bPaySupplierSearchID_clicked



    ###################################################
    # Radio Buttons
    ###################################################

    def setPriceGroupRadioButton(self):
       
            if self.iTogglePrice == 0:
                self.on_rbPNone_toggled(None)

            elif self.iTogglePrice == 1:
                self.on_rbP1_toggled(None)

            elif self.iTogglePrice == 2:
                self.on_rbP2_toggled(None)
            elif self.iTogglePrice == 3:
            
                self.on_rbP3_toggled(None)
            elif self.iTogglePrice == 4:
                
                self.on_rbP4_toggled(None)
            elif self.iTogglePrice == 5:
                self.on_rbP5_toggled(None)



    def togglePrice(self,iPriceGroup):
        self.iTogglePrice = iPriceGroup

        sNewPrice = '0.00'
        if iPriceGroup >0:
            sArticleID = self.getArticleID()
            if sArticleID:
                try:
                    iArticleID = int(sArticleID)
                except:
                    iArticleID = 0
                if iArticleID > 0:
                    sNewPrice = self.rpc.callRP('Article.getPriceForPricegroup',iPriceGroup,iArticleID,self.dicUser)
                    
                    self.getWidget("ePrice").set_text(sNewPrice)
                



         


    def on_rbPNone_toggled(self, event):
        self.togglePrice(0)

    def on_rbP1_toggled(self, event):
        self.togglePrice(1)

    def on_rbP2_toggled(self, event):
        self.togglePrice(2)

    def on_rbP3_toggled(self, event):
        self.togglePrice(3)

    def on_rbP4_toggled(self, event):
        self.togglePrice(4)



    

    ####################################################
    # Lists
    ###################################################


    # print the journal

    def on_JournalAccount_activate(self,event):
        wCDL = self.getWidget("CashDeskLists")
        wCDL.show()


    def on_bAllValues_CDL_clicked(self,event):
        wCDL = self.getWidget("CashDeskLists")
       
        Pdf = self.rpc.callRP('Report.server_order_cdl_journal',self.readCDLSearchfields(),"AllValues",  self.dicUser)
        wCDL.hide()
        self.showPdf(Pdf, self.dicUser)

    def on_bDailyCDL_clicked(self, event):
        wCDL = self.getWidget("CashDeskLists")

        Pdf = self.rpc.callRP('Report.server_order_cdl_journal',self.readCDLSearchfields(),"AllDaily",  self.dicUser)
        wCDL.hide()
        self.showPdf(Pdf, self.dicUser)


    def on_bUserAll_clicked(self, event):
        wCDL = self.getWidget("CashDeskLists")

        Pdf = self.rpc.callRP('Report.server_order_cdl_journal',self.readCDLSearchfields(),"AllUser",  self.dicUser)
        wCDL.hide()
        self.showPdf(Pdf, self.dicUser)


    def on_bUserDailyCDL_clicked(self, event):
        wCDL = self.getWidget("CashDeskLists")

        Pdf = self.rpc.callRP('Report.server_order_cdl_journal',self.readCDLSearchfields(),"UserDaily",  self.dicUser)
        wCDL.hide()
        self.showPdf(Pdf, self.dicUser)

    def on_bCC_clicked(self,event):
        wCDL = self.getWidget("CashDeskLists")
       
        Pdf = self.rpc.callRP('Report.server_order_cdl_journal',self.readCDLSearchfields(),"CreditCard",  self.dicUser)
        wCDL.hide()
        self.showPdf(Pdf, self.dicUser)


    def on_bStats_clicked(self,event):
        wCDL = self.getWidget("CashDeskLists")
       
        Pdf = self.rpc.callRP('Report.server_order_cdl_journal',self.readCDLSearchfields(),"Stats",  self.dicUser)
        wCDL.hide()
        self.showPdf(Pdf, self.dicUser)



    def on_bCancel_CDL_clicked(self,event):
        wCDL = self.getWidget("CashDeskLists")
        wCDL.hide()


    def set_cach_desk_menus(self,bSet):

        self.getWidget("file1").set_sensitive(bSet)
        self.getWidget("order1").set_sensitive(bSet)
        self.getWidget("supply1").set_sensitive(bSet)
        self.getWidget("gets1").set_sensitive(bSet)
        self.getWidget("position1").set_sensitive(bSet)
        self.getWidget("invoice1").set_sensitive(bSet)
        self.getWidget("payments1").set_sensitive(bSet)
        self.getWidget("misc1").set_sensitive(bSet)
        self.getWidget("Lists").set_sensitive(bSet)

        self.getWidget("New").set_sensitive(bSet)
        self.getWidget("tbCD_NewCustomerCard").set_sensitive(bSet)
        self.getWidget("tbAddPosition").set_sensitive(bSet)
        self.getWidget("print").set_sensitive(bSet)
        self.getWidget("delete_position").set_sensitive(bSet)
        self.getWidget("tbPay").set_sensitive(bSet)
        

    def enable_cash_desk_menu(self):
        self.set_cach_desk_menus(True)
    def disable_cash_desk_menu(self): 
        self.set_cach_desk_menus(False)

    def readCDLSearchfields(self):

      


        dicSearchfields = {}
        dicSearchfields["eCDLDateFrom"] = self.getWidget("eCDLDateFrom").get_text() 
        dicSearchfields["eCDLDateTo"] = self.getWidget("eCDLDateTo").get_text() 
        dicSearchfields["eCSFFrom"] = self.getWidget("eCSFFrom").get_text() 
        dicSearchfields["eCSFTo"] = self.getWidget("eCSFTo").get_text() 
        dicSearchfields["eCashNrFrom"] = self.getWidget("eCashNrFrom").get_text() 
        dicSearchfields["eCashNrTo"] = self.getWidget("eCashNrTo").get_text() 
        dicSearchfields["eCLIDFrom"] = self.getWidget("eCLIDFrom").get_text() 
        dicSearchfields["eCLIDTo"] = self.getWidget("eCLIDTo").get_text() 

        return dicSearchfields

    def refreshTree(self):
        print "#################### refresh Tree #########################"
        self.singleOrder.disconnectTree()
        self.singleOrderSupply.disconnectTree()
        self.singleOrderGet.disconnectTree()
        self.singleOrderPosition.disconnectTree()
        #self.singleOrderInvoice.disconnectTree()
        self.singleOrderMisc.disconnectTree()
        
        if self.tabOption == self.tabOrder:
            self.singleOrder.setEntries(self.getDataEntries(self.EntriesOrder) )
            if self.OrderID > 0:
                self.singleOrder.sWhere = ' where id = ' + `self.OrderID` + ' and process_status between 500  and 599'
            
            self.singleOrder.connectTree()                
            if self.oldTab < 1:

                self.singleOrder.refreshTree()
            else:
                self.singleOrder.refreshTree(False)


        elif self.tabOption == self.tabInfo:
            self.singleOrderInfo = self.singleOrder
            self.singleOrderInfo.setEntries(self.getDataEntries(self.EntriesOrderAdditionalInfo) )
            self.singleOrderInfo.fillEntries(self.singleOrder.ID)
           

        elif self.tabOption == self.tabSupply:
            self.singleOrderSupply.sWhere  ='where orderid = ' + `int(self.singleOrder.ID)`
            self.singleOrderSupply.connectTree()
            self.singleOrderSupply.refreshTree()

        elif self.tabOption == self.tabGet:
            self.singleOrderGet.sWhere  ='where orderid = ' + `int(self.singleOrder.ID)`
            self.singleOrderGet.connectTree()
            self.singleOrderGet.refreshTree()
 
        elif self.tabOption == self.tabPosition:
            self.singleOrderPosition.sWhere  ='where orderid = ' + `int(self.singleOrder.ID)` + ' and articleid = articles.id '
            self.singleOrderPosition.connectTree()
            self.singleOrderPosition.refreshTree()
            
        elif self.tabOption == self.tabInvoice:
            #self.singleOrder.connectTree()
            #self.singleOrder.refreshTree()
            self.singleOrderInvoice.sWhere  ='where orderid = ' + `self.singleOrder.ID`
            print 'Singleid =',  self.singleOrderInvoice.findSingleId()
            
            self.singleOrderInvoice.refreshTree()
            
            self.singleOrderInvoice.fillEntries(self.singleOrderInvoice.findSingleId())
            
        elif self.tabOption == self.tabMisc:
            
            self.singleOrder.setEntries(self.getDataEntries(self.EntriesOrderMisc) )
           

        elif self.tabOption == self.tabPayment:
            self.singleOrderPayment.sWhere  ='where order_id = ' + `int(self.singleOrder.ID)`
            self.singleOrderPayment.orderID = self.singleOrder.ID 
            self.singleOrderPayment.connectTree()
            self.singleOrderPayment.refreshTree()
        self.oldTab = self.tabOption

    def tabChanged(self):
        print 'tab changed to :'  + str(self.tabOption)
        print "#################### change tab #########################"
        if ( self.cashUser == False or ( self.cashUser and self.dicUser["short_key"] != "NONE" ) ):
            if self.tabOption == self.tabOrder:
                #Address
                self.disableMenuItem('tabs')
                self.enableMenuItem('order')
                print 'Seite 0'
                self.editAction = 'editOrder'
                self.setTreeVisible(True)
                #self.singleOrder.sWhere = ' where process_status between 500 and 599'

            elif self.tabOption == self.tabInfo:
                #Address
                self.disableMenuItem('tabs')
                self.enableMenuItem('order')
                print 'Seite 0'
                self.editAction = 'editOrder'
                self.setTreeVisible(False)
            elif self.tabOption == self.tabSupply:
                #Partner
                self.disableMenuItem('tabs')
                self.enableMenuItem('supply')
                self.editAction = 'editSupply'
                self.setTreeVisible(True)

                print 'Seite 1'

            elif self.tabOption == self.tabGet:
                self.disableMenuItem('tabs')
                self.enableMenuItem('gets')
                self.editAction = 'editGets'
                self.setTreeVisible(True)
                print 'Seite 2'
            elif self.tabOption == self.tabPosition:
                self.disableMenuItem('tabs')
                self.enableMenuItem('positions')
                self.editAction = 'editPositions'
                self.setTreeVisible(True)
                print 'Seite 3'  


            elif self.tabOption == self.tabInvoice:
                self.disableMenuItem('tabs')
                self.enableMenuItem('invoice')
                self.editAction = 'editInvoice'
                self.setTreeVisible(False)
                print 'Seite 4'

            elif self.tabOption == self.tabMisc:
                self.disableMenuItem('tabs')
                self.enableMenuItem('misc')
                self.setTreeVisible(False)
                self.editAction = 'editMisc'
                print 'Seite 5'

            elif self.tabOption == self.tabPayment:
                self.disableMenuItem('tabs')
                self.enableMenuItem('payment')
                self.editAction = 'editPayment'
                self.setTreeVisible(True)
                print 'Seite 6'  

        # refresh the Tree
        self.refreshTree()
        self.enableMenuItem(self.editAction)
        self.editEntries = False
