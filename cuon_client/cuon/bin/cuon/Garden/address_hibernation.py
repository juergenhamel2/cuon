# -*- coding: utf-8 -*-

##Copyright (C) [2011]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 


from cuon.Windows.gladeXml import gladeXml
import gtk
import pygtk
import gobject
#import gtk.glade 
import hibernation
import locale
from locale import gettext as _

class address_hibernation(gladeXml):
    
    def __init__(self):
        gladeXml.__init__(self, False)
        self.tree = None
        self.addresses = None
        self.treeConnectedRow = False
        self.newID = 0
        
    def setTree(self,  tree1):
        self.tree = tree1
        if not self.treeConnectedRow:   
            self.tree.connect("row_activated", self.on_tree_row_activated);
            self.treeConnectedRow = True
    def disconnectTree(self):
        try:
            
            self.tree.get_selection().disconnect(self.connectTreeId)
        except:
            pass

    def connectTree(self):
        try:
            self.connectTreeId = self.tree.get_selection().connect("changed", self.tree_select_callback)
        except:
            pass
   
    def tree_select_callback(self, treeSelection):
        listStore, iter = treeSelection.get_selected()
        
        print listStore,iter
        
        if listStore and len(listStore) > 0:
           row = listStore[0]
        else:
           row = -1
   
        if iter != None:
            sNewId = listStore.get_value(iter, 0)
            print sNewId
            try:
                self.newID = int(sNewId[sNewId.find('###')+ 3:])
                #self.setDateValues(newID)
                print 'newID = ',   self.newID
            except Exception,  params:
                self.newID = 0
             #   print Exception,  params
                
            #self.fillEntries(newId)
    def on_tree_row_activated(self, event, data1, data2):
        print 'event'
        #print "Hibernation_address",   self.addresses.singleAddress.ID,   self.addresses.allTables
        hib = hibernation.hibernationwindow( self.addresses.allTables,  addressid = self.addresses.singleAddress.ID,  hibernation_id = self.newID)
        
        
    def fillAddressHibernation(self,  liDates ,  addresses):
        treeview = self.tree
        self.addresses = addresses
        # Clean the tree
        columnlist = treeview.get_columns()
        for i in columnlist:
            treeview.remove_column(i)
            
        #treeview.set_model(liststore)
 
        #renderer = gtk.CellRendererText()
        #column = gtk.TreeViewColumn("Scheduls", renderer, text=0)
        #treeview.append_column(column)
        self.disconnectTree()
        
        treestore = gtk.TreeStore(object)
        treestore = gtk.TreeStore(str)
        renderer = gtk.CellRendererText()
 
        column = gtk.TreeViewColumn("Zweite Spalte", renderer, text=0)
        treeview.append_column(column)
        treeview.set_model(treestore)
        
        print 'Schedul by names: ', liDates
        if liDates and liDates not in self.liSQL_ERROR:
           
            Schedulname = None
            lastSchedulname = None
            
            iter = treestore.append(None,[_('Hibernation Number, Sequence')])
            iter2 = None
            iter3 = None
            for oneDate in liDates:
                Schedulname = oneDate['hibernations']
                print Schedulname
                iter2 = treestore.insert_after(iter,None,[Schedulname])   
        treeview.expand_all()        
        treeview.show_all()
        
        self.connectTree()
        print 'end filladdressHibernation'
        return True
        
        
