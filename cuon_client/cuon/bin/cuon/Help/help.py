 
import sys
from types import *
import pygtk
pygtk.require('2.0')
import gtk

   
import locale
from locale import gettext as _

#import gtk.glade
import gobject
import cuon.XMLRPC.xmlrpc
from cuon.Windows.windows  import windows
try:
    import webkit
except:
    print "no webkit module found"
    

class helpwindow(windows):

    
    def __init__(self):

        windows.__init__(self)

        self.loadGlade('help.xml')
        self.win1 = self.getWidget('HelpMainwindow')

        self.webview = webkit.WebView()
        

        #self.helpmoz = moz.MozEmbed()
        #sw1 = self.getWidget('swHelp')
        self.vbox = self.getWidget('vbox2')
        self.vbox.add(self.webview)
        self.webview.open("http://cuon.org")
       
        #self.webview.set_size_request(816,600)
        #self.helpmoz.show()
        self.win1.show_all()
        
    def on_tBack_clicked(self, event):
        """
            Back button clicked, go one page back
        """
        self.webview.go_back()
       
            
    
    def on_tForward_clicked(self, event):
        self.webview.go_forward()
        
    def on_tRelaod_clicked(self, event):
        self.webview.reload()


        
    def on_tQuit_clicked(self, event):
        self.closeWindow()
        
    def on_quit1_activate(self, event):
        print "exit help v2"
        self.closeWindow()



