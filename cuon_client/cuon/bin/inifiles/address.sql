 CREATE OR REPLACE FUNCTION fct_getAddress(  iAddressID int) returns  record AS '
 
     DECLARE
    sSql    text ;
    recAddress  record;
    
    
    
    BEGIN
    sSql := ''select address, lastname, lastname2,firstname, street, zip, city,city as cityfield,  
        state, country from address 
        where id = '' || iAddressID  || '' '' || fct_getWhere(2,'' '') ;
    for recAddress in execute sSql 
    LOOP 
        raise notice ''addresslastnmae = %'',recAddress.lastname ;
        raise notice ''address = %'',recAddress.address ;
        --raise notice ''cityfield = %'',recAddress.cityfield ;
       
        if recAddress.country is NULL then
            recAddress.cityfield := recAddress.zip || '' '' || recAddress.city ;
        else 
            recAddress.cityfield := recAddress.country || ''-'' || recAddress.zip || '' '' || recAddress.city ;
        END IF ;
        --recAddress.country := fct_getChar(recAddress.country) ;
        raise notice ''country = %'',recAddress.country ;
       raise notice ''cityfield = %'',recAddress.cityfield ;
       
    END LOOP ;
    return recAddress  ;
       END ;
    ' LANGUAGE 'plpgsql'; 
 
           
CREATE OR REPLACE FUNCTION fct_getLastname(  iAddressID int) returns  text AS '
 
     DECLARE
    sSql    text ;
    recAddress  record;
    sLastname text ;
    
    
    BEGIN
    sSql := ''select lastname from address 
        where id = '' || iAddressID  || '' '' || fct_getWhere(2,'' '') ;
    for recAddress in execute sSql 
    LOOP 
        raise notice ''addresslastnmae = %'',recAddress.lastname ;
        
        if recAddress.lastname is NULL then
            sLastname := ''NONE''  ;
        else 
            sLastname := recAddress.lastname;
        END IF ;
        
       
    END LOOP ;
    return sLastname ;
       END ;
    ' LANGUAGE 'plpgsql'; 
 
 CREATE OR REPLACE FUNCTION fct_generatePartner(iAddressID int)    returns  int AS '
 
     DECLARE
    sSql    text ;
    sSql2   text ;
    recAddress  record;
    sLastname text ;
    iNewID int := 0 ;
    newUUID text ;
    
    BEGIN
    execute(''select * from fct_new_uuid()'') into newUUID ;
    raise notice ''new uuid at partner = %'',newUUID ;
    sSql := ''select * from address where id = '' || iAddressID  || '' '' || fct_getWhere(2,'' '') ;
    raise notice '' sSql = %'',sSql ;
    execute(sSql) into recAddress ;
    raise notice '' rec = %'',recAddress ;
    sSql2 := '' insert into partner (id, uuid,lastname, firstname,street,zip,city,phone, addressid) values (nextval('' || quote_literal(''partner_id'') || ''),'' || quote_literal(newUUID) || '','' || quote_literal(recAddress.lastname) || '','' || quote_literal(recAddress.firstname) || '', '' || quote_literal(recAddress.street) || '', '' || quote_literal(recAddress.zip) || '', '' || quote_literal(recAddress.city) || '', ''  || quote_literal(recAddress.phone) || '', '' || iAddressID || '' ) '' ;
    raise notice '' sSql = %'',sSql2 ;
    execute (sSql2);
    sSql := ''select id from partner where uuid = '' || quote_literal(newUUID) ;
    execute(sSql) into iNewID ;
    return iNewID ;
       END ;
    ' LANGUAGE 'plpgsql';   

            
CREATE OR REPLACE FUNCTION fct_getSchedulTime(  iTimeBegin int, iTimeEnd int , aTimes char[] ,recId int) returns  varchar AS '
 
     DECLARE
    sSql    text ;
    sTime varchar;
    BEGIN 
        sTime := aTimes[iTimeBegin] || '' - '' ||  aTimes[iTimeEnd]  ;
        
    return sTime ;
       END ;
    ' LANGUAGE 'plpgsql'; 



 
