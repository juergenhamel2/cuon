CREATE OR REPLACE FUNCTION fct_getHibernationForAddressID(iAddressID int ) returns setof text AS '
 DECLARE
     iClient int ;
    r record;
    searchsql text := '''';

    BEGIN
       
        

        searchsql := ''select hibernation_number , sequence_of_stock, hibernation.id as hib_id from hibernation where addressnumber = '' || iAddressID ||  fct_getWhere(2,'''') ;
        FOR r in execute(searchsql)  LOOP
         
        
            return next r.hibernation_number || '', '' || r.sequence_of_stock || ''  ###'' || r.hib_id ;

        END LOOP ;
        
    END ;
    

    
     ' LANGUAGE 'plpgsql'; 

     
CREATE OR REPLACE FUNCTION fct_setGardenSchedule() returns int AS '
 DECLARE
    iClient int ;
    iOK int := 1 ;
    r record;
    searchsql text := '''';
    sSql1 text ;
    GraveServiceNotesAnnual int = 40105 ;
    
    BEGIN
       
        

        searchsql := ''select * from grave_work_year '' || fct_getWhere(1,'''');
        -- raise notice ''search SQL = %'', searchsql ;
        FOR r in execute(searchsql)  LOOP
         
        
            -- do it
            -- raise notice '' r.uuid = %'', r.uuid ;
            if r.uuid is not null then 
                sSql1 = '' select * from fct_createModulSchedule('' || GraveServiceNotesAnnual || '', '' || quote_literal(r.uuid) || '') '' ;
                -- raise notice ''sSql1 SQL = %'', sSql1 ;
                execute(sSql1) into iOK;
                -- raise notice '' ---------------------------------------------------------------------Result = %'', iOK ;
            end if ;
        END LOOP ;
        return iOK ;
    END ;
    

    
     ' LANGUAGE 'plpgsql'; 
     
CREATE OR REPLACE FUNCTION fct_createModulSchedule( iModule int, scheduleUUID text) returns int AS '
 DECLARE
    -- working standalone but can called from a trigger
    
    r record;
    s record ;
    t record ;
    u record ;
    v record ;
    q record ;
    o record ;
    
    searchsql text := '''';
    sSql1 text := '''';
    sSql2 text := '''';
    sSql3 text := '''';
    sSql4 text := '''';
    sSql5 text := '''';
    sSql6 text := '''';
    sSql7 text := '''';
    
    newID int := 0 ;
    idFound int := 0;
    idPartner int := 0;
    iClient int := 0 ;
    sSchedulStaffs text := '''';
    iStaffID int := 0 ;
    aStaff text array ;
    GraveServiceNotesAnnual int = 40105 ;
    sStaff text ;
    array_size int ;
    iArray int ;
    newUUID text ;
    newDate text ;
    newDate2 text ;
    sServiceDesignation text ;
    sServiceNote text ;
    
    BEGIN
       
        -- Search if schedule with the given uuid exist

            
        searchsql := ''select id from partner_schedul  where schedul_uuid = '' || quote_literal(scheduleUUID) || '' '' || fct_getWhere(2,'''') ;
        raise notice '' searchsql = %'', searchsql ;
        FOR r in execute(searchsql)  LOOP
         
            if r.id is not null then 
                if r.id > 0 then
                    idFound := r.id ;
                    -- if exist delete it
                    raise notice '' delete id found = %'', idFound ;
                    
                    sSql1 := ''delete from partner_schedul where id = '' || idFound ;
                    execute(sSql1) ;
                    raise notice '' delete this %'', sSql1 ;
                end if ;
            end if;

        END LOOP ;

        raise notice '' id found = %'', idFound ;
        
        
        
        if iModule = GraveServiceNotesAnnual then
            searchsql := ''select * from grave_work_year where uuid = '' || quote_literal(scheduleUUID)  ;
            FOR r in execute(searchsql)  LOOP
         
                if r.id > 0 then
                    if r.grave_id is not null then 
                        searchsql := ''select * from grave where id = '' || r.grave_id || '' and ( contract_ends_at is null or  (contract_ends_at is not null and contract_ends_at > now() ) or  contract_ends_at = date('' || quote_literal(''1900-01-01'') || '' ) ) ''  ;
                        raise notice ''s sql =  %'', searchsql ;
                        execute(searchsql) into s ;
                        
                        if s.graveyardid is not null then 
                            execute('' select * from graveyard where id = '' || s.graveyardid) into q ;
                        end if ;
                        if r.article_id is not null then 
                            execute(''select * from articles where id = '' || r.article_id ) into o ;
                        end if ;
                        
                        if s.addressid is not null then 
                            searchsql := ''select * from address where id = '' || s.addressid ;
                            execute(searchsql) into t ;
                            -- raise notice ''t = %'', t ;
                            if t.id is not null then  
                                searchsql := ''select * from partner where addressid = '' || t.id ;
                                execute(searchsql) into u ;
                                raise notice ''u = %'', u ;
                                if u.id > 0 then 
                                    idPartner := u.id ;
                                else
                                    -- generate partner
                                    sSql2 := ''select * from fct_generatePartner('' || t.id || '' ) '' ;
                                    execute(sSql2) into newID ;
                                    
                                    idPartner := newID ;
                                    
                                end if ;
                                -- raise notice ''idPartner = %'', idPartner ;
                                sSql1 := ''select * from fct_getUserDataClient()'' ;
                                execute(sSql1) into iClient ;
                                
                                sSql1 :=  ''select * from fct_get_config_option('' || iClient || '', '' || quote_literal(''clients.ini'') || '', '' || quote_literal(''CLIENT_'' || 1) || '', '' || quote_literal(''user_grave_schedule_yearly'') || '') '' ;
                                -- raise notice ''sSql1 = %'',sSql1 ;
                                execute(sSql1) into sSchedulStaffs ;    
                                -- raise notice ''sSchedulStaffs = %'',sSchedulStaffs ;
                                aStaff = string_to_array(sSchedulStaffs,'','') ;
                                
                                array_size := array_length(aStaff,1) ;
                                if r.service_designation is null then 
                                    sServiceDesignation := ''Yearly Graves'' ;
                                elseif char_length(r.service_designation) < 1 then 
                                    sServiceDesignation := ''Yearly Graves'' ;
                                else 
                                   select into  sServiceDesignation substring(r.service_designation from 1 for 59) ;
                                end if ;
                             
                                   
                                -- r = grave_work ;
                                -- s = grave ;
                                -- t = address ;
                                -- q = graveyard ;
                                -- o = article ;
                                
                                sServiceNote := '''';
                                sServiceNote := sServiceNote || ''Grave: '' ;
                               
                                if s.firstname is not null then 
                                    sServiceNote := sServiceNote ||  s.firstname ;
                                end if ;
                                 if s.lastname is not null then 
                                    sServiceNote := sServiceNote || '' '' ||  s.lastname ;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                if s.detachment is not null then 
                                    sServiceNote := sServiceNote ||  s.detachment ;
                                end if ;
                                if s.grave_number is not null then 
                                    sServiceNote := sServiceNote ||  '', '' || s.grave_number;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                
                                sServiceNote := sServiceNote || ''Address: '' ;
                                  
                                if t.lastname is not null then 
                                    sServiceNote := sServiceNote ||  t.lastname ;
                                end if ;
                                if t.firstname is not null then 
                                    sServiceNote := sServiceNote || '', '' || t.firstname ;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                
                                sServiceNote := sServiceNote || ''Graveyard: '' ;
                                if q.shortname is not null then 
                                    sServiceNote := sServiceNote || '', '' || q.shortname ;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                
                                sServiceNote := sServiceNote || ''Article: '' ;
                                 if r.service_count is not null then 
                                    sServiceNote := sServiceNote ||  r.service_count || ''x  '' ;
                                 else 
                                    sServiceNote := sServiceNote ||   ''1x  '' ;
                                end if ;
                                
                                if o.id is not null then 
                                    sServiceNote := sServiceNote ||  o.id ;
                                end if ;
                                if o.number is not null then 
                                    sServiceNote := sServiceNote || '', '' || o.number ;
                                end if ;
                                if r.service_price is not null then 
                                    sServiceNote := sServiceNote || '', Price =  '' || r.service_price || ''  Total = '' || r.service_price * r.service_count;
                                end if ;
                                if o.designation is not null then 
                                    sServiceNote := sServiceNote || chr(10) || o.designation ;
                                end if ;
                                sServiceNote := sServiceNote || chr(10) ;
                                sServiceNote := sServiceNote || chr(10) || chr(10) ;
                                if r.service_notes is not null then
                                  
                                    sServiceNote := sServiceNote || r.service_notes ;
                                end if ;
                                
                                -- raise notice ''ServiceNote = %'', sServiceNote ;
                                
                                for iArray in 1..array_size loop
                                    execute(''select * from fct_new_uuid()'') into newUUID ;
                                    -- raise notice ''single staff = %'',aStaff[iArray] ;
                                    newDate = date_part(''YEAR'', now()) || ''-'' || r.annual_month || ''-'' ||r.annual_day  ;
                                    newDate2 = fct_date2String(date(newDate) ) ;
                                    

                                    -- insert into the partner_schedul 
                                    sSql1 := ''insert into partner_schedul (id, uuid,status, process_status, partnerid ,schedul_staff_id, dschedul_date, dschedul_date_end,schedul_date, schedul_date_end, schedul_time_begin,schedul_time_end, short_remark, notes, schedul_uuid, schedul_insert_from) values (nextval('' || quote_literal(''partner_schedul_id'') || ''),'' || quote_literal(newUUID) || '', '' || quote_literal(''insert'') ||'',1,'' || idPartner || '', '' || aStaff[iArray] || '', '' || quote_literal(newDate) || '', '' || quote_literal(newDate ) || '', '' || quote_literal(newDate2 ) || '', '' || quote_literal(newDate2 ) || '', ''|| 0 ||'', '' || 0 || '', ''  || quote_literal( sServiceDesignation) ||'', ''|| quote_literal(sServiceNote) ||'', '' || quote_literal(scheduleUUID) || '', '' ||  iModule || '' ) '' ;
                                    -- raise notice ''sSql1 = %'', sSql1 ;
                                    execute (sSql1) ;
                                end loop ;
                            end if ;
                            
                        end if ; 
                    end if;
                end if ; 

            END LOOP ;
         END IF ;
        
        
        return idFound ;
    END ;
    

    
     ' LANGUAGE 'plpgsql'; 
     
