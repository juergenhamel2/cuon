#ifndef USER_HPP
    #define USER_HPP

    using namespace std;
    #include <iostream>
    #include <string>
    #include <map>
    #include <vector>
    #include <ctime>
    #include <gtkmm.h>
#include <cuon/TypeDefs/basics.hpp>


    class User : basics {

        public:
            map <string,string> sUser ;
            map <string,int> iUser ;
            map <string,float> fUser;
            map <string,string> sSqlUser ;
            map <string,int> iSqlUser ;
            map <string,float> fSqlUser;

            map <string,string> sServerData ;

            User();
            ~User();
            void init();
            void update();
            void refreshSqlUser();
            void clearServerData();

    };

#endif
