#include <gtk/gtk.h>


void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_chooseStaff_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_clear1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_fee_new1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_fee_edit1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_fee_save1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_fee_clear1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_eFindNumber_editing_done            (GtkCellEditable *celleditable,
                                        gpointer         user_data);

gboolean
on_eFindNumber_key_press_event         (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_eFindDesignation_editing_done       (GtkCellEditable *celleditable,
                                        gpointer         user_data);

gboolean
on_eFindDesignation_key_press_event    (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_tree1_row_activated                 (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data);

void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data);

void
on_bShowStaffDMS_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_misc_edit1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_misc_save1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_vacation_new1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_vacation_edit1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_vacation_delete1_activate           (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_disease_new1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_disease_edit1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_disease_delete1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_vacation_save1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_disease_save1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);
