#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_VatNew1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_VatEdit1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_VatSave1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_VatPrint1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_VatClear1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_chooseTaxVat_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_TopNew1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_TopEdit1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_TopSave1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_TopPrint1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_TopClear1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_chooseTop_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctInfoNew1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctInfoEdit1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctInfoSave1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctInfoPrint1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctInfoClear1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_choose_acct1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctPlanNew1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctPlanEdit1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctPlanSave1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctPlanPrint1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_AcctPlanClear1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_tree1_row_activated                 (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data)
{

}


void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data)
{

}


void
on_bImportAcct_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eAcctAcctPlan_changed               (GtkEditable     *editable,
                                        gpointer         user_data)
{

}

