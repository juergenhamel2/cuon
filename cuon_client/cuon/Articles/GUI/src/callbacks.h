#include <gnome.h>


gboolean
on_Mainwindow_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_choosePrinter1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_chooseArticle_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_print1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_clear1_activate                     (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_PurchaseNew1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_PurchaseEdit1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_PurchaseSave1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_PurchaseClear1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_SalesNew1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_SalesEdit1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_SalesSave1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_SalesClear1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_WebshopEdit1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_WebshopSave1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_StockEdit1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_StockSave1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_liArticlesNumber1_activate          (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_eFindNumber_editing_done            (GtkCellEditable *celleditable,
                                        gpointer         user_data);

gboolean
on_eFindNumber_key_press_event         (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_eFindDesignation_editing_done       (GtkCellEditable *celleditable,
                                        gpointer         user_data);

gboolean
on_eFindDesignation_key_press_event    (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);

void
on_bSearch_clicked                     (GtkButton       *button,
                                        gpointer         user_data);

void
on_tree1_row_activated                 (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data);

void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data);

void
on_eManufactorNumber_changed           (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_bChooseManufactor_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_eCategoryNr_changed                 (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_bChooseMaterialGroup_clicked        (GtkButton       *button,
                                        gpointer         user_data);

void
on_cbAssociatedWith_changed            (GtkComboBox     *combobox,
                                        gpointer         user_data);

void
on_bGotoAssociated_clicked             (GtkButton       *button,
                                        gpointer         user_data);

void
on_eAssoID_changed                     (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_bShowDMS_clicked                    (GtkButton       *button,
                                        gpointer         user_data);

void
on_bChooseVendor_clicked               (GtkButton       *button,
                                        gpointer         user_data);

void
on_eAddressNumber_changed              (GtkEditable     *editable,
                                        gpointer         user_data);

void
on_one_standard1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_two_standard1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_three_standard_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_four_standard_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_three_standard1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_four_standard1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_bSearchAssociated_clicked           (GtkButton       *button,
                                        gpointer         user_data);

void
on_sp101_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_sp102_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_sp201_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_sp202_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_sp301_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_sp302_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_sp401_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_sp402_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_eAssociatedNr_changed               (GtkEditable     *editable,
                                        gpointer         user_data);
