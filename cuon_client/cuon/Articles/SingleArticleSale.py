# -*- coding: utf-8 -*-
##Copyright (C) [2003]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

from cuon.Databases.SingleData import SingleData
import logging
import pygtk
pygtk.require('2.0')
import gtk
#import gtk.glade
import gobject
#from gtk import TRUE, FALSE
import locale
from locale import gettext as _


class SingleArticleSale(SingleData):

    
    def __init__(self, allTables):

        self.allTables = allTables
        SingleData.__init__(self)
        # tables.dbd and address
        self.sNameOfTable =  "articles_sales"
        self.xmlTableDef = 0
        # self.loadTable()
        # self.saveTable()

        self.loadTable(allTables)
        self.setStore( gtk.ListStore(gobject.TYPE_STRING,  gobject.TYPE_UINT) )
        self.listHeader['names'] = ['designation', 'ID']
        self.listHeader['size'] = [25,10,25,25,10]
        print "number of Columns "
        print len(self.table.Columns)
        #
        self.articlesNumber = 0

    def readNonWidgetEntries(self, dicValues):
        print 'readNonWidgetEntries(self) by SingleSales'
        dicValues['articles_number'] = [self.articlesNumber, 'int']
        return dicValues
        
        
    def fillOtherEntries(self, oneRecord):
        try:
            dt = cuon.DMS.documentTools.documentTools()
            sFile = dt.load_article_catalogue_pic(self.allTables,  self.articlesNumber)
            if sFile:
                print "image found"
                logo = self.getWidget("company_logo")
    #            
    #            newIm = Image.fromstring('RGB',[1024, 1024], bz2.decompress( image))
    #            newIm.thumbnail([208,208])
    #            sFile = self.dicUser['prefPath']['tmp'] + 'cuon_mainwindow_logo.png'
    #            save(sFile)
                print 'sFile = ',  sFile
                
                
                pixbuf = gtk.gdk.pixbuf_new_from_file(sFile)
                scaled_buf = pixbuf.scale_simple(208,208,gtk.gdk.INTERP_BILINEAR)
                logo.set_from_pixbuf(scaled_buf)
                logo.show()

        except:
            pass
       
    
