#include <gtk/gtk.h>


void
on_print_setup1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data);

void
on_bSend_clicked                       (GtkButton       *button,
                                        gpointer         user_data);

gboolean
on_tvQuestion_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data);
