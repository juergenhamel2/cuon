#ifdef HAVE_CONFIG_H
#  include <config.h>
#endif

#include <gtk/gtk.h>

#include "callbacks.h"
#include "interface.h"
#include "support.h"


gboolean
on_Mainwindow_key_press_event          (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_print_setup1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_quit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_new1_activate                       (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_edit1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_save1_activate                      (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_print_proposal1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_delete1_activate                    (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SupplyNew1_activate                 (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SupplyEdit1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SupplySave1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SupplyDelete1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_GetsNew1_activate                   (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_GetsEdit1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_GetsSave1_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_GetsDelete1_activate                (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_PositionNew1_activate               (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_PositionEdit1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_PositionSave1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_PositionPrint1_activate             (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_PositionDelete1_activate            (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SpecsEdit_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_SpecsSave_activate                  (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_Specs_delete1_activate              (GtkMenuItem     *menuitem,
                                        gpointer         user_data)
{

}


void
on_tbNew_clicked                       (GtkToolButton   *toolbutton,
                                        gpointer         user_data)
{

}


void
on_tbEdit_clicked                      (GtkToolButton   *toolbutton,
                                        gpointer         user_data)
{

}


void
on_tbSave_clicked                      (GtkToolButton   *toolbutton,
                                        gpointer         user_data)
{

}


void
on_tbInfo_clicked                      (GtkToolButton   *toolbutton,
                                        gpointer         user_data)
{

}


gboolean
on_bSearch_clicked                     (GtkWidget       *widget,
                                        GdkEventKey     *event,
                                        gpointer         user_data)
{

  return FALSE;
}


void
on_notebook1_switch_page               (GtkNotebook     *notebook,
                                        GtkNotebookPage *page,
                                        guint            page_num,
                                        gpointer         user_data)
{

}


void
on_bSearchCustom_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eAddressNumber_changed              (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bShowExtInfo_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eSupplyNumber_changed               (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bSearchSupply_clicked               (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_bSearchGet_clicked                  (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eGetsNumber_changed                 (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bSearchGetsPartner_clicked          (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eGetsPartner_changed                (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bSearchForwardingAgency_clicked     (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eForwardingAgency_changed           (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bContactPerson_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eContactPerson_changed              (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_eArticleID_changed                  (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bArticleSearch_clicked              (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_treeMaterialgroup_row_activated     (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data)
{

}


void
on_treeArticles_row_activated          (GtkTreeView     *treeview,
                                        GtkTreePath     *path,
                                        GtkTreeViewColumn *column,
                                        gpointer         user_data)
{

}


void
on_bQuickAppend_clicked                (GtkButton       *button,
                                        gpointer         user_data)
{

}


void
on_eInvoiceTOPID_changed               (GtkEditable     *editable,
                                        gpointer         user_data)
{

}


void
on_bInvoiceTOP_clicked                 (GtkButton       *button,
                                        gpointer         user_data)
{

}

