# -*- coding: utf-8 -*-

##Copyright (C) [20011]  [Jürgen Hamel, D-32584 Löhne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

import sys
import os
import os.path
import types
import pygtk
pygtk.require('2.0')
import gtk
#import gtk.glade
import gobject


import string

import logging

import cPickle
#import cuon.OpenOffice.letter
# localisation
import locale
from locale import gettext as _
import threading
import datetime as DateTime
from cuon.Windows.windows  import windows

import cuon.Misc.cuon_dialog
import cuon.ReportBuilder.reportbuilder
class report(windows):

    
    def __init__(self):
        windows.__init__(self)
        self.loadGlade('report.xml', 'ReportMainwindow')
        
        self.win1.show()
        self.ReportChooser = self.getWidget('ReportChooser')
        self.ReportChooser.hide()
        
        
    def on_quit_activate(self, event):
        self.closeWindow() 
        
        
    def on_edit_reports2_activate(self, event):
        self.editReport = 'TEXT'
        self.showReportDialog()
        
    def on_reports_gui_activate(self, event):
        self.editReport = 'GUI'
        self.showReportDialog()
        
    def showReportDialog(self):
        self.ReportChooser.show()
        liLists = self.rpc.callRP('Database.getComboReportLists',{'Name':'zope','client':-7}, '*.xml')
        print liLists
        
        cbDLists = self.getWidget('cbDLists')
        if cbDLists:
            liLists.sort()
            liststore = gtk.ListStore(str)
            for dList in liLists:
                liststore.append([dList])
            cbDLists.set_model(liststore)
            self.setComboBoxTextColumn(cbDLists)
            cbDLists.show()
            
        # Toolbar buttons, redirect to menu
    def on_tbEditGUI_clicked(self, event):
        self.activateClick('reports_gui')
        
        
    def on_tbEditTxt_clicked(self, event):
        self.activateClick('edit_reports2')
    def on_tbQuit_clicked(self, event):
        self.activateClick('quit')
        
        
    def startSSHEdit(self, infile, sType = 'text/x-text' ):
        self.openDB()
        td = self.loadObject('td')
        self.closeDB()
        
        #os.system('scp -P ' + td.sshPort + ' ' + td.sPrefix + '/etc/cuon/user.cfg inifiles')
        #ed = cuon.Editor.editor.editorwindow('inifiles/user.cfg', True)
        dicFilename = {'TYPE':'SSH', 'USER':td.ReportUser}
        dicFilename['HOST'] = td.server[td.server.find('://')+3:td.server.rfind(':')]
        dicFilename['PORT'] = `td.SSH_PORT`
        dicFilename['NAME'] = infile 
        
        print 'dicFilename = ', dicFilename
        
                 
        
        if self.editReport == 'GUI':
             rb = cuon.ReportBuilder.reportbuilder.reportbuilderwindow(dicFilename = dicFilename)
        else:   
            ed = cuon.Editor.editor.editorwindow(dicFilename, servermod = False)
            try: 
                ed.setLanguage(sType)
            except Exception, params:
                print Exception, params
             
         
    def  editReportFile(self, sName, sFolder):
        
        self.startSSHEdit('/usr/share/cuon/cuon_server/src/cuon/Reports/' +sFolder +'/' +sName, 'application/xml' ) 
        
    def readDDialogData(self):
        sFolder = self.getWidget('eDFolder').get_text()
        sList = self.getWidget('cbDLists').get_active_text()
        return sFolder, sList
        
    
    
    def on_bDSystem_clicked(self, event):
        sFolder, sList = self.readDDialogData()
        print sFolder,  sList
        self.ReportChooser.hide()
        self.editReportFile(sList.strip(),"XML")
        
    def on_bDClient_clicked(self, event):
        sFolder, sList = self.readDDialogData()
        print sFolder,  sList
        self.ReportChooser.hide()
        self.editReportFile(sList.strip(),"client_" + sFolder.strip())
        
    def on_bDUser_clicked(self, event):
        sFolder, sList = self.readDDialogData()
        print sFolder,  sList
        self.ReportChooser.hide()
        self.editReportFile(sList.strip(),"user_" + sFolder.strip())
    
    def on_bDCancel_clicked(self, event):
        self.ReportChooser.hide()
         
                  
