CREATE OR REPLACE FUNCTION fct_getUserStandardExe(sSuffix text) returns text as $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sImage text ;
        sSql text ;
	
	
	sPDFSuffix text ;
 	sExe text;
	sUserExe text ;
        vList text[]  ;
 	array_size integer ;

        BEGIN
		sUserExe := $$'NONE$$';
		vList[1] :=  $$'exe_oowriter,sxw,sdw,odt,ott,doc,rtf,$$';
        	vList := array_append(vList,$$'exe_pdf,pdf,$$') ;

		array_size := array_length(vList,1) ;

    	 
	        for ct in 1..array_size loop

		    if position(sSuffix in vlist[ct] ) >0 then 
		       raise notice $$'suffix found %$$',sSuffix ;
		       sExe = substring( vlist[ct],0, position($$',$$' in vlist[ct] ) );
		       raise notice $$'exe found %$$',sExe ;	
		       EXIT ;
		    end if ;
      
		end loop;  
    
		sSql = $$'select $$' || sExe || $$' from preferences where username = current_user and  is_standard_profile = $$'$$'t$$'$$'  $$';
		raise notice $$'sSql = % $$',sSql ;
		execute(sSql) into sUserExe ;
		return sUserExe ;
            
        END ;
    $$ LANGUAGE 'plpgsql'; 
 
CREATE OR REPLACE FUNCTION fct_getUserExe(dms_id int) returns text as $$   
 DECLARE
        iPaired_ID int ;
        iSearchID int ;
        sImage text ;
        sSql text ;
	sSuffix text;
	
	sPDFSuffix text ;
 	sExe text;
	sUserExe text ;
        vList text[]  ;
 	array_size integer ;

        BEGIN
		sUserExe := $$'NONE$$';
		vList[1] :=  $$'exe_oowriter,sxw,sdw,odt,ott,doc,rtf,$$';
        	vList := array_append(vList,$$'exe_pdf,pdf,$$') ;

		array_size := array_length(vList,1) ;

    	     	sSql := $$'select $$'$$',$$'$$' || file_suffix || $$'$$',$$'$$' from dms where id = $$' || dms_id ;
		raise notice $$'sSql = % $$',sSql ;
	     	execute(sSql) into sSuffix ;

	        for ct in 1..array_size loop

		    if position(sSuffix in vlist[ct] ) >0 then 
		       raise notice $$'suffix found %$$',sSuffix ;
		       sExe = substring( vlist[ct],0, position($$',$$' in vlist[ct] ) );
		       raise notice $$'exe found %$$',sExe ;	
		       EXIT ;
		    end if ;
      
		end loop;  
    
		sSql = $$'select $$' || sExe || $$' from preferences where username = current_user and  is_standard_profile = $$'$$'t$$'$$'  $$';
		raise notice $$'sSql = % $$',sSql ;
		execute(sSql) into sUserExe ;
		return sUserExe ;
            
        END ;
    $$ LANGUAGE 'plpgsql'; 
 
