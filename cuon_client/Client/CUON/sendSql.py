# -*- coding: utf-8 -*-
#!/usr/bin/python

##Copyright (C) [2003 -2012]  [Juergen Hamel, D-32584 Loehne]

##This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
##published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

##This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
##warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
##for more details.

##You should have received a copy of the GNU General Public License along with this program; if not, write to the
##Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 

import os, sys

  
SQL_DB = 'cuon'
SQL_USER = 'jhamel'
SQL_HOST = 'localhost'
SQL_PORT = '5432'

print sys.argv
if len(sys.argv) > 3: 
    if len(sys.argv[3]) > 1:
        SQL_DB =  sys.argv[3]
        
if len(sys.argv) > 2: 
    if len(sys.argv[2]) > 1:
        SQL_USER =  sys.argv[2]
        
if len(sys.argv) > 1: 
    if len(sys.argv[1]) > 1:
        configfile =  sys.argv[1]
        
        
        f = open(configfile, 'r')
        sSql = f.read()
        f.close()
        sSql = sSql.replace("$$", "'")
        f = open('runSql.sql', 'w')
        sSql = f.write(sSql)
        f.close()
        try:
            print sSql
            #print 'DatabaseValues = ',  SQL_DB, SQL_HOST, SQL_PORT, SQL_USER
        except:
            print 'local print is not working,  perhaps wrong charset'
        sysCommand =  'psql  ' + '-h ' + SQL_HOST + ' -p ' + SQL_PORT + ' -U ' + SQL_USER +   ' '  + SQL_DB + ' < runSql.sql'
        
        print sysCommand
       
        
        sReturn = os.system(sysCommand)
        
