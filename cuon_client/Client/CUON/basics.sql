-- Define some vars   

CREATE TYPE return_text3 AS (a text, b text, c text); 
CREATE TYPE return_float3 AS (a float, b float, c float);
CREATE TYPE return_misc3 AS (a int, b float, c text);
 
 -- All this here are for trigger or very basic funtions
 
  
       
 DROP FUNCTION fct_cuon_insert() CASCADE ;
 
 CREATE OR REPLACE FUNCTION fct_cuon_insert( ) returns OPAQUE as $$

  BEGIN
          NEW.uuid  := fct_new_uuid() ;
          
        RETURN NEW; 
    END;
     
    $$ LANGUAGE 'plpgsql'; 


 DROP FUNCTION fct_insert() CASCADE ;
 
 CREATE OR REPLACE FUNCTION fct_insert( ) returns OPAQUE as $$
    --  set default values at an insert operation 
     
    BEGIN
        NEW.user_id = current_user   ;
        NEW.insert_time = (select  now()) ;
        NEW.status = $$'insert$$' ;
        NEW.versions_uuid = fct_new_uuid()   ;
        NEW.versions_number = 1 ;
        -- RAISE NOTICE $$'Name =  % $$', TG_NAME ;
        
        IF TG_TABLE_NAME = $$'list_of_invoices$$' THEN 
                NEW.last_transaction_uuid = fct_write_invoice(NEW.uuid) ;
               
              
            END IF ;
        RETURN NEW; 
    END;
     
    $$ LANGUAGE 'plpgsql'; 

    
 DROP FUNCTION fct_update() CASCADE ;
 
 CREATE OR REPLACE FUNCTION fct_update( ) returns OPAQUE as $$
    --  set default values at an update operation 
     
    DECLARE
        history_table   varchar(400) ;
        cName           varchar(300) ;
        sText text ;
        ri RECORD;
        q record ;
	t TEXT;
    sSql text ;
    sSql2 text ;
        sExe text ;
        sExe2 text ;
        len integer ;
        
    BEGIN
        history_table := TG_TABLE_NAME || $$'_history$$' ;
        
       if NEW.uuid is NULL OR char_length(NEW.uuid ) <36 then 
            NEW.uuid := fct_new_uuid()   ;
        end if ;
        
        if OLD.versions_uuid is NULL OR char_length(OLD.versions_uuid ) <36 then 
            OLD.versions_uuid := fct_new_uuid()   ;
        end if ;  
        if OLD.versions_number is NULL OR OLD.versions_number  < 1 then 
                OLD.versions_number := 1 ;
        end if ; 
 
	if OLD.status is NULL then 
	   OLD.status = $$'insert$$';
	end if ;
	-- raise notice $$'Old.status = %$$', OLD.status ;
        if not OLD.status = $$'delete$$' then 
        
            sExe := $$' insert into $$' || history_table || $$' ( $$' ;
            sExe2 := $$' select  $$' ;
            sSql := $$' SELECT ordinal_position, column_name, data_type  FROM information_schema.columns   WHERE    table_schema = $$' || quote_literal(TG_TABLE_SCHEMA) || $$'  AND table_name = $$' || quote_literal(TG_TABLE_NAME) || $$' ORDER BY ordinal_position $$' ;
            
            
            FOR ri in execute(sSql)  
               
                LOOP
                    
                   
                    sExe := sExe || ri.column_name || $$', $$' ;
                    sExe2 := sExe2 || ri.column_name || $$', $$' ;
                  
                    
                END LOOP;
            len := length(sExe);    
            sExe := substring(sExe from 1 for (len-2) ) ;
            sExe := sExe || $$' ) $$' ;
            
            len := length(sExe2);    
            sExe2 := substring(sExe2 from 1 for (len-2) ) ;
            
            
            
            sExe := sExe || sExe2 || $$' from $$' || quote_ident(TG_TABLE_NAME) || $$' where id = $$' || OLD.id ;
            raise notice $$' sExe = %$$', sExe ;
            execute sExe ;
            
            NEW.update_user_id := current_user   ;
            NEW.update_time := (select  now()) ;
            NEW.user_id := OLD.user_id;
            
            NEW.insert_time := OLD.insert_time ;
            
           
            -- NEW.status := OLD.status ;
           
            
            NEW.versions_number := OLD.versions_number+1 ;
            NEW.versions_uuid :=OLD.versions_uuid ;
            IF TG_TABLE_NAME = $$'list_of_invoices$$' THEN 
                NEW.last_transaction_uuid = fct_write_invoice(NEW.uuid) ;
               
              
            END IF ;
            
            -- raise notice $$'new = % %$$', NEW.uuid, NEW.status ;
            
            RETURN NEW; 
        else 
	     -- raise notice $$'return old = %$$',OLD.status ;
            RETURN OLD;
       
       end if ;
      
    END;
    
  
    $$ LANGUAGE 'plpgsql'; 

    
 DROP FUNCTION fct_delete() CASCADE ;
 
CREATE OR REPLACE FUNCTION fct_delete( ) returns OPAQUE as $$
    --  set default values at a delete 
    -- if delete a record, dont realy delete, set status to delete 
     
    DECLARE
    f_upd     varchar(400);
    v_delete varchar(20) ;
    BEGIN
        v_delete   := $$'delete$$' ;
        f_upd := $$' update $$' || TG_RELNAME  || $$' set status =  $$'|| quote_literal(v_delete) || $$'  where id = $$' || OLD.id   ;
        -- RAISE NOTICE $$' table-name =  % $$', TG_RELNAME ;
        -- RAISE NOTICE $$' sql =  % $$',f_upd  ;
        execute f_upd ;
        -- RAISE NOTICE $$' Name =  % $$', TG_NAME ;
        RETURN NULL ;
    END;
     
     $$ LANGUAGE 'plpgsql'; 
 

 
     DROP FUNCTION fct_orderposition_insert() CASCADE ;
     
     CREATE OR REPLACE FUNCTION fct_orderposition_insert( ) returns OPAQUE as $$
    --  set default values to orderposition 
     
    
    BEGIN
               IF NEW.tax_vat is NULL THEN
                    NEW.tax_vat = 0.00 ;
               end if ;
               IF NEW.discount is NULL THEN
                    NEW.discount = 0.00 ;
               end if ;
               RETURN NEW; 
    END;
  
     $$ LANGUAGE 'plpgsql'; 
     
  
   DROP FUNCTION fct_orderbook_delete_invoice() CASCADE ;
     
     CREATE OR REPLACE FUNCTION fct_orderbook_delete_invoice( ) returns OPAQUE as $$
    --  remove all invoice for this deleted order
     DECLARE
    sSql text ;
    r2 record ;
    
    
    BEGIN
               sSql = $$'select * from list_of_invoices where order_number = $$' || OLD.id ;
               FOR r2 in execute(sSql)  LOOP
               
                -- raise notice $$' delete this invoice : % $$', r2.id ;
               END LOOP ;
               
                
               RETURN NEW; 
    END;
  
     $$ LANGUAGE 'plpgsql'; 
     
    

  
   DROP FUNCTION fct_partner_schedul_insert( ) CASCADE ;
     
     CREATE OR REPLACE FUNCTION fct_partner_schedul_insert( ) returns OPAQUE as $$
    --  set default values to orderposition 
     BEGIN
        NEW.user_id = current_user   ;
        NEW.insert_time = (select  now()) ;
        NEW.status = $$'insert$$' ;
        NEW.sep_info_3 = 5 ;
        RAISE NOTICE $$'Name =  % $$', TG_NAME ;
        RETURN NEW; 
    END;
    
  
     $$ LANGUAGE 'plpgsql'; 
     
     
   
DROP FUNCTION  fct_partner_schedul_change()  CASCADE ;
CREATE OR REPLACE FUNCTION fct_partner_schedul_change( ) returns OPAQUE as $$

    BEGIN
    
        if OLD.status is null then 
            OLD.status := $$'insert$$' ;
            NEW.status := $$'insert$$' ;
        end if ;
        
        if not OLD.status = $$'delete$$' then 
            NEW.update_user_id = current_user   ;
            NEW.update_time = (select  now()) ;
            NEW.user_id = OLD.user_id;
            NEW.insert_time = OLD.insert_time ;
            NEW.status = $$'update$$' ;
            if NEW.sep_info_3 = 2 then 
                NEW.sep_info_3 = 0 ;
            else  
                NEW.sep_info_3 = 5 ;
            end if ;
            RAISE NOTICE $$'Name =  % $$', TG_NAME ;
            RETURN NEW; 
      else 
            RETURN OLD;
      end if ;
    END;
    
     $$ LANGUAGE 'plpgsql'; 
     
     

DROP FUNCTION fct_pickup_schedul_insert( ) CASCADE ;
     
CREATE OR REPLACE FUNCTION fct_pickup_schedul_insert( ) returns OPAQUE as $$
    --  set default values to orderposition 
   DECLARE   
      sSql text ;
      gets_partner_id int ;
      r1 record ;
      r2 record ;
      newUUID varchar(36);

   BEGIN
         gets_partner_id := 0 ;
        NEW.user_id = current_user   ;
        NEW.insert_time = (select  now()) ;
        NEW.status = $$'insert$$' ;
        NEW.sep_info_3 = 5 ;
        RAISE NOTICE $$'Name =  % $$', TG_NAME ;
	if NEW.gets_staff_id is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_date is null then 
	   return NEW ;
	end if ;

	if NEW.gets_staff_time is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_id = 0 then 
	   return NEW ;
	end if ;

	if NEW.partnernumber is null or NEW.partnernumber < 1 then 

		sSql := $$'select orderbook.customers_partner_id as partner_id, orderbook.addressnumber as address_id, orderbook.designation as designation from orderbook where orderbook.id = $$' || NEW.orderid  ;
		RAISE NOTICE $$'sSql0  =  % $$', sSql ; 
		execute sSql into r1 ;
		RAISE NOTICE $$'Vars0 = % % % $$', r1.partner_id,r1.address_id, r1.designation ;
		if r1.partner_id is not null then
		   if r1.partner_id > 0 then 
	   	      gets_partner_id = r1.partner_id ;
                   end if ;
	         end if ;
	 	if r1.designation is null then 
		   r1.designation = $$'PICKUP$$' ;
	      	end if ;
	RAISE NOTICE $$'Vars = % % % $$', r1.partner_id,r1.address_id, r1.designation ;

	if gets_partner_id = 0 then
	   	sSql := $$'select min(partner.id) as partner_id from partner where partner.addressid = $$' || r1.address_id || $$' and status != $$' || quote_literal($$'delete$$') ;
		RAISE NOTICE $$'sSql1  =  % $$', sSql ;
		execute sSql into r2 ;
		if r2.partner_id is not null then 
		   gets_partner_id = r2.partner_id ;
		end if ;
	end if ;    
	raise notice $$' r1.addressid = %$$', r1.address_id;

	if gets_partner_id = 0 then
	   gets_partner_id =  fct_generatePartner(r1.address_id);
	end if ;

	else
	   gets_partner_id = NEW.partnernumber ;
	   sSql := $$'select  orderbook.designation as designation from orderbook where orderbook.id = $$' || NEW.orderid  ;
		RAISE NOTICE $$'sSql0  =  % $$', sSql ; 
		execute sSql into r1 ;
		if r1.designation is null then 
		   r1.designation = $$'PICKUP$$' ;
	      	end if ;
	end if ;
	
	newUUID =  fct_new_uuid() ;	 
	NEW.schedul_uuid = newUUID ;

	   
	sSql := $$'insert into partner_schedul (id, uuid,partnerid,schedul_staff_id, short_remark,dschedul_date,schedul_date, schedul_time_begin,dschedul_date_end,schedul_date_end, schedul_time_end ) values(nextval($$' || quote_literal($$'partner_schedul_id$$') || $$'),$$' || quote_literal(newUUID) || $$', $$' || gets_partner_id || $$', $$' || NEW.gets_staff_id || $$', $$' || quote_literal( r1.designation ) || $$', $$' || quote_literal(NEW.gets_staff_date)|| $$', $$' || quote_literal(NEW.gets_staff_date) ||$$', $$' || NEW.gets_staff_time  || $$', $$' || quote_literal(NEW.gets_staff_date)|| $$', $$' || quote_literal(NEW.gets_staff_date) ||$$', $$' || NEW.gets_staff_time +1  || $$' )$$';
	RAISE NOTICE $$'sSql2  =  % $$', sSql ;      
	execute(sSql);
        RETURN NEW; 
   END;
    
  
   $$ LANGUAGE 'plpgsql'; 
       


DROP FUNCTION fct_pickup_schedul_update( ) CASCADE ;
     
CREATE OR REPLACE FUNCTION fct_pickup_schedul_update( ) returns OPAQUE as $$
    --  set default values to orderposition 
   DECLARE   
      sSql text ;
      gets_partner_id int ;
      r1 record ;
      r2 record ;
      newUUID varchar(36);

   BEGIN
	if NEW.gets_staff_id is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_date is null then 
	   return NEW ;
	end if ;

	if NEW.gets_staff_time is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_id = 0 then 
	   return NEW ;
	end if ;

   if NEW.schedul_uuid is not null then 
      gets_partner_id := 0 ;
	if NEW.partnernumber is null or NEW.partnernumber < 1 then 
	sSql := $$'select orderbook.customers_partner_id as partner_id, orderbook.addressnumber as address_id, orderbook.designation as designation from orderbook where orderbook.id = $$' || NEW.orderid  ;
	RAISE NOTICE $$'sSql0  =  % $$', sSql ; 
	execute sSql into r1 ;
	RAISE NOTICE $$'Vars0 = % % % $$', r1.partner_id,r1.address_id, r1.designation ;
	if r1.partner_id is not null then
	   if r1.partner_id > 0 then 
	      gets_partner_id = r1.partner_id ;
           end if ;
        end if ;

	if r1.designation is null then 
	   r1.designation = $$'PICKUP$$' ;
	end if ;

	RAISE NOTICE $$'Vars = % % % $$', r1.partner_id,r1.address_id, r1.designation ;
	if gets_partner_id = 0 then
	   	sSql := $$'select min(partner.id) as partner_id from partner where partner.addressid = $$' || r1.address_id || $$' and status != $$' || quote_literal($$'delete$$') ;
		RAISE NOTICE $$'sSql1  =  % $$', sSql ;
		execute sSql into r2 ;
		if r2.partner_id is not null then 
		   gets_partner_id = r2.partner_id ;
		end if ;
	end if ;    

	if gets_partner_id = 0 then
	   gets_partner_id =  fct_generatePartner(r1.address_id);
	end if ;

	else
	   gets_partner_id = NEW.partnernumber ;
	   sSql := $$'select  orderbook.designation as designation from orderbook where orderbook.id = $$' || NEW.orderid  ;
		RAISE NOTICE $$'sSql0  =  % $$', sSql ; 
		execute sSql into r1 ;
		if r1.designation is null then 
		   r1.designation = $$'PICKUP$$' ;
	      	end if ;
	end if ;
	
      sSql := $$'update partner_schedul set  partnerid =  $$' || gets_partner_id || $$' ,schedul_staff_id = $$' || NEW.gets_staff_id || $$' , short_remark =  $$' || quote_literal( r1.designation ) || $$',dschedul_date =  $$' || quote_literal(NEW.gets_staff_date)|| $$' ,schedul_date = $$' || quote_literal(NEW.gets_staff_date)|| $$', schedul_time_begin = $$' || NEW.gets_staff_time  || $$',dschedul_date_end = $$' || quote_literal(NEW.gets_staff_date)|| $$',schedul_date_end = $$' || quote_literal(NEW.gets_staff_date)|| $$', schedul_time_end = $$' || NEW.gets_staff_time + 1  || $$' where uuid = $$' || quote_literal( NEW.schedul_uuid)  ;
      execute sSql ;

      else 

      gets_partner_id := 0 ;
        NEW.user_id = current_user   ;
        NEW.insert_time = (select  now()) ;
        NEW.status = $$'update$$' ;
        NEW.sep_info_3 = 5 ;
        RAISE NOTICE $$'Name =  % $$', TG_NAME ;
	if NEW.gets_staff_id is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_date is null then 
	   return NEW ;
	end if ;

	if NEW.gets_staff_time is null then 
	   return NEW ;
	end if ;
	if NEW.gets_staff_id = 0 then 
	   return NEW ;
	end if ;

	 if NEW.schedul_uuid is not null then 
	sSql := $$'select orderbook.customers_partner_id as partner_id, orderbook.addressnumber as address_id, orderbook.designation as designation from orderbook where orderbook.id = $$' || NEW.orderid  ;
	RAISE NOTICE $$'sSql0  =  % $$', sSql ; 
	execute sSql into r1 ;
	RAISE NOTICE $$'Vars0 = % % % $$', r1.partner_id,r1.address_id, r1.designation ;
	if r1.partner_id is not null then
	   if r1.partner_id > 0 then 
	      gets_partner_id = r1.partner_id ;
           end if ;
        end if ;

	if r1.designation is null then 
	   r1.designation = $$'PICKUP$$' ;
	end if ;

	RAISE NOTICE $$'Vars = % % % $$', r1.partner_id,r1.address_id, r1.designation ;
	if gets_partner_id = 0 then
	   	sSql := $$'select min(partner.id) as partner_id from partner where partner.addressid = $$' || r1.address_id || $$' and status != $$' || quote_literal($$'delete$$') ;
		RAISE NOTICE $$'sSql1  =  % $$', sSql ;
		execute sSql into r2 ;
		if r2.partner_id is not null then 
		   gets_partner_id = r2.partner_id ;
		end if ;
	end if ;    
	raise notice $$' r1.addressid = %$$', r1.address_id;
	if gets_partner_id = 0 then
	   gets_partner_id =  fct_generatePartner(r1.address_id);
	end if ;
	
	else
	   gets_partner_id = NEW.partnernumber ;
	   sSql := $$'select  orderbook.designation as designation from orderbook where orderbook.id = $$' || NEW.orderid  ;
		RAISE NOTICE $$'sSql0  =  % $$', sSql ; 
		execute sSql into r1 ;
		if r1.designation is null then 
		   r1.designation = $$'PICKUP$$' ;
	      	end if ;
	end if ;
	
	newUUID =  fct_new_uuid() ;	 
	NEW.schedul_uuid = newUUID ;

	   
	sSql := $$'insert into partner_schedul (id, uuid,partnerid,schedul_staff_id, short_remark,dschedul_date,schedul_date, schedul_time_begin,dschedul_date_end,schedul_date_end, schedul_time_end ) values(nextval($$' || quote_literal($$'partner_schedul_id$$') || $$'),$$' || quote_literal(newUUID) || $$', $$' || gets_partner_id || $$', $$' || NEW.gets_staff_id || $$', $$' || quote_literal( r1.designation ) || $$', $$' || quote_literal(NEW.gets_staff_date)|| $$', $$' || quote_literal(NEW.gets_staff_date) ||$$', $$' || NEW.gets_staff_time  || $$', $$' || quote_literal(NEW.gets_staff_date)|| $$', $$' || quote_literal(NEW.gets_staff_date) ||$$', $$' || NEW.gets_staff_time +1  || $$' )$$';
	RAISE NOTICE $$'sSql2  =  % $$', sSql ;      
	execute(sSql);
        
       
       end if ;


       return NEW ;

   END;
    
  
   $$ LANGUAGE 'plpgsql'; 
       


DROP FUNCTION fct_pickup_schedul_delete( ) CASCADE ;
     
CREATE OR REPLACE FUNCTION fct_pickup_schedul_delete( ) returns OPAQUE as $$
    --  set default values to orderposition 
   DECLARE   
      sSql text ;
      gets_partner_id int ;
      r1 record ;
      r2 record ;
      newUUID varchar(36);

   BEGIN
	if NEW.schedul_uuid is not null then 
	     sSql = $$'delete from partner_schedul where uuid = $$'  || quote_literal( NEW.schedul_uuid)  ;
	     execute sSql ;
	end if ;

	return NEW ;
   END;
    
  
   $$ LANGUAGE 'plpgsql'; 
       
 

  CREATE OR REPLACE FUNCTION fct_checkUserProfile( sUser char(50), sClient char(10) ) returns bool as $$
    -- set the userdata for the current work
     
    DECLARE
    sSql text ;
    sSql1 text ;  
    r1 record ;
    ok boolean ;
    newUUID char(36) ;

   BEGIN
	-- test if standard profile exists
	ok = false ;
	newUUID = fct_null_uuid();
	raise notice $$'newUUID 1  = % $$',newUUID ;
	sSql := $$'select is_standard_profile, uuid, id  from preferences where username = $$' || quote_literal(sUser) || $$' and client = $$' || sClient  || $$' and status != $$'|| quote_literal($$'delete$$') ;
	
	for r1 in execute sSql  
	LOOP
		if r1.uuid is null then 
		   newUUID =  fct_new_uuid();
		   sSql = $$'update preferences set uuid = $$' || quote_literal(newUUID) ||$$'  where id = $$' || r1.id ;
		   execute sSql ;
		   r1.uuid := newUUID ;
    	   end if;
		   

		if r1.is_standard_profile is not null and r1.is_standard_profile = true then 
		   ok = true ;
		   
		   newUUID = r1.uuid ;
	raise notice $$'newUUID 2 = % $$',newUUID ;
		end if ;
	
	

	END LOOP ;
	if ok = false then 
	   newUUID = fct_new_uuid();
	   sSql = $$'insert into preferences(id,uuid,username,client) values ( select nextval($$' || quote_literal($$'preferences_id$$') || $$', $$' || quote_literal(newUUID) || $$', $$' ||  quote_literal(sUser) || $$', $$' || sClient || $$') $$' ;
	   raise notice $$'sSql-11 = % $$',sSql ;
	raise notice $$'newUUID 3 = % $$',newUUID ;
	end if ;
	
	   
	-- set missing values
	raise notice $$'newUUID 4 = % $$',newUUID ;
	sSql := $$'select * from preferences where uuid = $$' || quote_literal(newUUID)  ;
	raise notice $$'sSql-14 = % $$',sSql ;

	for r1 in execute sSql
	
	LOOP
		-- dms vars
		if r1.exe_oowriter is null then 
		   sSql1 := $$'update preferences set exe_oowriter = $$'$$'/usr/bin/libreoffice$$'$$' where id = $$' || r1.id ;
		   execute sSql ;
		end if ;
  		if r1.exe_oocalc is null then 
		   sSql1 := $$'update preferences set exe_oocalc = $$'$$'/usr/bin/libreoffice$$'$$' where id = $$' || r1.id ;
		   execute sSql ;
		end if ;


	END LOOP ;
        return 1 ;
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
     
  
  CREATE OR REPLACE FUNCTION fct_setUserData( dicUser char  [] ) returns bool as $$
    -- set the userdata for the current work
     
    DECLARE
    sSql text ;
    sName char(50) ;    
    sClient char(10) ;
    sNoWhereClient char(10) ;
    sLocales char(10) ;
    SQLDateFormat char(20) ;
    SQLTimeFormat char(20) ;
    SQLDateTimeFormat char(20) ;
    
    
    iNextID     int ;

    BEGIN
        
        sName := dicUser[1];
        sClient := dicUser[2];
        sNoWhereClient := dicUser[3];
        sLocales  := dicUser[4];
        SQLDateFormat  := dicUser[5];
        SQLTimeFormat := dicUser[6];
        SQLDateTimeFormat := dicUser[7];
        

        execute $$'select * from  fct_checkUserProfile($$' ||  quote_literal(sName) || $$', $$' ||  quote_literal(sClient)||$$')$$' ;
        execute $$'delete from cuon_user where username = $$' || quote_literal(sName )  ;
        select into iNextID nextval from  nextval($$'cuon_user_id$$'); 

	sSql = $$'insert into  cuon_user (id,username, current_client, no_where_client ,user_locales, user_sql_date_format,user_sql_time_format,user_sql_date_time_format ) VALUES ($$' || iNextID || $$', $$' || quote_literal(sName) || $$',$$' || quote_literal(sClient)  || $$',$$' || quote_literal(sNoWhereClient) ||$$',$$' || quote_literal( sLocales) ||$$',$$' ||  quote_literal(SQLDateFormat) || $$',$$' ||  quote_literal(SQLTimeFormat) ||$$',$$' || quote_literal( SQLDateTimeFormat) || $$' )$$' ;
	raise notice $$'sSql = %$$',sSql ;
	execute (sSql);
      

        return 1 ;
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
     
     
     
   CREATE OR REPLACE FUNCTION  fct_getUserDataClient(  ) returns int as $$
   -- get the client id
    DECLARE
    iClient    int ;
    sClient char ;
    BEGIN
        
        select into iClient current_client from cuon_user where username = CURRENT_USER ;
        -- raise notice $$'Client id = %$$', iClient ;
	if iClient is NULL then 
	   iclient = 1;
	  End IF ;

        return iClient ;
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
     
   CREATE OR REPLACE FUNCTION  fct_getUserDataLocales(  ) returns text as $$
   -- get the client id
    DECLARE
    sData text ;
    sClient char ;
    BEGIN
    
        select into sData user_locales from cuon_user where username = CURRENT_USER ;
        raise notice $$'locales = %$$', sData ;
        return sData ;
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
        

     
   CREATE OR REPLACE FUNCTION  fct_getUserDataNoWhereClient(  ) returns int  as $$
       -- get the nowhereclient id
     DECLARE
    iClient    int ;
    sClient char ;
    BEGIN
    
        select into iClient no_where_client from cuon_user where username = CURRENT_USER ;
        -- raise notice $$'NoWhereClient id = %$$', iClient ;
        return iClient ;
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
     
     
   CREATE OR REPLACE FUNCTION  fct_getWhere(iSingle int,  sPrefix char ) returns text  as $$
    -- retuns the sWhere value
     DECLARE
    sWhere  text ;
    iClient int ;
    iNoWhereClient int ;

    BEGIN
        iClient := fct_getUserDataClient();
        iNoWhereClient := fct_getUserDataNoWhereClient();
        
    sWhere := $$' $$' ;
    if iNoWhereClient = $$'1$$'
    then
        if iSingle = 1
        then
             sWhere := $$'WHERE $$' ||  sPrefix  || $$'client = $$' || iClient || $$' and $$' || sPrefix  || $$'status != $$'$$'delete$$'$$' $$' ;
                    

        else 
            if iSingle = 2   then
                sWhere := $$'AND $$' ||  sPrefix  || $$'client = $$' || iClient || $$' and $$' || sPrefix  || $$'status != $$'$$'delete$$'$$' $$' ;
            END IF ;
        END IF ;
        
    END IF ;

  
    -- RAISE NOTICE $$' sWhere  = %$$', sWhere ;
    RETURN sWhere ;
    END ;
     $$ LANGUAGE 'plpgsql'; 
     
    
    CREATE OR REPLACE FUNCTION fct_new_uuid()   RETURNS char(36) as $$
    DECLARE
    
    this_uuid char(36) ;
    new_md5 char(32) ;
    BEGIN
        SELECT into new_md5 md5(current_database()|| user ||current_timestamp ||random() ) ;
        -- 8 4 4 4 12
        this_uuid = substring(new_md5 from 1 for 8) || $$'-$$' || substring(new_md5 from 9 for 4) || $$'-$$' || substring(new_md5 from 13 for 4) || $$'-$$' || substring(new_md5 from 17 for 4) || $$'-$$' || substring(new_md5 from 21 for 12) ;
    
        --raise notice $$'new uuid $$', this_uuid ;
       
        
        RETURN this_uuid ;
        
    END ;
    $$ LANGUAGE 'plpgsql'; 

     CREATE OR REPLACE FUNCTION fct_null_uuid()   RETURNS char(36) as $$
    DECLARE
    
    this_uuid char(36) ;
    new_md5 char(32) ;
    BEGIN
    
      this_uuid := $$'00000000-0000-0000-0000-000000000000$$' ;
    
      RETURN this_uuid ;
        
    END ;
    $$ LANGUAGE 'plpgsql'; 
    
    
    DROP FUNCTION fct_new_uuid_for_ticket_number() CASCADE ; 
    CREATE OR REPLACE FUNCTION fct_new_uuid_for_ticket_number()   RETURNS OPAQUE as $$
    DECLARE
    
    this_uuid char(36) ;
    
    BEGIN
       NEW.ticket_number = fct_new_uuid() ;
       
       RETURN NEW ;
        
    END ;
    $$ LANGUAGE 'plpgsql'; 

    
DROP FUNCTION fct_new_cash_desk_special_id() CASCADE ; 
CREATE OR REPLACE FUNCTION fct_new_cash_desk_special_id()   RETURNS OPAQUE as $$
    DECLARE
    
    this_id int ;
    old_id int ;
    iClient int;
    
    BEGIN
       
       iClient := 0;
       iClient := fct_getUserDataClient();
       select into old_id max(special_id) from cash_desk_book_number where client = iClient ;
       if old_id is NULL then
       	  old_id = 0;
       end if ;

      
       NEW.special_id = old_id + 1;
       NEW.real_timestamp = now() ;
       
       RETURN NEW ;
        
    END ;
    $$ LANGUAGE 'plpgsql'; 

 
CREATE OR REPLACE FUNCTION fct_to_date( sDate text) returns date as $$
    
    DECLARE
        dDate date ;
        sDateFormat text ;
        
    BEGIN
        select into sDateFormat type_c from cuon_values  where name = $$'SQLDateFormat$$'  ;
        
        select into dDate to_date(sDate,sDateFormat);
        RETURN dDate ;
    END;
     
     $$ LANGUAGE 'plpgsql'; 
 
DROP FUNCTION   fct_date2String( date) ;
CREATE OR REPLACE FUNCTION fct_date2String( dDate date) returns text as $$
    
    DECLARE
        sDate text ;
        sDateFormat text ;
        
    BEGIN
        select into sDateFormat type_c from cuon_values  where name = $$'SQLDateFormat$$'  ;
        
        select into sDate to_char(dDate,sDateFormat);
        RETURN sDate ;
    END;
     
     $$ LANGUAGE 'plpgsql';   
   
DROP FUNCTION     fct_getDays( date) ;
CREATE OR REPLACE FUNCTION fct_getDays( dDate date) returns int as $$
    
    DECLARE
      days int := 0 ;
        
    BEGIN
        select into days date_part($$'days$$',now() - dDate)  ;
        
        if days is not null then 
            RETURN days ;
        else 
            RETURN 0 ;
        END IF ;
    END;
     
     $$ LANGUAGE 'plpgsql';   
   
DROP FUNCTION      fct_getFirstDayOfMonth(date);
CREATE OR REPLACE FUNCTION fct_getFirstDayOfMonth(dDate date) returns int as $$
    
    DECLARE
      iMonth int ;

    BEGIN
        iMonth := extract(month from dDate)   ;
        return iMonth ;
    END; 
        $$ LANGUAGE 'plpgsql'; 

   
DROP FUNCTION      fct_getLastYear(date);
CREATE OR REPLACE FUNCTION fct_getLastYear(dDate date) returns int as $$
    
    DECLARE
      iYear int ;

    BEGIN
        iYear := extract(year from dDate)   ;
	raise notice $$' year = %, year -1 = % $$', iYear, iYear -1 ;
        return iYear -1 ;
    END; 
        $$ LANGUAGE 'plpgsql'; 
    

CREATE OR REPLACE FUNCTION fct_getFirstDayOfMonth(iMonth int, iYear int) returns date as $$
    
    DECLARE
        firstDay date ;
      
    BEGIN
        firstDay := date($$'01-$$' || iMonth || $$'-$$' || iYear ) ;
        RETURN firstDay ; 
    END; 
        $$ LANGUAGE 'plpgsql'; 
    
    
    
DROP FUNCTION  fct_getFirstDayOfMonth(date) ;
CREATE OR REPLACE FUNCTION fct_getFirstDayOfMonth(dDate date) returns date as $$
    
    DECLARE
        ret_val date ;
        firstDay date ;
        sDay date ;
        sSql text ;
    BEGIN
        sDay := $$' 01-$$' || extract(month from dDate) ||$$'-$$' || extract(year from dDate)  ;
        sSql := $$'SELECT date( $$' || quote_literal(sDay) ||  $$') $$' ; 
        execute sSql into ret_val ;
        RETURN ret_val; 
   
    END;
     
     $$ LANGUAGE 'plpgsql'; 
     
     
CREATE OR REPLACE FUNCTION fct_getLastDayOfMonth(dDate date) returns date as $$
    
    DECLARE
        ret_val date ;
        firstDay date ;
        
    BEGIN
        
        SELECT date(substr(text(dDate + interval $$'1 month$$'),1,7)||$$'-01$$')-1 into ret_val; 
        
        RETURN ret_val; 
   
    END;
     
     $$ LANGUAGE 'plpgsql'; 

CREATE OR REPLACE FUNCTION fct_getLastDayOfMonth(iMonth int, iYear int) returns date as $$
    
    DECLARE
        ret_val date ;
        firstDay date ;
        
    BEGIN
        firstDay := date($$'01-$$' || iMonth || $$'-$$' || iYear ) ;
        SELECT date(substr(text(firstDay + interval $$'1 month$$'),1,7)||$$'-01$$')-1 into ret_val; 
        
        RETURN ret_val; 
   
    END;
     
     $$ LANGUAGE 'plpgsql'; 
     
     
CREATE OR REPLACE  FUNCTION fct_add_months(date, integer) RETURNS DATE as $$ 
    DECLARE 
       base_date ALIAS FOR $1 ; 
       add_days ALIAS FOR $2 ; 
       ret_val DATE ; 
       sSql text ;
       
    BEGIN 
        sSql := $$'SELECT date(date($$' || quote_literal(base_date) || $$') + interval  $$' || quote_literal(add_days || $$' month$$' ) || $$') $$' ;
        execute sSql into ret_val ;
       return ret_val; 
    END; 

    $$ LANGUAGE 'plpgsql';
    


CREATE OR REPLACE FUNCTION fct_getTimeFromCheckbox( iTime int) returns text as $$
    
    DECLARE
      hours int;
      minutes int;
      sTime text;
       
    BEGIN
       
	hours = (iTime / 4);
	minutes = mod(iTime, 4) * 15;

	sTime = to_char(hours,$$'FM00$$') || $$':$$' || to_char(minutes,$$'FM00$$') ;

       
        RETURN sTime ;
       
    END;
     
     $$ LANGUAGE 'plpgsql';     
   
CREATE OR REPLACE  FUNCTION fct_write_config_value( iClientID int, sFile varchar(200), cpSection varchar(250), sOptions  varchar(250), sValue text) RETURNS boolean as $$ 
    DECLARE 
     r1 record ;
     ok boolean ;
    BEGIN 
        ok = true ;
        select into r1  key_id  from cuon_config where client_id = iClientID and config_file = sFile   and  section = cpSection and  option = sOptions ;
        IF r1.key_id IS NULL THEN 
            insert into cuon_config (client_id, config_file , section , option, value ) values (iClientID, sFile,cpSection,sOptions, sValue );
        ELSE
            update cuon_config set client_id = iClientID, config_file = sFile, section = cpSection, option = sOptions,value = sValue  where key_id = r1.key_id ;
            
        END IF ;
        return ok ;

    END; 

    
    $$ LANGUAGE 'plpgsql';
  
  
  
CREATE OR REPLACE  FUNCTION   fct_get_config_option(iClientID int,sFile varchar(200), cpSection varchar(250), sOptions  varchar(250 ) )RETURNS text as $$ 
    DECLARE 
     r1 record ;
     svalue text ;
     ok boolean ;
     sSql text ;
    BEGIN 
     -- raise notice $$' config options = % , %, %, %$$', iClientID, sFile,cpSection,sOptions ;
        sSql := $$'select value from cuon_config where client_id = $$' || iClientID || $$' and config_file = $$'  || quote_literal(sFile) || $$'  and  section = $$' || quote_literal(cpSection) || $$' and  option = $$' || quote_literal(sOptions )  ;
        -- raise notice $$' sSql = %$$', sSql ;
        execute(sSql) into svalue ;
        
        return svalue ;

    END; 

    
    $$ LANGUAGE 'plpgsql';

 

CREATE OR REPLACE   FUNCTION   fct_round_2_digits(value float)  RETURNS float as $$ 
    DECLARE 
        retValue float ;
    BEGIN 
        select into retValue  round(value::numeric,2);
        return retValue ;
        
    END; 

    
    $$ LANGUAGE 'plpgsql';
    
DROP FUNCTION   fct_setDMSPairedID() CASCADE ;
CREATE OR REPLACE   FUNCTION   fct_setDMSPairedID() returns OPAQUE as $$
    --  set default values at a delete 
    -- if delete a record, dont realy delete, set status to delete 
     
    DECLARE
    
        f_upd    text;
        r1 record ;
        sSql text ;
        
    BEGIN
        if NEW.paired_id is not null then 
        
            if NEW.paired_id >  0 then 
           
                sSql := $$'select file_format, file_suffix, document_rights_activated, document_rights_user_read,  document_rights_user_write ,  document_rights_user_execute ,  document_rights_group_read  , document_rights_group_write , document_rights_group_execute, document_rights_all_read  , document_rights_all_write , document_rights_all_execute , document_rights_user , document_rights_groups , dms_extract from dms where id = $$' || NEW.paired_id ;
                execute(sSql) into r1 ;
                RAISE NOTICE $$' paired id dms = %$$', r1 ;
                
                NEW.file_format := r1.file_format ;
                NEW.file_suffix := r1.file_suffix ;
                NEW.document_image := NULL ;
                RAISE NOTICE $$' paired suffix2 = %$$', NEW.file_suffix ;
            end if ;
        end if ;
        
        RETURN NEW ;
    END;
     
     $$ LANGUAGE 'plpgsql'; 

   

CREATE OR REPLACE FUNCTION  fct_getValueAsCurrency(value float) returns text as $$

    DECLARE
        sSql text ;  
        sValue text ;
        iClient int ;
        sCurrencySign text ;
        iCurrencyPrecision int ;
        fValue float ;
        sFormatter text ;
        
    BEGIN 
        iClient = fct_getUserDataClient(  ) ;
        sCurrencySign = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'CurrencySign$$') ; 
        iCurrencyPrecision = fct_get_config_option(iClient,$$'clients.ini$$', $$'CLIENT_$$' || iClient, $$'CurrencyPrecision$$')::int ;  
        if sCurrencySign is null then 
            sCurrencySign = $$'EUR$$' ;
        end if ;
        if iCurrencyPrecision is null then 
            iCurrencyPrecision = 2 ;
        end if ;
        select into fValue  round(value::numeric,iCurrencyPrecision);
        -- raise notice $$'Currency = % with %$$',sCurrencySign, iCurrencyPrecision ;
        if iCurrencyPrecision = 0 then 
            sFormatter = $$'99G999G999G999D$$' ;
        elseif iCurrencyPrecision = 2 then 
            sFormatter = $$'99G999G999G999D99$$' ;
        end if ;
            
        sValue = to_char(fValue,sFormatter) || $$' $$' || sCurrencySign ;
        if sValue is null then 
            sValue = $$'0$$' ;
        end if ;
        
        return sValue ;
    END ;
    
     $$ LANGUAGE 'plpgsql';      

  



CREATE OR REPLACE FUNCTION  fct_get_modul_name(iModul int) returns text as $$

    DECLARE
	sSql text ;  
     	sValue text ;
     	iClient int ;
     	sModul text ;
   
     Begin

	
     	return sModul ;
     END ;
     
     $$ LANGUAGE 'plpgsql';      


CREATE OR REPLACE FUNCTION  fct_get_statusbar(iModul int, iID int) returns text as $$

    DECLARE
        sSql text ;  
        sValue text ;
        iClient int ;
      	sModul text ;
	r record;

   Begin
	 iClient = fct_getUserDataClient(  ) ;
	 sValue = $$'NONE$$';
	 if iModul = 15300 then
	    sSql = $$' select pr.name ||$$'$$',$$'$$' ||  pa.name || $$'$$',$$'$$' || mo.name ||  $$'$$' - $$'$$'  || mo.designation as sValue from sourcecode_projects as pr, sourcecode_parts as pa , sourcecode_module as mo, sourcecode_file as sf where sf.id = $$' || iID || $$' and mo.id = sf.modul_id and pa.id = mo.part_id and pr.id = pa.project_id$$' ;
	  
	elseif  iModul = 15200 then
	    sSql = $$' select pr.name ||$$'$$',$$'$$' ||  pa.name || $$'$$',$$'$$' || pa.designation  as sValue from sourcecode_projects as pr, sourcecode_parts as pa , sourcecode_module as mo where mo.id = $$' || iID || $$'and  pa.id = mo.part_id and pr.id = pa.project_id$$' ;

	elseif  iModul = 15100 then
	    sSql = $$' select pr.name ||$$'$$',$$'$$' ||  pr.designation  as sValue from sourcecode_projects as pr, sourcecode_parts as pa where pa.id = $$' || iID || $$'and  pr.id = pa.project_id $$' ;

	    
	end if ;

        raise notice $$'sSql = %$$',sSql ;
	if sSql is not NULL then 
	        execute(sSql) into r ;
	        sValue := r.svalue ;
	end if ;
  

	return sValue ;
  END ;
    
     $$ LANGUAGE 'plpgsql';      


CREATE OR REPLACE FUNCTION  fct_executeTest() returns bool as $$

    DECLARE
    ok boolean ;
    success int ;
    sSql text ;
    
    BEGIN

    sSql := $$'insert into DMS (id,sub1) values (nextval( $$' || quote_literal($$'dms_id$$') || $$'), $$' || quote_literal($$'12345loooooooooookkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk3434341234567890kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk123456789012345678902123456789031234567890$$') || $$')$$' ;
    success =  fct_executeChecked(encode(sSql,$$'base64$$'));
    
      return success ;

    END ;
    
     $$ LANGUAGE 'plpgsql'; 



CREATE OR REPLACE FUNCTION  fct_executeChecked(sSqlb64 text) returns bool as $$

    DECLARE
    ok boolean ;
    success int ;
    sSql text ;
    BEGIN
	 -- raise notice $$'sSqlb64 =  %$$', sSqlb64 ;

	-- BEGIN
		sSql = CONVERT_FROM(decode(sSqlb64,$$'base64$$'),$$'UTF-8$$');
	-- EXCEPTION
		-- ok := false;
--  raise notice $$'wrong utf-code % $$',1 ;
	--  raise notice $$'sSql =  %$$', sSql ;
	BEGIN  
    	   execute (sSql) ;
	   success = 1;
	   raise notice $$'success = %$$', success ;
	EXCEPTION
	   when others then 
	    success = 0;
	   raise notice $$'success = %$$', success ;
        END;
    return success ;

    END ;
    
     $$ LANGUAGE 'plpgsql'; 


CREATE OR REPLACE FUNCTION  fct_set_block(tablename text, id int) returns bool  as $$
    DECLARE

	ok bool ;
	sSql text ;
	var_id text ;
	val int ;
	found_data int ;
	r record ;
	
    BEGIN

	ok = false ;
	found_data = 0 ;
	
	sSql := $$'select  $$' ;

	if tablename = $$'orderbook$$' then
	   var_id =  $$'order_id$$' ;
	elseif  tablename = $$'invoice$$' then
	  var_id =  $$'invoice_id$$' ;
	elseif  tablename = $$'in_payment$$' then
	 var_id =  $$'inpayment_id$$' ;
	elseif  tablename = $$'cash_desk$$' then
	 var_id =  $$'cash_desk_id$$' ;
		
	end if;

	sSql = sSql  || var_id ;
	sSql = sSql || $$' as lock_id  from lock_process where $$' || var_id || $$' = $$'  || id ;

	raise notice $$' sSql =%$$' , sSql ;
	execute (sSql) into r;
	raise notice $$' r.lock_id = %$$',r.lock_id ;
	
	if r.lock_id is null   then

	   ok = true ;
	   select into val  nextval ($$'lock_process_id$$') ;
	   sSql := $$' insert into lock_process (id, $$' || var_id || $$',user_id) values ( $$'  || val || $$', $$' || id ||$$',$$' ||   fct_getUserDataClient(  )  || $$')$$' ;
	   
	   
	   	raise notice $$' sSql =%$$' , sSql ;	
		execute sSql ;

	end if ;

	raise notice $$' ok = %$$' , ok ;
    	 


	return ok ;
	
    END ;
    	
     $$ LANGUAGE 'plpgsql';   

