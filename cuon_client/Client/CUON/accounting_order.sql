-- -*- coding: utf-8 -*-

-- Copyright (C) [2003 -2012]  [Juergen Hamel, D-32584 Loehne, Germany]

-- This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as
-- published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.

-- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
-- for more details.

-- You should have received a copy of the GNU General Public License along with this program; if not, write to the
-- Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA. 
-- IMPORTANT
-- To use this without cuon (www.cuon.org) replace all $$ with a '


CREATE OR REPLACE FUNCTION fct_get_account_id(acc_system_id int ) returns int AS $$
 DECLARE
  iClient int ;
    
    iNewAccountingID int ;
    sSql text ;
    
    
  BEGIN
    
        iClient = fct_getUserDataClient(  ) ;
        sSql := $$'select id  from account_info where accounting_system_type = $$' || quote_literal(acc_system_id) || fct_getWhere(2,$$' $$')  ;
        execute(sSql) into iNewAccountingID ;
        if iNewAccountingID is null then 
            iNewAccountingID := 0;
        end if ;
        
        return iNewAccountingID; 
        
     
       
        
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
         
        
 
DROP FUNCTION fct_write_invoice(int) CASCADE ;
DROP FUNCTION fct_write_invoice(varchar(36)) CASCADE ;

CREATE OR REPLACE FUNCTION fct_write_invoice(cUUID varchar(36) ) returns text AS $$
 DECLARE
    
    iClient int ;
    iNewOrderID int ; 
    iNewPositionID int ;
    iNewAccountingID int ;
    sSql text ;
    sSql1 text ;
    
    sSql2 text ;
    account_revenue_id int;
    account_receivable_id int ;
    account_taxvat_id int ;
    account_revenue_net_id int := 0;
    
    cur1 refcursor ;
     sCursor text ;
    r record ;
    r1 record ;
    r2 record ;
    old_transaction_uuid varchar(36) ;
    new_transaction_uuid varchar(36) ;
      fAmount     float;
    fPrice  float;
    fDiscount   float ;
    count   integer ;
    fTaxVat float ;
    iArticleID integer ;
    bNet  bool ;
    vMisc return_misc3 ;
    iTaxVatID int ;
    fSum float ;
    fSumNet float ;
    fSumTaxVat float ;
    
    BEGIN
        raise notice $$'write invoice for uuid %$$', cUUID ;
        
        iClient = fct_getUserDataClient(  ) ;
        -- sSql := $$'select id  from account_info where accounting_system_type = $$'$$'2400$$'$$' $$' || fct_getWhere(2,$$' $$')  ;
        -- execute(sSql) into account_receivable_id ;
        account_receivable_id = fct_get_account_id(2400) ;
        
        -- sSql := $$'select id  from account_info where accounting_system_type = $$'$$'5000$$'$$' $$' || fct_getWhere(2,$$' $$')  ;
        -- execute(sSql) into account_revenue_id ;
        account_revenue_id =  fct_get_account_id(5000) ;
        raise notice $$'account info id = %$$',account_receivable_id ;
        old_transaction_uuid := fct_null_uuid() ;
        new_transaction_uuid := fct_new_uuid() ;
        Begin 
            select into r * from list_of_invoices where uuid = cUUID ;
            if account_revenue_id  is not null  AND account_receivable_id is not null THEN 
                if r.last_transaction_uuid != fct_null_uuid() then
                    raise notice $$' uuid = %$$',r.last_transaction_uuid ;
                   
                    -- delete old accountings
                    sSql2 := $$' select * from account_system where transaction_uuid = $$' || quote_literal(r.last_transaction_uuid) ;
                    raise notice $$' sSql2 = %$$', sSql2 ;
                    
                    FOR r2 in execute(sSql2) 
                        LOOP
                            sSql := $$'select * from fct_create_single_account($$' || iClient || $$', $$' || quote_literal(r.invoice_number )|| $$', $$' || quote_literal($$'Delete $$' || r2.document_designation )  || $$', $$' || quote_literal(r.date_of_invoice) || $$', $$' || r2.account_id || $$', $$' || r2.contra_account_id || $$', $$' || r2.amount * -1  || $$', $$' ||  r2.address_id || $$', $$' || quote_literal(new_transaction_uuid ) || $$' )  $$' ;
                            execute(sSql) into iNewAccountingID ;
                            raise notice $$'new accounting id at delete = % $$', iNewAccountingID ;
                           
                            
                        END LOOP ;
                end if ;
                new_transaction_uuid := fct_new_uuid() ;
                select into r1 * from orderbook where id = r.order_number ;
                
                -- revenue to accounts receivable, 
                
                -- raise notice $$'r = %$$', r ;
                -- raise notice $$'r1 = %$$', r1 ;
                
               
                -- Now check the positions and the tax vat
                sSql1 := $$'select * from fct_create_single_account($$' || iClient || $$', $$' || quote_literal(r.invoice_number )|| $$', $$' || quote_literal( $$'Invoice $$' || $$', $$' || r.invoice_number  || $$' , $$' || r.date_of_invoice ) || $$', $$' || quote_literal(r.date_of_invoice) || $$', $$' || account_receivable_id || $$', $$' || account_revenue_id || $$', $$' || r.total_amount || $$', $$' ||  r1.addressnumber || $$', $$' || quote_literal(new_transaction_uuid ) || $$' )  $$' ;
                execute(sSql1) into iNewAccountingID ;
                raise notice $$'new accounting id = % $$', iNewAccountingID ;
                
                sCursor := $$'SELECT amount, price, discount, articleid, tax_vat FROM ORDERPOSITION WHERE  orderid = $$'|| r.order_number || $$' $$' || fct_getWhere(2,$$' $$')  ;
            fSum := 0.0 ;
            -- RAISE NOTICE $$'sCursor = %$$', sCursor ;
            OPEN cur1 FOR EXECUTE sCursor ;
            FETCH cur1 INTO fAmount, fPrice, fDiscount,iArticleID, fTaxVat ;
               WHILE FOUND LOOP
                    raise notice $$'Cursor = %, %, %, %, % $$',  fAmount, fPrice, fDiscount,iArticleID, fTaxVat ;
                    if fDiscount IS NULL then
                        fDiscount := 0.0 ;
                    end if ;
                    if fTaxVat IS NULL then 
                        fTaxVat:= 0.00;
                    END IF;
                    if fPrice IS NULL then
                        fPrice := 0.0 ;
                    end if ;
                    vMisc := fct_get_taxvat_for_article(iArticleID);
                    
                    fTaxVat = vMisc.b ;
                    iTaxVatID = vMisc.a ;
                    
                    -- now search for brutto/netto
                    bNet := fct_get_net_for_article(iArticleID);
                    raise notice $$' order bNet value is %$$',bNet ;
                    if fTaxVat > 0 THEN
                        if bNet = true then 
                        -- raise notice $$' order calc as bnet is true$$';
                            fSum :=  ( fAmount * ( fPrice + (fPrice *fTaxVat/100) ) * (100 - fDiscount)/100 ) ;
                            fSumNet := ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                            fSumTaxVat := fSum -fSumNet ;
                            
                        else 
                            fSum :=  ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                            fSumNet :=  ( fAmount * ( fPrice - (fPrice/(fTaxVat+100) *100) ) * (100 - fDiscount)/100 ) ;
                            fSumTaxVat := fSum -fSumNet ;
                        end if ;
                        
                    ELSE    
                        fSum := ( fAmount * (fPrice * (100 - fDiscount)/100 ) ) ;
                        fSumNet := fSum ;
                        fSumTaxVat := 0.00 ;
                        
                    END IF ;
                    
                    
                    raise notice $$'sum, net, taxvat, tax_vat_id, tax_vat_acct = %, %, % , %, % $$', fSum, fSumNet, fSumTaxVat, iTaxVatID, vMisc.c ;
                    
                    -- now set the accounts for the taxvat:
                    if iTaxVatID is null then 
                        iTaxVatID = 0;
                    end if ;
                    
                    if iTaxVatID > 0 AND fSumTaxVat > 0.00  then 
                        
                        -- first the taxvat account id
                        sSql := $$'select id  from account_info where accounting_system_type = $$' || quote_literal(vMisc.c) || fct_getWhere(2,$$' $$')  ;
                        raise notice $$'sSql = % $$',sSql ;
                        
                        execute(sSql) into account_taxvat_id ;
                         sSql := $$'select id  from account_info where accounting_system_type = $$' || quote_literal(vMisc.c::int +2000) || fct_getWhere(2,$$' $$')  ;
                        raise notice $$'sSql = % $$',sSql ;
                        execute(sSql) into account_revenue_net_id ;
                        
                        
                        -- First book the revenue to taxvat 
                        
                        sSql1 := $$'select * from fct_create_single_account($$' || iClient || $$', $$' || quote_literal(r.invoice_number )|| $$', $$' || quote_literal( $$'TaxVat $$' || $$', $$' || r.invoice_number  || $$' , $$' || r.date_of_invoice ) || $$', $$' || quote_literal(r.date_of_invoice) || $$', $$' || account_revenue_id || $$', $$' || account_taxvat_id  || $$', $$' || fSumTaxVat || $$', $$' ||  r1.addressnumber || $$', $$' || quote_literal(new_transaction_uuid ) || $$' )  $$' ;
                        raise notice $$' book the taxvat = % $$', sSql1 ;
                        
                        execute(sSql1) into iNewAccountingID ;
                        raise notice $$'new accounting id = % $$', iNewAccountingID ;
                        
                      
                        
                    else
                      -- set all to the null taxvat account
                      
                       -- first the taxvat account id
                        sSql := $$'select id  from account_info where accounting_system_type = $$' || quote_literal($$'38010$$') || fct_getWhere(2,$$' $$')  ;
                        raise notice $$'sSql = % $$',sSql ;
                        
                        execute(sSql) into account_taxvat_id ;
                         sSql := $$'select id  from account_info where accounting_system_type = $$' || quote_literal($$'43010$$') || fct_getWhere(2,$$' $$')  ;
                        raise notice $$'sSql = % $$',sSql ;
                        execute(sSql) into account_revenue_net_id ;
                        
                        
                      
                    end if ;
                    
                    raise notice $$'net values = %$$', account_revenue_net_id ;
                    
                    -- Then set the net to the new revenue id
                    sSql1 := $$'select * from fct_create_single_account($$' || iClient || $$', $$' || quote_literal(r.invoice_number )|| $$', $$' || quote_literal( $$'Net Value $$' || $$', $$' || r.invoice_number  || $$' , $$' || r.date_of_invoice ) || $$', $$' || quote_literal(r.date_of_invoice) || $$', $$' || account_revenue_id || $$', $$' || account_revenue_net_id  || $$', $$' || fSumNet || $$', $$' ||  r1.addressnumber || $$', $$' || quote_literal(new_transaction_uuid ) || $$' )  $$' ;
                    
                    raise notice $$' book the net = % $$', sSql1 ;
                    
                    execute(sSql1) into iNewAccountingID ;
                    raise notice $$'new accounting id = % $$', iNewAccountingID ;
                
                  
                    FETCH cur1 INTO fAmount, fPrice, fDiscount ,iArticleID, fTaxVat;
                END LOOP ;
                
    
            END IF ;
        exception when others then
          raise notice $$'exception error $$';
          raise notice $$'new transaction uuid = %$$', new_transaction_uuid ;
          return new_transaction_uuid ;
          END ;
       
       return new_transaction_uuid ;
        
    END ;
    
     $$ LANGUAGE 'plpgsql'; 
     
